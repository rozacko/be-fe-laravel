<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class FeAuthenticate
{
    public function handle(Request $request, Closure $next)
    {
        $this->authenticate($request);

        return $next($request);
    }

    protected function authenticate($request)
    {
        if ($request->session()->has('user') && $request->session()->has('token')) {
            return true;
        }

        $this->unauthenticated($request);
    }

    protected function unauthenticated($request)
    {
        throw new AuthenticationException(
            'Unauthenticated.',
            [],
            $this->redirectTo($request)
        );
    }

    protected function redirectTo($request)
    {
        if (!$request->expectsJson()) {
            return route('login');
        }
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TPMReportController extends Controller
{

    public function __construct()
    {
        $this->title = 'TPM Report';
        $this->route = '/tpm-report';
        $this->endpoint_scoreimplementation = 'tpm/scoreimplementation';
        $this->endpoint_focusedimprovement = 'tpm/focusedimprovement';
        $this->endpoint_focusarea = 'tpm/focusarea';
        $this->endpoint_genbasga = 'tpm/genbasga';
        $this->endpoint_partisipasiMember = 'tpm/partisipasiMember';
    }

    public function index()
    {
        $data = [
            'title' => $this->title,
            'route' => $this->route,
        ];
        addVendors(['amcharts', 'apexcharts']);
        return view('pages.tpmreport.tpm-report', $data);
    }

    public function scoreimplementation(Request $request)
    {
        return HttpTable($request, $this->endpoint_scoreimplementation);
    }

    public function focusedimprovement(Request $request)
    {
        return HttpTable($request, $this->endpoint_focusedimprovement);
    }

    public function focusarea(Request $request)
    {
        return HttpTable($request, $this->endpoint_focusarea);
    }

    public function genbasga(Request $request)
    {
        return HttpTable($request, $this->endpoint_genbasga);
    }

    public function partisipasiMember(Request $request)
    {
        return HttpTable($request, $this->endpoint_partisipasiMember);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PlantInspectionController extends Controller
{
    public function __construct()
    {
        $this->view = 'pages.inspection';
        $this->route = 'inspection';
        $this->endpointExport= 'plant_reability/download-template';
        $this->endpointImport= 'plant_reability/read-template';
        $this->endpointPlant= 'plant';
        $this->endpointArea= 'area';
        $this->endpointFungsi= 'plant_reability/get-fungsi';
        $this->endpointStore= 'plant_reability';
        $this->endpointDatatable= 'plant_reability/get-data';
        $this->endpointDatatableApprove = 'plant_reability/get-data-approval';
        $this->endpointApprove= 'plant_reability/get-detail';
        $this->endpointStoreApprove= 'plant_reability/approval';
        $this->endpointRole = 'role';
    }

    public function index(Request $request)
    {
        $name = $request->session()->get('user')->username;
        addVendors(['amcharts', 'apexcharts']);
        $data = [
            'route'=> $this->route,
            'name'=> $name
        ];
        return view($this->view . '.index', $data);
    }

    public function add(Request $request)
    {
        addVendors(['datatables']);
        $data = [
            'route'=> $this->route,
            'urlExport'=> env('API_URL').$this->endpointExport,
            'urlImport'=> env('API_URL').$this->endpointStore,
            'token'=> $request->session()->get('token'),
        ];
        return view($this->view . '.tambah', $data);
    }

    public function approve($id)
    {
        $data=[
            'id'=>$id,
            'route'=> $this->route,
        ];
        
        addVendors(['datatables']);
        return view($this->view . '.approve',$data);
    }

    public function detail($id)
    {
        $data=[
            'id'=>$id,
            'route'=> $this->route,
        ];
        
        addVendors(['datatables']);
        return view($this->view . '.detail',$data);
    }

    public function edit(Request $request, $id)
    {
        $data=[
            'id'=>$id,
            'route'=> $this->route,
            'urlExport'=> env('API_URL').$this->endpointExport,
            'urlImport'=> env('API_URL').$this->endpointStore,
            'token'=> $request->session()->get('token'),
        ];
        
        addVendors(['datatables']);
        return view($this->view . '.edit',$data);
    }

    public function modalAdd()
    {
        return view($this->view . '.create');
    }

    public function modalImport()
    {
        $data = [
            'route'=> $this->route,
        ];
        return view($this->view . '.import', $data);
    }

    public function import(Request $request)
    {
        return HttpPostFile($request, $this->endpointImport);
    }

    public function filterPlant(Request $request)
    {
        return HttpTable($request, $this->endpointPlant);
    }

    public function filterArea(Request $request)
    {
        return HttpTable($request, $this->endpointArea);
    }

    public function filterFungsi(Request $request)
    {
        return HttpTable($request, $this->endpointFungsi);
    }

    public function store(Request $request)
    {
        return HttpPost($request, $this->endpointStore);
    }

    public function datatable(Request $request)
    {
        return HttpTable($request, $this->endpointDatatable);
    }

    public function datatableApprove(Request $request)
    {
        return HttpTable($request, $this->endpointDatatableApprove);
    }

    public function detailData(Request $request)
    {
        $data = HttpGet($request, $request->id, $this->endpointApprove);
        $dataHeader = [
            'no_inspection'=>$data->kode_inspection,
            'desc_inspection'=>$data->desc_inspection,
            'tgl_inspection'=>$data->tgl_inspection,
            'kode_plant'=>$data->kode_plant,
            'kode_area'=>$data->desc_area,
            'nama_fungsi'=>$data->nama_fungsi
        ];
        $dataItem = $data->item;
        $good = 0;
        $low = 0;
        $med = 0;
        $high = 0;
        foreach ($dataItem as $value) {
            if ($value->id_kondisi == 1) {
                $good += 1;
            }elseif ($value->id_kondisi == 2) {
                $low += 1;
            }elseif ($value->id_kondisi == 3) {
                $med += 1;
            }elseif ($value->id_kondisi == 4) {
                $high += 1;
            }
        }

        $dataSummary = [
            [
                'area'=> $dataItem[0]->nama_area,
                'good'  => $good,
                'low'  => $low,
                'med'  => $med,
                'high'  => $high,
            ]
        ];
        $dataComment = $data->comment;
        $response = [
            'dataHeader'=>$dataHeader,
            'dataItem'=>$dataItem,
            'dataSummary'=>$dataSummary,
            'dataComment'=>$dataComment,
        ];
        return $response;
    }

    public function storeApprove(Request $request)
    {
        return HttpPost($request, $this->endpointStoreApprove);
    }
}

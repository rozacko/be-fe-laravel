<?php

namespace App\Http\Controllers\Production;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CrusherReportsController extends Controller
{
    //
    protected $title;
    protected $route;
    protected $endpoint;

    public function __construct()
    {
        $this->title = 'Crusher Production';
        $this->route = 'production.crusher';
        $this->endpoint = 'production/crusher';
    }

    public function index()
    {
        $page_title = $this->title;
        $page_description = '';
        $data = [
            'pagetitle' => $page_title,
            'subtitle' => 'report ' . strtolower($page_title),
            'route' => $this->route,
            'breadcrumb' => ['Production' => null, $page_title => route($this->route.'.index') ],
        ];
        addVendors(['datatables']);
        return view('pages.'.$this->route . '.index', compact('page_title', 'page_description'), $data);        
    }

    public function table(Request $request){
        return HttpTable($request, $this->endpoint);
    }
}

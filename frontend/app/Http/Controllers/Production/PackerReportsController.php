<?php

namespace App\Http\Controllers\Production;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PackerReportsController extends Controller
{
    //
    protected $title;
    protected $route;
    protected $endpoint;

    public function __construct()
    {
        $this->title = 'Packer Production';
        $this->route = 'production.packer';
        $this->endpoint = 'production/packer';
    }

    public function index()
    {
        $page_title = $this->title;
        $page_description = '';
        $data = [
            'pagetitle' => $page_title,
            'subtitle' => 'report ' . strtolower($page_title),
            'route' => $this->route,
            'breadcrumb' => ['Production' => null, $page_title => route($this->route.'.index') ],
        ];
        addVendors(['datatables','amcharts4']);
        return view('pages.'.$this->route . '.index', compact('page_title', 'page_description'), $data);        
    }

    public function table(Request $request){
        return HttpTable($request, $this->endpoint);
    }

    public function status(Request $request){
        return HttpTable($request, $this->endpoint.'status');
    }

    public function silo(Request $request){
        return HttpGet($request,'',$this->endpoint.'silo');
    }
}

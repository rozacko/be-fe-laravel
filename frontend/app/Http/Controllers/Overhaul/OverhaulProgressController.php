<?php

namespace App\Http\Controllers\Overhaul;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class OverhaulProgressController extends Controller
{
    //
    protected $title;
    protected $view;
    protected $endpoint;
    protected $route;
    
    function __construct()
    {
        $this->title = 'Dashboard Overhaul Progress';
        $this->view = 'pages.overhaul.progress';
        $this->endpoint = 'overhaul/progress/';
        $this->route = 'overhaul.progress';
    }

    public function index(){
        addVendors(['datatables','apexcharts']);

        return view('pages.overhaul.progress', ['route' => $this->route]);
    }

    public function getPeriode($id_plant, Request $request){
        return HttpGet($request, $id_plant, $this->endpoint.'periode');
    }

    public function addPeriode(Request $request){
        return HttpPost($request, $this->endpoint.'periode');
    }

    public function templateActivity(Request $request){
        return HttpGetFile($request, $this->endpoint.'template/activity');
    }

    public function uploadActivity(Request $request){
        return HttpPostFile($request, $this->endpoint.'upload/activity');
    }

    public function addActivity(Request $request){
        return HttpPost($request, $this->endpoint.'activity');
    }

    public function getActivity($plant, Request $request){
        return HttpGet($request, $plant, $this->endpoint.'activity');
    }

    public function templateProgress($plant, Request $request){
        return HttpGetFile($request, $this->endpoint.'template/progress/'.$plant);
    }

    public function uploadProgress(Request $request){
        return HttpPostFile($request, $this->endpoint.'upload/progress');
    }

    public function addProgress(Request $request){
        if ($request->hasFile('upload_file')) {            
            return HttpPostFile($request, $this->endpoint.'progress');
        }
        else{
            return HttpPost($request, $this->endpoint.'progress');
        }
    }

    public function progressMajor(Request $request){
        return HttpTable($request, $this->endpoint.'major');
    }

    public function progressMinor(Request $request){
        return HttpTable($request, $this->endpoint.'minor');
    }

    public function getDokumentasi($plant, Request $request){
        return HttpGet($request, $plant, $this->endpoint.'dokumentasi');
    }

    public function getChart($plant, Request $request){
        return HttpGet($request, $plant, $this->endpoint.'chart');
    }

    public function unsafe(Request $request){
        return HttpTable($request, $this->endpoint.'unsafe');
    }

    public function opex(Request $request){
        return HttpTable($request, $this->endpoint.'opex');
    }


}

<?php

namespace App\Http\Controllers\Overhaul;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class OverhaulFinalReportController extends Controller
{
    //
    protected $title;
    protected $view;
    protected $endpoint;
    protected $route;
    
    function __construct()
    {
        $this->title = 'Dashboard Overhaul Final Report';
        $this->view = 'pages.overhaul.finalreport';
        $this->endpoint = 'overhaul/finalreport/';
        $this->route = 'overhaul.finalreport';
    }

    public function index(){
        addVendors(['datatables']);

        return view('pages.overhaul.finalreport', ['route' => $this->route]);
    }

    public function table(Request $request){
        return HttpTable($request, $this->endpoint);
    }

    
    public function add(Request $request){
        if ($request->hasFile('upload_file')) {            
            return HttpPostFile($request, $this->endpoint);
        }
        else{
            return HttpPost($request, $this->endpoint);
        }
    }

    public function destroy(Request $request, $uuid){
        return HttpDelete($request, $uuid, $this->endpoint.'delete');
    }
}

<?php

namespace App\Http\Controllers\Overhaul;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class OverhaulPreparationController extends Controller
{
    //
    protected $title;
    protected $view;
    protected $endpoint;
    protected $route;
    
    function __construct()
    {
        $this->title = 'Dashboard Overhaul Preparation';
        $this->view = 'pages.overhaul.preparation';
        $this->endpoint = 'overhaul/preparation/';
        $this->route = 'overhaul.preparation';
    }

    public function index(){
        addVendors(['datatables','amcharts', 'apexcharts']);

        return view('pages.overhaul.preparation',['route' => $this->route]);
    }

    public function getChartPart(Request $request){
        return HttpGet($request, '', $this->endpoint.'partchart');
    }

    public function getTablePart(Request $request){
        return HttpTable($request, $this->endpoint.'parttable');
    }

    public function getTimeline(Request $request){
        return HttpGet($request,'', 'overhaul/progress/timelinechart');
    }

    public function getTemplate(Request $request){
        return HttpGetFile($request, $this->endpoint.'import');
    }

    public function uploadData(Request $request){
        return HttpPostFile($request, $this->endpoint.'import');
    }

    public function getTemplateBudget(Request $request){
        return HttpGetFile($request, $this->endpoint.'budget');
    }

    public function uploadDataBudget(Request $request){
        return HttpPostFile($request, $this->endpoint.'budget');
    }

    public function budgetUk(Request $request){
        return HttpGet($request,'',$this->endpoint.'barchartbudget');
    }

    public function partDonutChart(Request $request){
        return HttpGet($request,'',$this->endpoint.'partchartbudget');
    }

    public function opexDonut(Request $request){
        return HttpGet($request,'',$this->endpoint.'budgetopexdonut');
    }

    public function tableCapex(Request $request){
        return HttpTable($request, $this->endpoint.'tablecapex');
    }  
    
    public function tableIssue(Request $request){
        return HttpTable($request, $this->endpoint.'tableissue');
    }  
    
}

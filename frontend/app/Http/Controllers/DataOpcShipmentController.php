<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DataOpcShipmentController extends Controller
{
    public function index()
    {
        addVendors(['datatables']);
        return view('pages.opcshipment.index');
    }
}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TPMReportLaporanKPIMemberTPMController extends Controller
{

    public function __construct()
    {
        $this->title = 'Laporan KPI Member TPM';
        $this->route = 'tpm-report-laporan-kpi-member-tpm';
        $this->endpoint = 'tpm/listLaporankpimember';
        $this->endpointfilterorganisasi = 'tpm/filterOrganisasi';
        $this->endpointfilterunitkerja = 'tpm/filterUnitKerja';
    }

    public function index()
    {
        $data = [
            'title' => $this->title,
            'route' => $this->route
        ];
        addVendors(['amcharts', 'apexcharts']);
        return view('pages.tpmreport.tpm-report-laporan-kpi-member-tpm', $data);
    }

    public function table(Request $request)
    {
        return HttpTable($request, $this->endpoint);
    }

    public function FilterOrganisasi(Request $request)
    {
        return HttpTable($request, $this->endpointfilterorganisasi);
    }

    public function FilterUnitKerja(Request $request)
    {
        return HttpTable($request, $this->endpointfilterunitkerja);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

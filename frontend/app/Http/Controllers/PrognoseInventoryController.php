<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PrognoseInventoryController extends Controller
{

    public function __construct()
    {
        $this->title = 'Prognose Inventory Index';
        $this->route = 'prognose-inventory';
        $this->view = 'pages.prognose_inventory';
        $this->endpoint = 'rkap-prognose/prognose-inventory';
        $this->endpointImport = 'rkap-prognose/prognose-inventory-import';
        $this->endpointdownloadexcel = 'rkap-prognose/prognose-inventory-download';
    }

    public function table(Request $request)
    {
        return HttpTable($request, $this->endpoint);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page_title = $this->title;
        $page_description = '';
        $data = [
            'pagetitle'     => $page_title,
            'urlExport'   => env('API_URL').$this->endpointdownloadexcel,
            'token'         =>$request->session()->get('token'),
            'route'         => $this->route
        ];
        addVendors(['datatables']);
        return view($this->view . '.index', compact('page_title', 'page_description'), $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'pagetitle' => $this->title,
            'route' => $this->route
        ];
        return view($this->view . '.create', $data);
    }

    public function DownloadTemplate()
    {
        return Storage::disk('TempExcel')->download('template-prognose-inventory.xlsx');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return HttpPostFile($request, $this->endpointImport);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $dataRKAP = HttpGet($request, $id, $this->endpoint);
        $data = [
            'route'=> $this->route,
            'data' => $dataRKAP,
        ];

        return view($this->view . ".edit", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return HttpPut($request, $id, $this->endpoint);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        return HttpDelete($request, $id, $this->endpoint);
    }
}

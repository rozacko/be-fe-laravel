<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class KoreksiKPIController extends Controller
{
    public function __construct()
    {
        $this->title = 'Koreksi KPI';
        $this->view = 'pages.koreksikpi';
        $this->route = 'koreksi-kpi';
        $this->endpointstore = 'kpidireksi/import';
        $this->endpointdata = 'kpidireksi/list';
        $this->endpointexport = 'kpidireksi/export';
    }

    public function index(Request $request)
    {
        $data = [
            'title'=> $this->title,
            'route'=> $this->route,
        ];
        return view($this->view . '.index', $data);
    }

    public function temp()
    {
        return Storage::disk('TempExcel')->download('template-koreksi-kpi-direksi.xlsx');
    }

    public function datatable(Request $request)
    {
        return HttpTable($request, $this->endpointdata);
    }

    public function create()
    {
        $data = [
            'route'=> $this->route,
        ];
        return view($this->view . '.create-kpi', $data);
    }

    public function store(Request $request)
    {
        return HttpPostFile($request, $this->endpointstore);
    }

    public function download(Request $request)
    {
        return HttpPostDownloadExcel($request, $this->endpointexport);
    }
}
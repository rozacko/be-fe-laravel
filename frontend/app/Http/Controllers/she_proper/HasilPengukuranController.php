<?php
namespace App\Http\Controllers\she_proper;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Http;
use DataTables;
use Session;
use DB;

class HasilPengukuranController extends Controller
{
    public function __construct()
    {
        $this->title = 'Enviro Data Digital Dashboard';
        $this->route = 'she_proper.hasil_pengukuran';
        $this->endpoint = 'hasil_pengukuran';
    }

    public function index()
    {
        $page_title = $this->title;
        $page_description = '';
        $data = [
            'pagetitle' => $page_title,
            'subtitle' => 'manage data ' . strtolower($page_title),
            'route' => $this->route,
            'breadcrumb' => ['SHE & Proper / Enviro Digital Data' => null, $page_title => route($this->route.'.index') ],
            'bulan' => bulan(),
            'tahun' => tahun(),
        ];
        addVendors(['datatables']);
        return view($this->route . '.index', compact('page_title', 'page_description'), $data);        
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {        
        $data = [
            'pagetitle' => $this->title,
            'subtitle' => 'manage data ' . strtolower($this->title),
            'route' => $this->route,
            'bulan' => bulan(),
            'tahun' => tahun(),
        ];

        return view($this->route . '.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {         
        return HttpPost($request, $this->endpoint);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {        
        $param = [
            'pagetitle' => $this->title,
            'subtitle' => 'manage data ',
            'route' => $this->route,
            'data' => HttpGet($request, $id, $this->endpoint),
            'bulan' => bulan(),
            'tahun' => tahun(),
        ];
        return view($this->route . '.edit', $param);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return HttpPut($request, $id, $this->endpoint);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function table(Request $request, $tahun)
    {
        $request['tahun'] = $tahun;
        return HttpTable($request, $this->endpoint);
    }

    public function destroy(Request $request, $id)
    {
        return HttpDelete($request, $id, $this->endpoint);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class AuthController extends Controller
{
    public function store(Request $request)
    {
        $response = Http::post(env('API_URL') . 'login', $request->only('username', 'password'));
        if ($response->status() === 200) {
            $data = json_decode($response->body());
            $token = $response->header('Authorization');
            $user = $data->data;

            //save user detail & response token from login to session
            $request->session()->put('user', $user);
            $request->session()->put('token', $token);

            $menuRequest = Http::withToken($request->session()->get('token'))->get(env('API_URL') . 'me/menu');
            $dt_menu = json_decode($menuRequest->body());
            $menu = $dt_menu->data;
            //save menu to session
            $request->session()->put('menu', $menu);

            return response($response->body(), $response->status())->header('Content-Type', 'application/json');
        }
        return response($response->body(), $response->status())->header('Content-Type', 'application/json');
    }
}

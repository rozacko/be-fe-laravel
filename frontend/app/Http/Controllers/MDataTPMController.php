<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class MDataTPMController extends Controller
{
    public function __construct()
    {
        $this->title = 'Data Dashboard TPM';
        $this->view = 'pages.tpmreport';
        $this->route = 'mtpm';
        $this->endpoint = 'tpm/importreport';
        $this->endpointtable = 'tpm/listdashboardtpm';
    }

    public function index()
    {
        $page_title = $this->title;
        $page_description = '';
        $data = [
            'pagetitle' => $page_title,
            'route' => $this->route
        ];
        addVendors(['datatables']);
        return view($this->view . '.index-data-dashboard-tpm', compact('page_title', 'page_description'), $data);
    }

    public function table(Request $request)
    {
        return HttpTable($request, $this->endpointtable);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $data = [
            'pagetitle' => $this->title,
            'route' => $this->route
        ];
        return view($this->view . '.create-data-dashboard-tpm', $data);
    }

    public function DownloadTemplate()
    {
        return Storage::disk('TempExcel')->download('format-data-dashboard-tpm.xlsx');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return HttpPostFile($request, $this->endpoint);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TPMReportLaporanGugusController extends Controller
{
    public function __construct()
    {
        $this->title = 'Laporan Gugus';
        $this->route = 'tpm-report-laporan-gugus';
        $this->endpoint = 'tpm/listLaporangugus';
        $this->endpointfilterFasilitator = 'tpm/filterFasilitator';
        $this->endpointfilterGugus = 'tpm/filterGugus';
        $this->endpointdownloadexcel = 'tpm/exportGugus';
    }

    public function index()
    {
        $data = [
            'title' => $this->title,
            'route' => $this->route
        ];
        addVendors(['amcharts', 'apexcharts']);
        return view('pages.tpmreport.tpm-report-laporan-gugus', $data);
    }

    public function table(Request $request)
    {
        return HttpTable($request, $this->endpoint);
    }

    public function FilterFasilitator(Request $request)
    {
        return HttpTable($request, $this->endpointfilterFasilitator);
    }

    public function FilterGugus(Request $request)
    {
        return HttpTable($request, $this->endpointfilterGugus);
    }

    public function DownloadExcel(Request $request)
    {
        return HttpPostDownloadExcel($request, $this->endpointdownloadexcel);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

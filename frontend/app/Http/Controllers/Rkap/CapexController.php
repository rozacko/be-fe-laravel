<?php

namespace App\Http\Controllers\Rkap;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CapexController extends Controller
{
    //
    public function __construct()
    {
        $this->title = 'Capex';
        $this->route = 'pages.rkap.capex';
        $this->endpoint = 'rkap/rkap-capex';
        $this->endpointdownloadexcel = 'rkap/rkap-capex-download';
    }

    public function index(Request $request)
    {
        # code...
        $page_title = $this->title;
        $page_description = 'Data Rkap Capex';
        $data = [
            'pagetitle' => $page_title,
            'subtitle' => 'manage data ' . strtolower($page_title),
            'urlExport'   => env('API_URL').$this->endpointdownloadexcel,
            // 'urlExport'   => 'http://localhost:8022/'.$this->endpointdownloadexcel,
            'token'         =>$request->session()->get('token'),
            'route' => $this->route,
            'breadcrumb' => ['Data RKAP ' => null, $page_title => "" ],
        ];
        // dd($data);
        addVendors(['datatables']);
        return view($this->route, compact('page_title', 'page_description'), $data); 
    }

    public function table(Request $request)
    {
        return HttpTable($request, $this->endpoint);
    }

    public function getProject(Request $request)
    {
        return HttpTable($request, $this->endpoint.'-project');
    }

    public function show($uuid, Request $request){
        return HttpGet($request, $uuid, $this->endpoint);
    }

    public function update($uuid, Request $request)
    {
        # code...
        // return response()->json($uuid);
        return HttpPut($request,$uuid, $this->endpoint);
    }

    public function import(Request $request)
    {
        # code...        
        return HttpPostFile($request, $this->endpoint.'-import');
    }

    public function downloadTemplate()
    {
        # code...
        return Storage::disk('TempExcel')->download('template-rkap-capex.xlsx');
    }

    public function destroy(Request $request, $uuid)
    {
        return HttpDelete($request, $uuid, $this->endpoint);
    }
}

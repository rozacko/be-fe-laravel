<?php

namespace App\Http\Controllers\Rkap;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PlantPerformanceController extends Controller
{
    //
    public function __construct()
    {
        $this->title = 'Plant Performance';
        $this->route = 'pages.rkap.plant-performance';
        $this->endpoint = 'rkap/rkap-performance';
    }

    public function index(Request $request)
    {
        # code...
        $page_title = $this->title;
        $page_description = 'Data Rkap & Prognose Plant Performance';
        $data = [
            'pagetitle' => $page_title,
            'subtitle' => 'manage data ' . strtolower($page_title),
            'route' => $this->route,
            'urlExport'   => env('API_URL').$this->endpoint.'-download',
            'token'         =>$request->session()->get('token'),
            'breadcrumb' => ['Data RKAP  & Prognose' => null, $page_title => "" ],
        ];
        addVendors(['datatables']);
        return view($this->route, compact('page_title', 'page_description'), $data); 
    }

    public function table(Request $request)
    {
        return HttpTable($request, $this->endpoint);
    }

    public function show($uuid, Request $request){
        return HttpGet($request, $uuid,$this->endpoint);
    }

    public function update($uuid, Request $request)
    {
        # code...
        // return response()->json($uuid);
        return HttpPut($request,$uuid, $this->endpoint);
    }

    public function import(Request $request)
    {
        # code...        
        return HttpPostFile($request, $this->endpoint.'-import');
    }

    public function downloadTemplate()
    {
        # code...
        return Storage::disk('TempExcel')->download('template-rkap-plant-performance.xlsx');
    }
}

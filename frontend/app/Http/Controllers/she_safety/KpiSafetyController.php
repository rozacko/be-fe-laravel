<?php
namespace App\Http\Controllers\she_safety;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Http;
use DataTables;
use Session;
use DB;

class KpiSafetyController extends Controller
{
    public function __construct()
    {
        $this->title = 'KPI Safety Configure';
        $this->route = 'she_safety.kpisafety';
        $this->endpoint = 'kpisafety';
    }

    public function index()
    {
        $page_title = $this->title;
        $page_description = '';
        $data = [
            'pagetitle' => $page_title,
            'subtitle' => 'manage data ' . strtolower($page_title),
            'route' => $this->route,
            'breadcrumb' => ['SHE Safety Performance' => null, $page_title => route($this->route.'.index') ],
            'bulan' => bulan(),
            'tahun' => tahun(),
        ];
        // return $data;
        addVendors(['datatables']);
        return view($this->route . '.index', compact('page_title', 'page_description'), $data);        
    }
    
    public function show(Request $request, $tahun)
    {
        return HttpGet($request, $tahun, $this->endpoint);
    }

    public function store(Request $request)
    {      
        return HttpPost($request, $this->endpoint);
    }
}

<?php
namespace App\Http\Controllers\she_safety;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Http;
use DataTables;
use Session;
use DB;

class HydrantController extends Controller
{
    public function __construct()
    {
        $this->title = 'Hydrant';
        $this->route = 'she_safety.hydrant';
        $this->endpoint = 'hydrant';        
        $this->endpoint2 = 'hydrant_file';
    }

    public function index()
    {
        $page_title = $this->title;
        $page_description = '';
        $data = [
            'pagetitle' => $page_title,
            'subtitle' => 'manage data ' . strtolower($page_title),
            'route' => $this->route,
            'breadcrumb' => ['SHE Safety Performance' => null, $page_title => route($this->route.'.index') ],
            'bulan' => bulan(),
            'tahun' => tahun(),
        ];
        addVendors(['datatables']);
        return view($this->route . '.index', compact('page_title', 'page_description'), $data);        
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {         
        $kondisi_code = ['V','K','(-)','I','X'];
        $kondisi_desc = ['BAIK','KOTOR','TIDAK ADA','SERET/MACET/MEREMBES','RUSAK'];
        foreach ($kondisi_code as $key => $value) {
            $kondisi[$key]['id'] = $value;
            $kondisi[$key]['desc'] = $kondisi_desc[$key];
        }
        $data = [
            'pagetitle' => $this->title,
            'subtitle' => 'manage data ' . strtolower($this->title),
            'route' => $this->route,
            'kondisi' => $kondisi,
            'bulan' => bulan(),
            'tahun' => tahun(),
        ];
        return view($this->route . '.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return HttpPost($request, $this->endpoint);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $kondisi_code = ['V','K','(-)','I','X'];
        $kondisi_desc = ['BAIK','KOTOR','TIDAK ADA','SERET/MACET/MEREMBES','RUSAK'];
        foreach ($kondisi_code as $key => $value) {
            $kondisi[$key]['id'] = $value;
            $kondisi[$key]['desc'] = $kondisi_desc[$key];
        }
        $param = [
            'pagetitle' => $this->title,
            'subtitle' => 'manage data ',
            'route' => $this->route,
            'data' => HttpGet($request, $id, $this->endpoint),            
            'kondisi' => $kondisi,
            'bulan' => bulan(),
            'tahun' => tahun(),
        ];
        return view($this->route . '.edit', $param);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return HttpPut($request, $id, $this->endpoint);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function table(Request $request, $bulan, $tahun)
    {
        $request['bulan'] = $bulan;
        $request['tahun'] = $tahun;
        return HttpTable($request, $this->endpoint);
    }

    public function destroy(Request $request, $id)
    {        
        return HttpDelete($request, $id, $this->endpoint);
    }

    public function dokumentasi(Request $request, $id)
    {        
        $param = [
            'pagetitle' => $this->title,
            'subtitle' => 'manage data ',
            'route' => $this->route,
            'id' => $id,
        ];        
        return view($this->route . '.dokumentasi', $param);
    }

    public function upload_file(Request $request)
    {
        return HttpPostFile($request, 'file_'.$this->endpoint);
    }

    public function store_doc(Request $request)
    {         
        return HttpPost($request, $this->endpoint2);
    }

    public function table_doc(Request $request, $id)
    {        
        $request['id'] = $id;
        return HttpTable($request, $this->endpoint2);
    }    

    public function destroy_doc(Request $request, $id)
    {
        return HttpDelete($request, $id, $this->endpoint2);
    }

    public function mdl_import(Request $request)
    {        
        $data = [
            'pagetitle' => $this->title,
            'subtitle' => 'manage data ' . strtolower($this->title),
            'route' => $this->route,
            'bulan' => bulan(),
            'tahun' => tahun(),
        ];

        return view($this->route . '.import', $data);
    }

    public function template(Request $request)
    {
        return HttpGetFile($request, $this->endpoint.'_template');
    }

    public function import(Request $request)
    {      
        return HttpPostFile($request, $this->endpoint.'_import');
    }
}

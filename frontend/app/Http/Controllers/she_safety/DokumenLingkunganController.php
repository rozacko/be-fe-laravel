<?php
namespace App\Http\Controllers\she_safety;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Http;
use DataTables;
use Session;
use DB;

class DokumenLingkunganController extends Controller
{
    public function __construct()
    {
        $this->title = 'Dokumen Lingkungan';
        $this->route = 'she_safety.dokumen-lingkungan';
        $this->endpoint = 'dokumen-lingkungan';
        $this->endpointTable = 'dokumen-lingkungan/list';
        $this->route_master = 'masterdata.marea';
        $this->endpointMasterArea = 'dokumen-lingkungan/ddl/area';
        $this->endpointUpload = 'dokumen-lingkungan/upload';
        $this->endpointMasterJenisDokumen = 'dokumen-lingkungan/ddl/jenisdokumen';
    }

    public function index(Request $request)
    {
        $getArea = $this->getArea($request);
        $area = json_decode($getArea->original);

        $getJenisDokumen = $this->getJenisDokumen($request);
        $jenisDokumen = json_decode($getJenisDokumen->original);

        $page_title = $this->title;
        $page_description = '';
        $data = [
            'pagetitle' => $page_title,
            'subtitle' => 'manage data ' . strtolower($page_title),
            'route' => $this->route,
            'route_master' => $this->route_master,
            'breadcrumb' => ['SHE Safety Performance / Enviro Digital Data' => null, $page_title => route($this->route.'.index') ],
            'bulan' => bulan(),
            'tahun' => tahun(),
            'area' => $area->data,
            'jenisDokumen' => $jenisDokumen->data
        ];
        addVendors(['datatables']);
        return view($this->route . '.index', compact('page_title', 'page_description'), $data);        
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {        
        $getArea = $this->getArea($request);
        $area = json_decode($getArea->original);

        $getJenisDokumen = $this->getJenisDokumen($request);
        $jenisDokumen = json_decode($getJenisDokumen->original);

        $data = [
            'pagetitle' => $this->title,
            'subtitle' => 'manage data ' . strtolower($this->title),
            'route' => $this->route,
            'route_master' => $this->route_master,
            'bulan' => bulan(),
            'tahun' => tahun(),
            'area' => $area->data,
            'jenisDokumen' => $jenisDokumen->data,
        ];

        return view($this->route . '.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {         
        return HttpPost($request, $this->endpoint.'/store');
    }

    public function uploadFile(Request $request)
    {                 
        return HttpPostFile($request, $this->endpointUpload);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $uuid)
    {        
        $getArea = $this->getArea($request);
        $area = json_decode($getArea->original);

        $getJenisDokumen = $this->getJenisDokumen($request);
        $jenisDokumen = json_decode($getJenisDokumen->original);

        $param = [
            'pagetitle' => $this->title,
            'subtitle' => 'manage data ',
            'route' => $this->route,
            'data' => HttpGetByid($request, $uuid, $this->endpoint.'/edit'),
            'bulan' => bulan(),
            'tahun' => tahun(),
            'area' => $area->data,
            'jenisDokumen' => $jenisDokumen->data,
        ];        
        return view($this->route . '.edit', $param);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $uuid)
    {
        return HttpPut($request, $uuid, $this->endpoint.'/update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     public function getArea($request){
        return HttpTable($request, $this->endpointMasterArea);
     }

     public function getJenisDokumen($request){
        return HttpTable($request, $this->endpointMasterJenisDokumen);
     }

     public function table(Request $request, $area, $tahun)
     {
        $request['area'] = $area;
        $request['tahun'] = $tahun;
         return HttpTable($request, $this->endpointTable);

     }

    public function destroy(Request $request, $uuid)
    {
        return HttpDelete($request, $uuid, $this->endpoint.'/delete');
    }

    public function mdl_import(Request $request)
    {        
        $data = [
            'pagetitle' => $this->title,
            'subtitle' => 'manage data ' . strtolower($this->title),
            'route' => $this->route,
            'bulan' => bulan(),
            'tahun' => tahun(),
        ];

        return view($this->route . '.import', $data);
    }

    public function template(Request $request)
    {
        return HttpGetFile($request, $this->endpoint.'_template');
    }

    public function import(Request $request)
    {      
        return HttpPostFile($request, $this->endpoint.'_import');
    }
}

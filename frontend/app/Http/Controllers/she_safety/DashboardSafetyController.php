<?php
namespace App\Http\Controllers\she_safety;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Http;
use DataTables;
use Session;
use DB;

class DashboardSafetyController extends Controller
{
    public function __construct()
    {
        $this->title = 'GHOPO Safety Report';
        $this->route = 'she_safety.dashboardsafety';
        $this->endpoint = '_kpi';
    }

    public function index()
    {
        $page_title = $this->title;
        $page_description = '';
        $data = [
            'pagetitle' => $page_title,
            'subtitle' => 'manage data ' . strtolower($page_title),
            'route' => $this->route,
            'breadcrumb' => ['SHE Safety Performance' => null, $page_title => route($this->route.'.index') ],
            'bulan' => bulan(),
            'tahun' => tahun(),
        ];
        // return $data;
        addVendors(['datatables']);
        return view($this->route . '.index', compact('page_title', 'page_description'), $data);        
    }
    
    public function dash_kpi(Request $request, $bulan, $tahun)
    {      
        $request['bulan'] = $bulan;
        $request['tahun'] = $tahun;
        return HttpTable($request, 'dashboard_kpi');
    }

    public function dash_ltifr_ltisr(Request $request, $bulan, $tahun, $status)
    {      
        $request['bulan'] = $bulan;
        $request['tahun'] = $tahun;
        $request['status'] = $status;
        return HttpTable($request, 'dashboard_ltifr_ltisr');
    }

    public function dash_man_power(Request $request, $bulan, $tahun)
    {      
        $request['bulan'] = $bulan;
        $request['tahun'] = $tahun;
        return HttpTable($request, 'dashboard_man_power');
    }

    public function dash_man_hours(Request $request, $bulan, $tahun)
    {      
        $request['bulan'] = $bulan;
        $request['tahun'] = $tahun;
        return HttpTable($request, 'dashboard_man_hours');
    }

    public function dash_unsafe_action(Request $request, $bulan, $tahun)
    {      
        $request['bulan'] = $bulan;
        $request['tahun'] = $tahun;
        return HttpTable($request, 'dashboard_unsafe_action');
    }

    public function dash_unsafe_condition(Request $request, $bulan, $tahun)
    {      
        $request['bulan'] = $bulan;
        $request['tahun'] = $tahun;
        return HttpTable($request, 'dashboard_unsafe_condition');
    }

    public function dash_accident(Request $request, $bulan, $tahun)
    {      
        $request['bulan'] = $bulan;
        $request['tahun'] = $tahun;
        return HttpTable($request, 'dashboard_accident');
    }

    public function dash_accident_fire(Request $request, $bulan, $tahun)
    {      
        $request['bulan'] = $bulan;
        $request['tahun'] = $tahun;
        return HttpTable($request, 'dashboard_accident_fire');
    }

    public function dash_safety_training(Request $request, $bulan, $tahun)
    {      
        $request['bulan'] = $bulan;
        $request['tahun'] = $tahun;
        return HttpTable($request, 'dashboard_safety_training');
    }
}

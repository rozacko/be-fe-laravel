<?php
namespace App\Http\Controllers\she_safety;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Http;
use DataTables;
use Session;
use DB;

class FireAlarmController extends Controller
{
    public function __construct()
    {
        $this->title = 'Fire Alarm';
        $this->route = 'she_safety.firealarm';
        $this->endpoint = 'firealarm';
        $this->endpoint2 = 'mfirealarm';
        $this->endpoint3 = 'firealarmfile';
    }

    public function index()
    {
        $page_title = $this->title;
        $page_description = '';
        $data = [
            'pagetitle' => $page_title,
            'subtitle' => 'manage data ' . strtolower($page_title),
            'route' => $this->route,
            'breadcrumb' => ['SHE Safety Performance' => null, $page_title => route($this->route.'.index') ],
            'bulan' => bulan(),
            'tahun' => tahun(),
        ];
        addVendors(['datatables']);
        return view($this->route . '.index', compact('page_title', 'page_description'), $data);        
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {        
        $master = HttpGet($request, $id = '', $this->endpoint2);
        $data = [
            'pagetitle' => $this->title,
            'subtitle' => 'manage data ' . strtolower($this->title),
            'route' => $this->route,
            'master' => $master,
            'bulan' => bulan(),
            'tahun' => tahun(),
        ];
        return view($this->route . '.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return HttpPost($request, $this->endpoint);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        
        $getdata = HttpGet($request, $id, $this->endpoint);
        $data = [];
        foreach ($getdata as $key => $value) {
            $data['parameter_id'.$value->parameter_id]['ccr'.$value->ccr_tuban] = $value;
        }            
        $data_id = $id;           
        $param = [
            'pagetitle' => $this->title,
            'subtitle' => 'manage data ',
            'route' => $this->route,
            'data' => $data,
            'master' => HttpGet($request, $id = '', $this->endpoint2),
            'id' => $data_id,
            'bulan_dt' => substr($data_id,-2),
            'tahun_dt' => substr($data_id,0,4),
            'bulan' => bulan(),
            'tahun' => tahun(),
        ];
        return view($this->route . '.edit', $param);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return HttpPut($request, $id, $this->endpoint);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function table(Request $request, $bulan, $tahun)
    {
        $request['bulan'] = $bulan;
        $request['tahun'] = $tahun;
        return HttpTable($request, $this->endpoint);
    }

    public function destroy(Request $request, $id)
    {
        return HttpDelete($request, $id, $this->endpoint);
    }

    public function upload_file(Request $request)
    {                 
        return HttpPostFile($request, 'file_'.$this->endpoint3);
    }

    public function dokumentasi(Request $request, $id)
    {        
        $param = [
            'pagetitle' => $this->title,
            'subtitle' => 'manage data ',
            'route' => $this->route,
            'id' => $id,
        ];        
        return view($this->route . '.dokumentasi', $param);
    }

    public function store_doc(Request $request)
    {         
        return HttpPost($request, $this->endpoint3);
    }

    public function table_doc(Request $request, $id)
    {        
        $request['id'] = $id;        
        return HttpTable($request, $this->endpoint3, $id);
    }    

    public function destroy_doc(Request $request, $id)
    {
        return HttpDelete($request, $id, $this->endpoint3);
    }

    public function mdl_import(Request $request)
    {        
        $data = [
            'pagetitle' => $this->title,
            'subtitle' => 'manage data ' . strtolower($this->title),
            'route' => $this->route,
            'bulan' => bulan(),
            'tahun' => tahun(),
        ];

        return view($this->route . '.import', $data);
    }

    public function template(Request $request)
    {
        return HttpGetFile($request, $this->endpoint.'_template');
    }

    public function import(Request $request)
    {      
        return HttpPostFile($request, $this->endpoint.'_import');
    }
}

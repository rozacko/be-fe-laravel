<?php
namespace App\Http\Controllers\she_safety;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Http;
use DataTables;
use Session;
use DB;

class MasterInspeskiLingkunganController extends Controller
{
    public function __construct()
    {
        $this->title = 'Inspeksi Lingkungan';
        $this->route = 'she_safety.master-inspeksi-lingkungan';
        $this->endpoint = 'master-inspeksi-lingkungan';
        $this->endpointTable = 'master-inspeksi-lingkungan/list';
        $this->route_master = 'masterdata.marea';
    }

    public function index()
    {
        $page_title = $this->title;
        $page_description = '';
        $data = [
            'pagetitle' => $page_title,
            'subtitle' => 'manage data ' . strtolower($page_title),
            'route' => $this->route,
            'route_master' => $this->route_master,
            'breadcrumb' => ['SHE Safety Performance / Enviro Digital Data' => null, $page_title => route($this->route.'.index') ],
            'bulan' => bulan(),
            'tahun' => tahun(),
        ];
        addVendors(['datatables']);
        return view($this->route . '.index', compact('page_title', 'page_description'), $data);        
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {        
        $data = [
            'pagetitle' => $this->title,
            'subtitle' => 'manage data ' . strtolower($this->title),
            'route' => $this->route,
            'route_master' => $this->route_master,
            'bulan' => bulan(),
            'tahun' => tahun(),
        ];

        return view($this->route . '.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {         
        return HttpPost($request, $this->endpoint);
    }

    public function upload_file(Request $request)
    {                 
        return HttpPostFile($request, 'file_'.$this->endpoint);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {        
        $param = [
            'pagetitle' => $this->title,
            'subtitle' => 'manage data ',
            'route' => $this->route,
            'data' => HttpGet($request, $id, $this->endpoint),
            'bulan' => bulan(),
            'tahun' => tahun(),
        ];        
        return view($this->route . '.edit', $param);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return HttpPut($request, $id, $this->endpoint);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     public function table(Request $request, $plant)
     {
         $request['plant'] = $plant;
        //  return HttpTable($request, $this->endpointTable);

        $json['data'] = [
            [
                "id" => "1",
                "uuid" => "xxx-xxxx-xxx1",
                "id_area" => "area_id",
                "nm_area" => "nm_area",
                "equipment" => "equipment",
                "uraian_temuan" => "uraian_temuan",
                "tanggal" => "tanggal",
                "foto_temuan" => "foto_temuan",
                "foto_closing" => "foto_closing",
                "devisi_area" => "devisi_area",
                "unit_kerja" => "unit_kerja",
                "evaluasi" => "evaluasi",
                "status" => "status"
            ]
        ];

        return $json;
     }

    public function destroy(Request $request, $id)
    {
        return HttpDelete($request, $id, $this->endpoint);
    }

    public function mdl_import(Request $request)
    {        
        $data = [
            'pagetitle' => $this->title,
            'subtitle' => 'manage data ' . strtolower($this->title),
            'route' => $this->route,
            'bulan' => bulan(),
            'tahun' => tahun(),
        ];

        return view($this->route . '.import', $data);
    }

    public function template(Request $request)
    {
        return HttpGetFile($request, $this->endpoint.'_template');
    }

    public function import(Request $request)
    {      
        return HttpPostFile($request, $this->endpoint.'_import');
    }
}

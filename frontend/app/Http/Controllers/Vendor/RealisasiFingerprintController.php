<?php

namespace App\Http\Controllers\Vendor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class RealisasiFingerprintController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
     public function __construct()
     {
        $this->title = 'Data Realisasi Finger Print';
        $this->route = 'realisasi-fingerprint';
        $this->endpoint = 'realisasi-fingerprint';
        $this->endpointdownloadexcel = 'realisasi-fingerprint/download-excel';
        $this->endpointdownloadTemplate = 'vendormanagement/template';
        $this->endpointImportexcel = 'upload-realisasi-fingerprint';
     }

    public function index()
    {
        $data = [
            'title' => $this->title,
            'route' => $this->route,
            'urlDownload' => env('API_URL').$this->endpointdownloadexcel,
            'token'=> \Session::get('token')
        ];
        addVendors(['datatables']);
        return view('pages.vendor.realisasi-fingerprint.index', $data);
    }

    public function table(Request $request, $bulan, $tahun)
    {
        $request['tahun'] = $tahun;
        $request['bulan'] = $bulan;

        return HttpTable($request, $this->endpoint);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function create()
    {
        $data = [
            'pagetitle' => $this->title,
            'route' => $this->route
        ];
        return view('pages.vendor.realisasi-fingerprint.upload', $data);
    }

    public function import(Request $request)
    {      
        return HttpPostFileVendor($request, $this->endpointImportexcel);
    }

    public function show($uuid, Request $request){
        return HttpGet($request, $uuid, $this->endpoint);
    }

    public function DownloadTemplate(Request $request, $type)
    {
        return HttpGetFile($request, $this->endpointdownloadTemplate.'/'.$type);
    }
}

<?php

namespace App\Http\Controllers\Vendor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class PlanningRealisasiFingerprintController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
     public function __construct()
     {
        $this->title = 'Data Planning vs Realisasi Absensi Finger Print';
        $this->route = 'planning-realisasi-fingerprint';
        $this->endpoint = 'planning-realisasi-fingerprint';
        $this->endpointdownloadexcel = 'planning-realisasi-fingerprint/download-excel';
     }

    public function index()
    {
        $data = [
            'title' => $this->title,
            'route' => $this->route,
            'urlDownload' => env('API_URL').$this->endpointdownloadexcel,
            'token'=> \Session::get('token')
        ];
        addVendors(['datatables']);
        return view('pages.vendor.planning-realisasi-fingerprint.index', $data);
    }

    public function table(Request $request, $bulan, $tahun)
    {
        $request['tahun'] = $tahun;
        $request['bulan'] = $bulan;

        return HttpTable($request, $this->endpoint);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function DownloadTemplate()
    {
        return Storage::disk('TempExcel')->download('template_kpi_member.xlsx');
    }
}

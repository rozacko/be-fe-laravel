<?php

namespace App\Http\Controllers\Vendor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class UploadScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
     public function __construct()
     {
        $this->title = 'Upload Schedule';
        $this->route = 'upload-schedule';
        $this->endpointdownloadTemplate = 'vendormanagement/template';
        $this->downloadExcel = 'upload-schedule/download-excel';
     }

    public function index()
    {

        $data = [
            'title' => $this->title,
            'route' => $this->route,
            'urlDownload' => env('API_URL').$this->downloadExcel,
            'token'=> \Session::get('token')
        ];
        addVendors(['datatables']);

        return view('pages.vendor.upload-schedule.index', $data);
    }

    public function table(Request $request)
    {
        
        return HttpTable($request, $this->route.'/list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'pagetitle' => $this->title,
            'route' => $this->route
        ];
        return view('pages.vendor.upload-schedule.upload-schedule', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     public function import(Request $request)
     {      
         return HttpPostFileVendor($request, $this->route);
     }
    
    public function DownloadTemplate(Request $request, $type)
    {
        return HttpGetFile($request, $this->endpointdownloadTemplate.'/'.$type);
        // return Storage::disk('TempExcel')->download('template_kpi_member.xlsx');
    }
}

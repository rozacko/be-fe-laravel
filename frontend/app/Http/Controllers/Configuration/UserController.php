<?php

namespace App\Http\Controllers\Configuration;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct()
    {
        $this->title = 'User Management';
        $this->route = 'configuration.user';
        $this->endpoint = 'user';
    }

    public function index()
    {
        $pageTitle = $this->title;
        $data = [
            'pageTitle' => $pageTitle,
            'route' => $this->route,
            'breadcrumb' => ['Master Data' => null, $pageTitle => route($this->route . '.index')],
        ];
        addVendors(['datatables']);
        return view("pages.$this->route.index", compact('pageTitle'), $data);
    }

    public function create(Request $request)
    {
        $roles = HttpGet($request, '', 'role');
        $data = [
            'pageTitle' => $this->title,
            'route' => $this->route,
            'roles' => $roles,
        ];

        return view("pages.$this->route.create", $data);
    }

    public function store(Request $request)
    {
        return HttpPost($request, $this->endpoint);
    }

    public function edit(Request $request, $id)
    {
        $user = HttpGet($request, $id, $this->endpoint);
        $roles = HttpGet($request, '', 'role');
        if (count($user->roles)) {
            $user->roles = collect($user->roles)->pluck('id')->all();
        }
        $param = [
            'pageTitle' => $this->title,
            'route' => $this->route,
            'data' => $user,
            'roles' => $roles,
        ];

        return view("pages.$this->route.edit", $param);
    }

    public function update(Request $request, $id)
    {
        return HttpPut($request, $id, $this->endpoint);
    }

    public function table(Request $request)
    {
        return HttpTable($request, $this->endpoint);
    }

    public function destroy(Request $request, $uuid)
    {
        return HttpDelete($request, $uuid, $this->endpoint);
    }
}

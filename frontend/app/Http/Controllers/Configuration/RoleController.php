<?php

namespace App\Http\Controllers\Configuration;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->title = 'Role Management';
        $this->route = 'configuration.role';
        $this->endpoint = 'role';
    }

    public function index()
    {
        $pageTitle = $this->title;
        $data = [
            'pageTitle' => $pageTitle,
            'route' => $this->route,
            'breadcrumb' => ['Master Data' => null, $pageTitle => route($this->route . '.index')],
        ];
        addVendors(['datatables']);
        return view("pages.$this->route.index", compact('pageTitle'), $data);
    }

    public function create(Request $request)
    {
        $data = [
            'pageTitle' => $this->title,
            'route' => $this->route
        ];

        return view("pages.$this->route.create", $data);
    }

    public function store(Request $request)
    {
        return HttpPost($request, $this->endpoint);
    }

    public function edit(Request $request, $id)
    {
        $role = HttpGet($request, $id, $this->endpoint);
        $param = [
            'pageTitle' => $this->title,
            'route' => $this->route,
            'data' => $role
        ];

        return view("pages.$this->route.edit", $param);
    }

    public function detail(Request $request, $id)
    {
        $role = HttpGet($request, $id, $this->endpoint);
        $param = [
            'pageTitle' => $this->title,
            'route' => $this->route,
            'data' => $role,
            'uuid' => $id
        ];
        return view("pages.$this->route.detail", $param);
    }

    public function user(Request $request, $uuid)
    {
        $param = [
            'pageTitle' => $this->title,
            'route' => $this->route,
            'uuid' => $uuid,
        ];

        return view("pages.$this->route.user", $param);
    }

    public function permission(Request $request, $uuid)
    {
        $permission = HttpGet($request, $uuid . "/permission", $this->endpoint);
        $param = [
            'pageTitle' => $this->title,
            'route' => $this->route,
            'data' => $permission,
            'uuid' => $uuid,
        ];

        return view("pages.$this->route.permission", $param);
    }

    public function addUser(Request $request)
    {
        return HttpPost($request, $this->endpoint . "/" . $request->post('uuid') . "/user");
    }

    public function destroyUser(Request $request)
    {
        return HttpDelete($request, $request->post('uuid') . "/user", $this->endpoint);
    }

    public function addPermission(Request $request)
    {
        return HttpPost($request, $this->endpoint . "/" . $request->post('uuid') . "/permission");
    }

    public function update(Request $request, $id)
    {
        return HttpPut($request, $id, $this->endpoint);
    }

    public function table(Request $request)
    {
        return HttpTable($request, $this->endpoint);
    }

    public function destroy(Request $request, $uuid)
    {
        return HttpDelete($request, $uuid, $this->endpoint);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PlantPerformanceController extends Controller
{
    //

    public function index(Request $request)
    {
        # code...
        addVendors(['amcharts', 'apexcharts']);
        return view('pages.maintenance.plant-performance');
    }
}

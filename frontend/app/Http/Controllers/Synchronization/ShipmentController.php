<?php

namespace App\Http\Controllers\Synchronization;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ShipmentController extends Controller
{
    //
    public function index()
    {
        addVendors(['datatables']);

        return view('pages.shipment.index');
    }

    public function datatable(Request $request){
        return HttpTable($request, 'shipment');
    }

    public function syncData(Request $request){
        return HttpPost($request, 'shipment/sync');
    }
}

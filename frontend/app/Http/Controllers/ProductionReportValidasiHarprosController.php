<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductionReportValidasiHarprosController extends Controller
{
    public function __construct()
    {
        $this->title = 'Production Report';
        $this->view = 'pages.productionreport';
        $this->route = 'production-report-validasi-harpros';
    }

    public function index()
    {
        $page_title = $this->title;
        $page_description = '';
        $data = [
            'pagetitle' => $page_title,
            'route' => $this->route
        ];
        addVendors(['datatables']);
        return view($this->view . '.production-report-validasi-harpros', compact('page_title', 'page_description'), $data);
    }

}

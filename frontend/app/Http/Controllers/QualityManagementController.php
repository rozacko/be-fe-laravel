<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class QualityManagementController extends Controller
{
    public function __construct()
    {
        $this->view = 'pages.qualitymanagement';
        $this->route = 'quality-management';
        $this->endpointstoreRM = 'qc/raw-meal-import';
        $this->endpointstoreKF = 'qc/kiln-feed-import';
        $this->endpointstoreCK = 'qc/clinker-import';
        $this->endpointstoreSE = 'qc/quality-cement-import';
        $this->endpointstoreCM = 'qc/coal-import';
        $this->endpointstoreHM = 'qc/hot-meal-import';
        $this->endpointdataRM = 'qc/raw-meal';
        $this->endpointdataKF = 'qc/kiln-feed';
        $this->endpointdataCK = 'qc/clinker';
        $this->endpointdataSE = 'qc/quality-cement';
        $this->endpointdataCM = 'qc/coal';
        $this->endpointdataHM = 'qc/hot-meal';
        $this->endpointexportRM = 'qc/raw-meal-download';
        $this->endpointexportKF = 'qc/kiln-feed-download';
        $this->endpointexportCK = 'qc/clinker-download';
        $this->endpointexportSE = 'qc/quality-cement-download';
        $this->endpointexportCM = 'qc/coal-download';
        $this->endpointexportHM = 'qc/hot-meal-download';
    }

    public function index(Request $request)
    {
        $data = [
            'route'=> $this->route,
            'urlExportRM'=> env('API_URL').$this->endpointexportRM,
            'urlExportKF'=> env('API_URL').$this->endpointexportKF,
            'urlExportCK'=> env('API_URL').$this->endpointexportCK,
            'urlExportSE'=> env('API_URL').$this->endpointexportSE,
            'urlExportCM'=> env('API_URL').$this->endpointexportCM,
            'urlExportHM'=> env('API_URL').$this->endpointexportHM,
            'token'=>$request->session()->get('token')
        ];
        return view($this->view . '.index', $data);
    }
    
    public function importRM()
    {
        $data = [
            'route'=> $this->route,
        ];
        return view($this->view . '.create-raw-mill ', $data);
    }

    public function importKF()
    {
        $data = [
            'route'=> $this->route,
        ];
        return view($this->view . '.create-kiln-feed ', $data);
    }

    public function importCK()
    {
        $data = [
            'route'=> $this->route,
        ];
        return view($this->view . '.create-clinker ', $data);
    }

    public function importSE()
    {
        $data = [
            'route'=> $this->route,
        ];
        return view($this->view . '.create-semen ', $data);
    }

    public function importCM()
    {
        $data = [
            'route'=> $this->route,
        ];
        return view($this->view . '.create-coal-mill ', $data);
    }

    public function importHM()
    {
        $data = [
            'route'=> $this->route,
        ];
        return view($this->view . '.create-hot-mill ', $data);
    }

    public function tempRM()
    {
        return Storage::disk('TempExcel')->download('template-qm-raw-mill.xlsx');
    }

    public function tempKF()
    {
        return Storage::disk('TempExcel')->download('template-qm-kiln-feed.xlsx');
    }

    public function tempCK()
    {
        return Storage::disk('TempExcel')->download('template-qm-clinker.xlsx');
    }

    public function tempSE()
    {
        return Storage::disk('TempExcel')->download('template-qm-semen.xlsx');
    }

    public function tempCM()
    {
        return Storage::disk('TempExcel')->download('template-qm-coal-mill.xlsx');
    }

    public function tempHM()
    {
        return Storage::disk('TempExcel')->download('template-qm-hot-mill.xlsx');
    }

    public function storeRM(Request $request)
    {
        return HttpPostFile($request, $this->endpointstoreRM);
    }

    public function storeKF(Request $request)
    {
        return HttpPostFile($request, $this->endpointstoreKF);
    }

    public function storeCK(Request $request)
    {
        return HttpPostFile($request, $this->endpointstoreCK);
    }

    public function storeSE(Request $request)
    {
        return HttpPostFile($request, $this->endpointstoreSE);
    }

    public function storeCM(Request $request)
    {
        return HttpPostFile($request, $this->endpointstoreCM);
    }

    public function storeHM(Request $request)
    {
        return HttpPostFile($request, $this->endpointstoreHM);
    }

    public function datatableRM(Request $request)
    {
        return HttpTable($request, $this->endpointdataRM);
    }

    public function datatableKF(Request $request)
    {
        return HttpTable($request, $this->endpointdataKF);
    }

    public function datatableCK(Request $request)
    {
        return HttpTable($request, $this->endpointdataCK);
    }

    public function datatableSE(Request $request)
    {
        return HttpTable($request, $this->endpointdataSE);
    }

    public function datatableCM(Request $request)
    {
        return HttpTable($request, $this->endpointdataCM);
    }

    public function datatableHM(Request $request)
    {
        return HttpTable($request, $this->endpointdataHM);
    }

    public function editRM(Request $request, $id)
    {
        $rawMill = HttpGet($request, $id, $this->endpointdataRM);
        $data = [
            'route'=> $this->route,
            'data' => $rawMill,
        ];

        return view($this->view . ".edit-raw-mill", $data);
    }

    public function editKF(Request $request, $id)
    {
        $kilnFeed = HttpGet($request, $id, $this->endpointdataKF);
        $data = [
            'route'=> $this->route,
            'data' => $kilnFeed,
        ];

        return view($this->view . ".edit-kiln-feed", $data);
    }

    public function editCK(Request $request, $id)
    {
        $clinker = HttpGet($request, $id, $this->endpointdataCK);
        $data = [
            'route'=> $this->route,
            'data' => $clinker,
        ];

        return view($this->view . ".edit-clinker", $data);
    }

    public function editSE(Request $request, $id)
    {
        $semen = HttpGet($request, $id, $this->endpointdataSE);
        $data = [
            'route'=> $this->route,
            'data' => $semen,
        ];

        return view($this->view . ".edit-semen", $data);
    }

    public function editCM(Request $request, $id)
    {
        $coalMill = HttpGet($request, $id, $this->endpointdataCM);
        $data = [
            'route'=> $this->route,
            'data' => $coalMill,
        ];

        return view($this->view . ".edit-coal-mill", $data);
    }

    public function editHM(Request $request, $id)
    {
        $hotMill = HttpGet($request, $id, $this->endpointdataHM);
        $data = [
            'route'=> $this->route,
            'data' => $hotMill,
        ];

        return view($this->view . ".edit-hot-mill", $data);
    }

    public function updateRM(Request $request, $id)
    {
        return HttpPut($request, $id, $this->endpointdataRM);
    }

    public function updateKF(Request $request, $id)
    {
        return HttpPut($request, $id, $this->endpointdataKF);
    }

    public function updateCK(Request $request, $id)
    {
        return HttpPut($request, $id, $this->endpointdataCK);
    }

    public function updateSE(Request $request, $id)
    {
        return HttpPut($request, $id, $this->endpointdataSE);
    }

    public function updateCM(Request $request, $id)
    {
        return HttpPut($request, $id, $this->endpointdataCM);
    }

    public function updateHM(Request $request, $id)
    {
        return HttpPut($request, $id, $this->endpointdataHM);
    }

    public function deleteRM(Request $request, $id)
    {
        return HttpDelete($request, $id, $this->endpointdataRM);
    }

    public function deleteKF(Request $request, $id)
    {
        return HttpDelete($request, $id, $this->endpointdataKF);
    }

    public function deleteCK(Request $request, $id)
    {
        return HttpDelete($request, $id, $this->endpointdataCK);
    }

    public function deleteSE(Request $request, $id)
    {
        return HttpDelete($request, $id, $this->endpointdataSE);
    }

    public function deleteCM(Request $request, $id)
    {
        return HttpDelete($request, $id, $this->endpointdataCM);
    }

    public function deleteHM(Request $request, $id)
    {
        return HttpDelete($request, $id, $this->endpointdataHM);
    }
}
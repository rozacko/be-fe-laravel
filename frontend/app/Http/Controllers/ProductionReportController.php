<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductionReportController extends Controller
{
    public function __construct()
    {
        $this->title = 'Production Report';
        $this->view = 'pages.productionreport';
        $this->route = 'production-report';
    }

    public function index()
    {
        $page_title = $this->title;
        $page_description = '';
        $data = [
            'pagetitle' => $page_title,
            'route' => $this->route
        ];
        addVendors(['amcharts', 'apexcharts', 'datatables']);
        return view($this->view . '.dashboard-production-report', compact('page_title', 'page_description'), $data);
    }


}

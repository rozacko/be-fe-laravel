<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PlantReabilityController extends Controller
{
    public function __construct()
    {
        $this->view = 'pages.reliability';
        $this->route = 'plant-reliability';
        $this->endpointSummary= 'plant_reability/get-summary';
        $this->endpointPie= 'plant_reability/get-pie-summary';
    }

    public function index()
    {
        addVendors(['amcharts', 'apexcharts']);
        $data = [
            'route'=> $this->route,
        ];
        return view($this->view . '.index', $data);
    }

    public function summary(Request $request)
    {
        return HttpTable($request, $this->endpointSummary);
    }

    public function pie(Request $request)
    {
        return HttpTable($request, $this->endpointPie);
    }
}

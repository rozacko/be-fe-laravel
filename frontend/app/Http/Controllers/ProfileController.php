<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->title = 'Profile Ghopo';
    }
    public function index(Request $request)
    {
        $dataUser =  $request->session()->get('user');
        $data = [
            'pagetitle' => $this->title,
            'biodata' => $dataUser
        ];
        return view('pages.profile.profile', $data);
    }
}

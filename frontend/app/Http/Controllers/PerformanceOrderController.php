<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PerformanceOrderController extends Controller
{
    public function __construct()
    {
        $this->title = 'Performance Order';
        $this->route = 'performance-order';
        $this->view = 'pages.performance.order';
        $this->endpoint = 'data-koreksi/order-task';
        $this->endpointImport = 'data-koreksi/order-task-import';
    }

    public function table(Request $request)
    {
        return HttpTable($request, $this->endpoint);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page_title = $this->title;
        $page_description = '';
        $data = [
            'pagetitle'     => $page_title,
            'token'         =>$request->session()->get('token'),
            'route'         => $this->route
        ];
        addVendors(['datatables']);
        return view($this->view . '.index', compact('page_title', 'page_description'), $data);
    }

    public function DownloadTemplate()
    {
        return Storage::disk('TempExcel')->download('template-import-order-task.xlsx');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'pagetitle' => $this->title,
            'route' => $this->route
        ];
        return view($this->view . '.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return HttpPostFile($request, $this->endpointImport);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $dataRKAP = HttpGet($request, $id, $this->endpoint);
        $data = [
            'route'=> $this->route,
            'data' => $dataRKAP,
        ];

        return view($this->view . ".detail", $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        return HttpDelete($request, $id, $this->endpoint);
    }
}

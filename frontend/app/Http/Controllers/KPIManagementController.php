<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class KPIManagementController extends Controller
{
    public function index()
    {
        addVendors(['datatables']);
        return view('pages.kpimanagement.index');
    }
}
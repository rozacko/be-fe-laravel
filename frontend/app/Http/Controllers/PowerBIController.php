<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PowerBIController extends Controller
{
    public function index(Request $request)
    {
        $data = [];
        $data['powerbi_url'] = base64_decode($request->get('powerbi_url'));
        return view('pages.powerbi.iframe', $data);
    }
}
<?php
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\File;

function HttpTable($request, $endpoint = '')
{
    $data = Http::withToken($request->session()->get('token'))->get(config('app.api_url') . $endpoint.'/', $request->all());
    return response($data->body(), $data->status())->header('Content-Type', 'application/json');
}

function HttpGet($request, $id = '', $endpoint = '')
{
    $response = Http::withToken($request->session()->get('token'))->get(config('app.api_url') . $endpoint.'/'.$id, $request->all());
    $decode = json_decode($response->body());
    if ($response->status() === 401) {
        $res = [
            'status' => 'fail',
            'message'=> $decode->message
        ];
        return $res;
    }
    $res = $decode->data;
    return $res;
}

function HttpPost($request, $endpoint = '')
{
    $response = Http::withToken($request->session()->get('token'))->post(config('app.api_url') . $endpoint, $request);        
    $decode = json_decode($response->body());
    if ($response->status() === 201 || $response->status() === 200) {
        $res = [
            'status' => 'success',
            'message'=> $decode->message
        ];
        $res['modal'] = 'close';
    }elseif ($response->status() === 400) {
        foreach ($decode->errors as $key => $value) {
            $message[] = implode('<br>', $value);
        }        
        $res = [
            'status' => 'fail',
            'message'=> '<ul>' . implode('<br>', $message) . '</ul>',
        ];
    }else{  
        $res = [
            'status' => 'fail',
            'message'=> '<b>Error Code : '.$response->status() . '</b><br>' . isset($decode->message) ?: ''
        ];        
    }        
    return response()->json($res);
}

function HttpPut($request, $id = '', $endpoint = '')
{
    $response = Http::withToken($request->session()->get('token'))->put(config('app.api_url') . $endpoint.'/'.$id, $request);        
    $decode = json_decode($response->body());        
    if ($response->status() === 200) {
        $res = [
            'status' => 'success',
            'message'=> $decode->message
        ];
        $res['modal'] = 'close';
    }
    elseif($response->status() === 201){
        $res = [
            'status' => 'success',
            'message'=> $decode->message
        ];
        $res['modal'] = 'close';
    }
    elseif ($response->status() === 400) {
        foreach ($decode->errors as $key => $value) {
            $message[] = implode('<br>', $value);
        }        
        $res = [
            'status' => 'fail',
            'message'=> '<ul>' . implode('<br>', $message) . '</ul>',
        ];
    }else{  
        $res = [
            'status' => 'fail',
            'message'=> '<b>Error Code : '.$response->status() . '</b><br>' . isset($decode->message) ?: ''
        ];        
    }        
    return response()->json($res);
}

function HttpDelete($request, $id = '', $endpoint = '')
{
    $response = Http::withToken($request->session()->get('token'))->delete(env('API_URL') . $endpoint . '/' . $id, $request);    
    $decode = json_decode($response->body());        
    if ($response->status() === 200) {
        $res = [
            'status' => 'success',
            'message' => $decode->message
        ];
    } elseif ($response->status() === 400) {
        foreach ($decode->errors as $key => $value) {
            $message[] = implode('<br>', $value);
        }
        $res = [
            'status' => 'fail',
            'message' => '<ul>' . implode('<br>', $message) . '</ul>',
        ];
    } else {
        $res = [
            'status' => 'fail',
            'message'=> '<b>Error Code : '.$response->status() . '</b><br>' . isset($decode->message) ?: ''
        ];
    }
    return response()->json($res);
}

function HttpPostFile($request, $endpoint = '')
{
    $data = $request->except(['upload_file']);
    $response = Http::attach('upload_file', file_get_contents($request->file('upload_file')->getRealPath()), $request->file('upload_file')->getClientOriginalName())->withToken($request->session()->get('token'))->post(env('API_URL') . $endpoint, $data);    
    $decode = json_decode($response->body());
    if ($response->status() === 200) {
        $res = [
            'status' => 'success',
            'message'=> $decode->message,
            'data'  => $decode->data
        ];
        $res['modal'] = 'close';
    }elseif ($response->status() === 201) {
        $res = [
            'status' => 'success',
            'message'=> $decode->message,
            'data'=> isset($decode->data) ? $decode->data : ''
        ];
        $res['modal'] = 'close';
    }elseif ($response->status() === 400) {
        foreach ($decode->errors as $key => $value) {
            $message[] = implode('<br>', $value);
        }
        $res = [
            'status' => 'fail',
            'message'=> '<ul>' . implode('<br>', $message) . '</ul>',
        ];
    }
    elseif ($response->status() === 500) {
        print_r($response->serverError());exit;
        foreach ($decode->errors as $key => $value) {
            $message[] = implode('<br>', $value);
        }
        $res = [
            'status' => 'fail',
            'message'=> '<ul>' . implode('<br>', $message) . '</ul>',
        ];
    }else{
        $res = [
            'status' => 'fail',
            'message'=> $decode->message
        ];
    }
    return response()->json($res);
}

function HttpGetFile($request, $endpoint = '')
{
    $response = Http::withToken($request->session()->get('token'))->get(config('app.api_url') . $endpoint);
    $filename = str_replace("attachment; filename=","",$response->header("Content-Disposition"));
    return response()->streamDownload(function() use($response){
        echo $response->body();
    }, $filename);
}

function HttpPostDownloadExcel($request, $endpoint = '')
{
    $response = Http::withToken($request->session()->get('token'))->post(config('app.api_url') . $endpoint, $request);
    $decode = json_decode($response->body());
    if ($response->status() === 201) {
        $res = [
            'status' => 'success',
            'message'=> $decode->message,
            'data'=> $decode->data
        ];
    }elseif ($response->status() === 400) {
        foreach ($decode->errors as $key => $value) {
            $message[] = implode('<br>', $value);
        }
        $res = [
            'status' => 'fail',
            'message'=> '<ul>' . implode('<br>', $message) . '</ul>',
        ];
    }else{
        $res = [
            'status' => 'fail',
            'message'=> $decode->message
        ];
    }
    return response()->json($res);
}


function HttpPostFileVendor($request, $endpoint = '')
{
    $data = $request->except(['upload_file']);
    $response = Http::attach('upload_file', file_get_contents($request->file('upload_file')->getRealPath()), $request->file('upload_file')->getClientOriginalName())->withToken($request->session()->get('token'))->post(env('API_URL') . $endpoint, $data);    
    $decode = json_decode($response->body());
    if ($response['data']) {
        $res = [
            'status' => 'success',
            'message'=> $response['data'],
            'data'  => $decode->data
        ];
        $res['modal'] = 'close';
    }
    
    return response()->json($res);
}

function HttpGetByid($request, $id = '', $endpoint = '')
{
    $response = Http::withToken($request->session()->get('token'))->get(config('app.api_url') . $endpoint.'/'.$id);
    $decode = json_decode($response->body());
    if ($response->status() === 401) {
        $res = [
            'status' => 'fail',
            'message'=> $decode->message
        ];
        return $res;
    }
    $res = $decode->data[0];
    return $res;
}
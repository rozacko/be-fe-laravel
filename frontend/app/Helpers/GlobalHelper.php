<?php

function getYear()
{
    for($a=date("Y")+1;$a>=2019;$a--){
        $year[] = $a;
    }
    return $year;
}

function getBulan()
{
    $dt = [
        1 => ['month' => '1', 'month_name' => 'January', 'month_value' => 'january'],
        2 => ['month' => '2', 'month_name' => 'February', 'month_value' => 'february'],
        3 => ['month' => '3', 'month_name' => 'March', 'month_value' => 'march'],
        4 => ['month' => '4', 'month_name' => 'April', 'month_value' => 'april'],
        5 => ['month' => '5', 'month_name' => 'May', 'month_value' => 'may'],
        6 => ['month' => '6', 'month_name' => 'June', 'month_value' => 'june'],
        7 => ['month' => '7', 'month_name' => 'July', 'month_value' => 'july'],
        8 => ['month' => '8', 'month_name' => 'August', 'month_value' => 'august'],
        9 => ['month' => '9', 'month_name' => 'September', 'month_value' => 'september'],
        10 => ['month' => '10', 'month_name' => 'October', 'month_value' => 'october'],
        11 => ['month' => '11', 'month_name' => 'November', 'month_value' => 'november'],
        12 => ['month' => '12', 'month_name' => 'December', 'month_value' => 'december'],
        // 13 => ['month' => 13, 'month_name' => 'All Month', 'month_value' => 'all month'],
    ];
    return $dt;

}

function getMonth()
{
    $dt = [
        1 => ['month' => '01', 'month_name' => 'January', 'month_value' => 'january'],
        2 => ['month' => '02', 'month_name' => 'February', 'month_value' => 'february'],
        3 => ['month' => '03', 'month_name' => 'March', 'month_value' => 'march'],
        4 => ['month' => '04', 'month_name' => 'April', 'month_value' => 'april'],
        5 => ['month' => '05', 'month_name' => 'May', 'month_value' => 'may'],
        6 => ['month' => '06', 'month_name' => 'June', 'month_value' => 'june'],
        7 => ['month' => '07', 'month_name' => 'July', 'month_value' => 'july'],
        8 => ['month' => '08', 'month_name' => 'August', 'month_value' => 'august'],
        9 => ['month' => '09', 'month_name' => 'September', 'month_value' => 'september'],
        10 => ['month' => '10', 'month_name' => 'October', 'month_value' => 'october'],
        11 => ['month' => '11', 'month_name' => 'November', 'month_value' => 'november'],
        12 => ['month' => '12', 'month_name' => 'December', 'month_value' => 'december'],
        // 13 => ['month' => 13, 'month_name' => 'All Month', 'month_value' => 'all month'],
    ];
    return $dt;

}

function getParameter()
{
    $parameter = [
        1 => ['value' => 'Target'],
        2 => ['value' => 'Realisasi'],
        3 => ['value' => 'Achievement'],
    ];
    return $parameter;
}

function parameterNilaiInventory()
{
    $dataParameter = [
            1 => ['value' => 'kap. produksi clinker (ton)'],
            2 => ['value' => 'construction materials'],
            3 => ['value' => 'emergency part'],
            4 => ['value' => 'heavy equipment & vehicle'],
            5 => ['value' => 'elect./instrument/gen use'],
            6 => ['value' => 'office supplies'],
            7 => ['value' => 'machinary & spare part'],
            8 => ['value' => 'equipment & supplies'],
            9 => ['value' => 'brick'],
            10 => ['value' => 'grinding ball'],
            11 => ['value' => 'uitval'],
            12 => ['value' => 'nilai inventory non raw material'],
            13 => ['value' => 'index inventory (usd/ton)'],
        ];
    return $dataParameter;
}

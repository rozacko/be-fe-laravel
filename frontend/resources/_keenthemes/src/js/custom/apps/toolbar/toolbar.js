"use strict";

// Class definition
var KTMToolbar = (function () {
    var submitButton;
    var cancelButton;
    var validator;
    var form;
    var modal;
    var modalEl;

    // Init form inputs
    var initForm = function () {
        // Ticket attachments
        // For more info about Dropzone plugin visit:  https://www.dropzonejs.com/#usage

        // Due date. For more info, please visit the official plugin site: https://flatpickr.js.org/
        var dueDate = $(form.querySelector('[name="date_pick"]'));
        dueDate.flatpickr({
            enableTime: true,
            dateFormat: "d, M Y, H:i",
        });
    };

    return {
        // Public functions
        init: function () {
            initForm();
            handleForm();
        },
    };
})();

// On document ready
KTUtil.onDOMContentLoaded(function () {
    KTMToolbar.init();
});

"use strict";

// Class definition
var KtRealisasiTHDRkap = (function () {
    var chart = {
        self: null,
        rendered: false,
    };

    // Private methods
    var initChart = function (chart) {
        var element = document.getElementById(
            "kt_realisasi_thd_rkap"
        );

        if (!element) {
            return;
        }

        var height = parseInt(KTUtil.css(element, "height"));

        var options = {
            series: [{
            name: 'TERAK OPC',
            color: 'rgb(184, 184, 184)',
            data: [20, 30, 25, 15, 0, 22, 12]
            }, {
            name: 'KAPASITAS STORAGE',
            color: 'rgb(233, 68, 69)',
            data: [13, 23, 20, 8, 10, 13, 20]
            }],
            // dataLabels: {
            //   enabled: false
            // },
            chart: {
            type: 'bar',
            height: height,
            stacked: true,
            //   stackType: '100%'
            },
                        responsive: [{
                        breakpoint: 480,
                        options: {

                        }
                        }],
                        xaxis: {
                            categories: ['TUBAN 1', 'TUBAN 2', 'TUBAN 3', 'TUBAN 4','LAP' ,'TOTAL TUBAN', 'GRESIK'
                            ],
                        },
                        fill: {
                        opacity: 1,
                        type: "gradient"
                        },
                        legend: {
                            position: 'top',
                            horizontalAlign: 'left',
                            offsetX: 0
                        },
        };
        chart.self = new ApexCharts(element, options);

        // Set timeout to properly get the parent elements width
        setTimeout(function () {
            chart.self.render();
            chart.rendered = true;
        }, 200);
    };

    // Public methods
    return {
        init: function () {
            initChart(chart);

            // Update chart on theme mode change
            KTThemeMode.on("kt.thememode.change", function () {
                if (chart.rendered) {
                    chart.self.destroy();
                }

                initChart(chart);
            });
        },
    };
})();

// Webpack support
if (typeof module !== "undefined") {
    module.exports = KtRealisasiTHDRkap;
}

// On document ready
KTUtil.onDOMContentLoaded(function () {
    KtRealisasiTHDRkap.init();
});

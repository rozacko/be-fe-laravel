"use strict";

// Class definition
var KTChartsPlantPerformanceMtbfKlin = (function () {
    var chart = {
        self: null,
        rendered: false,
    };

    // Private methods
    var initChart = function (chart) {
        var element = document.getElementById("kt_charts_widget_plant_performance_mtbf_klin");

        if (!element) {
            return;
        }

        var height = parseInt(KTUtil.css(element, "height"));
        var width = parseInt(KTUtil.css(element, "width"));
        var labelColor = KTUtil.getCssVariableValue("--bs-gray-500");
        var borderColor = KTUtil.getCssVariableValue(
            "--bs-border-dashed-color"
        );
        var baseprimaryColor = KTUtil.getCssVariableValue("--bs-primary");
        var lightprimaryColor = KTUtil.getCssVariableValue("--bs-primary");
        var basesuccessColor = KTUtil.getCssVariableValue("--bs-success");
        var lightsuccessColor = KTUtil.getCssVariableValue("--bs-success");


        var options = {
            series: [
                {
                    name: "RKAP",
                    data: [44, 55, 41, 64, 22, 43, 40],
                },
                {
                    name: "REALISASI",
                    data: [53, 32, 33, 52, 13, 44, 32],
                },
                {
                    name: "PROGNOSA",
                    data: [53, 32, 33, 52, 13, 44, 32],
                },
            ],
            chart: {
                type: "bar",
                height: 450,
            },
            plotOptions: {
                bar: {
                    horizontal: true,
                    dataLabels: {
                        position: "top",
                    },
                },
            },
            dataLabels: {
                enabled: true,
                offsetX: -6,
                style: {
                    fontSize: "12px",
                    colors: ["#fff"],
                },
            },
            stroke: {
                show: true,
                width: 1,
                colors: ["#fff"],
            },
            tooltip: {
                shared: true,
                intersect: false,
            },
            xaxis: {
                categories: [2001, 2002, 2003, 2004, 2005, 2006, 2007],
            },
            legend: {
                show: true,
                position: "top",
                horizontalAlign: "left",
                borderRadius: "50px",
                markers: {
                    radius: 50,
                },
            },
            colors: ["#021e4d", "#058001","#f2ff03"],
        };
        chart.self = new ApexCharts(element, options);

        // Set timeout to properly get the parent elements width
        setTimeout(function () {
            chart.self.render();
            chart.rendered = true;
        }, 200);
    };

    // Public methods
    return {
        init: function () {
            initChart(chart);

            // Update chart on theme mode change
            KTThemeMode.on("kt.thememode.change", function () {
                if (chart.rendered) {
                    chart.self.destroy();
                }

                initChart(chart);
            });
        },
    };
})();

// Webpack support
if (typeof module !== "undefined") {
    module.exports = KTChartsPlantPerformanceMtbfKlin;
}

// On document ready
KTUtil.onDOMContentLoaded(function () {
    KTChartsPlantPerformanceMtbfKlin.init();
});

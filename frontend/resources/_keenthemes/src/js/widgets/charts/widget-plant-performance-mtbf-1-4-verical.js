"use strict";

// Class definition
var KTChartsPlantPerformanceMtbfKlinVertival = (function () {
    var chart = {
        self: null,
        rendered: false,
    };

    // Private methods
    var initChart = function (chart) {
        var element = document.getElementById(
            "kt_charts_widget_plant_performance_mtbf_klin_vertical"
        );

        if (!element) {
            return;
        }

        var height = parseInt(KTUtil.css(element, "height"));
        var width = parseInt(KTUtil.css(element, "width"));
        var labelColor = KTUtil.getCssVariableValue("--bs-gray-500");
        var borderColor = KTUtil.getCssVariableValue(
            "--bs-border-dashed-color"
        );
        var baseprimaryColor = KTUtil.getCssVariableValue("--bs-primary");
        var lightprimaryColor = KTUtil.getCssVariableValue("--bs-primary");
        var basesuccessColor = KTUtil.getCssVariableValue("--bs-success");
        var lightsuccessColor = KTUtil.getCssVariableValue("--bs-success");

        var options = {
            series: [
                {
                    name: "A",
                    type: "column",
                    data: [23, 42, 35, 27, 43, 22, 17, 31, 22, 22, 12, 16],
                },
                {
                    name: "B",
                    type: "column",
                    data: [23, 42, 31, 22, 22, 12, 35, 27, 43, 22, 17, 16],
                },
                {
                    name: "C",
                    type: "column",
                    data: [23, 42, 35, 31, 22, 22, 27, 43, 22, 17, 12, 16],
                },
                {
                    name: "D",
                    type: "column",
                    data: [27, 43, 22, 17, 31, 22, 22, 12, 23, 42, 35, 16],
                },
                {
                    name: "LINE A",
                    type: "line",
                    data: [43, 31, 22, 23, 22, 17, 12, 42, 35, 27, 22, 16],
                },
                {
                    name: "LINE B",
                    type: "line",
                    data: [22, 16, 22, 17, 43, 31, 22, 23, 35, 27, 12, 42],
                },
            ],
            chart: {
                height: 250,
                type: "line",
            },
            stroke: {
                width: [0, 4],
            },
            tooltip: {
                shared: true,
                intersect: false,
            },
            dataLabels: {
                enabled: true,
                enabledOnSeries: [1],
            },
            labels: [
                "JAN",
                "FEB",
                "MAR",
                "APR",
                "MEI",
                "JUN",
                "JUL",
                "AGS",
                "SEPT",
                "OKT",
                "NOV",
                "DES",
            ],
            colors: ["#2a78f5","#f70233","#90cc3d", "#591bcc"],
            legend: {
                show: false,
                position: "top",
                horizontalAlign: "left",
                borderRadius: "50px",
                markers: {
                    radius: 50,
                },
            },
        };
        chart.self = new ApexCharts(element, options);

        // Set timeout to properly get the parent elements width
        setTimeout(function () {
            chart.self.render();
            chart.rendered = true;
        }, 200);
    };

    // Public methods
    return {
        init: function () {
            initChart(chart);

            // Update chart on theme mode change
            KTThemeMode.on("kt.thememode.change", function () {
                if (chart.rendered) {
                    chart.self.destroy();
                }

                initChart(chart);
            });
        },
    };
})();

// Webpack support
if (typeof module !== "undefined") {
    module.exports = KTChartsPlantPerformanceMtbfKlinVertival;
}

// On document ready
KTUtil.onDOMContentLoaded(function () {
    KTChartsPlantPerformanceMtbfKlinVertival.init();
});

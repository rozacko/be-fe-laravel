"use strict";

// Class definition
var KTChartsPartNonRutin = (function () {
    var chart = {
        self: null,
        rendered: false,
    };

    // Private methods
    var initChart = function (chart) {
        var element = document.getElementById("kt_charts_widget_non_rutin");

        if (!element) {
            return;
        }

        var height = parseInt(KTUtil.css(element, "height"));
        var width = parseInt(KTUtil.css(element, "width"));

        var options = {
            series: [44, 55, 22, 50],
            chart: {
                type: "pie",
                height: height,
                // width: 260,
            },
            legend: {
                position: "bottom",
                horizontalAlign:'center',
            },
            colors: ["#5B9BD5", "#ED7D31"],
            labels: ["PO", "RESERVASI", "READY", "PR"],
            plotOptions: {
                pie: {},
            },
            grid: {
                padding: {
                    top: 20,
                    right: 0,
                    bottom: 0,
                    left: 0,
                },
            },
        };

        chart.self = new ApexCharts(element, options);

        // Set timeout to properly get the parent elements width
        setTimeout(function () {
            chart.self.render();
            chart.rendered = true;
        }, 200);
    };

    // Public methods
    return {
        init: function () {
            initChart(chart);

            // Update chart on theme mode change
            KTThemeMode.on("kt.thememode.change", function () {
                if (chart.rendered) {
                    chart.self.destroy();
                }

                initChart(chart);
            });
        },
    };
})();

// Webpack support
if (typeof module !== "undefined") {
    module.exports = KTChartsPartNonRutin;
}

// On document ready
KTUtil.onDOMContentLoaded(function () {
    KTChartsPartNonRutin.init();
});

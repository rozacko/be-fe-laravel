"use strict";

// Class definition
var KTChartsBacklogOrderType = (function () {
    var chart = {
        self: null,
        rendered: false,
    };

    // Private methods
    var initChart = function (chart) {
        var element = document.getElementById("kt_charts_widget_plant_performance_backlog_order_type");

        if (!element) {
            return;
        }

        var height = parseInt(KTUtil.css(element, "height"));
        var width = parseInt(KTUtil.css(element, "width"));

        var options = {
            series: [44, 55, 41, 17, 15],
            chart: {
            type: 'donut',
          },
          responsive: [{
            breakpoint: 480,
            options: {
              chart: {
                width: 200,
                height: 100,
              },
              legend: {
                position: 'bottom'
              }
            }
          }]
          };

        chart.self = new ApexCharts(element, options);

        // Set timeout to properly get the parent elements width
        setTimeout(function () {
            chart.self.render();
            chart.rendered = true;
        }, 200);
    };

    // Public methods
    return {
        init: function () {
            initChart(chart);

            // Update chart on theme mode change
            KTThemeMode.on("kt.thememode.change", function () {
                if (chart.rendered) {
                    chart.self.destroy();
                }

                initChart(chart);
            });
        },
    };
})();

// Webpack support
if (typeof module !== "undefined") {
    module.exports = KTChartsBacklogOrderType;
}

// On document ready
KTUtil.onDOMContentLoaded(function () {
    KTChartsBacklogOrderType.init();
});

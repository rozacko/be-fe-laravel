"use strict";

// Class definition
var KTChartsSparepartNonRutinTwo = (function () {
    var chart = {
        self: null,
        rendered: false,
    };

    // Private methods
    var initChart = function (chart) {
        var element = document.getElementById(
            "kt_charts_widget_sparepart_non_rutin_two"
        );

        if (!element) {
            return;
        }

        var height = parseInt(KTUtil.css(element, "height"));

        var options = {
            series: [
                {
                    data: [400, 430, 448, 470],
                },
            ],
            chart: {
                type: "bar",
                height: 250,
                toolbar: {
                    show: false,
                },
            },
            plotOptions: {
                bar: {
                    borderRadius: 0,
                    horizontal: true,
                    distributed: true,
                },
            },

            colors: ["#0072F0", "#3FAEB5", "#F66D00", "#FCB414"],
            dataLabels: {
                enabled: true,
            },
            legend: {
                show: true,
                position: "top",
                horizontalAlign: "left",
                fontSize: "10px",
                borderRadius: "50px",
                markers: {
                    radius: 50,
                },
            },
            xaxis: {
                position: "bottom",
                categories: [
                    "Electrical",
                    "Mekanical",
                    "Sipil",
                    "Pabrik Semen",
                ],
            },
            yaxis: {
                show: false,
                labels: {
                    show: false,
                },
            },
        };
        chart.self = new ApexCharts(element, options);

        // Set timeout to properly get the parent elements width
        setTimeout(function () {
            chart.self.render();
            chart.rendered = true;
        }, 200);
    };

    // Public methods
    return {
        init: function () {
            initChart(chart);

            // Update chart on theme mode change
            KTThemeMode.on("kt.thememode.change", function () {
                if (chart.rendered) {
                    chart.self.destroy();
                }

                initChart(chart);
            });
        },
    };
})();

// Webpack support
if (typeof module !== "undefined") {
    module.exports = KTChartsSparepartNonRutinTwo;
}

// On document ready
KTUtil.onDOMContentLoaded(function () {
    KTChartsSparepartNonRutinTwo.init();
});

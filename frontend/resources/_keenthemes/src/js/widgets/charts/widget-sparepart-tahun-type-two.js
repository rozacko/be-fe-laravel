"use strict";

// Class definition
var KTChartsSparepartTahunTwo = (function () {
    var chart = {
        self: null,
        rendered: false,
    };

    // Private methods
    var initChart = function (chart) {
        var element = document.getElementById(
            "kt_charts_sparepart_tahun_type_two"
        );

        if (!element) {
            return;
        }

        var height = parseInt(KTUtil.css(element, "height"));

        var options = {
            series: [
                {
                    data: [400, 430, 448, 470],
                },
            ],
            chart: {
                type: "bar",
                height: 250,
                toolbar: {
                    show: false,
                },
            },
            plotOptions: {
                bar: {
                    borderRadius: 0,
                    horizontal: true,
                    distributed: true,
                },
            },

            colors: ["#084E67", "#3FAEB5", "#F66D00", "#FCB414"],
            dataLabels: {
                enabled: true,
            },
            legend: {
                show: true,
                position: "top",
                horizontalAlign: "left",
                fontSize: "10px",
                borderRadius: "50px",
                markers: {
                    radius: 50,
                },
            },
            xaxis: {
                position: "bottom",
                categories: [
                    "Total ECE",
                    "Nilai OP",
                    "Nilai GR",
                    "Nilai GI (%)",
                ],
            },
            yaxis: {
                show: false,
                labels: {
                    show: false,
                },
            },
        };
        chart.self = new ApexCharts(element, options);

        // Set timeout to properly get the parent elements width
        setTimeout(function () {
            chart.self.render();
            chart.rendered = true;
        }, 200);
    };

    // Public methods
    return {
        init: function () {
            initChart(chart);

            // Update chart on theme mode change
            KTThemeMode.on("kt.thememode.change", function () {
                if (chart.rendered) {
                    chart.self.destroy();
                }

                initChart(chart);
            });
        },
    };
})();

// Webpack support
if (typeof module !== "undefined") {
    module.exports = KTChartsSparepartTahunTwo;
}

// On document ready
KTUtil.onDOMContentLoaded(function () {
    KTChartsSparepartTahunTwo.init();
});

"use strict";

// Class definition
var KTChartsPlanProperty = (function () {
    var chart = {
        self: null,
        rendered: false,
    };

    // Private methods
    var initChart = function (chart) {
        var element = document.getElementById(
            "kt_charts_widget_plant_property"
        );

        if (!element) {
            return;
        }

        var height = parseInt(KTUtil.css(element, "height"));

        var options = {
            series: [
                {
                    name: ["JASA"],
                    data: [44, 43],
                },
                {
                    name: ["SPARE PART"],
                    data: [53, 32],
                },
            ],
            chart: {
                type: "bar",
                height: height,
            },
            plotOptions: {
                bar: {
                    horizontal: false,
                    dataLabels: {
                        position: "top",
                    },
                },
            },
            dataLabels: {
                enabled: true,
                offsetX: -6,
                style: {
                    fontSize: "12px",
                    colors: ["#fff"],
                },
            },
            stroke: {
                show: true,
                width: 1,
                colors: ["#fff"],
            },
            tooltip: {
                shared: true,
                intersect: false,
            },
            colors: ["#042568", "#ED7D31"],

            legend: {
                show: true,
                position: "top",
                horizontalAlign: "left",
                fontSize: "10px",
                borderRadius: "50px",
                markers: {
                    radius: 50,
                },
            },
            xaxis: {
                categories: ["JASA", "SPARE PART"],
            },
        };
        chart.self = new ApexCharts(element, options);

        // Set timeout to properly get the parent elements width
        setTimeout(function () {
            chart.self.render();
            chart.rendered = true;
        }, 200);
    };

    // Public methods
    return {
        init: function () {
            initChart(chart);

            // Update chart on theme mode change
            KTThemeMode.on("kt.thememode.change", function () {
                if (chart.rendered) {
                    chart.self.destroy();
                }

                initChart(chart);
            });
        },
    };
})();

// Webpack support
if (typeof module !== "undefined") {
    module.exports = KTChartsPlanProperty;
}

// On document ready
KTUtil.onDOMContentLoaded(function () {
    KTChartsPlanProperty.init();
});

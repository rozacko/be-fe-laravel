"use strict";

// Class definition
var KTChartsPlantPerformanceParetoFrequesiDowntime = (function () {
    var chart = {
        self: null,
        rendered: false,
    };

    // Private methods
    var initChart = function (chart) {
        var element = document.getElementById(
            "kt_charts_widget_plant_performance_pareto_frequesi_upplant_downtime_klin"
        );

        if (!element) {
            return;
        }

        var height = parseInt(KTUtil.css(element, "height"));
        var width = parseInt(KTUtil.css(element, "width"));
        var labelColor = KTUtil.getCssVariableValue("--bs-gray-500");
        var borderColor = KTUtil.getCssVariableValue(
            "--bs-border-dashed-color"
        );
        var baseprimaryColor = KTUtil.getCssVariableValue("--bs-primary");
        var lightprimaryColor = KTUtil.getCssVariableValue("--bs-primary");
        var basesuccessColor = KTUtil.getCssVariableValue("--bs-success");
        var lightsuccessColor = KTUtil.getCssVariableValue("--bs-success");

        var options = {
            series: [
                {
                    name: "A",
                    type: "column",
                    data: [23, 42, 35, 27, 43, 22, 17, 31, 22, 22, 12, 16],
                },
                {
                    name: "LINE A",
                    type: "line",
                    data: [43, 31, 22, 23, 22, 17, 12, 42, 35, 27, 22, 16],
                },
            ],
            chart: {
                height: 250,
                type: "line",
            },
            stroke: {
                width: [0, 4],
            },
            tooltip: {
                shared: true,
                intersect: false,
            },
            dataLabels: {
                enabled: true,
                enabledOnSeries: [1],
            },
            labels: [
                "441KLI1",
                "442KLI1",
                "443KLI1",
                "444KLI1",
                "445KLI1",
                "446KLI1",
                "447KLI1",
                "448KLI1",
                "449KLI1",
                "4410KLI1",
                "4411KLI1",
                "4412KLI1",
            ],
            colors: ["#2a78f5"],
            legend: {
                show: false,
                position: "top",
                horizontalAlign: "left",
                borderRadius: "50px",
                markers: {
                    radius: 50,
                },
            },
        };
        chart.self = new ApexCharts(element, options);

        // Set timeout to properly get the parent elements width
        setTimeout(function () {
            chart.self.render();
            chart.rendered = true;
        }, 200);
    };

    // Public methods
    return {
        init: function () {
            initChart(chart);

            // Update chart on theme mode change
            KTThemeMode.on("kt.thememode.change", function () {
                if (chart.rendered) {
                    chart.self.destroy();
                }

                initChart(chart);
            });
        },
    };
})();

// Webpack support
if (typeof module !== "undefined") {
    module.exports = KTChartsPlantPerformanceParetoFrequesiDowntime;
}

// On document ready
KTUtil.onDOMContentLoaded(function () {
  KTChartsPlantPerformanceParetoFrequesiDowntime.init();
});

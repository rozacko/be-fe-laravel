"use strict";

// Class definition
var KTChartsRealisasiCapex = (function () {
    var chart = {
        self: null,
        rendered: false,
    };

    // Private methods
    var initChart = function (chart) {
        var element = document.getElementById(
            "kt_charts_widget_progress_realisasi_capex"
        );

        if (!element) {
            return;
        }

        var height = parseInt(KTUtil.css(element, "height"));

        var options = {
            series: [
                {
                    data: [400, 430, 448, 470, 400, 400, 420],
                },
            ],
            chart: {
                type: "bar",
                height: height,
                toolbar: {
                    show: false,
                },
            },
            plotOptions: {
                bar: {
                    borderRadius: 0,
                    horizontal: false,
                    distributed: true,
                    columnWidth: "80%",
                    dataLabels: {
                        total: {
                            enabled: true,
                            offsetX: 0,
                            style: {
                                fontSize: "13px",
                                fontWeight: 900,
                            },
                        },
                    },
                },
            },

            colors: ["#D26314"],
            dataLabels: {
                enabled: false,
            },
            legend: {
                show: false,
            },
            xaxis: {
                position: "bottom",
                categories: [
                    "WBS",
                    "NETWORK",
                    "RESERVAS",
                    "PR",
                    "RFQ",
                    "PO",
                    "GR",
                ],
            },
            yaxis: {
                show: false,
                labels: {
                    show: false,
                },
            },
        };
        chart.self = new ApexCharts(element, options);

        // Set timeout to properly get the parent elements width
        setTimeout(function () {
            chart.self.render();
            chart.rendered = true;
        }, 200);
    };

    // Public methods
    return {
        init: function () {
            initChart(chart);

            // Update chart on theme mode change
            KTThemeMode.on("kt.thememode.change", function () {
                if (chart.rendered) {
                    chart.self.destroy();
                }

                initChart(chart);
            });
        },
    };
})();

// Webpack support
if (typeof module !== "undefined") {
    module.exports = KTChartsRealisasiCapex;
}

// On document ready
KTUtil.onDOMContentLoaded(function () {
    KTChartsRealisasiCapex.init();
});

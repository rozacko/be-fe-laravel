"use strict";

// Class definition
var KTChartsOverhoulKurva = (function () {
    var chart = {
        self: null,
        rendered: false,
    };

    // Private methods
    var initChart = function (chart) {
        var element = document.getElementById(
            "kt_charts_widget_overhoul_kurva"
        );

        if (!element) {
            return;
        }

        var height = parseInt(KTUtil.css(element, "height"));
        var options = {
            series: [
                {
                    name: "PLANT MAJOR",
                    data: [10, 41, 35, 100, 49, 62, 100, 91, 148],
                },
                {
                    name: "PLANT MINOR",
                    data: [10, 41, 25, 51, 49, 100, 69, 100, 148],
                },
                {
                    name: "ACTUAL MINOR",
                    data: [10, 41, 100, 51, 49, 62, 69, 100, 148],
                },
                {
                    name: "ACTUAL MAJOR",
                    data: [10, 41, 100, 51, 49, 62, 69, 100, 148],
                },
            ],
            chart: {
                height: 500,
                type: "line",
                zoom: {
                    enabled: false,
                },
            },
            dataLabels: {
                enabled: false,
            },
            stroke: {
                curve: "straight",
            },
            title: {
                text: "KURVA ACTIVITY",
                align: "left",
                style: {
                    fontSize: "14px",
                    fontWeight: "bold",
                    color: "#000000",
                },
            },
            grid: {
                row: {
                    colors: ["#f3f3f3", "transparent"], // takes an array which will be repeated on columns
                    opacity: 0.5,
                },
            },
            legend: {
                show: true,
                position: "top",
                horizontalAlign: "left",
                borderRadius: "50px",
                fontSize: "8px",
                markers: {
                    radius: 50,
                },
            },
            colors: ["#08A5E2", "#04CB04", "#FFCC06", "#FC0000"],

            xaxis: {
                categories: [
                    "1148",
                    "1374",
                    "1439",
                    "1577",
                    "1784",
                    "2798",
                    "3536",
                    "3933",
                    "4152",
                ],
            },
        };

        chart.self = new ApexCharts(element, options);

        // Set timeout to properly get the parent elements width
        setTimeout(function () {
            chart.self.render();
            chart.rendered = true;
        }, 200);
    };

    // Public methods
    return {
        init: function () {
            initChart(chart);

            // Update chart on theme mode change
            KTThemeMode.on("kt.thememode.change", function () {
                if (chart.rendered) {
                    chart.self.destroy();
                }

                initChart(chart);
            });
        },
    };
})();

// Webpack support
if (typeof module !== "undefined") {
    module.exports = KTChartsOverhoulKurva;
}

// On document ready
KTUtil.onDOMContentLoaded(function () {
    KTChartsOverhoulKurva.init();
});

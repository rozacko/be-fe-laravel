"use strict";

// Class definition
var KTChartsKlinRateTDP = (function () {
    var chart = {
        self: null,
        rendered: false,
    };

    // Private methods
    var initChart = function (chart) {
        var element = document.getElementById("kt_charts_widget_klin_rate_tdp");

        if (!element) {
            return;
        }

        var height = parseInt(KTUtil.css(element, "height"));
        var options = {
            series: [
                {
                    name: "NAI Rkap (%)",
                    data: [10, 21, 15, 51, 49, 62, 69, 91, 108],
                },
                {
                    name: "NAI Real (%)",
                    data: [10, 31, 35, 51, 49, 62, 29, 91, 18],
                },
                {
                    name: "NAI Real (%)",
                    data: [10, 41, 25, 51, 20, 62, 49, 91, 248],
                },
            ],
            chart: {
                height: height,
                type: "line",
                zoom: {
                    enabled: false,
                },
                toolbar: {
                    show: false,
                },
            },
            dataLabels: {
                enabled: false,
            },
            stroke: {
                curve: "smooth",
            },
            // title: {
            //     text: "Product Trends by Month",
            //     align: "left",
            // },
            grid: {
                row: {
                    colors: ["#f3f3f3", "transparent"], // takes an array which will be repeated on columns
                    opacity: 0.5,
                },
            },
            legend: {
                show: true,
                position: "top",
                horizontalAlign: "left",
                borderRadius: "50px",
                markers: {
                    radius: 50,
                },
            },
            xaxis: {
                categories: [
                    "Jan",
                    "Feb",
                    "Mar",
                    "Apr",
                    "May",
                    "Jun",
                    "Jul",
                    "Aug",
                    "Sep",
                ],
            },
        };

        chart.self = new ApexCharts(element, options);

        // Set timeout to properly get the parent elements width
        setTimeout(function () {
            chart.self.render();
            chart.rendered = true;
        }, 200);
    };

    // Public methods
    return {
        init: function () {
            initChart(chart);

            // Update chart on theme mode change
            KTThemeMode.on("kt.thememode.change", function () {
                if (chart.rendered) {
                    chart.self.destroy();
                }

                initChart(chart);
            });
        },
    };
})();

// Webpack support
if (typeof module !== "undefined") {
    module.exports = KTChartsKlinRateTDP;
}

// On document ready
KTUtil.onDOMContentLoaded(function () {
    KTChartsKlinRateTDP.init();
});

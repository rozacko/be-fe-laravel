"use strict";

// Class definition
var KTChartsKlinNetAvailability = (function () {
    var chart = {
        self: null,
        rendered: false,
    };

    // Private methods
    var initChart = function (chart) {
        var element = document.getElementById(
            "kt_charts_widget_klin-net-availability"
        );

        if (!element) {
            return;
        }

        var height = parseInt(KTUtil.css(element, "height"));
        var labelColor = KTUtil.getCssVariableValue("--bs-gray-500");
        var borderColor = KTUtil.getCssVariableValue(
            "--bs-border-dashed-color"
        );
        var baseprimaryColor = KTUtil.getCssVariableValue("--bs-primary");
        var lightprimaryColor = KTUtil.getCssVariableValue("--bs-primary");
        var basesuccessColor = KTUtil.getCssVariableValue("--bs-success");
        var lightsuccessColor = KTUtil.getCssVariableValue("--bs-success");

        var options = {
            series: [
                {
                    name: "AKUMULASI RKAP",
                    type: "line",
                    data: [80, 55, 41, 64, 22, 43, 21],
                },
                {
                    name: "AKUMULASI REALISASI",
                    type: "line",
                    data: [100, 55, 41, 64, 22, 43, 21],
                },
                {
                    name: "AKUMULASI PROGNOSA",
                    type: "line",
                    data: [12, 55, 41, 64, 22, 43, 21],
                },
                {
                    name: "RKAP PERBULAN",
                    type: "column",
                    data: [53, 32, 33, 52, 13, 44, 32],
                },
                {
                    name: "REALISASI",
                    type: "column",
                    data: [53, 32, 33, 52, 13, 44, 32],
                },
                {
                    name: "PROGNOSA",
                    type: "column",
                    data: [53, 32, 33, 52, 13, 44, 32],
                },
            ],
            chart: {
                type: "bar",
                height: height,
                toolbar: {
                    show: false,
                },
            },
            plotOptions: {
                bar: {
                    horizontal: false,
                    dataLabels: {
                        position: "top",
                    },
                },
            },
            dataLabels: {
                enabled: false,
                offsetX: -6,
                style: {
                    fontSize: "12px",
                    colors: ["#fff"],
                },
            },
            stroke: {
                show: true,
                width: 1,
                colors: ["#1B366D", "#04CB04", "#FFCC06"],
            },
            tooltip: {
                shared: true,
                intersect: false,
            },
            xaxis: {
                categories: [
                    "GH01",
                    "GH02",
                    "GH03",
                    "GH04",
                    "SG",
                    "SP2",
                    "SP3",
                ],
            },
            legend: {
                show: true,
                position: "top",
                horizontalAlign: "left",
                fontSize: "10px",
                borderRadius: "50px",
                markers: {
                    radius: 50,
                },
            },
            colors: ["#1B366D", "#04CB04", "#FFCC06"],
        };
        chart.self = new ApexCharts(element, options);

        // Set timeout to properly get the parent elements width
        setTimeout(function () {
            chart.self.render();
            chart.rendered = true;
        }, 200);
    };

    // Public methods
    return {
        init: function () {
            initChart(chart);

            // Update chart on theme mode change
            KTThemeMode.on("kt.thememode.change", function () {
                if (chart.rendered) {
                    chart.self.destroy();
                }

                initChart(chart);
            });
        },
    };
})();

// Webpack support
if (typeof module !== "undefined") {
    module.exports = KTChartsKlinNetAvailability;
}

// On document ready
KTUtil.onDOMContentLoaded(function () {
    KTChartsKlinNetAvailability.init();
});

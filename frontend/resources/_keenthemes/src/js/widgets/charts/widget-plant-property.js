"use strict";

// Class definition
var KTChartsWidgePlantProperty = (function () {
    var chart = {
        self: null,
        rendered: false,
    };

    // Private methods
    var initChart = function (chart) {
        var element = document.getElementById("kt_charts_widget_plant_prop");

        if (!element) {
            return;
        }

        var height = parseInt(KTUtil.css(element, "height"));
        var options = {
            series: [44, 55, 41],
            chart: {
                type: "donut",
                height: height,
            },
            legend: {
                position: "bottom",
                horizontalAlign:'center',
            },
            colors: ["#15AB92", "#1B6372", "#A9D18E"],
            labels: [
                "Act. Rate",
                "Lose Rate UPTD",
                "Lose Rate IDLE",
            ],
            responsive: [
                {
                    breakpoint: 480,
                    options: {
                        chart: {
                            width: 200,
                        },
                        legend: {
                            position: "bottom",
                        },
                    },
                },
            ],
        };

        chart.self = new ApexCharts(element, options);

        // Set timeout to properly get the parent elements width
        setTimeout(function () {
            chart.self.render();
            chart.rendered = true;
        }, 200);
    };

    // Public methods
    return {
        init: function () {
            initChart(chart);

            // Update chart on theme mode change
            KTThemeMode.on("kt.thememode.change", function () {
                if (chart.rendered) {
                    chart.self.destroy();
                }

                initChart(chart);
            });
        },
    };
})();

// Webpack support
if (typeof module !== "undefined") {
    module.exports = KTChartsWidgePlantProperty;
}

// On document ready
KTUtil.onDOMContentLoaded(function () {
    KTChartsWidgePlantProperty.init();
});

"use strict";

// Class definition
var KTChartsSparePartOverhoulSembaranPengawasan = (function () {
    var chart = {
        self: null,
        rendered: false,
    };

    // Private methods
    var initChart = function (chart) {
        var element = document.getElementById(
            "kt_charts_widget_sparepart_overhoul_sembaran_pengawasan"
        );

        if (!element) {
            return;
        }

        var height = parseInt(KTUtil.css(element, "height"));
        var width = parseInt(KTUtil.css(element, "width"));

        var options = {
            series: [44, 55, 33],
            chart: {
                type: "donut",
                height: 210,
                width: width,
            },
            legend: {
                position: "right",
                horizontalAlign: "center",
            },
            colors: ["#084E67", "#3FAEB5", "#F66D00"],
            labels: [
                "Centralized Troubleshooting",
                "Section of Hygiene",
                "Section of Contruction",
            ],
            plotOptions: {
                pie: {
                    donut: {
                        size: "50%",
                    },
                },
            },
            grid: {
                padding: {
                    top: 20,
                    right: 0,
                    bottom: 0,
                    left: 0,
                },
            },
        };

        chart.self = new ApexCharts(element, options);

        // Set timeout to properly get the parent elements width
        setTimeout(function () {
            chart.self.render();
            chart.rendered = true;
        }, 200);
    };

    // Public methods
    return {
        init: function () {
            initChart(chart);

            // Update chart on theme mode change
            KTThemeMode.on("kt.thememode.change", function () {
                if (chart.rendered) {
                    chart.self.destroy();
                }

                initChart(chart);
            });
        },
    };
})();

// Webpack support
if (typeof module !== "undefined") {
    module.exports = KTChartsSparePartOverhoulSembaranPengawasan;
}

// On document ready
KTUtil.onDOMContentLoaded(function () {
    KTChartsSparePartOverhoulSembaranPengawasan.init();
});

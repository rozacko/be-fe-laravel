"use strict";

// Class definition
var KTChartsKlinNet2 = (function () {
    var chart = {
        self: null,
        rendered: false,
    };

    // Private methods
    var initChart = function (chart) {
        var element = document.getElementById("kt_charts_widget_klin_net_2");

        if (!element) {
            return;
        }

        var height = parseInt(KTUtil.css(element, "height"));

        var options = {
            series: [
                {
                    data: [400, 430, 448, 470],
                },
            ],
            chart: {
                type: "bar",
                height: height,
                toolbar: {
                    show: false,
                },
            },
            plotOptions: {
                bar: {
                    borderRadius: 0,
                    horizontal: true,
                    distributed: true,
                },
            },

            colors: ["#1B366D", "#FFCC06", "#027002"],
            dataLabels: {
                enabled: false,
            },
            legend: {
                show: false,
            },
            xaxis: {
                position: "bottom",
                categories: ["GHO1", "GHO2", "GHO3", "SG"],
            },
            yaxis: {
                show: false,
                labels: {
                    show: false,
                },
            },
        };
        chart.self = new ApexCharts(element, options);

        // Set timeout to properly get the parent elements width
        setTimeout(function () {
            chart.self.render();
            chart.rendered = true;
        }, 200);
    };

    // Public methods
    return {
        init: function () {
            initChart(chart);

            // Update chart on theme mode change
            KTThemeMode.on("kt.thememode.change", function () {
                if (chart.rendered) {
                    chart.self.destroy();
                }

                initChart(chart);
            });
        },
    };
})();

// Webpack support
if (typeof module !== "undefined") {
    module.exports = KTChartsKlinNet2;
}

// On document ready
KTUtil.onDOMContentLoaded(function () {
    KTChartsKlinNet2.init();
});

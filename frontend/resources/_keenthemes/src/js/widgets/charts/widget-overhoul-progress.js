"use strict";

// Class definition
var KTChartsOverhoulProgress = (function () {
    var chart = {
        self: null,
        rendered: false,
    };

    // Private methods
    var initChart = function (chart) {
        var element = document.getElementById(
            "kt_charts_widget_overhoul_progress"
        );

        if (!element) {
            return;
        }

        var height = parseInt(KTUtil.css(element, "height"));
        var width = parseInt(KTUtil.css(element, "width"));

        var options = {
            series: [
                {
                    data: [21, 22, 10, 40],
                },
            ],
            chart: {
                width: width,
                height: 360,
                type: "bar",
                events: {
                    click: function (chart, w, e) {
                        // console.log(chart, w, e)
                    },
                },
                toolbar: {
                    show: false,
                },
            },
            colors: ["#0579CC", "#042568", "#D26314", "#FFCC06"],
            plotOptions: {
                bar: {
                    columnWidth: "80%",
                    distributed: true,
                },
            },

            dataLabels: {
                enabled: true,
                formatter: function (val) {
                    return val + "%";
                },
                offsetY: -20,
                style: {
                    fontSize: "12px",
                    colors: ["#304758"],
                },
            },
            legend: {
                show: false,
            },
            yaxis: {
                axisBorder: {
                    show: false,
                },
                axisTicks: {
                    show: false,
                },
                labels: {
                    show: false,
                    formatter: function (val) {
                        return val + "%";
                    },
                },
            },
            xaxis: {
                categories: ["ACTIVITY", "SPAREPART", "FABRIKASI", "MANPOWER"],
                labels: {
                    style: {
                        // colors: colors,
                        fontSize: "7px",
                    },
                },
            },
        };
        chart.self = new ApexCharts(element, options);

        // Set timeout to properly get the parent elements width
        setTimeout(function () {
            chart.self.render();
            chart.rendered = true;
        }, 200);
    };

    // Public methods
    return {
        init: function () {
            initChart(chart);

            // Update chart on theme mode change
            KTThemeMode.on("kt.thememode.change", function () {
                if (chart.rendered) {
                    chart.self.destroy();
                }

                initChart(chart);
            });
        },
    };
})();

// Webpack support
if (typeof module !== "undefined") {
    module.exports = KTChartsOverhoulProgress;
}

// On document ready
KTUtil.onDOMContentLoaded(function () {
    KTChartsOverhoulProgress.init();
});

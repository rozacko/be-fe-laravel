"use strict";

// Class definition
var KTChartsSparepartNonRutinTypeThree = (function () {
    var chart = {
        self: null,
        rendered: false,
    };

    // Private methods
    var initChart = function (chart) {
        var element = document.getElementById(
            "kt_charts_sparepart_non_rutin_type_three"
        );

        if (!element) {
            return;
        }

        var height = parseInt(KTUtil.css(element, "height"));
        var labelColor = KTUtil.getCssVariableValue("--bs-gray-500");
        var borderColor = KTUtil.getCssVariableValue(
            "--bs-border-dashed-color"
        );
        var baseprimaryColor = KTUtil.getCssVariableValue("--bs-primary");
        var lightprimaryColor = KTUtil.getCssVariableValue("--bs-primary");
        var basesuccessColor = KTUtil.getCssVariableValue("--bs-success");
        var lightsuccessColor = KTUtil.getCssVariableValue("--bs-success");

        var options = {
            series: [
                {
                    name: "Estimator",
                    type: "column",
                    data: [53, 32, 33, 52],
                },
                {
                    name: "Enginer Status",
                    type: "column",
                    data: [53, 32, 33, 52],
                },
                {
                    name: "RFQ Status",
                    type: "column",
                    data: [53, 32, 33, 52],
                },
                {
                    name: "PO Status",
                    type: "column",
                    data: [53, 32, 33, 52],
                },
            ],
            chart: {
                type: "bar",
                height: 250,
                toolbar: {
                    show: false,
                },
            },
            plotOptions: {
                bar: {
                    horizontal: false,
                    barHeight: "80%",
                    columnWidth: "70%",
                    dataLabels: {
                        position: "top",
                    },
                },
            },
            dataLabels: {
                enabled: false,
                offsetX: -6,
                style: {
                    fontSize: "12px",
                    colors: ["#fff"],
                },
            },
            stroke: {
                show: true,
                width: 1,
                colors: ["#0072F0", "#3FAEB5", "#F66D00", "#FCB414"],
            },
            tooltip: {
                shared: true,
                intersect: false,
            },
            xaxis: {
                categories: [
                    "Elektrical",
                    "Mekanikal",
                    "Sipil",
                    "Pabrik Semen",
                ],
            },
            legend: {
                show: true,
                position: "top",
                horizontalAlign: "left",
                fontSize: "10px",
                borderRadius: "50px",
                markers: {
                    radius: 50,
                },
            },
            colors: ["#0072F0", "#3FAEB5", "#F66D00", "#FCB414"],
        };
        chart.self = new ApexCharts(element, options);

        // Set timeout to properly get the parent elements width
        setTimeout(function () {
            chart.self.render();
            chart.rendered = true;
        }, 200);
    };

    // Public methods
    return {
        init: function () {
            initChart(chart);

            // Update chart on theme mode change
            KTThemeMode.on("kt.thememode.change", function () {
                if (chart.rendered) {
                    chart.self.destroy();
                }

                initChart(chart);
            });
        },
    };
})();

// Webpack support
if (typeof module !== "undefined") {
    module.exports = KTChartsSparepartNonRutinTypeThree;
}

// On document ready
KTUtil.onDOMContentLoaded(function () {
    KTChartsSparepartNonRutinTypeThree.init();
});

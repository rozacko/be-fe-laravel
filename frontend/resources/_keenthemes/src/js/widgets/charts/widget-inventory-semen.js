"use strict";

// Class definition
var KtInventorySemen = (function () {
    var chart = {
        self: null,
        rendered: false,
    };

    // Private methods
    var initChart = function (chart) {
        var element = document.getElementById(
            "kt_inventory_semen"
        );

        if (!element) {
            return;
        }

        var height = parseInt(KTUtil.css(element, "height"));

        var options = {
          series: [{
          name: 'OPC PREM',
          color: 'rgb(233, 68, 69)',
          data: [44, 55, 41, 67, 22, 31]
        }, {
          name: 'OPC REG',
          color: 'rgb(91, 155, 213)',
          data: [13, 23, 20, 8, 13, 44]
        }, {
          name: 'PPC PREM',
          color: 'rgb(246, 167, 248)',
          data: [11, 17, 15, 15, 21, 24]
        }, {
          name: 'PPC REG',
          color: 'rgb(146, 208, 83)',
          data: [11, 17, 15, 15, 21, 42]
        }],
        dataLabels: {
          enabled: false
        },
          chart: {
          type: 'bar',
          height: height,
          stacked: true,
        //   stackType: '100%'
        },
                    responsive: [{
                    breakpoint: 480,
                    options: {

                    }
                    }],
                    xaxis: {
                        categories: ['TUBAN 1', 'TUBAN 2', 'TUBAN 3', 'TUBAN 4', 'TOTAL TUBAN', 'GRESIK'
                        ],
                    },
                    fill: {
                    opacity: 1,
                    type: "gradient"
                    },
                    legend: {
                        position: 'top',
                        horizontalAlign: 'left',
                        offsetX: 0
                    },
        };
        chart.self = new ApexCharts(element, options);

        // Set timeout to properly get the parent elements width
        setTimeout(function () {
            chart.self.render();
            chart.rendered = true;
        }, 200);
    };

    // Public methods
    return {
        init: function () {
            initChart(chart);

            // Update chart on theme mode change
            KTThemeMode.on("kt.thememode.change", function () {
                if (chart.rendered) {
                    chart.self.destroy();
                }

                initChart(chart);
            });
        },
    };
})();

// Webpack support
if (typeof module !== "undefined") {
    module.exports = KtInventorySemen;
}

// On document ready
KTUtil.onDOMContentLoaded(function () {
    KtInventorySemen.init();
});

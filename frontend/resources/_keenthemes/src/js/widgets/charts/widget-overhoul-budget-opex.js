"use strict";

// Class definition
var KTChartsBudgetOpex = (function () {
    var chart = {
        self: null,
        rendered: false,
    };

    // Private methods
    var initChart = function (chart) {
        var element = document.getElementById("kt_charts_widget_budget_opex");

        if (!element) {
            return;
        }

        var height = parseInt(KTUtil.css(element, "height"));
        var width = parseInt(KTUtil.css(element, "width"));

        var options = {
            series: [44, 55],
            chart: {
                type: "donut",
                height: height,
                // width: 260,
            },
            legend: {
                position: "bottom",
                horizontalAlign:'center',
            },
            colors: ["#5B9BD5", "#ED7D31"],
            labels: ["Not ready", "Ready"],
            plotOptions: {
                pie: {
                    donut: {
                        size: "50%",
                    },
                },
            },
            grid: {
                padding: {
                    top: 0,
                    right: 0,
                    bottom: 0,
                    left: 0,
                },
            },
        };

        chart.self = new ApexCharts(element, options);

        // Set timeout to properly get the parent elements width
        setTimeout(function () {
            chart.self.render();
            chart.rendered = true;
        }, 200);
    };

    // Public methods
    return {
        init: function () {
            initChart(chart);

            // Update chart on theme mode change
            KTThemeMode.on("kt.thememode.change", function () {
                if (chart.rendered) {
                    chart.self.destroy();
                }

                initChart(chart);
            });
        },
    };
})();

// Webpack support
if (typeof module !== "undefined") {
    module.exports = KTChartsBudgetOpex;
}

// On document ready
KTUtil.onDOMContentLoaded(function () {
    KTChartsBudgetOpex.init();
});

<div class="modal-header pb-0 border-0 justify-content-end">
    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">					
        <i class="fa fa-close" style="font-size:24px"></i>
    </div>
</div>
<div class="modal-body scroll-y px-10 px-lg-15 pt-0 pb-15">
    <div class="mb-13 text-center">
        <h1 class="mb-3">Create Data</h1>
        </div>
    <form method="POST" action="{{ route($route . '.store') }}" enctype="multipart/form-data" id="idForm">
        <div class="modal-body">
        @csrf   
            <div class="d-flex flex-column mb-8 fv-row form-group">            
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">Tahun</span>                    
                </label>
                <select id="select_tahun" class="form-select h-25 select_option" data-placeholder="Pilih Tahun" name="tahun">
                    <option value=""></option>
                    @foreach($tahun as $key => $value)                    
                        <option value="{{$value}}" {{$value == date("Y") ? "selected" : ""}}>{{$value}}</option>
                    @endforeach
                </select>
            </div>                
            <div class="d-flex flex-column mb-8 fv-row form-group">            
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">KPI / Kebijakan perusahaan</span>
                </label>
                <input type="text" class="form-control" placeholder="Enter Kebijakan" name="kebijakan" />
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">            
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">Satuan</span>
                </label>
                <input type="text" class="form-control" placeholder="Enter Satuan" name="satuan" />
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">            
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">Target</span>
                </label>
                <input type="number" placeholder="0.00" name="target" min="0" step="0.01" pattern="^\d+(?:\.\d{1,2})?$" />
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">            
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">Realisasi</span>
                </label>
                <input type="number" placeholder="0.00" name="realisasi" min="0" step="0.01" pattern="^\d+(?:\.\d{1,2})?$" />
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">            
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">%KPI</span>
                </label>
                <input type="number" placeholder="0.00" name="kpi" min="0" step="0.01" pattern="^\d+(?:\.\d{1,2})?$" />
            </div>            
        </div> 
        <div class="modal-footer">
            <div class="text-center">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary">
                    <span class="indicator-label">Submit</span>
                    <span class="indicator-progress">Please wait... 
                    <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                </button>
            </div>
        </div>
    </form>
</div>


<script type="text/javascript">
     $(".select_option").select2({
        dropdownParent: $('#modal')
    });
    $("#idForm").submit(function(e) {
    e.preventDefault(); // avoid to execute the actual submit of the form.
    var form = $(this);
    var actionUrl = form.attr('action');
        $.ajax({
            type: "POST",
            url: actionUrl,
            data: form.serialize(), 
            success: function(data)
            {
                if(data.status == 'success'){
                    Swal.fire(
                        'Success',
                        data.message,
                        'success'
                    );
                    $('#modal').modal('hide');
                    $('#tb_kpi_proper').DataTable().ajax.reload();
                }else{
                    Swal.fire(
                        'Warning',
                        data.message,
                        'warning'
                    );
                }
            }
        });
    });
</script>

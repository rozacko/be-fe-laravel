<div class="modal-header pb-0 border-0 justify-content-end">
    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">					
        <i class="fa fa-close" style="font-size:24px"></i>
    </div>
</div>
<div class="modal-body scroll-y px-10 px-lg-15 pt-0 pb-15">
    <div class="mb-13 text-center">
        <h1 class="mb-3">Create Data</h1>
    </div>
    <form method="POST" action="{{ route($route . '.store_file') }}" enctype="multipart/form-data" id="idFormFile">
        <div class="modal-body">
        @csrf   
            <div class="d-flex flex-column mb-8 fv-row form-group">            
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="requi#009ef7">Area</span>
                </label>
                <input type="text" class="form-control " name="id_perizinan" value="{{$id}}" hidden/>
                <select id="filter_tahun" class="form-select h-25 select_file" data-placeholder="Pilih Area" name="area">
                    <option value="Pabrik Tuban">Pabrik Tuban</option>
                    <option value="Pabrik Gresik">Pabrik Gresik</option>
                    <option value="Pelabuhan Tuban">Pelabuhan Tuban</option>
                    <option value="Pelabuhan Gresik">Pelabuhan Gresik</option>
                    <option value="Tambang Tuban">Tambang Tuban</option>
                </select>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">            
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="requi#009ef7">No Izin</span>
                </label>
                <input type="text" class="form-control" placeholder="Enter No Perizinan" name="no_izin" />
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">            
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="requi#009ef7">Nama Penerbit</span>
                </label>
                <input type="text" class="form-control" placeholder="Enter Nama Penerbit" name="penerbit" />
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">            
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="requi#009ef7">Masa Berlaku</span>
                </label>
                <input type="date" class="form-control" name="masa_berlaku" />
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">                            
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">Document</span>
                </label>
                <p class="mt-2" style="color: #1F7793;">*format file .pdf .ppt</p>
                <input type="file" id="default_file" name="upload_file" accept="application/pdf, application/ppt" style="display: flex !important;"/>
                <input type="text" id="file" name="file" hidden/>                
            </div>
        </div>
        <div class="modal-footer">
            <div class="text-center">
                <button type="submit" id="submit_" class="btn btn-primary">
                    <span class="indicator-label">Tambah Data</span>
                    <span class="indicator-progress">Please wait... 
                    <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                </button>
            </div>
        </div>
    </form>
    <table class="table table-borde#009ef7 data-table" id="tb_izin_ling_file">
        <thead>
            <tr>
            <th>No</th>
            <th>Area</th>            
            <th>No Izin</th>
            <th>Penerbit</th>
            <th>Masa Berlaku</th>
            <th>Dokumen</th>            
            <th>Status</th>            
            <th>Action</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    <div class="modal-footer">
        <div class="text-center">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>                
        </div>
    </div>
</div>


<script type="text/javascript">
    $(".select_file").select2({
        dropdownParent: $('#modal2')
    });
    $(function() {
        var table = $('#tb_izin_ling_file').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route($route.'.table_file', $id) }}",
            columns: [{ 
                data: 'DT_RowIndex', 
                name: 'DT_RowIndex', 
                orderable: false, 
                searchable: false },
                {
                data: 'area',
                name: 'area'
                },
                {
                data: 'no_izin',
                name: 'no_izin'
                },
                {
                data: 'penerbit',
                name: 'penerbit'
                },
                {
                data: 'masa_berlaku',
                name: 'masa_berlaku'
                },
                {
                data: 'files',
                render : function(data, type, row, meta) {
                    return '<a href="'+data+'" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-download" style="padding-right: 0px; font-size:10;"></i></a> ';
                },
                },
                {
                data: 'status',
                render : function(data, type, row, meta) {
                    if(data == 1)
                        return '<center><button class="btn btn-warning btn-sm" type="button" style="color: black;" disabled>Will be Expired</button></center>';                    
                    else if(data == 2)
                        return '<center><button class="btn btn-danger btn-sm" type="button" style="color: white;" disabled>Expired</button></center>';                    
                    else
                        return '<center><button class="btn btn-success btn-sm" type="button" style="color: white;" disabled>Active</button></center>';                    
                },
                width: 120
                },
                {
                data: 'action',
                name: 'action',
                orderable: false,
                searchable: false,
                width: 150
                },
            ],
        });
    });

    $("#idFormFile").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.
        var form = $(this);
        var actionUrl = form.attr('action');
        $.ajax({
            type: "POST",
            url: actionUrl,
            data: form.serialize(), 
            success: function(data)
            {
                if(data.status == 'success'){
                    Swal.fire(
                        'Success',
                        data.message,
                        'success'
                    );
                    $('#tb_izin_ling').DataTable().ajax.reload();
                    $('#tb_izin_ling_file').DataTable().ajax.reload();
                }else{
                    Swal.fire(
                        'Warning',
                        data.message,
                        'warning'
                    );
                }
            }
        });
    });

    function btn_delete_file(id){
        event.preventDefault(); 
        var url = '{{ route($route.".destroy_file", ":id") }}';
        url = url.replace(':id', id);    
        Swal.fire({
            icon: 'warning',
            title: 'Apakah anda yakin?',
            text: "Data ini akan di hapus!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak',
            confirmButtonClass: 'btn btn-primary margin-right-10',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: true,
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function(isConfirm) {
            if (isConfirm.isConfirmed) {
                $.post(url, {_token: "{{ csrf_token() }}", _method: 'DELETE' }, function(res) {
                    if (res.status == 'success') {
                        Swal.fire(
                            'Success',
                            res.message,
                            'success'
                        );                    
                        $('#tb_izin_ling_file').DataTable().ajax.reload();
                        $('#tb_izin').DataTable().ajax.reload();
                    } else {
                        Swal.fire(
                            'Gagal',
                            res.message,
                            'error'
                        );
                    }
                }, 'json');
            } else {
                Swal.fire(
                    'Cancelled',
                    'Your data is safe',
                    'error'
                );
            }            
        });

        return false;
    }

    $("#default_file").change(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form. 
        var input = this;
        var files = $(this)[0].files;
        var fd = new FormData();
        // Append data 
        fd.append('upload_file',files[0]);
        fd.append('_token', "{{ csrf_token() }}");
        var actionUrl = "{{ route($route . '.upload_file') }}";
        $.ajax({
            type: "POST",
            url: actionUrl,
            enctype: 'multipart/form-data',
            data: fd,
            cache:false,
            contentType: false,
            processData: false, 
            success: function(data)
            {
                if(data.status == 'success'){
                    Swal.fire(
                        'Success',
                        data.message,
                        'success'
                    );
                    $("#file").val(data.data);
                }else{
                    Swal.fire(
                        'Warning',
                        data.message,
                        'warning'
                    );
                }
            }
        });
    });
</script>

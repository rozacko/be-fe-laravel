<div class="modal-header pb-0 border-0 justify-content-end">
    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">					
        <i class="fa fa-close" style="font-size:24px"></i>
    </div>
</div>
<div class="modal-body scroll-y px-10 px-lg-15 pt-0 pb-15">
    <div class="mb-13 text-center">
        <h1 class="mb-3">Create Data</h1>
    </div>
    <form method="POST" action="{{ route($route . '.store') }}" enctype="multipart/form-data" id="idForm">
        <div class="modal-body">        
        @csrf
            <div class="d-flex flex-column mb-8 fv-row form-group">            
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">Tahun</span>                    
                </label>
                <select id="select_tahun" class="form-select h-25 select_option" data-placeholder="Pilih Plant" name="tahun">
                    <option value=""></option>
                    @foreach($tahun as $key => $value)                    
                        <option value="{{$value}}" {{$value == date("Y") ? "selected" : ""}}>{{$value}}</option>
                    @endforeach
                </select>
            </div> 
            <div class="d-flex flex-column mb-8 fv-row form-group">            
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">Jenis Peraturan Perundangan & Persyaratan Lain</span>                    
                </label>
                <input type="text" class="form-control" placeholder="Enter Jenis" name="jenis" />
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">            
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">Nomor Peraturan Perundangan & Persyaratan Lain</span>                    
                </label>
                <input type="text" class="form-control" placeholder="Enter Nomor" name="nomor" />
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">            
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">Peraturan Perundang-undangan</span>                    
                </label>
                <textarea name="peraturan" cols="30" rows="2" class="wd-100 form-control form-control-lg"></textarea>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    Aspek Lingkungan
                </label>
                <div class="row">
                    <div class="col-6">        
                        <label class="form-check form-check-custom">                            
                            <input class="form-check-input" type="checkbox" name="emisi_udara"/>&nbsp;Emisi ke Udara
                        </label>
                    </div>
                    <div class="col-6">
                        <label class="form-check form-check-custom">                            
                            <input class="form-check-input" type="checkbox" name="pembuangan_air"/>&nbsp;Pembuangan ke Air
                        </label>
                    </div>
                    <div class="col-6">
                        <label class="form-check form-check-custom">                            
                            <input class="form-check-input" type="checkbox" name="pembuangan_tanah"/>&nbsp;Pembuangan ke Tanah
                        </label>
                    </div>
                    <div class="col-6">
                        <label class="form-check form-check-custom">
                            <input class="form-check-input" type="checkbox" name="sda"/>&nbsp;Penggunaan Bahan Baku & SDA
                        </label>
                    </div>
                    <div class="col-6">
                        <label class="form-check form-check-custom">
                            <input class="form-check-input" type="checkbox" name="penggunaan_energi"/>&nbsp;Penggunaan Energi
                        </label>
                    </div>
                    <div class="col-6">
                        <label class="form-check form-check-custom">
                            <input class="form-check-input" type="checkbox" name="pancaran_energi"/>&nbsp;Pancaran Energi
                        </label>
                    </div>
                    <div class="col-6">
                        <label class="form-check form-check-custom">
                            <input class="form-check-input" type="checkbox" name="limbah"/>&nbsp;Limbah
                        </label>
                    </div>
                    <div class="col-6">
                        <label class="form-check form-check-custom">
                            <input class="form-check-input" type="checkbox" name="atribut_fisik"/>&nbsp;Atribut Fisik
                        </label>
                    </div>
                </div>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="form-check form-check-custom">                            
                    <input class="form-check-input" type="checkbox" name="k3"/>&nbsp;K3
                </label>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <div class="row">
                    <div class="col-6">
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Ketaatan</span>
                        </label>                        
                        <label class="form-check form-check-custom flex-stack">                            
                            <input class="form-check-input" type="radio" value="1" name="taat"/>
                            <span class="form-check-label text-gray-700 fs-6 fw-semibold ms-0 me-2">Ya</span>
                            <input class="form-check-input" type="radio" value="0" name="taat"/>
                            <span class="form-check-label text-gray-700 fs-6 fw-semibold ms-0 me-2">Tidak</span>
                            <input class="form-check-input" type="radio" value="2" name="taat"/>
                            <span class="form-check-label text-gray-700 fs-6 fw-semibold ms-0 me-2">Tidak ada</span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">            
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    Rekomendasi/ Keterangan
                </label>
                <textarea name="keterangan" cols="30" rows="2" class="wd-100 form-control form-control-lg"></textarea>
            </div>
        </div> 
        <div class="modal-footer">
            <div class="text-center">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary">
                    <span class="indicator-label">Submit</span>
                    <span class="indicator-progress">Please wait... 
                    <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                </button>
            </div>
        </div>
    </form>
</div>


<script type="text/javascript">
     $(".select_option").select2({
        dropdownParent: $('#modal')
    });
    $("#idForm").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form. 
        var formData = new FormData(this);
        var form = $(this);
        var actionUrl = form.attr('action');
        $.ajax({
            type: "POST",
            url: actionUrl,
            enctype: 'multipart/form-data',
            data: formData,
            cache:false,
            contentType: false,
            processData: false, 
            success: function(data)
            {
                if(data.status == 'success'){
                    Swal.fire(
                        'Success',
                        data.message,
                        'success'
                    );
                    $('#modal').modal('hide');
                    $('#tb_matrik_ling').DataTable().ajax.reload();
                }else{
                    Swal.fire(
                        'Warning',
                        data.message,
                        'warning'
                    );
                }
            }
        });
    });
</script>

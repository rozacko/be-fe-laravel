<div class="modal-header pb-0 border-0 justify-content-end">
    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">					
        <i class="fa fa-close" style="font-size:24px"></i>
    </div>
</div>
<div class="modal-body scroll-y px-10 px-lg-15 pt-0 pb-15">
    <div class="mb-13 text-center">
        <h1 class="mb-3">Update Data</h1>
    </div>
    <form action="{{ route($route . '.update', $data->id) }}" enctype="multipart/form-data" id="idForm">
        <div class="modal-body">        
        @csrf 
            <div class="d-flex flex-column mb-8 fv-row form-group">            
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">Tahun</span>
                </label>
                <select id="select_tahun0" class="form-select h-25 select_option" data-placeholder="Pilih Tahun" name="tahun">
                    <option value=""></option>
                    @foreach($tahun as $key => $value)                    
                        <option value="{{$value}}" {{$value == $data->tahun ? "selected" : ""}}>{{$value}}</option>
                    @endforeach
                </select>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">            
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">Nama Sertifikat/Penghargaan</span>                    
                </label>
                <input type="text" class="form-control" placeholder="Enter Nama Sertifikat/Penghargaan" name="nm_sertifikat" value="{{ $data->nm_sertifikat }}"/>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">            
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">Nama Penerbit</span>                    
                </label>
                <input type="text" class="form-control" placeholder="Enter Nama Penerbit" name="nm_penerbit" value="{{ $data->nm_penerbit }}"/>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">            
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    Tanggal Expired
                </label>
                <input type="date" class="form-control" name="end_date" value="{{ $data->end_date }}"/>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">                            
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">Document</span>
                </label>
                <p class="mt-2" style="color: #1F7793;">*format file .pdf .ppt</p>
                <input type="file" id="default_file" name="upload_file" accept="application/pdf, application/ppt" style="display: flex !important;"/>
                <input type="text" id="file" name="file" value="{{ $data->file }}" hidden/>
            </div>
        </div> 
        <div class="modal-footer">
            <div class="text-center">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary">
                    <span class="indicator-label">Submit</span>
                    <span class="indicator-progress">Please wait... 
                    <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                </button>
            </div>
        </div>
    </form>
</div>


<script type="text/javascript">
     $(".select_option").select2({
        dropdownParent: $('#modal')
    });
    $("#idForm").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.
        var form = $(this);
        var actionUrl = form.attr('action');
        $.ajax({
            type: "patch",
            url: actionUrl,
            data: form.serialize(), 
            success: function(data)
            {
                if(data.status == 'success'){
                    Swal.fire(
                        'Success',
                        data.message,
                        'success'
                    );
                    $('#modal').modal('hide');
                    $('#tb_sertif_ling').DataTable().ajax.reload();
                }else{
                    Swal.fire(
                        'Warning',
                        data.message,
                        'warning'
                    );
                }
            }
        });
    });

    $("#default_file").change(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form. 
        var input = this;
        var files = $(this)[0].files;
        var fd = new FormData();
        // Append data 
        fd.append('upload_file',files[0]);
        fd.append('_token', "{{ csrf_token() }}");
        var actionUrl = "{{ route($route . '.upload_file') }}";
        $.ajax({
            type: "POST",
            url: actionUrl,
            enctype: 'multipart/form-data',
            data: fd,
            cache:false,
            contentType: false,
            processData: false, 
            success: function(data)
            {
                if(data.status == 'success'){
                    Swal.fire(
                        'Success',
                        data.message,
                        'success'
                    );
                    $("#file").val(data.data);
                }else{
                    Swal.fire(
                        'Warning',
                        data.message,
                        'warning'
                    );
                }
            }
        });
    });
</script>
<div class="modal-header pb-0 border-0 justify-content-end">
    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">					
        <i class="fa fa-close" style="font-size:24px"></i>
    </div>
</div>
<div class="modal-body scroll-y px-10 px-lg-15 pt-0 pb-15">
    <div class="mb-13 text-center">
        <h1 class="mb-3">Create Data</h1>
    </div>
    <form method="POST" action="{{ route($route . '.store_is') }}" enctype="multipart/form-data" id="idFormIs">
        <div class="modal-body">
        @csrf   
            <div class="d-flex flex-column mb-8 fv-row form-group">            
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="requi#009ef7">Inisiatif Strategi</span>
                </label>
                <textarea name="inisiatif_strategis" cols="30" rows="2" class="wd-100 form-control form-control-lg"></textarea>
                <input type="text" class="form-control " name="id_program" value="{{$id}}" hidden/>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">            
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="requi#009ef7">Start Date</span>
                </label>
                <select id="select_bulan1" class="form-select h-25" data-placeholder="Pilih Bulan" name="start_date">
                    <option value=""></option>
                    @foreach($bulan as $key => $value)                    
                        <option value="{{$value['data']}}" {{$value['data'] == date("m") ? "selected" : ""}}>{{$value['desc']}}</option>
                    @endforeach
                </select>                
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">            
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="requi#009ef7">End Date</span>
                </label>
                <select id="select_bulan2" class="form-select h-25" data-placeholder="Pilih Bulan" name="end_date">
                    <option value=""></option>
                    @foreach($bulan as $key => $value)                    
                        <option value="{{$value['data']}}" {{$value['data'] == date("m") ? "selected" : ""}}>{{$value['desc']}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="modal-footer">
            <div class="text-center">
                <button type="submit" id="submit_is" class="btn btn-primary">
                    <span class="indicator-label">Tambah Data</span>
                    <span class="indicator-progress">Please wait... 
                    <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                </button>
            </div>
        </div>
    </form>
    <table class="table table-borde#009ef7 data-table" id="tb_program_ling_is">
        <thead>
            <tr>
            <th>No</th>
            <th>Inisiatif Strategi</th>            
            <th>Jan</th>
            <th>Feb</th>
            <th>Mar</th>
            <th>Apr</th>
            <th>Mei</th>
            <th>Jun</th>
            <th>Jul</th>
            <th>Aug</th>
            <th>Sep</th>
            <th>Okt</th>
            <th>Nov</th>
            <th>Des</th>
            <th>Action</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    <div class="modal-footer">
        <div class="text-center">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>                
        </div>
    </div>
</div>


<script type="text/javascript">
    $(".select_option").select2({
        dropdownParent: $('#modal')
    });
    $(function() {
        var table = $('#tb_program_ling_is').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route($route.'.table_is', $id) }}",
            columns: [{ 
                data: 'DT_RowIndex', 
                name: 'DT_RowIndex', 
                orderable: false, 
                searchable: false },
                {
                data: 'inisiatif_strategis',
                name: 'inisiatif_strategis'
                },               
                {
                data: 'jan',
                width: 20,
                render : function(data, type, row, meta) {    
                        if(data == '1')
                            return "<div style='background-color: #009ef7;color: #009ef7;width: 25px;'>"+00+"</div>";
                        else
                            return "";
                    }
                },                
                {
                data: 'feb',
                width: 20,
                render : function(data, type, row, meta) {    
                        if(data == '1')
                            return "<div style='background-color: #009ef7;color: #009ef7;width: 25px;'>"+00+"</div>";
                        else
                            return "";
                    }
                },
                {
                data: 'mar',
                width: 20,
                render : function(data, type, row, meta) {    
                        if(data == '1')
                            return "<div style='background-color: #009ef7;color: #009ef7;width: 25px;'>"+00+"</div>";
                        else
                            return "";
                    }
                },
                {
                data: 'apr',
                width: 20,
                render : function(data, type, row, meta) {    
                        if(data == '1')
                            return "<div style='background-color: #009ef7;color: #009ef7;width: 25px;'>"+00+"</div>";
                        else
                            return "";
                    }
                },
                {
                data: 'mei',
                width: 20,
                render : function(data, type, row, meta) {    
                        if(data == '1')
                            return "<div style='background-color: #009ef7;color: #009ef7;width: 25px;'>"+00+"</div>";
                        else
                            return "";
                    }
                },
                {
                data: 'jun',
                width: 20,
                render : function(data, type, row, meta) {    
                        if(data == '1')
                            return "<div style='background-color: #009ef7;color: #009ef7;width: 25px;'>"+00+"</div>";
                        else
                            return "";
                    }
                },
                {
                data: 'jul',
                width: 20,
                render : function(data, type, row, meta) {    
                        if(data == '1')
                            return "<div style='background-color: #009ef7;color: #009ef7;width: 25px;'>"+00+"</div>";
                        else
                            return "";
                    }
                },
                {
                data: 'aug',
                width: 20,
                render : function(data, type, row, meta) {    
                        if(data == '1')
                            return "<div style='background-color: #009ef7;color: #009ef7;width: 25px;'>"+00+"</div>";
                        else
                            return "";
                    }
                },
                {
                data: 'sep',
                width: 20,
                render : function(data, type, row, meta) {    
                        if(data == '1')
                            return "<div style='background-color: #009ef7;color: #009ef7;width: 25px;'>"+00+"</div>";
                        else
                            return "";
                    }
                },
                {
                data: 'okt',
                width: 20,
                render : function(data, type, row, meta) {    
                        if(data == '1')
                            return "<div style='background-color: #009ef7;color: #009ef7;width: 25px;'>"+00+"</div>";
                        else
                            return "";
                    }
                },
                {
                data: 'nov',
                width: 20,
                render : function(data, type, row, meta) {    
                        if(data == '1')
                            return "<div style='background-color: #009ef7;color: #009ef7;width: 25px;'>"+00+"</div>";
                        else
                            return "";
                    }
                },
                {
                data: 'des',
                width: 20,
                render : function(data, type, row, meta) {    
                        if(data == '1')
                            return "<div style='background-color: #009ef7;color: #009ef7;width: 25px;'>"+00+"</div>";
                        else
                            return "";
                    }
                },
                {
                data: 'action',
                name: 'action',
                orderable: false,
                searchable: false,
                width: 150
                },
            ],
        });
    });

    $("#idFormIs").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.
        var form = $(this);
        var actionUrl = form.attr('action');
        $.ajax({
            type: "POST",
            url: actionUrl,
            data: form.serialize(), 
            success: function(data)
            {
                if(data.status == 'success'){
                    Swal.fire(
                        'Success',
                        data.message,
                        'success'
                    );
                    $('#tb_program_ling_is').DataTable().ajax.reload();
                }else{
                    Swal.fire(
                        'Warning',
                        data.message,
                        'warning'
                    );
                }
            }
        });
    });

    function btn_delete_is(id){
        event.preventDefault(); 
        var url = '{{ route($route.".destroy_is", ":id") }}';
        url = url.replace(':id', id);    
        Swal.fire({
            icon: 'warning',
            title: 'Apakah anda yakin?',
            text: "Data ini akan di hapus!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak',
            confirmButtonClass: 'btn btn-primary margin-right-10',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: true,
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function(isConfirm) {
            if (isConfirm.isConfirmed) {
                $.post(url, {_token: "{{ csrf_token() }}", _method: 'DELETE' }, function(res) {
                    if (res.status == 'success') {
                        Swal.fire(
                            'Success',
                            res.message,
                            'success'
                        );                    
                        $('#tb_program_ling_is').DataTable().ajax.reload();
                        $('#tb_program_ling').DataTable().ajax.reload();
                    } else {
                        Swal.fire(
                            'Gagal',
                            res.message,
                            'error'
                        );
                    }
                }, 'json');
            } else {
                Swal.fire(
                    'Cancelled',
                    'Your data is safe',
                    'error'
                );
            }            
        });

        return false;
    }
</script>

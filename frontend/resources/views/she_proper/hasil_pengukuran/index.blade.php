{{-- Extends layout --}}
<x-default-layout>
<div class="flex-stack mb-0 mb-lg-n4 pt-5">
    <div class="row">
        <div class="col-sm-4">
            <div class="information">
                    <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{$pagetitle}}</h1>                       
                    <ul class="breadcrumb breadcrumb-separatorless ">
                    @foreach($breadcrumb as $key => $value)
                        @if ($key === array_key_first($breadcrumb)) 
                            <li class="breadcrumb-item text-muted">{{$key . ' /'}}
                            </li>
                        @endif

                        @if ($key === array_key_last($breadcrumb)) 
                            <li class="breadcrumb-item text-ghopo">   
                                <a href="{{$value}}" class="text-ghopo text-hover-primary">{{$key}}</a>
                            </li>
                        @endif                            
                    @endforeach                        
                </ul>
            </div>
        </div>
        <div class="col-sm-8 d-flex justify-content-md-end flex-no-wrap px-5">
        </div>
    </div>
</div>
<div class="flex-stack mb-0 mb-lg-n4 pt-5">
    <div class="row">
        <!--begin::Chart widget 22-->
        <div class="card h-xl-100" style="border: none !important;">
            <!--begin::Header-->
            <div class="card-header position-relative py-0 border-bottom-2">
                <!--begin::Nav-->
                <ul class="nav nav-stretch nav-pills nav-pills-custom d-flex mt-3">
                    <!--begin::Item-->
                    <li class="nav-item p-0 ms-0 me-8">
                        <!--begin::Link-->
                        <a class="nav-link btn btn-color-muted active px-0" data-bs-toggle="tab" id="kt_chart_widgets_22_tab_1" href="#kt_chart_widgets_22_tab_content_1">
                            <!--begin::Subtitle-->
                            <span class="nav-text fw-semibold fs-4 mb-3">Dashboard</span>
                            <!--end::Subtitle-->
                            <!--begin::Bullet-->
                            <span class="bullet-custom position-absolute z-index-2 w-100 h-2px top-100 bottom-n100 bg-primary rounded"></span>
                            <!--end::Bullet-->
                        </a>
                        <!--end::Link-->
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="nav-item p-0 ms-0 me-8">
                        <!--begin::Link-->
                        <a class="nav-link btn btn-color-muted px-0" data-bs-toggle="tab" id="kt_chart_widgets_22_tab_2" href="#kt_chart_widgets_22_tab_content_2">
                            <!--begin::Subtitle-->
                            <span class="nav-text fw-semibold fs-4 mb-3">Data List</span>
                            <!--end::Subtitle-->
                            <!--begin::Bullet-->
                            <span class="bullet-custom position-absolute z-index-2 w-100 h-2px top-100 bottom-n100 bg-primary rounded"></span>
                            <!--end::Bullet-->
                        </a>
                        <!--end::Link-->
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="nav-item p-0 ms-0 me-8">
                        <!--begin::Link-->
                        <a class="nav-link btn btn-color-muted px-0" data-bs-toggle="tab" id="kt_chart_widgets_22_tab_2" href="#kt_chart_widgets_22_tab_content_3">
                            <!--begin::Subtitle-->
                            <span class="nav-text fw-semibold fs-4 mb-3">Approval</span>
                            <!--end::Subtitle-->
                            <!--begin::Bullet-->
                            <span class="bullet-custom position-absolute z-index-2 w-100 h-2px top-100 bottom-n100 bg-primary rounded"></span>
                            <!--end::Bullet-->
                        </a>
                        <!--end::Link-->
                    </li>
                    <!--end::Item-->
                </ul>
                <!--end::Nav-->
                <!--begin::Toolbar-->
                <div class="card-toolbar">
                </div>
                <!--end::Toolbar-->
            </div>
            <!--end::Header-->
            <!--begin::Body-->
            <div class="card-body pb-3">
                <!--begin::Tab Content-->
                <div class="tab-content">
                    <!--begin::Tap pane-->
                    <div class="tab-pane fade show active" id="kt_chart_widgets_22_tab_content_1">
                        <!--begin::Wrapper-->
                        <div class="flex-wrap flex-md-nowrap">
                        <div class="row">
                            <div class="card card-flush mt-6 mt-xl-9">
                                <div class="py-lg-5 p-3">
                                    <div class="row">
                                        <div class="col-md-3"></div>
                                        <div class="col-md-9">
                                            <div class="">
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <select id="filter_objekukur" class="form-select h-25 select_filter" data-placeholder="Pilih Objek Pengukuran" name="objekukur">
                                                            <option value="Pilih Objek Pengukuran">Pilih Objek Pengukuran</option>
                                                            <!-- @foreach($tahun as $key => $value)                    
                                                                <option value="{{$value}}" {{$value == date("Y") ? "selected" : ""}}>{{$value}}</option>
                                                            @endforeach -->
                                                        </select>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <select id="filter_periode" class="form-select h-25 select_filter" data-placeholder="Pilih Periode" name="periode">
                                                            <option value="Pilih Periode">Pilih Periode</option>
                                                            <!-- @foreach($tahun as $key => $value)                    
                                                                <option value="{{$value}}" {{$value == date("Y") ? "selected" : ""}}>{{$value}}</option>
                                                            @endforeach -->
                                                        </select>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <select id="filter_bulan" class="form-select h-25 select_filter" data-placeholder="Pilih Bulan" name="bulan">
                                                            <option value="Pilih Bulan">Pilih Bulan</option>
                                                            <!-- @foreach($tahun as $key => $value)                    
                                                                <option value="{{$value}}" {{$value == date("Y") ? "selected" : ""}}>{{$value}}</option>
                                                            @endforeach -->
                                                        </select>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <select id="filter_tahun" class="form-select h-25 select_filter" data-placeholder="Pilih Tahun" name="tahun">
                                                            <option value="Pilih Tahun">Pilih Tahun</option>
                                                            <!-- @foreach($tahun as $key => $value)                    
                                                                <option value="{{$value}}" {{$value == date("Y") ? "selected" : ""}}>{{$value}}</option>
                                                            @endforeach -->
                                                        </select>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <button class="btn btn-history w-100" id="get_data">Tampilkan</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body pt-0" style="width:100%">
                                    <div class="row">
                                        <div class="card card-klin-net-availability-2 ">
                                            <div class="card-header">
                                                <span>DASHBOARD PER PARAMETER</span>
                                            </div>
                                            <div class="card-body">
                                                <!-- <div id="kt_charts_widget_klin_net_availability_2" class="charts"></div> -->
                                                <div id="kt_charts_dashboard_parameter" class="charts"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                        <!--end::Wrapper-->
                    </div>
                    <!--end::Tap pane-->
                    <!--begin::Tap pane-->
                    <div class="tab-pane fade" id="kt_chart_widgets_22_tab_content_2">
                        <!--begin::Wrapper-->
                        <div class="flex-wrap flex-md-nowrap">
                            <div class="card card-flush mt-6 mt-xl-9">
                                <div class="card-header p-3">
                                    <div class="card-title flex-column">			
                                    </div>
                                    <div class="card-toolbar my-1">		        
                                        <button type="button" class="btn btn-sm btn-primary float-right btn-add">
                                            Tambah Data
                                        </button>
                                    </div>
                                </div>
                                <div class="card-body pt-0">
                                    <table class="table table-bordered data-table" id="tb_data_list" style="width:100%">
                                        <thead>
                                            <tr>
                                            <th>No</th>
                                            <th>TANGGAL</th>
                                            <th>OBJEK PENGUKURAN</th>
                                            <th>AREA</th>
                                            <th>LAB UJI</th>
                                            <th>HASIL</th>
                                            <th>STATUS</th>
                                            <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                        <!--end::Wrapper-->
                    </div>
                    <!--end::Tap pane-->
                    <!--begin::Tap pane-->
                    <div class="tab-pane fade" id="kt_chart_widgets_22_tab_content_3">
                        <!--begin::Wrapper-->
                        <div class="flex-wrap flex-md-nowrap">
                            <div class="card card-flush mt-6 mt-xl-9">
                                <div class="card-header p-3">
                                    <div class="card-title flex-column">			
                                    </div>
                                    <div class="card-toolbar my-1">			        
                                        <button type="button" class="btn btn-sm btn-primary float-right btn-add" style="margin: 10px; background-color: #2AC371 !important;">
                                            Approve All
                                        </button>	        
                                        <button type="button" class="btn btn-sm btn-danger float-right btn-add" style="margin: 10px; background-color: #F5333F !important;">
                                            Reject All
                                        </button>
                                    </div>
                                </div>
                                <div class="card-body pt-0">
                                    <table class="table table-bordered data-table" id="tb_approval" style="width:100%">
                                        <thead>
                                            <tr>
                                            <th>No</th>
                                            <th>TANGGAL</th>
                                            <th>OBJEK PENGUKURAN</th>
                                            <th>AREA</th>
                                            <th>LAB UJI</th>
                                            <th>HASIL</th>
                                            <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                        <!--end::Wrapper-->
                    </div>
                    <!--end::Tap pane-->
                </div>
                <!--end::Tab Content-->
            </div>
            <!--end: Card Body-->
        </div>
        <!--end::Chart widget 22-->
    </div>
</div>

<div class="modal fade" id="modal" tabindex="-1" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered mw-650px">
		<div class="modal-content rounded"></div>
	</div>
</div>

</x-default-layout>
<script>    

    var tahun = $("#filter_tahun").val();
    var url = '{{ route($route.".table", [":tahun"]) }}';
    url = url.replace(':tahun', tahun);
    var tablelist = $('#tb_data_list').DataTable({
        processing: true,
        serverSide: true,
        scrollX: true,
        ajax: url,
        columns: [{ 
                data: 'DT_RowIndex', 
                name: 'DT_RowIndex', 
                orderable: false, 
                searchable: false 
                },
            {
            data: 'tahun',
            name: 'tahun'
            },
            {
            data: 'jenis',
            name: 'jenis'
            },
            {
            data: 'nomor',
            name: 'nomor'
            },
            {
            data: 'peraturan',
            name: 'peraturan',            
            },
            {
                data: 'roles',
                name: 'roles',
                orderable: false,
                searchable: false,
                render: function(data, type, row) {
                    let roles = '';
                    if (data.length > 0) {
                        data.forEach(function myFunction(item, index, arr) {
                            roles +=
                                `<span class="badge badge-success">${item.name}</span> `
                        })
                    }
                    return roles;
                }
            },
            {
                data: 'roles',
                name: 'roles',
                orderable: false,
                searchable: false,
                render: function(data, type, row) {
                    let roles = '';
                    if (data.length > 0) {
                        data.forEach(function myFunction(item, index, arr) {
                            roles +=
                                `<span class="badge badge-success">${item.name}</span> `
                        })
                    }
                    return roles;
                }
            },
            {
                data: 'uuid',
                name: 'uuid',
                orderable: false,
                searchable: false,
                width: 150,
                render: function(data, type, row) {
                    let button =
                        `<a href="#" onclick="show('${data}')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>&nbsp;
                            <a href="#" onclick="btn_delete('${data}')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>`;
                    return button;
                }
            },
        ]
    });

    var tableappr = $('#tb_approval').DataTable({
        processing: true,
        serverSide: true,
        scrollX: true,
        ajax: url,
        columns: [{ 
                data: 'DT_RowIndex', 
                name: 'DT_RowIndex', 
                orderable: false, 
                searchable: false 
                },
            {
            data: 'tahun',
            name: 'tahun'
            },
            {
            data: 'jenis',
            name: 'jenis'
            },
            {
            data: 'nomor',
            name: 'nomor'
            },
            {
            data: 'peraturan',
            name: 'peraturan',            
            },{
                data: 'roles',
                name: 'roles',
                orderable: false,
                searchable: false,
                render: function(data, type, row) {
                    let roles = '';
                    if (data.length > 0) {
                        data.forEach(function myFunction(item, index, arr) {
                            roles +=
                                `<span class="badge badge-success">${item.name}</span> `
                        })
                    }
                    return roles;
                }
            },
            {
                data: 'uuid',
                name: 'uuid',
                orderable: false,
                searchable: false,
                width: 150,
                render: function(data, type, row) {
                    let button =
                        `<a href="#" onclick="search('${data}')" class="btn btn-info btn-sm"><i class="fa fa-search"></i></a>&nbsp;
                            <a href="#" onclick="btn_approve('${data}')" class="btn btn-primary btn-sm"><i class="fa fa-check"></i></a>&nbsp;
                            <a href="#" onclick="btn_reject('${data}')" class="btn btn-danger btn-sm"><i class="fa fa-times"></i></a>`;
                    return button;
                }
            },
        ]
    });
    
    $(function() {
        $(".select_filter").select2();        
        getdatatable();
    });  
    setTimeout(function(){
        if ($('#alertNotif').length > 0) {
            $('#alertNotif').remove();
        }
    }, 5000) 

    function getdatatable(){        
        table.ajax.url(url).load();
    }

    $('.btn-add').click(function (e) {
        e.preventDefault();
        var url = "{{ route($route.'.create') }}";
        $.get(url, function (html) {
            $('#modal .modal-content').html(html);
            $('#modal').modal('show');            
        }).fail(function () {
            Swal.fire(
                'Warning',
                'Something error',
                'warning'
            );
        });
    })

    $('#get_data').click(function (e) {
        e.preventDefault(); 
        var tahun = $("#filter_tahun").val();
        var url = '{{ route($route.".table", [":tahun"]) }}';
        url = url.replace(':tahun', tahun);
        table.ajax.url(url).load();
    })

    function show(id){
        event.preventDefault(); 
        var url = '{{ route($route.".edit", ":id") }}';
        url = url.replace(':id', id);  
        $.get(url, function (html) {
            $('#modal .modal-content').html(html);
            $('#modal').modal('show');
        }).fail(function () {
            Swal.fire(
                'Warning',
                'Something error',
                'warning'
            );
        });          
    }

    function btn_delete(id){
        event.preventDefault(); 
        var url = '{{ route($route.".destroy", ":id") }}';
        url = url.replace(':id', id);    
        Swal.fire({
            icon: 'warning',
            title: 'Apakah anda yakin?',
            text: "Data ini akan di hapus!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak',
            confirmButtonClass: 'btn btn-primary margin-right-10',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: true,
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function(isConfirm) {
            if (isConfirm.isConfirmed) {
                $.post(url, {_token: "{{ csrf_token() }}", _method: 'DELETE' }, function(res) {
                    if (res.status == 'success') {
                        Swal.fire(
                            'Success',
                            res.message,
                            'success'
                        );                    
                        $('#tb_matrik_ling').DataTable().ajax.reload();
                    } else {
                        Swal.fire(
                            'Gagal',
                            res.message,
                            'error'
                        );
                    }
                }, 'json');
            } else {
                Swal.fire(
                    'Cancelled',
                    'Your data is safe',
                    'error'
                );
            }        
        });

        return false;
    }

    // Class definition
    var KTChartsDashboardParameter = (function () {
        var chart = {
            self: null,
            rendered: false,
        };

        // Private methods
        var initChart = function (chart) {
            var element = document.getElementById(
                "kt_charts_dashboard_parameter"
            );

            if (!element) {
                return;
            }

            var height = parseInt(KTUtil.css(element, "height"));

            var options = {
                series: [                
                    {
                        name: "NILAI",
                        type: "column",
                        color: "#042568",
                        data: [2000, 1500, 2100, 3400, 1200, 2300, 5000, 3500, 2100, 3400, 1200, 2300, 2000, 1500, 2100, 3400, 1200, 5000, 3500, 2100, 3400, 1200, 2300, 1100, 1100],
                    },
                    {
                        name: "BAKU MUTU",
                        type: "line",
                        color: "#0099CC",
                        data: [4000, 3500, 4100, 6400, 2200, 4300, 10000, 5500, 4100, 6400, 2200, 4300, 4000, 3500, 4100, 6400, 2200, 10000, 5500, 4100, 6400, 2200, 4300, 2100, 2100],
                    },
                
                ],
                chart: {
                    type: "bar",
                    height: 500,
                    toolbar: {
                        show: false,
                    },
                },
                plotOptions: {
                    bar: {
                        horizontal: false,
                        dataLabels: {
                            position: "top",
                        },
                    },
                },
                dataLabels: {
                    enabled: false,
                    offsetX: -6,
                    style: {
                        fontSize: "12px",
                        colors: ["#fff"],
                    },
                },
                stroke: {
                    show: true,
                    width: 1,
                    // colors: [ "#042568"],
                },
                tooltip: {
                    shared: true,
                    intersect: false,
                },
                xaxis: {
                    categories: [
                        "Kekeruhan",
                        "Warna",
                        "TDS",
                        "Suhu",
                        "Besi",
                        "Florida",
                        "CaCO2",
                        "Mangan",
                        "Nitrat",
                        "Nitrit",
                        "Sianida",
                        "MBAS",
                        "Air Raksa",
                        "Arsen",
                        "Kadmium",
                        "Cr VI",
                        "Selenium",
                        "Seng",
                        "Sulfat",
                        "Timbah",
                        "KMnO4",
                        "Pestisida Total",
                        "Benzene",
                        "Total Coliform",
                        "E. Coli",
                    ],
                },
                legend: {
                    show: true,
                    position: "top",
                    horizontalAlign: "left",
                    fontSize: "10px",
                    borderRadius: "50px",
                    markers: {
                        radius: 50,
                    },
                },
                colors: ["#1B366D", "#04CB04", "#FFCC06"],
            };
            chart.self = new ApexCharts(element, options);

            // Set timeout to properly get the parent elements width
            setTimeout(function () {
                chart.self.render();
                chart.rendered = true;
            }, 200);
        };

        // Public methods
        return {
            init: function () {
                initChart(chart);

                // Update chart on theme mode change
                KTThemeMode.on("kt.thememode.change", function () {
                    if (chart.rendered) {
                        chart.self.destroy();
                    }

                    initChart(chart);
                });
            },
        };
    })();

    // Webpack support
    if (typeof module !== "undefined") {
        module.exports = KTChartsDashboardParameter;
    }

    // On document ready
    KTUtil.onDOMContentLoaded(function () {
        KTChartsDashboardParameter.init();
    });
</script>
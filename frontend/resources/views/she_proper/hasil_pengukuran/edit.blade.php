<div class="modal-header pb-0 border-0 justify-content-end">
    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">					
        <i class="fa fa-close" style="font-size:24px"></i>
    </div>
</div>
<div class="modal-body scroll-y px-10 px-lg-15 pt-0 pb-15">
    <div class="mb-13 text-center">
        <h1 class="mb-3">Update Data</h1>
    </div>
    <form action="{{ route($route . '.update', $data->id) }}" enctype="multipart/form-data" id="idForm">
        <div class="modal-body">        
        @csrf 
            <div class="d-flex flex-column mb-8 fv-row form-group">            
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">Tahun</span>                    
                </label>
                <select id="select_tahun" class="form-select h-25 select_option" data-placeholder="Pilih Plant" name="tahun">
                    <option value=""></option>
                    @foreach($tahun as $key => $value)                    
                        <option value="{{$value}}" {{$value == $data->tahun ? "selected" : ""}}>{{$value}}</option>
                    @endforeach
                </select>
            </div> 
            <div class="d-flex flex-column mb-8 fv-row form-group">            
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">Tahun</span>                    
                </label>
                <select id="select_tahun" class="form-select h-25 select_option" data-placeholder="Pilih Plant" name="tahun">
                    <option value=""></option>
                    @foreach($tahun as $key => $value)                    
                        <option value="{{$value}}" {{$value == date("Y") ? "selected" : ""}}>{{$value}}</option>
                    @endforeach
                </select>
            </div> 
            <div class="d-flex flex-column mb-8 fv-row form-group">            
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">Jenis Peraturan Perundangan & Persyaratan Lain</span>                    
                </label>
                <input type="text" class="form-control" placeholder="Enter Jenis" name="jenis" value="{{ $data->jenis }}"/>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">            
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">Nomor Peraturan Perundangan & Persyaratan Lain</span>                    
                </label>
                <input type="text" class="form-control" placeholder="Enter Nomor" name="nomor" value="{{ $data->nomor }}"/>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">            
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">Peraturan Perundang-undangan</span>                    
                </label>
                <textarea name="peraturan" cols="30" rows="2" class="wd-100 form-control form-control-lg">{{ $data->peraturan }}</textarea>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    Aspek Lingkungan
                </label>
                <div class="row">
                    <div class="col-6">        
                        <label class="form-check form-check-custom">                            
                            <input class="form-check-input" type="checkbox" name="emisi_udara" {{$data->emisi_udara == '1' ? "checked" : ""}}/>&nbsp;Emisi ke Udara
                        </label>
                    </div>
                    <div class="col-6">
                        <label class="form-check form-check-custom">                            
                            <input class="form-check-input" type="checkbox" name="pembuangan_air" {{$data->pembuangan_air == '1' ? "checked" : ""}}/>&nbsp;Pembuangan ke Air
                        </label>
                    </div>
                    <div class="col-6">
                        <label class="form-check form-check-custom">                            
                            <input class="form-check-input" type="checkbox" name="pembuangan_tanah" {{$data->pembuangan_tanah == '1' ? "checked" : ""}}/>&nbsp;Pembuangan ke Tanah
                        </label>
                    </div>
                    <div class="col-6">
                        <label class="form-check form-check-custom">
                            <input class="form-check-input" type="checkbox" name="sda" {{$data->sda == '1' ? "checked" : ""}}/>&nbsp;Penggunaan Bahan Baku & SDA
                        </label>
                    </div>
                    <div class="col-6">
                        <label class="form-check form-check-custom">
                            <input class="form-check-input" type="checkbox" name="penggunaan_energi" {{$data->penggunaan_energi == '1' ? "checked" : ""}}/>&nbsp;Penggunaan Energi
                        </label>
                    </div>
                    <div class="col-6">
                        <label class="form-check form-check-custom">
                            <input class="form-check-input" type="checkbox" name="pancaran_energi" {{$data->pancaran_energi == '1' ? "checked" : ""}}/>&nbsp;Pancaran Energi
                        </label>
                    </div>
                    <div class="col-6">
                        <label class="form-check form-check-custom">
                            <input class="form-check-input" type="checkbox" name="limbah" {{$data->limbah == '1' ? "checked" : ""}}/>&nbsp;Limbah
                        </label>
                    </div>
                    <div class="col-6">
                        <label class="form-check form-check-custom">
                            <input class="form-check-input" type="checkbox" name="atribut_fisik" {{$data->atribut_fisik == '1' ? "checked" : ""}}/>&nbsp;Atribut Fisik
                        </label>
                    </div>
                </div>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="form-check form-check-custom">                            
                    <input class="form-check-input" type="checkbox" name="k3" {{$data->k3 == '1' ? "checked" : ""}}/>&nbsp;K3
                </label>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <div class="row">
                    <div class="col-6">
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Ketaatan</span>
                        </label>                        
                        <label class="form-check form-check-custom flex-stack">                            
                            <input class="form-check-input" type="radio" value="1" name="taat" {{$data->taat == '1' ? "checked" : ""}}/>
                            <span class="form-check-label text-gray-700 fs-6 fw-semibold ms-0 me-2">Ya</span>
                            <input class="form-check-input" type="radio" value="0" name="taat" {{$data->taat == '0' ? "checked" : ""}}/>
                            <span class="form-check-label text-gray-700 fs-6 fw-semibold ms-0 me-2">Tidak</span>
                            <input class="form-check-input" type="radio" value="2" name="taat" {{$data->taat == '2' ? "checked" : ""}}/>
                            <span class="form-check-label text-gray-700 fs-6 fw-semibold ms-0 me-2">Tidak ada</span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">            
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    Rekomendasi/ Keterangan
                </label>
                <textarea name="keterangan" cols="30" rows="2" class="wd-100 form-control form-control-lg">{{ $data->keterangan }}</textarea>
            </div>            
        </div> 
        <div class="modal-footer">
            <div class="text-center">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary">
                    <span class="indicator-label">Submit</span>
                    <span class="indicator-progress">Please wait... 
                    <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                </button>
            </div>
        </div>
    </form>
</div>


<script type="text/javascript">
     $(".select_option").select2({
        dropdownParent: $('#modal')
    });
    $("#idForm").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.
        var form = $(this);
        var actionUrl = form.attr('action');
        $.ajax({
            type: "patch",
            url: actionUrl,
            data: form.serialize(), 
            success: function(data)
            {
                if(data.status == 'success'){
                    Swal.fire(
                        'Success',
                        data.message,
                        'success'
                    );
                    $('#modal').modal('hide');
                    $('#tb_matrik_ling').DataTable().ajax.reload();
                }else{
                    Swal.fire(
                        'Warning',
                        data.message,
                        'warning'
                    );
                }
            }
        });
    });
</script>
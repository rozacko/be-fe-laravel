<div class="modal-header pb-0 border-0 justify-content-end">
    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">					
        <i class="fa fa-close" style="font-size:24px"></i>
    </div>
</div>
<div class="modal-body scroll-y px-10 px-lg-15 pt-0 pb-15">
    <div class="mb-13 text-center">
        <h1 class="mb-3">Update Data</h1>
    </div>
    <form action="{{ route($route . '.update', $data->id) }}" enctype="multipart/form-data" id="idForm">
        <div class="modal-body">        
        @csrf 
        <div class="d-flex flex-column mb-8 fv-row form-group">            
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">Tahun</span>                    
                </label>
                <select id="select_tahun" class="form-select h-25 select_option" data-placeholder="Pilih Plant" name="tahun">
                    <option value=""></option>
                    @foreach($tahun as $key => $value)                    
                    <option value="{{$value}}" {{$value == $data->tahun ? "selected" : ""}}>{{$value}}</option>
                    @endforeach
                </select>
            </div>             
            <div class="d-flex flex-column mb-8 fv-row form-group">            
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">Nama Perizinan</span>                    
                </label>
                <select id="select_izin" class="form-select h-25 select_option" data-placeholder="Pilih Izin" name="nm_perizinan">
                    <option value="Izin Pengelolaan Limbah B3" {{ $data->nm_perizinan == 'Izin Pengelolaan Limbah B3' ? "selected" : ""}}>Izin Pengelolaan Limbah B3</option>
                    <option value="Izin Pengelolaan Limbah Non B3" {{ $data->nm_perizinan == 'Izin Pengelolaan Limbah Non B3' ? "selected" : ""}}>Izin Pengelolaan Limbah Non B3</option>
                    <option value="Izin Pembuangan Emisi" {{ $data->nm_perizinan == 'Izin Pembuangan Emisi' ? "selected" : ""}}>Izin Pembuangan Emisi</option>
                    <option value="Izin Pembuangan Air Limbah" {{ $data->nm_perizinan == 'Izin Pembuangan Air Limbah' ? "selected" : ""}}>Izin Pembuangan Air Limbah</option>
                </select>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">Sub Perizinan</span>
                </label>
                <input type="text" class="form-control" placeholder="Enter Perizinan" name="sub_perizinan" value="{{ $data->sub_perizinan }}"/>
            </div>            
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <div class="row">
                    <div class="col-6">
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Pabrik Tuban</span>
                        </label>                        
                        <label class="form-check form-check-custom flex-stack">                            
                            <input class="form-check-input" type="radio" value="1" name="p_tuban" {{$data->p_tuban == '1' ? "checked" : ""}} />
                            <span class="form-check-label text-gray-700 fs-6 fw-semibold ms-0 me-2">Ada</span>
                            <input class="form-check-input" type="radio" value="0" name="p_tuban" {{$data->p_tuban == '0' ? "checked" : ""}} />
                            <span class="form-check-label text-gray-700 fs-6 fw-semibold ms-0 me-2">Tidak Ada</span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <div class="row">
                    <div class="col-6">
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Pabrik Gresik</span>
                        </label>                        
                        <label class="form-check form-check-custom flex-stack">                            
                            <input class="form-check-input" type="radio" value="1" name="p_gresik" {{$data->p_gresik == '1' ? "checked" : ""}} />
                            <span class="form-check-label text-gray-700 fs-6 fw-semibold ms-0 me-2">Ada</span>
                            <input class="form-check-input" type="radio" value="0" name="p_gresik" {{$data->p_gresik == '0' ? "checked" : ""}} />
                            <span class="form-check-label text-gray-700 fs-6 fw-semibold ms-0 me-2">Tidak Ada</span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <div class="row">
                    <div class="col-6">
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Pelabuhan Tuban</span>
                        </label>                        
                        <label class="form-check form-check-custom flex-stack">                            
                            <input class="form-check-input" type="radio" value="1" name="pelabuhan_tuban" {{$data->pelabuhan_tuban == '1' ? "checked" : ""}} />
                            <span class="form-check-label text-gray-700 fs-6 fw-semibold ms-0 me-2">Ada</span>
                            <input class="form-check-input" type="radio" value="0" name="pelabuhan_tuban" {{$data->pelabuhan_tuban == '0' ? "checked" : ""}} />
                            <span class="form-check-label text-gray-700 fs-6 fw-semibold ms-0 me-2">Tidak Ada</span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <div class="row">
                    <div class="col-6">
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Pelabuhan Gresik</span>                            
                        </label>                        
                        <label class="form-check form-check-custom flex-stack">                            
                            <input class="form-check-input" type="radio" value="1" name="pelabuhan_gresik" {{$data->pelabuhan_gresik == '1' ? "checked" : ""}} />
                            <span class="form-check-label text-gray-700 fs-6 fw-semibold ms-0 me-2">Ada</span>
                            <input class="form-check-input" type="radio" value="0" name="pelabuhan_gresik" {{$data->pelabuhan_gresik == '0' ? "checked" : ""}} />
                            <span class="form-check-label text-gray-700 fs-6 fw-semibold ms-0 me-2">Tidak Ada</span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <div class="row">
                    <div class="col-6">
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Tambang Tuban</span>
                        </label>                        
                        <label class="form-check form-check-custom flex-stack">                            
                        <input class="form-check-input" type="radio" value="1" name="tambang_tuban" {{$data->tambang_tuban == '1' ? "checked" : ""}} />
                        <span class="form-check-label text-gray-700 fs-6 fw-semibold ms-0 me-2">Ada</span>
                        <input class="form-check-input" type="radio" value="0" name="tambang_tuban" {{$data->tambang_tuban == '0' ? "checked" : ""}} />
                        <span class="form-check-label text-gray-700 fs-6 fw-semibold ms-0 me-2">Tidak Ada</span>
                    </label>
                    </div>
                </div>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">            
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    Keterangan
                </label>
                <textarea name="keterangan" cols="30" rows="2" class="wd-100 form-control form-control-lg">{{ $data->keterangan }}</textarea>
            </div>
        </div> 
        <div class="modal-footer">
            <div class="text-center">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary">
                    <span class="indicator-label">Submit</span>
                    <span class="indicator-progress">Please wait... 
                    <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                </button>
            </div>
        </div>
    </form>
</div>


<script type="text/javascript">
     $(".select_option").select2({
        dropdownParent: $('#modal')
    });
    $("#idForm").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.
        var form = $(this);
        var actionUrl = form.attr('action');
        $.ajax({
            type: "patch",
            url: actionUrl,
            data: form.serialize(), 
            success: function(data)
            {
                if(data.status == 'success'){
                    Swal.fire(
                        'Success',
                        data.message,
                        'success'
                    );
                    $('#modal').modal('hide');
                    $('#tb_izin_ling').DataTable().ajax.reload();
                }else{
                    Swal.fire(
                        'Warning',
                        data.message,
                        'warning'
                    );
                }
            }
        });
    });
</script>
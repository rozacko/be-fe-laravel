<x-default-layout>
    <div class="flex-stack mb-0 mb-lg-n4 pt-5">
        <div class="row">
            <div class="col-sm-3">
                <div class="information">
                    <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">TPM Report</h1>
                        <ul class="breadcrumb breadcrumb-separatorless ">
							<li class="breadcrumb-item text-muted">
								<a href="/" class="text-muted text-hover-primary">TPM Report</a>
							</li>
							<li class="breadcrumb-item text-muted">/</li>
							<li class="breadcrumb-item text-ghopo">{{ $title }}</li>
                        </ul>
                </div>
            </div>
            <div class="col-sm-9 d-flex justify-content-md-end flex-no-wrap px-5">
                <div class="filter">
                    <div class="row">
                        <div class="col mt-2 dropdown-filter">
                            <select id="filter_organisasi" class="form-select h-25" data-control="select2" data-placeholder="Pilih Organisasi">
                                <option value="all">Pilih Organisasi</option>
                            </select>
                        </div>
                        <div class="col mt-2 dropdown-filter">
                            <div id="loader_filter_unit_kerja" class="flex-grow-1 ml-1" >
                                <select id="filter_unit_kerja" class="form-select h-25" data-control="select2" data-placeholder="Pilih Unit Kerja">
                                    <option value="all">Pilih Unit Kerja</option>
                                </select>
                            </div>
                        </div>
                        <div class="col mt-2 dropdown-filter">
                            <select id="filter_year" class="form-select h-25" data-control="select2" data-placeholder="Pilih Tahun">
                                @foreach(getYear() as $key => $value)
                                    @if ($value == date("Y") )
                                        <option value="{{$value}}" selected>{{$value}}</option>
                                    @else
                                        <option value="{{$value}}">{{$value}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col mt-2">
                            <button class="btn btn-primary" id="filter-btn">Tampilkan</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card card-flush mt-6 mt-xl-9">

        <div class="card-body mt-6 pt-0">
            <h3>Laporan KPI Member TPM GHOPO</h3>
            <table class="table table-bordered data-table" id="tb_laporan_kpi_member_tpm">
                <thead>
                    <tr>
                        <th class="align-middle text-center color-header-tabel">No</th>
                        <th class="align-middle text-center color-header-tabel">Nomor Pegawai</th>
                        <th class="align-middle text-center color-header-tabel">Nama Pegawai</th>
                        <th class="align-middle text-center color-header-tabel">Nama Organisasi</th>
                        <th class="align-middle text-center color-header-tabel">Unit Kerja</th>
                        <th class="align-middle text-center color-header-tabel">Target Point</th>
                        <th class="align-middle text-center color-header-tabel">Q1</th>
                        <th class="align-middle text-center color-header-tabel">Q2</th>
                        <th class="align-middle text-center color-header-tabel">S1</th>
                        <th class="align-middle text-center color-header-tabel">Q3</th>
                        <th class="align-middle text-center color-header-tabel">Q4</th>
                        <th class="align-middle text-center color-header-tabel">S2</th>
                        <th class="align-middle text-center color-header-tabel"><span id="th_tahun">Tahun</span></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>

</x-default-layout>
<script>
    $(document).ready(function() {
        document.getElementById("th_tahun").innerHTML = $("#filter_year").val();
        getFilterOrganisasi();
    });

    $('#filter-btn').on('click',function() {
        document.getElementById("th_tahun").innerHTML = $("#filter_year").val();
        table.draw();
    })

    function getFilterOrganisasi() {
        $.ajax({
            type: "GET",
            url: "{{ route($route.'.FilterOrganisasi') }}",
            dataType: 'JSON',
            beforeSend: function () {
            },
            success: function (data) {

                $.each(data.data, function (i, item) {
                    var newOption = new Option(item.nama_organisasi, item.nama_organisasi, false, false);
                    $('#filter_organisasi').append(newOption).trigger('change');
                });

            },
            error: function (xmlhttprequest, textstatus, message) {}
        });
    }

    $('#filter_organisasi').on('select2:closing',function() {
        let organisasi = $(this).val()
        getUnitKerjabyOrganisasi(organisasi)
    })

    function getUnitKerjabyOrganisasi(organisasi) {
        $.ajax({
            type: "GET",
            url: "{{ route($route.'.FilterUnitKerja') }}",
            data: {
                organisasi: organisasi
            },
            dataType: 'JSON',
            beforeSend: function () {
                $('#filter_unit_kerja').attr('disabled', true);
                $("#loader_filter_unit_kerja").LoadingOverlay("show");
            },
            success: function (data) {
                $('#filter_unit_kerja').empty();
                $('#filter_unit_kerja').append(new Option("Pilih Unit Kerja", "all", false, false)).trigger('change');
                $.each(data.data, function (i, item) {
                    var newOption = new Option(item.unit_kerja, item.unit_kerja, false, false);
                    $('#filter_unit_kerja').append(newOption).trigger('change');
                });
            },
            error: function (xmlhttprequest, textstatus, message) {}
            }).always(function(){
                $('#filter_unit_kerja').attr('disabled', false);
                $("#loader_filter_unit_kerja").LoadingOverlay("hide");
        });
    }

        // data dumy
    var data = [
                    {
                        'no':'1',
                        'nomer_pegawai':'0345679875',
                        'nama_pegawai':'Dwi Kumara W.',
                        'nama_organisasi':'Programmer',
                        'unit_kerja':'IT',
                        'target_point':'1234',
                        'Q1':'1234',
                        'Q2':'1234',
                        'S1':'1234',
                        'Q3':'1234',
                        'Q4':'1234',
                        'S2':'1234',
                        'tahun':'2023'
                    },
                    {
                        'no':'2',
                        'nomer_pegawai':'0345679876',
                        'nama_pegawai':'Moch. Yusqi',
                        'nama_organisasi':'Programmer',
                        'unit_kerja':'IT',
                        'target_point':'1234',
                        'Q1':'1234',
                        'Q2':'1234',
                        'S1':'1234',
                        'Q3':'1234',
                        'Q4':'1234',
                        'S2':'1234',
                        'tahun':'2023'
                    }
    ];

    var table = $('#tb_laporan_kpi_member_tpm').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            "url": "{{ route($route.'.table') }}",
            'type': 'GET',
            'dataType': 'JSON',
            'data': function(d){
                d.organisasi= $("#filter_organisasi").val(),
                d.unit_kerja= $("#filter_unit_kerja").val(),
                d.tahun= $("#filter_year").val()
            }
        },
        responsive: false,
        scrollX: true,
        ordering: false,
        // filter:false,
        // paging: false,
        // data: data,
        columns: [{
            data: 'DT_RowIndex',
            name: 'no',
            searchable: false
            },
            {
            data: 'no_pegawai',
            name: 'no_pegawai'
            },
            {
            data: 'nama_pegawai',
            name: 'nama_pegawai'
            },
            {
            data: 'nama_organisasi',
            name: 'nama_organisasi'
            },
            {
            data: 'unit_kerja',
            name: 'unit_kerja',
            className: 'text-center'
            },
            {
            data: 'target_point',
            name: 'target_point',
            className: 'text-center',
            searchable: false
            },
            {
            data: 'q1',
            name: 'q1',
            className: 'text-center',
            searchable: false
            },
            {
            data: 'q2',
            name: 'q2',
            className: 'text-center',
            searchable: false
            },
            {
            data: 's1',
            name: 's1',
            className: 'text-center',
            searchable: false
            },
            {
            data: 'q3',
            name: 'q3',
            className: 'text-center',
            searchable: false
            },
            {
            data: 'q4',
            name: 'q4',
            className: 'text-center',
            searchable: false
            },
            {
            data: 's2',
            name: 's2',
            className: 'text-center',
            searchable: false
            },
            {
            data: 'nilai_akhir',
            name: 'nilai_akhir',
            className: 'text-center',
            searchable: false
            },
        ]
        ,
        rowCallback: function(row, data, index){
            var indexToRemove = $(row).parent().index();
            if(data['target_point']){
                $(row).find('td:eq(5)').css('color', '#C40008');
            }
            if(data['S1']){
                $(row).find('td:eq(8)').css('background-color', '#528BD5');
            }
            if(data['S2']){
                $(row).find('td:eq(11)').css('background-color', '#8FCF50');
            }
            if(data['tahun']){
                $(row).find('td:eq(12)').css('background-color', '#FFFF00');
            }
        }
    });

</script>

<x-default-layout>
    <div class="flex-stack mb-0 mb-lg-n4 pt-5">
        <div class="row">
            <div class="col-sm-3">
                <div class="information">
                    <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">TPM Report</h1>
                        <ul class="breadcrumb breadcrumb-separatorless ">
							<li class="breadcrumb-item text-muted">
								<a href="/" class="text-muted text-hover-primary">TPM Report</a>
							</li>
							<li class="breadcrumb-item text-muted">/</li>
							<li class="breadcrumb-item text-ghopo">{{$pagetitle}}</li>
                        </ul>
                </div>
            </div>
            <div class="col-sm-9 d-flex justify-content-md-end flex-no-wrap px-5">
                <div class="filter">
                    <div class="row">
                        {{-- <div class="col mt-2">
                            <select class="form-select h-25" data-control="select2" data-placeholder="Pilih Parameter" id="parameter_filter" name="parameter_filter">
                                <option value="All">Pilih Parameter</option>
                                @foreach(getParameter() as $key => $value)
                                    <option value="{{$value['value']}}">{{$value['value']}}</option>
                                @endforeach
                            </select>
                        </div> --}}
                        <div class="col mt-2">
                            <select class="form-select h-25" data-control="select2" data-placeholder="Pilih Tahun" id="filter_year" name="filter_year">
                                @foreach(getYear() as $key => $value)
                                    @if ($value == date("Y") )
                                        <option value="{{$value}}" selected>{{$value}}</option>
                                    @else
                                        <option value="{{$value}}">{{$value}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col mt-2 card-breadcrumb-button ">
                            <button class="btn btn-primary" id="filter-btn">Tampilkan</button>
                        </div>
                        <div class="col mt-2 card-breadcrumb-button">
                            <button id="btn-add" class="btn btn-primary btn-add">Upload</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card card-flush mt-6 mt-xl-9">
        <div class="card-body mt-6 pt-0">
            <h3>{{$pagetitle}}</h3>
            <table class="table table-bordered data-table" id="tb_data-dashboard-tpm">
                <thead>
                    <tr>
                        <th class="align-middle text-center color-header-tabel">Deskripsi</th>
                        <th class="align-middle text-center color-header-tabel">Parameter</th>
                        <th class="align-middle text-center color-header-tabel">Januari</th>
                        <th class="align-middle text-center color-header-tabel">Februari</th>
                        <th class="align-middle text-center color-header-tabel">Maret</th>
                        <th class="align-middle text-center color-header-tabel">Q1</th>
                        <th class="align-middle text-center color-header-tabel">April</th>
                        <th class="align-middle text-center color-header-tabel">Mei</th>
                        <th class="align-middle text-center color-header-tabel">Juni</th>
                        <th class="align-middle text-center color-header-tabel">Q2</th>
                        <th class="align-middle text-center color-header-tabel">S1</th>
                        <th class="align-middle text-center color-header-tabel">Juli</th>
                        <th class="align-middle text-center color-header-tabel">Agustus</th>
                        <th class="align-middle text-center color-header-tabel">September</th>
                        <th class="align-middle text-center color-header-tabel">Q3</th>
                        <th class="align-middle text-center color-header-tabel">Oktober</th>
                        <th class="align-middle text-center color-header-tabel">November</th>
                        <th class="align-middle text-center color-header-tabel">Desember</th>
                        <th class="align-middle text-center color-header-tabel">Q4</th>
                        <th class="align-middle text-center color-header-tabel">S2</th>
                        <th class="align-middle text-center color-header-tabel"><span id="th_tahun">Tahun</span></th>
                        {{-- <th class="align-middle text-center color-header-tabel">Total Deskripsi</th> --}}
                        {{-- <th class="align-middle text-center color-header-tabel">Action</th> --}}
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>

    <div class="modal fade" id="modal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered mw-650px">
            <div class="modal-content rounded"></div>
        </div>
    </div>

</x-default-layout>
<script>

    $(document).ready(function() {
        document.getElementById("th_tahun").innerHTML = $("#filter_year").val();
    });

    $('#filter-btn').on('click',function() {
        document.getElementById("th_tahun").innerHTML = $("#filter_year").val();
        table.draw();
    })
        // data dumy
        // var data = [
        //     {
        //         'Deskripsi':'SCORE TPM IMPLEMENTATION',
        //         'Parameter':'Target (poin)',
        //         'Januari':'0',
        //         'Februari':'0',
        //         'Maret':'0',
        //         'Q1':'0',
        //         'April':'0',
        //         'Mei':'0',
        //         'Juni':'0',
        //         'Q2':'0',
        //         'S1':'0',
        //         'Juli':'0',
        //         'Agustus':'0',
        //         'September':'0',
        //         'Q3':'0',
        //         'Oktober':'0',
        //         'November':'0',
        //         'Desember':'0',
        //         'Q4':'0',
        //         'S2':'0',
        //         'Tahun':'0',
        //         'Total_Deskripsi':'3',
        //     },
        //     {
        //         'Deskripsi':'SCORE TPM IMPLEMENTATION',
        //         'Parameter':'Realisasi (poin)',
        //         'Januari':'0',
        //         'Februari':'0',
        //         'Maret':'0',
        //         'Q1':'0',
        //         'April':'0',
        //         'Mei':'0',
        //         'Juni':'0',
        //         'Q2':'0',
        //         'S1':'0',
        //         'Juli':'0',
        //         'Agustus':'0',
        //         'September':'0',
        //         'Q3':'0',
        //         'Oktober':'0',
        //         'November':'0',
        //         'Desember':'0',
        //         'Q4':'0',
        //         'S2':'0',
        //         'Tahun':'0',
        //         'Total_Deskripsi':'3',
        //     },
        //     {
        //         'Deskripsi':'SCORE TPM IMPLEMENTATION',
        //         'Parameter':'Achievement (%)',
        //         'Januari':'0',
        //         'Februari':'0',
        //         'Maret':'0',
        //         'Q1':'0',
        //         'April':'0',
        //         'Mei':'0',
        //         'Juni':'0',
        //         'Q2':'0',
        //         'S1':'0',
        //         'Juli':'0',
        //         'Agustus':'0',
        //         'September':'0',
        //         'Q3':'0',
        //         'Oktober':'0',
        //         'November':'0',
        //         'Desember':'0',
        //         'Q4':'0',
        //         'S2':'0',
        //         'Tahun':'0',
        //         'Total_Deskripsi':'3',
        //     },
        //     {
        //         'Deskripsi':'FOCUSED IMPROVEMENT',
        //         'Parameter':'Target (tema)',
        //         'Januari':'0',
        //         'Februari':'0',
        //         'Maret':'0',
        //         'Q1':'0',
        //         'April':'0',
        //         'Mei':'0',
        //         'Juni':'0',
        //         'Q2':'0',
        //         'S1':'0',
        //         'Juli':'0',
        //         'Agustus':'0',
        //         'September':'0',
        //         'Q3':'0',
        //         'Oktober':'0',
        //         'November':'0',
        //         'Desember':'0',
        //         'Q4':'0',
        //         'S2':'0',
        //         'Tahun':'0',
        //         'Total_Deskripsi':'3',
        //     },
        //     {
        //         'Deskripsi':'FOCUSED IMPROVEMENT',
        //         'Parameter':'Realisasi (tema)',
        //         'Januari':'0',
        //         'Februari':'0',
        //         'Maret':'0',
        //         'Q1':'0',
        //         'April':'0',
        //         'Mei':'0',
        //         'Juni':'0',
        //         'Q2':'0',
        //         'S1':'0',
        //         'Juli':'0',
        //         'Agustus':'0',
        //         'September':'0',
        //         'Q3':'0',
        //         'Oktober':'0',
        //         'November':'0',
        //         'Desember':'0',
        //         'Q4':'0',
        //         'S2':'0',
        //         'Tahun':'0',
        //         'Total_Deskripsi':'3',
        //     },
        //     {
        //         'Deskripsi':'FOCUSED IMPROVEMENT',
        //         'Parameter':'Achievement (%)',
        //         'Januari':'0',
        //         'Februari':'0',
        //         'Maret':'0',
        //         'Q1':'0',
        //         'April':'0',
        //         'Mei':'0',
        //         'Juni':'0',
        //         'Q2':'0',
        //         'S1':'0',
        //         'Juli':'0',
        //         'Agustus':'0',
        //         'September':'0',
        //         'Q3':'0',
        //         'Oktober':'0',
        //         'November':'0',
        //         'Desember':'0',
        //         'Q4':'0',
        //         'S2':'0',
        //         'Tahun':'0',
        //         'Total_Deskripsi':'3',
        //     },
        // ];

        var table = $('#tb_data-dashboard-tpm').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                "url": "{{ route($route.'.table') }}",
                'type': 'GET',
                'dataType': 'JSON',
                'data': function(d){
                    d.tahun= $("#filter_year").val()
                }
            },
            processing: true,
            // serverSide: true,
            ordering: false,
            responsive: false,
            scrollX: true,
            aLengthMenu: [[3,6,9,12,15],[3,6,9,12,15]],
            // paging: false,
            // data: data,
            columns: [
                {
                data: 'deskripsi',
                name: 'deskripsi'
                },
                {
                data: 'parameter',
                name: 'parameter',
                searchable: false,
                },
                {
                data: 'jan',
                name: 'jan',
                className: 'text-center',
                searchable: false,
                },
                {
                data: 'feb',
                name: 'feb',
                className: 'text-center',
                searchable: false,
                },
                {
                data: 'mar',
                name: 'mar',
                className: 'text-center',
                searchable: false,
                },
                {
                data: 'q1',
                name: 'q1',
                className: 'text-center',
                searchable: false,
                },
                {
                data: 'apr',
                name: 'apr',
                className: 'text-center',
                searchable: false,
                },
                {
                data: 'mei',
                name: 'mei',
                className: 'text-center',
                searchable: false,
                },
                {
                data: 'jun',
                name: 'jun',
                className: 'text-center',
                searchable: false,
                },
                {
                data: 'q2',
                name: 'q2',
                className: 'text-center',
                searchable: false,
                },
                {
                data: 's1',
                name: 's1',
                className: 'text-center',
                searchable: false,
                },
                {
                data: 'jul',
                name: 'jul',
                className: 'text-center',
                searchable: false,
                },
                {
                data: 'aug',
                name: 'aug',
                className: 'text-center',
                searchable: false,
                },
                {
                data: 'sep',
                name: 'sep',
                className: 'text-center',
                searchable: false,
                },
                {
                data: 'q3',
                name: 'q3',
                className: 'text-center',
                searchable: false,
                },
                {
                data: 'okt',
                name: 'okt',
                className: 'text-center',
                searchable: false,
                },
                {
                data: 'nov',
                name: 'nov',
                className: 'text-center',
                searchable: false,
                },
                {
                data: 'des',
                name: 'des',
                className: 'text-center',
                searchable: false,
                },
                {
                data: 'q4',
                name: 'q4',
                className: 'text-center',
                searchable: false,
                },
                {
                data: 's2',
                name: 's2',
                className: 'text-center',
                searchable: false,
                },
                {
                data: 'nilai_akhir',
                name: 'nilai_akhir',
                className: 'text-center',
                searchable: false,
                },
                // {
                // data: 'Total_Deskripsi',
                // name: 'Total_Deskripsi',
                // searchable: false,
                // visible : false
                // },
                // {
                // data: 'action',
                // name: 'action',
                // orderable: false,
                // searchable: false,
                // width: 150
                // },
            ],
            "initComplete": function(settings, json) {
                processColumnDeskripsi( $('#tb_data-dashboard-tpm').DataTable() );
            }
        });

        $('#filter-tabel').on('click', function () {
            table.draw();
        });


        table.on( 'draw', function () {
            processColumnDeskripsi( $('#tb_data-dashboard-tpm').DataTable() );
        });

        function processColumnDeskripsi(tbl) {
            var selector_modifier = { order: 'current', page: 'current', search: 'applied' }

            var previous = '';
            var officeNodes = tbl.column(0, selector_modifier).nodes();
            var officeData = tbl.column(0, selector_modifier).data();
            // var Total = tbl.column(21, selector_modifier).data();
                for (var i = 0; i < officeData.length; i++) {

                    var current = officeData[i];
                    if (current === previous) {
                        officeNodes[i].textContent = '';
                        officeNodes[i].setAttribute("style", "display:none;");
                    } else {
                        officeNodes[i].textContent = current;
                        officeNodes[i].setAttribute("rowspan", 3);
                        // officeNodes[i].setAttribute("rowspan", Total[i]);
                    }
                        previous = current;
                }
        }

        setTimeout(function(){
            if ($('#alertNotif').length > 0) {
                $('#alertNotif').remove();
            }
        }, 5000)

        $('#btn-add').click(function (e) {
            e.preventDefault();
            var url = "{{ route($route.'.create') }}";
            $.get(url, function (html) {
                $('#modal .modal-content').html(html);
                $('#modal').modal('show');
            }).fail(function () {

            });
        })

</script>

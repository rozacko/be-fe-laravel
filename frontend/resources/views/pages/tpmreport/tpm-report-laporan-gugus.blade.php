<x-default-layout>
    <div class="flex-stack mb-0 mb-lg-n4 pt-5">
        <div class="row">
            <div class="col-sm-4">
                <div class="information">
                    <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">TPM Report</h1>
                        <ul class="breadcrumb breadcrumb-separatorless ">
							<li class="breadcrumb-item text-muted">
								<a href="/" class="text-muted text-hover-primary">TPM Report</a>
							</li>
							<li class="breadcrumb-item text-muted">/</li>
							<li class="breadcrumb-item text-ghopo">{{ $title }}</li>
                        </ul>
                </div>
            </div>
            <div class="col-sm-8 d-flex justify-content-md-end flex-no-wrap px-5">
            <form method="POST" action="{{ route($route . '.downloadExcel') }}" enctype="multipart/form-data" id="download-excel">
                @csrf
                    <div class="row">
                        <div class="col mt-2 dropdown-filter">
                            <select id="filter_fasilitator" class="form-select h-25" data-control="select2" data-placeholder="Pilih Fasilitator">
                                <option value="all">Pilih Fasilitator</option>
                            </select>
                        </div>
                        <div class="col mt-2 dropdown-filter">
                            <div id="loader_filter_gugus" class="flex-grow-1 ml-1" >
                                <select id="filter_gugus" class="form-select h-25" data-control="select2" data-placeholder="Pilih Gugus">
                                    <option value="all">Pilih Gugus</option>
                                </select>
                            </div>
                        </div>
                        <div class="col mt-2 dropdown-filter">
                            <select id="filter_month" class="form-select h-25" data-control="select2" data-placeholder="Pilih Bulan">
                                @foreach(getMonth() as $key => $value)
                                    @if ($value['month'] == date("m") )
                                        <option value="{{$value['month']}}" selected>{{$value['month_name']}}</option>
                                    @else
                                        <option value="{{$value['month']}}">{{$value['month_name']}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col mt-2 dropdown-filter">
                            <select id="filter_year" class="form-select h-25" data-control="select2" data-placeholder="Pilih Tahun">
                                @foreach(getYear() as $key => $value)
                                    @if ($value == date("Y") )
                                        <option value="{{$value}}" selected>{{$value}}</option>
                                    @else
                                        <option value="{{$value}}">{{$value}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col mt-2 card-breadcrumb-button">
                            <button class="btn btn-primary">Download</button>
                        </div>
                    </div>
            </form>
            <div class="row">
                <div class="col mt-2 card-breadcrumb-button ms-2">
                    <button class="btn btn-primary" id="filter-btn">Tampilkan</button>
                </div>
            </div>
        </div>
        </div>
    </div>

    <div class="card card-flush mt-6 mt-xl-9">
        <div class="card-body mt-6 pt-0">
            <h3>Laporan Gugus</h3>
            <table class="table table-bordered data-table" id="tb_laporan-gugus">
                <thead>
                    <tr>
                        <th class="align-middle text-center color-header-tabel" rowspan="2">No</th>
                        <th class="align-middle text-center color-header-tabel" rowspan="2">Fasilitator</th>
                        <th class="align-middle text-center color-header-tabel" rowspan="2">Gugus</th>
                        <th class="align-middle text-center color-header-tabel" rowspan="2">SGA</th>
                        <th class="align-middle text-center color-header-tabel" rowspan="2">Commitment Management</th>
                        <th class="align-middle text-center color-header-tabel" rowspan="2">Papan Control</th>
                        <th class="align-middle text-center color-header-tabel" rowspan="2">Safety, Health & Envirotment</th>
                        <th class="align-middle text-center color-header-tabel" colspan="5">5R</th>
                        <th class="align-middle text-center color-header-tabel" rowspan="2">Autonomous Maintenance</th>
                        <th class="align-middle text-center color-header-tabel" rowspan="2">Planed Maintenance</th>
                        <th class="align-middle text-center color-header-tabel" rowspan="2">Focused Improvement</th>
                        <th class="align-middle text-center color-header-tabel" rowspan="2">Taget</th>
                        <th class="align-middle text-center color-header-tabel" rowspan="2">Nilai SGA</th>
                        <th class="align-middle text-center color-header-tabel" rowspan="2">Nilai Gugus</th>
                        <th class="align-middle text-center color-header-tabel" rowspan="2">Nilai Fasilitator</th>
                    </tr>
                    <tr>
                        <th class="align-middle text-center color-header-tabel">R1</th>
                        <th class="align-middle text-center color-header-tabel">R2</th>
                        <th class="align-middle text-center color-header-tabel">R3</th>
                        <th class="align-middle text-center color-header-tabel">R4</th>
                        <th class="align-middle text-center color-header-tabel">R5</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
    </table>
        </div>
    </div>

</x-default-layout>
<script>
    $(document).ready(function() {
        getFilterFasilitator();
    });

    $('#filter-btn').on('click',function() {
        table.draw();
    })

    $("#download-excel").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.
        var form = $(this);
        var formData = new FormData();
        var actionUrl = form.attr('action');
        formData.append('tahun', $('#filter_year').val());
        formData.append('bulan', $('#filter_month').val());
        $.ajax({
            type: "POST",
            url: actionUrl,
            data: formData,
            dataType: 'JSON',
            contentType: false,
            processData: false,
            beforeSend: function () {
            },
            success: function(data)
            {
                window.location.href = data.data.file_path;
            }
        });
    });

    function getFilterFasilitator() {
        $.ajax({
            type: "GET",
            url: "{{ route($route.'.FilterFasilitator') }}",
            dataType: 'JSON',
            beforeSend: function () {
            },
            success: function (data) {
                $.each(data.data, function (i, item) {
                    var newOption = new Option(item.fasilitator, item.fasilitator, false, false);
                    $('#filter_fasilitator').append(newOption).trigger('change');
                });

            },
            error: function (xmlhttprequest, textstatus, message) {}
        });
    }

    $('#filter_fasilitator').on('select2:closing',function() {
        let fasilitator = $(this).val()
        getFasilitatorbyGugus(fasilitator)
    })

    function getFasilitatorbyGugus(fasilitator) {
        $.ajax({
            type: "GET",
            url: "{{ route($route.'.FilterGugus') }}",
            data: {
                fasilitator: fasilitator
            },
            dataType: 'JSON',
            beforeSend: function () {
                $('#filter_gugus').attr('disabled', true);
                $("#loader_filter_gugus").LoadingOverlay("show");
            },
            success: function (data) {
                $('#filter_gugus').empty();
                $('#filter_gugus').append(new Option("Pilih Gugus", "all", false, false)).trigger('change');
                $.each(data.data, function (i, item) {
                    var newOption = new Option(item.gugus, item.gugus, false, false);
                    $('#filter_gugus').append(newOption).trigger('change');
                });
            },
            error: function (xmlhttprequest, textstatus, message) {}
            }).always(function(){
                $('#filter_gugus').attr('disabled', false);
                $("#loader_filter_gugus").LoadingOverlay("hide");
        });
    }

    var data = [
                {
                    'no':'1',
                    'Fasilitator':'Bahan Baku',
                    'Gugus':'Mining',
                    'SGA':'Crusser Tuban 1',
                    'Commitment_Management':'90',
                    'Papan_Control':'90',
                    'Safety_Health_Envirotment':'90',
                    '5R':'80',
                    'Autonomous_Maintenance':'90',
                    'Planed_Maintenance':'90',
                    'Focused_Improvement':'100',
                    'Target':'76',
                    'Nilai_SGA':'80',
                    'Nilai_Gugus':'70',
                    'Nilai_Fasilitator':'92',
                    'Total_Gugus':'2',
                    'Total_Fasilitator':'4',
                },
                {
                    'no':'2',
                    'Fasilitator':'Bahan Baku',
                    'Gugus':'Mining',
                    'SGA':'Crusser Tuban 1',
                    'Commitment_Management':'90',
                    'Papan_Control':'90',
                    'Safety_Health_Envirotment':'90',
                    '5R':'80',
                    'Autonomous_Maintenance':'90',
                    'Planed_Maintenance':'90',
                    'Focused_Improvement':'90',
                    'Target':'76',
                    'Nilai_SGA':'86',
                    'Nilai_Gugus':'',
                    'Nilai_Fasilitator':'',
                    'Total_Gugus':'2',
                    'Total_Fasilitator':'4',
                },
                {
                    'no':'3',
                    'Fasilitator':'Bahan Baku',
                    'Gugus':'Crusser 1-2',
                    'SGA':'Crusser Tuban 1',
                    'Commitment_Management':'90',
                    'Papan_Control':'90',
                    'Safety_Health_Envirotment':'90',
                    '5R':'80',
                    'Autonomous_Maintenance':'90',
                    'Planed_Maintenance':'90',
                    'Focused_Improvement':'78',
                    'Target':'76',
                    'Nilai_SGA':'71',
                    'Nilai_Gugus':'70',
                    'Nilai_Fasilitator':'',
                    'Total_Gugus':'2',
                    'Total_Fasilitator':'4',
                },
                {
                    'no':'4',
                    'Fasilitator':'Bahan Baku',
                    'Gugus':'Crusser 1-2',
                    'SGA':'Crusser Tuban 1',
                    'Commitment_Management':'90',
                    'Papan_Control':'90',
                    'Safety_Health_Envirotment':'90',
                    '5R':'80',
                    'Autonomous_Maintenance':'90',
                    'Planed_Maintenance':'90',
                    'Focused_Improvement':'50',
                    'Target':'76',
                    'Nilai_SGA':'50',
                    'Nilai_Gugus':'',
                    'Nilai_Fasilitator':'',
                    'Total_Gugus':'2',
                    'Total_Fasilitator':'4',
                },
                {
                    'no':'5',
                    'Fasilitator':'SUPPORTING',
                    'Gugus':'CORPORATE',
                    'SGA':'CORPORATE',
                    'Commitment_Management':'90',
                    'Papan_Control':'90',
                    'Safety_Health_Envirotment':'90',
                    '5R':'80',
                    'Autonomous_Maintenance':'90',
                    'Planed_Maintenance':'90',
                    'Focused_Improvement':'100',
                    'Target':'76',
                    'Nilai_SGA':'80',
                    'Nilai_Gugus':'70',
                    'Nilai_Fasilitator':'70',
                    'Total_Gugus':'1',
                    'Total_Fasilitator':'3',
                },
                {
                    'no':'6',
                    'Fasilitator':'SUPPORTING',
                    'Gugus':'HUMAN',
                    'SGA':'HUMAN',
                    'Commitment_Management':'90',
                    'Papan_Control':'90',
                    'Safety_Health_Envirotment':'90',
                    '5R':'80',
                    'Autonomous_Maintenance':'90',
                    'Planed_Maintenance':'90',
                    'Focused_Improvement':'90',
                    'Target':'76',
                    'Nilai_SGA':'86',
                    'Nilai_Gugus':'70',
                    'Nilai_Fasilitator':'70',
                    'Total_Gugus':'1',
                    'Total_Fasilitator':'3',
                },
                {
                    'no':'7',
                    'Fasilitator':'SUPPORTING',
                    'Gugus':'SECURITY',
                    'SGA':'SECURITY',
                    'Commitment_Management':'90',
                    'Papan_Control':'90',
                    'Safety_Health_Envirotment':'90',
                    '5R':'80',
                    'Autonomous_Maintenance':'90',
                    'Planed_Maintenance':'90',
                    'Focused_Improvement':'78',
                    'Target':'76',
                    'Nilai_SGA':'71',
                    'Nilai_Gugus':'76',
                    'Nilai_Fasilitator':'70',
                    'Total_Gugus':'1',
                    'Total_Fasilitator':'3',
                }
    ];

    var previous = '';

    var table = $('#tb_laporan-gugus').DataTable( {
        processing: true,
        serverSide: true,
        ajax: {
            "url": "{{ route($route.'.table') }}",
            'type': 'GET',
            'dataType': 'JSON',
            'data': function(d){
                d.fasilitator= $("#filter_fasilitator").val(),
                d.gugus= $("#filter_gugus").val(),
                d.bulan= $("#filter_month").val(),
                d.tahun= $("#filter_year").val()
            }
        },
        // data: data,
        paging: false,
        ordering: false,
        responsive: false,
        scrollX: true,
        columns: [{
            data: 'DT_RowIndex',
            name: 'no',
            searchable: false
            },
            {
            data: 'fasilitator',
            name: 'fasilitator'
            },
            {
            data: 'gugus',
            name: 'gugus'
            },
            {
            data: 'sga',
            name: 'sga'
            },
            {
            data: 'commitment_management',
            name: 'commitment_management',
            className: 'text-center',
            searchable: false
            },
            {
            data: 'papan_control',
            name: 'papan_control',
            className: 'text-center',
            searchable: false
            },
            {
            data: 'she',
            name: 'she',
            className: 'text-center',
            searchable: false
            },
            {
            data: '5r',
            name: '5r',
            className: 'text-center',
            searchable: false
            },
            {
            data: 'r2',
            name: 'r2',
            className: 'text-center',
            searchable: false
            },
            {
            data: 'r3',
            name: 'r3',
            className: 'text-center',
            searchable: false
            },
            {
            data: 'r4',
            name: 'r4',
            className: 'text-center',
            searchable: false
            },
            {
            data: 'r5',
            name: 'r5',
            className: 'text-center',
            searchable: false
            },
            {
            data: 'autonomous_maintenance',
            name: 'autonomous_maintenance',
            className: 'text-center',
            searchable: false
            },
            {
            data: 'planed_maintenance',
            name: 'planed_maintenance',
            className: 'text-center',
            searchable: false
            },
            {
            data: 'focused_improvement',
            name: 'focused_improvement',
            className: 'text-center',
            searchable: false
            },
            {
            data: 'target',
            name: 'target',
            className: 'text-center',
            searchable: false
            },
            {
            data: 'nilai_SGA',
            name: 'nilai_SGA',
            className: 'text-center',
            searchable: false
            },
            {
            data: 'nilai_gugus',
            name: 'nilai_gugus',
            className: 'text-center align-middle',
            searchable: false
            },
            {
            data: 'nilai_fasilitator',
            name: 'nilai_fasilitator',
            className: 'text-center align-middle',
            searchable: false
            },
            {
            data: 'total_gugus',
            name: 'total_gugus',
            searchable: false,
            visible : false
            },
            {
            data: 'total_fasilitator',
            name: 'total_fasilitator',
            searchable: false,
            visible : false
            },
        ],
        "initComplete": function(settings, json) {
            processColumnSGA( $('#tb_laporan-gugus').DataTable() );
            processColumnGugus( $('#tb_laporan-gugus').DataTable() );
            processColumnFasilitator( $('#tb_laporan-gugus').DataTable() );
        }
    } );

    table.on( 'draw', function () {
        processColumnSGA( $('#tb_laporan-gugus').DataTable() );
        processColumnGugus( $('#tb_laporan-gugus').DataTable() );
        processColumnFasilitator( $('#tb_laporan-gugus').DataTable() );
    } );

    function processColumnSGA(tbl) {
        var selector_modifier = { order: 'current', page: 'current', search: 'applied' }
        var previous = '';
        var officeNodes = tbl.column(16, selector_modifier).nodes();
        var Data_Target = tbl.column(15, selector_modifier).data();
        var officeData = tbl.column(16, selector_modifier).data();
            for (var i = 0; i < officeData.length; i++) {
                if (Data_Target[i] <= officeData[i]) {
                    officeNodes[i].setAttribute("style", "background-color:#91D04F;");
                }else{
                    officeNodes[i].setAttribute("style", "background-color:#F64752;");
                }
            }
    }

    function processColumnGugus(tbl) {
        var selector_modifier = { order: 'current', page: 'current', search: 'applied' }
        var previous = '';
        var officeNodes = tbl.column(17, selector_modifier).nodes();
        var officeData = tbl.column(2, selector_modifier).data();
        var Data1 = tbl.column(17, selector_modifier).data();
        var Data_Target = tbl.column(15, selector_modifier).data();
        var total = tbl.column(19, selector_modifier).data();
            for (var i = 0; i < officeData.length; i++) {
                var current = officeData[i];
                if (current === previous) {
                    officeNodes[i].textContent = '';
                    officeNodes[i].setAttribute("style", "display:none;");
                } else {
                    officeNodes[i].setAttribute("rowspan", total[i]);
                    officeNodes[i].textContent = Data1[i];
                    if (Data_Target[i] <= Data1[i]) {
                        officeNodes[i].setAttribute("style", "background-color:#91D04F;");
                    }else{
                        officeNodes[i].setAttribute("style", "background-color:#F64752;");
                    }
                }
                    previous = current;
            }
    }

    function processColumnFasilitator(tbl) {
        var selector_modifier = { order: 'current', page: 'current', search: 'applied' }

        var previous = '';
        var officeNodes = tbl.column(18, selector_modifier).nodes();
        var officeData = tbl.column(1, selector_modifier).data();
        var Data1 = tbl.column(18, selector_modifier).data();
        var Data_Target = tbl.column(15, selector_modifier).data();
        var Total = tbl.column(20, selector_modifier).data();

            for (var i = 0; i < officeData.length; i++) {

                var current = officeData[i];
                if (current === previous) {
                    officeNodes[i].textContent = '';
                    officeNodes[i].setAttribute("style", "display:none;");
                } else {
                    officeNodes[i].textContent = Data1[i];
                    officeNodes[i].setAttribute("rowspan", Total[i]);
                    officeNodes[i].setAttribute("style", "background-color:#91D04F;");
                    if (Data_Target[i] <= Data1[i]) {
                        officeNodes[i].setAttribute("style", "background-color:#91D04F;");
                    }else{
                        officeNodes[i].setAttribute("style", "background-color:#F64752;");
                    }
                }
                    previous = current;
            }
    }

</script>

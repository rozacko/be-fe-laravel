<x-default-layout>
    <div class="flex-stack mb-0 mb-lg-n4 pt-5">
        <div class="row">
            <div class="col-sm-5">
       <div class="information">
            <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{ $title }}</h1>
				<ul class="breadcrumb breadcrumb-separatorless ">
                    <li class="breadcrumb-item text-muted">
                        <a href="/" class="text-muted text-hover-primary">{{ $title }}</a>
                    </li>
                    <li class="breadcrumb-item text-muted">/</li>
                    <li class="breadcrumb-item text-ghopo">Dashboard TPM</li>
				</ul>
       </div>
            </div>
            <div class="col-sm-7 d-flex justify-content-md-end flex-no-wrap px-5">
        <div class="filter">
                    <div class="row">
                        <div class="col-6">
                            <select id="filter_year" name="filter_year" class="form-select h-25" data-control="select2" data-placeholder="Pilih Tahun">
                                @foreach(getYear() as $key => $value)
                                    @if ($value == date("Y") )
                                        <option value="{{$value}}" selected>{{$value}}</option>
                                    @else
                                        <option value="{{$value}}">{{$value}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-6">
                            <button class="btn btn-primary" onclick="changeFilterData();">Tampilkan</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>

    <div class="mt-5 g-5 g-xl-10 mb-3 mb-xl-3">
        <div class="card-tpm-report">
            <div class="row">
                <div class="col-md-4 col-sm-12 mt-2 col-custom">
                    <div id="loader_score_tpm_implementation">
                        @include('partials/widgets/cards/_widget-score-tpm-implementation')
                    </div>
                </div>
                <div class="col-md-4 col-sm-12 mt-2 col-custom">
                    <div id="loader_focused_improvement">
                        @include('partials/widgets/cards/_widget-focused-improvement')
                    </div>
                </div>
                <div class="col-md-4 col-sm-12 mt-2 col-custom">
                    <div id="loader_focus_area">
                        @include('partials/widgets/cards/_widget-focus-area')
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="g-5 g-xl-10 mb-3 mb-xl-3">
        <div class="card-tpm-report">
            <div class="row">
                <div class="col-md-6 col-sm-12 mt-2 col-custom">
                    <div id="loader_genba_sga">
                        @include('partials/widgets/cards/_widget-genba-sga')
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 mt-2 col-custom">
                    <div id="loader_partisipasi_member_tpm">
                        @include('partials/widgets/cards/_widget-partisipasi-member-tpm')
                    </div>
                </div>
            </div>
        </div>
    </div>

</x-default-layout>
<script>
    $(document).ready(function() {
        allCard();
    });

    function changeFilterData() {
        var filter = "True";
        scoreimplementation(filter);
        focusedimprovement(filter);
        focusarea(filter);
        genbasga(filter);
        partisipasimember(filter);
    }

    function allCard() {
        var filter = "False";
        scoreimplementation(filter);
        focusedimprovement(filter);
        focusarea(filter);
        genbasga(filter);
        partisipasimember(filter);
    }

    function scoreimplementation(filter) {
                $.ajax({
                    type: "GET",
                    url: "{{ $route.'/scoreimplementation' }}",
                    data: {
                        tahun: $("#filter_year").val()
                    },
                    dataType: 'JSON',
                    beforeSend: function() {
                        $("#loader_score_tpm_implementation").LoadingOverlay("show");
                    },
                    success: function(data)
                    {
                        if(data.status == 'success'){
                            var KTChartsWidgetScoreTpmImplementation = (function () {
                                var chart = {
                                    self: null,
                                    rendered: false,
                                };

                                // Private methods
                                var initChart = function (chart) {
                                    var element = document.getElementById(
                                        "kt_charts_widget_score_tpm_implementation"
                                    );

                                    if (!element) {
                                        return;
                                    }

                                    var height = parseInt(KTUtil.css(element, "height"));

                                    var options = {
                                        series: [
                                                {
                                                    name: "Realisasi (poin)",
                                                    color: '#02AFF0',
                                                    type: "column",
                                                    data: data.data.Realisasi,
                                                },
                                                {
                                                    name: "Target (poin)",
                                                    color: '#DE4848',
                                                    type: "line",
                                                    data: data.data.Target,
                                                },
                                                {
                                                    name: "Achievement (%)",
                                                    color: '#000000',
                                                    type: "line",
                                                    data: data.data.Achievement,
                                                }
                                            ],
                                            chart: {
                                                id: 'chart_widget_score_tpm_implementation',
                                                height: height,
                                                type: "line",
                                                toolbar: {
                                                    show: false,
                                                },
                                            },
                                            stroke: {
                                                width: [0, 2]
                                            },
                                            // title: {
                                            //   text: 'Traffic Sources'
                                            // },
                                            // dataLabels: {
                                            //   enabled: true,
                                            //   enabledOnSeries: [1]
                                            // },
                                            xaxis: {
                                                categories: data.data.categori
                                            },
                                            legend: {
                                                position: 'top',
                                                horizontalAlign: 'left',
                                                offsetX: 0
                                            }
                                    };

                                    chart.self = new ApexCharts(element, options);
                                    // Set timeout to properly get the parent elements width
                                    setTimeout(function () {
                                        if (filter == "False") {
                                            chart.self.render();
                                        } else {
                                            ApexCharts.exec('chart_widget_score_tpm_implementation', "updateOptions", {
                                                xaxis: {
                                                    categories: data.data.categori
                                                }
                                            });

                                            ApexCharts.exec('chart_widget_score_tpm_implementation', "updateSeries", [
                                                {
                                                    name: "Realisasi (poin)",
                                                    color: '#02AFF0',
                                                    type: "column",
                                                    data: data.data.Realisasi,
                                                },
                                                {
                                                    name: "Target (poin)",
                                                    color: '#DE4848',
                                                    type: "line",
                                                    data: data.data.Target,
                                                },
                                                {
                                                    name: "Achievement (%)",
                                                    color: '#000000',
                                                    type: "line",
                                                    data: data.data.Achievement,
                                                }
                                            ]);
                                        }
                                        chart.rendered = true;
                                    }, 200);
                                };

                                // Public methods
                                return {
                                    init: function () {
                                        initChart(chart);

                                        // Update chart on theme mode change
                                        KTThemeMode.on("kt.thememode.change", function () {
                                            if (chart.rendered) {
                                                chart.self.destroy();
                                            }

                                            initChart(chart);
                                        });
                                    },
                                };
                            })();

                            // Webpack support
                            if (typeof module !== "undefined") {
                                module.exports = KTChartsWidgetScoreTpmImplementation;
                            }

                            // On document ready
                            KTUtil.onDOMContentLoaded(function () {
                                KTChartsWidgetScoreTpmImplementation.init();
                            });
                        }else{
                            // Swal.fire(
                            //     'Warning',
                            //     data.message,
                            //     'warning'
                            // );
                        }
                    },
                }).always(function(){
                    $("#loader_score_tpm_implementation").LoadingOverlay("hide");
                });
    }

    function focusedimprovement(filter) {

                $.ajax({
                    type: "GET",
                    url: "{{ $route.'/focusedimprovement' }}",
                    data: {
                        tahun: $("#filter_year").val()
                    },
                    dataType: 'JSON',
                    beforeSend: function() {
                        $("#loader_focused_improvement").LoadingOverlay("show");
                    },
                    success: function(data)
                    {
                        if(data.status == 'success'){


                            var KTChartWidgetFocusedImprovement = (function () {
                                var chart = {
                                    self: null,
                                    rendered: false,
                                };

                                // Private methods
                                var initChart = function (chart) {
                                    var element = document.getElementById(
                                        "kt_chart_widget_focused_improvement"
                                    );

                                    if (!element) {
                                        return;
                                    }

                                    var height = parseInt(KTUtil.css(element, "height"));

                                    var options = {
                                    series: [
                                            {
                                                name: "Realisasi (Tema)",
                                                color: '#02AFF0',
                                                type: "column",
                                                data: data.data.Realisasi,
                                            },
                                            {
                                                name: "Target (Tema)",
                                                color: '#DE4848',
                                                type: "line",
                                                data: data.data.Target,
                                            },
                                            {
                                                name: "Achievement (%)",
                                                color: '#000000',
                                                type: "line",
                                                data: data.data.Achievement,
                                            }
                                        ],
                                        chart: {
                                            id: 'chart_widget_focused_improvement',
                                            height: height,
                                            type: "line",
                                            toolbar: {
                                                show: false,
                                            },
                                        },
                                        stroke: {
                                            width: [0, 2]
                                        },
                                        // title: {
                                        //   text: 'Traffic Sources'
                                        // },
                                        // dataLabels: {
                                        //   enabled: true,
                                        //   enabledOnSeries: [1]
                                        // },
                                        xaxis: {
                                            categories: data.data.categori,
                                        },
                                        legend: {
                                            position: 'top',
                                            horizontalAlign: 'left',
                                            offsetX: 0
                                        }
                                    };
                                    chart.self = new ApexCharts(element, options);

                                    // Set timeout to properly get the parent elements width
                                    setTimeout(function () {
                                        if (filter == "False") {
                                            chart.self.render();
                                        } else {
                                            ApexCharts.exec('chart_widget_focused_improvement', "updateOptions", {
                                                xaxis: {
                                                    categories: data.data.categori
                                                }
                                            });

                                            ApexCharts.exec('chart_widget_focused_improvement', "updateSeries", [
                                                {
                                                    name: "Realisasi (Tema)",
                                                    color: '#02AFF0',
                                                    type: "column",
                                                    data: data.data.Realisasi,
                                                },
                                                {
                                                    name: "Target (Tema)",
                                                    color: '#DE4848',
                                                    type: "line",
                                                    data: data.data.Target,
                                                },
                                                {
                                                    name: "Achievement (%)",
                                                    color: '#000000',
                                                    type: "line",
                                                    data: data.data.Achievement,
                                                }
                                            ]);
                                        }
                                        chart.rendered = true;
                                    }, 200);
                                };

                                // Public methods
                                return {
                                    init: function () {
                                        initChart(chart);

                                        // Update chart on theme mode change
                                        KTThemeMode.on("kt.thememode.change", function () {
                                            if (chart.rendered) {
                                                chart.self.destroy();
                                            }

                                            initChart(chart);
                                        });
                                    },
                                };
                            })();

                            // Webpack support
                            if (typeof module !== "undefined") {
                                module.exports = KTChartWidgetFocusedImprovement;
                            }

                            // On document ready
                            KTUtil.onDOMContentLoaded(function () {
                                KTChartWidgetFocusedImprovement.init();
                            });

                        }else{
                            // Swal.fire(
                            //     'Warning',
                            //     data.message,
                            //     'warning'
                            // );
                        }
                    },
                }).always(function(){
                    $("#loader_focused_improvement").LoadingOverlay("hide");
                });
    }

    function focusarea(filter) {

                $.ajax({
                    type: "GET",
                    url: "{{ $route.'/focusarea' }}",
                    data: {
                        tahun: $("#filter_year").val()
                    },
                    dataType: 'JSON',
                    beforeSend: function() {
                        $("#loader_focus_area").LoadingOverlay("show");
                    },
                    success: function(data)
                    {
                        if(data.status == 'success'){

                            var KTChartsWidgetFocusArea = (function () {
                                var chart = {
                                    self: null,
                                    rendered: false,
                                };

                                // Private methods
                                var initChart = function (chart) {
                                    var element = document.getElementById(
                                        "kt_charts_widget_focus_area"
                                    );

                                    if (!element) {
                                        return;
                                    }

                                    var height = parseInt(KTUtil.css(element, "height"));

                                    var options = {
                                    series: [
                                            {
                                                name: "Realisasi (Area)",
                                                color: '#02AFF0',
                                                type: "column",
                                                data: data.data.Realisasi,
                                            },
                                            {
                                                name: "Target (Area)",
                                                color: '#DE4848',
                                                type: "line",
                                                data: data.data.Target,
                                            },
                                            {
                                                name: "Achievement (%)",
                                                color: '#000000',
                                                type: "line",
                                                data: data.data.Achievement,
                                            }
                                        ],
                                        chart: {
                                            id: 'chart_widget_focus_area',
                                            height: height,
                                            type: "line",
                                            toolbar: {
                                                show: false,
                                            },
                                        },
                                        stroke: {
                                            width: [0, 2]
                                        },
                                        // title: {
                                        //   text: 'Traffic Sources'
                                        // },
                                        // dataLabels: {
                                        //   enabled: true,
                                        //   enabledOnSeries: [1]
                                        // },
                                        xaxis: {
                                            categories: data.data.categori,
                                        },
                                        legend: {
                                            position: 'top',
                                            horizontalAlign: 'left',
                                            offsetX: 0
                                        }
                                    };
                                    chart.self = new ApexCharts(element, options);

                                    // Set timeout to properly get the parent elements width
                                    setTimeout(function () {
                                        if (filter == "False") {
                                            chart.self.render();
                                        } else {
                                            ApexCharts.exec('chart_widget_focus_area', "updateOptions", {
                                                xaxis: {
                                                    categories: data.data.categori
                                                }
                                            });

                                            ApexCharts.exec('chart_widget_focus_area', "updateSeries", [
                                                {
                                                name: "Realisasi (Area)",
                                                color: '#02AFF0',
                                                type: "column",
                                                data: data.data.Realisasi,
                                                },
                                                {
                                                    name: "Target (Area)",
                                                    color: '#DE4848',
                                                    type: "line",
                                                    data: data.data.Target,
                                                },
                                                {
                                                    name: "Achievement (%)",
                                                    color: '#000000',
                                                    type: "line",
                                                    data: data.data.Achievement,
                                                }
                                            ]);
                                        }
                                        chart.rendered = true;
                                    }, 200);
                                };

                                // Public methods
                                return {
                                    init: function () {
                                        initChart(chart);

                                        // Update chart on theme mode change
                                        KTThemeMode.on("kt.thememode.change", function () {
                                            if (chart.rendered) {
                                                chart.self.destroy();
                                            }

                                            initChart(chart);
                                        });
                                    },
                                };
                            })();

                            // Webpack support
                            if (typeof module !== "undefined") {
                                module.exports = KTChartsWidgetFocusArea;
                            }

                            // On document ready
                            KTUtil.onDOMContentLoaded(function () {
                                KTChartsWidgetFocusArea.init();
                            });

                        }else{
                            // Swal.fire(
                            //     'Warning',
                            //     data.message,
                            //     'warning'
                            // );
                        }
                    },
                }).always(function(){
                    $("#loader_focus_area").LoadingOverlay("hide");
                });
    }

    function genbasga(filter) {

                $.ajax({
                    type: "GET",
                    url: "{{ $route.'/genbasga' }}",
                    data: {
                        tahun: $("#filter_year").val()
                    },
                    dataType: 'JSON',
                    beforeSend: function() {
                        $("#loader_genba_sga").LoadingOverlay("show");
                    },
                    success: function(data)
                    {
                        if(data.status == 'success'){

                            var KTChartsWidgetGenbaSga = (function () {
                                var chart = {
                                    self: null,
                                    rendered: false,
                                };

                                // Private methods
                                var initChart = function (chart) {
                                    var element = document.getElementById(
                                        "kt_charts_widget_genba_sga"
                                    );

                                    if (!element) {
                                        return;
                                    }

                                    var height = parseInt(KTUtil.css(element, "height"));

                                    var options = {
                                    series: [
                                            {
                                                name: "Realisasi (Kali/Bulan)",
                                                color: '#02AFF0',
                                                type: "column",
                                                data: data.data.Realisasi,
                                            },
                                            {
                                                name: "Target (Kali/Bulan)",
                                                color: '#DE4848',
                                                type: "line",
                                                data: data.data.Target,
                                            },
                                            {
                                                name: "Achievement (%)",
                                                color: '#000000',
                                                type: "line",
                                                data: data.data.Achievement,
                                            }
                                        ],
                                        chart: {
                                            id:'chart_widget_genba_sga',
                                            height: height,
                                            type: "line",
                                            toolbar: {
                                                show: false,
                                            },
                                        },
                                        stroke: {
                                            width: [0, 2]
                                        },
                                        // title: {
                                        //   text: 'Traffic Sources'
                                        // },
                                        // dataLabels: {
                                        //   enabled: true,
                                        //   enabledOnSeries: [1]
                                        // },
                                        xaxis: {
                                            categories: data.data.categori,
                                        },
                                        legend: {
                                            position: 'top',
                                            horizontalAlign: 'left',
                                            offsetX: 0
                                        }
                                    };
                                    chart.self = new ApexCharts(element, options);

                                    // Set timeout to properly get the parent elements width
                                    setTimeout(function () {
                                        if (filter == "False") {
                                            chart.self.render();
                                        } else {
                                            ApexCharts.exec('chart_widget_genba_sga', "updateOptions", {
                                                xaxis: {
                                                    categories: data.data.categori
                                                }
                                            });

                                            ApexCharts.exec('chart_widget_genba_sga', "updateSeries", [
                                                {
                                                    name: "Realisasi (Kali/Bulan)",
                                                    color: '#02AFF0',
                                                    type: "column",
                                                    data: data.data.Realisasi,
                                                },
                                                {
                                                    name: "Target (Kali/Bulan)",
                                                    color: '#DE4848',
                                                    type: "line",
                                                    data: data.data.Target,
                                                },
                                                {
                                                    name: "Achievement (%)",
                                                    color: '#000000',
                                                    type: "line",
                                                    data: data.data.Achievement,
                                                }
                                            ]);
                                        }
                                        chart.rendered = true;
                                    }, 200);
                                };

                                // Public methods
                                return {
                                    init: function () {
                                        initChart(chart);

                                        // Update chart on theme mode change
                                        KTThemeMode.on("kt.thememode.change", function () {
                                            if (chart.rendered) {
                                                chart.self.destroy();
                                            }

                                            initChart(chart);
                                        });
                                    },
                                };
                            })();

                            // Webpack support
                            if (typeof module !== "undefined") {
                                module.exports = KTChartsWidgetGenbaSga;
                            }

                            // On document ready
                            KTUtil.onDOMContentLoaded(function () {
                                KTChartsWidgetGenbaSga.init();
                            });

                        }else{
                            // Swal.fire(
                            //     'Warning',
                            //     data.message,
                            //     'warning'
                            // );
                        }
                    },
                }).always(function(){
                    $("#loader_genba_sga").LoadingOverlay("hide");
                });
    }

    function partisipasimember(filter) {

                $.ajax({
                    type: "GET",
                    url: "{{ $route.'/partisipasimember' }}",
                    data: {
                        tahun: $("#filter_year").val()
                    },
                    dataType: 'JSON',
                    beforeSend: function() {
                        $("#loader_partisipasi_member_tpm").LoadingOverlay("show");
                    },
                    success: function(data)
                    {
                        if(data.status == 'success'){

                            var KTChartsWidgetPartisipasiMemberTpm = (function () {
                                var chart = {
                                    self: null,
                                    rendered: false,
                                };

                                // Private methods
                                var initChart = function (chart) {
                                    var element = document.getElementById(
                                        "kt_charts_widget_partisipasi_member_tpm"
                                    );

                                    if (!element) {
                                        return;
                                    }

                                    var height = parseInt(KTUtil.css(element, "height"));

                                    var options = {
                                    series: [
                                            {
                                                name: "Realisasi (%)",
                                                color: '#02AFF0',
                                                type: "column",
                                                data: data.data.Realisasi,
                                            },
                                            {
                                                name: "Target (%)",
                                                color: '#DE4848',
                                                type: "line",
                                                data: data.data.Target,
                                            },
                                            {
                                                name: "Achievement (%)",
                                                color: '#000000',
                                                type: "line",
                                                data: data.data.Achievement,
                                            }
                                        ],
                                        chart: {
                                            id: 'chart_widget_partisipasi_member_tpm',
                                            height: height,
                                            type: "line",
                                            toolbar: {
                                                show: false,
                                            },
                                        },
                                        stroke: {
                                            width: [0, 2]
                                        },
                                        // title: {
                                        //   text: 'Traffic Sources'
                                        // },
                                        // dataLabels: {
                                        //   enabled: true,
                                        //   enabledOnSeries: [1]
                                        // },
                                        xaxis: {
                                            categories: data.data.categori,
                                        },
                                        legend: {
                                            position: 'top',
                                            horizontalAlign: 'left',
                                            offsetX: 0
                                        }
                                    };
                                    chart.self = new ApexCharts(element, options);

                                    // Set timeout to properly get the parent elements width
                                    setTimeout(function () {
                                        if (filter == "False") {
                                            chart.self.render();
                                        } else {
                                            ApexCharts.exec('chart_widget_partisipasi_member_tpm', "updateOptions", {
                                                xaxis: {
                                                    categories: data.data.categori
                                                }
                                            });

                                            ApexCharts.exec('chart_widget_partisipasi_member_tpm', "updateSeries", [
                                                {
                                                name: "Realisasi (%)",
                                                color: '#02AFF0',
                                                type: "column",
                                                data: data.data.Realisasi,
                                                },
                                                {
                                                    name: "Target (%)",
                                                    color: '#DE4848',
                                                    type: "line",
                                                    data: data.data.Target,
                                                },
                                                {
                                                    name: "Achievement (%)",
                                                    color: '#000000',
                                                    type: "line",
                                                    data: data.data.Achievement,
                                                }
                                            ]);
                                        }
                                        chart.rendered = true;
                                    }, 200);
                                };

                                // Public methods
                                return {
                                    init: function () {
                                        initChart(chart);

                                        // Update chart on theme mode change
                                        KTThemeMode.on("kt.thememode.change", function () {
                                            if (chart.rendered) {
                                                chart.self.destroy();
                                            }

                                            initChart(chart);
                                        });
                                    },
                                };
                            })();

                            // Webpack support
                            if (typeof module !== "undefined") {
                                module.exports = KTChartsWidgetPartisipasiMemberTpm;
                            }

                            // On document ready
                            KTUtil.onDOMContentLoaded(function () {
                                KTChartsWidgetPartisipasiMemberTpm.init();
                            });


                        }else{
                            // Swal.fire(
                            //     'Warning',
                            //     data.message,
                            //     'warning'
                            // );
                        }
                    },
                }).always(function(){
                    $("#loader_partisipasi_member_tpm").LoadingOverlay("hide");
                });
    }

</script>

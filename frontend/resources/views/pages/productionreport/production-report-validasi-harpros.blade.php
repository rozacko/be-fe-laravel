<x-default-layout>
    <div class="flex-stack mb-0 mb-lg-n4 pt-5">
        <div class="row">
            <div class="col-sm-3">
                <div class="information">
                    <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{$pagetitle}}</h1>
                            <ul class="breadcrumb breadcrumb-separatorless ">
                                        <li class="breadcrumb-item text-muted">
                                            <a href="/" class="text-muted text-hover-primary">{{$pagetitle}}</a>
                                        </li>
                                        <li class="breadcrumb-item text-muted">/</li>
                                        <li class="breadcrumb-item text-ghopo">Validasi dan Harpros</li>
                            </ul>
                </div>
            </div>
            <div class="col-sm-9 d-flex justify-content-md-end flex-no-wrap px-5">
                <div class="filter">
                    <div class="row">
                        <div class="col mt-2">
                            <select class="form-select h-25" data-control="select2" data-placeholder="Pilih Plant">
                                <option></option>
                                <option value="1">Option 1</option>
                                <option value="2">Option 2</option>
                            </select>
                        </div>
                        <div class="col mt-2">
                            <select class="form-select h-25" data-control="select2" data-placeholder="Pilih Bulan">
                                @foreach(getMonth() as $key => $value)
                                    @if ($value['month'] == date("n") )
                                        <option value="{{$value['month']}}" selected>{{$value['month_name']}}</option>
                                    @else
                                        <option value="{{$value['month']}}">{{$value['month_name']}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col mt-2">
                            <select class="form-select h-25" data-control="select2" data-placeholder="Pilih Tahun">
                                @foreach(getYear() as $key => $value)
                                    @if ($value == date("Y") )
                                        <option value="{{$value}}" selected>{{$value}}</option>
                                    @else
                                        <option value="{{$value}}">{{$value}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col mt-2 card-breadcrumb-button ">
                            <button class="btn btn-primary">Tampilkan</button>
                        </div>
                        <div class="col mt-2 card-breadcrumb-button">
                            <button type="button" class="btn btn-primary btn-add">Download</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>

    <div class="card card-flush mt-6 mt-xl-9">
        <div class="card-body mt-6 pt-0">
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <h3>VALIDASI DAN HARPROS</h3>
                        <table class="table table-bordered data-table" id="tb_laporan_validasi_harpros">
                            <thead>
                                <tr>
                                    <th class="align-middle text-center color-header-tabel">KETERANGAN</th>
                                    <th class="align-middle text-center color-header-tabel">OPC</th>
                                    <th class="align-middle text-center color-header-tabel">VALIDASI</th>
                                    <th class="align-middle text-center color-header-tabel">HARPROS</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card card-flush mt-6 mt-xl-9">
        <div class="card-body mt-6 pt-0">
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <table class="table table-bordered data-table" id="tb_laporan_release_semen">
                            <thead>
                                <tr>
                                    <th class="align-middle text-center color-header-tabel">RELEASE SEMEN</th>
                                    <th class="align-middle text-center color-header-tabel">OPC</th>
                                    <th class="align-middle text-center color-header-tabel">VALIDASI</th>
                                    <th class="align-middle text-center color-header-tabel">HARPROS</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card card-flush mt-6 mt-xl-9">
        <div class="card-body mt-6 pt-0">
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <table class="table table-bordered data-table" id="tb_laporan_job_release_semen">
                            <thead>
                                <tr>
                                    <th class="align-middle text-center color-header-tabel">JOB RELEASE SEMEN</th>
                                    <th class="align-middle text-center color-header-tabel">OPC</th>
                                    <th class="align-middle text-center color-header-tabel">VALIDASI</th>
                                    <th class="align-middle text-center color-header-tabel">HARPROS</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</x-default-layout>
<script>
    $(function() {

        // data dumy
        var data_validasi_dan_harpros = [
            {
                'keterangan':'Prod. BK Crusher #1',
                'opc':'740',
                'validasi':'8000',
                'harpros':'15000',
            },
            {
                'keterangan':'Jam OP. Prod. BK Crusher #1',
                'opc':'740',
                'validasi':'8000',
                'harpros':'15000',
            },
            {
                'keterangan':'Prod. BK Crusher #2',
                'opc':'740',
                'validasi':'8000',
                'harpros':'15000',
            },
            {
                'keterangan':'Jam OP. Prod. BK Crusher #2',
                'opc':'740',
                'validasi':'8000',
                'harpros':'15000',
            },
        ];

        var table = $('#tb_laporan_validasi_harpros').DataTable({
            processing: true,
            // serverSide: true,
            filter: false,
            ordering: false,
            responsive: false,
            scrollX: true,
            // paging: false,
            data: data_validasi_dan_harpros,
            columns: [
                {
                data: 'keterangan',
                name: 'keterangan',
                },
                {
                data: 'opc',
                name: 'opc'
                },
                {
                data: 'validasi',
                name: 'validasi'
                },
                {
                data: 'harpros',
                name: 'harpros'
                },
            ],
        });

        var data_release_semen = [
            {
                'keterangan':'Prod. BK Crusher #1',
                'opc':'740',
                'validasi':'8000',
                'harpros':'15000',
            },
            {
                'keterangan':'Jam OP. Prod. BK Crusher #1',
                'opc':'740',
                'validasi':'8000',
                'harpros':'15000',
            },
            {
                'keterangan':'Prod. BK Crusher #2',
                'opc':'740',
                'validasi':'8000',
                'harpros':'15000',
            },
            {
                'keterangan':'Jam OP. Prod. BK Crusher #2',
                'opc':'740',
                'validasi':'8000',
                'harpros':'15000',
            },
        ];

        var table = $('#tb_laporan_release_semen').DataTable({
            processing: true,
            // serverSide: true,
            filter: false,
            ordering: false,
            responsive: false,
            scrollX: true,
            // paging: false,
            data: data_release_semen,
            columns: [
                {
                data: 'keterangan',
                name: 'keterangan',
                },
                {
                data: 'opc',
                name: 'opc'
                },
                {
                data: 'validasi',
                name: 'validasi'
                },
                {
                data: 'harpros',
                name: 'harpros'
                },
            ],
        });

         var data_job_release_semen = [
            {
                'keterangan':'Prod. BK Crusher #1',
                'opc':'740',
                'validasi':'8000',
                'harpros':'15000',
            },
            {
                'keterangan':'Jam OP. Prod. BK Crusher #1',
                'opc':'740',
                'validasi':'8000',
                'harpros':'15000',
            },
            {
                'keterangan':'Prod. BK Crusher #2',
                'opc':'740',
                'validasi':'8000',
                'harpros':'15000',
            },
            {
                'keterangan':'Jam OP. Prod. BK Crusher #2',
                'opc':'740',
                'validasi':'8000',
                'harpros':'15000',
            },
        ];

        var table = $('#tb_laporan_job_release_semen').DataTable({
            processing: true,
            // serverSide: true,
            filter: false,
            ordering: false,
            responsive: false,
            scrollX: true,
            // paging: false,
            data: data_job_release_semen,
            columns: [
                {
                data: 'keterangan',
                name: 'keterangan',
                },
                {
                data: 'opc',
                name: 'opc'
                },
                {
                data: 'validasi',
                name: 'validasi'
                },
                {
                data: 'harpros',
                name: 'harpros'
                },
            ],
        });

        setTimeout(function(){
            if ($('#alertNotif').length > 0) {
                $('#alertNotif').remove();
            }
        }, 5000)


    });
</script>

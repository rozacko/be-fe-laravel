<x-default-layout>
    <div class="flex-stack mb-0 mb-lg-n4 pt-5">
        <div class="row">
            <div class="col-sm-3">
                <div class="information">
                    <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{$pagetitle}}</h1>
                            <ul class="breadcrumb breadcrumb-separatorless ">
                                        <li class="breadcrumb-item text-muted">
                                            <a href="/" class="text-muted text-hover-primary">{{$pagetitle}}</a>
                                        </li>
                                        <li class="breadcrumb-item text-muted">/</li>
                                        <li class="breadcrumb-item text-ghopo">Laporan Harian</li>
                            </ul>
                </div>
            </div>
            <div class="col-sm-9 d-flex justify-content-md-end flex-no-wrap px-5">
                <div class="filter">
                    <div class="row">
                        <div class="col mt-2">
                            <select class="form-select h-25" data-control="select2" data-placeholder="Pilih Plant">
                                <option></option>
                                <option value="1">Option 1</option>
                                <option value="2">Option 2</option>
                            </select>
                        </div>
                        <div class="col mt-2">
                            <select class="form-select h-25" data-control="select2" data-placeholder="Pilih Bulan">
                                @foreach(getMonth() as $key => $value)
                                    @if ($value['month'] == date("n") )
                                        <option value="{{$value['month']}}" selected>{{$value['month_name']}}</option>
                                    @else
                                        <option value="{{$value['month']}}">{{$value['month_name']}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col mt-2">
                            <select class="form-select h-25" data-control="select2" data-placeholder="Pilih Tahun">
                                @foreach(getYear() as $key => $value)
                                    @if ($value == date("Y") )
                                        <option value="{{$value}}" selected>{{$value}}</option>
                                    @else
                                        <option value="{{$value}}">{{$value}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col mt-2 card-breadcrumb-button ">
                            <button class="btn btn-primary">Tampilkan</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>

    <div class="card card-flush mt-6 mt-xl-9">
        <div class="card-body mt-6 pt-0">
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <h3>LAPORAN HARIAN</h3>
                        <table class="table table-bordered data-table" id="tb_laporan_produksi_tahun">
                            <thead>
                                <tr>
                                    <th class="align-middle text-center color-header-tabel" rowspan="2" >PRODUKSI PERALATAN</th>
                                    <th class="align-middle text-center color-header-tabel" rowspan="2" ></th>
                                    <th class="align-middle text-center color-header-tabel" colspan="3">HARI INI</th>
                                    <th class="align-middle text-center color-header-tabel" colspan="5">BULAN JALAN</th>
                                    <th class="align-middle text-center color-header-tabel" colspan="5">TAHUN JALAN</th>
                                </tr>
                                <tr>
                                    <th class="align-middle text-center color-header-tabel">JOP</th>
                                    <th class="align-middle text-center color-header-tabel">TON/JAM</th>
                                    <th class="align-middle text-center color-header-tabel">TON</th>
                                    <th class="align-middle text-center color-header-tabel">JOP</th>
                                    <th class="align-middle text-center color-header-tabel">TON/JAM</th>
                                    <th class="align-middle text-center color-header-tabel">TON</th>
                                    <th class="align-middle text-center color-header-tabel">TKAP</th>
                                    <th class="align-middle text-center color-header-tabel">% REAL</th>
                                    <th class="align-middle text-center color-header-tabel">JOP</th>
                                    <th class="align-middle text-center color-header-tabel">TON/JAM</th>
                                    <th class="align-middle text-center color-header-tabel">TON</th>
                                    <th class="align-middle text-center color-header-tabel">TKAP</th>
                                    <th class="align-middle text-center color-header-tabel">% REAL</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</x-default-layout>
<script>
    $(function() {

        // data dumy

        var data_laporan_produksi_tahun = [
            {
                'group':'BATU KAPUR',
                'produksi':'CRUSSER #1',
                'today_job':'8000',
                'today_jam':'15000',
                'today_ton':'6000',
                'month_job':'6500',
                'month_jam':'15000',
                'month_ton':'20000',
                'month_rkap':'23000',
                'month_real':'23000',
                'year_job':'6500',
                'year_jam':'15000',
                'year_ton':'20000',
                'year_rkap':'23000',
                'year_real':'23000',
                'total_group':'7',
            },
            {
                'group':'BATU KAPUR',
                'produksi':'CRUSSER #2',
                'today_job':'8000',
                'today_jam':'15000',
                'today_ton':'6000',
                'month_job':'6500',
                'month_jam':'15000',
                'month_ton':'20000',
                'month_rkap':'23000',
                'month_real':'23000',
                'year_job':'6500',
                'year_jam':'15000',
                'year_ton':'20000',
                'year_rkap':'23000',
                'year_real':'23000',
                'total_group':'7',
            },
            {
                'group':'BATU KAPUR',
                'produksi':'SUB TOTAL T 1',
                'today_job':'8000',
                'today_jam':'15000',
                'today_ton':'6000',
                'month_job':'6500',
                'month_jam':'15000',
                'month_ton':'20000',
                'month_rkap':'23000',
                'month_real':'23000',
                'year_job':'6500',
                'year_jam':'15000',
                'year_ton':'20000',
                'year_rkap':'23000',
                'year_real':'23000',
                'total_group':'7',
            },
            {
                'group':'BATU KAPUR',
                'produksi':'CRUSSER #3',
                'today_job':'8000',
                'today_jam':'15000',
                'today_ton':'6000',
                'month_job':'6500',
                'month_jam':'15000',
                'month_ton':'20000',
                'month_rkap':'23000',
                'month_real':'23000',
                'year_job':'6500',
                'year_jam':'15000',
                'year_ton':'20000',
                'year_rkap':'23000',
                'year_real':'23000',
                'total_group':'7',
            },
            {
                'group':'BATU KAPUR',
                'produksi':'CRUSSER #4',
                'today_job':'8000',
                'today_jam':'15000',
                'today_ton':'6000',
                'month_job':'6500',
                'month_jam':'15000',
                'month_ton':'20000',
                'month_rkap':'23000',
                'month_real':'23000',
                'year_job':'6500',
                'year_jam':'15000',
                'year_ton':'20000',
                'year_rkap':'23000',
                'year_real':'23000',
                'total_group':'7',
            },
            {
                'group':'BATU KAPUR',
                'produksi':'SUB TOTAL T 2',
                'today_job':'8000',
                'today_jam':'15000',
                'today_ton':'6000',
                'month_job':'6500',
                'month_jam':'15000',
                'month_ton':'20000',
                'month_rkap':'23000',
                'month_real':'23000',
                'year_job':'6500',
                'year_jam':'15000',
                'year_ton':'20000',
                'year_rkap':'23000',
                'year_real':'23000',
                'total_group':'7',
            },
            {
                'group':'BATU KAPUR',
                'produksi':'total',
                'today_job':'8000',
                'today_jam':'15000',
                'today_ton':'6000',
                'month_job':'6500',
                'month_jam':'15000',
                'month_ton':'20000',
                'month_rkap':'23000',
                'month_real':'23000',
                'year_job':'6500',
                'year_jam':'15000',
                'year_ton':'20000',
                'year_rkap':'23000',
                'year_real':'23000',
                'total_group':'7',
            },

            {
                'group':'TANAH LIAT',
                'produksi':'CRUSSER #1',
                'today_job':'8000',
                'today_jam':'15000',
                'today_ton':'6000',
                'month_job':'6500',
                'month_jam':'15000',
                'month_ton':'20000',
                'month_rkap':'23000',
                'month_real':'23000',
                'year_job':'6500',
                'year_jam':'15000',
                'year_ton':'20000',
                'year_rkap':'23000',
                'year_real':'23000',
                'total_group':'7',
            },
            {
                'group':'TANAH LIAT',
                'produksi':'CRUSSER #2',
                'today_job':'8000',
                'today_jam':'15000',
                'today_ton':'6000',
                'month_job':'6500',
                'month_jam':'15000',
                'month_ton':'20000',
                'month_rkap':'23000',
                'month_real':'23000',
                'year_job':'6500',
                'year_jam':'15000',
                'year_ton':'20000',
                'year_rkap':'23000',
                'year_real':'23000',
                'total_group':'7',
            },
            {
                'group':'TANAH LIAT',
                'produksi':'SUB TOTAL T 1',
                'today_job':'8000',
                'today_jam':'15000',
                'today_ton':'6000',
                'month_job':'6500',
                'month_jam':'15000',
                'month_ton':'20000',
                'month_rkap':'23000',
                'month_real':'23000',
                'year_job':'6500',
                'year_jam':'15000',
                'year_ton':'20000',
                'year_rkap':'23000',
                'year_real':'23000',
                'total_group':'7',
            },
            {
                'group':'TANAH LIAT',
                'produksi':'CRUSSER #3',
                'today_job':'8000',
                'today_jam':'15000',
                'today_ton':'6000',
                'month_job':'6500',
                'month_jam':'15000',
                'month_ton':'20000',
                'month_rkap':'23000',
                'month_real':'23000',
                'year_job':'6500',
                'year_jam':'15000',
                'year_ton':'20000',
                'year_rkap':'23000',
                'year_real':'23000',
                'total_group':'7',
            },
            {
                'group':'TANAH LIAT',
                'produksi':'CRUSSER #4',
                'today_job':'8000',
                'today_jam':'15000',
                'today_ton':'6000',
                'month_job':'6500',
                'month_jam':'15000',
                'month_ton':'20000',
                'month_rkap':'23000',
                'month_real':'23000',
                'year_job':'6500',
                'year_jam':'15000',
                'year_ton':'20000',
                'year_rkap':'23000',
                'year_real':'23000',
                'total_group':'7',
            },
            {
                'group':'TANAH LIAT',
                'produksi':'SUB TOTAL T 2',
                'today_job':'8000',
                'today_jam':'15000',
                'today_ton':'6000',
                'month_job':'6500',
                'month_jam':'15000',
                'month_ton':'20000',
                'month_rkap':'23000',
                'month_real':'23000',
                'year_job':'6500',
                'year_jam':'15000',
                'year_ton':'20000',
                'year_rkap':'23000',
                'year_real':'23000',
                'total_group':'7',
            },
            {
                'group':'TANAH LIAT',
                'produksi':'total',
                'today_job':'8000',
                'today_jam':'15000',
                'today_ton':'6000',
                'month_job':'6500',
                'month_jam':'15000',
                'month_ton':'20000',
                'month_rkap':'23000',
                'month_real':'23000',
                'year_job':'6500',
                'year_jam':'15000',
                'year_ton':'20000',
                'year_rkap':'23000',
                'year_real':'23000',
                'total_group':'7',
            },
        ];

        var table = $('#tb_laporan_produksi_tahun').DataTable({
            processing: true,
            // serverSide: true,
            responsive: false,
            scrollX: true,
            paging: false,
            ordering: false,
            data: data_laporan_produksi_tahun,
            columns: [
                {
                data: 'group',
                name: 'group',
                },
                {
                data: 'produksi',
                name: 'produksi'
                },
                {
                data: 'today_job',
                name: 'today_job'
                },
                {
                data: 'today_jam',
                name: 'today_jam'
                },
                {
                data: 'today_ton',
                name: 'today_ton'
                },
                {
                data: 'month_job',
                name: 'month_job'
                },
                {
                data: 'month_jam',
                name: 'month_jam'
                },
                {
                data: 'month_ton',
                name: 'month_ton'
                },
                {
                data: 'month_rkap',
                name: 'month_rkap'
                },
                {
                data: 'month_real',
                name: 'month_real'
                },
                {
                data: 'year_job',
                name: 'year_job'
                },
                {
                data: 'year_jam',
                name: 'year_jam'
                },
                {
                data: 'year_ton',
                name: 'year_ton'
                },
                {
                data: 'year_rkap',
                name: 'year_rkap'
                },
                {
                data: 'year_real',
                name: 'year_real'
                },
                {
                data: 'total_group',
                name: 'total_group',
                searchable: false,
                visible : false
                },
            ],
            "initComplete": function(settings, json) {
                    processColumnProduksi( $('#tb_laporan_produksi_tahun').DataTable() );
            },
            rowCallback: function(row, data, index){
                if(data['produksi'] == 'total'){
                    $(row).find('td:eq(1)').css('background-color', '#FDFD17');
                    $(row).find('td:eq(2)').css('background-color', '#FDFD17');
                    $(row).find('td:eq(3)').css('background-color', '#FDFD17');
                    $(row).find('td:eq(4)').css('background-color', '#FDFD17');
                    $(row).find('td:eq(5)').css('background-color', '#FDFD17');
                    $(row).find('td:eq(6)').css('background-color', '#FDFD17');
                    $(row).find('td:eq(7)').css('background-color', '#FDFD17');
                    $(row).find('td:eq(8)').css('background-color', '#FDFD17');
                    $(row).find('td:eq(9)').css('background-color', '#FDFD17');
                    $(row).find('td:eq(10)').css('background-color', '#FDFD17');
                    $(row).find('td:eq(11)').css('background-color', '#FDFD17');
                    $(row).find('td:eq(12)').css('background-color', '#FDFD17');
                    $(row).find('td:eq(13)').css('background-color', '#FDFD17');
                    $(row).find('td:eq(14)').css('background-color', '#FDFD17');
                }
            },
        });

        table.on( 'draw', function () {
            processColumnProduksi( $('#tb_laporan_produksi_tahun').DataTable() );
        } );

        function processColumnProduksi(tbl) {
            var selector_modifier = { order: 'current', page: 'current', search: 'applied' }
            var previous = '';
            var officeNodes = tbl.column(0, selector_modifier).nodes();
            var officeData = tbl.column(0, selector_modifier).data();
            var total = tbl.column(15, selector_modifier).data();
                for (var i = 0; i < officeData.length; i++) {
                    var current = officeData[i];
                    if (current === previous) {
                        officeNodes[i].textContent = '';
                        officeNodes[i].setAttribute("style", "display:none;");
                    } else {
                        officeNodes[i].setAttribute("rowspan", total[i]);
                        officeNodes[i].textContent = officeData[i];
                    }
                        previous = current;
                }
        }

        setTimeout(function(){
            if ($('#alertNotif').length > 0) {
                $('#alertNotif').remove();
            }
        }, 5000)


    });
</script>

<x-default-layout>
    <div class="flex-stack mb-0 mb-lg-n4 pt-5">
        <div class="row">
            <div class="col-sm-6">
                <div class="information">
                    <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{$pagetitle}}</h1>
                    <ul class="breadcrumb breadcrumb-separatorless ">
                        <li class="breadcrumb-item text-muted">
                            <a href="/" class="text-muted text-hover-primary">{{$pagetitle}}</a>
                        </li>
                        <li class="breadcrumb-item text-muted">/</li>
                        <li class="breadcrumb-item text-ghopo">Dashboard Production Report</li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-6 d-flex justify-content-md-end flex-no-wrap px-5">
                <div class="filter">
                    <div class="row">
                        <div class="col-4 mt-2 dropdown-filter">
                            <select class="form-select h-25" data-control="select2" data-placeholder="Pilih Periode">
                                <option></option>
                                <option value="1">Option 1</option>
                                <option value="2">Option 2</option>
                            </select>
                        </div>
                        <div class="col-4 mt-2 dropdown-filter">
                            <select class="form-select h-25" data-control="select2" data-placeholder="Pilih Tahun">
                                @foreach(getYear() as $key => $value)
                                    @if ($value == date("Y") )
                                        <option value="{{$value}}" selected>{{$value}}</option>
                                    @else
                                        <option value="{{$value}}">{{$value}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-4 mt-2 dropdown-filter">
                            <button class="btn btn-primary">Tampilkan</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>

    <div class="card card-flush mt-6 mt-xl-9">
        {{-- hop --}}
        <div class="ms-3 me-3 mt-3 mb-3">
            <ul class="nav nav-tabs nav-line-tabs mb-5 fs-6">
                <li class="nav-item">
                    <a class="nav-link active" data-bs-toggle="tab" href="#kt_tab_pane_1" onclick="myDashboard()">DASHBOARD</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-bs-toggle="tab" href="#kt_tab_pane_2" onclick="myListData()">LIST DATA</a>
                </li>
            </ul>

            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="kt_tab_pane_1" role="tabpanel">
                    <div class="card-body mt-6 pt-0">
                        <div class="row">
                            <div class="col-12">
                                <div class="row">
                                    <h3>RESUME LAPORAN HARIAN PRODUKSI</h3>
                                    <table class="table table-bordered data-table" id="tb_laporan_harian_produksi">
                                        <thead>
                                            <tr>
                                                <th class="align-middle text-center color-header-tabel">PRODUKSI</th>
                                                <th class="align-middle text-center color-header-tabel">TUBAN 1</th>
                                                <th class="align-middle text-center color-header-tabel">TUBAN 2</th>
                                                <th class="align-middle text-center color-header-tabel">TUBAN 3</th>
                                                <th class="align-middle text-center color-header-tabel">TUBAN 4</th>
                                                <th class="align-middle text-center color-header-tabel">TOTAL TUBAN</th>
                                                <th class="align-middle text-center color-header-tabel">GRESIK</th>
                                                <th class="align-middle text-center color-header-tabel">TOTAL</th>
                                            </tr>
                                        </thead>
                                        <tfoot align="right">
                                            <tr>
                                                <th class="color-total-tabel" colspan="5"></th>
                                                <th class="color-total-tabel"></th>
                                                <th class="color-total-tabel"></th>
                                                <th class="color-total-tabel"></th>
                                            </tr>
                                            <tr>
                                                <th class="color-total-tabel" colspan="5"></th>
                                                <th class="color-total-tabel"></th>
                                                <th class="color-total-tabel"></th>
                                                <th class="color-total-tabel"></th>
                                            </tr>
                                            <tr>
                                                <th class="color-total-tabel" colspan="5"></th>
                                                <th class="color-total-tabel"></th>
                                                <th class="color-total-tabel"></th>
                                                <th class="color-total-tabel"></th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="g-5 g-xl-10 mt-4 mb-3 mb-xl-3">
                        <div class="row">
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-sm-6">
                                        @include('partials/widgets/cards/_widget-realisasi-thd-rkap')
                                    </div>
                                    <div class="col-sm-6">
                                        @include('partials/widgets/cards/_widget-inventory-semen')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card card-flush mt-6 mt-xl-9">
                        <div class="card-body mt-6 pt-0">
                            <div class="row">
                                <div class="col-12">
                                    <div class="row">
                                        <table class="table table-bordered data-table" id="tb_packer">
                                            <thead>
                                                <tr>
                                                    <th class="align-middle text-center color-header-tabel">PACKER</th>
                                                    <th class="align-middle text-center color-header-tabel">FM#1</th>
                                                    <th class="align-middle text-center color-header-tabel">FM#2</th>
                                                    <th class="align-middle text-center color-header-tabel">FM#9</th>
                                                    <th class="align-middle text-center color-header-tabel">SILO 1</th>
                                                    <th class="align-middle text-center color-header-tabel">SILO 2</th>
                                                    <th class="align-middle text-center color-header-tabel">SILO 3</th>
                                                    <th class="align-middle text-center color-header-tabel">SILO 4</th>
                                                    <th class="align-middle text-center color-header-tabel">TOTAL</th>
                                                </tr>
                                            </thead>
                                            <tfoot align="right">
                                                <tr>
                                                    <th class="color-total-tabel" colspan="4"></th>
                                                    <th class="color-total-tabel"></th>
                                                    <th class="color-total-tabel"></th>
                                                    <th class="color-total-tabel"></th>
                                                    <th class="color-total-tabel"></th>
                                                    <th class="color-total-tabel"></th>
                                                </tr>
                                                <tr>
                                                    <th class="color-total-tabel" colspan="4"></th>
                                                    <th class="color-total-tabel"></th>
                                                    <th class="color-total-tabel"></th>
                                                    <th class="color-total-tabel"></th>
                                                    <th class="color-total-tabel"></th>
                                                    <th class="color-total-tabel"></th>
                                                </tr>
                                                <tr>
                                                    <th class="color-total-tabel" colspan="4"></th>
                                                    <th class="color-total-tabel"></th>
                                                    <th class="color-total-tabel"></th>
                                                    <th class="color-total-tabel"></th>
                                                    <th class="color-total-tabel"></th>
                                                    <th class="color-total-tabel"></th>
                                                </tr>
                                            </tfoot>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="tab-pane fade" id="kt_tab_pane_2" role="tabpanel">
                    {{-- <div class="card card-flush mt-6 mt-xl-9"> --}}
                        <div class="card-body mt-6 pt-0">
                            <div class="row">
                                <div class="col-12">
                                    <div class="row">
                                        <table class="table table-bordered data-table" id="tb_laporan_produksi_tahun">
                                            <thead>
                                                <tr>
                                                    <th class="align-middle text-center color-header-tabel">GROUP</th>
                                                    <th class="align-middle text-center color-header-tabel">PRODUKSI</th>
                                                    <th class="align-middle text-center color-header-tabel">HARI INI</th>
                                                    <th class="align-middle text-center color-header-tabel">REAL JAN</th>
                                                    <th class="align-middle text-center color-header-tabel">RKAP JAN</th>
                                                    <th class="align-middle text-center color-header-tabel">% REAL</th>
                                                    <th class="align-middle text-center color-header-tabel">REAL THN JLN S/D JAN</th>
                                                    <th class="align-middle text-center color-header-tabel">RKAP THN JLN S/D JAN</th>
                                                    <th class="align-middle text-center color-header-tabel">%REAL THN S/D JAN</th>
                                                    <th class="align-middle text-center color-header-tabel">RKAP TH. 2021</th>
                                                    <th class="align-middle text-center color-header-tabel">% REAL THD 1 THN</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    {{-- </div> --}}

                    <div class="card card-flush mt-6 mt-xl-9">
                        <div class="card-body mt-6 pt-0">
                            <div class="row">
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md col-lg-6">
                                            <div class="card-body">
                                                <table class="table table-bordered data-table" id="tb_laporan_stock_terak">
                                                    <thead>
                                                        <tr>
                                                            <th class="align-middle text-center color-header-tabel" colspan="4">STOCK TERAK</th>
                                                        </tr>
                                                        <tr>
                                                            <th class="align-middle text-end"></th>
                                                            <th class="align-middle text-end"></th>
                                                            <th class="align-middle text-end"></th>
                                                            <th class="align-middle text-end">MAX CAPACITY</th>
                                                        </tr>
                                                    </thead>
                                                    <tfoot align="right">
                                                        <tr>
                                                            <th class="color-total-tabel"></th>
                                                            <th class="color-total-tabel"></th>
                                                            <th class="color-total-tabel"></th>
                                                            <th class="color-total-tabel"></th>
                                                        </tr>
                                                    </tfoot>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md col-lg-6">
                                            <div class="card-body">
                                                <table class="table table-bordered data-table" id="tb_laporan_stock_semen">
                                                    <thead>
                                                        <tr>
                                                            <th class="align-middle text-center color-header-tabel" colspan="4">STOCK SEMEN</th>
                                                        </tr>
                                                        <tr>
                                                            <th class="align-middle text-end" ></th>
                                                            <th class="align-middle text-end" ></th>
                                                            <th class="align-middle text-end" ></th>
                                                            <th class="align-middle text-end" >MAX CAPACITY</th>
                                                        </tr>
                                                    </thead>
                                                    <tfoot align="right">
                                                        <tr>
                                                            <th class="color-total-tabel"></th>
                                                            <th class="color-total-tabel"></th>
                                                            <th class="color-total-tabel"></th>
                                                            <th class="color-total-tabel"></th>
                                                        </tr>
                                                    </tfoot>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>




    </div>


</x-default-layout>
<script>
    $(document).ready(function() {
        myDashboard();
    });
    function myDashboard() {

        var data_laporan_harian = [
            {
                'produksi':'Terak',
                'tuban_satu':'8000',
                'tuban_dua':'15000',
                'tuban_tiga':'6000',
                'tuban_empat':'6500',
                'total_tuban':'15000',
                'gresik':'20000',
                'total':'23000',
            },
            {
                'produksi':'Terak',
                'tuban_satu':'8000',
                'tuban_dua':'15000',
                'tuban_tiga':'6000',
                'tuban_empat':'6500',
                'total_tuban':'15000',
                'gresik':'20000',
                'total':'23000',
            },
        ];

        var table = $('#tb_laporan_harian_produksi').DataTable({
            processing: true,
            // serverSide: true,
            ordering: false,
            responsive: false,
            scrollX: true,
            // paging: false,
            data: data_laporan_harian,
            columns: [
                {
                data: 'produksi',
                name: 'produksi'
                },
                {
                data: 'tuban_satu',
                name: 'tuban_satu'
                },
                {
                data: 'tuban_dua',
                name: 'tuban_dua'
                },
                {
                data: 'tuban_tiga',
                name: 'tuban_tiga'
                },
                {
                data: 'tuban_empat',
                name: 'tuban_empat'
                },
                {
                data: 'total_tuban',
                name: 'total_tuban'
                },
                {
                data: 'gresik',
                name: 'gresik'
                },
                {
                data: 'total',
                name: 'total'
                },
            ],
            footerCallback: function ( row, data, start, end, display ) {
                var api = this.api(), data;
                // converting to interger to find total
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                var friTotal = api
                        .column( 5 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                var Totala = api
                        .column( 6 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                var Totalb = api
                        .column( 7 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    $( api.column( 0 ).footer() ).html('% REALEASE SEMEN NON OPC (BULAN BERJALAN)');
                    $( api.column( 5 ).footer() ).html(friTotal);
                    $( api.column( 6 ).footer() ).html(Totala);
                    $( api.column( 7 ).footer() ).html(Totalb);

                    $( 'tr:eq(1) th:eq(0)',api.table().footer() ).html('% PRODUKSI SEMEN NON OPC (BULAN BERJALAN)');
                    $( 'tr:eq(1) th:eq(1)',api.table().footer() ).html(friTotal);
                    $( 'tr:eq(1) th:eq(2)',api.table().footer() ).html(Totala);
                    $( 'tr:eq(1) th:eq(3)',api.table().footer() ).html(Totalb);

                    $( 'tr:eq(2) th:eq(0)',api.table().footer() ).html('% STOCK SEMEN NON OPC');
                    $( 'tr:eq(2) th:eq(1)',api.table().footer() ).html(friTotal);
                    $( 'tr:eq(2) th:eq(2)',api.table().footer() ).html(Totala);
                    $( 'tr:eq(2) th:eq(3)',api.table().footer() ).html(Totalb);
            },
        });

        var data_packer = [
            {
                'packer':'Tuban 1',
                'fm_satu':'RUN',
                'fm_dua':'RUN',
                'fm_sembilan':'STOP',
                'silo_satu':'6500',
                'silo_dua':'15000',
                'silo_tiga':'20000',
                'silo_empat':'20000',
                'total':'23000',
            },
            {
                'packer':'Tuban 2',
                'fm_satu':'RUN',
                'fm_dua':'STOP',
                'fm_sembilan':'RUN',
                'silo_satu':'6500',
                'silo_dua':'15000',
                'silo_tiga':'20000',
                'silo_empat':'20000',
                'total':'23000',
            },
            {
                'packer':'Tuban 3',
                'fm_satu':'STOP',
                'fm_dua':'RUN',
                'fm_sembilan':'RUN',
                'silo_satu':'6500',
                'silo_dua':'15000',
                'silo_tiga':'20000',
                'silo_empat':'20000',
                'total':'23000',
            },
            {
                'packer':'Tuban 4',
                'fm_satu':'RUN',
                'fm_dua':'RUN',
                'fm_sembilan':'RUN',
                'silo_satu':'6500',
                'silo_dua':'15000',
                'silo_tiga':'20000',
                'silo_empat':'20000',
                'total':'23000',
            },
        ];

        var table = $('#tb_packer').DataTable({
            processing: true,
            // serverSide: true,
            ordering: false,
            responsive: false,
            scrollX: true,
            // paging: false,
            data: data_packer,
            columns: [
                {
                data: 'packer',
                name: 'packer'
                },
                {
                data: 'fm_satu',
                name: 'fm_satu'
                },
                {
                data: 'fm_dua',
                name: 'fm_dua'
                },
                {
                data: 'fm_sembilan',
                name: 'fm_sembilan'
                },
                {
                data: 'silo_satu',
                name: 'silo_satu'
                },
                {
                data: 'silo_dua',
                name: 'silo_dua'
                },
                {
                data: 'silo_tiga',
                name: 'silo_tiga'
                },
                {
                data: 'silo_empat',
                name: 'silo_empat'
                },
                {
                data: 'total',
                name: 'total'
                },
            ],
            rowCallback: function(row, data, index){
                if(data['fm_satu'] == 'RUN'){
                    $(row).find('td:eq(1)').css('background-color', '#61CA7C');
                }else if (data['fm_satu'] == 'STOP') {
                    $(row).find('td:eq(1)').css('background-color', '#FE0000');
                }

                if(data['fm_dua'] == 'RUN'){
                    $(row).find('td:eq(2)').css('background-color', '#61CA7C');
                }else if (data['fm_dua'] == 'STOP') {
                    $(row).find('td:eq(2)').css('background-color', '#FE0000');
                }

                if(data['fm_sembilan'] == 'RUN'){
                    $(row).find('td:eq(3)').css('background-color', '#61CA7C');
                }else if (data['fm_sembilan'] == 'STOP') {
                    $(row).find('td:eq(3)').css('background-color', '#FE0000');
                }
            },
            footerCallback: function ( row, data, start, end, display ) {
                var api = this.api(), data;
                // converting to interger to find total
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                var thuTotal = api
                        .column( 4 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                var friTotal = api
                        .column( 5 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                var Totala = api
                        .column( 6 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                var Totalb = api
                        .column( 7 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                var Totalc = api
                        .column( 8 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    $( api.column( 0 ).footer() ).html('GRESIK NON OPC');
                    $( api.column( 4 ).footer() ).html(thuTotal);
                    $( api.column( 5 ).footer() ).html(friTotal);
                    $( api.column( 6 ).footer() ).html(Totala);
                    $( api.column( 7 ).footer() ).html(Totalb);
                    $( api.column( 8 ).footer() ).html(Totalc);

                    $( 'tr:eq(1) th:eq(0)',api.table().footer() ).html('% PRODUKSI SEMEN NON OPC (BULAN BERJALAN)');
                    $( 'tr:eq(1) th:eq(1)',api.table().footer() ).html(thuTotal);
                    $( 'tr:eq(1) th:eq(2)',api.table().footer() ).html(friTotal);
                    $( 'tr:eq(1) th:eq(3)',api.table().footer() ).html(Totala);
                    $( 'tr:eq(1) th:eq(4)',api.table().footer() ).html(Totalb);
                    $( 'tr:eq(1) th:eq(5)',api.table().footer() ).html(Totalc);

                    $( 'tr:eq(2) th:eq(0)',api.table().footer() ).html('% STOCK SEMEN NON OPC');
                    $( 'tr:eq(2) th:eq(1)',api.table().footer() ).html(thuTotal);
                    $( 'tr:eq(2) th:eq(2)',api.table().footer() ).html(friTotal);
                    $( 'tr:eq(2) th:eq(3)',api.table().footer() ).html(Totala);
                    $( 'tr:eq(2) th:eq(4)',api.table().footer() ).html(Totalb);
                    $( 'tr:eq(2) th:eq(5)',api.table().footer() ).html(Totalc);
            },
        });
    }

    function myListData() {
        var data_laporan_produksi_tahun = [
            {
                'group':'terak',
                'produksi':'kiln 1',
                'hari_ini':'8000',
                'real_januari':'15000',
                'rkap_januari':'6000',
                'persen_real':'6500',
                'real_bulan_tahun':'15000',
                'rkap_bulan_tahun':'20000',
                'persen_real_bulan':'23000',
                'rkap_tahun':'23000',
                'persen_real_tahun':'23000',
            },
            {
                'group':'terak',
                'produksi':'kiln 2',
                'hari_ini':'8000',
                'real_januari':'15000',
                'rkap_januari':'6000',
                'persen_real':'6500',
                'real_bulan_tahun':'15000',
                'rkap_bulan_tahun':'20000',
                'persen_real_bulan':'23000',
                'rkap_tahun':'23000',
                'persen_real_tahun':'23000',
            },
            {
                'group':'terak',
                'produksi':'kiln 3',
                'hari_ini':'8000',
                'real_januari':'15000',
                'rkap_januari':'6000',
                'persen_real':'6500',
                'real_bulan_tahun':'15000',
                'rkap_bulan_tahun':'20000',
                'persen_real_bulan':'23000',
                'rkap_tahun':'23000',
                'persen_real_tahun':'23000',
            },
            {
                'group':'terak',
                'produksi':'kiln 4',
                'hari_ini':'8000',
                'real_januari':'15000',
                'rkap_januari':'6000',
                'persen_real':'6500',
                'real_bulan_tahun':'15000',
                'rkap_bulan_tahun':'20000',
                'persen_real_bulan':'23000',
                'rkap_tahun':'23000',
                'persen_real_tahun':'23000',
            },
            {
                'group':'terak',
                'produksi':'total',
                'hari_ini':'8000',
                'real_januari':'15000',
                'rkap_januari':'6000',
                'persen_real':'6500',
                'real_bulan_tahun':'15000',
                'rkap_bulan_tahun':'20000',
                'persen_real_bulan':'23000',
                'rkap_tahun':'23000',
                'persen_real_tahun':'23000',
            },
            {
                'group':'SEMEN',
                'produksi':'kiln 1',
                'hari_ini':'8000',
                'real_januari':'15000',
                'rkap_januari':'6000',
                'persen_real':'6500',
                'real_bulan_tahun':'15000',
                'rkap_bulan_tahun':'20000',
                'persen_real_bulan':'23000',
                'rkap_tahun':'23000',
                'persen_real_tahun':'23000',
            },
            {
                'group':'SEMEN',
                'produksi':'kiln 2',
                'hari_ini':'8000',
                'real_januari':'15000',
                'rkap_januari':'6000',
                'persen_real':'6500',
                'real_bulan_tahun':'15000',
                'rkap_bulan_tahun':'20000',
                'persen_real_bulan':'23000',
                'rkap_tahun':'23000',
                'persen_real_tahun':'23000',
            },
            {
                'group':'SEMEN',
                'produksi':'kiln 3',
                'hari_ini':'8000',
                'real_januari':'15000',
                'rkap_januari':'6000',
                'persen_real':'6500',
                'real_bulan_tahun':'15000',
                'rkap_bulan_tahun':'20000',
                'persen_real_bulan':'23000',
                'rkap_tahun':'23000',
                'persen_real_tahun':'23000',
            },
            {
                'group':'SEMEN',
                'produksi':'kiln 4',
                'hari_ini':'8000',
                'real_januari':'15000',
                'rkap_januari':'6000',
                'persen_real':'6500',
                'real_bulan_tahun':'15000',
                'rkap_bulan_tahun':'20000',
                'persen_real_bulan':'23000',
                'rkap_tahun':'23000',
                'persen_real_tahun':'23000',
            },
            {
                'group':'SEMEN',
                'produksi':'total',
                'hari_ini':'8000',
                'real_januari':'15000',
                'rkap_januari':'6000',
                'persen_real':'6500',
                'real_bulan_tahun':'15000',
                'rkap_bulan_tahun':'20000',
                'persen_real_bulan':'23000',
                'rkap_tahun':'23000',
                'persen_real_tahun':'23000',
            },
            {
                'group':'SEMEN 1',
                'produksi':'kiln 1',
                'hari_ini':'8000',
                'real_januari':'15000',
                'rkap_januari':'6000',
                'persen_real':'6500',
                'real_bulan_tahun':'15000',
                'rkap_bulan_tahun':'20000',
                'persen_real_bulan':'23000',
                'rkap_tahun':'23000',
                'persen_real_tahun':'23000',
            },
        ];

        var table = $('#tb_laporan_produksi_tahun').DataTable({
            processing: true,
            // serverSide: true,
            ordering: false,
            responsive: false,
            scrollX: true,
            // paging: false,
            data: data_laporan_produksi_tahun,
            columns: [
                {
                data: 'group',
                name: 'group',
                visible: false
                },
                {
                data: 'produksi',
                name: 'produksi'
                },
                {
                data: 'hari_ini',
                name: 'hari_ini'
                },
                {
                data: 'real_januari',
                name: 'real_januari'
                },
                {
                data: 'rkap_januari',
                name: 'rkap_januari'
                },
                {
                data: 'persen_real',
                name: 'persen_real'
                },
                {
                data: 'real_bulan_tahun',
                name: 'real_bulan_tahun'
                },
                {
                data: 'rkap_bulan_tahun',
                name: 'rkap_bulan_tahun'
                },
                {
                data: 'persen_real_bulan',
                name: 'persen_real_bulan'
                },
                {
                data: 'rkap_tahun',
                name: 'rkap_tahun'
                },
                {
                data: 'persen_real_tahun',
                name: 'persen_real_tahun'
                },
            ],
            drawCallback: function (settings) {
                var api = this.api();
                var rows = api.rows({ page: 'current' }).nodes();
                var last = null;
                api
                    .column(0, { page: 'current' })
                    .data()
                    .each(function (group, i) {
                        if (last !== group) {
                            $(rows)
                                .eq(i)
                                .before('<tr style="background-color: #D0D0D0;"><td colspan="10"><b>' + group + '</td></tr>');
                            last = group;
                        }
                    });
            },
            rowCallback: function(row, data, index){
                if(data['produksi'] == 'total'){
                    $(row).find('td:eq(0)').css('background-color', '#FDFD17');
                    $(row).find('td:eq(1)').css('background-color', '#FDFD17');
                    $(row).find('td:eq(2)').css('background-color', '#FDFD17');
                    $(row).find('td:eq(3)').css('background-color', '#FDFD17');
                    $(row).find('td:eq(4)').css('background-color', '#FDFD17');
                    $(row).find('td:eq(5)').css('background-color', '#FDFD17');
                    $(row).find('td:eq(6)').css('background-color', '#FDFD17');
                    $(row).find('td:eq(7)').css('background-color', '#FDFD17');
                    $(row).find('td:eq(8)').css('background-color', '#FDFD17');
                    $(row).find('td:eq(9)').css('background-color', '#FDFD17');
                }
            },
        });

        var data_laporan_stock_terak = [
            {
                'categori':'Tuban 1',
                'total_satu':'8000',
                'total_dua':'15000',
                'max_capacity':'6000',
            },
            {
                'categori':'Tuban 2',
                'total_satu':'8000',
                'total_dua':'15000',
                'max_capacity':'6000',
            },
            {
                'categori':'Tuban 3',
                'total_satu':'8000',
                'total_dua':'15000',
                'max_capacity':'6000',
            },
            {
                'categori':'Tuban 4',
                'total_satu':'8000',
                'total_dua':'15000',
                'max_capacity':'6000',
            },
            {
                'categori':'Yard',
                'total_satu':'8000',
                'total_dua':'15000',
                'max_capacity':'6000',
            },
            {
                'categori':'Gresik',
                'total_satu':'8000',
                'total_dua':'15000',
                'max_capacity':'6000',
            },
        ];

        var table = $('#tb_laporan_stock_terak').DataTable({
            processing: true,
            // serverSide: true,
            ordering: false,
            responsive: false,
            scrollX: true,
            // paging: false,
            data: data_laporan_stock_terak,
            columns: [
                {
                data: 'categori',
                name: 'categori'
                },
                {
                data: 'total_satu',
                name: 'total_satu'
                },
                {
                data: 'total_dua',
                name: 'total_dua'
                },
                {
                data: 'max_capacity',
                name: 'max_capacity'
                },
            ],
            footerCallback: function ( row, data, start, end, display ) {
                var api = this.api(), data;
                // converting to interger to find total
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                var friTotal = api
                        .column( 1 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                var Totala = api
                        .column( 2 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                var Totalb = api
                        .column( 3 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    $( api.column( 0 ).footer() ).html('Total');
                    $( api.column( 1 ).footer() ).html(friTotal);
                    $( api.column( 2 ).footer() ).html(Totala);
                    $( api.column( 3 ).footer() ).html(Totalb);

            },
        });

        var data_laporan_stock_semen = [
            {
                'categori':'Tuban 1',
                'total_satu':'8000',
                'total_dua':'15000',
                'max_capacity':'6000',
            },
            {
                'categori':'Tuban 2',
                'total_satu':'8000',
                'total_dua':'15000',
                'max_capacity':'6000',
            },
            {
                'categori':'Tuban 3',
                'total_satu':'8000',
                'total_dua':'15000',
                'max_capacity':'6000',
            },
            {
                'categori':'Tuban 4',
                'total_satu':'8000',
                'total_dua':'15000',
                'max_capacity':'6000',
            },
            {
                'categori':'Yard',
                'total_satu':'8000',
                'total_dua':'15000',
                'max_capacity':'6000',
            },
            {
                'categori':'Gresik',
                'total_satu':'8000',
                'total_dua':'15000',
                'max_capacity':'6000',
            },
        ];

        var table = $('#tb_laporan_stock_semen').DataTable({
            processing: true,
            // serverSide: true,
            ordering: false,
            responsive: false,
            scrollX: true,
            // paging: false,
            data: data_laporan_stock_semen,
            columns: [
                {
                data: 'categori',
                name: 'categori'
                },
                {
                data: 'total_satu',
                name: 'total_satu'
                },
                {
                data: 'total_dua',
                name: 'total_dua'
                },
                {
                data: 'max_capacity',
                name: 'max_capacity'
                },
            ],
            footerCallback: function ( row, data, start, end, display ) {
                var api = this.api(), data;
                // converting to interger to find total
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                var friTotal = api
                        .column( 1 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                var Totala = api
                        .column( 2 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                var Totalb = api
                        .column( 3 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    $( api.column( 0 ).footer() ).html('Total');
                    $( api.column( 1 ).footer() ).html(friTotal);
                    $( api.column( 2 ).footer() ).html(Totala);
                    $( api.column( 3 ).footer() ).html(Totalb);

            },
        });
    }


        setTimeout(function(){
            if ($('#alertNotif').length > 0) {
                $('#alertNotif').remove();
            }
        }, 5000)
</script>

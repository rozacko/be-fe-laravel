<x-default-layout>
  <div class="flex-stack mb-0 mb-lg-n4 pt-5">
    <div class="row">
      <div class="col-sm-5">
        <div class="information">
          <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">Rkap Plant Performance</h1>
          <ul class="breadcrumb breadcrumb-separatorless ">
            <li class="breadcrumb-item text-muted">
              <a href="/" class="text-muted text-hover-primary">Data & Rkap Performance</a>
            </li>
            <li class="breadcrumb-item text-muted">/</li>
            <li class="breadcrumb-item text-ghopo">Rkap Plant Performance</li>
          </ul>
        </div>
      </div>
      <div class="col-sm-7 d-flex justify-content-md-end flex-no-wrap px-5">
        <div class="filter">
          <div class="row">
            <div class="col">
              <select class="form-select h-25" data-control="select2" id="filter_plant" name='filter_plant'>
                <option value="">All Plant</option>
              </select>
            </div>
            <div class="col">
              <select class="form-select h-25" data-control="select2" data-placeholder="Tahun" name="filter_tahun" id="filter_tahun">
                <option></option>
                @foreach(getYear() as $key => $value)
                @if ($value == date("Y") )
                <option value="{{$value}}" selected>{{$value}}</option>
                @else
                <option value="{{$value}}">{{$value}}</option>
                @endif
                @endforeach
              </select>
            </div>
            <div class="col">
              <button id="filter" name="filter" class="btn btn-light-primary">Tampilkan</button>
            </div>
            <div class="col">
              <button class="d-flex btn btn-outline-info" id="btn-download" name="btn-download">
                <i class="bi bi-download flex"></i>
                Download
              </button>
            </div>
            <div class="col">
              <button class="d-flex btn btn-primary btn-import">
                <i class="bi bi-upload"></i>
                Import
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="modal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered mw-650px">
      <div class="modal-content rounded">
        <div class="mt-13 text-center">
          <h1 class="mb-3">Import Data</h1>
        </div>
        <form method="POST" action="#" name="idForm" enctype="multipart/formdata" id="idForm">
          <div class="modal-body">
            @csrf
            <div class="d-flex flex-column mb-8 fv-row form-group">
              <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                Template
                <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Download Template"></i>
              </label>
              <a href="{{ url('rkap-prognose/plant-performance/download-template') }}" target="_blank" class="btn btn-secondary">
                <span class="indicator-label">Download Template</span>
              </a>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
              <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                <span class="required">Upload File</span>
              </label>
              <input type="file" name="upload_file" style="display: flex !important;" required />
              <p class="mt-2" style="color: #1F7793;">*file yang di upload adalah file berdasarkan template yang disediakan</p>
            </div>
          </div>
          <div class="modal-footer">
            <div class="text-center">
              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
              <button type="submit" class="btn btn-primary" id="submitImport">
                <span class="indicator-label">Submit</span>
                <span class="indicator-progress">Please wait...
                  <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>


  <div class="card card-flush mt-6 mt-xl-9">
    <div class="card-body mt-6 pt-0">
      <h3>Rkap Plant Performance</h3>
      <table class="table table-bordered data-table" id="tb_rkap_plant_performance">
        <thead>
          <tr>
            <th class="align-middle text-center color-header-tabel">PLANT</th>
            <th class="align-middle text-center color-header-tabel">PARAMETER</th>
            <th class="align-middle text-center color-header-tabel">TAHUN</th>
            <th class="align-middle text-center color-header-tabel">JAN</th>
            <th class="align-middle text-center color-header-tabel">FEB</th>
            <th class="align-middle text-center color-header-tabel">MAR</th>
            <th class="align-middle text-center color-header-tabel">APR</th>
            <th class="align-middle text-center color-header-tabel">MEI</th>
            <th class="align-middle text-center color-header-tabel">JUN</th>
            <th class="align-middle text-center color-header-tabel">JUL</th>
            <th class="align-middle text-center color-header-tabel">AGU</th>
            <th class="align-middle text-center color-header-tabel">SEP</th>
            <th class="align-middle text-center color-header-tabel">OKT</th>
            <th class="align-middle text-center color-header-tabel">NOV</th>
            <th class="align-middle text-center color-header-tabel">DES</th>
            <th class="align-middle text-center color-header-tabel">ACTION</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>
  </div>


  <div class="modal fade" id="modal-update" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered mw-650px">
      <div class="modal-content rounded">
        <div class="mt-13 text-center">
          <h1 class="mb-3">Edit Data</h1>
        </div>
        <form method="POST" action="#" enctype="multipart/formdata" id="form-update" name="form-update" data-uuid="">
          <div class="modal-body">
            @csrf
            <div class="row mt-3">
              <div class="col">
                <label for=""><b>Januari</b></label>
                <input type="number" id="jan" name="jan" class="form-control">
              </div>
              <div class="col">
                <label for=""><b>Februari</b></label>
                <input type="number" id="feb" name="feb" class="form-control">
              </div>
            </div>
            <div class="row mt-3">
              <div class="col">
                <label for=""><b>Maret</b></label>
                <input type="number" id="mar" name="mar" class="form-control">
              </div>
              <div class="col">
                <label for=""><b>April</b></label>
                <input type="number" id="apr" name="apr" class="form-control">
              </div>
            </div>
            <div class="row mt-3">
              <div class="col">
                <label for=""><b>Mei</b></label>
                <input type="number" id="mei" name="mei" class="form-control">
              </div>
              <div class="col">
                <label for=""><b>Juni</b></label>
                <input type="number" id="jun" name="jun" class="form-control">
              </div>
            </div>
            <div class="row mt-3">
              <div class="col">
                <label for=""><b>Juli</b></label>
                <input type="number" id="jul" name="jul" class="form-control">
              </div>
              <div class="col">
                <label for=""><b>Agustus</b></label>
                <input type="number" id="ags" name="ags" class="form-control">
              </div>
            </div>
            <div class="row mt-3">
              <div class="col">
                <label for=""><b>September</b></label>
                <input type="number" id="sep" name="sep" class="form-control">
              </div>
              <div class="col">
                <label for=""><b>Oktober</b></label>
                <input type="number" id="okt" name="okt" class="form-control">
              </div>
            </div>
            <div class="row mt-3">
              <div class="col">
                <label for=""><b>November</b></label>
                <input type="number" id="nov" name="nov" class="form-control">
              </div>
              <div class="col">
                <label for=""><b>Desember</b></label>
                <input type="number" id="des" name="des" class="form-control">
              </div>
            </div>

            <div class="modal-footer">
              <div class="text-center">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary" id="submitUpdate">
                  <span class="indicator-label">Submit</span>
                </button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>


</x-default-layout>
<script>
  $(document).ready(function() {
    filterPlantRM()

    var table = $('#tb_rkap_plant_performance').DataTable({
      responsive: true,
      scrollX: true,
      processing: true,
      serverSide: true,
      ajax: {
        type: "get",
        url: "{{ route($route.'.table') }}",
        data: function(d) {
          d.tahun = $('#filter_tahun').val(),
            d.plant_id = $("#filter_plant").val()
        },
      },
      columns: [{
          data: 'plant_name',
          name: 'plant_name',
          className: 'text-center',
        },
        {
          data: 'paramter_name',
          name: 'paramter_name',
          className: 'text-center',
        },
        {
          data: 'tahun',
          name: 'tahun',
          className: 'text-center',
        },
        {
          data: 'jan',
          name: 'jan',
          className: 'text-center',
        },
        {
          data: 'feb',
          name: 'feb',
          className: 'text-center',
        },
        {
          data: 'mar',
          name: 'mar',
          className: 'text-center',
        },
        {
          data: 'apr',
          name: 'apr',
          className: 'text-center',
        },
        {
          data: 'mei',
          name: 'mei',
          className: 'text-center',
        },
        {
          data: 'jun',
          name: 'jun',
          className: 'text-center',
        },
        {
          data: 'jul',
          name: 'jul',
          className: 'text-center',
        },
        {
          data: 'ags',
          name: 'ags',
          className: 'text-center',
        },
        {
          data: 'sep',
          name: 'sep',
          className: 'text-center',
        },
        {
          data: 'okt',
          name: 'okt',
          className: 'text-center',
        },
        {
          data: 'nov',
          name: 'nov',
          className: 'text-center',
        },
        {
          data: 'des',
          name: 'des'
        },
        {
          data: 'uuid',
          render: function(data, t, f) {
            return "<button class='btn btn-primary btn-sm btn-edit-row' onclick=show('" + data + "') data-uuid='" + data + "'><i class='fa fa-pencil' aria-hidden='true'></i></button>";
          }
        },

      ],
      rowCallback: function(row, data, index) {}
    });

    $('.btn-import').click(function(e) {
      e.preventDefault();
      $('#modal').modal('show');
    });

    $("#idForm").submit(function(e) {
      e.preventDefault(); // avoid to execute the actual submit of the form.
      var formData = new FormData($(this)[0]);
      $.ajax({
        type: "POST",
        url: "{{ route($route . '.import') }}",
        data: formData,
        contentType: false,
        processData: false,
        beforeSend: function() {
          $('#submitImport').attr('disabled', true).html("<i class='fa fa-spinner fa-spin'></i> Processing");
        },
        success: function(data) {
          if (data.status == 'success') {
            Swal.fire(
              'Success',
              data.message,
              'success'
            );
            $('#submitImport').attr('disabled', false).html("<span class='indicator-label'>Submit</span>");
            $('#modal').modal('hide');
            $('#tb_rkap_plant_performance').DataTable().ajax.reload();
          } else {
            Swal.fire(
              'Warning',
              data.message,
              'warning'
            );
            $('#submitImport').attr('disabled', false).html("<span class='indicator-label'>Submit</span>");
          }
        }
      });
    });

    $("#form-update").submit(function(e) {
      e.preventDefault(); // avoid to execute the actual submit of the form.
      var formData = new FormData($(this)[0]);
      var form = $(this);
      let uuid = $(this).data('uuid');
      var urlUpdate = '{{ route($route.".update", ":id") }}';
      urlUpdate = urlUpdate.replace(':id', uuid);
      $.ajax({
        type: "PUT",
        url: urlUpdate,
        data: form.serialize(),
        beforeSend: function() {
          $('#submitUpdate').attr('disabled', true).html("<i class='fa fa-spinner fa-spin'></i> Processing");
        },
        success: function(data) {
          if (data.status == 'success') {
            Swal.fire(
              'Success',
              data.message,
              'success'
            );
            $('#submitUpdate').attr('disabled', false).html("<span class='indicator-label'>Submit</span>");
            $('#modal-update').modal('hide');
            $('#tb_rkap_plant_performance').DataTable().ajax.reload();
          } else {
            Swal.fire(
              'Warning',
              data.message,
              'warning'
            );
            $('#submitUpdate').attr('disabled', false).html("<span class='indicator-label'>Submit</span>");
          }
        }
      });
    });

    $('#filter').on('click', function(e) {
      $('#tb_rkap_plant_performance').DataTable().ajax.reload();
    });

    $('#btn-download').click(function(e) {
      e.preventDefault();
      $.ajax({
        xhrFields: {
          responseType: 'blob',
        },
        url: "{{ $urlExport }}",
        type: 'GET',
        data: {
          area: $('#filter_tahun').val(),
          plant_id: $("#filter_plant").val()
        },
        beforeSend: function(xhr) {
          xhr.setRequestHeader('Authorization', '{{ $token }}');
        },
        success: function(result, status, xhr) {
          var disposition = xhr.getResponseHeader('content-disposition');
          var filename = ('Plant Performance.xlsx');

          var blob = new Blob([result], {
            type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
          });
          var link = document.createElement('a');
          link.href = window.URL.createObjectURL(blob);
          link.download = filename;
          document.body.appendChild(link);
          link.click();
          document.body.removeChild(link);
        }
      })
    });
  });

  function filterPlantRM() {
    $.ajax({
      type: "GET",
      url: "{{ url('/inspection/plant') }}",
      dataType: 'JSON',
      contentType: false,
      processData: false,
      success: function(data) {
        $.each(data.data, function(i, item) {
          var newOption = new Option(item.nm_plant, item.id, false, false);
          $('#filter_plant').append(newOption);
        });
      }
    });
  }

  function show(id) {
    var url = '{{ route($route.".show", ":id") }}';
    url = url.replace(':id', id);
    $("#form-update").data('uuid', id)
    $('.btn-edit-row').attr('disabled', true).html("<i class='fa fa-spinner fa-spin'></i>");
    $.get(url, function(html) {
      console.log(html);
      $('#jan').val(html.jan);
      $('#feb').val(html.feb);
      $('#mar').val(html.mar);
      $('#apr').val(html.apr);
      $('#mei').val(html.mei);
      $('#jun').val(html.jun);
      $('#jul').val(html.jul);
      $('#ags').val(html.ags);
      $('#sep').val(html.sep);
      $('#okt').val(html.okt);
      $('#nov').val(html.nov);
      $('#des').val(html.des);
      $('#modal-update').modal('show');
      $('.btn-edit-row').attr('disabled', false).html("<i class='fa fa-pencil' aria-hidden='true'></i>");
    }).fail(function() {
      console.log('Gagal');
    });
  }
</script>
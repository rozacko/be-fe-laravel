<x-default-layout>
  <div class="flex-stack mb-0 mb-lg-n4 pt-5">
    <div class="row">
      <div class="col-sm-5">
        <div class="information">
          <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">Prognose Capex</h1>
          <ul class="breadcrumb breadcrumb-separatorless ">
            <li class="breadcrumb-item text-muted">
              <a href="/" class="text-muted text-hover-primary">Data & Prognose Capex</a>
            </li>
            <li class="breadcrumb-item text-muted">/</li>
            <li class="breadcrumb-item text-ghopo">Prognose Capex</li>
          </ul>
        </div>
      </div>
      <div class="col-sm-7 d-flex justify-content-md-end flex-no-wrap px-5">
        <div class="filter">
          <div class="row">
            <div class="col">
            <select class="form-select h-25" id="filter-project" data-control="select2" data-placeholder="No Project">
              <option></option>
              </select>
            </div>
            <!-- <div class="col">
              <select class="form-select h-25" data-control="select2" data-placeholder="Tahun">
                <option></option>
                @foreach(getYear() as $key => $value)
                @if ($value == date("Y") )
                <option value="{{$value}}" selected>{{$value}}</option>
                @else
                <option value="{{$value}}">{{$value}}</option>
                @endif
                @endforeach
              </select>
            </div> -->
            <div class="col">
              <button class="btn btn-light-primary" id="filter-btn">Tampilkan</button>
            </div>
            <div class="col dropdown-filter">
                <select id="filter_year" class="form-select h-25" data-control="select2" data-placeholder="Pilih Tahun">
                    @foreach(getYear() as $key => $value)
                        @if ($value == date("Y") )
                            <option value="{{$value}}" selected>{{$value}}</option>
                        @else
                            <option value="{{$value}}">{{$value}}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <div class="col">
              <button class="d-flex btn btn-outline-info" id="download-prognose-capex">
                <i class="bi bi-download flex"></i>
                Download
              </button>
            </div>
            <div class="col">
              <button class="d-flex btn btn-primary btn-import">
                <i class="bi bi-upload"></i>
                Import
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="modal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered mw-650px">
      <div class="modal-content rounded">
        <div class="mt-13 text-center">
          <h1 class="mb-3">Import Data</h1>
        </div>
        <form method="POST" action="#" enctype="multipart/form-data" id="idForm">
          <div class="modal-body">
            @csrf
            <div class="d-flex flex-column mb-8 fv-row form-group">
              <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                Template
                <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Download Template"></i>
              </label>
              <a href="{{ url('rkap-prognose/prognose-capex/download-template') }}" target="_blank" class="btn btn-secondary">
                <span class="indicator-label">Download Template</span>
              </a>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
              <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                <span class="required">Upload File</span>
              </label>
              <input type="file" name="upload_file" style="display: flex !important;" required/>
              <p class="mt-2" style="color: #1F7793;">*file yang di upload adalah file berdasarkan template yang disediakan</p>
            </div>
          </div>
          <div class="modal-footer">
            <div class="text-center">
              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
              <button type="submit" class="btn btn-primary" id="submitImport">
                <span class="indicator-label">Submit</span>
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>


  <div class="card card-flush mt-6 mt-xl-9">
    <div class="card-body mt-6 pt-0">
      <h3>Prognose Capex</h3>
      <table class="table table-bordered data-table" id="tb_prognose_capex">
        <thead>
          <tr>
            <th class="align-middle text-center color-header-tabel">NO PROJECT</th>
            <th class="align-middle text-center color-header-tabel">DESCRIPTION</th>
            <th class="align-middle text-center color-header-tabel">WBS</th>
            <th class="align-middle text-center color-header-tabel">TAHUN</th>
            <th class="align-middle text-center color-header-tabel">BULAN</th>
            <th class="align-middle text-center color-header-tabel">PROGNOSE</th>
            <th class="align-middle text-center color-header-tabel">ACTION</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>
  </div>


  <div class="modal fade" id="modal-update" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered mw-650px">
      <div class="modal-content rounded">
        <div class="mt-13 text-center">
          <h1 class="mb-3">Edit Data</h1>
        </div>
        <form method="POST" action="#" enctype="multipart/form-data" id="form-update">
          <div class="modal-body">
            @csrf
            <div class="row mt-3">
              <div class="col">
                <label for=""><b>No Project</b></label>
                <input type="text" id="no_project" name="no_project" class="form-control">
              </div>
            </div>
            <div class="row mt-3">
              <div class="col">
                <label for=""><b>Description</b></label>
                <input type="text" id="prognose_description" name="prognose_description" class="form-control">
              </div>
            </div>
            <div class="row mt-3">
              <div class="col">
                <label for=""><b>WBS</b></label>
                <input type="text" id="wbs" name="wbs" class="form-control">
              </div>
            </div>
            <div class="row mt-3">
              <div class="col">
                <label for=""><b>Tahun</b></label>
                <!-- <input type="text" id="tahun" name="tahun" class="form-control"> -->
                <select class="form-select h-25" id="tahun" name="tahun" data-control="select2" data-placeholder="Pilih Tahun">
                  @foreach(getYear() as $key => $value)
                  @if ($value == date("Y") )
                  <option value="{{$value}}" selected>{{$value}}</option>
                  @else
                  <option value="{{$value}}">{{$value}}</option>
                  @endif
                  @endforeach
                </select>
              </div>
            </div>
            <div class="row mt-3">
              <div class="col">
                <label for=""><b>Bulan</b></label>
                <!-- <input type="text" id="bulan" name="bulan" class="form-control"> -->
                <select class="form-select h-25" id="bulan" name="bulan" data-control="select2" data-placeholder="Pilih Bulan">
                @foreach(getMonth() as $key => $value)
                @if ($value['month'] == date("n") )
                <option value="{{$value['month']}}" selected>{{$value['month_name']}}</option>
                @else
                <option value="{{$value['month']}}">{{$value['month_name']}}</option>
                @endif
                @endforeach
              </select>
              </div>
            </div>
            <div class="row mt-3">
              <div class="col">
                <label for=""><b>Prognose</b></label>
                <input type="number" id="prognose" name="prognose" class="form-control">
              </div>
            </div>
            <div class="modal-footer">
              <div class="text-center">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary" id="submitUpdate">
                  <span class="indicator-label">Submit</span>
                </button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>


</x-default-layout>
<script>
  $(document).ready(function() {

    // data dumy
    var data = [{
      'plant': 'TUBAN 1',
      'parameter': 'OEE',
      'tahun': '2023',
      '1': 34,
      '2': 34,
      '3': 34,
      '4': 34,
      '5': 34,
      '6': 34,
      '7': 34,
      '8': 34,
      '9': 34,
      '10': 34,
      '11': 34,
      '12': 34,
    }, {
      'plant': 'TUBAN 1',
      'parameter': 'MTBF',
      'tahun': '2023',
      '1': 34,
      '2': 34,
      '3': 34,
      '4': 34,
      '5': 34,
      '6': 34,
      '7': 34,
      '8': 34,
      '9': 34,
      '10': 34,
      '11': 34,
      '12': 34,
    }, {
      'plant': 'TUBAN 1',
      'parameter': 'BTOSOD',
      'tahun': '2023',
      '1': 34,
      '2': 34,
      '3': 34,
      '4': 34,
      '5': 34,
      '6': 34,
      '7': 34,
      '8': 34,
      '9': 34,
      '10': 34,
      '11': 34,
      '12': 34,
    }, ];

    var table = $('#tb_prognose_capex').DataTable({
      responsive: true,
      scrollX: true,
      processing: true,
      serverSide: true,
      ajax: "{{ route($route.'.table') }}",
      columns: [{
          data: 'no_project',
          name: 'no_project',
          className: 'text-center',
        },
        {
          data: 'prognose_description',
          name: 'prognose_description',
          className: 'text-center',
        },
        {
          data: 'wbs',
          name: 'wbs',
          className: 'text-center',
        },
        {
          data: 'tahun',
          name: 'tahun',
          className: 'text-center',
        },
        {
          data: 'bulan',
          name: 'bulan',
          className: 'text-center',
        },
        {
          data: 'prognose',
          name: 'prognose',
          className: 'text-center',
        },
        {
          data: 'uuid',
          render: function(data, t, f) {
            // return "<button class='btn btn-primary btn-sm btn-edit-row' onclick=show('"+data+"') data-uuid='"+data+"'><i class='fa fa-pencil' aria-hidden='true'></i></button>";
            let button =
                            `<a href="#" onclick="show('${data}')" class="btn btn-primary btn-sm btn-edit-row"><i class="fa fa-edit"></i></a>&nbsp;
                             <a href="#" onclick="btn_delete('${data}')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>`;
                        return button;
          }
        },

      ],
      rowCallback: function(row, data, index) {}
    });

    $('.btn-import').click(function(e) {
      e.preventDefault();
      $('#modal').modal('show');
    });
    $('.edit-row-data').click(function(e) {
      e.preventDefault();
      $("#modal-update").modal('show');
    });

    $("#form-update").submit(function(e) {
      e.preventDefault(); // avoid to execute the actual submit of the form.
      var formData = new FormData($(this)[0]);
      var form = $(this);
      let uuid = $(this).data('uuid');
      var urlUpdate = '{{ route($route.".update", ":id") }}';
      urlUpdate = urlUpdate.replace(':id', uuid);  
      $.ajax({
        type: "PUT",
        url: urlUpdate,
        data: form.serialize(),
        beforeSend : function(){
          $('#submitUpdate').attr('disabled', true).html("<i class='fa fa-spinner fa-spin'></i> Processing");
        },
        success: function(data) {
          if (data.status == 'success') {
            Swal.fire(
              'Success',
              data.message,
              'success'
            );
            $('#submitUpdate').attr('disabled', false).html("<span class='indicator-label'>Submit</span>");
            $('#modal-update').modal('hide');
            $('#tb_prognose_capex').DataTable().ajax.reload();
          } else {
            Swal.fire(
              'Warning',
              data.message,
              'warning'
            );
            $('#submitUpdate').attr('disabled', false).html("<span class='indicator-label'>Submit</span>");
          }
        }
      });
    });

    $("#idForm").submit(function(e) {
      e.preventDefault(); // avoid to execute the actual submit of the form.
      var formData = new FormData($(this)[0]);
      $.ajax({
        type: "POST",
        url: "{{ route($route . '.import') }}",
        data: formData,
        contentType: false,
        processData: false,
        beforeSend : function(){
          $('#submitImport').attr('disabled', true).html("<i class='fa fa-spinner fa-spin'></i> Processing");
        },
        success: function(data) {
          if (data.status == 'success') {
            Swal.fire(
              'Success',
              data.message,
              'success'
            );
            $('#submitImport').attr('disabled', false).html("<span class='indicator-label'>Submit</span>");
            $('#modal').modal('hide');
            $('#tb_prognose_capex').DataTable().ajax.reload();
          } else {
            Swal.fire(
              'Warning',
              data.message,
              'warning'
            );
            $('#submitImport').attr('disabled', false).html("<span class='indicator-label'>Submit</span>");
          }
        }
      });
    });

    $.ajax({
            type: "GET",
            url: "{{ route($route.'.project') }}",
            dataType: 'JSON',
            contentType: false,
            processData: false,
            success: function(data){
              console.log(data)
                $.each(data.data, function (i, item) {
                    var newOption = new Option(item.no_project);
                    $('#filter-project').append(newOption);
                });
            }
        });

    });
    
    function show(id){
      var url = '{{ route($route.".show", ":id") }}';
      url = url.replace(':id', id);  
      $("#form-update").data('uuid',id)
      $('.btn-edit-row').attr('disabled', true).html("<i class='fa fa-spinner fa-spin'></i>");
      $.get(url, function (html) {
        console.log(html);
        $('#no_project').val(html.no_project);
        $('#prognose_description').val(html.prognose_description);
        $('#wbs').val(html.wbs);
        $('#tahun').val(html.tahun);
        $('#bulan').val(html.bulan);
        $('#prognose').val(html.prognose);
        $('#modal-update').modal('show');
        $('.btn-edit-row').attr('disabled', false).html("<i class='fa fa-pencil' aria-hidden='true'></i>");
      }).fail(function () {
          console.log('Gagal');
      });
    }

    $('#download-prognose-capex').click(function(e){
        e.preventDefault();
        $.ajax({
            xhrFields: {
                responseType: 'blob',
            },
            url: "{{ $urlExport }}",
            type: 'GET',
            data: {
                tahun : $("#filter_year").val()
            },
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', '{{ $token }}');
            },
            success: function(result, status, xhr) {
                var disposition = xhr.getResponseHeader('content-disposition');
                var filename = ('Data Prognose Capex.xlsx');

                var blob = new Blob([result], {
                    type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                });
                var link = document.createElement('a');
                link.href = window.URL.createObjectURL(blob);
                link.download = filename;

                document.body.appendChild(link);

                link.click();
                document.body.removeChild(link);
            }
        })
    })
    
    function btn_delete(id) {
        event.preventDefault();
        var url = '{{ route($route . '.destroy', ':id') }}';
        url = url.replace(':id', id);
        Swal.fire({
            icon: 'warning',
            title: 'Apakah anda yakin?',
            text: "Data ini akan di hapus!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak',
            confirmButtonClass: 'btn btn-primary margin-right-10',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then((result) => {
            if (result.isConfirmed) {
                $.post(url, {
                    _token: "{{ csrf_token() }}",
                    _method: 'DELETE'
                }, function(res) {
                    if (res.status == 'success') {
                        Swal.fire(
                            'Success',
                            res.message,
                            'success'
                        );
                        $('#tb_prognose_capex').DataTable().ajax.reload();
                    } else {
                        Swal.fire(
                            'Gagal',
                            res.message,
                            'error'
                        );
                    }
                }, 'json');
            }
        });

        return false;
    }
  
</script>
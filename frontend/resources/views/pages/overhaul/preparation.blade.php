<x-default-layout>
    <div class="flex-stack mb-0 mb-lg-n4 pt-5">
        <div class="row">
            <div class="col-sm-5">
                <div class="information">
                    <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">Overhaul Report</h1>
                    <ul class="breadcrumb breadcrumb-separatorless ">
                        <li class="breadcrumb-item text-muted">
                            <a href="/" class="text-muted text-hover-primary">Overhaul Report</a>
                        </li>
                        <li class="breadcrumb-item text-muted">/</li>
                        <li class="breadcrumb-item text-ghopo">Dashboard Overhaul Preparation</li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-7 d-flex justify-content-md-end flex-no-wrap px-5">
                <div class="filter">
                    <div class="row">
                        <div class="col mt-2">
                            <select class="form-select h-25" id="plant_overhaul" data-control="select2" data-placeholder="Pilih Plant">
                                <option></option>
                                <option value="1">Option 1</option>
                                <option value="2">Option 2</option>
                            </select>
                        </div>
                        <div class="col mt-2">
                            <select class="form-select h-25" data-control="select2" id="overhaul_year" data-placeholder="Pilih Tahun">
                                @foreach(getYear() as $key => $value)
                                @if ($value == date("Y") )
                                <option value="{{$value}}" selected>{{$value}}</option>
                                @else
                                <option value="{{$value}}">{{$value}}</option>
                                @endif
                                @endforeach
                            </select>
                        </div>
                        <!-- <div class="col mt-2 card-breadcrumb-button ">
                            <button class="btn btn-primary">Tampilkan</button>
                        </div> -->
                        <div class="col mt-2 card-breadcrumb-button">
                            <button href="#" class="btn btn-add" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Tambah</button>
                            <div class="menu menu-sub menu-sub-dropdown w-200px w-md-200px" data-kt-menu="true" id="kt_menu_63d780f5e6a12">
                                <div class="separator border-gray-200"></div>
                                <div class="">
                                    <div class="text-left">
                                    <!-- <button class="btn btn-sm btn-active-light-primary py-4 w-100" data-bs-toggle="modal" data-bs-target="#kt_modal_data_budget" data-kt-menu-dismiss="true">Import Data Budget Overhaul</button> -->
                                        <button class="btn btn-sm btn-active-light-primary py-4 w-100" data-bs-toggle="modal" data-bs-target="#kt_modal_data_preparation" data-kt-menu-dismiss="true">Import Data Preparation</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="g-5 g-xl-10 mb-3 mb-xl-3 mt-6">
        <div class="card-dashboard-overhoul">
            <div class="row">
                <div class="col-sm-12">
                    <div class="preparation">
                        <h1>Preparation</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="g-5 g-xl-10 mt-4 mb-3 mb-xl-3">
        <div class="card-dashboard-overhoul">
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-3 card-top-over-houl">
                            @include('partials/widgets/cards/_widget-overhoul-progress')
                        </div>
                        <div class="col-3 card-top-over-houl">
                            @include('partials/widgets/cards/_widget-overhoul-budget')
                        </div>
                        <div class="col-3 card-top-over-houl">
                            @include('partials/widgets/cards/_widget-overhoul-part-opex')
                            @include('partials/widgets/cards/_widget-overhoul-part-opex-two')
                        </div>
                        <div class="col-3 card-top-over-houl">
                            @include('partials/widgets/cards/_widget-overhoul-part-budget-opex')
                            @include('partials/widgets/cards/_widget-overhoul-part-capex')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="g-5 g-xl-10 mt-4 mb-3 mb-xl-3">
        <div class="card-dashboard-overhoul">
            <div class="row">
                <div class="col-12">
                    <div class="row">                        
                        <div class="col-3 card-top-over-houl">
                            @include('partials/widgets/cards/_widget-overhoul-part-rutin')
                            @include('partials/widgets/cards/_widget-overhoul-part-non-rutin')
                        </div>
                        <div class="col-3 card-top-over-houl">
                            @include('partials/widgets/cards/_widget-overhoul-part-rutin-data')
                        </div>
                        <div class="col-3 card-top-over-houl">
                            @include('partials/widgets/cards/_widget-overhoul-part-non-rutin-data')
                        </div>
                        <div class="col-3 card-top-over-houl">
                            @include('partials/widgets/cards/_widget-overhoul-hot-issue')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- modal import data budget -->
    <div class="modal fade" id="kt_modal_data_budget" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered mw-450px">
            <div class="modal-content">
                <div class="modal-header">
                    <h2>Import Data Budget Overhaul</h2>
                    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                        <span class="svg-icon svg-icon-1">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="currentColor" />
                                <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="currentColor" />
                            </svg>
                        </span>
                    </div>
                </div>
                <div class="modal-body py-lg-10 px-lg-10">
                    <form method="POST" action="{{ route($route . '.budget') }}" enctype="multipart/form-data" id="form_upload_budget">
                        <input type="hidden" id="plant_budget" name="id_plant">
                        <input type="file" id="file_budget" name="upload_file" style="display:none;">

                    <div class="d-flex flex-column mb-5 fv-row form-group">
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="">Tahun</span>
                        </label>
                        <select class="form-select" data-control="select2" name="tahun" data-placeholder="Pilih Tahun">
                            @foreach(getYear() as $key => $value)
                            @if ($value == date("Y") )
                            <option value="{{$value}}" selected>{{$value}}</option>
                            @else
                            <option value="{{$value}}">{{$value}}</option>
                            @endif
                            @endforeach
                        </select>
                    </div>

                    <div class="d-flex flex-column mb-5 fv-row form-group">
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="">Template</span>
                        </label>
                        <button type="button" class="btn w-100 btn-download-template btn-lg" onclick="window.open(`{{route('overhaul.preparation.budget')}}`,`_blank`)">
                            <img src="{{ image('icons/ghopo-icon/ghopo_download_icon.png') }}" alt="JS">
                            Download Template</button>
                    </div>
                    <div class="d-flex flex-column fv-row form-group">
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Upload File</span>
                        </label>
                        <button type="button" class="btn w-100 btn-download-template y btn-lg me-3" onclick="document.getElementById('file_budget').click()">
                            <img src="{{ image('icons/ghopo-icon/ghopo_choose_icon.png') }}" alt="JS">
                            Chose File</button>
                    </div>

                    <span class="mb-3 text-muted-ghopo" id="filename_budget"></span><br>
                    <span class="mb-3 text-primary-ghopo">*file yang di upload adalah file berdasarkan template yang disediakan</span>

                   

                        <div class="d-flex justify-content-end mt-4">
                            <button type="button" class="btn btn-border-primary btn-lg me-3" data-bs-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary">
                                <span class="indicator-label">Submit</span>
                                <span class="indicator-progress">Please wait...
                                    <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                            </button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- modal import data preparation -->
    <div class="modal fade" id="kt_modal_data_preparation" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered mw-450px">
            <div class="modal-content">
                <div class="modal-header">
                    <h2>Import Data Preparation</h2>
                    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                        <span class="svg-icon svg-icon-1">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="currentColor" />
                                <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="currentColor" />
                            </svg>
                        </span>
                    </div>
                </div>
                <div class="modal-body py-lg-10 px-lg-10">
                    <form method="POST" action="{{ route($route . '.import') }}" enctype="multipart/form-data" id="form_upload_preparation">
                        <input type="hidden" id="plant_import" name="id_plant">
                        <input type="file" id="file_preparation" name="upload_file" style="display:none;">

                    <div class="d-flex flex-column mb-5 fv-row form-group">
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="">Tahun</span>
                        </label>
                        <select class="form-select" data-control="select2" name="tahun" data-placeholder="Pilih Tahun">
                            @foreach(getYear() as $key => $value)
                            @if ($value == date("Y") )
                            <option value="{{$value}}" selected>{{$value}}</option>
                            @else
                            <option value="{{$value}}">{{$value}}</option>
                            @endif
                            @endforeach
                        </select>
                    </div>

                    <div class="d-flex flex-column mb-5 fv-row form-group">
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="">Template</span>
                        </label>
                        <button type="button" class="btn w-100 btn-download-template btn-lg" onclick="window.open(`{{route('overhaul.preparation.import')}}`,`_blank`)">
                            <img src="{{ image('icons/ghopo-icon/ghopo_download_icon.png') }}" alt="JS">
                            Download Template</button>
                    </div>
                    <div class="d-flex flex-column fv-row form-group">
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Upload File</span>
                        </label>
                        <button type="button" class="btn w-100 btn-download-template y btn-lg me-3" onclick="document.getElementById('file_preparation').click()">
                            <img src="{{ image('icons/ghopo-icon/ghopo_choose_icon.png') }}" alt="JS">
                            Chose File</button>
                    </div>

                    <span class="mb-3 text-muted-ghopo" id="filename_preparation"></span><br>
                    <span class="mb-3 text-primary-ghopo">*file yang di upload adalah file berdasarkan template yang disediakan</span>
               
                        <div class="d-flex justify-content-end mt-4">
                            <button type="button" class="btn btn-border-primary btn-lg me-3" data-bs-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary">
                                <span class="indicator-label">Submit</span>
                                <span class="indicator-progress">Please wait...
                                    <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                            </button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</x-default-layout>
<script>
    var id_plant;
    var dtRutin;
    var dtNonRutin;
    var dtPartCapex;
    var dtIssue;
    $(function() {
        $.get("{{route('masterdata.mplant.table')}}", function(data) {
            let html = ``;
            let selected = '';
            $.each(data.data, function(index, value) {
                html += '<option value="' + value.id + '">' + value.nm_plant + '</option>';
                if (index == 0) {
                    selected = value.id;
                }
            })
            $('#plant_overhaul').html(html);
            $('#plant_overhaul').val(selected).trigger('change');
        }, 'json');

        dtRutin = $('#datatable-rutin').DataTable({
            processing: true,
            serverSide: true,
            searching: false,
            paging: false,
            ajax: {
                url: "{{route($route. '.parttable')}}",
                data: function(d) {
                    d.id_plant = $('#plant_overhaul').val();
                    d.tahun = $('#overhaul_year').val();
                    d.category = 'Rutin';
                },
            },
            columns: [{
                    data: 'nm_unit_kerja',
                    name: 'nm_unit_kerja',                    
                },
                {
                    data: 'not_ready',
                    name: 'not_ready',
                    className: "text-center",
                    render: function(data, type) {
                        return localeNumber(data) + '%';
                    }
                },
                {
                    data: 'ready',
                    name: 'ready',
                    className: "text-center",
                    render: function(data, type) {
                        return localeNumber(data) + '%';
                    }
                },
                {
                    data: 'total',
                    name: 'total',
                    className: "text-center",
                    render: function(data, type) {
                        return localeNumber(data) + '%';
                    }
                },
            ],
            footerCallback: function(row, data, start, end, display) {
                var api = this.api();

                // Total over all pages
                total_not_ready = api
                    .column(1)
                    .data()
                    .reduce(function(a, b) {
                        return parseFloat(a) + parseFloat(b);
                    }, 0)/ api
                    .column(1)
                    .data()
                    .length;

                total_ready = api
                    .column(2)
                    .data()
                    .reduce(function(a, b) {
                        return parseFloat(a) + parseFloat(b);
                    }, 0) / api
                    .column(2)
                    .data()
                    .length;

                total = api
                    .column(3)
                    .data()
                    .reduce(function(a, b) {
                        return parseFloat(a) + parseFloat(b);
                    }, 0);

                $(api.column(1).footer()).html(localeNumber(total_not_ready)+'%');
                $(api.column(2).footer()).html(localeNumber(total_ready)+'%');
            },
        });

        dtNonRutin = $('#datatable-non-rutin').DataTable({
            processing: true,
            serverSide: true,
            searching: false,
            paging: false,
            ajax: {
                url: "{{route($route. '.parttable')}}",
                data: function(d) {
                    d.id_plant = $('#plant_overhaul').val();
                    d.tahun = $('#overhaul_year').val();
                    d.category = 'Non-Rutin';
                },
            },
            columns: [{
                    data: 'nm_unit_kerja',
                    name: 'nm_unit_kerja',                    
                },
                {
                    data: 'not_ready',
                    name: 'not_ready',
                    className: "text-center",
                    render: function(data, type) {
                        return localeNumber(data) + '%';
                    }
                },
                {
                    data: 'ready',
                    name: 'ready',
                    className: "text-center",
                    render: function(data, type) {
                        return localeNumber(data) + '%';
                    }
                },
                {
                    data: 'total',
                    name: 'total',
                    className: "text-center",
                    render: function(data, type) {
                        return localeNumber(data) + '%';
                    }
                },
            ],
            footerCallback: function(row, data, start, end, display) {
                var api = this.api();

                // Total over all pages
                total_not_ready = api
                    .column(1)
                    .data()
                    .reduce(function(a, b) {
                        return parseFloat(a) + parseFloat(b);
                    }, 0)/ api
                    .column(1)
                    .data()
                    .length;

                total_ready = api
                    .column(2)
                    .data()
                    .reduce(function(a, b) {
                        return parseFloat(a) + parseFloat(b);
                    }, 0) / api
                    .column(2)
                    .data()
                    .length;

                total = api
                    .column(3)
                    .data()
                    .reduce(function(a, b) {
                        return parseFloat(a) + parseFloat(b);
                    }, 0);

                $(api.column(1).footer()).html(localeNumber(total_not_ready)+'%');
                $(api.column(2).footer()).html(localeNumber(total_ready)+'%');
            },
        });

        dtPartCapex = $('#table_part_capex').DataTable({
            processing: true,
            serverSide: true,
            searching: false,
            paging: false,
            ajax: {
                url: "{{route($route. '.tablecapex')}}",
                data: function(d) {
                    d.id_plant = $('#plant_overhaul').val();
                    d.tahun = $('#overhaul_year').val();
                },
            },
            columns: [{
                    data: 'nm_unit_kerja',
                    name: 'nm_unit_kerja',  
                    className: "left-right",                  
                },
                {
                    data: 'total_harga',
                    name: 'total_harga',
                    className: "dt-right",
                    render: function(data, type) {
                        return 'Rp '+localeNumber(parseFloat(data));
                    }
                },
            ],
            footerCallback: function(row, data, start, end, display) {
                var api = this.api();

                // Total over all pages
                total = api
                    .column(1)
                    .data()
                    .reduce(function(a, b) {
                        return parseFloat(a) + parseFloat(b);
                    }, 0)/ api
                    .column(1)
                    .data()
                    .length;

                $(api.column(1).footer()).html('Rp '+localeNumber(total));
            },
        });

        dtIssue = $('#table_overhaul_issue').DataTable({
            processing: true,
            serverSide: true,
            searching: false,
            paging: false,
            ajax: {
                url: "{{route($route. '.tableissue')}}",
                data: function(d) {
                    d.id_plant = $('#plant_overhaul').val();
                    d.tahun = $('#overhaul_year').val();
                },
            },
            columns: [{
                    data: 'issues',
                    name: 'issues',  
                    className: "dt-left",                  
                },
                {
                    data: 'budget',
                    name: 'budget',
                    className: "dt-center",
                },
            ],
        });
    });

    $("#form_upload_budget").on('submit', (function(e) {
        $('#plant_budget').val($('#plant_overhaul').val());
        e.preventDefault();
        $.ajax({
            url: "{{ route($route . '.budget') }}",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {
                //$("#preview").fadeOut();
                $("#err").fadeOut();
            },
            success: function(data) {
                if (data.status == 'success') {
                    Swal.fire(
                        'Success',
                        data.message,
                        'success'
                    );
                    $('#kt_modal_data_budget').modal('hide');
                } else {
                    Swal.fire(
                        'Warning',
                        data.message,
                        'warning'
                    );
                }
            },
            error: function(e) {
                $("#err").html(e).fadeIn();
            }
        });
    }));

    $('#file_budget').on('change', function(e) {
        $('#filename_budget').html($('#file_budget').val().replace(/C:\\fakepath\\/i, ''));
    });

    $("#form_upload_preparation").on('submit', (function(e) {
        $('#plant_import').val($('#plant_overhaul').val());
        e.preventDefault();
        $.ajax({
            url: "{{ route($route . '.import') }}",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {
                //$("#preview").fadeOut();
                $("#err").fadeOut();
            },
            success: function(data) {
                if (data.status == 'success') {
                    Swal.fire(
                        'Success',
                        data.message,
                        'success'
                    );
                    $('#kt_modal_data_preparation').modal('hide');
                } else {
                    Swal.fire(
                        'Warning',
                        data.message,
                        'warning'
                    );
                }
            },
            error: function(e) {
                $("#err").html(e).fadeIn();
            }
        });
    }));

    $('#file_preparation').on('change', function(e) {
        $('#filename_preparation').html($('#file_preparation').val().replace(/C:\\fakepath\\/i, ''));
    });

    $('#plant_overhaul,#overhaul_year').on('change', function() {
        refreshPage();
    });

    function zoomImg(elm) {
        modal.style.display = "block";
        modalImg.src = elm.src;
        captionText.innerHTML = elm.alt;
    }

    function localeNumber(num) {
        return num.toLocaleString('id-ID', {
            maximumFractionDigits: 2
        });
    }

    function denomNumber(num){
        if(num >= (Math.pow(10, 9))){
            return localeNumber(num / Math.pow(10, 9)) + " Milyar";
        }
        else if(num >= (Math.pow(10, 6))){
            return localeNumber(num / Math.pow(10, 6)) + " Juta";
        }
        else if(num >= (Math.pow(10, 3))){
            return localeNumber(num / Math.pow(10, 3)) + " Ribu";
        }
        else {
            return localeNumber(num);
        }
    }

    function refreshPage() {
        getPartChart('Rutin');
        getPartChart('Non-Rutin');
        dtRutin.ajax.reload();
        dtNonRutin.ajax.reload();
        getBudgetBarChart();
        getPartChartBudget('Opex');
        getPartChartBudget('Capex');
        getDonutOpex();
        dtPartCapex.ajax.reload();
        dtIssue.ajax.reload();
    }

    function getPartChart(cat) {
        const tahun = $('#overhaul_year').val();
        const plant = $('#plant_overhaul').val();

        $.get("{{route($route.'.partchart')}}", {
            id_plant: plant,
            tahun: tahun,
            category: cat
        }, function(data) {
            var options = {
                series: data.series,
                colors: ['#5B9BD5', '#ED7D31', '#A5A5A5', '#FFC000', '#4472C4'],
                chart: {
                    height: '100%',
                    type: 'pie',
                },
                labels: data.label,
                responsive: [{
                    breakpoint: 480,
                    options: {
                        chart: {
                            height: '100%',
                        },
                        legend: {
                            position: 'bottom'
                        }
                    }
                }]
            };

            var chart = new ApexCharts(document.querySelector("#chart_part_" + cat), options);
            chart.render();

        }, 'json');
    }

    function getBudgetBarChart() {
        const tahun = $('#overhaul_year').val();
        const plant = $('#plant_overhaul').val();
        $.get("{{route($route.'.budgetuk')}}", {
            id_plant: plant,
            tahun: tahun,
        }, function(data) {
            var options = {
                series: data.series,
                colors: ['#5B9BD5', '#ED7D31'],
                chart: {
                    type: 'bar',
                    height: "100%"
                },
                plotOptions: {
                bar: {
                    horizontal: true,
                    dataLabels: {
                        position: 'top',
                    },
                }
                },
                dataLabels: {
                    enabled: true,
                    offsetX: -6,
                    style: {
                        fontSize: '12px',
                        colors: ['#fff']
                    },
                    formatter: function(value){
                        return denomNumber(value)
                    }
                },
                stroke: {
                    show: true,
                    width: 1,
                    colors: ['#fff']
                },
                tooltip: {
                    shared: true,
                    intersect: false,
                    y: {
                        formatter: function(value, { series, seriesIndex, dataPointIndex, w }) {
                            return denomNumber(value)
                        }
                    }
                },
                xaxis: {
                    categories: data.categories,
                    labels: {
                        formatter: function(value){
                            return denomNumber(value)
                        },
                    }
                },
            };

            var chart = new ApexCharts(document.querySelector("#kt_charts_widget_clustered_budget"), options);
            chart.render();

        }, 'json');
    }

    function getPartChartBudget(cat) {
        const tahun = $('#overhaul_year').val();
        const plant = $('#plant_overhaul').val();

        $.get("{{route($route.'.partchartbudget')}}", {
            id_plant: plant,
            tahun: tahun,
            budget: cat
        }, function(data) {
            var options = {
                series: data.series,
                colors: ['#5B9BD5', '#ED7D31'],
                chart: {
                    height: '100%',
                    type: 'donut',
                },
                labels: data.label,
                responsive: [{
                    breakpoint: 480,
                    options: {
                        chart: {
                            height: '100%',
                        },
                        legend: {
                            position: 'bottom'
                        }
                    }
                }]
            };

            var chart = new ApexCharts(document.querySelector("#chart_donut_"+cat), options);
            chart.render();

        }, 'json');
    }

    function getDonutOpex() {
        const tahun = $('#overhaul_year').val();
        const plant = $('#plant_overhaul').val();

        $.get("{{route($route.'.opexdonut')}}", {
            id_plant: plant,
            tahun: tahun,
        }, function(data) {
            var options = {
                series: data.series,
                colors: ['#5B9BD5', '#ED7D31'],
                chart: {
                    height: '100%',
                    type: 'donut',
                },
                labels: data.label,                
                tooltip: {
                    y: {
                        formatter: function(value) {
                            return 'IDR '+denomNumber(value)
                        }
                    }
                },
                responsive: [{
                    breakpoint: 480,
                    options: {
                        chart: {
                            height: '100%',
                        },
                        legend: {
                            position: 'bottom'
                        }
                    }
                }]
            };

            var chart = new ApexCharts(document.querySelector("#chart_all_opex_donut"), options);
            chart.render();

            $('#total_budget_opex').html(localeNumber(data.total));

        }, 'json');
    }
    
</script>
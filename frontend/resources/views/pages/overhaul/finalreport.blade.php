<x-default-layout>
    <div class="flex-stack mb-0 mb-lg-n4 pt-5">
        <div class="row">
            <div class="col-sm-5">
                <div class="information">
                    <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">Overhaul Report</h1>
                    <ul class="breadcrumb breadcrumb-separatorless ">
                        <li class="breadcrumb-item text-muted">
                            <a href="/" class="text-muted text-hover-primary">Overhaul Report</a>
                        </li>
                        <li class="breadcrumb-item text-muted">/</li>
                        <li class="breadcrumb-item text-ghopo">Dashboard Overhaul Final Report</li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-7 d-flex justify-content-md-end flex-no-wrap px-5">
                <div class="filter">
                    <div class="row">
                        <div class="col mt-2">
                            <select class="form-select h-25" id="plant_overhaul" data-control="select2" data-placeholder="Pilih Plant">
                                <option></option>
                                <option value="1">Option 1</option>
                                <option value="2">Option 2</option>
                            </select>
                        </div>
                        <div class="col mt-2">
                            <select class="form-select h-25" data-control="select2" id="overhaul_year" data-placeholder="Pilih Tahun">
                                @foreach(getYear() as $key => $value)
                                @if ($value == date("Y") )
                                <option value="{{$value}}" selected>{{$value}}</option>
                                @else
                                <option value="{{$value}}">{{$value}}</option>
                                @endif
                                @endforeach
                            </select>
                        </div>
                        <!-- <div class="col mt-2 card-breadcrumb-button ">
                            <button class="btn btn-primary">Tampilkan</button>
                        </div> -->
                        <div class="col mt-2 card-breadcrumb-button">
                            <button href="#" class="btn btn-add" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Tambah</button>
                            <div class="menu menu-sub menu-sub-dropdown w-200px w-md-200px" data-kt-menu="true" id="kt_menu_63d780f5e6a12">
                                <div class="separator border-gray-200"></div>
                                <div class="">
                                    <div class="text-left">
                                        <button class="btn btn-sm btn-active-light-primary py-4 w-100" data-bs-toggle="modal" data-bs-target="#kt_modal_create_finalreport" data-kt-menu-dismiss="true">Tambah Final Report</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="g-5 g-xl-10 mb-3 mb-xl-3">
        <div class="card-dashboard-overhoul">
            <div class="card-header">
                <div class="row">
                    <div class="col-sm-12 col-custom">
                        <div class="preparation">
                            <h1>Final Report</h1>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-body">
                <div class="dataTables_wrapper dt-bootstrap4 no-footer">
                    <div class="table-responsive">
                        <table id="datatable-final" class="table table-row-bordered table-row-dashed gy-4 align-middle fw-bold dataTable no-footer" style="width:100%">
                            <thead class="fs-7 text-gray-400 text-uppercase">
                                <tr>
                                    <th>Judul Final Report</th>
                                    <th>File</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>

    {{-- Modal View Img --}}
    <div id="myModalViewImg" class="modal-view-img">
        <span class="closeViewImg">&times;</span>
        <img class="modal-content-view-img" id="img01">
        <div id="caption-view-img" class="caption-view-img-over-houl"></div>
    </div>

    <!-- modal add finalreport -->
    <div class="modal fade" id="kt_modal_create_finalreport" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered mw-450px">
            <div class="modal-content">
                <div class="modal-header">
                    <h2>Tambah Final Report</h2>
                    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                        <span class="svg-icon svg-icon-1">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="currentColor" />
                                <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="currentColor" />
                            </svg>
                        </span>
                    </div>
                </div>
                <div class="modal-body py-lg-5 px-lg-5">
                    <form method="POST" class="mx-auto mw-500px w-100" enctype="multipart/form-data" id="form_add_finalreport">
                        <input type="hidden" id="plant_report" name="id_plant"/>
                        <div class="d-flex flex-column mb-5 fv-row form-group">
                            <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                                <span class="">Tahun</span>
                            </label>
                            <select class="form-select" data-control="select2" name="report_year" data-placeholder="Pilih Tahun">
                                @foreach(getYear() as $key => $value)
                                @if ($value == date("Y") )
                                <option value="{{$value}}" selected>{{$value}}</option>
                                @else
                                <option value="{{$value}}">{{$value}}</option>
                                @endif
                                @endforeach
                            </select>
                        </div>

                        <div class="fv-row mb-8">
                            <label class="required fs-6 fw-semibold mb-2">Judul Final Report</label>
                            <div class="position-relative d-flex align-items-center">
                                <input type="text" class="border form-control form-control-solid ps-12" name="title" placeholder="Judul Final Report" />
                            </div>
                        </div>

                        <div class="d-flex flex-column fv-row form-group">
                            <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                                <span class="required">File Final Report</span>
                            </label>
                            <button type="button" class="btn w-100 btn-download-template y btn-lg me-3" onclick="document.getElementById('file_final').click()">
                                <img src="{{ image('icons/ghopo-icon/ghopo_choose_icon.png') }}" alt="JS">
                                Chose File</button>
                        </div>
                        <span class="mb-3 text-muted-ghopo" id="filename_report"></span><br>
                        <span class="mb-3 text-primary-ghopo">*file yang di upload harus berextensi *jpg,*jpeg,*png</span>
                        <input type="file" id="file_final" name="upload_file" style="display:none;"/>
                        <div class="d-flex justify-content-end mt-4">
                            <button type="button" class="btn btn-border-primary btn-lg me-3" data-bs-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary">
                                <span class="indicator-label">Submit</span>
                                <span class="indicator-progress">Please wait...
                                    <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-default-layout>
<script>
  
    var tableFile;
    $('#plant_overhaul,#overhaul_year').on('change', function(e) {
        refreshPage();
    });

    $(function() {
        $.get("{{route('masterdata.mplant.table')}}", function(data) {
            let html = ``;
            let selected = '';
            $.each(data.data, function(index, value) {
                html += '<option value="' + value.id + '">' + value.nm_plant + '</option>';
                if (index == 0) {
                    selected = value.id;
                }
            })
            $('#plant_overhaul').html(html);
            $('#plant_overhaul').val(selected).trigger('change');
        }, 'json');

        $.get("{{route('masterdata.marea.table')}}", function(data) {
            let html = ``;
            let selected = '';
            $.each(data.data, function(index, value) {
                html += '<option value="' + value.id + '">' + value.nm_area + '</option>';
                if (index == 0) {
                    selected = value.id;
                }
            })
            $('#activity_area_form').html(html);
            $('#activity_area_form').val(selected).trigger('change');
        }, 'json');

        var span = document.getElementsByClassName("closeViewImg")[0];

        span.onclick = function() {
            modal.style.display = "none";
        }

        tableFile = $('#datatable-final').DataTable({
            processing: true,
            serverSide: true,
            searching: false,
            paging: false,
            ajax: {
                url: "{{route($route. '.table')}}",
                data: function(d) {
                    d.plant = $('#plant_overhaul').val();
                    d.tahun = $('#overhaul_year').val();
                },
            },
            columns: [{
                    data: 'title',
                    name: 'title',     
                    className: "dt-center",               
                },
                {
                    data: 'filepath',
                    name: 'filepath',
                    className: "dt-center",
                    render: function(data, type) {
                        return `<a href="#" onclick="window.open('`+data+`','_blank')" class="btn btn-info btn-sm"><i class="fa fa-file" style="padding-right: 0px; font-size:10;"></i></a>`;
                    }
                },
                {
                    data: 'action',
                    name: 'action',
                }
            ],
        });
    });

    function refreshPage() { 
        tableFile.ajax.reload();
    }

    $('#file_final').on('change', function(e) {
        $('#filename_report').html($('#file_final').val().replace(/C:\\fakepath\\/i, ''));
    });

    $("#form_add_finalreport").on('submit', (function(e) {
        e.preventDefault();
        $('#plant_report').val($('#plant_overhaul').val());
        $.ajax({
            url: "{{ route($route . '.add') }}",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {
                //$("#preview").fadeOut();
                $("#err").fadeOut();
            },
            success: function(data) {
                if (data.status == 'success') {
                    refreshPage();
                    Swal.fire(
                        'Success',
                        data.message,
                        'success'
                    );
                    tableFile.ajax.reload();
                    $('#kt_modal_create_finalreport').modal('hide');
                } else {
                    Swal.fire(
                        'Warning',
                        data.message,
                        'warning'
                    );
                }
            },
            error: function(e) {
                $("#err").html(e).fadeIn();
            }
        });
    }));


   
    function zoomImg(elm) {
        modal.style.display = "block";
        modalImg.src = elm.src;
        captionText.innerHTML = elm.alt;
    }

    function localeNumber(num) {
        if(isNaN(num))
            return num;

        return num.toLocaleString('id-ID', {
            maximumFractionDigits: 2
        });
    }

    function btn_delete(id){
        event.preventDefault(); 
        var url = '{{ route($route.".delete", ":id") }}';
        url = url.replace(':id', id);    
        Swal.fire({
            icon: 'warning',
            title: 'Apakah anda yakin?',
            text: "Data ini akan di hapus!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak',
            confirmButtonClass: 'btn btn-primary margin-right-10',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: true,
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function(isConfirm) {
            if (isConfirm.isConfirmed) {
                $.post(url, {_token: "{{ csrf_token() }}", _method: 'DELETE' }, function(res) {
                    if (res.status == 'success') {
                        Swal.fire(
                            'Success',
                            res.message,
                            'success'
                        );                    
                        tableFile.ajax.reload();
                    } else {
                        Swal.fire(
                            'Gagal',
                            res.message,
                            'error'
                        );
                    }
                }, 'json');
            } else {
                Swal.fire(
                    'Cancelled',
                    'Your data is safe',
                    'error'
                );
            } 
        });

        return false;
    }
</script>
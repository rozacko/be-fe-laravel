<x-default-layout>
    <div class="flex-stack mb-0 mb-lg-n4 pt-5">
        <div class="row">
            <div class="col-sm-5">
                <div class="information">
                    <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">Overhaul Report</h1>
                    <ul class="breadcrumb breadcrumb-separatorless ">
                        <li class="breadcrumb-item text-muted">
                            <a href="/" class="text-muted text-hover-primary">Overhaul Report</a>
                        </li>
                        <li class="breadcrumb-item text-muted">/</li>
                        <li class="breadcrumb-item text-ghopo">Dashboard Overhaul Progress</li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-7 d-flex justify-content-md-end flex-no-wrap px-5">
                <div class="filter">
                    <div class="row">
                        <div class="col mt-2">
                            <select class="form-select h-25" id="plant_overhaul" data-control="select2" data-placeholder="Pilih Plant">
                                <option></option>
                                <option value="1">Option 1</option>
                                <option value="2">Option 2</option>
                            </select>
                        </div>
                        <div class="col mt-2 card-breadcrumb-button">
                            <button href="#" class="btn btn-add" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Tambah</button>
                            <div class="menu menu-sub menu-sub-dropdown w-200px w-md-200px" data-kt-menu="true" id="kt_menu_63d780f5e6a12">
                                <div class="separator border-gray-200"></div>
                                <div class="">
                                    <div class="text-left">
                                        <button class="btn btn-sm btn-active-light-primary py-4 w-100" data-bs-toggle="modal" data-bs-target="#kt_modal_import_data_progress" data-kt-menu-dismiss="true">Import Data Progress</button>
                                        <button class="btn btn-sm btn-active-light-primary py-4 w-100" data-bs-toggle="modal" data-bs-target="#kt_modal_create_progress" data-kt-menu-dismiss="true">Add Data Progress</button>
                                        <button class="btn btn-sm btn-active-light-primary py-4 w-100" data-bs-toggle="modal" data-bs-target="#kt_modal_setting_periode" data-kt-menu-dismiss="true">Setting Periode</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="g-5 g-xl-10 mb-3 mb-xl-3">
        <div class="card-dashboard-overhoul">
            <div class="row">
                <div class="col-sm-12 col-custom">
                    <div class="preparation">
                        <h1>Progress</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="g-5 g-xl-10 mt-4 mb-3 mb-xl-3">
        <div class="card-dashboard-overhoul">
            <div class="row">
                <div class="col-sm-8 col-custom">
                    @include('partials/widgets/cards/_widget-overhoul-progress-table')
                </div>
                <div class="col-sm-2">
                    @include('partials/widgets/cards/_widget-overhoul-progress-day')

                </div>
                <div class="col-sm-2">
                    @include('partials/widgets/cards/_widget-overhoul-progress-total')
                </div>

            </div>
        </div>
    </div>

    <div class="g-5 g-xl-10 mt-4 mb-3 mb-xl-3">
        <div class="card-dashboard-overhoul">
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-12 col-lg">
                            @include('partials/widgets/cards/_widget-overhoul-major-activity')
                        </div>
                        <div class="col-12 col-lg">
                            @include('partials/widgets/cards/_widget-overhoul-biaya-opex')
                        </div>
                        <div class="col-12 col-lg">
                            @include('partials/widgets/cards/_widget-overhoul-kurva')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="g-5 g-xl-10 mt-4 mb-3 mb-xl-3">
        <div class="card-dashboard-overhoul">
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-12 col-lg">
                            @include('partials/widgets/cards/_widget-overhoul-minor-activity')
                        </div>
                        <div class="col-12 col-lg">
                            @include('partials/widgets/cards/_widget-overhoul-post-biaya-opex')
                        </div>
                        <div class="col-12 col-lg">
                            @include('partials/widgets/cards/_widget-overhoul-gallery-dokumentasi')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal View Img --}}
    <div id="myModalViewImg" class="modal-view-img">
        <span class="closeViewImg">&times;</span>
        <img class="modal-content-view-img" id="img01">
        <div id="caption-view-img" class="caption-view-img-over-houl"></div>
    </div>

    <!-- modal import data progress -->
    <div class="modal fade" id="kt_modal_import_data_progress" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered mw-450px">
            <div class="modal-content">
                <div class="modal-header">
                    <h2>Import Data Activity</h2>
                    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                        <span class="svg-icon svg-icon-1">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="currentColor" />
                                <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="currentColor" />
                            </svg>
                        </span>
                    </div>
                </div>
                <div class="modal-body py-lg-5 px-lg-5">
                    <div class="d-flex flex-column mb-5 fv-row form-group">
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="">Template</span>
                        </label>
                        <button type="button" class="btn w-100 btn-download-template btn-lg" onclick="downloadProgress()">
                            <img src="{{ image('icons/ghopo-icon/ghopo_download_icon.png') }}" alt="JS">
                            Download Template</button>
                    </div>
                    <div class="d-flex flex-column fv-row form-group">
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Upload File</span>
                        </label>
                        <button type="button" class="btn w-100 btn-download-template y btn-lg me-3" onclick="document.getElementById('file_progress').click()">
                            <img src="{{ image('icons/ghopo-icon/ghopo_choose_icon.png') }}" alt="JS">
                            Chose File</button>
                    </div>
                    <span class="mb-3 text-muted-ghopo" id="filename_progress"></span><br>
                    <span class="mb-3 text-primary-ghopo">*file yang di upload adalah file berdasarkan template yang disediakan</span>

                    <form method="POST" action="{{ route($route . '.import.progress') }}" enctype="multipart/form-data" id="form_upload_progress">
                        <input type="hidden" id="plant_progress" name="id_plant">
                        <input type="file" id="file_progress" name="upload_file" style="display:none;">

                        <div class="d-flex justify-content-end mt-4">
                            <button type="button" class="btn btn-border-primary btn-lg me-3" data-bs-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary">
                                <span class="indicator-label">Submit</span>
                                <span class="indicator-progress">Please wait...
                                    <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                            </button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>


    <!-- modal import data activity -->
    <div class="modal fade" id="kt_modal_import_data_activity" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered mw-450px">
            <div class="modal-content">
                <div class="modal-header">
                    <h2>Import Data Activity</h2>
                    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                        <span class="svg-icon svg-icon-1">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="currentColor" />
                                <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="currentColor" />
                            </svg>
                        </span>
                    </div>
                </div>
                <div class="modal-body py-lg-5 px-lg-5">
                    <div class="d-flex flex-column mb-5 fv-row form-group">
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="">Template</span>
                        </label>
                        <button type="button" class="btn w-100 btn-download-template btn-lg" onclick="window.open(`{{route('overhaul.progress.import.activity')}}`,`_blank`)">
                            <img src="{{ image('icons/ghopo-icon/ghopo_download_icon.png') }}" alt="JS">
                            Download Template</button>
                    </div>
                    <div class="d-flex flex-column fv-row form-group">
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Upload File</span>
                        </label>
                        <button type="button" class="btn w-100 btn-download-template y btn-lg me-3" onclick="document.getElementById('file_activity').click()">
                            <img src="{{ image('icons/ghopo-icon/ghopo_choose_icon.png') }}" alt="JS">
                            Chose File</button>
                    </div>
                    <span class="mb-3 text-muted-ghopo" id="filename_activity"></span><br>
                    <span class="mb-3 text-primary-ghopo">*file yang di upload adalah file berdasarkan template yang disediakan</span>

                    <form method="POST" action="{{ route($route . '.import.activity') }}" enctype="multipart/form-data" id="form_upload_activity">
                        <input type="hidden" id="plant_activity" name="id_plant">
                        <input type="file" id="file_activity" name="upload_file" style="display:none;">

                        <div class="d-flex justify-content-end mt-4">
                            <button type="button" class="btn btn-border-primary btn-lg me-3" data-bs-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary">
                                <span class="indicator-label">Submit</span>
                                <span class="indicator-progress">Please wait...
                                    <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                            </button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- modal Setting Periode -->
    <div class="modal fade" id="kt_modal_setting_periode" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered mw-450px">
            <div class="modal-content">
                <div class="modal-header">
                    <h2>Setting Periode</h2>
                    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                        <span class="svg-icon svg-icon-1">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="currentColor" />
                                <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="currentColor" />
                            </svg>
                        </span>
                    </div>
                </div>
                <div class="modal-body py-lg-5 px-lg-5">
                    <form method="POST" class="mx-auto mw-500px w-100" novalidate="novalidate" id="form_setting_periode">
                        <div class="fv-row mb-8">
                            <label class="required fs-6 fw-semibold mb-2">Tanggal Awal</label>
                            <div class="position-relative d-flex align-items-center">
                                <!--begin::Icon-->
                                <!--begin::Svg Icon | path: icons/duotune/general/gen014.svg-->
                                <span class="svg-icon svg-icon-2 position-absolute mx-4">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path opacity="0.3" d="M21 22H3C2.4 22 2 21.6 2 21V5C2 4.4 2.4 4 3 4H21C21.6 4 22 4.4 22 5V21C22 21.6 21.6 22 21 22Z" fill="currentColor" />
                                        <path d="M6 6C5.4 6 5 5.6 5 5V3C5 2.4 5.4 2 6 2C6.6 2 7 2.4 7 3V5C7 5.6 6.6 6 6 6ZM11 5V3C11 2.4 10.6 2 10 2C9.4 2 9 2.4 9 3V5C9 5.6 9.4 6 10 6C10.6 6 11 5.6 11 5ZM15 5V3C15 2.4 14.6 2 14 2C13.4 2 13 2.4 13 3V5C13 5.6 13.4 6 14 6C14.6 6 15 5.6 15 5ZM19 5V3C19 2.4 18.6 2 18 2C17.4 2 17 2.4 17 3V5C17 5.6 17.4 6 18 6C18.6 6 19 5.6 19 5Z" fill="currentColor" />
                                        <path d="M8.8 13.1C9.2 13.1 9.5 13 9.7 12.8C9.9 12.6 10.1 12.3 10.1 11.9C10.1 11.6 10 11.3 9.8 11.1C9.6 10.9 9.3 10.8 9 10.8C8.8 10.8 8.59999 10.8 8.39999 10.9C8.19999 11 8.1 11.1 8 11.2C7.9 11.3 7.8 11.4 7.7 11.6C7.6 11.8 7.5 11.9 7.5 12.1C7.5 12.2 7.4 12.2 7.3 12.3C7.2 12.4 7.09999 12.4 6.89999 12.4C6.69999 12.4 6.6 12.3 6.5 12.2C6.4 12.1 6.3 11.9 6.3 11.7C6.3 11.5 6.4 11.3 6.5 11.1C6.6 10.9 6.8 10.7 7 10.5C7.2 10.3 7.49999 10.1 7.89999 10C8.29999 9.90003 8.60001 9.80003 9.10001 9.80003C9.50001 9.80003 9.80001 9.90003 10.1 10C10.4 10.1 10.7 10.3 10.9 10.4C11.1 10.5 11.3 10.8 11.4 11.1C11.5 11.4 11.6 11.6 11.6 11.9C11.6 12.3 11.5 12.6 11.3 12.9C11.1 13.2 10.9 13.5 10.6 13.7C10.9 13.9 11.2 14.1 11.4 14.3C11.6 14.5 11.8 14.7 11.9 15C12 15.3 12.1 15.5 12.1 15.8C12.1 16.2 12 16.5 11.9 16.8C11.8 17.1 11.5 17.4 11.3 17.7C11.1 18 10.7 18.2 10.3 18.3C9.9 18.4 9.5 18.5 9 18.5C8.5 18.5 8.1 18.4 7.7 18.2C7.3 18 7 17.8 6.8 17.6C6.6 17.4 6.4 17.1 6.3 16.8C6.2 16.5 6.10001 16.3 6.10001 16.1C6.10001 15.9 6.2 15.7 6.3 15.6C6.4 15.5 6.6 15.4 6.8 15.4C6.9 15.4 7.00001 15.4 7.10001 15.5C7.20001 15.6 7.3 15.6 7.3 15.7C7.5 16.2 7.7 16.6 8 16.9C8.3 17.2 8.6 17.3 9 17.3C9.2 17.3 9.5 17.2 9.7 17.1C9.9 17 10.1 16.8 10.3 16.6C10.5 16.4 10.5 16.1 10.5 15.8C10.5 15.3 10.4 15 10.1 14.7C9.80001 14.4 9.50001 14.3 9.10001 14.3C9.00001 14.3 8.9 14.3 8.7 14.3C8.5 14.3 8.39999 14.3 8.39999 14.3C8.19999 14.3 7.99999 14.2 7.89999 14.1C7.79999 14 7.7 13.8 7.7 13.7C7.7 13.5 7.79999 13.4 7.89999 13.2C7.99999 13 8.2 13 8.5 13H8.8V13.1ZM15.3 17.5V12.2C14.3 13 13.6 13.3 13.3 13.3C13.1 13.3 13 13.2 12.9 13.1C12.8 13 12.7 12.8 12.7 12.6C12.7 12.4 12.8 12.3 12.9 12.2C13 12.1 13.2 12 13.6 11.8C14.1 11.6 14.5 11.3 14.7 11.1C14.9 10.9 15.2 10.6 15.5 10.3C15.8 10 15.9 9.80003 15.9 9.70003C15.9 9.60003 16.1 9.60004 16.3 9.60004C16.5 9.60004 16.7 9.70003 16.8 9.80003C16.9 9.90003 17 10.2 17 10.5V17.2C17 18 16.7 18.4 16.2 18.4C16 18.4 15.8 18.3 15.6 18.2C15.4 18.1 15.3 17.8 15.3 17.5Z" fill="currentColor" />
                                    </svg>
                                </span>
                                <!--end::Svg Icon-->
                                <!--end::Icon-->
                                <!--begin::Datepicker-->
                                <input type="text" class="border form-control form-control-solid ps-12 tgl-picker" id="periode_awal" name="start_date" value="{{date('d-m-Y')}}" />
                                <!--end::Datepicker-->
                            </div>
                        </div>
                        <div class="fv-row mb-8">
                            <label class="required fs-6 fw-semibold mb-2">Tanggal Akhir</label>
                            <div class="position-relative d-flex align-items-center">
                                <!--begin::Icon-->
                                <!--begin::Svg Icon | path: icons/duotune/general/gen014.svg-->
                                <span class="svg-icon svg-icon-2 position-absolute mx-4">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path opacity="0.3" d="M21 22H3C2.4 22 2 21.6 2 21V5C2 4.4 2.4 4 3 4H21C21.6 4 22 4.4 22 5V21C22 21.6 21.6 22 21 22Z" fill="currentColor" />
                                        <path d="M6 6C5.4 6 5 5.6 5 5V3C5 2.4 5.4 2 6 2C6.6 2 7 2.4 7 3V5C7 5.6 6.6 6 6 6ZM11 5V3C11 2.4 10.6 2 10 2C9.4 2 9 2.4 9 3V5C9 5.6 9.4 6 10 6C10.6 6 11 5.6 11 5ZM15 5V3C15 2.4 14.6 2 14 2C13.4 2 13 2.4 13 3V5C13 5.6 13.4 6 14 6C14.6 6 15 5.6 15 5ZM19 5V3C19 2.4 18.6 2 18 2C17.4 2 17 2.4 17 3V5C17 5.6 17.4 6 18 6C18.6 6 19 5.6 19 5Z" fill="currentColor" />
                                        <path d="M8.8 13.1C9.2 13.1 9.5 13 9.7 12.8C9.9 12.6 10.1 12.3 10.1 11.9C10.1 11.6 10 11.3 9.8 11.1C9.6 10.9 9.3 10.8 9 10.8C8.8 10.8 8.59999 10.8 8.39999 10.9C8.19999 11 8.1 11.1 8 11.2C7.9 11.3 7.8 11.4 7.7 11.6C7.6 11.8 7.5 11.9 7.5 12.1C7.5 12.2 7.4 12.2 7.3 12.3C7.2 12.4 7.09999 12.4 6.89999 12.4C6.69999 12.4 6.6 12.3 6.5 12.2C6.4 12.1 6.3 11.9 6.3 11.7C6.3 11.5 6.4 11.3 6.5 11.1C6.6 10.9 6.8 10.7 7 10.5C7.2 10.3 7.49999 10.1 7.89999 10C8.29999 9.90003 8.60001 9.80003 9.10001 9.80003C9.50001 9.80003 9.80001 9.90003 10.1 10C10.4 10.1 10.7 10.3 10.9 10.4C11.1 10.5 11.3 10.8 11.4 11.1C11.5 11.4 11.6 11.6 11.6 11.9C11.6 12.3 11.5 12.6 11.3 12.9C11.1 13.2 10.9 13.5 10.6 13.7C10.9 13.9 11.2 14.1 11.4 14.3C11.6 14.5 11.8 14.7 11.9 15C12 15.3 12.1 15.5 12.1 15.8C12.1 16.2 12 16.5 11.9 16.8C11.8 17.1 11.5 17.4 11.3 17.7C11.1 18 10.7 18.2 10.3 18.3C9.9 18.4 9.5 18.5 9 18.5C8.5 18.5 8.1 18.4 7.7 18.2C7.3 18 7 17.8 6.8 17.6C6.6 17.4 6.4 17.1 6.3 16.8C6.2 16.5 6.10001 16.3 6.10001 16.1C6.10001 15.9 6.2 15.7 6.3 15.6C6.4 15.5 6.6 15.4 6.8 15.4C6.9 15.4 7.00001 15.4 7.10001 15.5C7.20001 15.6 7.3 15.6 7.3 15.7C7.5 16.2 7.7 16.6 8 16.9C8.3 17.2 8.6 17.3 9 17.3C9.2 17.3 9.5 17.2 9.7 17.1C9.9 17 10.1 16.8 10.3 16.6C10.5 16.4 10.5 16.1 10.5 15.8C10.5 15.3 10.4 15 10.1 14.7C9.80001 14.4 9.50001 14.3 9.10001 14.3C9.00001 14.3 8.9 14.3 8.7 14.3C8.5 14.3 8.39999 14.3 8.39999 14.3C8.19999 14.3 7.99999 14.2 7.89999 14.1C7.79999 14 7.7 13.8 7.7 13.7C7.7 13.5 7.79999 13.4 7.89999 13.2C7.99999 13 8.2 13 8.5 13H8.8V13.1ZM15.3 17.5V12.2C14.3 13 13.6 13.3 13.3 13.3C13.1 13.3 13 13.2 12.9 13.1C12.8 13 12.7 12.8 12.7 12.6C12.7 12.4 12.8 12.3 12.9 12.2C13 12.1 13.2 12 13.6 11.8C14.1 11.6 14.5 11.3 14.7 11.1C14.9 10.9 15.2 10.6 15.5 10.3C15.8 10 15.9 9.80003 15.9 9.70003C15.9 9.60003 16.1 9.60004 16.3 9.60004C16.5 9.60004 16.7 9.70003 16.8 9.80003C16.9 9.90003 17 10.2 17 10.5V17.2C17 18 16.7 18.4 16.2 18.4C16 18.4 15.8 18.3 15.6 18.2C15.4 18.1 15.3 17.8 15.3 17.5Z" fill="currentColor" />
                                    </svg>
                                </span>
                                <!--end::Svg Icon-->
                                <!--end::Icon-->
                                <!--begin::Datepicker-->
                                <input type="text" class="border form-control form-control-solid ps-12 tgl-picker" id="periode_akhir" name="end_date" value="{{date('d-m-Y', strtotime('+24 days'))}}" />
                                <!--end::Datepicker-->
                            </div>
                        </div>
                        <input type="hidden" id="plant_setting" name="id_plant" />
                        <div class="d-flex justify-content-end mt-4">
                            <button type="button" class="btn btn-border-primary btn-lg me-3" data-bs-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary">
                                <span class="indicator-label">Submit</span>
                                <span class="indicator-progress">Please wait...
                                    <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- modal add activity -->
    <div class="modal fade" id="kt_modal_create_activity" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered mw-450px">
            <div class="modal-content">
                <div class="modal-header">
                    <h2>Tambah Activity</h2>
                    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                        <span class="svg-icon svg-icon-1">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="currentColor" />
                                <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="currentColor" />
                            </svg>
                        </span>
                    </div>
                </div>
                <div class="modal-body py-lg-5 px-lg-5">
                    <form method="POST" class="mx-auto mw-500px w-100" novalidate="novalidate" id="form_add_activity">
                        <div class="fv-row mb-8">
                            <label class="required fs-6 fw-semibold mb-2">Area</label>
                            <div class="position-relative d-flex align-items-center">
                                <select class="border form-control form-control-solid ps-12" id="activity_area_form" name="id_area">
                                </select>
                            </div>
                        </div>
                        <div class="fv-row mb-8">
                            <label class="required fs-6 fw-semibold mb-2">Major / Minor</label>
                            <div class="position-relative d-flex align-items-center">
                                <select class="border form-control form-control-solid ps-12" name="activity_category">
                                    <option value="Major">Major</option>
                                    <option value="Minor">Minor</option>
                                </select>
                            </div>
                        </div>
                        <div class="fv-row mb-8">
                            <label class="required fs-6 fw-semibold mb-2">Activity Name</label>
                            <div class="position-relative d-flex align-items-center">
                                <input type="text" class="border form-control form-control-solid ps-12" name="activity_name" placeholder="Nama Aktivitas" />
                            </div>
                        </div>
                        <div class="fv-row mb-8">
                            <label class="required fs-6 fw-semibold mb-2">Tanggal Awal</label>
                            <div class="position-relative d-flex align-items-center">
                                <!--begin::Icon-->
                                <!--begin::Svg Icon | path: icons/duotune/general/gen014.svg-->
                                <span class="svg-icon svg-icon-2 position-absolute mx-4">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path opacity="0.3" d="M21 22H3C2.4 22 2 21.6 2 21V5C2 4.4 2.4 4 3 4H21C21.6 4 22 4.4 22 5V21C22 21.6 21.6 22 21 22Z" fill="currentColor" />
                                        <path d="M6 6C5.4 6 5 5.6 5 5V3C5 2.4 5.4 2 6 2C6.6 2 7 2.4 7 3V5C7 5.6 6.6 6 6 6ZM11 5V3C11 2.4 10.6 2 10 2C9.4 2 9 2.4 9 3V5C9 5.6 9.4 6 10 6C10.6 6 11 5.6 11 5ZM15 5V3C15 2.4 14.6 2 14 2C13.4 2 13 2.4 13 3V5C13 5.6 13.4 6 14 6C14.6 6 15 5.6 15 5ZM19 5V3C19 2.4 18.6 2 18 2C17.4 2 17 2.4 17 3V5C17 5.6 17.4 6 18 6C18.6 6 19 5.6 19 5Z" fill="currentColor" />
                                        <path d="M8.8 13.1C9.2 13.1 9.5 13 9.7 12.8C9.9 12.6 10.1 12.3 10.1 11.9C10.1 11.6 10 11.3 9.8 11.1C9.6 10.9 9.3 10.8 9 10.8C8.8 10.8 8.59999 10.8 8.39999 10.9C8.19999 11 8.1 11.1 8 11.2C7.9 11.3 7.8 11.4 7.7 11.6C7.6 11.8 7.5 11.9 7.5 12.1C7.5 12.2 7.4 12.2 7.3 12.3C7.2 12.4 7.09999 12.4 6.89999 12.4C6.69999 12.4 6.6 12.3 6.5 12.2C6.4 12.1 6.3 11.9 6.3 11.7C6.3 11.5 6.4 11.3 6.5 11.1C6.6 10.9 6.8 10.7 7 10.5C7.2 10.3 7.49999 10.1 7.89999 10C8.29999 9.90003 8.60001 9.80003 9.10001 9.80003C9.50001 9.80003 9.80001 9.90003 10.1 10C10.4 10.1 10.7 10.3 10.9 10.4C11.1 10.5 11.3 10.8 11.4 11.1C11.5 11.4 11.6 11.6 11.6 11.9C11.6 12.3 11.5 12.6 11.3 12.9C11.1 13.2 10.9 13.5 10.6 13.7C10.9 13.9 11.2 14.1 11.4 14.3C11.6 14.5 11.8 14.7 11.9 15C12 15.3 12.1 15.5 12.1 15.8C12.1 16.2 12 16.5 11.9 16.8C11.8 17.1 11.5 17.4 11.3 17.7C11.1 18 10.7 18.2 10.3 18.3C9.9 18.4 9.5 18.5 9 18.5C8.5 18.5 8.1 18.4 7.7 18.2C7.3 18 7 17.8 6.8 17.6C6.6 17.4 6.4 17.1 6.3 16.8C6.2 16.5 6.10001 16.3 6.10001 16.1C6.10001 15.9 6.2 15.7 6.3 15.6C6.4 15.5 6.6 15.4 6.8 15.4C6.9 15.4 7.00001 15.4 7.10001 15.5C7.20001 15.6 7.3 15.6 7.3 15.7C7.5 16.2 7.7 16.6 8 16.9C8.3 17.2 8.6 17.3 9 17.3C9.2 17.3 9.5 17.2 9.7 17.1C9.9 17 10.1 16.8 10.3 16.6C10.5 16.4 10.5 16.1 10.5 15.8C10.5 15.3 10.4 15 10.1 14.7C9.80001 14.4 9.50001 14.3 9.10001 14.3C9.00001 14.3 8.9 14.3 8.7 14.3C8.5 14.3 8.39999 14.3 8.39999 14.3C8.19999 14.3 7.99999 14.2 7.89999 14.1C7.79999 14 7.7 13.8 7.7 13.7C7.7 13.5 7.79999 13.4 7.89999 13.2C7.99999 13 8.2 13 8.5 13H8.8V13.1ZM15.3 17.5V12.2C14.3 13 13.6 13.3 13.3 13.3C13.1 13.3 13 13.2 12.9 13.1C12.8 13 12.7 12.8 12.7 12.6C12.7 12.4 12.8 12.3 12.9 12.2C13 12.1 13.2 12 13.6 11.8C14.1 11.6 14.5 11.3 14.7 11.1C14.9 10.9 15.2 10.6 15.5 10.3C15.8 10 15.9 9.80003 15.9 9.70003C15.9 9.60003 16.1 9.60004 16.3 9.60004C16.5 9.60004 16.7 9.70003 16.8 9.80003C16.9 9.90003 17 10.2 17 10.5V17.2C17 18 16.7 18.4 16.2 18.4C16 18.4 15.8 18.3 15.6 18.2C15.4 18.1 15.3 17.8 15.3 17.5Z" fill="currentColor" />
                                    </svg>
                                </span>
                                <!--end::Svg Icon-->
                                <!--end::Icon-->
                                <!--begin::Datepicker-->
                                <input type="text" class="border form-control form-control-solid ps-12 tgl-picker" id="activity_awal" name="start_date" value="{{date('d-m-Y')}}" />
                                <!--end::Datepicker-->
                            </div>
                        </div>
                        <div class="fv-row mb-8">
                            <label class="required fs-6 fw-semibold mb-2">Tanggal Akhir</label>
                            <div class="position-relative d-flex align-items-center">
                                <!--begin::Icon-->
                                <!--begin::Svg Icon | path: icons/duotune/general/gen014.svg-->
                                <span class="svg-icon svg-icon-2 position-absolute mx-4">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path opacity="0.3" d="M21 22H3C2.4 22 2 21.6 2 21V5C2 4.4 2.4 4 3 4H21C21.6 4 22 4.4 22 5V21C22 21.6 21.6 22 21 22Z" fill="currentColor" />
                                        <path d="M6 6C5.4 6 5 5.6 5 5V3C5 2.4 5.4 2 6 2C6.6 2 7 2.4 7 3V5C7 5.6 6.6 6 6 6ZM11 5V3C11 2.4 10.6 2 10 2C9.4 2 9 2.4 9 3V5C9 5.6 9.4 6 10 6C10.6 6 11 5.6 11 5ZM15 5V3C15 2.4 14.6 2 14 2C13.4 2 13 2.4 13 3V5C13 5.6 13.4 6 14 6C14.6 6 15 5.6 15 5ZM19 5V3C19 2.4 18.6 2 18 2C17.4 2 17 2.4 17 3V5C17 5.6 17.4 6 18 6C18.6 6 19 5.6 19 5Z" fill="currentColor" />
                                        <path d="M8.8 13.1C9.2 13.1 9.5 13 9.7 12.8C9.9 12.6 10.1 12.3 10.1 11.9C10.1 11.6 10 11.3 9.8 11.1C9.6 10.9 9.3 10.8 9 10.8C8.8 10.8 8.59999 10.8 8.39999 10.9C8.19999 11 8.1 11.1 8 11.2C7.9 11.3 7.8 11.4 7.7 11.6C7.6 11.8 7.5 11.9 7.5 12.1C7.5 12.2 7.4 12.2 7.3 12.3C7.2 12.4 7.09999 12.4 6.89999 12.4C6.69999 12.4 6.6 12.3 6.5 12.2C6.4 12.1 6.3 11.9 6.3 11.7C6.3 11.5 6.4 11.3 6.5 11.1C6.6 10.9 6.8 10.7 7 10.5C7.2 10.3 7.49999 10.1 7.89999 10C8.29999 9.90003 8.60001 9.80003 9.10001 9.80003C9.50001 9.80003 9.80001 9.90003 10.1 10C10.4 10.1 10.7 10.3 10.9 10.4C11.1 10.5 11.3 10.8 11.4 11.1C11.5 11.4 11.6 11.6 11.6 11.9C11.6 12.3 11.5 12.6 11.3 12.9C11.1 13.2 10.9 13.5 10.6 13.7C10.9 13.9 11.2 14.1 11.4 14.3C11.6 14.5 11.8 14.7 11.9 15C12 15.3 12.1 15.5 12.1 15.8C12.1 16.2 12 16.5 11.9 16.8C11.8 17.1 11.5 17.4 11.3 17.7C11.1 18 10.7 18.2 10.3 18.3C9.9 18.4 9.5 18.5 9 18.5C8.5 18.5 8.1 18.4 7.7 18.2C7.3 18 7 17.8 6.8 17.6C6.6 17.4 6.4 17.1 6.3 16.8C6.2 16.5 6.10001 16.3 6.10001 16.1C6.10001 15.9 6.2 15.7 6.3 15.6C6.4 15.5 6.6 15.4 6.8 15.4C6.9 15.4 7.00001 15.4 7.10001 15.5C7.20001 15.6 7.3 15.6 7.3 15.7C7.5 16.2 7.7 16.6 8 16.9C8.3 17.2 8.6 17.3 9 17.3C9.2 17.3 9.5 17.2 9.7 17.1C9.9 17 10.1 16.8 10.3 16.6C10.5 16.4 10.5 16.1 10.5 15.8C10.5 15.3 10.4 15 10.1 14.7C9.80001 14.4 9.50001 14.3 9.10001 14.3C9.00001 14.3 8.9 14.3 8.7 14.3C8.5 14.3 8.39999 14.3 8.39999 14.3C8.19999 14.3 7.99999 14.2 7.89999 14.1C7.79999 14 7.7 13.8 7.7 13.7C7.7 13.5 7.79999 13.4 7.89999 13.2C7.99999 13 8.2 13 8.5 13H8.8V13.1ZM15.3 17.5V12.2C14.3 13 13.6 13.3 13.3 13.3C13.1 13.3 13 13.2 12.9 13.1C12.8 13 12.7 12.8 12.7 12.6C12.7 12.4 12.8 12.3 12.9 12.2C13 12.1 13.2 12 13.6 11.8C14.1 11.6 14.5 11.3 14.7 11.1C14.9 10.9 15.2 10.6 15.5 10.3C15.8 10 15.9 9.80003 15.9 9.70003C15.9 9.60003 16.1 9.60004 16.3 9.60004C16.5 9.60004 16.7 9.70003 16.8 9.80003C16.9 9.90003 17 10.2 17 10.5V17.2C17 18 16.7 18.4 16.2 18.4C16 18.4 15.8 18.3 15.6 18.2C15.4 18.1 15.3 17.8 15.3 17.5Z" fill="currentColor" />
                                    </svg>
                                </span>
                                <!--end::Svg Icon-->
                                <!--end::Icon-->
                                <!--begin::Datepicker-->
                                <input type="text" class="border form-control form-control-solid ps-12 tgl-picker" id="activity_akhir" name="end_date" value="{{date('d-m-Y', strtotime('+24 days'))}}" />
                                <!--end::Datepicker-->
                            </div>
                        </div>
                        <input type="hidden" id="plan_activity_form" name="id_plant" />
                        <div class="d-flex justify-content-end mt-4">
                            <button type="button" class="btn btn-border-primary btn-lg me-3" data-bs-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary">
                                <span class="indicator-label">Submit</span>
                                <span class="indicator-progress">Please wait...
                                    <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- modal add progress -->
    <div class="modal fade" id="kt_modal_create_progress" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered mw-450px">
            <div class="modal-content">
                <div class="modal-header">
                    <h2>Tambah Progress</h2>
                    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                        <span class="svg-icon svg-icon-1">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="currentColor" />
                                <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="currentColor" />
                            </svg>
                        </span>
                    </div>
                </div>
                <div class="modal-body py-lg-5 px-lg-5">
                    <form method="POST" class="mx-auto mw-500px w-100" enctype="multipart/form-data" id="form_add_progress">
                        <div class="fv-row mb-8">
                            <label class="required fs-6 fw-semibold mb-2">Activity</label>
                            <div class="position-relative d-flex align-items-center">
                                <select class="border form-control form-control-solid ps-12" id="activity_progress_form" name="id_activity">
                                </select>
                            </div>
                        </div>
                        <div class="fv-row mb-8">
                            <label class="required fs-6 fw-semibold mb-2">Tanggal Progress</label>
                            <div class="position-relative d-flex align-items-center">
                                <!--begin::Icon-->
                                <!--begin::Svg Icon | path: icons/duotune/general/gen014.svg-->
                                <span class="svg-icon svg-icon-2 position-absolute mx-4">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path opacity="0.3" d="M21 22H3C2.4 22 2 21.6 2 21V5C2 4.4 2.4 4 3 4H21C21.6 4 22 4.4 22 5V21C22 21.6 21.6 22 21 22Z" fill="currentColor" />
                                        <path d="M6 6C5.4 6 5 5.6 5 5V3C5 2.4 5.4 2 6 2C6.6 2 7 2.4 7 3V5C7 5.6 6.6 6 6 6ZM11 5V3C11 2.4 10.6 2 10 2C9.4 2 9 2.4 9 3V5C9 5.6 9.4 6 10 6C10.6 6 11 5.6 11 5ZM15 5V3C15 2.4 14.6 2 14 2C13.4 2 13 2.4 13 3V5C13 5.6 13.4 6 14 6C14.6 6 15 5.6 15 5ZM19 5V3C19 2.4 18.6 2 18 2C17.4 2 17 2.4 17 3V5C17 5.6 17.4 6 18 6C18.6 6 19 5.6 19 5Z" fill="currentColor" />
                                        <path d="M8.8 13.1C9.2 13.1 9.5 13 9.7 12.8C9.9 12.6 10.1 12.3 10.1 11.9C10.1 11.6 10 11.3 9.8 11.1C9.6 10.9 9.3 10.8 9 10.8C8.8 10.8 8.59999 10.8 8.39999 10.9C8.19999 11 8.1 11.1 8 11.2C7.9 11.3 7.8 11.4 7.7 11.6C7.6 11.8 7.5 11.9 7.5 12.1C7.5 12.2 7.4 12.2 7.3 12.3C7.2 12.4 7.09999 12.4 6.89999 12.4C6.69999 12.4 6.6 12.3 6.5 12.2C6.4 12.1 6.3 11.9 6.3 11.7C6.3 11.5 6.4 11.3 6.5 11.1C6.6 10.9 6.8 10.7 7 10.5C7.2 10.3 7.49999 10.1 7.89999 10C8.29999 9.90003 8.60001 9.80003 9.10001 9.80003C9.50001 9.80003 9.80001 9.90003 10.1 10C10.4 10.1 10.7 10.3 10.9 10.4C11.1 10.5 11.3 10.8 11.4 11.1C11.5 11.4 11.6 11.6 11.6 11.9C11.6 12.3 11.5 12.6 11.3 12.9C11.1 13.2 10.9 13.5 10.6 13.7C10.9 13.9 11.2 14.1 11.4 14.3C11.6 14.5 11.8 14.7 11.9 15C12 15.3 12.1 15.5 12.1 15.8C12.1 16.2 12 16.5 11.9 16.8C11.8 17.1 11.5 17.4 11.3 17.7C11.1 18 10.7 18.2 10.3 18.3C9.9 18.4 9.5 18.5 9 18.5C8.5 18.5 8.1 18.4 7.7 18.2C7.3 18 7 17.8 6.8 17.6C6.6 17.4 6.4 17.1 6.3 16.8C6.2 16.5 6.10001 16.3 6.10001 16.1C6.10001 15.9 6.2 15.7 6.3 15.6C6.4 15.5 6.6 15.4 6.8 15.4C6.9 15.4 7.00001 15.4 7.10001 15.5C7.20001 15.6 7.3 15.6 7.3 15.7C7.5 16.2 7.7 16.6 8 16.9C8.3 17.2 8.6 17.3 9 17.3C9.2 17.3 9.5 17.2 9.7 17.1C9.9 17 10.1 16.8 10.3 16.6C10.5 16.4 10.5 16.1 10.5 15.8C10.5 15.3 10.4 15 10.1 14.7C9.80001 14.4 9.50001 14.3 9.10001 14.3C9.00001 14.3 8.9 14.3 8.7 14.3C8.5 14.3 8.39999 14.3 8.39999 14.3C8.19999 14.3 7.99999 14.2 7.89999 14.1C7.79999 14 7.7 13.8 7.7 13.7C7.7 13.5 7.79999 13.4 7.89999 13.2C7.99999 13 8.2 13 8.5 13H8.8V13.1ZM15.3 17.5V12.2C14.3 13 13.6 13.3 13.3 13.3C13.1 13.3 13 13.2 12.9 13.1C12.8 13 12.7 12.8 12.7 12.6C12.7 12.4 12.8 12.3 12.9 12.2C13 12.1 13.2 12 13.6 11.8C14.1 11.6 14.5 11.3 14.7 11.1C14.9 10.9 15.2 10.6 15.5 10.3C15.8 10 15.9 9.80003 15.9 9.70003C15.9 9.60003 16.1 9.60004 16.3 9.60004C16.5 9.60004 16.7 9.70003 16.8 9.80003C16.9 9.90003 17 10.2 17 10.5V17.2C17 18 16.7 18.4 16.2 18.4C16 18.4 15.8 18.3 15.6 18.2C15.4 18.1 15.3 17.8 15.3 17.5Z" fill="currentColor" />
                                    </svg>
                                </span>
                                <!--end::Svg Icon-->
                                <!--end::Icon-->
                                <!--begin::Datepicker-->
                                <input type="text" class="border form-control form-control-solid ps-12 tgl-picker" id="tgl_progress" name="progress_date" value="{{date('d-m-Y')}}" />
                                <!--end::Datepicker-->
                            </div>
                        </div>
                        <div class="fv-row mb-8">
                            <label class="required fs-6 fw-semibold mb-2">Plan Cumulative</label>
                            <div class="position-relative d-flex align-items-center">
                                <input type="text" class="border form-control form-control-solid ps-12" name="plan_cumulative" placeholder="Plan Cumulative" />
                            </div>
                        </div>
                        <div class="fv-row mb-8">
                            <label class="required fs-6 fw-semibold mb-2">Real Cumulative</label>
                            <div class="position-relative d-flex align-items-center">
                                <input type="text" class="border form-control form-control-solid ps-12" name="real_cumulative" placeholder="Real Cumulative" />
                            </div>
                        </div>
                        <div class="d-flex flex-column fv-row form-group">
                            <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                                <span class="required">Dokumentasi</span>
                            </label>
                            <button type="button" class="btn w-100 btn-download-template y btn-lg me-3" onclick="document.getElementById('dokumentasi_progress').click()">
                                <img src="{{ image('icons/ghopo-icon/ghopo_choose_icon.png') }}" alt="JS">
                                Chose File</button>
                        </div>
                        <span class="mb-3 text-muted-ghopo" id="filename_progress"></span><br>
                        <span class="mb-3 text-primary-ghopo">*file yang di upload harus berextensi *jpg,*jpeg,*png</span>
                        <input type="file" id="dokumentasi_progress" name="upload_file" />
                        <div class="d-flex justify-content-end mt-4">
                            <button type="button" class="btn btn-border-primary btn-lg me-3" data-bs-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary">
                                <span class="indicator-label">Submit</span>
                                <span class="indicator-progress">Please wait...
                                    <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-default-layout>
<script>
    let _plannedmajor = 0;
    let _plannedminor = 0;
    let _realmajor = 0;
    let _realminor = 0;
    var modal = document.getElementById("myModalViewImg");
    var modalImg = document.getElementById("img01");
    var captionText = document.getElementById("caption-view-img");
    var progressChart;
    var chartOptions;
    var tableUnsafe;
    var tableOpex;

    $('#plant_overhaul').on('change', function(e) {
        refreshPage();
    });

    $(function() {
        $.get("{{route('masterdata.mplant.table')}}", function(data) {
            let html = ``;
            let selected = '';
            $.each(data.data, function(index, value) {
                html += '<option value="' + value.id + '">' + value.nm_plant + '</option>';
                if (index == 0) {
                    selected = value.id;
                }
            })
            $('#plant_overhaul').html(html);
            $('#plant_overhaul').val(selected).trigger('change');
        }, 'json');

        $.get("{{route('masterdata.marea.table')}}", function(data) {
            let html = ``;
            let selected = '';
            $.each(data.data, function(index, value) {
                html += '<option value="' + value.id + '">' + value.nm_area + '</option>';
                if (index == 0) {
                    selected = value.id;
                }
            })
            $('#activity_area_form').html(html);
            $('#activity_area_form').val(selected).trigger('change');
        }, 'json');

        var span = document.getElementsByClassName("closeViewImg")[0];

        span.onclick = function() {
            modal.style.display = "none";
        }

        tableUnsafe = $('#table_unsafe_condition').DataTable({
            processing: true,
            serverSide: true,
            searching: false,
            paging: false,
            ajax: {
                url: "{{route($route. '.unsafe')}}",
                data: function(d) {
                    d.plant = $('#plant_overhaul').val();
                    d.tahun = $('#overhaul_year').val();
                },
            },
            columns: [{
                    data: 'area',
                    name: 'area',                    
                },
                {
                    data: 'total',
                    name: 'total',
                    className: "dt-right",
                    render: function(data, type) {
                        return localeNumber(data);
                    }
                },
                {
                    data: 'open',
                    name: 'open',
                    className: "dt-right dt-danger",
                    render: function(data, type) {
                        return localeNumber(data);
                    }
                },
                {
                    data: 'close',
                    name: 'close',
                    className: "dt-right dt-success",
                    render: function(data, type) {
                        return localeNumber(data);
                    }
                },
                {
                    data: 'progress',
                    name: 'progress',
                    className: "dt-right",
                    render: function(data, type) {
                        return localeNumber(data) + '%';
                    }
                },
            ],
            footerCallback: function(row, data, start, end, display) {
                var api = this.api();

                // Total over all pages
                total_progress = api
                    .column(4)
                    .data()
                    .reduce(function(a, b) {
                        return parseFloat(a) + parseFloat(b);
                    }, 0)/ api
                    .column(1)
                    .data()
                    .length;

                $(api.column(4).footer()).html(localeNumber(total_progress)+'%');
            },
        });

        tableOpex = $('#table_post_opex').DataTable({
            processing: true,
            serverSide: true,
            searching: false,
            paging: false,
            ajax: {
                url: "{{route($route. '.opex')}}",
                data: function(d) {
                    d.plant = $('#plant_overhaul').val();
                },
            },
            columns: [{
                    data: 'nm_unit_kerja',
                    name: 'nm_unit_kerja',                    
                },
                {
                    data: 'rencana',
                    name: 'rencana',
                    className: "dt-right",
                    render: function(data, type) {
                        return 'Rp '+localeNumber(parseFloat(data));
                    }
                },
                {
                    data: 'realisasi',
                    name: 'realisasi',
                    className: "dt-right",
                    render: function(data, type) {
                        return 'Rp '+localeNumber(parseFloat(data));
                    }
                },
            ],
            footerCallback: function(row, data, start, end, display) {
                var api = this.api();

                // Total over all pages
                total_rencana = api
                    .column(1)
                    .data()
                    .reduce(function(a, b) {
                        return parseFloat(a) + parseFloat(b);
                    }, 0);

                total_realisasi = api
                    .column(2)
                    .data()
                    .reduce(function(a, b) {
                        return parseFloat(a) + parseFloat(b);
                    }, 0);

                realisasi = (parseFloat(total_realisasi) / parseFloat(total_rencana)) * 100
                   

                $("#sum_rencana_opex").html(localeNumber(total_rencana));
                $("#sum_realisasi_opex").html(localeNumber(total_realisasi));
                $('#persen_real_opex').html(localeNumber(realisasi));
            },
        });
    });

    function refreshPage() {
        const plant = $('#plant_overhaul').val();
        $.get("{{route('overhaul.periode')}}/" + plant, function(data) {
            $('#start_periode').html((new Date(data.start_date)).toLocaleDateString("id-ID", {
                year: 'numeric',
                month: 'long',
                day: 'numeric'
            }));
            $('#end_periode').html((new Date(data.end_date)).toLocaleDateString("id-ID", {
                year: 'numeric',
                month: 'long',
                day: 'numeric'
            }));
            $('#duration_periode').html(data.duration);
            $('#_duration_periode').html(data.duration);
            $('#_day_periode').html(data.current_day);

            getProgressTable(plant, 'major');
            getProgressTable(plant, 'minor');

            getGallery(plant);
            getActivity();

            renderProgressChart();

            tableUnsafe.ajax.reload();
            tableOpex.ajax.reload();
        }, 'json');
    }

    $('#file_activity').on('change', function(e) {
        $('#filename_activity').html($('#file_activity').val().replace(/C:\\fakepath\\/i, ''));
    });

    $('#file_progress').on('change', function(e) {
        $('#filename_progress').html($('#file_progress').val().replace(/C:\\fakepath\\/i, ''));
    });

    $('#dokumentasi_progress').on('change', function(e) {
        console.log($('#dokumentasi_progress').val());
    });

    $("#form_upload_activity").on('submit', (function(e) {
        $('#plant_activity').val($('#plant_overhaul').val());
        e.preventDefault();
        $.ajax({
            url: "{{ route($route . '.import.activity') }}",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {
                //$("#preview").fadeOut();
                $("#err").fadeOut();
            },
            success: function(data) {
                if (data.status == 'success') {
                    Swal.fire(
                        'Success',
                        data.message,
                        'success'
                    );
                    $('#kt_modal_import_data_activity').modal('hide');
                } else {
                    Swal.fire(
                        'Warning',
                        data.message,
                        'warning'
                    );
                }
            },
            error: function(e) {
                $("#err").html(e).fadeIn();
            }
        });
    }));

    $("#form_add_activity").on('submit', (function(e) {
        $('#plan_activity_form').val($('#plant_overhaul').val());
        e.preventDefault();
        $.ajax({
            url: "{{ route($route . '.activity') }}",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {
                //$("#preview").fadeOut();
                $("#err").fadeOut();
            },
            success: function(data) {
                if (data.status == 'success') {
                    refreshPage();
                    Swal.fire(
                        'Success',
                        data.message,
                        'success'
                    );
                    $('#kt_modal_create_activity').modal('hide');
                } else {
                    Swal.fire(
                        'Warning',
                        data.message,
                        'warning'
                    );
                }
            },
            error: function(e) {
                $("#err").html(e).fadeIn();
            }
        });
    }));

    $("#form_add_progress").on('submit', (function(e) {
        e.preventDefault();
        $.ajax({
            url: "{{ route($route . '.progress') }}",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {
                //$("#preview").fadeOut();
                $("#err").fadeOut();
            },
            success: function(data) {
                if (data.status == 'success') {
                    refreshPage();
                    Swal.fire(
                        'Success',
                        data.message,
                        'success'
                    );
                    $('#kt_modal_create_activity').modal('hide');
                } else {
                    Swal.fire(
                        'Warning',
                        data.message,
                        'warning'
                    );
                }
            },
            error: function(e) {
                $("#err").html(e).fadeIn();
            }
        });
    }));

    $("#form_upload_progress").on('submit', (function(e) {
        $('#plant_progress').val($('#plant_overhaul').val());
        e.preventDefault();
        $.ajax({
            url: "{{ route($route . '.import.progress') }}",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {
                //$("#preview").fadeOut();
                $("#err").fadeOut();
            },
            success: function(data) {
                if (data.status == 'success') {
                    Swal.fire(
                        'Success',
                        data.message,
                        'success'
                    );
                    $('#kt_modal_import_data_progress').modal('hide');
                } else {
                    Swal.fire(
                        'Warning',
                        data.message,
                        'warning'
                    );
                }
            },
            error: function(e) {
                $("#err").html(e).fadeIn();
            }
        });
    }));

    $('#periode_awal').flatpickr({
        dateFormat: "d-m-Y",
        onChange: function(selectedDates, dateStr, instance) {
            $('#periode_akhir').flatpickr({
                dateFormat: "d-m-Y",
                minDate: dateStr,
            });
        },
    });

    $('#activity_awal').flatpickr({
        dateFormat: "d-m-Y",
        onChange: function(selectedDates, dateStr, instance) {
            $('#activity_akhir').flatpickr({
                dateFormat: "d-m-Y",
                minDate: dateStr,
            });
        },
    });

    $('#tgl_progress').flatpickr({
        dateFormat: "d-m-Y"
    })

    $("#form_setting_periode").on('submit', function(e) {
        e.preventDefault();
        $('#plant_setting').val($('#plant_overhaul').val());
        $.ajax({
            url: "{{ route('overhaul.periode') }}",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {
                //$("#preview").fadeOut();
                $("#err").fadeOut();
            },
            success: function(data) {
                if (data.status == 'success') {
                    refreshPage();
                    Swal.fire(
                        'Success',
                        data.message,
                        'success'
                    );
                    $('#kt_modal_setting_periode').modal('hide');
                } else {
                    Swal.fire(
                        'Warning',
                        data.message,
                        'warning'
                    );
                }
            },
            error: function(e) {
                $("#err").html(e).fadeIn();
            }
        });
    });

    function downloadProgress() {
        window.open(`{{route('overhaul.progress.import.progress')}}/` + $('#plant_overhaul').val(), `_blank`)
    }

    function getActivity() {
        $.get("{{route($route .'.activity')}}/" + $('#plant_overhaul').val(), function(data) {
            let html = ``;
            let selected = '';
            $.each(data, function(index, value) {
                html += '<option value="' + value.id + '">' + value.activity_name + '</option>';
                if (index == 0) {
                    selected = value.id;
                }
            })
            $('#activity_progress_form').html(html);
            $('#activity_progress_form').val(selected).trigger('change');
        }, 'json');
    }

    function getProgressTable(plant, category) {
        $.get("{{route('overhaul.progress')}}/" + category , {plant: plant}, function(data) {
            let html = '';
            $.each(data, function(index, value) {
                if (value.status == 'header') {
                    html += '<tr class="bg-yellow">';
                    eval('_planned' + category + '=' + value.plan + ';');
                    eval('_real' + category + '=' + value.real + ';');
                } else if (value.status == 'area') {
                    html += '<tr class="bg-gray">';
                } else {
                    html += '<tr>';
                }

                html += '<td>' + value.ket + '</td>';
                html += '<td>' + localeNumber(value.plan) + '%</td>';
                html += '<td>' + localeNumber(value.real) + '%</td>';

                if (value.variance < 0)
                    html += '<td class="d-flex align-items-center"><div class="circle-danger"></div> <div class="percent ml-2">' + localeNumber(value.variance) + '%</div></td>';
                else
                    html += '<td class="d-flex align-items-center"><div class="circle-success"></div> <div class="percent ml-2">' + localeNumber(value.variance) + '%</div></td>';

                html += '</tr>';
            });

            $('#tbody_' + category).html(html);

            const _avgplan = (_plannedmajor + _plannedminor) / 2;
            const _avgreal = (_realmajor + _realminor) / 2;
            const _finvariance = _avgreal - _avgplan;

            $('#sum_planned').html(localeNumber(_avgplan) + '%');
            $('#sum_real').html(localeNumber(_avgreal) + '%');
            $('#sum_variance').html(localeNumber(_finvariance) + '%');
            $('#_variancetotal').html(localeNumber(_avgreal) + '%');
        }, 'json');
    }

    function getGallery(plant) {
        $.get("{{route('overhaul.progress.dokumentasi')}}/" + plant, function(data) {
            let html = ``;
            $.each(data, function(index, value) {
                html += `<div class="col-12 col-md-auto col-lg col-custom-6">'
                <img id="` + value.img_id + `" src="` + value.img_url + `" alt="` + value.activity_name + `" class="myImgViewOverHoul" onclick="zoomImg(this)">
                </div>`;
            });
            $('#gallery_overhaul').html(html);
        }, 'json');
    }

    function zoomImg(elm) {
        modal.style.display = "block";
        modalImg.src = elm.src;
        captionText.innerHTML = elm.alt;
    }

    function localeNumber(num) {
        if(isNaN(num))
            return num;

        return num.toLocaleString('id-ID', {
            maximumFractionDigits: 2
        });
    }

    function renderProgressChart() {
        $.get("progress/chart/" + $('#plant_overhaul').val(), function(data) {
            chartOptions = {
                series: data.series,
                chart: {
                    height: 350,
                    type: 'line',
                    zoom: {
                        enabled: false
                    }
                },
                dataLabels: {
                    enabled: false
                },
                stroke: {
                    curve: 'straight'
                },
                title: {
                    text: 'Kurva S Activity',
                    align: 'left'
                },
                grid: {
                    row: {
                        colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
                        opacity: 0.5
                    },
                },
                xaxis: {
                    type: "date",
                },
            };

            progressChart = new ApexCharts(document.querySelector("#kurva_s_activity"), chartOptions);
            progressChart.render();

        }, 'json');
    }
</script>
<x-default-layout>
  <div class="g-5 g-xl-10 mb-3 mb-xl-3 pt-5">
    <div class="row">
      <div class="col-sm-3">
        <div class="information">
          <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">
            {{ $title }}
          </h1>
          <ul class="breadcrumb breadcrumb-separatorless ">
                <li class="breadcrumb-item text-muted">
                  <a href="/" class="text-muted text-hover-primary">Data Koreksi</a>
                </li>
                <li class="breadcrumb-item text-muted">/</li>
                <li class="breadcrumb-item text-ghopo">{{ $title }}</li>
          </ul>
         </div>
      </div>
      <div class="col-sm-9 d-flex justify-content-md-end flex-no-wrap px-5">
        <div class="filter">
          <form method="POST" action="{{ route($route . '.download') }}" enctype="multipart/form-data" id="download">
            @csrf
            <div class="row">
              <div class="col mt-2 dropdown-filter">
                <select class="form-select h-25" data-control="select2" data-placeholder="Pilih Tahun" id="filter-tahun">
                    @foreach(getYear() as $key => $value)
                        @if ($value == date("Y") )
                            <option value="{{$value}}" selected>{{$value}}</option>
                        @else
                            <option value="{{$value}}">{{$value}}</option>
                        @endif
                    @endforeach
                </select>
              </div>
              <div class="col mt-2 dropdown-filter">
                <select class="form-select h-25" data-control="select2" data-placeholder="Pilih Bulan" id="filter-bulan">
                  @foreach(getMonth() as $key => $value)
                  @if ($value['month'] == date("n") )
                      <option value="{{$value['month']}}" selected>{{$value['month_name']}}</option>
                  @else
                      <option value="{{$value['month']}}">{{$value['month_name']}}</option>
                  @endif
                  @endforeach
                </select>
              </div>
              <div class="col mt-2 card-breadcrumb-button ">
                  <button class="btn btn-history" id="apply">Tampilkan</button>
              </div>
              <div class="col mt-2 card-breadcrumb-button">
                  <button type="submit" class="btn btn-add">Download</button>
              </div>
              <div class="col mt-2 card-breadcrumb-button">
                  <button class="btn btn-add" id="import">Import</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="card card-flush mt-6 mt-xl-9">
    <div class="card-body mt-6 pt-0" id="loading-kpi">
      <h3>{{ $title }}</h3>
      <table class="table table-bordered data-table" id="dt-kpi">
          <thead>
              <tr>
                  <th class="fw-bold text-center">KEY PERFORMANCE INDICATOR (KPI)</th>
                  <th class="fw-bold text-center">TAHUN</th>
                  <th class="fw-bold text-center">BULAN</th>
                  <th class="fw-bold text-center">KPI GROUP</th>
                  <th class="fw-bold text-center">UNIT</th>
                  <th class="fw-bold text-center">POLARISASI</th>
                  <th class="fw-bold text-center">TARGET</th>
                  <th class="fw-bold text-center">PROGNOSE</th>
                  <th class="fw-bold text-center">REAL</th>
                  <th class="fw-bold text-center">CAPAIAN KPI</th>
              </tr>
          </thead>
          <tbody>
          </tbody>
      </table>
   </div>
  </div>

  {{-- Modal Import --}}
  <div class="modal fade" id="modal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered mw-500px">
      <div class="modal-content rounded"></div>
    </div>
  </div>

  {{-- Modal Update --}}
  <div class="modal fade" id="modal-update" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered mw-650px">
        <div class="modal-content rounded"></div>
    </div>
  </div>
</x-default-layout>

<script>
  let dataKPI;

  $(document).ready(function () {
    KPI()
  })

  // function datatable
  function KPI() {
    $.ajax({
      type: "GET",
      url: "{{ url('/'.$route . '/datatable') }}",
      data: {
        'tahun': $('#filter-tahun').val(),
        'bulan': $('#filter-bulan').val()
      },
      dataType: 'JSON',
      beforeSend: function() {
        $('#loading-kpi').LoadingOverlay('show');
      },
      success: function(data){
        var tempData = [];
        $.each(data.data, function(index, value) {
          $.each(value['detail'], function(i, v) {
            tempData.push(v)
          });
        });
        dataKPI = tempData
        tableKPI(dataKPI, true)
      }
    }).always(function(){
      $('#loading-kpi').LoadingOverlay('hide');
    });
  }
  function tableKPI(data, redraw= false) {
    var table_raw_mill = $('#dt-kpi').DataTable({
      processing: true,
      ordering: true,
      responsive: false,
      scrollX: true,
      filter:false,
      data: data,
      columns: [{
              data: 'kpi',
              name: 'kpi',
              class: 'text-center',
          },{
              data: 'tahun',
              name: 'tahun',
              class: 'text-center',
          },
          {
              data: 'bulan',
              name: 'bulan',
              class: 'text-center',
          },
          {
              data: 'kpi_group',
              name: 'kpi_group',
              class: 'text-center',
          },
          {
              data: 'unit',
              name: 'unit',
              class: 'text-center',
          },
          {
              data: 'polarisasi',
              name: 'polarisasi',
              class: 'text-center',
          },
          {
              data: 'target',
              name: 'target',
              class: 'text-center',
              render: function(data, type, full, meta) {
                data = parseFloat(data).toFixed(2)
                return data;
              }
          },
          {
              data: 'prognose',
              name: 'prognose',
              class: 'text-center',
              render: function(data, type, full, meta) {
                data = parseFloat(data).toFixed(2)
                return data;
              }
          },
          {
              data: 'realisasi',
              name: 'realisasi',
              class: 'text-center',
              render: function(data, type, full, meta) {
                data = parseFloat(data).toFixed(2)
                return data;
              }
          },
          {
              data: 'persentase',
              name: 'persentase',
              class: 'text-center',
          }
      ],
    });
    if (redraw) {
        table_raw_mill.clear();
        table_raw_mill.rows.add(data);
        table_raw_mill.draw();
    };
  }

  // event button apply
  $('#apply').click(function (e) {
    e.preventDefault();
    KPI();
  })

  // event button import (open modal)
  $('#import').click(function (e) {
    e.preventDefault();
    var url = "{{ url('/'.$route . '/create') }}";
    $.get(url, function (html) {
        $('#modal .modal-content').html(html);
        $('#modal').modal('show');
    }).fail(function () {
      Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Something went wrong!',
      })
    });
  })

  $("#download").submit(function(e) {
    e.preventDefault(); // avoid to execute the actual submit of the form.
    var form = $(this);
    var formData = new FormData();
    var actionUrl = form.attr('action');
    formData.append('tahun', $('#filter-tahun').val());
    formData.append('bulan', $('#filter-bulan').val());
    $.ajax({
      type: "POST",
      url: "{{ route($route . '.download') }}",
      data: formData,
      dataType: 'JSON',
      contentType: false,
      processData: false,
      success: function(data)
      {
        window.location.href = data.data.file_path;
      }
    });
  });
</script>
<x-default-layout>
  <div class="g-5 g-xl-10 mb-3 mb-xl-3 pt-5">
    <div class="row">
      <div class="col-sm-3">
        <div class="information">
          <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">
            Data OPC & Shipment
          </h1>
          <ul class="breadcrumb breadcrumb-separatorless ">
                <li class="breadcrumb-item text-muted">
                  <a href="/" class="text-muted text-hover-primary">Data OPC & Shipment</a>
                </li>
                <li class="breadcrumb-item text-muted">/</li>
                <li class="breadcrumb-item text-ghopo">Dashboard</li>
          </ul>
         </div>
      </div>
      <div class="col-sm-9 d-flex justify-content-md-end flex-no-wrap">
          <div class="filter">
              <div class="row">
                  <div class="col">
                    <input class="form-control form-control-solid" placeholder="Pick date rage"
                    id="date-range"/>
                  </div>
                  <div class="col card-breadcrumb-button ">
                      <button class="btn btn-history">Tampilkan</button>
                  </div>
                  <div class="col card-breadcrumb-button ">
                    <button class="btn btn-add">Download</button>
                </div>
              </div>
          </div>
      </div>
    </div>
  </div>
  <div class="card card-flush mt-6 mt-xl-9">
    <div class="card-body">
      <ul class="nav nav-tabs nav-line-tabs nav-line-tabs-2x pt-5" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="#opc" data-bs-toggle="tab">Data OPC</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" aria-current="page" href="#shipment" data-bs-toggle="tab">Data Shipment</a>
        </li>
      </ul>
      <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="opc" role="tabpanel">
          <div class="pt-3">
            <div class="card">
              <div class="card-body">
                <div class="card-header ps-1 border-bottom-0">
                  <div class="card-title flex-column">
                      <h5 class="fw-bold">DATA OPC</h5>
                  </div>
                </div>
                <table class="table table-striped table-row-bordered" id="dt_opc">
                  <thead>
                      <tr>
                          <th class="fw-bold">Keterangan</th>
                          <th class="fw-bold">Tuban 1</th>
                          <th class="fw-bold">Tuban 2</th>
                          <th class="fw-bold">Tuban 3</th>
                          <th class="fw-bold">Tuban 4</th>
                          <th class="fw-bold">Tuban 0</th>
                      </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

          <div class="pt-3">
            <div class="card">
              <div class="card-body">
                <div class="card-header ps-1 border-bottom-0">
                  <div class="card-title flex-column">
                      <h5 class="fw-bold">INDEKS BAHAN TERAK & KWH</h5>
                  </div>
                </div>
                <table class="table table-striped table-row-bordered" id="dt_terak">
                  <thead>
                      <tr>
                          <th class="fw-bold">Unit</th>
                          <th class="fw-bold">Tuban 1</th>
                          <th class="fw-bold">Tuban 2</th>
                          <th class="fw-bold">Tuban 3</th>
                          <th class="fw-bold">Tuban 4</th>
                          <th class="fw-bold">Tuban 0</th>
                      </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

          <div class="pt-3">
            <div class="card">
              <div class="card-body">
                <div class="card-header ps-1 border-bottom-0">
                  <div class="card-title flex-column">
                      <h5 class="fw-bold">INDEKS BAHAN SEMEN & KWH</h5>
                  </div>
                </div>
                <table class="table table-striped table-row-bordered" id="dt_semen">
                  <thead>
                      <tr>
                          <th class="fw-bold">group</th>
                          <th class="fw-bold">Unit</th>
                          <th class="fw-bold">Tuban 1</th>
                          <th class="fw-bold">Tuban 2</th>
                          <th class="fw-bold">Tuban 3</th>
                          <th class="fw-bold">Tuban 4</th>
                          <th class="fw-bold">Tuban 0</th>
                      </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <div class="tab-pane fade" id="shipment" role="tabpanel">
          <div class="pt-3">
            <div class="card">
              <div class="card-body">
                <div class="card-header ps-1 border-bottom-0">
                  <div class="card-title flex-column">
                      <h5 class="fw-bold">DATA SHIPMENT</h5>
                  </div>
                </div>
                <table class="table table-striped table-row-bordered" id="dt_shipment">
                  <thead>
                      <tr>
                          <th class="fw-bold">RELEASE SEMEN</th>
                          <th class="fw-bold">TUBAN 1</th>
                          <th class="fw-bold">TUBAN 2</th>
                          <th class="fw-bold">TUBAN 3</th>
                          <th class="fw-bold">TUBAN 4</th>
                          <th class="fw-bold">TUBAN 0</th>
                      </tr>
                  </thead>
                  <tbody>
                  </tbody>
                  <tfoot>
                    <tr>
                        <th class="fw-bold">Total Release</th>
                        <th class="fw-bold"></th>
                        <th class="fw-bold"></th>
                        <th class="fw-bold"></th>
                        <th class="fw-bold"></th>
                        <th class="fw-bold"></th>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
          </div>

          <div class="pt-3">
            <div class="card">
              <div class="card-body">
                <table class="table table-striped table-row-bordered" id="dt_jop">
                  <thead>
                      <tr>
                          <th class="fw-bold">JOP RELEASE SEMEN</th>
                          <th class="fw-bold">TUBAN 1</th>
                          <th class="fw-bold">TUBAN 2</th>
                          <th class="fw-bold">TUBAN 3</th>
                          <th class="fw-bold">TUBAN 4</th>
                          <th class="fw-bold">TUBAN 0</th>
                      </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

          <div class="pt-3">
            <div class="card">
              <div class="card-body">
                <table class="table table-striped table-row-bordered" id="dt_rincian">
                  <thead>
                      <tr>
                          <th class="fw-bold">RINCIAN TYPE SEMEN</th>
                          <th class="fw-bold">TUBAN 1</th>
                          <th class="fw-bold">TUBAN 2</th>
                          <th class="fw-bold">TUBAN 3</th>
                          <th class="fw-bold">TUBAN 4</th>
                          <th class="fw-bold">TUBAN 0</th>
                      </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

          <div class="pt-3">
            <div class="card">
              <div class="card-body">
                <table class="table table-striped table-row-bordered" id="dt_whrpg">
                  <thead>
                      <tr>
                          <th class="fw-bold">WHRPG</th>
                          <th class="fw-bold">TUBAN 1</th>
                          <th class="fw-bold">TUBAN 2</th>
                          <th class="fw-bold">TUBAN 3</th>
                          <th class="fw-bold">TUBAN 4</th>
                          <th class="fw-bold">TUBAN 0</th>
                      </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

          <div class="pt-3">
            <div class="card">
              <div class="card-body">
                <table class="table table-striped table-row-bordered" id="dt_penimbangan">
                  <thead>
                      <tr>
                        <th class="fw-bold align-middle" rowspan="2">PENIMBANGAN BAHAN</th>
                        <th class="fw-bold align-middle" rowspan="2">TERIMA</th>
                        <th class="fw-bold text-center" colspan="6">STOK SEMEN</th>
                      </tr>
                      <tr>
                        <th class="fw-bold"></th>
                        <th class="fw-bold">OPC</th>
                        <th class="fw-bold">PCC</th>
                        <th class="fw-bold">OPC PREMIUM</th>
                        <th class="fw-bold">PCC PREMIUM</th>
                        <th class="fw-bold">TOTAL</th>
                      </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</x-default-layout>

<script>
  $("#date-range").daterangepicker();

  // config datatable and nav tabs
  $(document).ready(function() {
    $('a[data-bs-toggle="tab"]').on( 'shown.bs.tab', function (e) {
        $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
    } );
  });

  $(function() {
    // dummy data
    let dataOpc = [
        {
            keterangan:"Prod. BK Crusher #1 (ton)",
            tuban1:5000,
            tuban2:4500,
            tuban3:4500,
            tuban4:4500,
            tuban0:78945611
        },
        {
            keterangan:"Jam Op. BK Crusher #1 (jam)",
            tuban1:24,
            tuban2:23,
            tuban3:24,
            tuban4:23,
            tuban0:78945612
        },
        {
            keterangan:"Prod. BK Crusher #1 (ton)",
            tuban1:5000,
            tuban2:4500,
            tuban3:4500,
            tuban4:3000,
            tuban0:78945611
        },
        {
            keterangan:"Jam Op. BK Crusher #1 (jam)",
            tuban1:24,
            tuban2:23,
            tuban3:24,
            tuban4:23,
            tuban0:78945612
        },
        {
            keterangan:"Prod. BK Crusher #1 (ton)",
            tuban1:5000,
            tuban2:4500,
            tuban3:4500,
            tuban4:3000,
            tuban0:78945611
        },
        {
            keterangan:"Jam Op. BK Crusher #1 (jam)",
            tuban1:24,
            tuban2:23,
            tuban3:24,
            tuban4:23,
            tuban0:78945612
        },
        {
            keterangan:"Prod. BK Crusher #1 (ton)",
            tuban1:5000,
            tuban2:4500,
            tuban3:4500,
            tuban4:3000,
            tuban0:78945611
        },
        {
            keterangan:"Jam Op. BK Crusher #1 (jam)",
            tuban1:24,
            tuban2:23,
            tuban3:24,
            tuban4:23,
            tuban0:78945612
        }
    ];
    var table = $('#dt_opc').DataTable({
        processing: true,
        ordering: false,
        responsive: false,
        scrollX: true,
        filter:false,
        paging: false,
        bInfo : false,
        data: dataOpc,
        columns: [{
                data: 'keterangan',
                name: 'keterangan',
                width:'200px'
            },
            {
                data: 'tuban1',
                name: 'tuban1'
            },
            {
                data: 'tuban2',
                name: 'tuban2'
            },
            {
                data: 'tuban3',
                name: 'tuban3'
            },
            {
                data: 'tuban4',
                name: 'tuban4'
            },
            {
                data: 'tuban0',
                name: 'tuban0'
            }
        ]
    });
  });

  // dummy data
  let dataTerak = [
    {
        unit:"Prod. BK Crusher",
        tuban1:5000,
        tuban2:4500,
        tuban3:4500,
        tuban4:3000,
        tuban0:78945611
    },
    {
        unit:"Prod. Clay Crusher",
        tuban1:24,
        tuban2:23,
        tuban3:24,
        tuban4:23,
        tuban0:78945612
    },
    {
        unit:"KWH Crusher Batukapur",
        tuban1:5000,
        tuban2:4500,
        tuban3:4500,
        tuban4:3000,
        tuban0:78945611
    },
    {
        unit:"KWH Clay Crusher",
        tuban1:24,
        tuban2:23,
        tuban3:24,
        tuban4:23,
        tuban0:78945612
    },
    {
        unit:"Umpan Kiln",
        tuban1:5000,
        tuban2:4500,
        tuban3:4500,
        tuban4:3000,
        tuban0:78945611
    },
    {
        unit:"Pemaikan IDO/Solar Kiln",
        tuban1:24,
        tuban2:23,
        tuban3:24,
        tuban4:23,
        tuban0:78945612
    }
  ];
  var table = $('#dt_terak').DataTable({
    processing: true,
    ordering: false,
    responsive: false,
    scrollX: true,
    filter:false,
    paging: false,
    bInfo : false,
    data: dataTerak,
    columns: [{
            data: 'unit',
            name: 'unit',
            width:'200px'
        },
        {
            data: 'tuban1',
            name: 'tuban1'
        },
        {
            data: 'tuban2',
            name: 'tuban2'
        },
        {
            data: 'tuban3',
            name: 'tuban3'
        },
        {
            data: 'tuban4',
            name: 'tuban4'
        },
        {
            data: 'tuban0',
            name: 'tuban0'
        }
    ]
  });

  // dummy data
  let dataSemen = [
    {
        group:"PROD SEMEN FM#1",
        unit:"Pemakaian Terak FM #1",
        tuban1:5000,
        tuban2:4500,
        tuban3:4500,
        tuban4:3000,
        tuban0:78945611
    },
    {
        group:"PROD SEMEN FM#1",
        unit:"Pemakaian Gypsum FM #1",
        tuban1:24,
        tuban2:23,
        tuban3:24,
        tuban4:23,
        tuban0:78945612
    },
    {
        group:"PROD SEMEN FM#1",
        unit:"Pemakaian Trass FM #1",
        tuban1:5000,
        tuban2:4500,
        tuban3:4500,
        tuban4:3000,
        tuban0:78945611
    },
    {
        group:"PROD SEMEN FM#2",
        unit:"Pemakaian Terak FM #2",
        tuban1:5000,
        tuban2:4500,
        tuban3:4500,
        tuban4:3000,
        tuban0:78945611
    },
    {
        group:"PROD SEMEN FM#2",
        unit:"Pemakaian Gypsum FM #2",
        tuban1:24,
        tuban2:23,
        tuban3:24,
        tuban4:23,
        tuban0:78945612
    },
    {
        group:"PROD SEMEN FM#2",
        unit:"Pemakaian Trass FM #2",
        tuban1:5000,
        tuban2:4500,
        tuban3:4500,
        tuban4:3000,
        tuban0:78945611
    }
  ];
  var table = $('#dt_semen').DataTable({
    processing: true,
    ordering: false,
    responsive: false,
    scrollX: true,
    filter:false,
    paging: false,
    bInfo : false,
    data: dataSemen,
    columns: [{
            data: 'group',
            name: 'group',
            visible: false
        },
        {
            data: 'unit',
            name: 'unit',
            width:'200px'
        },
        {
            data: 'tuban1',
            name: 'tuban1'
        },
        {
            data: 'tuban2',
            name: 'tuban2'
        },
        {
            data: 'tuban3',
            name: 'tuban3'
        },
        {
            data: 'tuban4',
            name: 'tuban4'
        },
        {
            data: 'tuban0',
            name: 'tuban0'
        }
    ],
    drawCallback: function (settings) {
                var api = this.api();
                var rows = api.rows({ page: 'current' }).nodes();
                var last = null;
                api
                    .column(0, { page: 'current' })
                    .data()
                    .each(function (group, i) {
                        if (last !== group) {
                            $(rows)
                              .eq(i)
                              .before('<tr style="background-color: #D0D0D0;"><td colspan="6"><b>'
                              + group + '</td></tr>');
                            last = group;
                        }
                    });
            }
  });

  // dummy data
  let dataShipment = [
    {
        release_semen:"Packer 1",
        tuban1:5000,
        tuban2:4500,
        tuban3:4500,
        tuban4:4500,
        tuban0:78945611
    },
    {
        release_semen:"Packer 2",
        tuban1:24,
        tuban2:23,
        tuban3:24,
        tuban4:23,
        tuban0:78945612
    },
    {
        release_semen:"Packer 3",
        tuban1:5000,
        tuban2:4500,
        tuban3:4500,
        tuban4:4500,
        tuban0:78945611
    },
    {
        release_semen:"Packer 4",
        tuban1:24,
        tuban2:23,
        tuban3:24,
        tuban4:23,
        tuban0:78945612
    },
    {
        release_semen:"Packer 5",
        tuban1:5000,
        tuban2:4500,
        tuban3:4500,
        tuban4:4500,
        tuban0:78945611
    },
    {
        release_semen:"Packer 6",
        tuban1:24,
        tuban2:23,
        tuban3:24,
        tuban4:23,
        tuban0:78945612
    },
    {
        release_semen:"Packer 7",
        tuban1:5000,
        tuban2:4500,
        tuban3:4500,
        tuban4:4500,
        tuban0:78945611
    }
  ];
  var table = $('#dt_shipment').DataTable({
    processing: true,
    ordering: false,
    responsive: false,
    scrollX: true,
    filter:false,
    paging: false,
    bInfo : false,
    data: dataShipment,
    columns: [{
            data: 'release_semen',
            name: 'release_semen',
            width:'200px'
        },
        {
            data: 'tuban1',
            name: 'tuban1'
        },
        {
            data: 'tuban2',
            name: 'tuban2'
        },
        {
            data: 'tuban3',
            name: 'tuban3'
        },
        {
            data: 'tuban4',
            name: 'tuban4'
        },
        {
            data: 'tuban0',
            name: 'tuban0'
        }
    ],
    "footerCallback": function ( row, data, start, end, display ) {
      var api = this.api();
      nb_cols = api.columns().nodes().length;
      var j = 1;
      var sum_column = 0
      var list_total = []
      //get list sum column and sum all data
      while(j < nb_cols){
        var pageTotal = api
              .column( j, { page: 'current'} )
              .data()
              .reduce( function (a, b) {
                  return Number(a) + Number(b);
              }, 0 );
        $( api.column( j ).footer() ).html(pageTotal);
        j++;
      }
    }
  });

  // dummy data
  let dataJop = [
    {
        jop_release:"Packer 1",
        tuban1:5000,
        tuban2:4500,
        tuban3:4500,
        tuban4:4500,
        tuban0:78945611
    },
    {
        jop_release:"Packer 2",
        tuban1:24,
        tuban2:23,
        tuban3:24,
        tuban4:23,
        tuban0:78945612
    },
    {
        jop_release:"Packer 3",
        tuban1:5000,
        tuban2:4500,
        tuban3:4500,
        tuban4:4500,
        tuban0:78945611
    },
    {
        jop_release:"Packer 4",
        tuban1:24,
        tuban2:23,
        tuban3:24,
        tuban4:23,
        tuban0:78945612
    },
    {
        jop_release:"Packer 5",
        tuban1:5000,
        tuban2:4500,
        tuban3:4500,
        tuban4:4500,
        tuban0:78945611
    },
    {
        jop_release:"Packer 6",
        tuban1:24,
        tuban2:23,
        tuban3:24,
        tuban4:23,
        tuban0:78945612
    },
    {
        jop_release:"Packer 7",
        tuban1:5000,
        tuban2:4500,
        tuban3:4500,
        tuban4:4500,
        tuban0:78945611
    }
  ];
  var table = $('#dt_jop').DataTable({
    processing: true,
    ordering: false,
    responsive: false,
    scrollX: true,
    filter:false,
    paging: false,
    bInfo : false,
    data: dataJop,
    columns: [{
            data: 'jop_release',
            name: 'jop_release',
            width:'200px'
        },
        {
            data: 'tuban1',
            name: 'tuban1'
        },
        {
            data: 'tuban2',
            name: 'tuban2'
        },
        {
            data: 'tuban3',
            name: 'tuban3'
        },
        {
            data: 'tuban4',
            name: 'tuban4'
        },
        {
            data: 'tuban0',
            name: 'tuban0'
        }
    ]
  });

  // dummy data
  let dataRincian = [
    {
      rincian:"Produksi PCC FM#1",
      tuban1:"TUBAN 1&2.TUBAN 1.FM 1.Z1_PROD_PCC.OUT",
      tuban2:"TUBAN 1&2.TUBAN 2.FM 3.Z3_PROD_PCC.OUT",
      tuban3:"Tuban3New.RM3:PROD_FM5_PCC",
      tuban4:"Tuban4.FM7:PROD_FM7_PCC",
      tuban0:""
    },
    {
      rincian:"Pemakaian Terak FM #1 PCC",
      tuban1:"TUBAN 1&2.TUBAN 1.FM 1.Z541WF3FQI_S2.OUT",
      tuban2:"TUBAN 1&2.TUBAN 2.FM 3.Z543WF3FQI_S5.OUT",
      tuban3:"Tuban3New.RM3:TERAK_FM5_PCC",
      tuban4:"Tuban4.FM7:TERAK_FM7_PCC",
      tuban0:""
    },
    {
      rincian:"Pemakaian Gypsum FM #1 PCC",
      tuban1:"TUBAN 1&2.TUBAN 1.FM 1.Z541WF1FQI_S2.OUT",
      tuban2:"TUBAN 1&2.TUBAN 2.FM 3.Z543WF1FQI_S5.OUT",
      tuban3:"Tuban3New.RM3:GYPSUM_FM5_PCC",
      tuban4:"Tuban4.FM7:GYPSUM_FM7_PCC",
      tuban0:""
    },
    {
      rincian:"Pemakaian Trass FM #1 PCC",
      tuban1:"TUBAN 1&2.TUBAN 1.FM 1.Z541WF5FQI_S2.OUT",
      tuban2:"TUBAN 1&2.TUBAN 2.FM 3.Z543WF5FQI_S5.OUT",
      tuban3:"Tuban3New.RM3:TRASS_FM5_PCC",
      tuban4:"Tuban4.FM7:TRASS_FM7_PCC",
      tuban0:""
    }
  ];
  var table = $('#dt_rincian').DataTable({
    processing: true,
    ordering: false,
    responsive: false,
    scrollX: true,
    filter:false,
    paging: false,
    bInfo : false,
    data: dataRincian,
    columns: [{
        data: 'rincian',
        name: 'rincian',
        width:'200px'
      },
      {
          data: 'tuban1',
          name: 'tuban1'
      },
      {
          data: 'tuban2',
          name: 'tuban2'
      },
      {
          data: 'tuban3',
          name: 'tuban3'
      },
      {
          data: 'tuban4',
          name: 'tuban4'
      },
      {
          data: 'tuban0',
          name: 'tuban0',
          width: '200px'
      }
    ]
  });

  // dummy data
  let dataWhrpg = [
    {
      whrpg:"WHRPG Running",
      tuban1:"N/A",
      tuban2:"N/A",
      tuban3:"N/A",
      tuban4:"N/A",
      tuban0:"WHRPG.PLC2.GENERTOR RUN"
    },
    {
      whrpg:"Produksi Listrik (kWh)",
      tuban1:"N/A",
      tuban2:"N/A",
      tuban3:"N/A",
      tuban4:"N/A",
      tuban0:"GCP.Generator.kW"
    },
    {
      whrpg:"Daya Terbangkitkan (MW)",
      tuban1:"N/A",
      tuban2:"N/A",
      tuban3:"N/A",
      tuban4:"N/A",
      tuban0:"GCP.Generator.kWH"
    },
    {
      whrpg:"Jam Operasi Generator (Hours)",
      tuban1:"N/A",
      tuban2:"N/A",
      tuban3:"N/A",
      tuban4:"N/A",
      tuban0:"WHRPG.PLC2.JOP GENERATOR"
    }
  ];
  var table = $('#dt_whrpg').DataTable({
    processing: true,
    ordering: false,
    responsive: false,
    scrollX: true,
    filter:false,
    paging: false,
    bInfo : false,
    data: dataWhrpg,
    columns: [{
        data: 'whrpg',
        name: 'whrpg',
        width:'200px'
      },
      {
        data: 'tuban1',
        name: 'tuban1',
        width:'200px'
      },
      {
        data: 'tuban2',
        name: 'tuban2',
        width:'200px'
      },
      {
        data: 'tuban3',
        name: 'tuban3',
        width:'200px'
      },
      {
        data: 'tuban4',
        name: 'tuban4',
        width:'200px'
      },
      {
        data: 'tuban0',
        name: 'tuban0',
        width: '200px'
      }
    ]
  });

  // dummy data
  let dataPenimbangan = [
    {
        penimbangan:"TRASS",
        terima:"Shipment manajemen",
        plant:"SILO 1",
        opc:"",
        pcc:"",
        opc_premium:"Shipment manajemen",
        pcc_premium:"",
        total:"Shipment manajemen",
    },
    {
        penimbangan:"TRASS",
        terima:"Shipment manajemen",
        plant:"SILO 2",
        opc:"",
        pcc:"Shipment manajemen",
        opc_premium:"",
        pcc_premium:"",
        total:"Shipment manajemen",
    },
    {
        penimbangan:"TRASS",
        terima:"Shipment manajemen",
        plant:"SILO 3",
        opc:"Shipment manajemen",
        pcc:"",
        opc_premium:"",
        pcc_premium:"",
        total:"Shipment manajemen",
    }
  ];
  var table = $('#dt_penimbangan').DataTable({
    processing: true,
    ordering: false,
    responsive: false,
    scrollX: true,
    filter:false,
    paging: false,
    bInfo : false,
    data: dataPenimbangan,
    columns: [{
          data: 'penimbangan',
          name: 'penimbangan',
          width:'200px'
        },
        {
          data: 'terima',
          name: 'terima'
        },
        {
          data: 'plant',
          name: 'plant'
        },
        {
          data: 'opc',
          name: 'opc'
        },
        {
          data: 'pcc',
          name: 'pcc'
        },
        {
          data: 'opc_premium',
          name: 'opc_premium'
        },
        {
          data: 'pcc_premium',
          name: 'pcc_premium'
        },
        {
          data: 'total',
          name: 'total'
        }
    ]
  });
</script>

<x-default-layout>
    <div class="flex-stack mb-0 mb-lg-n4 pt-5">
        <div class="row">
            <div class="col-md-6 mt-2">
                <div class="information">
                     <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">GHOPO Summary Report </h1>
                        <ul class="breadcrumb breadcrumb-separatorless ">
                         <li class="breadcrumb-item text-muted">   
                                        <a href="/" class="text-muted text-hover-primary">Dashboard</a>
                                    </li>
                                    <li class="breadcrumb-item text-muted">/</li>
                                    <li class="breadcrumb-item text-ghopo">Executive Summary Report</li>
                        </ul>
                </div>
            </div>
            <div class="col-md-6">
                <div class="">
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-3 mt-2">
                              <select class="form-select h-25" data-control="select2" data-placeholder="Pilih tahun">
                                @foreach(getYear() as $key => $value)
                                    @if ($value == date("Y") )
                                        <option value="{{$value}}" selected>{{$value}}</option>
                                    @else
                                        <option value="{{$value}}">{{$value}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-3 mt-2">
                              <select class="form-select h-25" data-control="select2" data-placeholder="Pilih bulan">
                                    @foreach(getMonth() as $key => $value)
                                        @if ($value['month'] == date("m") )
                                            <option value="{{$value['month']}}" selected>{{$value['month_name']}}</option>
                                        @else
                                            <option value="{{$value['month']}}">{{$value['month_name']}}</option>
                                        @endif
                                    @endforeach
                                </select>
                        </div>
                        <div class="col-md-3 mt-2">
                            <button class="btn btn-history w-100">Tampilkan</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>

      <div class="row g-5 g-xl-10 mb-5 mb-xl-4 mt-2">
        <div class="col-md-12 col-custom">
            @include('partials/widgets/cards/_widget-executive-summary')
        </div>
    </div>

    
    <div class="row g-5 g-xl-10 mb-5 mb-xl-4">
      <div class="col-md-8 col-custom">
           <div class="card-plant-klin">
            @include('partials/widgets/cards/_widget-klin-rate')
           </div>
      </div>
      <div class="col-md-4 col-custom">
         <div class="card-plant-klin">
            @include('partials/widgets/cards/_widget-plant-property')
           </div>
      </div>
    </div>
    

    <div class="row g-5 g-xl-10 mb-5 mb-xl-5">
       <div class="col-md-12 col-custom">
        @include('partials/widgets/cards/_widget-volume')
       </div>
    </div>
    

    {{-- inventory --}}
    <div class="row g-5 g-xl-10 mb-5 mb-xl-5">
        <div class="col-md-6 col-custom">
            <div class="card-inventory">
                @include('partials/widgets/cards/_widget-inventory-coal-stock')
            </div>
        </div>
         <div class="col-md-6 col-custom">
            <div class="card-inventory">
                @include('partials/widgets/cards/_widget-coal-stock')
            </div>
        </div>
    </div>


</x-default-layout>

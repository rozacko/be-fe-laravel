<form action="{{ route($route . '.store') }}" enctype="multipart/form-data" id="idForm">
    <div class="modal-header">
        <h1 class="mb-3">Create Data</h1>
        <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
            <span class="fa fa-close"></span>
        </div>
    </div>
    <div class="modal-body">
        @csrf
        <div class="d-flex flex-column mb-8 fv-row form-group">
            <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                <span class="required">Code</span>
            </label>
            <input type="text" class="form-control form-control-solid" name="code" />
        </div>
        <div class="d-flex flex-column mb-8 fv-row form-group">
            <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                <span class="required">Nama</span>
            </label>
            <input type="text" class="form-control form-control-solid" name="name" />
        </div>
    </div>
    <div class="modal-footer">
        <div class="text-center">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary" id="btn-submit">
                <span class="indicator-label"><i class="fas fa-spinner fa-spin spinner-btn"></i> Submit</span>
                <span class="indicator-progress">Please wait...
                    <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
            </button>
        </div>
    </div>
</form>

<script type="text/javascript">
    $('.spinner-btn').hide();
    $("#idForm").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.
        var form = $(this);
        var actionUrl = form.attr('action');
        $('#btn-submit .spinner-btn').show();
        $("#btn-submit").attr('disabled', true);
        $.ajax({
            type: "POST",
            url: actionUrl,
            data: form.serialize(),
            success: function(data) {
                if (data.status == 'success') {
                    Swal.fire(
                        'Success',
                        data.message,
                        'success'
                    );
                    $('#modal').modal('hide');
                    $('#tb_data').DataTable().ajax.reload();
                } else {
                    Swal.fire(
                        'Warning',
                        data.message,
                        'warning'
                    );
                }
            }
        }).always(function() {
            $('.spinner-btn').hide();
            $("#btn-submit").attr('disabled', false);
        });
    });
</script>

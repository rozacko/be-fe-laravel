<form class="form-inline" action="{{ route($route . '.addPermission') }}" enctype="multipart/form-data" id="idForm">
    <div class="modal-header">
        <h5 class="modal-title">Edit Permission</h5>
        <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
            <span class="svg-icon svg-icon-2x"></span>
        </div>
    </div>
    <div class="modal-body">
        @csrf
        <input type="hidden" name="uuid" value="{{ $uuid }}">
        <div class="d-flex flex-column">
            @foreach ($data->modules as $modul)
                <li class="d-flex align-items-center py-2">
                    <span class="bullet bullet-vertical bg-primary me-5"></span>
                    <h4>{{ $modul->name }}</h4>
                </li>
                <div class="d-flex flex-column">
                    @foreach ($modul->features as $feature)
                        <li class="d-flex align-items-center pt-6 pb-4">
                            <span class="bullet bullet-vertical bg-success me-5"></span>
                            <h5>{{ $feature->name }}</h5>
                        </li>
                        <div class="row">
                            @foreach ($feature->permissions as $permission)
                                <div class="col-md-3">
                                    <div class="form-check form-check-custom form-check-solid">
                                        <input class="form-check-input" name="permissions[]" type="checkbox"
                                            value="{{ $permission->id }}"
                                            {{ $permission->selected == 'y' ? 'checked' : '' }} />
                                        <label class="form-check-label">
                                            {{ ucwords($permission->name) }}
                                        </label>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endforeach
                </div>
            @endforeach
        </div>
    </div>
    <div class="modal-footer">
        <div class="text-center">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary" id="btn-submit">
                <span class="indicator-label"><i class="fas fa-spinner fa-spin spinner-btn"></i> Submit</span>
                <span class="indicator-progress">Please wait...
                    <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
            </button>
        </div>
    </div>
</form>

<script type="text/javascript">
    $('.spinner-btn').hide();
    $("#idForm").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.
        var form = $(this);
        var actionUrl = form.attr('action');
        $('#btn-submit .spinner-btn').show();
        $("#btn-submit").attr('disabled', true);
        $.ajax({
            type: "post",
            url: actionUrl,
            data: form.serialize(),
            success: function(data) {
                if (data.status == 'success') {
                    Swal.fire(
                        'Success',
                        data.message,
                        'success'
                    );
                    $('#modal').modal('hide');
                } else {
                    Swal.fire(
                        'Warning',
                        data.message,
                        'warning'
                    );
                }
            }
        }).always(function() {
            $('.spinner-btn').hide();
            $("#btn-submit").attr('disabled', false);
        });
    });
</script>

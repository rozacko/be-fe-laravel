<x-default-layout>
    <div class="card card-flush mt-6 mt-xl-9">
        <div class="card-header p-3">
            <div class="card-title flex-column">
                <h3 class="fw-bold mb-1">{{ $pageTitle }}</h3>
            </div>
        </div>
        <div class="card-body pt-0">
            <div class="row">
                <div class="col-lg-3">
                    <div class="card card-flush">
                        <div class="card-header">
                            <div class="card-title">
                                <h2 class="mb-0">Role Detail</h2>
                            </div>
                        </div>
                        <div class="card-body pt-0">
                            <div class="d-flex flex-column text-gray-600">
                                <div class="d-flex align-items-center py-2">
                                    <span class="bullet bg-primary me-3"></span>Code: {{ $data->code }}
                                </div>
                                <div class="d-flex align-items-center py-2">
                                    <span class="bullet bg-primary me-3"></span>Name: {{ $data->name }}
                                </div>
                            </div>
                        </div>
                        <div class="card-footer pt-0">
                            <button type="button" class="btn btn-primary" id="btn-permission">
                                <i class="fas fa-spinner fa-spin spinner-btn"></i> Edit Permission
                            </button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="card card-flush mb-6 mb-xl-9">
                        <div class="card-header pt-5">
                            <div class="card-title">
                                <h2 class="d-flex align-items-center">Users Assigned
                                    <span class="text-gray-600 fs-6 ms-1">({{ count($data->users) }})</span>
                                </h2>
                            </div>
                            <div class="card-toolbar my-1">
                                <button type="button" class="btn btn-sm btn-primary float-right" id="btn-add">
                                    <i class="fas fa-spinner fa-spin spinner-btn"></i> <i class="fas fa-plus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body pt-0">
                            <table class="table align-middle table-row-dashed fs-6 gy-5 mb-0" id="kt_roles_view_table">
                                <thead>
                                    <tr class="text-start text-muted fw-bolder fs-7 text-uppercase gs-0">
                                        <th class="min-w-50px">User ID</th>
                                        <th class="min-w-150px">Name</th>
                                        <th class="min-w-125px">Created At</th>
                                        <th class="min-w-100px">Actions</th>
                                    </tr>
                                </thead>
                                <tbody class="fw-bold text-gray-600">
                                    @foreach ($data->users as $user)
                                        <tr>
                                            <td>{{ $user->id }}</td>
                                            <td>{{ $user->name }}</td>
                                            <td>{{ $user->created_at }}</td>
                                            <td><a href="#" onclick="btn_delete('{{ $user->id }}')"
                                                    class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a></td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered mw-650px">
            <div class="modal-content rounded"></div>
        </div>
    </div>
</x-default-layout>

<script>
    $(function() {
        $('.spinner-btn').hide();
    })

    $('#btn-add').click(function(e) {
        e.preventDefault();
        var url = "{{ route($route . '.user', $uuid) }}";
        $('#btn-add .spinner-btn').show();
        $("#btn-add").attr('disabled', true);
        $.get(url, function(html) {
            $('#modal .modal-content').html(html);
            $('#modal').modal('show');
        }).fail(function(xhr) {
            var {
                message
            } = xhr.responseJSON;
            Swal.fire({
                icon: 'error',
                title: message,
                showConfirmButton: false,
                timer: 3000
            })
        }).always(function() {
            $('.spinner-btn').hide();
            $("#btn-add").attr('disabled', false);
        })
    })

    $('#btn-permission').click(function(e) {
        var url = "{{ route($route . '.permission', $uuid) }}";
        $('#btn-permission .spinner-btn').show();
        $("#btn-permission").attr('disabled', true);
        $.get(url, function(html) {
            $('#modal .modal-content').html(html);
            $('#modal').modal('show');
        }).fail(function(xhr) {
            var {
                message
            } = xhr.responseJSON;
            Swal.fire({
                icon: 'error',
                title: message,
                showConfirmButton: false,
                timer: 3000
            })
        }).always(function() {
            $('.spinner-btn').hide();
            $("#btn-permission").attr('disabled', false);
        })
    })

    function btn_delete(id) {
        event.preventDefault();
        var url = "{{ route($route . '.destroyUser') }}";
        Swal.fire({
            icon: 'warning',
            title: 'Apakah anda yakin?',
            text: "Data ini akan di hapus!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak',
            confirmButtonClass: 'btn btn-primary margin-right-10',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then((result) => {
            if (result.isConfirmed) {
                $.post(url, {
                    _token: "{{ csrf_token() }}",
                    uuid: "{{ $uuid }}",
                    users: [
                        id
                    ]
                }, function(res) {
                    if (res.status == 'success') {
                        Swal.fire(
                            'Success',
                            res.message,
                            'success'
                        );
                        window.location.reload();
                    } else {
                        Swal.fire(
                            'Gagal',
                            res.message,
                            'error'
                        );
                    }
                }, 'json');
            }
        });

        return false;
    }
</script>

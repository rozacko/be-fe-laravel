<div class="modal-header pb-0 border-0 justify-content-end">
    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
        <span class="svg-icon svg-icon-1">
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1"
                    transform="rotate(-45 6 17.3137)" fill="currentColor" />
                <rect x="7.41422" y="6" width="16" height="2" rx="1"
                    transform="rotate(45 7.41422 6)" fill="currentColor" />
            </svg>
        </span>
    </div>
</div>
<div class="modal-body scroll-y px-10 px-lg-15 pt-0 pb-15">
    <div class="mb-13 text-center">
        <h1 class="mb-3">Add Data</h1>
    </div>
    <form method="POST" action="{{ route($route . '.addUser') }}" enctype="multipart/form-data" id="idForm">
        <div class="modal-body">
            @csrf
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">User</span>
                </label>
                <input type="hidden" name="uuid" value="{{$uuid}}">
                <select class="form-select form-select-solid" data-kt-select2="true" data-placeholder="Select option"
                    data-allow-clear="true" id="user-select" name="users[]">
                    <option></option>
                </select>
            </div>
        </div>
        <div class="modal-footer">
            <div class="text-center">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary" id="btn-submit">
                    <span class="indicator-label"><i class="fas fa-spinner fa-spin spinner-btn"></i> Submit</span>
                </button>
            </div>
        </div>
    </form>
</div>


<script type="text/javascript">
    $('.spinner-btn').hide();
    $("#idForm").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.
        var form = $(this);
        var actionUrl = form.attr('action');
        $('#btn-submit .spinner-btn').show();
        $("#btn-submit").attr('disabled', true);
        $.ajax({
            type: "POST",
            url: actionUrl,
            data: form.serialize(),
            success: function(data) {
                if (data.status == 'success') {
                    Swal.fire(
                        'Success',
                        data.message,
                        'success'
                    );
                    $('#modal').modal('hide');
                    window.location.reload();
                } else {
                    Swal.fire(
                        'Warning',
                        data.message,
                        'warning'
                    );
                }
            }
        }).always(function() {
            $('.spinner-btn').hide();
            $("#btn-submit").attr('disabled', false);
        });
    });

    $(function() {
        $("#user-select").select2({
            dropdownParent: $("#modal .modal-content"),
            ajax: {
                headers: {
                    "Accept": "text/json",
                    "Content-Type": "application/json",
                },
                url: "{{ route('configuration.user.table') }}",
                data: function(params) {
                    var query = {
                        name: params.term
                    }
                    return query;
                },
                processResults: function(data) {
                    if (data.data && data.data.length == 0) {
                        $('#user-select').val(null).trigger('change');
                    }
                    return {
                        results: data.data.map(item => {
                            return {
                                id: item.id,
                                text: item.id + " > " + item.name
                            };
                        })
                    };
                }
            },
            placeholder: "No User"
        });
    })
</script>

{{-- Extends layout --}}
<x-default-layout>
    <div class="card card-flush mt-6 mt-xl-9">
        <div class="card-header p-3">
            <div class="card-title flex-column">
                <h3 class="fw-bold mb-1">{{ $pageTitle }}</h3>
            </div>
            <div class="card-toolbar my-1">
                <button type="button" class="btn btn-sm btn-primary float-right btn-add">
                    <i class="fas fa-spinner fa-spin spinner-btn"></i> Tambah Data
                </button>
            </div>
        </div>
        <div class="card-body pt-0">
            <table class="table table-bordered data-table" id="tb_data">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Roles</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>

        </div>
    </div>

    <div class="modal fade" id="modal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered mw-650px">
            <div class="modal-content rounded"></div>
        </div>
    </div>

</x-default-layout>

<script>
    $(function() {
        $('.spinner-btn').hide();
        var table = $('#tb_data').DataTable({
            searchDelay: 2000,
            processing: true,
            serverSide: true,
            ajax: "{{ route($route . '.table') }}",
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    orderable: false,
                    searchable: false,
                },
                {
                    data: 'name',
                    name: 'name'
                },
                {
                    data: 'username',
                    name: 'username'
                },
                {
                    data: 'email',
                    name: 'email '
                },
                {
                    data: 'roles',
                    name: 'roles',
                    orderable: false,
                    searchable: false,
                    render: function(data, type, row) {
                        let roles = '';
                        if (data.length > 0) {
                            data.forEach(function myFunction(item, index, arr) {
                                roles +=
                                    `<span class="badge badge-success">${item.name}</span> `
                            })
                        }
                        return roles;
                    }
                },
                {
                    data: 'uuid',
                    name: 'uuid',
                    orderable: false,
                    searchable: false,
                    width: 150,
                    render: function(data, type, row) {
                        let button =
                            `<a href="#" onclick="show('${data}')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>&nbsp;
                             <a href="#" onclick="btn_delete('${data}')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>`;
                        return button;
                    }
                },
            ],
        });
    });
    setTimeout(function() {
        if ($('#alertNotif').length > 0) {
            $('#alertNotif').remove();
        }
    }, 5000)

    $('.btn-add').click(function(e) {
        e.preventDefault();
        var url = "{{ route($route . '.create') }}";
        $('.spinner-btn').show();
        $(".btn-add").attr('disabled', true);
        $.get(url, function(html) {
            $('#modal .modal-content').html(html);
            $('#modal').modal('show');
        }).fail(function(xhr) {
            var {
                message
            } = xhr.responseJSON;
            Swal.fire({
                icon: 'error',
                title: message,
                showConfirmButton: false,
                timer: 3000
            })
        }).always(function() {
            $('.spinner-btn').hide();
            $(".btn-add").attr('disabled', false);
        })
    })

    function show(id) {
        event.preventDefault();
        var url = '{{ route($route . '.edit', ':id') }}';
        url = url.replace(':id', id);
        $.get(url, function(html) {
            $('#modal .modal-content').html(html);
            $('#modal').modal('show');
        }).fail(function(xhr) {
            var {
                message
            } = xhr.responseJSON;
            Swal.fire({
                icon: 'error',
                title: message,
                showConfirmButton: false,
                timer: 3000
            })
        });
    }

    function btn_delete(id) {
        event.preventDefault();
        var url = '{{ route($route . '.destroy', ':id') }}';
        url = url.replace(':id', id);
        Swal.fire({
            icon: 'warning',
            title: 'Apakah anda yakin?',
            text: "Data ini akan di hapus!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak',
            confirmButtonClass: 'btn btn-primary margin-right-10',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then((result) => {
            if (result.isConfirmed) {
                $.post(url, {
                    _token: "{{ csrf_token() }}",
                    _method: 'DELETE'
                }, function(res) {
                    if (res.status == 'success') {
                        Swal.fire(
                            'Success',
                            res.message,
                            'success'
                        );
                        $('#tb_data').DataTable().ajax.reload();
                    } else {
                        Swal.fire(
                            'Gagal',
                            res.message,
                            'error'
                        );
                    }
                }, 'json');
            }
        });

        return false;
    }
</script>

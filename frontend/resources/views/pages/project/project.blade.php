<x-default-layout>
<div class="card card-flush mt-6 mt-xl-9">
								<!--begin::Card header-->
								<div class="card-header p-3">
									<!--begin::Card title-->
									<div class="card-title flex-column">
										<h3 class="fw-bold mb-1">Project Spendings</h3>
										<div class="fs-6 text-gray-400">Total $260,300 sepnt so far</div>
									</div>
									<!--begin::Card title-->
									<!--begin::Card toolbar-->
									<div class="card-toolbar my-1">
										<!--begin::Select-->
										<div class="me-6 my-1">
											<select id="kt_filter_year" name="year" data-control="select2" data-hide-search="true" class="w-125px form-select form-select-solid form-select-sm select2-hidden-accessible" data-select2-id="select2-data-kt_filter_year" tabindex="-1" aria-hidden="true" data-kt-initialized="1">
												<option value="All" selected="selected" data-select2-id="select2-data-44-rfrq">All time</option>
												<option value="thisyear">This year</option>
												<option value="thismonth">This month</option>
												<option value="lastmonth">Last month</option>
												<option value="last90days">Last 90 days</option>
											</select><span class="select2 select2-container select2-container--bootstrap5" dir="ltr" data-select2-id="select2-data-43-z1y1" style="width: 100%;"><span class="selection"><span class="select2-selection select2-selection--single w-125px form-select form-select-solid form-select-sm" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-disabled="false" aria-labelledby="select2-kt_filter_year-container" aria-controls="select2-kt_filter_year-container"><span class="select2-selection__rendered" id="select2-kt_filter_year-container" role="textbox" aria-readonly="true" title="All time">All time</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
										</div>
										<!--end::Select-->
										<!--begin::Select-->
										<div class="me-4 my-1">
											<select id="kt_filter_orders" name="orders" data-control="select2" data-hide-search="true" class="w-125px form-select form-select-solid form-select-sm select2-hidden-accessible" data-select2-id="select2-data-kt_filter_orders" tabindex="-1" aria-hidden="true" data-kt-initialized="1">
												<option value="All" selected="selected" data-select2-id="select2-data-46-n74k">All Orders</option>
												<option value="Approved">Approved</option>
												<option value="Declined">Declined</option>
												<option value="In Progress">In Progress</option>
												<option value="In Transit">In Transit</option>
											</select><span class="select2 select2-container select2-container--bootstrap5" dir="ltr" data-select2-id="select2-data-45-iekc" style="width: 100%;"><span class="selection"><span class="select2-selection select2-selection--single w-125px form-select form-select-solid form-select-sm" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-disabled="false" aria-labelledby="select2-kt_filter_orders-container" aria-controls="select2-kt_filter_orders-container"><span class="select2-selection__rendered" id="select2-kt_filter_orders-container" role="textbox" aria-readonly="true" title="All Orders">All Orders</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
										</div>
										<!--end::Select-->
										<!--begin::Search-->
										<div class="d-flex align-items-center position-relative my-1">
											<!--begin::Svg Icon | path: icons/duotune/general/gen021.svg-->
											<span class="svg-icon svg-icon-3 position-absolute ms-3">
												<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
													<rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2" rx="1" transform="rotate(45 17.0365 15.1223)" fill="currentColor"></rect>
													<path d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z" fill="currentColor"></path>
												</svg>
											</span>
											<!--end::Svg Icon-->
											<input type="text" id="kt_filter_search" class="form-control form-control-solid form-select-sm w-150px ps-9" placeholder="Search Order">
										</div>
<a href="#" class="btn btn-sm btn-light-success me-3" data-bs-toggle="modal" data-bs-target="#kt_modal_create_project">Create</a>
                    <a href="#" class="btn btn-sm btn-primary me-3" data-bs-toggle="modal" data-bs-target="#kt_modal_new_target">Add Target</a>
	<!--begin::Menu-->
													<div class="me-0">
														<button class="btn btn-sm btn-icon btn-bg-light btn-active-color-primary" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
															<i class="bi bi-three-dots fs-3"></i>
														</button>
														<!--begin::Menu 3-->
														<div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-semibold w-200px py-3" data-kt-menu="true">
															<!--begin::Heading-->
															<div class="menu-item px-3">
																<div class="menu-content text-muted pb-2 px-3 fs-7 text-uppercase">Payments</div>
															</div>
															<!--end::Heading-->
															<!--begin::Menu item-->
															<div class="menu-item px-3">
																<a href="#" class="menu-link px-3">Create Invoice</a>
															</div>
															<!--end::Menu item-->
															<!--begin::Menu item-->
															<div class="menu-item px-3">
																<a href="#" class="menu-link flex-stack px-3">Create Payment
																<i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Specify a target name for future usage and reference"></i></a>
															</div>
															<!--end::Menu item-->
															<!--begin::Menu item-->
															<div class="menu-item px-3">
																<a href="#" class="menu-link px-3">Generate Bill</a>
															</div>
															<!--end::Menu item-->
															<!--begin::Menu item-->
															<div class="menu-item px-3" data-kt-menu-trigger="hover" data-kt-menu-placement="right-end">
																<a href="#" class="menu-link px-3">
																	<span class="menu-title">Subscription</span>
																	<span class="menu-arrow"></span>
																</a>
																<!--begin::Menu sub-->
																<div class="menu-sub menu-sub-dropdown w-175px py-4">
																	<!--begin::Menu item-->
																	<div class="menu-item px-3">
																		<a href="#" class="menu-link px-3">Plans</a>
																	</div>
																	<!--end::Menu item-->
																	<!--begin::Menu item-->
																	<div class="menu-item px-3">
																		<a href="#" class="menu-link px-3">Billing</a>
																	</div>
																	<!--end::Menu item-->
																	<!--begin::Menu item-->
																	<div class="menu-item px-3">
																		<a href="#" class="menu-link px-3">Statements</a>
																	</div>
																	<!--end::Menu item-->
																	<!--begin::Menu separator-->
																	<div class="separator my-2"></div>
																	<!--end::Menu separator-->
																	<!--begin::Menu item-->
																	<div class="menu-item px-3">
																		<div class="menu-content px-3">
																			<!--begin::Switch-->
																			<label class="form-check form-switch form-check-custom form-check-solid">
																				<!--begin::Input-->
																				<input class="form-check-input w-30px h-20px" type="checkbox" value="1" checked="checked" name="notifications" />
																				<!--end::Input-->
																				<!--end::Label-->
																				<span class="form-check-label text-muted fs-6">Recuring</span>
																				<!--end::Label-->
																			</label>
																			<!--end::Switch-->
																		</div>
																	</div>
																	<!--end::Menu item-->
																</div>
																<!--end::Menu sub-->
															</div>
															<!--end::Menu item-->
															<!--begin::Menu item-->
															<div class="menu-item px-3 my-1">
																<a href="#" class="menu-link px-3">Settings</a>
															</div>
															<!--end::Menu item-->
														</div>
														<!--end::Menu 3-->
													</div>
													<!--end::Menu-->
										<!--end::Search-->
									</div>
									<!--begin::Card toolbar-->
								</div>
								<!--end::Card header-->
								<!--begin::Card body-->
								<div class="card-body pt-0">
									<!--begin::Table container-->
									<div class="table-responsive">
										<!--begin::Table-->
										<div id="kt_profile_overview_table_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer"><div class="table-responsive"><table id="kt_profile_overview_table" class="table table-row-bordered table-row-dashed gy-4 align-middle fw-bold dataTable no-footer">
											<!--begin::Head-->
											<thead class="fs-7 text-gray-400 text-uppercase">
												<tr><th class="min-w-250px sorting" tabindex="0" aria-controls="kt_profile_overview_table" rowspan="1" colspan="1" aria-label="Manager: activate to sort column ascending" style="width: 447.391px;">Manager</th><th class="min-w-150px sorting" tabindex="0" aria-controls="kt_profile_overview_table" rowspan="1" colspan="1" aria-label="Date: activate to sort column ascending" style="width: 276.125px;">Date</th><th class="min-w-90px sorting" tabindex="0" aria-controls="kt_profile_overview_table" rowspan="1" colspan="1" aria-label="Amount: activate to sort column ascending" style="width: 168.75px;">Amount</th><th class="min-w-90px sorting" tabindex="0" aria-controls="kt_profile_overview_table" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending" style="width: 179.016px;">Status</th><th class="min-w-50px text-end sorting" tabindex="0" aria-controls="kt_profile_overview_table" rowspan="1" colspan="1" aria-label="Details: activate to sort column ascending" style="width: 120.219px;">Details</th></tr>
											</thead>
											<!--end::Head-->
											<!--begin::Body-->
											<tbody class="fs-6">

											<tr class="odd">
													<td>
														<!--begin::User-->
														<div class="d-flex align-items-center">
															<!--begin::Wrapper-->
															<div class="me-5 position-relative">
																<!--begin::Avatar-->
																<div class="symbol symbol-35px symbol-circle">
																	<img alt="Pic" src="assets/media/avatars/300-6.jpg">
																</div>
																<!--end::Avatar-->
															</div>
															<!--end::Wrapper-->
															<!--begin::Info-->
															<div class="d-flex flex-column justify-content-center">
																<a href="" class="fs-6 text-gray-800 text-hover-primary">Emma Smith</a>
																<div class="fw-semibold text-gray-400">smith@kpmg.com</div>
															</div>
															<!--end::Info-->
														</div>
														<!--end::User-->
													</td>
													<td data-order="2023-03-10T00:00:00+07:00">Mar 10, 2023</td>
													<td>$701.00</td>
													<td>
														<span class="badge badge-light-info fw-bold px-4 py-3">In progress</span>
													</td>
													<td class="text-end">
														<a href="#" class="btn btn-light btn-sm">View</a>
													</td>
												</tr><tr class="even">
													<td>
														<!--begin::User-->
														<div class="d-flex align-items-center">
															<!--begin::Wrapper-->
															<div class="me-5 position-relative">
																<!--begin::Avatar-->
																<div class="symbol symbol-35px symbol-circle">
																	<span class="symbol-label bg-light-danger text-danger fw-semibold">M</span>
																</div>
																<!--end::Avatar-->
																<!--begin::Online-->
																<div class="bg-success position-absolute h-8px w-8px rounded-circle translate-middle start-100 top-100 ms-n1 mt-n1"></div>
																<!--end::Online-->
															</div>
															<!--end::Wrapper-->
															<!--begin::Info-->
															<div class="d-flex flex-column justify-content-center">
																<a href="" class="fs-6 text-gray-800 text-hover-primary">Melody Macy</a>
																<div class="fw-semibold text-gray-400">melody@altbox.com</div>
															</div>
															<!--end::Info-->
														</div>
														<!--end::User-->
													</td>
													<td data-order="2023-11-10T00:00:00+07:00">Nov 10, 2023</td>
													<td>$980.00</td>
													<td>
														<span class="badge badge-light-success fw-bold px-4 py-3">Approved</span>
													</td>
													<td class="text-end">
														<a href="#" class="btn btn-light btn-sm">View</a>
													</td>
												</tr><tr class="odd">
													<td>
														<!--begin::User-->
														<div class="d-flex align-items-center">
															<!--begin::Wrapper-->
															<div class="me-5 position-relative">
																<!--begin::Avatar-->
																<div class="symbol symbol-35px symbol-circle">
																	<img alt="Pic" src="assets/media/avatars/300-1.jpg">
																</div>
																<!--end::Avatar-->
															</div>
															<!--end::Wrapper-->
															<!--begin::Info-->
															<div class="d-flex flex-column justify-content-center">
																<a href="" class="fs-6 text-gray-800 text-hover-primary">Max Smith</a>
																<div class="fw-semibold text-gray-400">max@kt.com</div>
															</div>
															<!--end::Info-->
														</div>
														<!--end::User-->
													</td>
													<td data-order="2023-10-25T00:00:00+07:00">Oct 25, 2023</td>
													<td>$943.00</td>
													<td>
														<span class="badge badge-light-info fw-bold px-4 py-3">In progress</span>
													</td>
													<td class="text-end">
														<a href="#" class="btn btn-light btn-sm">View</a>
													</td>
												</tr><tr class="even">
													<td>
														<!--begin::User-->
														<div class="d-flex align-items-center">
															<!--begin::Wrapper-->
															<div class="me-5 position-relative">
																<!--begin::Avatar-->
																<div class="symbol symbol-35px symbol-circle">
																	<img alt="Pic" src="assets/media/avatars/300-5.jpg">
																</div>
																<!--end::Avatar-->
															</div>
															<!--end::Wrapper-->
															<!--begin::Info-->
															<div class="d-flex flex-column justify-content-center">
																<a href="" class="fs-6 text-gray-800 text-hover-primary">Sean Bean</a>
																<div class="fw-semibold text-gray-400">sean@dellito.com</div>
															</div>
															<!--end::Info-->
														</div>
														<!--end::User-->
													</td>
													<td data-order="2023-06-20T00:00:00+07:00">Jun 20, 2023</td>
													<td>$750.00</td>
													<td>
														<span class="badge badge-light-danger fw-bold px-4 py-3">Rejected</span>
													</td>
													<td class="text-end">
														<a href="#" class="btn btn-light btn-sm">View</a>
													</td>
												</tr><tr class="odd">
													<td>
														<!--begin::User-->
														<div class="d-flex align-items-center">
															<!--begin::Wrapper-->
															<div class="me-5 position-relative">
																<!--begin::Avatar-->
																<div class="symbol symbol-35px symbol-circle">
																	<img alt="Pic" src="assets/media/avatars/300-25.jpg">
																</div>
																<!--end::Avatar-->
															</div>
															<!--end::Wrapper-->
															<!--begin::Info-->
															<div class="d-flex flex-column justify-content-center">
																<a href="" class="fs-6 text-gray-800 text-hover-primary">Brian Cox</a>
																<div class="fw-semibold text-gray-400">brian@exchange.com</div>
															</div>
															<!--end::Info-->
														</div>
														<!--end::User-->
													</td>
													<td data-order="2023-10-25T00:00:00+07:00">Oct 25, 2023</td>
													<td>$589.00</td>
													<td>
														<span class="badge badge-light-success fw-bold px-4 py-3">Approved</span>
													</td>
													<td class="text-end">
														<a href="#" class="btn btn-light btn-sm">View</a>
													</td>
												</tr><tr class="even">
													<td>
														<!--begin::User-->
														<div class="d-flex align-items-center">
															<!--begin::Wrapper-->
															<div class="me-5 position-relative">
																<!--begin::Avatar-->
																<div class="symbol symbol-35px symbol-circle">
																	<span class="symbol-label bg-light-warning text-warning fw-semibold">C</span>
																</div>
																<!--end::Avatar-->
																<!--begin::Online-->
																<div class="bg-success position-absolute h-8px w-8px rounded-circle translate-middle start-100 top-100 ms-n1 mt-n1"></div>
																<!--end::Online-->
															</div>
															<!--end::Wrapper-->
															<!--begin::Info-->
															<div class="d-flex flex-column justify-content-center">
																<a href="" class="fs-6 text-gray-800 text-hover-primary">Mikaela Collins</a>
																<div class="fw-semibold text-gray-400">mik@pex.com</div>
															</div>
															<!--end::Info-->
														</div>
														<!--end::User-->
													</td>
													<td data-order="2023-04-15T00:00:00+07:00">Apr 15, 2023</td>
													<td>$953.00</td>
													<td>
														<span class="badge badge-light-danger fw-bold px-4 py-3">Rejected</span>
													</td>
													<td class="text-end">
														<a href="#" class="btn btn-light btn-sm">View</a>
													</td>
												</tr><tr class="odd">
													<td>
														<!--begin::User-->
														<div class="d-flex align-items-center">
															<!--begin::Wrapper-->
															<div class="me-5 position-relative">
																<!--begin::Avatar-->
																<div class="symbol symbol-35px symbol-circle">
																	<img alt="Pic" src="assets/media/avatars/300-9.jpg">
																</div>
																<!--end::Avatar-->
															</div>
															<!--end::Wrapper-->
															<!--begin::Info-->
															<div class="d-flex flex-column justify-content-center">
																<a href="" class="fs-6 text-gray-800 text-hover-primary">Francis Mitcham</a>
																<div class="fw-semibold text-gray-400">f.mit@kpmg.com</div>
															</div>
															<!--end::Info-->
														</div>
														<!--end::User-->
													</td>
													<td data-order="2023-03-10T00:00:00+07:00">Mar 10, 2023</td>
													<td>$979.00</td>
													<td>
														<span class="badge badge-light-success fw-bold px-4 py-3">Approved</span>
													</td>
													<td class="text-end">
														<a href="#" class="btn btn-light btn-sm">View</a>
													</td>
												</tr><tr class="even">
													<td>
														<!--begin::User-->
														<div class="d-flex align-items-center">
															<!--begin::Wrapper-->
															<div class="me-5 position-relative">
																<!--begin::Avatar-->
																<div class="symbol symbol-35px symbol-circle">
																	<span class="symbol-label bg-light-danger text-danger fw-semibold">O</span>
																</div>
																<!--end::Avatar-->
																<!--begin::Online-->
																<div class="bg-success position-absolute h-8px w-8px rounded-circle translate-middle start-100 top-100 ms-n1 mt-n1"></div>
																<!--end::Online-->
															</div>
															<!--end::Wrapper-->
															<!--begin::Info-->
															<div class="d-flex flex-column justify-content-center">
																<a href="" class="fs-6 text-gray-800 text-hover-primary">Olivia Wild</a>
																<div class="fw-semibold text-gray-400">olivia@corpmail.com</div>
															</div>
															<!--end::Info-->
														</div>
														<!--end::User-->
													</td>
													<td data-order="2023-06-24T00:00:00+07:00">Jun 24, 2023</td>
													<td>$512.00</td>
													<td>
														<span class="badge badge-light-info fw-bold px-4 py-3">In progress</span>
													</td>
													<td class="text-end">
														<a href="#" class="btn btn-light btn-sm">View</a>
													</td>
												</tr><tr class="odd">
													<td>
														<!--begin::User-->
														<div class="d-flex align-items-center">
															<!--begin::Wrapper-->
															<div class="me-5 position-relative">
																<!--begin::Avatar-->
																<div class="symbol symbol-35px symbol-circle">
																	<span class="symbol-label bg-light-primary text-primary fw-semibold">N</span>
																</div>
																<!--end::Avatar-->
																<!--begin::Online-->
																<div class="bg-success position-absolute h-8px w-8px rounded-circle translate-middle start-100 top-100 ms-n1 mt-n1"></div>
																<!--end::Online-->
															</div>
															<!--end::Wrapper-->
															<!--begin::Info-->
															<div class="d-flex flex-column justify-content-center">
																<a href="" class="fs-6 text-gray-800 text-hover-primary">Neil Owen</a>
																<div class="fw-semibold text-gray-400">owen.neil@gmail.com</div>
															</div>
															<!--end::Info-->
														</div>
														<!--end::User-->
													</td>
													<td data-order="2023-10-25T00:00:00+07:00">Oct 25, 2023</td>
													<td>$472.00</td>
													<td>
														<span class="badge badge-light-danger fw-bold px-4 py-3">Rejected</span>
													</td>
													<td class="text-end">
														<a href="#" class="btn btn-light btn-sm">View</a>
													</td>
												</tr><tr class="even">
													<td>
														<!--begin::User-->
														<div class="d-flex align-items-center">
															<!--begin::Wrapper-->
															<div class="me-5 position-relative">
																<!--begin::Avatar-->
																<div class="symbol symbol-35px symbol-circle">
																	<img alt="Pic" src="assets/media/avatars/300-23.jpg">
																</div>
																<!--end::Avatar-->
															</div>
															<!--end::Wrapper-->
															<!--begin::Info-->
															<div class="d-flex flex-column justify-content-center">
																<a href="" class="fs-6 text-gray-800 text-hover-primary">Dan Wilson</a>
																<div class="fw-semibold text-gray-400">dam@consilting.com</div>
															</div>
															<!--end::Info-->
														</div>
														<!--end::User-->
													</td>
													<td data-order="2023-06-20T00:00:00+07:00">Jun 20, 2023</td>
													<td>$830.00</td>
													<td>
														<span class="badge badge-light-danger fw-bold px-4 py-3">Rejected</span>
													</td>
													<td class="text-end">
														<a href="#" class="btn btn-light btn-sm">View</a>
													</td>
												</tr></tbody>
											<!--end::Body-->
										</table></div><div class="row"><div class="col-sm-12 col-md-5 d-flex align-items-center justify-content-center justify-content-md-start"><div class="dataTables_length" id="kt_profile_overview_table_length"><label><select name="kt_profile_overview_table_length" aria-controls="kt_profile_overview_table" class="form-select form-select-sm form-select-solid"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select></label></div></div><div class="col-sm-12 col-md-7 d-flex align-items-center justify-content-center justify-content-md-end"><div class="dataTables_paginate paging_simple_numbers" id="kt_profile_overview_table_paginate"><ul class="pagination"><li class="paginate_button page-item previous disabled" id="kt_profile_overview_table_previous"><a href="#" aria-controls="kt_profile_overview_table" data-dt-idx="0" tabindex="0" class="page-link"><i class="previous"></i></a></li><li class="paginate_button page-item active"><a href="#" aria-controls="kt_profile_overview_table" data-dt-idx="1" tabindex="0" class="page-link">1</a></li><li class="paginate_button page-item "><a href="#" aria-controls="kt_profile_overview_table" data-dt-idx="2" tabindex="0" class="page-link">2</a></li><li class="paginate_button page-item "><a href="#" aria-controls="kt_profile_overview_table" data-dt-idx="3" tabindex="0" class="page-link">3</a></li><li class="paginate_button page-item next" id="kt_profile_overview_table_next"><a href="#" aria-controls="kt_profile_overview_table" data-dt-idx="4" tabindex="0" class="page-link"><i class="next"></i></a></li></ul></div></div></div></div>
										<!--end::Table-->
									</div>
									<!--end::Table container-->
								</div>
								<!--end::Card body-->
							</div>
</x-default-layout>
<x-default-layout>
    <!--begin::Row-->
    <div class="row g-5 g-xl-10 mb-5 mb-xl-10">
        <div class="card card-executive-summary">
            <div class="card-header">
                <div class="card-title">
                    <span>Shipment Management Synchronization</span>
                </div>

                <div class="card-toolbar my-1">
                    <div class="me-6 my-1">
                        Tanggal Sinkronisasi
                    </div>
                    <div class="me-6 my-1">
                        <input type="text" id="filter_tanggal" class="form-control form-control-solid form-select-sm w-150px ps-9" value="{{date('d/m/Y', strtotime('yesterday'))}}">
                    </div>
                </div>
            </div>

            <div class="card-body">
                <div class="dataTables_wrapper dt-bootstrap4 no-footer">
                    <div class="table-responsive">
                        <table id="datatable-shipment" class="table table-row-bordered table-row-dashed gy-4 align-middle fw-bold dataTable no-footer" style="width:100%">
                            <thead class="fs-7 text-gray-400 text-uppercase">
                                <tr>
                                    <th>Shipment Management</th>
                                    <th>Tanggal Sinkronisasi</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-default-layout>

<script>
    var shipmenttable;
    $(document).ready(function () {
        shipmenttable = $('#datatable-shipment').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{route('shipment.datatable')}}",
                data: function (d) {
                    d.tgl = $('#filter_tanggal').val();
                    // d.custom = $('#myInput').val();
                    // etc
                },
            },
            columns: [{
                data: 'api_name',
                name: 'api_name'
                },
                {
                data: 'synched_time',
                name: 'synched_time'
                },
                {
                data: 'status',
                name: 'status'
                },
                {
                data: 'action',
                name: 'action',
                orderable: false,
                searchable: false,
                width: 150
                },
            ]
        });
        $('#filter_tanggal').flatpickr({
            dateFormat: "d/m/Y",
            onChange: function(dt){
                shipmenttable.ajax.reload();
            },
        });
    });

    function syncData(id){
        event.preventDefault(); 
        var url = "{{ route('shipment.syncdata') }}";

        $.post(url,{id_tag: id, tgl_sync: $('#filter_tanggal').val()},function(res){
            if(res.status=='success'){
                Swal.fire(
                    'Success',
                    res.message,
                    'success'
                );
               shipmenttable.ajax.reload();
            }
            else{
                Swal.fire(
                    'Warning',
                    res.message,
                    'warning'
                );
            }
        },'json');
    }
</script>

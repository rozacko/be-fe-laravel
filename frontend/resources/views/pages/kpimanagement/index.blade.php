<x-default-layout>
  <div class="g-5 g-xl-10 mb-3 mb-xl-3 pt-5">
    <div class="row">
      <div class="col-sm-3">
        <div class="information mt-2">
          <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">
            KPI Management
          </h1>
          <ul class="breadcrumb breadcrumb-separatorless ">
                <li class="breadcrumb-item text-muted">
                  <a href="/" class="text-muted text-hover-primary">Dashboard</a>
                </li>
                <li class="breadcrumb-item text-muted">/</li>
                <li class="breadcrumb-item text-ghopo">KPI Management</li>
          </ul>
         </div>
      </div>
      <div class="col-sm-9 d-flex justify-content-md-end flex-no-wrap">
          <div class="filter">
              <div class="row">
                <div class="col-6 col-lg col-md mt-2">
                  <select class="form-select h-25" data-control="select2" data-placeholder="Pilih Bulan">
                    @foreach(getMonth() as $key => $value)
                      @if ($value['month'] == date("n") )
                          <option value="{{$value['month']}}" selected>{{$value['month_name']}}</option>
                      @else
                          <option value="{{$value['month']}}">{{$value['month_name']}}</option>
                      @endif
                    @endforeach
                  </select>
                </div>
                <div class="col-6 col-lg col-md mt-2">
                  <select class="form-select h-25" data-control="select2" data-placeholder="Pilih Tahun">
                    @foreach(getYear() as $key => $value)
                      @if ($value == date("Y") )
                          <option value="{{$value}}" selected>{{$value}}</option>
                      @else
                          <option value="{{$value}}">{{$value}}</option>
                      @endif
                    @endforeach
                  </select>
                </div>
                <div class="col-6 col-lg col-md mt-2">
                  <button class="btn btn-history w-100">Tampilkan</button>
                </div>
                <div class="col-6 col-lg col-md mt-2">
                  <button class="btn btn-add w-100">Download</button>
                </div>
              </div>
          </div>
      </div>
    </div>
  </div>
  <div class="card mt-6 mt-xl-9">
    <div class="card-body">
      <table class="table table-striped table-row-bordered" id="dt_kpi">
        <thead>
            <tr>
              <th class="fw-bold align-middle" rowspan="2">group</th>
              <th class="fw-bold align-middle text-center" rowspan="2">No</th>
              <th class="fw-bold align-middle" rowspan="2">Key Performance Indicator (KPI)</th>
              <th class="fw-bold align-middle text-center" rowspan="2">Unit</th>
              <th class="fw-bold align-middle text-center" rowspan="2">Polarisasi</th>
              <th class="fw-bold text-center" colspan="3">MTD</th>
              <th class="fw-bold text-center" colspan="3">YTD</th>
              <th class="fw-bold text-center" colspan="3">Prognosa FY</th>
            </tr>
            <tr>
              <th class="fw-bold text-center">Target</th>
              <th class="fw-bold text-center">Real</th>
              <th class="fw-bold text-center">% Capaian KPI</th>
              <th class="fw-bold text-center">Target</th>
              <th class="fw-bold text-center">Real</th>
              <th class="fw-bold text-center">% Capaian KPI</th>
              <th class="fw-bold text-center">Target</th>
              <th class="fw-bold text-center">Real</th>
              <th class="fw-bold text-center">% Capaian KPI</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>
  </div>
</x-default-layout>

<script>

  $(function() {
    // dummy data
    let dataKPI = [
        {
          group: "A. Nilai Ekonomi dan Sosial untuk Indonesia",
          no: 1,
          kpi: "Pembuatan Saluran",
          unit:"%",
          polarisasi:"Max",
          mtdTarget: Math.floor((Math.random() * 100) + 10),
          mtdReal: Math.floor((Math.random() * 100) + 10),
          mtdKpi: Math.floor((Math.random() * 100) + 10),
          ytdTarget: Math.floor((Math.random() * 100) + 10),
          ytdReal: Math.floor((Math.random() * 100) + 10),
          ytdKpi: Math.floor((Math.random() * 100) + 10),
          prognosaTarget: Math.floor((Math.random() * 100) + 10),
          prognosaReal: Math.floor((Math.random() * 100) + 10),
          prognosaKpi: Math.floor((Math.random() * 100) + 10),
        },
        {
          group: "A. Nilai Ekonomi dan Sosial untuk Indonesia",
          no: 1,
          kpi: "Pembuatan Saluran",
          unit:"Jam",
          polarisasi:"Max",
          mtdTarget: Math.floor((Math.random() * 100) + 10),
          mtdReal: Math.floor((Math.random() * 100) + 10),
          mtdKpi: Math.floor((Math.random() * 100) + 10),
          ytdTarget: Math.floor((Math.random() * 100) + 10),
          ytdReal: Math.floor((Math.random() * 100) + 10),
          ytdKpi: Math.floor((Math.random() * 100) + 10),
          prognosaTarget: Math.floor((Math.random() * 100) + 10),
          prognosaReal: Math.floor((Math.random() * 100) + 10),
          prognosaKpi: Math.floor((Math.random() * 100) + 10),
        },
        {
          group: "A. Nilai Ekonomi dan Sosial untuk Indonesia",
          no: 1,
          kpi: "Pembuatan Saluran",
          unit:"Score",
          polarisasi:"Max",
          mtdTarget: Math.floor((Math.random() * 100) + 10),
          mtdReal: Math.floor((Math.random() * 100) + 10),
          mtdKpi: Math.floor((Math.random() * 100) + 10),
          ytdTarget: Math.floor((Math.random() * 100) + 10),
          ytdReal: Math.floor((Math.random() * 100) + 10),
          ytdKpi: Math.floor((Math.random() * 100) + 10),
          prognosaTarget: Math.floor((Math.random() * 100) + 10),
          prognosaReal: Math.floor((Math.random() * 100) + 10),
          prognosaKpi: Math.floor((Math.random() * 100) + 10),
        },
        {
          group: "A. Nilai Ekonomi dan Sosial untuk Indonesia",
          no: 1,
          kpi: "Pembuatan Saluran",
          unit:"%",
          polarisasi:"Max",
          mtdTarget: Math.floor((Math.random() * 100) + 10),
          mtdReal: Math.floor((Math.random() * 100) + 10),
          mtdKpi: Math.floor((Math.random() * 100) + 10),
          ytdTarget: Math.floor((Math.random() * 100) + 10),
          ytdReal: Math.floor((Math.random() * 100) + 10),
          ytdKpi: Math.floor((Math.random() * 100) + 10),
          prognosaTarget: Math.floor((Math.random() * 100) + 10),
          prognosaReal: Math.floor((Math.random() * 100) + 10),
          prognosaKpi: Math.floor((Math.random() * 100) + 10),
        },
        {
          group: "A. Nilai Ekonomi dan Sosial untuk Indonesia",
          no: 1,
          kpi: "Pembuatan Saluran",
          unit:"kCal/Kg Clinker",
          polarisasi:"Max",
          mtdTarget: Math.floor((Math.random() * 100) + 10),
          mtdReal: Math.floor((Math.random() * 100) + 10),
          mtdKpi: Math.floor((Math.random() * 100) + 10),
          ytdTarget: Math.floor((Math.random() * 100) + 10),
          ytdReal: Math.floor((Math.random() * 100) + 10),
          ytdKpi: Math.floor((Math.random() * 100) + 10),
          prognosaTarget: Math.floor((Math.random() * 100) + 10),
          prognosaReal: Math.floor((Math.random() * 100) + 10),
          prognosaKpi: Math.floor((Math.random() * 100) + 10),
        },
        {
          group: "A. Nilai Ekonomi dan Sosial untuk Indonesia",
          no: 1,
          kpi: "Pembuatan Saluran",
          unit:"%",
          polarisasi:"Max",
          mtdTarget: Math.floor((Math.random() * 100) + 10),
          mtdReal: Math.floor((Math.random() * 100) + 10),
          mtdKpi: Math.floor((Math.random() * 100) + 10),
          ytdTarget: Math.floor((Math.random() * 100) + 10),
          ytdReal: Math.floor((Math.random() * 100) + 10),
          ytdKpi: Math.floor((Math.random() * 100) + 10),
          prognosaTarget: Math.floor((Math.random() * 100) + 10),
          prognosaReal: Math.floor((Math.random() * 100) + 10),
          prognosaKpi: Math.floor((Math.random() * 100) + 10),
        },
        {
          group: "A. Nilai Ekonomi dan Sosial untuk Indonesia",
          no: 1,
          kpi: "Pembuatan Saluran",
          unit:"%",
          polarisasi:"Max",
          mtdTarget: Math.floor((Math.random() * 100) + 10),
          mtdReal: Math.floor((Math.random() * 100) + 10),
          mtdKpi: Math.floor((Math.random() * 100) + 10),
          ytdTarget: Math.floor((Math.random() * 100) + 10),
          ytdReal: Math.floor((Math.random() * 100) + 10),
          ytdKpi: Math.floor((Math.random() * 100) + 10),
          prognosaTarget: Math.floor((Math.random() * 100) + 10),
          prognosaReal: Math.floor((Math.random() * 100) + 10),
          prognosaKpi: Math.floor((Math.random() * 100) + 10),
        },
        {
          group: "A. Nilai Ekonomi dan Sosial untuk Indonesia",
          no: 1,
          kpi: "Pembuatan Saluran",
          unit:"%",
          polarisasi:"Max",
          mtdTarget: Math.floor((Math.random() * 100) + 10),
          mtdReal: Math.floor((Math.random() * 100) + 10),
          mtdKpi: Math.floor((Math.random() * 100) + 10),
          ytdTarget: Math.floor((Math.random() * 100) + 10),
          ytdReal: Math.floor((Math.random() * 100) + 10),
          ytdKpi: Math.floor((Math.random() * 100) + 10),
          prognosaTarget: Math.floor((Math.random() * 100) + 10),
          prognosaReal: Math.floor((Math.random() * 100) + 10),
          prognosaKpi: Math.floor((Math.random() * 100) + 10),
        },
        {
          group: "B. Inovasi Model Bisnis",
          no: 1,
          kpi: "Pembuatan Saluran",
          unit:"Rp. Miliar",
          polarisasi:"Max",
          mtdTarget: "CLOSE",
          mtdReal: "CLOSE",
          mtdKpi: "CLOSE",
          ytdTarget: "CLOSE",
          ytdReal: "CLOSE",
          ytdKpi: "CLOSE",
          prognosaTarget: "CLOSE",
          prognosaReal: "CLOSE",
          prognosaKpi: "CLOSE",
        },
        {
          group: "B. Inovasi Model Bisnis",
          no: 1,
          kpi: "Pembuatan Saluran",
          unit:"$/ton Clinker",
          polarisasi:"Max",
          mtdTarget: "CLOSE",
          mtdReal: "CLOSE",
          mtdKpi: "CLOSE",
          ytdTarget: "CLOSE",
          ytdReal: "CLOSE",
          ytdKpi: "CLOSE",
          prognosaTarget: "CLOSE",
          prognosaReal: "CLOSE",
          prognosaKpi: "CLOSE",
        },
        {
          group: "B. Inovasi Model Bisnis",
          no: 1,
          kpi: "Pembuatan Saluran",
          unit:"%",
          polarisasi:"Max",
          mtdTarget: "CLOSE",
          mtdReal: "CLOSE",
          mtdKpi: "CLOSE",
          ytdTarget: "CLOSE",
          ytdReal: "CLOSE",
          ytdKpi: "CLOSE",
          prognosaTarget: "CLOSE",
          prognosaReal: "CLOSE",
          prognosaKpi: "CLOSE",
        },
        {
          group: "B. Inovasi Model Bisnis",
          no: 1,
          kpi: "Pembuatan Saluran",
          unit:"kWH/ton",
          polarisasi:"Max",
          mtdTarget: "CLOSE",
          mtdReal: "CLOSE",
          mtdKpi: "CLOSE",
          ytdTarget: "CLOSE",
          ytdReal: "CLOSE",
          ytdKpi: "CLOSE",
          prognosaTarget: "CLOSE",
          prognosaReal: "CLOSE",
          prognosaKpi: "CLOSE",
        },
        {
          group: "C. Kepemimpinan Teknologi",
          no: 1,
          kpi: "Pembuatan Saluran",
          unit:"Number of Equipment",
          polarisasi:"Max",
          mtdTarget: "CLOSE",
          mtdReal: "CLOSE",
          mtdKpi: "CLOSE",
          ytdTarget: "CLOSE",
          ytdReal: "CLOSE",
          ytdKpi: "CLOSE",
          prognosaTarget: "CLOSE",
          prognosaReal: "CLOSE",
          prognosaKpi: "CLOSE",
        },
        {
          group: "D. Peningkatan Investasi",
          no: 1,
          kpi: "Pembuatan Saluran",
          unit:"%",
          polarisasi:"Max",
          mtdTarget: "CLOSE",
          mtdReal: "CLOSE",
          mtdKpi: "CLOSE",
          ytdTarget: "CLOSE",
          ytdReal: "CLOSE",
          ytdKpi: "CLOSE",
          prognosaTarget: "CLOSE",
          prognosaReal: "CLOSE",
          prognosaKpi: "CLOSE",
        },
        {
          group: "E. Pengembangan Talenta",
          no: 1,
          kpi: "Pembuatan Saluran",
          unit:"%",
          polarisasi:"Max",
          mtdTarget: "CLOSE",
          mtdReal: "CLOSE",
          mtdKpi: "CLOSE",
          ytdTarget: "CLOSE",
          ytdReal: "CLOSE",
          ytdKpi: "CLOSE",
          prognosaTarget: "CLOSE",
          prognosaReal: "CLOSE",
          prognosaKpi: "CLOSE",
        }
    ];
    var table = $('#dt_kpi').DataTable({
        processing: true,
        ordering: false,
        responsive: false,
        scrollX: true,
        filter:false,
        paging: false,
        bInfo : false,
        data: dataKPI,
        columns: [
          {
            data: 'group',
            name: 'group',
            visible: false
          },
          {
            data: 'no',
            name: 'no',
            className: "text-center"
          },
          {
            data: 'kpi',
            name: 'kpi'
          },
          {
            data: 'unit',
            name: 'unit',
            className:"text-center"
          },
          {
            data: 'polarisasi',
            name: 'polarisasi',
            className:"text-center"
          },
          {
            data: 'mtdTarget',
            name: 'mtdTarget',
            className:"text-center"
          },
          {
            data: 'mtdReal',
            name: 'mtdReal',
            className:"text-center"
          },
          {
            data: 'mtdKpi',
            name: 'mtdKpi',
            className:"text-center",
          },
          {
            data: 'ytdTarget',
            name: 'ytdTarget',
            className:"text-center"
          },
          {
            data: 'ytdReal',
            name: 'ytdReal',
            className:"text-center"
          },
          {
            data: 'ytdKpi',
            name: 'ytdKpi',
            className:"text-center",
          },
          {
            data: 'prognosaTarget',
            name: 'prognosaTarget',
            className:"text-center"
          },
          {
            data: 'prognosaReal',
            name: 'prognosaReal',
            className:"text-center"
          },
          {
            data: 'prognosaKpi',
            name: 'prognosaKpi',
            className:"text-center",
          }
        ],
        drawCallback: function (settings) {
          var api = this.api();
          var rows = api.rows({ page: 'current' }).nodes();
          var last = null;
          api
            .column(0, { page: 'current' })
            .data()
            .each(function (group, i) {
                if (last !== group) {
                    $(rows)
                      .eq(i)
                      .before('<tr style="background-color: #D0D0D0;"><td colspan="13"><b>'
                      + group + '</td></tr>');
                    last = group;
                }
            });
        },
        rowCallback: function(row, data, index){
          if(data['mtdKpi'] < 75){
              $(row).find('td:eq(6)').addClass("bg-table-bad");
          }else {
              $(row).find('td:eq(6)').addClass("bg-table-good");
          }

          if(data['ytdKpi'] < 75){
              $(row).find('td:eq(9)').addClass("bg-table-bad");
          }else {
              $(row).find('td:eq(9)').addClass("bg-table-good");
          }

          if(data['prognosaKpi'] < 75){
              $(row).find('td:eq(12)').addClass("bg-table-bad");
          }else {
              $(row).find('td:eq(12)').addClass("bg-table-good");
          }
        },
    });
  });
</script>

<div class="modal-header pb-0 border-0 justify-content-end">
    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
        <span class="svg-icon svg-icon-1">
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1"
                    transform="rotate(-45 6 17.3137)" fill="currentColor" />
                <rect x="7.41422" y="6" width="16" height="2" rx="1"
                    transform="rotate(45 7.41422 6)" fill="currentColor" />
            </svg>
        </span>
    </div>
</div>
<div class="modal-body scroll-y px-10 px-lg-15 pt-0 pb-15">
    <div class="mb-13 text-center">
        <h1 class="mb-3">Edit Data Hot Mill</h1>
    </div>
    <form action="{{ url('/'.$route . '/edit-hm'. '/' .$data->uuid) }}" enctype="multipart/form-data" id="form-edit-hm">
        <div class="modal-body">
            @csrf
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">Tanggal</span>
                </label>
                <input type="text" class="form-control form-control-solid" name="tanggal" id="tanggal"
                    value="{{ $data->tanggal }}" disabled/>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">Quality</span>
                </label>
                <select id="quality" class="form-control form-control-solid" name="quality" disabled>
                    <option value="{{ $data->quality }}">{{ $data->quality }}</option>
                </select>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">SO3 ILC</span>
                </label>
                <input type="text" class="form-control form-control-solid" name="so3_ilc" id="so3_ilc"
                    value="{{ $data->so3_ilc }}" />
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">SO3 SLC</span>
                </label>
                <input type="text" class="form-control form-control-solid" name="so3_slc" id="so3_slc"
                    value="{{ $data->so3_slc }}" />
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">DERAJAT CALSINASI ILC</span>
                </label>
                <input type="text" class="form-control form-control-solid" name="derajat_kalsinasi_ilc" id="derajat_kalsinasi_ilc"
                    value="{{ $data->derajat_kalsinasi_ilc }}" />
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">DERAJAT CALSINASI SLC</span>
                </label>
                <input type="text" class="form-control form-control-solid" name="derajat_kalsinasi_slc" id="derajat_kalsinasi_slc"
                    value="{{ $data->derajat_kalsinasi_slc }}" />
            </div>
        </div>
        <div class="modal-footer">
            <div class="text-center">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary" id="btn-submit">
                    <span class="indicator-label"></i> Submit</span>
                </button>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    $("#form-edit-hm").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.
        var form = $(this);
        var actionUrl = form.attr('action');
        $.ajax({
            type: "PUT",
            url: actionUrl,
            data: {
                'tanggal': $('#tanggal').val(),
                'quality': $('#quality').val(),
                'so3_ilc': $('#so3_ilc').val(),
                'so3_slc': $('#so3_slc').val(),
                'derajat_kalsinasi_ilc': $('#derajat_kalsinasi_ilc').val(),
                'derajat_kalsinasi_slc': $('#derajat_kalsinasi_slc').val(),
            },
            success: function(data)
            {
                if(data.status == 'success'){
                    Swal.fire(
                        'Success',
                        data.message,
                        'success'
                    );
                    $('#modal-update-hm').modal('hide');
                    hotMill();
                }else{
                    Swal.fire(
                        'Warning',
                        data.message,
                        'warning'
                    );
                }
            }
        });
    });
</script>

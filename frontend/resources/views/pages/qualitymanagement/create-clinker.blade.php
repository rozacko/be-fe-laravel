<div class="modal-header pb-0 border-0 justify-content-end">
    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
        <span class="svg-icon svg-icon-1">
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="currentColor" />
                <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="currentColor" />
            </svg>
        </span>
    </div>
</div>
<div class="modal-body scroll-y px-10 px-lg-15 pt-0 pb-15">
    <div class="text-center">
        <h1>Import Data Clinker</h1>
    </div>
    <form method="POST" action="{{ url('/'.$route . '/store-ck') }}" enctype="multipart/form-data" id="form-ck">
        <div class="modal-body">
            @csrf
            <div class="d-flex flex-column mb-5 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    Pilih Tanggal
                </label>
                <input class="form-select h-25" id="date-modal-ck">
            </div>
            <div class="d-flex flex-column mb-5 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    Plant
                </label>
                <select class="form-select h-25" data-control="select2" data-placeholder="Pilih Plant" id="plant-modal-ck">
                </select>
            </div>
            <div class="d-flex flex-column mb-5 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    Template
                </label>
                <a href="{{ url('/'.$route.'/temp-ck') }}" class="button form-control h-25">
                    Download Template
                </a>
            </div>
            <div class="d-flex flex-column mb-5 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    Upload File
                </label>
                <input type="file" class="form-control h-25 d-flex" id="upload_file_ck">
                <p class="mt-2" style="color: #1F7793;">*file yang di upload adalah file berdasarkan template yang disediakan</p>
            </div>
        </div>
        <div class="modal-footer">
            <div class="text-center">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                <button id="btn_submit" type="submit" class="btn btn-primary">
                    <span class="indicator-label">Submit</span>
                </button>
            </div>
        </div>
    </form>
</div>

<script>
    $(document).ready(function () {
        filterPlant()
    })

    $("#form-ck").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.
        var form = $(this);
        var formData = new FormData();
        var actionUrl = form.attr('action');
        formData.append('plant_id', $('#plant-modal-ck').val());
        formData.append('tanggal', $('#date-modal-ck').val());
        formData.append('upload_file', document.getElementById('upload_file_ck').files[0]);
        $.ajax({
            type: "POST",
            url: "{{ url('/'.$route . '/store-ck') }}",
            data: formData,
            dataType: 'JSON',
            contentType: false,
            processData: false,
            success: function(data)
            {
                if(data.status == 'success'){
                    Swal.fire(
                        'Success',
                        data.message,
                        'success'
                    );
                    $('#modal-ck').modal('hide');
                    clinker();
                }else{
                    Swal.fire(
                        'Warning',
                        data.message,
                        'warning'
                    );
                }
            }
        });
    });

    //config date picker
    $("#date-modal-ck").daterangepicker({
        singleDatePicker: true,
        locale: {
            format: 'DD MMMM YYYY'
        }
    });

    function filterPlant() {
        $.ajax({
            type: "GET",
            url: "{{ url('/inspection/plant') }}",
            dataType: 'JSON',
            contentType: false,
            processData: false,
            success: function(data){
                $.each(data.data, function (i, item) {
                    var newOption = new Option(item.nm_plant, item.id, false, false);
                    $('#plant-modal-ck').append(newOption);
                });
            }
        });
    }
</script>
<div class="modal-header pb-0 border-0 justify-content-end">
    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
        <span class="svg-icon svg-icon-1">
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1"
                    transform="rotate(-45 6 17.3137)" fill="currentColor" />
                <rect x="7.41422" y="6" width="16" height="2" rx="1"
                    transform="rotate(45 7.41422 6)" fill="currentColor" />
            </svg>
        </span>
    </div>
</div>
<div class="modal-body scroll-y px-10 px-lg-15 pt-0 pb-15">
    <div class="mb-13 text-center">
        <h1 class="mb-3">Edit Data Coal Mill</h1>
    </div>
    <form action="{{ url('/'.$route . '/edit-cm'. '/' .$data->uuid) }}" enctype="multipart/form-data" id="form-edit-cm">
        <div class="modal-body">
            @csrf
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">Tanggal</span>
                </label>
                <input type="text" class="form-control form-control-solid" name="tanggal" id="tanggal"
                    value="{{ $data->tanggal }}" disabled/>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">Quality</span>
                </label>
                <select id="quality" class="form-control form-control-solid" name="quality" disabled>
                    <option value="{{ $data->quality }}">{{ $data->quality }}</option>
                </select>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">GHV</span>
                </label>
                <input type="text" class="form-control form-control-solid" name="ghv" id="ghv"
                    value="{{ $data->ghv }}" />
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">AC</span>
                </label>
                <input type="text" class="form-control form-control-solid" name="ac" id="ac"
                    value="{{ $data->ac }}" />
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">TM</span>
                </label>
                <input type="text" class="form-control form-control-solid" name="tm" id="tm"
                    value="{{ $data->tm }}" />
            </div>
        </div>
        <div class="modal-footer">
            <div class="text-center">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary" id="btn-submit">
                    <span class="indicator-label"></i> Submit</span>
                </button>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    $("#form-edit-cm").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.
        var form = $(this);
        var actionUrl = form.attr('action');
        $.ajax({
            type: "PUT",
            url: actionUrl,
            data: {
                'tanggal': $('#tanggal').val(),
                'quality': $('#quality').val(),
                'ghv': $('#ghv').val(),
                'ac': $('#ac').val(),
                'tm': $('#tm').val(),
            },
            success: function(data)
            {
                if(data.status == 'success'){
                    Swal.fire(
                        'Success',
                        data.message,
                        'success'
                    );
                    $('#modal-update-cm').modal('hide');
                    coalMill();
                }else{
                    Swal.fire(
                        'Warning',
                        data.message,
                        'warning'
                    );
                }
            }
        });
    });
</script>

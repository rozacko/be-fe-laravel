<x-default-layout>
  <div class="g-5 g-xl-10 mb-3 mb-xl-3 pt-5">
    <div class="row">
      <div class="col-sm-3">
        <div class="information">
          <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">
            Dashboard Quality Management
          </h1>
          <ul class="breadcrumb breadcrumb-separatorless ">
                <li class="breadcrumb-item text-muted">
                  <a href="/" class="text-muted text-hover-primary">Dashboard Quality Management</a>
                </li>
                <li class="breadcrumb-item text-muted">/</li>
                <li class="breadcrumb-item text-ghopo">Dashboard</li>
          </ul>
         </div>
      </div>
    </div>
  </div>
  <div class="card card-flush mt-6 mt-xl-9">
    <div class="card-body">
      <ul class="nav nav-tabs nav-line-tabs nav-line-tabs-2x pt-5" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="#raw-mill" data-bs-toggle="tab">RAW MILL</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" aria-current="page" href="#kiln-feed" data-bs-toggle="tab">KILN FEED</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" aria-current="page" href="#clinker" data-bs-toggle="tab">CLINKER</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" aria-current="page" href="#cement" data-bs-toggle="tab">SEMEN</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" aria-current="page" href="#coal-mill" data-bs-toggle="tab">COAL MILL</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" aria-current="page" href="#hot-mill" data-bs-toggle="tab">HOT MILL</a>
        </li>
      </ul>
      <div class="tab-content" id="myTabContent">
        {{-- RAW MILL --}}
        <div class="tab-pane fade show active" id="raw-mill" role="tabpanel">
          <div class="pt-3">
            <div class="card">
              <div class="card-body">
                <div class="row py-2">
                  <div class="col-sm-4 pt-3">
                    <h5 class="class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0 pt-2">KUALITAS RAW MILL</h5>
                  </div>
                  <div class="col-sm-8 d-flex justify-content-md-end flex-no-wrap">
                    <div class="filter">
                        <div class="row">
                          <div class="col">
                            <select class="form-select h-25" data-control="select2" id="filter-plant-rm">
                              <option value="">All Plant</option>
                            </select>
                          </div>
                          <div class="col">
                            <input class="form-control form-control-solid" placeholder="Pick date rage"
                            id="date-rm"/>
                          </div>
                          <div class="col card-breadcrumb-button ">
                              <button class="btn btn-history" id="apply-rm">Tampilkan</button>
                          </div>
                          <div class="col card-breadcrumb-button ">
                            <button class="btn btn-add" id="download-rm">Download</button>
                          </div>
                          <div class="col card-breadcrumb-button ">
                            <button class="btn btn-add" id="import-rm">Import Data</button>
                          </div>
                        </div>
                    </div>
                  </div>
                </div>
                <table class="table table-striped table-row-bordered" id="dt_raw_mill">
                  <thead>
                      <tr>
                        <th class="fw-bold text-center">PLANT</th>
                        <th class="fw-bold text-center">JAM</th>
                        <th class="fw-bold text-center">LSF</th>
                        <th class="fw-bold text-center">SIM</th>
                        <th class="fw-bold text-center">ALM</th>
                        <th class="fw-bold text-center">H20</th>
                        <th class="fw-bold text-center">Res90</th>
                        <th class="fw-bold text-center">Res200</th>
                        <th class="fw-bold text-center">Action</th>
                      </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

        {{-- KILN FEED --}}
        <div class="tab-pane fade" id="kiln-feed" role="tabpanel">
          <div class="pt-3">
            <div class="card">
              <div class="card-body">
                <div class="row py-2">
                  <div class="col-sm-4 pt-3">
                    <h5 class="class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0 pt-2">KUALITAS KILN FEED</h5>
                  </div>
                  <div class="col-sm-8 d-flex justify-content-md-end flex-no-wrap">
                    <div class="filter">
                        <div class="row">
                          <div class="col">
                            <select class="form-select h-25" data-control="select2" id="filter-plant-kf">
                              <option value="">All Plant</option>
                            </select>
                          </div>
                          <div class="col">
                            <input class="form-control form-control-solid" placeholder="Pick date rage"
                            id="date-kf"/>
                          </div>
                          <div class="col card-breadcrumb-button ">
                              <button class="btn btn-history" id="apply-kf">Tampilkan</button>
                          </div>
                          <div class="col card-breadcrumb-button ">
                            <button class="btn btn-add" id="download-kf">Download</button>
                          </div>
                          <div class="col card-breadcrumb-button ">
                            <button class="btn btn-add" id="import-kf">Import Data</button>
                          </div>
                        </div>
                    </div>
                  </div>
                </div>
                <table class="table table-striped table-row-bordered" id="dt_kiln_feed">
                  <thead>
                      <tr>
                        <th class="fw-bold text-center">PLANT</th>
                        <th class="fw-bold text-center">JAM</th>
                        <th class="fw-bold text-center">LSF</th>
                        <th class="fw-bold text-center">SIM</th>
                        <th class="fw-bold text-center">ALM</th>
                        <th class="fw-bold text-center">H20</th>
                        <th class="fw-bold text-center">Res90</th>
                        <th class="fw-bold text-center">Res200</th>
                        <th class="fw-bold text-center">Action</th>
                      </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

        {{-- CLINKER --}}
        <div class="tab-pane fade" id="clinker" role="tabpanel">
          <div class="pt-3">
            <div class="card">
              <div class="card-body">
                <div class="row py-2">
                  <div class="col-sm-4 pt-3">
                    <h5 class="class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0 pt-2">KUALITAS CLINKER</h5>
                  </div>
                  <div class="col-sm-8 d-flex justify-content-md-end flex-no-wrap">
                    <div class="filter">
                        <div class="row">
                          <div class="col">
                            <select class="form-select h-25" data-control="select2" id="filter-plant-ck">
                              <option value="">All Plant</option>
                            </select>
                          </div>
                          <div class="col">
                            <input class="form-control form-control-solid" placeholder="Pick date rage"
                            id="date-ck"/>
                          </div>
                          <div class="col card-breadcrumb-button ">
                              <button class="btn btn-history" id="apply-ck">Tampilkan</button>
                          </div>
                          <div class="col card-breadcrumb-button ">
                            <button class="btn btn-add" id="download-ck">Download</button>
                          </div>
                          <div class="col card-breadcrumb-button ">
                            <button class="btn btn-add" id="import-ck">Import Data</button>
                          </div>
                        </div>
                    </div>
                  </div>
                </div>
                <table class="table table-striped table-row-bordered" id="dt_clinker">
                  <thead>
                      <tr>
                        <th class="fw-bold text-center">PLANT</th>
                        <th class="fw-bold text-center">JAM</th>
                        <th class="fw-bold text-center">LSF</th>
                        <th class="fw-bold text-center">SIM</th>
                        <th class="fw-bold text-center">ALM</th>
                        <th class="fw-bold text-center">C3S</th>
                        <th class="fw-bold text-center">FCaO</th>
                        <th class="fw-bold text-center">SO3</th>
                        <th class="fw-bold text-center">Action</th>
                      </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

        {{-- SEMEN --}}
        <div class="tab-pane fade" id="cement" role="tabpanel">
          <div class="pt-3">
            <div class="card">
              <div class="card-body">
                <div class="row py-2">
                  <div class="col-sm-4 pt-3">
                    <h5 class="class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0 pt-2">KUALITAS SEMEN</h5>
                  </div>
                  <div class="col-sm-8 d-flex justify-content-md-end flex-no-wrap">
                    <div class="filter">
                        <div class="row">
                          <div class="col">
                            <select class="form-select h-25" data-control="select2" id="filter-plant-se">
                              <option value="">All Plant</option>
                            </select>
                          </div>
                          <div class="col">
                            <input class="form-control form-control-solid" placeholder="Pick date rage"
                            id="date-se"/>
                          </div>
                          <div class="col card-breadcrumb-button ">
                              <button class="btn btn-history" id="apply-se">Tampilkan</button>
                          </div>
                          <div class="col card-breadcrumb-button ">
                            <button class="btn btn-add" id="download-se">Download</button>
                          </div>
                          <div class="col card-breadcrumb-button ">
                            <button class="btn btn-add" id="import-se">Import Data</button>
                          </div>
                        </div>
                    </div>
                  </div>
                </div>
                <table class="table table-striped table-row-bordered" id="dt_cement">
                  <thead>
                      <tr>
                        <th class="fw-bold text-center">PLANT</th>
                        <th class="fw-bold text-center">JAM</th>
                        <th class="fw-bold text-center">SO3</th>
                        <th class="fw-bold text-center">FCaO</th>
                        <th class="fw-bold text-center">Blaine</th>
                        <th class="fw-bold text-center">RES45</th>
                        <th class="fw-bold text-center">LOI</th>
                        <th class="fw-bold text-center">Action</th>
                      </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

        {{-- COAL MILL --}}
        <div class="tab-pane fade" id="coal-mill" role="tabpanel">
          <div class="pt-3">
            <div class="card">
              <div class="card-body">
                <div class="row py-2">
                  <div class="col-sm-4 pt-3">
                    <h5 class="class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0 pt-2">KUALITAS COAL MILL</h5>
                  </div>
                  <div class="col-sm-8 d-flex justify-content-md-end flex-no-wrap">
                    <div class="filter">
                        <div class="row">
                          <div class="col">
                            <input class="form-control form-control-solid" placeholder="Pick date rage"
                            id="date-cm"/>
                          </div>
                          <div class="col card-breadcrumb-button ">
                              <button class="btn btn-history" id="apply-cm">Tampilkan</button>
                          </div>
                          <div class="col card-breadcrumb-button ">
                            <button class="btn btn-add" id="download-cm">Download</button>
                          </div>
                          <div class="col card-breadcrumb-button ">
                            <button class="btn btn-add" id="import-cm">Import Data</button>
                          </div>
                        </div>
                    </div>
                  </div>
                </div>
                <table class="table table-striped table-row-bordered" id="dt_coal_mill">
                  <thead>
                      <tr>
                        <th class="fw-bold text-center">QUALITY</th>
                        <th class="fw-bold text-center">TANGGAL</th>
                        <th class="fw-bold text-center">GHV</th>
                        <th class="fw-bold text-center">AC</th>
                        <th class="fw-bold text-center">TM</th>
                        <th class="fw-bold text-center">Action</th>
                      </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

        {{-- HOT MILL --}}
        <div class="tab-pane fade" id="hot-mill" role="tabpanel">
          <div class="pt-3">
            <div class="card">
              <div class="card-body">
                <div class="row py-2">
                  <div class="col-sm-4 pt-3">
                    <h5 class="class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0 pt-2">KUALITAS HOT MILL</h5>
                  </div>
                  <div class="col-sm-8 d-flex justify-content-md-end flex-no-wrap">
                    <div class="filter">
                        <div class="row">
                          <div class="col">
                            <input class="form-control form-control-solid" placeholder="Pick date rage"
                            id="date-hm"/>
                          </div>
                          <div class="col card-breadcrumb-button ">
                              <button class="btn btn-history" id="apply-hm">Tampilkan</button>
                          </div>
                          <div class="col card-breadcrumb-button ">
                            <button class="btn btn-add" id="download-hm">Download</button>
                          </div>
                          <div class="col card-breadcrumb-button ">
                            <button class="btn btn-add" id="import-hm">Import Data</button>
                          </div>
                        </div>
                    </div>
                  </div>
                </div>
                <table class="table table-striped table-row-bordered" id="dt_hot_mill">
                  <thead>
                      <tr>
                          <th class="fw-bold text-center">QUALITY</th>
                          <th class="fw-bold text-center">TANGGAL</th>
                          <th class="fw-bold text-center">SO3 ILC</th>
                          <th class="fw-bold text-center">SO3 SLC</th>
                          <th class="fw-bold text-center">DERAJAT CALSINASI ILC</th>
                          <th class="fw-bold text-center">DERAJAT CALSINASI SLC</th>
                          <th class="fw-bold text-center">Action</th>
                      </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  {{-- Modal Import --}}
  <div class="modal fade" id="modal-rm" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered mw-500px">
      <div class="modal-content rounded"></div>
    </div>
  </div>
  <div class="modal fade" id="modal-kf" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered mw-500px">
      <div class="modal-content rounded"></div>
    </div>
  </div>
  <div class="modal fade" id="modal-ck" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered mw-500px">
      <div class="modal-content rounded"></div>
    </div>
  </div>
  <div class="modal fade" id="modal-se" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered mw-500px">
      <div class="modal-content rounded"></div>
    </div>
  </div>
  <div class="modal fade" id="modal-cm" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered mw-500px">
      <div class="modal-content rounded"></div>
    </div>
  </div>
  <div class="modal fade" id="modal-hm" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered mw-500px">
      <div class="modal-content rounded"></div>
    </div>
  </div>

  {{-- Modal Update --}}
  <div class="modal fade" id="modal-update-rm" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered mw-650px">
        <div class="modal-content rounded"></div>
    </div>
  </div>
  <div class="modal fade" id="modal-update-kf" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered mw-650px">
        <div class="modal-content rounded"></div>
    </div>
  </div>
  <div class="modal fade" id="modal-update-ck" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered mw-650px">
        <div class="modal-content rounded"></div>
    </div>
  </div>
  <div class="modal fade" id="modal-update-se" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered mw-650px">
        <div class="modal-content rounded"></div>
    </div>
  </div>
  <div class="modal fade" id="modal-update-cm" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered mw-650px">
        <div class="modal-content rounded"></div>
    </div>
  </div>
  <div class="modal fade" id="modal-update-hm" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered mw-650px">
        <div class="modal-content rounded"></div>
    </div>
  </div>
</x-default-layout>

<script>
  let dataRawMill;
  let dataKilnFeed;
  let dataClinker;
  let dataCement;
  let dataCoalMill;
  let dataHotMill;

  $(document).ready(function () {
    // config datatable and nav tabs
    $('a[data-bs-toggle="tab"]').on( 'shown.bs.tab', function (e) {
        $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
    } );

    filterPlantRM()
    filterPlantKF()
    filterPlantCK()
    filterPlantSE()
    filterPlantCM()
    filterPlantHM()

  })

  //config date picker
  $("#date-rm, #date-kf, #date-ck, #date-se, #date-cm, #date-hm").daterangepicker({
    singleDatePicker: true
  });

  // config all filter
  function filterPlantRM() {
    $.ajax({
        type: "GET",
        url: "{{ url('/inspection/plant') }}",
        dataType: 'JSON',
        contentType: false,
        processData: false,
        success: function(data){
          $.each(data.data, function (i, item) {
              var newOption = new Option(item.nm_plant, item.id, false, false);
              $('#filter-plant-rm').append(newOption);
          });
          rawMill()
        }
    });
  }

  function filterPlantKF() {
    $.ajax({
        type: "GET",
        url: "{{ url('/inspection/plant') }}",
        dataType: 'JSON',
        contentType: false,
        processData: false,
        success: function(data){
          $.each(data.data, function (i, item) {
              var newOption = new Option(item.nm_plant, item.id, false, false);
              $('#filter-plant-kf').append(newOption);
          });
          kilnFeed()
        }
    });
  }

  function filterPlantCK() {
    $.ajax({
        type: "GET",
        url: "{{ url('/inspection/plant') }}",
        dataType: 'JSON',
        contentType: false,
        processData: false,
        success: function(data){
          $.each(data.data, function (i, item) {
              var newOption = new Option(item.nm_plant, item.id, false, false);
              $('#filter-plant-ck').append(newOption);
          });
          clinker()
        }
    });
  }

  function filterPlantSE() {
    $.ajax({
        type: "GET",
        url: "{{ url('/inspection/plant') }}",
        dataType: 'JSON',
        contentType: false,
        processData: false,
        success: function(data){
          $.each(data.data, function (i, item) {
              var newOption = new Option(item.nm_plant, item.id, false, false);
              $('#filter-plant-se').append(newOption);
          });
          cement()
        }
    });
  }

  function filterPlantCM() {
    coalMill()
  }

  function filterPlantHM() {
    hotMill()
  }

  // function datatable all tab
  function rawMill() {
    $.ajax({
      type: "GET",
      url: "{{ url('/'.$route . '/datatable-rm') }}",
      data: {
        'plant_id': $('#filter-plant-rm').val(),
        'tanggal': $('#date-rm').val()
      },
      dataType: 'JSON',
      beforeSend: function() {
        $('#dt_raw_mill').LoadingOverlay('show');
      },
      success: function(data){
        dataRawMill = data.data
        tableRawMill(dataRawMill, true)
      }
    }).always(function(){
      $('#dt_raw_mill').LoadingOverlay('hide');
    });
  }
  function tableRawMill(data, redraw= false) {
    var table_raw_mill = $('#dt_raw_mill').DataTable({
      processing: true,
      ordering: true,
      responsive: false,
      scrollX: true,
      filter:false,
      data: data,
      columns: [{
              data: 'nm_plant',
              name: 'nm_plant',
              class: 'text-center',
          },{
              data: 'jam',
              name: 'jam',
              class: 'text-center',
          },
          {
              data: 'lsf',
              name: 'lsf',
              class: 'text-center',
          },
          {
              data: 'sim',
              name: 'sim',
              class: 'text-center',
          },
          {
              data: 'alm',
              name: 'alm',
              class: 'text-center',
          },
          {
              data: 'h2o',
              name: 'h2o',
              class: 'text-center',
          },
          {
              data: 'res90',
              name: 'res90',
              class: 'text-center',
          },
          {
              data: 'res200',
              name: 'res200',
              class: 'text-center',
          },
          {
              data: 'uuid',
              name: 'action',
              width: 150,
              render: function(data, type, full, meta) {
                let action =
                `<center>
                <button onClick="showRM('${data}')" class="btn btn-outline btn-primary btn-sm"><i class="fa fa-edit"></i></button>&nbsp;
                <button onClick="deleteRM('${data}')" class="btn btn-outline btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                </center>`;
                return action;
              }
          }
      ],
    });
    if (redraw) {
        table_raw_mill.clear();
        table_raw_mill.rows.add(data);
        table_raw_mill.draw();
    };
  }

  function kilnFeed() {
    $.ajax({
      type: "GET",
      url: "{{ url('/'.$route . '/datatable-kf') }}",
      data: {
        'plant_id': $('#filter-plant-kf').val(),
        'tanggal': $('#date-kf').val()
      },
      dataType: 'JSON',
      beforeSend: function() {
        $('#dt_kiln_feed').LoadingOverlay('show');
      },
      success: function(data){
          dataKilnFeed = data.data
          tableKilnFeed(dataKilnFeed, true)
      }
    }).always(function(){
      $('#dt_kiln_feed').LoadingOverlay('hide');
    });
  }
  function tableKilnFeed(data, redraw= false) {
    var table_kiln_feed = $('#dt_kiln_feed').DataTable({
      processing: true,
      ordering: true,
      responsive: false,
      scrollX: true,
      filter:false,
      data: data,
      columns: [{
              data: 'nm_plant',
              name: 'nm_plant',
              class: 'text-center',
          },{
              data: 'jam',
              name: 'jam',
              class: 'text-center',
          },
          {
              data: 'lsf',
              name: 'lsf',
              class: 'text-center',
          },
          {
              data: 'sim',
              name: 'sim',
              class: 'text-center',
          },
          {
              data: 'alm',
              name: 'alm',
              class: 'text-center',
          },
          {
              data: 'h2o',
              name: 'h2o',
              class: 'text-center',
          },
          {
              data: 'res90',
              name: 'res90',
              class: 'text-center',
          },
          {
              data: 'res200',
              name: 'res200',
              class: 'text-center',
          },
          {
              data: 'uuid',
              name: 'action',
              width: 150,
              render: function(data, type, full, meta) {
                let action =
                `<center>
                <button onClick="showKF('${data}')" class="btn btn-outline btn-primary btn-sm"><i class="fa fa-edit"></i></button>&nbsp;
                <button onClick="deleteKF('${data}')" class="btn btn-outline btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                </center>`;
                return action;
              }
          }
      ],
    });
    if (redraw) {
        table_kiln_feed.clear();
        table_kiln_feed.rows.add(data);
        table_kiln_feed.draw();
    };
  }

  function clinker() {
    $.ajax({
      type: "GET",
      url: "{{ url('/'.$route . '/datatable-ck') }}",
      data: {
        'plant_id': $('#filter-plant-ck').val(),
        'tanggal': $('#date-ck').val()
      },
      dataType: 'JSON',
      beforeSend: function() {
        $('#dt_clinker').LoadingOverlay('show');
      },
      success: function(data){
          dataClinker = data.data
          tableClinker(dataClinker, true)
      }
    }).always(function(){
      $('#dt_clinker').LoadingOverlay('hide');
    });
  }
  function tableClinker(data, redraw= false) {
    var table_clinker = $('#dt_clinker').DataTable({
      processing: true,
      ordering: true,
      responsive: false,
      scrollX: true,
      filter:false,
      data: data,
      columns: [{
              data: 'nm_plant',
              name: 'nm_plant',
              class: 'text-center',
          },{
              data: 'jam',
              name: 'jam',
              class: 'text-center',
          },
          {
              data: 'lsf',
              name: 'lsf',
              class: 'text-center',
          },
          {
              data: 'sim',
              name: 'sim',
              class: 'text-center',
          },
          {
              data: 'alm',
              name: 'alm',
              class: 'text-center',
          },
          {
              data: 'c3s',
              name: 'c3s',
              class: 'text-center',
          },
          {
              data: 'fcao',
              name: 'fcao',
              class: 'text-center',
          },
          {
              data: 'so3',
              name: 'so3',
              class: 'text-center',
          },
          {
              data: 'uuid',
              name: 'action',
              width: 150,
              render: function(data, type, full, meta) {
                let action =
                `<center>
                <button onClick="showCK('${data}')" class="btn btn-outline btn-primary btn-sm"><i class="fa fa-edit"></i></button>&nbsp;
                <button onClick="deleteCK('${data}')" class="btn btn-outline btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                </center>`;
                return action;
              }
          }
      ],
    });
    if (redraw) {
        table_clinker.clear();
        table_clinker.rows.add(data);
        table_clinker.draw();
    };
  }

  function cement() {
    $.ajax({
      type: "GET",
      url: "{{ url('/'.$route . '/datatable-se') }}",
      data: {
        'plant_id': $('#filter-plant-se').val(),
        'tanggal': $('#date-se').val()
      },
      dataType: 'JSON',
      beforeSend: function() {
        $('#dt_cement').LoadingOverlay('show');
      },
      success: function(data){
          dataCement = data.data
          tableCement(dataCement, true)
      }
    }).always(function(){
      $('#dt_cement').LoadingOverlay('hide');
    });
  }
  function tableCement(data, redraw= false) {
    var table_cement = $('#dt_cement').DataTable({
      processing: true,
      ordering: true,
      responsive: false,
      scrollX: true,
      filter:false,
      data: data,
      columns: [{
              data: 'nm_plant',
              name: 'nm_plant',
              class: 'text-center',
          },{
              data: 'jam',
              name: 'jam',
              class: 'text-center',
          },
          {
              data: 'so3',
              name: 'so3',
              class: 'text-center',
          },
          {
              data: 'fcao',
              name: 'fcao',
              class: 'text-center',
          },
          {
              data: 'blaine',
              name: 'blaine',
              class: 'text-center',
          },
          {
              data: 'res45',
              name: 'res45',
              class: 'text-center',
          },
          {
              data: 'loi',
              name: 'loi',
              class: 'text-center',
          },
          {
              data: 'uuid',
              name: 'action',
              width: 150,
              render: function(data, type, full, meta) {
                let action =
                `<center>
                <button onClick="showSE('${data}')" class="btn btn-outline btn-primary btn-sm"><i class="fa fa-edit"></i></button>&nbsp;
                <button onClick="deleteSE('${data}')" class="btn btn-outline btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                </center>`;
                return action;
              }
          }
      ],
    });
    if (redraw) {
        table_cement.clear();
        table_cement.rows.add(data);
        table_cement.draw();
    };
  }

  function coalMill() {
    $.ajax({
      type: "GET",
      url: "{{ url('/'.$route . '/datatable-cm') }}",
      data: {
        'tanggal': $('#date-cm').val()
      },
      dataType: 'JSON',
      beforeSend: function() {
        $('#dt_coal_mill').LoadingOverlay('show');
      },
      success: function(data){
          dataCoalMill = data.data
          tableCoalMill(dataCoalMill, true)
      }
    }).always(function(){
      $('#dt_coal_mill').LoadingOverlay('hide');
    });
  }
  function tableCoalMill(data, redraw= false) {
    var table_coal_mill = $('#dt_coal_mill').DataTable({
      processing: true,
      ordering: true,
      responsive: false,
      scrollX: true,
      filter:false,
      data: data,
      columns: [
          {
              data: 'quality',
              name: 'quality',
              class: 'text-center',
          },
          {
              data: 'tanggal',
              name: 'tanggal',
              class: 'text-center',
          },
          {
              data: 'ghv',
              name: 'ghv',
              class: 'text-center',
          },
          {
              data: 'ac',
              name: 'ac',
              class: 'text-center',
          },
          {
              data: 'tm',
              name: 'tm',
              class: 'text-center',
          },
          {
              data: 'uuid',
              name: 'action',
              width: 150,
              render: function(data, type, full, meta) {
                let action =
                `<center>
                <button onClick="showCM('${data}')" class="btn btn-outline btn-primary btn-sm"><i class="fa fa-edit"></i></button>&nbsp;
                <button onClick="deleteCM('${data}')" class="btn btn-outline btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                </center>`;
                return action;
              }
          }
      ],
    });
    if (redraw) {
        table_coal_mill.clear();
        table_coal_mill.rows.add(data);
        table_coal_mill.draw();
    };
  }

  function hotMill() {
    $.ajax({
      type: "GET",
      url: "{{ url('/'.$route . '/datatable-hm') }}",
      data: {
        'tanggal': $('#date-hm').val()
      },
      dataType: 'JSON',
      beforeSend: function() {
        $('#dt_hot_mill').LoadingOverlay('show');
      },
      success: function(data){
          dataHotMill = data.data
          tableHotMill(dataHotMill, true)
      }
    }).always(function(){
      $('#dt_hot_mill').LoadingOverlay('hide');
    });
  }
  function tableHotMill(data, redraw= false) {
    var table_hot_mill = $('#dt_hot_mill').DataTable({
      processing: true,
      ordering: true,
      responsive: false,
      scrollX: true,
      filter:false,
      data: data,
      columns: [
          {
              data: 'quality',
              name: 'quality',
              class: 'text-center',
          },
          {
              data: 'tanggal',
              name: 'tanggal',
              class: 'text-center',
          },
          {
              data: 'so3_ilc',
              name: 'so3_ilc',
              class: 'text-center',
          },
          {
              data: 'so3_slc',
              name: 'so3_slc',
              class: 'text-center',
          },
          {
              data: 'derajat_kalsinasi_ilc',
              name: 'derajat_kalsinasi_ilc',
              class: 'text-center',
          },
          {
              data: 'derajat_kalsinasi_slc',
              name: 'derajat_kalsinasi_slc',
              class: 'text-center',
          },
          {
              data: 'uuid',
              name: 'action',
              width: 150,
              render: function(data, type, full, meta) {
                let action =
                `<center>
                <button onClick="showHM('${data}')" class="btn btn-outline btn-primary btn-sm"><i class="fa fa-edit"></i></button>&nbsp;
                <button onClick="deleteHM('${data}')" class="btn btn-outline btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                </center>`;
                return action;
              }
          }
      ],
    });
    if (redraw) {
        table_hot_mill.clear();
        table_hot_mill.rows.add(data);
        table_hot_mill.draw();
    };
  }

  // event button apply
  $('#apply-rm').click(function (e) {
    e.preventDefault();
    rawMill();
  })

  $('#apply-kf').click(function (e) {
    e.preventDefault();
    kilnFeed();
  })

  $('#apply-ck').click(function (e) {
    e.preventDefault();
    clinker();
  })

  $('#apply-se').click(function (e) {
    e.preventDefault();
    cement();
  })

  $('#apply-cm').click(function (e) {
    e.preventDefault();
    coalMill();
  })

  $('#apply-hm').click(function (e) {
    e.preventDefault();
    hotMill();
  })

  // event button import (open modal)
  $('#import-rm').click(function (e) {
    e.preventDefault();
    var url = "{{ url('/'.$route . '/modal-import-rm') }}";
    $.get(url, function (html) {
        $('#modal-rm .modal-content').html(html);
        $('#modal-rm').modal('show');
    }).fail(function () {
      Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Something went wrong!',
      })
    });
  })

  $('#import-kf').click(function (e) {
    e.preventDefault();
    var url = "{{ url('/'.$route . '/modal-import-kf') }}";
    $.get(url, function (html) {
        $('#modal-kf .modal-content').html(html);
        $('#modal-kf').modal('show');
    }).fail(function () {
      Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Something went wrong!',
      })
    });
  })

  $('#import-ck').click(function (e) {
    e.preventDefault();
    var url = "{{ url('/'.$route . '/modal-import-ck') }}";
    $.get(url, function (html) {
        $('#modal-ck .modal-content').html(html);
        $('#modal-ck').modal('show');
    }).fail(function () {
      Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Something went wrong!',
      })
    });
  })

  $('#import-se').click(function (e) {
    e.preventDefault();
    var url = "{{ url('/'.$route . '/modal-import-se') }}";
    $.get(url, function (html) {
        $('#modal-se .modal-content').html(html);
        $('#modal-se').modal('show');
    }).fail(function () {
      Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Something went wrong!',
      })
    });
  })

  $('#import-cm').click(function (e) {
    e.preventDefault();
    var url = "{{ url('/'.$route . '/modal-import-cm') }}";
    $.get(url, function (html) {
        $('#modal-cm .modal-content').html(html);
        $('#modal-cm').modal('show');
    }).fail(function () {
      Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Something went wrong!',
      })
    });
  })

  $('#import-hm').click(function (e) {
    e.preventDefault();
    var url = "{{ url('/'.$route . '/modal-import-hm') }}";
    $.get(url, function (html) {
        $('#modal-hm .modal-content').html(html);
        $('#modal-hm').modal('show');
    }).fail(function () {
      Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Something went wrong!',
      })
    });
  })

  // event button edit
  function showRM(id) {
    event.preventDefault();
    var url = "{{ url('/'.$route . '/edit-rm'.'/:id') }}";
    url = url.replace(':id', id);
    $.get(url, function(html) {
        $('#modal-update-rm .modal-content').html(html);
        $('#modal-update-rm').modal('show');
    }).fail(function(xhr) {
        var {
            message
        } = xhr.responseJSON;
        Swal.fire({
            icon: 'error',
            title: message,
            showConfirmButton: false,
            timer: 3000
        })
    });
  }

  function showKF(id) {
    event.preventDefault();
    var url = "{{ url('/'.$route . '/edit-kf'.'/:id') }}";
    url = url.replace(':id', id);
    $.get(url, function(html) {
        $('#modal-update-kf .modal-content').html(html);
        $('#modal-update-kf').modal('show');
    }).fail(function(xhr) {
        var {
            message
        } = xhr.responseJSON;
        Swal.fire({
            icon: 'error',
            title: message,
            showConfirmButton: false,
            timer: 3000
        })
    });
  }

  function showCK(id) {
    event.preventDefault();
    var url = "{{ url('/'.$route . '/edit-ck'.'/:id') }}";
    url = url.replace(':id', id);
    $.get(url, function(html) {
        $('#modal-update-ck .modal-content').html(html);
        $('#modal-update-ck').modal('show');
    }).fail(function(xhr) {
        var {
            message
        } = xhr.responseJSON;
        Swal.fire({
            icon: 'error',
            title: message,
            showConfirmButton: false,
            timer: 3000
        })
    });
  }

  function showSE(id) {
    event.preventDefault();
    var url = "{{ url('/'.$route . '/edit-se'.'/:id') }}";
    url = url.replace(':id', id);
    $.get(url, function(html) {
        $('#modal-update-se .modal-content').html(html);
        $('#modal-update-se').modal('show');
    }).fail(function(xhr) {
        var {
            message
        } = xhr.responseJSON;
        Swal.fire({
            icon: 'error',
            title: message,
            showConfirmButton: false,
            timer: 3000
        })
    });
  }

  function showCM(id) {
    event.preventDefault();
    var url = "{{ url('/'.$route . '/edit-cm'.'/:id') }}";
    url = url.replace(':id', id);
    $.get(url, function(html) {
        $('#modal-update-cm .modal-content').html(html);
        $('#modal-update-cm').modal('show');
    }).fail(function(xhr) {
        var {
            message
        } = xhr.responseJSON;
        Swal.fire({
            icon: 'error',
            title: message,
            showConfirmButton: false,
            timer: 3000
        })
    });
  }

  function showHM(id) {
    event.preventDefault();
    var url = "{{ url('/'.$route . '/edit-hm'.'/:id') }}";
    url = url.replace(':id', id);
    $.get(url, function(html) {
        $('#modal-update-hm .modal-content').html(html);
        $('#modal-update-hm').modal('show');
    }).fail(function(xhr) {
        var {
            message
        } = xhr.responseJSON;
        Swal.fire({
            icon: 'error',
            title: message,
            showConfirmButton: false,
            timer: 3000
        })
    });
  }

  // event button delete
  function deleteRM(id){
    event.preventDefault();
    var url = "{{ url('/'.$route . '/delete-rm'.'/:id') }}";
    url = url.replace(':id', id);
    Swal.fire({
        icon: 'warning',
        title: 'Apakah anda yakin?',
        text: "Data ini akan di hapus!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ya',
        cancelButtonText: 'Tidak',
        confirmButtonClass: 'btn btn-primary margin-right-10',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then((result) => {
      if (result.isConfirmed) {
        $.post(url, {
            _token: "{{ csrf_token() }}",
            _method: 'DELETE'
        }, function(res) {
          if (res.status == 'success') {
            Swal.fire(
                'Success',
                res.message,
                'success'
            );
            rawMill();
          } else {
            Swal.fire(
                'Gagal',
                res.message,
                'error'
            );
          }
        }, 'json');
      }
    });
  }

  function deleteKF(id){
    event.preventDefault();
    var url = "{{ url('/'.$route . '/delete-kf'.'/:id') }}";
    url = url.replace(':id', id);
    Swal.fire({
        icon: 'warning',
        title: 'Apakah anda yakin?',
        text: "Data ini akan di hapus!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ya',
        cancelButtonText: 'Tidak',
        confirmButtonClass: 'btn btn-primary margin-right-10',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
      }).then((result) => {
        if (result.isConfirmed) {
          $.post(url, {
              _token: "{{ csrf_token() }}",
              _method: 'DELETE'
          }, function(res) {
            if (res.status == 'success') {
              Swal.fire(
                  'Success',
                  res.message,
                  'success'
              );
              kilnFeed();
            } else {
              Swal.fire(
                  'Gagal',
                  res.message,
                  'error'
              );
            }
          }, 'json');
        }
      });
  }
  
  function deleteCK(id){
    event.preventDefault();
    var url = "{{ url('/'.$route . '/delete-ck'.'/:id') }}";
    url = url.replace(':id', id);
    Swal.fire({
        icon: 'warning',
        title: 'Apakah anda yakin?',
        text: "Data ini akan di hapus!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ya',
        cancelButtonText: 'Tidak',
        confirmButtonClass: 'btn btn-primary margin-right-10',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
      }).then((result) => {
        if (result.isConfirmed) {
          $.post(url, {
              _token: "{{ csrf_token() }}",
              _method: 'DELETE'
          }, function(res) {
            if (res.status == 'success') {
              Swal.fire(
                  'Success',
                  res.message,
                  'success'
              );
              clinker();
            } else {
              Swal.fire(
                  'Gagal',
                  res.message,
                  'error'
              );
            }
          }, 'json');
        }
      });
  }

  function deleteCM(id){
    event.preventDefault();
    var url = "{{ url('/'.$route . '/delete-cm'.'/:id') }}";
    url = url.replace(':id', id);
    Swal.fire({
        icon: 'warning',
        title: 'Apakah anda yakin?',
        text: "Data ini akan di hapus!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ya',
        cancelButtonText: 'Tidak',
        confirmButtonClass: 'btn btn-primary margin-right-10',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
      }).then((result) => {
        if (result.isConfirmed) {
          $.post(url, {
              _token: "{{ csrf_token() }}",
              _method: 'DELETE'
          }, function(res) {
            if (res.status == 'success') {
              Swal.fire(
                  'Success',
                  res.message,
                  'success'
              );
              cement();
            } else {
              Swal.fire(
                  'Gagal',
                  res.message,
                  'error'
              );
            }
          }, 'json');
        }
      });
  }

  function deleteCM(id){
    event.preventDefault();
    var url = "{{ url('/'.$route . '/delete-cm'.'/:id') }}";
    url = url.replace(':id', id);
    Swal.fire({
        icon: 'warning',
        title: 'Apakah anda yakin?',
        text: "Data ini akan di hapus!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ya',
        cancelButtonText: 'Tidak',
        confirmButtonClass: 'btn btn-primary margin-right-10',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
      }).then((result) => {
        if (result.isConfirmed) {
          $.post(url, {
              _token: "{{ csrf_token() }}",
              _method: 'DELETE'
          }, function(res) {
            if (res.status == 'success') {
              Swal.fire(
                  'Success',
                  res.message,
                  'success'
              );
              coalMill();
            } else {
              Swal.fire(
                  'Gagal',
                  res.message,
                  'error'
              );
            }
          }, 'json');
        }
      });
  }

  function deleteHM(id){
    event.preventDefault();
    var url = "{{ url('/'.$route . '/delete-hm'.'/:id') }}";
    url = url.replace(':id', id);
    Swal.fire({
        icon: 'warning',
        title: 'Apakah anda yakin?',
        text: "Data ini akan di hapus!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ya',
        cancelButtonText: 'Tidak',
        confirmButtonClass: 'btn btn-primary margin-right-10',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
      }).then((result) => {
        if (result.isConfirmed) {
          $.post(url, {
              _token: "{{ csrf_token() }}",
              _method: 'DELETE'
          }, function(res) {
            if (res.status == 'success') {
              Swal.fire(
                  'Success',
                  res.message,
                  'success'
              );
              hotMill();
            } else {
              Swal.fire(
                  'Gagal',
                  res.message,
                  'error'
              );
            }
          }, 'json');
        }
      });
  }

  // event button download (export data)
  $('#download-rm').click(function(e){
    e.preventDefault();
    $.ajax({
      xhrFields: {
          responseType: 'blob',
      },
      url: "{{ $urlExportRM }}",
      type: 'GET',
      data: {
        tanggal: $('#date-rm').val(),
        plant_id: $('#filter-plant-rm').val()
      },
      beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', '{{ $token }}');
      },
      success: function(result, status, xhr) {
        var disposition = xhr.getResponseHeader('content-disposition');
        var filename = ('Data Quality Management Raw Mill.xlsx');

        var blob = new Blob([result], {
            type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        });
        var link = document.createElement('a');
        link.href = window.URL.createObjectURL(blob);
        link.download = filename;

        document.body.appendChild(link);

        link.click();
        document.body.removeChild(link);
      }
    })
  })

  $('#download-kf').click(function(e){
    e.preventDefault();
    $.ajax({
      xhrFields: {
          responseType: 'blob',
      },
      url: "{{ $urlExportKF }}",
      type: 'GET',
      data: {
        tanggal: $('#date-kf').val(),
        plant_id: $('#filter-plant-kf').val()
      },
      beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', '{{ $token }}');
      },
      success: function(result, status, xhr) {
        var disposition = xhr.getResponseHeader('content-disposition');
        var filename = ('Data Quality Management Kiln Feed.xlsx');

        var blob = new Blob([result], {
            type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        });
        var link = document.createElement('a');
        link.href = window.URL.createObjectURL(blob);
        link.download = filename;

        document.body.appendChild(link);

        link.click();
        document.body.removeChild(link);
      }
    })
  })

  $('#download-ck').click(function(e){
    e.preventDefault();
    $.ajax({
      xhrFields: {
          responseType: 'blob',
      },
      url: "{{ $urlExportCK }}",
      type: 'GET',
      data: {
        tanggal: $('#date-ck').val(),
        plant_id: $('#filter-plant-ck').val()
      },
      beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', '{{ $token }}');
      },
      success: function(result, status, xhr) {
        var disposition = xhr.getResponseHeader('content-disposition');
        var filename = ('Data Quality Management Clinker.xlsx');

        var blob = new Blob([result], {
            type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        });
        var link = document.createElement('a');
        link.href = window.URL.createObjectURL(blob);
        link.download = filename;

        document.body.appendChild(link);

        link.click();
        document.body.removeChild(link);
      }
    })
  })

  $('#download-se').click(function(e){
    e.preventDefault();
    $.ajax({
      xhrFields: {
          responseType: 'blob',
      },
      url: "{{ $urlExportSE }}",
      type: 'GET',
      data: {
        tanggal: $('#date-se').val(),
        plant_id: $('#filter-plant-se').val()
      },
      beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', '{{ $token }}');
      },
      success: function(result, status, xhr) {
        var disposition = xhr.getResponseHeader('content-disposition');
        var filename = ('Data Quality Management Semen.xlsx');

        var blob = new Blob([result], {
            type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        });
        var link = document.createElement('a');
        link.href = window.URL.createObjectURL(blob);
        link.download = filename;

        document.body.appendChild(link);

        link.click();
        document.body.removeChild(link);
      }
    })
  })

  $('#download-cm').click(function(e){
    e.preventDefault();
    $.ajax({
      xhrFields: {
          responseType: 'blob',
      },
      url: "{{ $urlExportCM }}",
      type: 'GET',
      data: {
        tanggal: $('#date-cm').val(),
      },
      beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', '{{ $token }}');
      },
      success: function(result, status, xhr) {
        var disposition = xhr.getResponseHeader('content-disposition');
        var filename = ('Data Quality Management Coal Mill.xlsx');

        var blob = new Blob([result], {
            type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        });
        var link = document.createElement('a');
        link.href = window.URL.createObjectURL(blob);
        link.download = filename;

        document.body.appendChild(link);

        link.click();
        document.body.removeChild(link);
      }
    })
  })

  $('#download-hm').click(function(e){
    e.preventDefault();
    $.ajax({
      xhrFields: {
          responseType: 'blob',
      },
      url: "{{ $urlExportHM }}",
      type: 'GET',
      data: {
        tanggal: $('#date-hm').val(),
        plant_id: $('#filter-plant-hm').val()
      },
      beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', '{{ $token }}');
      },
      success: function(result, status, xhr) {
        var disposition = xhr.getResponseHeader('content-disposition');
        var filename = ('Data Quality Management Hot Mill.xlsx');

        var blob = new Blob([result], {
            type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        });
        var link = document.createElement('a');
        link.href = window.URL.createObjectURL(blob);
        link.download = filename;

        document.body.appendChild(link);

        link.click();
        document.body.removeChild(link);
      }
    })
  })


</script>
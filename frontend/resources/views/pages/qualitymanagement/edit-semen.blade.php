<div class="modal-header pb-0 border-0 justify-content-end">
    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
        <span class="svg-icon svg-icon-1">
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1"
                    transform="rotate(-45 6 17.3137)" fill="currentColor" />
                <rect x="7.41422" y="6" width="16" height="2" rx="1"
                    transform="rotate(45 7.41422 6)" fill="currentColor" />
            </svg>
        </span>
    </div>
</div>
<div class="modal-body scroll-y px-10 px-lg-15 pt-0 pb-15">
    <div class="mb-13 text-center">
        <h1 class="mb-3">Edit Data Semen</h1>
    </div>
    <form action="{{ url('/'.$route . '/edit-se'. '/' .$data->uuid) }}" enctype="multipart/form-data" id="form-edit-se">
        <div class="modal-body">
            @csrf
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">Tanggal</span>
                </label>
                <input type="text" class="form-control form-control-solid" name="tanggal" id="tanggal"
                    value="{{ $data->tanggal }}" disabled/>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">Plant</span>
                </label>
                <select id="plant_id" class="form-control form-control-solid" name="plant_id" disabled>
                    <option value="{{ $data->plant_id }}">{{ $data->nm_plant }}</option>
                </select>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">Type</span>
                </label>
                <select id="type" class="form-control form-control-solid" name="type" disabled>
                    <option value="{{ $data->type }}">{{ $data->type }}</option>
                </select>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">Quality</span>
                </label>
                <select id="quality" class="form-control form-control-solid" name="quality" disabled>
                    <option value="{{ $data->quality }}">{{ $data->quality }}</option>
                </select>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">JAM</span>
                </label>
                <input type="text" class="form-control form-control-solid" name="jam" id="jam"
                    value="{{ $data->jam }}" disabled/>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">SO3</span>
                </label>
                <input type="text" class="form-control form-control-solid" name="so3" id="so3"
                    value="{{ $data->so3 }}" />
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">FCaO</span>
                </label>
                <input type="text" class="form-control form-control-solid" name="fcao" id="fcao"
                    value="{{ $data->fcao }}" />
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">Blaine</span>
                </label>
                <input type="text" class="form-control form-control-solid" name="blaine" id="blaine"
                    value="{{ $data->blaine }}" />
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">Res45</span>
                </label>
                <input type="text" class="form-control form-control-solid" name="res45" id="res45"
                    value="{{ $data->res45 }}" />
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">LOI</span>
                </label>
                <input type="text" class="form-control form-control-solid" name="loi" id="loi"
                    value="{{ $data->loi }}" />
            </div>
        </div>
        <div class="modal-footer">
            <div class="text-center">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary" id="btn-submit">
                    <span class="indicator-label"></i> Submit</span>
                </button>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    $("#form-edit-se").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.
        var form = $(this);
        var actionUrl = form.attr('action');
        $.ajax({
            type: "PUT",
            url: actionUrl,
            data: {
                'plant_id': $('#plant_id').val(),
                'tanggal': $('#tanggal').val(),
                'type': $('#type').val(),
                'quality': $('#quality').val(),
                'jam': $('#jam').val(),
                'so3': $('#so3').val(),
                'fcao': $('#fcao').val(),
                'blaine': $('#blaine').val(),
                'res45': $('#res45').val(),
                'loi': $('#loi').val(),
            },
            success: function(data)
            {
                if(data.status == 'success'){
                    Swal.fire(
                        'Success',
                        data.message,
                        'success'
                    );
                    $('#modal-update-se').modal('hide');
                    cement();
                }else{
                    Swal.fire(
                        'Warning',
                        data.message,
                        'warning'
                    );
                }
            }
        });
    });
</script>

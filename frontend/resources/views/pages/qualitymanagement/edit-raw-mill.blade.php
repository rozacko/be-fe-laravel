<div class="modal-header pb-0 border-0 justify-content-end">
    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
        <span class="svg-icon svg-icon-1">
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1"
                    transform="rotate(-45 6 17.3137)" fill="currentColor" />
                <rect x="7.41422" y="6" width="16" height="2" rx="1"
                    transform="rotate(45 7.41422 6)" fill="currentColor" />
            </svg>
        </span>
    </div>
</div>
<div class="modal-body scroll-y px-10 px-lg-15 pt-0 pb-15">
    <div class="mb-13 text-center">
        <h1 class="mb-3">Edit Data Raw Mill</h1>
    </div>
    <form action="{{ url('/'.$route . '/edit-rm'. '/' .$data->uuid) }}" enctype="multipart/form-data" id="form-edit-rm">
        <div class="modal-body">
            @csrf
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">Tanggal</span>
                </label>
                <input type="text" class="form-control form-control-solid" name="tanggal" id="tanggal"
                    value="{{ $data->tanggal }}" disabled/>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">Plant</span>
                </label>
                <select id="plant_id" class="form-control form-control-solid" name="plant_id" disabled>
                    <option value="{{ $data->plant_id }}">{{ $data->nm_plant }}</option>
                </select>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">JAM</span>
                </label>
                <input type="text" class="form-control form-control-solid" name="jam" id="jam"
                    value="{{ $data->jam }}" disabled/>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">LSF</span>
                </label>
                <input type="text" class="form-control form-control-solid" name="lsf" id="lsf"
                    value="{{ $data->lsf }}" />
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">SIM</span>
                </label>
                <input type="text" class="form-control form-control-solid" name="sim" id="sim"
                    value="{{ $data->sim }}" />
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">ALM</span>
                </label>
                <input type="text" class="form-control form-control-solid" name="alm" id="alm"
                    value="{{ $data->alm }}" />
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">H20</span>
                </label>
                <input type="text" class="form-control form-control-solid" name="h2o" id="h2o"
                    value="{{ $data->h2o }}" />
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">Res90</span>
                </label>
                <input type="text" class="form-control form-control-solid" name="res90" id="res90"
                    value="{{ $data->res90 }}" />
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">Res200</span>
                </label>
                <input type="text" class="form-control form-control-solid" name="res200" id="res200"
                    value="{{ $data->res200 }}" />
            </div>
        </div>
        <div class="modal-footer">
            <div class="text-center">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary" id="btn-submit">
                    <span class="indicator-label"></i> Submit</span>
                </button>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    $("#form-edit-rm").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.
        var form = $(this);
        var actionUrl = form.attr('action');
        $.ajax({
            type: "PUT",
            url: actionUrl,
            data: {
                'plant_id': $('#plant_id').val(),
                'tanggal': $('#tanggal').val(),
                'jam': $('#jam').val(),
                'lsf': $('#lsf').val(),
                'sim': $('#sim').val(),
                'alm': $('#alm').val(),
                'h2o': $('#h2o').val(),
                'res90': $('#res90').val(),
                'res200': $('#res200').val(),
            },
            success: function(data)
            {
                if(data.status == 'success'){
                    Swal.fire(
                        'Success',
                        data.message,
                        'success'
                    );
                    $('#modal-update-rm').modal('hide');
                    rawMill();
                }else{
                    Swal.fire(
                        'Warning',
                        data.message,
                        'warning'
                    );
                }
            }
        });
    });
</script>
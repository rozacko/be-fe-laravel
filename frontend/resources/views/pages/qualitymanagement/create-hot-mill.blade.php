<div class="modal-header pb-0 border-0 justify-content-end">
    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
        <span class="svg-icon svg-icon-1">
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="currentColor" />
                <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="currentColor" />
            </svg>
        </span>
    </div>
</div>
<div class="modal-body scroll-y px-10 px-lg-15 pt-0 pb-15">
    <div class="text-center">
        <h1>Import Data Hot Mill</h1>
    </div>
    <form method="POST" action="{{ url('/'.$route . '/store-hm') }}" enctype="multipart/form-data" id="form-hm">
        <div class="modal-body">
            @csrf
            <div class="d-flex flex-column mb-5 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    Quality
                </label>
                <select class="form-select h-25" data-control="select2" data-placeholder="Pilih Quality" id="quality-modal-hm">
                    <option value="klin-tuban-1">klin-tuban-1</option>
                    <option value="klin-tuban-2">klin-tuban-2</option>
                    <option value="klin-tuban-3">klin-tuban-3</option>
                    <option value="klin-tuban-4">klin-tuban-4</option>
                </select>
            </div>
            <div class="d-flex flex-column mb-5 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    Template
                </label>
                <a href="{{ url('/'.$route.'/temp-hm') }}" class="button form-control h-25">
                    Download Template
                </a>
            </div>
            <div class="d-flex flex-column mb-5 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    Upload File
                </label>
                <input type="file" class="form-control h-25 d-flex" id="upload_file_hm">
                <p class="mt-2" style="color: #1F7793;">*file yang di upload adalah file berdasarkan template yang disediakan</p>
            </div>
        </div>
        <div class="modal-footer">
            <div class="text-center">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                <button id="btn_submit" type="submit" class="btn btn-primary">
                    <span class="indicator-label">Submit</span>
                </button>
            </div>
        </div>
    </form>
</div>

<script>
    $(document).ready(function () {
    })

    $("#form-hm").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.
        var form = $(this);
        var formData = new FormData();
        var actionUrl = form.attr('action');
        formData.append('quality', $('#quality-modal-hm').val());
        formData.append('upload_file', document.getElementById('upload_file_hm').files[0]);
        $.ajax({
            type: "POST",
            url: "{{ url('/'.$route . '/store-hm') }}",
            data: formData,
            dataType: 'JSON',
            contentType: false,
            processData: false,
            success: function(data)
            {
                if(data.status == 'success'){
                    Swal.fire(
                        'Success',
                        data.message,
                        'success'
                    );
                    $('#modal-hm').modal('hide');
                    hotMill();
                }else{
                    Swal.fire(
                        'Warning',
                        data.message,
                        'warning'
                    );
                }
            }
        });
    });
</script>

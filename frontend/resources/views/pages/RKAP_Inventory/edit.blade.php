<div class="modal-header pb-0 border-0 justify-content-end">
    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
        <span class="svg-icon svg-icon-1">
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1"
                    transform="rotate(-45 6 17.3137)" fill="currentColor" />
                <rect x="7.41422" y="6" width="16" height="2" rx="1"
                    transform="rotate(45 7.41422 6)" fill="currentColor" />
            </svg>
        </span>
    </div>
</div>
<div class="modal-body scroll-y px-10 px-lg-15 pt-0 pb-15">
    <div class="mb-13 text-center">
        <h1 class="mb-3">Edit Data RKAP Inventory</h1>
    </div>
    <form action="{{ url('/'.$route . '/edit'. '/' .$data->uuid) }}" enctype="multipart/form-data" id="form-edit">
        <div class="modal-body">
            @csrf
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">Parameter</span>
                </label>
                <select id="parameters" class="form-control form-control-solid" name="parameters" >
                    <option value="biaya-pemeliharaan" selected>biaya-pemeliharaan</option>
                </select>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">Tahun</span>
                </label>
                <select id="tahun" class="form-control form-control-solid" name="tahun" >
                    @foreach(getYear() as $key => $value)
                        @if ($value == $data->tahun )
                            <option value="{{$value}}" selected>{{$value}}</option>
                        @else
                            <option value="{{$value}}">{{$value}}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">Bulan</span>
                </label>
                <select id="bulan" class="form-control form-control-solid" name="bulan" >
                    @foreach(getBulan() as $key => $value)
                        @if ($value['month'] == $data->bulan )
                            <option value="{{$value['month']}}" selected>{{$value['month_name']}}</option>
                        @else
                            <option value="{{$value['month']}}">{{$value['month_name']}}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">RKAP INDEX INVENTORY</span>
                </label>
                <input type="number" class="form-control form-control-solid" name="value" id="value"
                    value="{{ $data->value }}" />
            </div>
        </div>
        <div class="modal-footer">
            <div class="text-center">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary" id="btn-submit">
                    <span class="indicator-label"></i> Submit</span>
                </button>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    $("#form-edit").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.
        var form = $(this);
        var actionUrl = form.attr('action');
        $.ajax({
            type: "PUT",
            url: actionUrl,
            data: {
                'parameters': $('#parameters').val(),
                'tahun': $('#tahun').val(),
                'bulan': $('#bulan').val(),
                'value': $('#value').val(),
            },
            success: function(data)
            {
                if(data.status == 'success'){
                    Swal.fire(
                        'Success',
                        data.message,
                        'success'
                    );
                    $('#modal-update').modal('hide');
                    table.draw();
                }else{
                    Swal.fire(
                        'Warning',
                        data.message,
                        'warning'
                    );
                }
            }
        });
    });
</script>

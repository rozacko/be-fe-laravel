<x-default-layout>
    <div class="flex-stack mb-0 mb-lg-n4 pt-5">
        <div class="row">
            <div class="col-sm-4">
                <div class="information">
                    <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">Vendor
                        Management</h1>
                    <ul class="breadcrumb breadcrumb-separatorless ">
                        <li class="breadcrumb-item text-muted">
                            <a href="/" class="text-muted text-hover-primary">Vendor Management</a>
                        </li>
                        <li class="breadcrumb-item text-muted">/</li>
                        <li class="breadcrumb-item text-ghopo">{{ $title }}</li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-8 d-flex justify-content-md-end flex-no-wrap px-5">
                {{-- <form method="POST" action="{{ route($route . '.downloadExcel') }}"> --}}
                {{-- @csrf --}}
                <div class="row">
                    <div class="col mt-2">
                        <select id="filter_year" class="form-select h-25" data-control="select2"
                            data-placeholder="Pilih Tahun">
                            @foreach (getYear() as $key => $value)
                                @if ($value == date('Y'))
                                    <option value="{{ $value }}" selected>{{ $value }}</option>
                                @else
                                    <option value="{{ $value }}">{{ $value }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="col mt-2">
                        <select id="filter_month" class="form-select h-25" data-control="select2"
                            data-placeholder="Pilih Bulan">
                            @foreach (getMonth() as $key => $value)
                                @if ($value['month'] == date('m'))
                                    <option value="{{ $value['month'] }}" selected>{{ $value['month_name'] }}
                                    </option>
                                @else
                                    <option value="{{ $value['month'] }}">{{ $value['month_name'] }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>

                    <div class="col mt-2">
                        <button class="btn btn-light-primary" id="filter-btn">Tampilkan</button>
                    </div>
                    <div class="col mt-2">
                        <button class="d-flex btn btn-outline-info" id="download-excel">
                            <i class="bi bi-download flex"></i>
                            Download
                        </button>
                    </div>
                    <div class="col mt-2">
                        <button type="button" class="d-flex btn btn-primary btn-add">
                            <i class="bi bi-upload"></i>
                            Import
                        </button>
                    </div>
                </div>
                {{-- </form> --}}
            </div>
        </div>
    </div>

    <div class="card card-flush mt-6 mt-xl-9">
        <div class="card-body mt-6 pt-0">
            <h3>Vendor Management</h3>
            <div class="row d-flex justify-content-end">
                <div class="col-md-2">
                    <input type="text" class="form-control" id="search" placeholder="Search and enter" onchange="searchData()">
                </div>
            </div>
            <table class="table table-bordered data-table" id="tb_vendor">
                <thead>
                    <tr>
                        <th class="align-middle text-center color-header-tabel" rowspan="2">NO</th>
                        <th class="align-middle text-center color-header-tabel" rowspan="2">NO PEGAWAI</th>
                        <th class="align-middle text-center color-header-tabel" rowspan="2">NAMA PEGAWAI</th>
                        <th class="align-middle text-center color-header-tabel" rowspan="2">SPESIFIKASI</th>
                        <th class="align-middle text-center color-header-tabel" rowspan="2">PERUSAHAAN</th>
                        <th class="align-middle text-center color-header-tabel" rowspan="2">TAHUN</th>
                        <th class="align-middle text-center color-header-tabel" rowspan="2">BULAN</th>
                        <th class="align-middle text-center color-header-tabel" colspan="31">TANGGAL</th>
                    </tr>
                    <tr>
                        @for ($i = 0; $i <= 30; $i++)
                            <th class="align-middle text-center color-header-tabel">{{ $i + 1 }}</th>
                        @endfor
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            </table>
        </div>
    </div>

    <div class="modal fade" id="modal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered mw-650px">
            <div class="modal-content rounded"></div>
        </div>
    </div>
</x-default-layout>
<script>
    $('#filter-btn').on('click', function() {
        var bulan = $("#filter_month").val();
        var tahun = $("#filter_year").val();
        var url = '{{ route($route . '.table', [':bulan', ':tahun']) }}';
        url = url.replace(':bulan', bulan);
        url = url.replace(':tahun', tahun);
        table.ajax.url(url).load();
    });

    function searchData() {
        var url = '{{ route($route . '.table') }}';
        table.ajax.url(url).load();
    }

    $("#download-excel").click(function(e) {
        var bulan = $("#filter_month").val();
        var tahun = $("#filter_year").val();
        var actionUrl = "{{ $urlDownload }}";
        tahun = $('#filter_year').val();
        bulan = $('#filter_month').val();
        const d = new Date();
        let day = d.getDay();
        let getMilliseconds = d.getMilliseconds();

        $.ajax({
            url: actionUrl,
            type: 'POST',
            data: {
                "bulan": bulan,
                "tahun": tahun,
            },
            xhrFields: {
                responseType: "blob",
            },
            beforeSend: function(xhr) {
                xhr.setRequestHeader('Authorization', '{{ $token }}');
            },
            success: function(result, status, xhr) {

                var filename =
                    `planning-upload-schedule-${bulan}-${tahun}-${day}-${getMilliseconds}`;

                let a = document.createElement("a")
                a.href = URL.createObjectURL(result)
                a.download = filename;
                a.style.display = "none";
                a.className = "downloadFile";
                document.body.appendChild(a)
                a.click();

                a.remove();
            }
        })
    });

    function getUrlVars() {
        var vars = [],
            hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    }

    var previous = '';
    var bulan = $("#filter_month").val();   
    var tahun = $("#filter_year").val();

    var url = '{{ route($route . '.table') }}';
    url = url.replace(':bulan', bulan);
    url = url.replace(':tahun', tahun);

    var searchTerm = $('input[type=search]').val();
    var table = $('#tb_vendor').DataTable({
        processing: true,
        serverSide: true,
        searching: false,
        ordering: true,
        ajax: {
            "url": url,
            'type': 'POST',
            'dataType': 'JSON',
            'data': function(d) {
                d.bulan = $("#filter_month").val(),
                    d.tahun = $("#filter_year").val(),
                    d.keyword = $("#search").val()
            }
        },
        // data: data,
        paging: false,
        responsive: false,
        scrollX: true,
        columns: [{
                data: 'DT_RowIndex',
                name: 'id_planning_schedule_kerja',
                searchable: true
            },
            {
                data: 'no_pegawai',
                name: 'no_pegawai',
                searchable: true
            },
            {
                data: 'nm_pegawai',
                name: 'nm_pegawai',
                searchable: true
            },
            {
                data: 'spesifikasi',
                name: 'spesifikasi',
                searchable: true
            },
            {
                data: 'perusahaan',
                name: 'perusahaan',
                searchable: true
            },
            {
                data: 'tahun',
                name: 'tahun',
                searchable: true
            },
            {
                data: 'bulan',
                name: 'bulan',
                searchable: true
            },
            {
                data: 'tgl_1',
                name: 'tgl_1',
                className: 'text-center',
                searchable: false
            }, {
                data: 'tgl_2',
                name: 'tgl_2',
                className: 'text-center',
                searchable: false
            }, {
                data: 'tgl_3',
                name: 'tgl_3',
                className: 'text-center',
                searchable: false
            }, {
                data: 'tgl_4',
                name: 'tgl_4',
                className: 'text-center',
                searchable: false
            }, {
                data: 'tgl_5',
                name: '5',
                className: 'text-center',
                searchable: false
            }, {
                data: 'tgl_6',
                name: '6',
                className: 'text-center',
                searchable: false
            }, {
                data: 'tgl_7',
                name: '7',
                className: 'text-center',
                searchable: false
            }, {
                data: 'tgl_8',
                name: '8',
                className: 'text-center',
                searchable: false
            }, {
                data: 'tgl_9',
                name: '9',
                className: 'text-center',
                searchable: false
            }, {
                data: 'tgl_10',
                name: '10',
                className: 'text-center',
                searchable: false
            },
            {
                data: 'tgl_11',
                name: '11',
                className: 'text-center',
                searchable: false
            }, {
                data: 'tgl_12',
                name: '12',
                className: 'text-center',
                searchable: false
            }, {
                data: 'tgl_13',
                name: '13',
                className: 'text-center',
                searchable: false
            }, {
                data: 'tgl_14',
                name: '14',
                className: 'text-center',
                searchable: false
            }, {
                data: 'tgl_15',
                name: '15',
                className: 'text-center',
                searchable: false
            }, {
                data: 'tgl_6',
                name: 'tgl_6',
                className: 'text-center',
                searchable: false
            }, {
                data: 'tgl_17',
                name: 'tgl_17',
                className: 'text-center',
                searchable: false
            }, {
                data: 'tgl_18',
                name: 'tgl_18',
                className: 'text-center',
                searchable: false
            }, {
                data: 'tgl_19',
                name: 'tgl_19',
                className: 'text-center',
                searchable: false
            }, {
                data: 'tgl_20',
                name: 'tgl_20',
                className: 'text-center',
                searchable: false
            },
            {
                data: 'tgl_21',
                name: 'tgl_21',
                className: 'text-center',
                searchable: false
            }, {
                data: 'tgl_22',
                name: 'tgl_22',
                className: 'text-center',
                searchable: false
            }, {
                data: 'tgl_23',
                name: 'tgl_23',
                className: 'text-center',
                searchable: false
            }, {
                data: 'tgl_24',
                name: 'tgl_24',
                className: 'text-center',
                searchable: false
            }, {
                data: 'tgl_25',
                name: 'tgl_25',
                className: 'text-center',
                searchable: false
            }, {
                data: 'tgl_26',
                name: 'tgl_26',
                className: 'text-center',
                searchable: false
            }, {
                data: 'tgl_27',
                name: 'tgl_27',
                className: 'text-center',
                searchable: false
            }, {
                data: 'tgl_28',
                name: 'tgl_28',
                className: 'text-center',
                searchable: false
            }, {
                data: 'tgl_29',
                name: 'tgl_29',
                className: 'text-center',
                searchable: false
            }, {
                data: 'tgl_30',
                name: 'tgl_30',
                className: 'text-center',
                searchable: false
            }, {
                data: 'tgl_31',
                name: 'tgl_31',
                className: 'text-center',
                searchable: false
            },
        ]
    });

    $('.btn-add').click(function(e) {
        e.preventDefault();
        var url = "{{ route($route . '.create') }}";
        $.get(url, function(html) {
            $('#modal .modal-content').html(html);
            $('#modal').modal('show');
        }).fail(function() {
            console.log('Gagal');
        });
    })
</script>

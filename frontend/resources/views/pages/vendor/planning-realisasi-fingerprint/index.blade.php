{{-- Extends layout --}}
<x-default-layout>
    <div class="flex-stack mb-0 mb-lg-n4 pt-5">
        <div class="row">
            <div class="col-sm-4">
                <div class="information">
                    <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">Vendor
                        Management</h1>
                    <ul class="breadcrumb breadcrumb-separatorless ">
                        <li class="breadcrumb-item text-muted">
                            <a href="/" class="text-muted text-hover-primary">Vendor Management</a>
                        </li>
                        <li class="breadcrumb-item text-muted">/</li>
                        <li class="breadcrumb-item text-ghopo">{{ $title }}</li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-8 d-flex justify-content-md-end flex-no-wrap px-5">
                {{-- <form method="POST" action="{{ route($route . '.downloadExcel') }}" id="download-excel"> --}}
                    @csrf
                    <div class="row">
                        <div class="col mt-2">
                            <select id="filter_year" class="form-select h-25" data-control="select2"
                                data-placeholder="Pilih Tahun">
                                @foreach (getYear() as $key => $value)
                                    @if ($value == date('Y'))
                                        <option value="{{ $value }}" selected>{{ $value }}</option>
                                    @else
                                        <option value="{{ $value }}">{{ $value }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col mt-2">
                            <select id="filter_month" class="form-select h-25" data-control="select2"
                                data-placeholder="Pilih Bulan">
                                @foreach (getMonth() as $key => $value)
                                    @if ($value['month'] == date('m'))
                                        <option value="{{ $value['month'] }}" selected>{{ $value['month_name'] }}
                                        </option>
                                    @else
                                        <option value="{{ $value['month'] }}">{{ $value['month_name'] }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>

                        <div class="col mt-2">
                            <button type="button" class="btn btn-light-primary" id="filter-btn">Tampilkan</button>
                        </div>
                        <div class="col mt-2">
                            <button class="d-flex btn btn-outline-info btn-add" id="download-excel">
                                <i class="bi bi-download flex"></i>
                                Download
                            </button>
                        </div>
                    </div>
                {{-- </form> --}}
            </div>
        </div>
    </div>
    <div class="card card-flush mt-6 mt-xl-9">
        <div class="card-body pt-5">
            <h3>DATA PLANNING VS REALISASI ABSENSI FINGER PRINT</h3>
            <table class="table table-bordered data-table" id="tb_finger_print">
                <thead>
                    <tr>
                        <th class="align-middle color-header-tabel">NO</th>
                        <th class="align-middle color-header-tabel">NO PEGAWAI</th>
                        <th class="align-middle color-header-tabel">NAMA PEGAWAI</th>
                        <th class="align-middle color-header-tabel">SPESIFIKASI</th>
                        <th class="align-middle color-header-tabel">PERUSAHAAN</th>
                        <th class="align-middle color-header-tabel">TAHUN</th>
                        <th class="align-middle color-header-tabel">BULAN</th>
                        <th class="align-middle color-header-tabel">PLANNING</th>
                        <th class="align-middle color-header-tabel">REALISASI</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>

        </div>
    </div>

    <div class="modal fade" id="modal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered mw-650px">
            <div class="modal-content rounded"></div>
        </div>
    </div>

</x-default-layout>

<script>
    $('#filter-btn').on('click', function() {
        var bulan = $("#filter_month").val();
        var tahun = $("#filter_year").val();
        var url = '{{ route($route . '.table', [':bulan', ':tahun']) }}';
        url = url.replace(':bulan', bulan);
        url = url.replace(':tahun', tahun);
        table.ajax.url(url).load();
    });

    var bulan = $("#filter_month").val();
    var tahun = $("#filter_year").val();

    var url = '{{ route($route . '.table', [':bulan', ':tahun']) }}';
    url = url.replace(':bulan', bulan);
    url = url.replace(':tahun', tahun);

    var table = $('#tb_finger_print').DataTable({
        processing: true,
        serverSide: true,
        ajax: url,
        columns: [{
                data: 'DT_RowIndex',
                name: 'id_planning_schedule_kerja'
            },
            {
                data: 'no_pegawai',
                name: 'no_pegawai'
            },
            {
                data: 'nm_pegawai',
                name: 'nm_pegawai'
            },
            {
                data: 'spesifikasi',
                name: 'spesifikasi'
            },
            {
                data: 'perusahaan',
                name: 'perusahaan'
            },
            {
                data: 'tahun',
                name: 'tahun'
            },
            {
                data: 'bulan',
                name: 'bulan'
            },
            {
                data: 'planning',
                name: 'planning'
            },
            {
                data: 'realisasi',
                name: 'realisasi'
            },
        ]
    });

    setTimeout(function() {
        if ($('#alertNotif').length > 0) {
            $('#alertNotif').remove();
        }
    }, 5000);
    
    $("#download-excel").click(function(e) {
        var bulan = $("#filter_month").val();
        var tahun = $("#filter_year").val();
        var actionUrl = "{{ $urlDownload }}";
        tahun = $('#filter_year').val();
        bulan = $('#filter_month').val();
        const d = new Date();
        let day = d.getDay();
        let getMilliseconds = d.getMilliseconds();

        $.ajax({
            url: actionUrl,
            type: 'POST',
            data: {
                "tahun": tahun,
                "bulan": bulan,
            },
            xhrFields: {
                responseType: "blob",
            },
            beforeSend: function(xhr) {
                xhr.setRequestHeader('Authorization', '{{ $token }}');
            },
            success: function(result, status, xhr) {

                var filename = `realisasi-${bulan}-${tahun}-${day}-${getMilliseconds}`;

                let a = document.createElement("a")
                a.href = URL.createObjectURL(result)
                a.download = filename;
                a.style.display = "none";
                a.className = "downloadFile";
                document.body.appendChild(a)
                a.click();

                a.remove();
            }
        })
    });
</script>

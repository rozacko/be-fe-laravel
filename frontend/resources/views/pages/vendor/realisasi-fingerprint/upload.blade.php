<div class="modal-header pb-0 border-0 justify-content-end">
    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
        <span class="svg-icon svg-icon-1">
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1"
                    transform="rotate(-45 6 17.3137)" fill="currentColor" />
                <rect x="7.41422" y="6" width="16" height="2" rx="1"
                    transform="rotate(45 7.41422 6)" fill="currentColor" />
            </svg>
        </span>
    </div>
</div>
<div class="modal-body scroll-y px-10 px-lg-15 pt-0 pb-15">
    <div class="mb-13 text-center">
        <h1 class="mb-3">Import Data</h1>
    </div>
    <div class="modal-body">
        <div class="d-flex flex-column mb-8 fv-row form-group">
            <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                Template
                <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Download Template"></i>
            </label>
            <a href="{{ route('realisasi-fingerprint.downloadTemplate', 'actual') }}" target="_blank"
                class="btn btn-secondary">
                <span class="indicator-label">Download Template</span>
            </a>
        </div>
    </div>
    <form method="POST" action="{{ route($route . '.import') }}" enctype="multipart/form-data" id="import-planning">
        <div class="modal-body">
            @csrf
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">Upload File</span>
                </label>
                <input type="file" name="upload_file" id="upload_file" style="display: flex !important;" />
                <p class="mt-2" style="color: #1F7793;">*file yang di upload adalah file berdasarkan template yang
                    disediakan</p>
            </div>
        </div>
        <div class="modal-footer">
            <div class="text-center">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                <button id="btn_submit" type="submit" class="btn btn-primary">
                    <span class="indicator-label">Submit</span>
                    <span class="indicator-progress">Please wait...
                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                </button>
            </div>
        </div>
    </form>
</div>


<script type="text/javascript">
    $(".select_option").select2({
        dropdownParent: $('#modal')
    });

    $("#import-planning").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form. 
        var formData = new FormData(this);
        var form = $(this);
        var actionUrl = form.attr('action');
        $.ajax({
            type: "POST",
            url: actionUrl,
            enctype: 'multipart/form-data',
            data: formData,
            cache:false,
            contentType: false,
            processData: false, 
            success: function(data)
            {
                if(data.status == 'success'){
                    let htmlStatus = `<ul style="list-style: none;">`;
                        htmlStatus += `<li>Total data: ${data.message.total_data}`;
                        htmlStatus += `<li>Total data berhasil: ${data.message.total_success}`;
                        htmlStatus += `<li>Total data gagal: ${data.message.total_failed}`;
                        htmlStatus += `</ul>`;

                    Swal.fire(
                        'Success',
                        htmlStatus,
                        'success',
                    );
                    $('#modal').modal('hide');
                    $('#tb_finger_print').DataTable().ajax.reload();
                }else{
                    Swal.fire(
                        'Warning',
                        data.message,
                        'warning'
                    );
                }
            }
        });
    });
</script>

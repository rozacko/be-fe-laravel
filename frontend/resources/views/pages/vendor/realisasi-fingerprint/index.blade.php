{{-- Extends layout --}}
<x-default-layout>
    <div class="flex-stack mb-0 mb-lg-n4 pt-5">
        <div class="row">
            <div class="col-sm-4">
                <div class="information">
                    <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">Vendor
                        Management</h1>
                    <ul class="breadcrumb breadcrumb-separatorless ">
                        <li class="breadcrumb-item text-muted">
                            <a href="/" class="text-muted text-hover-primary">Vendor Management</a>
                        </li>
                        <li class="breadcrumb-item text-muted">/</li>
                        <li class="breadcrumb-item text-ghopo">{{ $title }}</li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-8 d-flex justify-content-md-end flex-no-wrap px-5">
                {{-- <form method="POST" action="{{ route($route . '.downloadExcel') }}" id="download-excel"> --}}
                <div class="row">
                    <div class="col mt-2">
                        <select id="filter_year" class="form-select h-25" data-control="select2"
                            data-placeholder="Pilih Tahun">
                            @foreach (getYear() as $key => $value)
                                @if ($value == date('Y'))
                                    <option value="{{ $value }}" selected>{{ $value }}</option>
                                @else
                                    <option value="{{ $value }}">{{ $value }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="col mt-2">
                        <select id="filter_month" class="form-select h-25" data-control="select2"
                            data-placeholder="Pilih Bulan">
                            @foreach (getMonth() as $key => $value)
                                @if ($value['month'] == date('m'))
                                    <option value="{{ $value['month'] }}" selected>{{ $value['month_name'] }}
                                    </option>
                                @else
                                    <option value="{{ $value['month'] }}">{{ $value['month_name'] }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>

                    <div class="col mt-2">
                        <button class="btn btn-light-primary" id="filter-btn">Tampilkan</button>
                    </div>
                    <div class="col mt-2">
                        <button class="d-flex btn btn-outline-info" id="download-excel">
                            <i class="bi bi-download flex"></i>
                            Download
                        </button>
                    </div>
                    <div class="col mt-2">
                        <button type="button" class="d-flex btn btn-primary btn-add">
                            <i class="bi bi-upload"></i>
                            Import
                        </button>
                    </div>
                </div>
                {{-- </form> --}}
            </div>
        </div>
    </div>
    <div class="card card-flush mt-6 mt-xl-9">
        <div class="card-header p-3">
            <div class="card-title flex-column">
                <h3 class="fw-bold mb-1">Vendor Management</h3>
            </div>
        </div>
        <div class="card-body pt-0">
            <h3>DATA REALISASI ABSENSI FINGER PRINT</h3>
            <table class="table table-bordered data-table" id="tb_finger_print">
                <thead>
                    <tr>
                        <th class="align-middle color-header-tabel">NO</th>
                        <th class="align-middle color-header-tabel">NO PEGAWAI</th>
                        <th class="align-middle color-header-tabel">NAMA PEGAWAI</th>
                        <th class="align-middle color-header-tabel">TIME</th>
                        <th class="align-middle color-header-tabel">STATE</th>
                        <th class="align-middle color-header-tabel">NEW STATE</th>
                        <th class="align-middle color-header-tabel">EXCEPTION</th>
                        <th class="align-middle color-header-tabel">PREFERENCE</th>
                        <th class="align-middle color-header-tabel">TANGGAL</th>
                        <th class="align-middle color-header-tabel">EVENT</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>

        </div>
    </div>

    <div class="modal fade" id="modal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered mw-650px">
            <div class="modal-content rounded"></div>
        </div>
    </div>

</x-default-layout>

<script>
    $('#filter-btn').on('click', function() {
        var bulan = $("#filter_month").val();
        var tahun = $("#filter_year").val();
        var url = '{{ route($route . '.table', [':bulan', ':tahun']) }}';
        url = url.replace(':bulan', bulan);
        url = url.replace(':tahun', tahun);
        table.ajax.url(url).load();
    });

    var bulan = $("#filter_month").val();
    var tahun = $("#filter_year").val();

    var url = '{{ route($route . '.table', [':bulan', ':tahun']) }}';
    url = url.replace(':bulan', bulan);
    url = url.replace(':tahun', tahun);

    var table = $('#tb_finger_print').DataTable({
        processing: true,
        serverSide: true,
        ajax: url,
        columns: [{
                data: 'DT_RowIndex',
                name: 'id_presensi_pegawai'
            },
            {
                data: 'no_pegawai',
                name: 'no_pegawai'
            },
            {
                data: 'nama_pegawai',
                name: 'nama_pegawai'
            },
            {
                data: 'time',
                name: 'time'
            },
            {
                data: 'state',
                name: 'state'
            },
            {
                data: 'new_state',
                name: 'new_state'
            },
            {
                data: 'exception',
                name: 'exception'
            },
            {
                data: 'preference',
                name: 'preference'
            },
            {
                data: 'tanggal',
                name: 'tanggal'
            },
            {
                data: 'event',
                name: 'event'
            }
        ]
    });

    setTimeout(function() {
        if ($('#alertNotif').length > 0) {
            $('#alertNotif').remove();
        }
    }, 5000);

    $("#download-excel").click(function(e) {
        var bulan = $("#filter_month").val();
        var tahun = $("#filter_year").val();
        var actionUrl = "{{ $urlDownload }}";
        tahun = $('#filter_year').val();
        bulan = $('#filter_month').val();
        const d = new Date();
        let day = d.getDay();
        let getMilliseconds = d.getMilliseconds();

        $.ajax({
            url: actionUrl,
            type: 'POST',
            data: {
                "bulan": bulan,
                "tahun": tahun,
            },
            xhrFields: {
                responseType: "blob",
            },
            beforeSend: function(xhr) {
                xhr.setRequestHeader('Authorization', '{{ $token }}');
            },
            success: function(result, status, xhr) {

                var filename = `realisasi-${bulan}-${tahun}-${day}-${getMilliseconds}`;

                let a = document.createElement("a")
                a.href = URL.createObjectURL(result)
                a.download = filename;
                a.style.display = "none";
                a.className = "downloadFile";
                document.body.appendChild(a)
                a.click();

                a.remove();
            }
        })
    });

    $('.btn-add').click(function(e) {
        e.preventDefault();
        var url = "{{ route($route . '.create') }}";
        $.get(url, function(html) {
            $('#modal .modal-content').html(html);
            $('#modal').modal('show');
        }).fail(function() {
            console.log('Gagal');
        });
    })
</script>

<x-default-layout>
    <div class="flex-stack mb-0 mb-lg-n4 pt-5">
        <div class="row">
            <div class="col-sm-5">
                <div class="information">
                    <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">
                        Overhoul Report</h1>
                    <ul class="breadcrumb breadcrumb-separatorless ">
                        <li class="breadcrumb-item text-muted">
                            <a href="/" class="text-muted text-hover-primary">Overhoul Report</a>
                        </li>
                        <li class="breadcrumb-item text-muted">/</li>
                        <li class="breadcrumb-item text-ghopo">Dashboard Overhoul</li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-7 d-flex justify-content-md-end flex-no-wrap px-5">
                <div class="filter">
                    <div class="row">
                        <div class="col mt-2">
                            <select class="form-select h-25" data-control="select2"
                                data-placeholder="Pilih Fasilitator">
                                <option></option>
                                <option value="1">Option 1</option>
                                <option value="2">Option 2</option>
                            </select>
                        </div>
                        <div class="col mt-2">
                            <select class="form-select h-25" data-control="select2" data-placeholder="Pilih Bulan">
                                @foreach(getMonth() as $key => $value)
                                @if ($value['month'] == date("n") )
                                <option value="{{$value['month']}}" selected>{{$value['month_name']}}</option>
                                @else
                                <option value="{{$value['month']}}">{{$value['month_name']}}</option>
                                @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col mt-2">
                            <select class="form-select h-25" data-control="select2" data-placeholder="Pilih Tahun">
                                @foreach(getYear() as $key => $value)
                                @if ($value == date("Y") )
                                <option value="{{$value}}" selected>{{$value}}</option>
                                @else
                                <option value="{{$value}}">{{$value}}</option>
                                @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col mt-2 card-breadcrumb-button ">
                            <button class="btn btn-primary">Tampilkan</button>
                        </div>
                        <div class="col mt-2 card-breadcrumb-button">
                            <button href="#" class="btn btn-add" data-kt-menu-trigger="click"
                                data-kt-menu-placement="bottom-end">Tambah</button>
                            <div class="menu menu-sub menu-sub-dropdown w-200px w-md-200px" data-kt-menu="true"
                                id="kt_menu_63d780f5e6a12">
                                <div class="separator border-gray-200"></div>
                                <div class="">
                                    <div class="text-left">
                                        <button class="btn btn-sm btn-active-light-primary py-4 w-100"
                                            data-bs-toggle="modal" data-bs-target="#kt_modal_data_preparation"
                                            data-kt-menu-dismiss="true">Import Data Preparation</button>
                                        <button class="btn btn-sm btn-active-light-primary py-4 w-100"
                                            data-bs-toggle="modal" data-bs-target="#kt_modal_add_data_process"
                                            data-kt-menu-dismiss="true">Import Data Activity</button>
                                        <button class="btn btn-sm btn-active-light-primary py-4 w-100"
                                            data-bs-toggle="modal" data-bs-target="#kt_modal_create_app"
                                            data-kt-menu-dismiss="true">Add Data Proses</button>
                                        <button class="btn btn-sm btn-active-light-primary py-4 w-100"
                                            data-bs-toggle="modal" data-bs-target="#kt_modal_setting_periode"
                                            data-kt-menu-dismiss="true">Setting Periode</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="g-5 g-xl-10 mb-3 mb-xl-3 mt-6">
        <div class="card-dashboard-overhoul">
            <div class="row">
                <div class="col-sm-12">
                    <div class="preparation">
                        <h1>Preparation</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="g-5 g-xl-10 mt-4 mb-3 mb-xl-3">
        <div class="card-dashboard-overhoul">
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md col-lg-2 mt-1 card-top-over-houl">
                            @include('partials/widgets/cards/_widget-overhoul-progress')
                        </div>
                        <div class="col-12 col-sm-12 col-md col-lg-2 mt-1 card-top-over-houl">
                            @include('partials/widgets/cards/_widget-overhoul-budget')
                        </div>
                        <div class="col-12 col-sm-12 col-md col-lg-2 mt-1 card-top-over-houl">
                            @include('partials/widgets/cards/_widget-overhoul-part-opex')
                            @include('partials/widgets/cards/_widget-overhoul-part-opex-two')
                        </div>
                        <div class="col-12 col-sm-12 col-md col-lg-3 mt-1 card-top-over-houl">
                            @include('partials/widgets/cards/_widget-overhoul-part-budget-opex')
                            @include('partials/widgets/cards/_widget-overhoul-part-capex')
                        </div>
                        <div class="col-12 col-sm-12 col-md col-lg-3 mt-1 card-top-over-houl">
                            @include('partials/widgets/cards/_widget-overhoul-part-rutin')
                            @include('partials/widgets/cards/_widget-overhoul-part-non-rutin')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="g-5 g-xl-10 mt-4 mb-3 mb-xl-3">
        <div class="card-dashboard-overhoul">
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-12 col-lg">
                            @include('partials/widgets/cards/_widget-overhoul-hot-issue')
                        </div>
                        <div class="col-12 col-lg">
                            @include('partials/widgets/cards/_widget-overhoul-part-rutin-data')
                        </div>
                        <div class="col-12 col-lg">
                            @include('partials/widgets/cards/_widget-overhoul-part-non-rutin-data')
                        </div>
                        <div class="col-12 col-lg">
                            @include('partials/widgets/cards/_widget-overhoul-gallery-preparation')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="g-5 g-xl-10 mb-3 mb-xl-3">
        <div class="card-dashboard-overhoul">
            <div class="row">
                <div class="col-sm-12 col-custom">
                    <div class="preparation">
                        <h1>Progress</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="g-5 g-xl-10 mt-4 mb-3 mb-xl-3">
        <div class="card-dashboard-overhoul">
            <div class="row">
                <div class="col-sm-8 col-custom">
                    @include('partials/widgets/cards/_widget-overhoul-progress-table')
                </div>
                <div class="col-sm-2">
                    @include('partials/widgets/cards/_widget-overhoul-progress-day')

                </div>
                <div class="col-sm-2">
                    @include('partials/widgets/cards/_widget-overhoul-progress-total')
                </div>

            </div>
        </div>
    </div>

    <div class="g-5 g-xl-10 mt-4 mb-3 mb-xl-3">
        <div class="card-dashboard-overhoul">
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-12 col-lg">
                            @include('partials/widgets/cards/_widget-overhoul-major-activity')
                        </div>
                        <div class="col-12 col-lg">
                            @include('partials/widgets/cards/_widget-overhoul-biaya-opex')
                        </div>
                        <div class="col-12 col-lg">
                            @include('partials/widgets/cards/_widget-overhoul-kurva')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="g-5 g-xl-10 mt-4 mb-3 mb-xl-3">
        <div class="card-dashboard-overhoul">
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-12 col-lg">
                            @include('partials/widgets/cards/_widget-overhoul-post-biaya-opex')
                        </div>
                        <div class="col-12 col-lg">
                            @include('partials/widgets/cards/_widget-overhoul-gallery-dokumentasi')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal View Img --}}
    <div id="myModalViewImg" class="modal-view-img">
        <span class="closeViewImg">&times;</span>
        <img class="modal-content-view-img" id="img01">
        <div id="caption-view-img" class="caption-view-img-over-houl"></div>
    </div>

    <!-- modal import data preparation -->
    <div class="modal fade" id="kt_modal_data_preparation" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered mw-450px">
            <div class="modal-content">
                <div class="modal-header">
                    <h2>Import Data Preparation</h2>
                    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                        <span class="svg-icon svg-icon-1">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1"
                                    transform="rotate(-45 6 17.3137)" fill="currentColor" />
                                <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)"
                                    fill="currentColor" />
                            </svg>
                        </span>
                    </div>
                </div>
                <div class="modal-body py-lg-10 px-lg-10">
                    <div class="d-flex flex-column mb-5 fv-row form-group">
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="">Template</span>
                        </label>
                        <button type="button" class="btn w-100 btn-download-template btn-lg"
                            data-kt-stepper-action="next">
                            <img src="{{ image('icons/ghopo-icon/ghopo_download_icon.png') }}" alt="JS">
                            Download Template</button>
                    </div>
                    <div class="d-flex flex-column fv-row form-group">
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Upload File</span>
                        </label>
                        <button type="button" class="btn w-100 btn-download-template y btn-lg me-3"
                            data-kt-stepper-action="next">
                            <img src="{{ image('icons/ghopo-icon/ghopo_choose_icon.png') }}" alt="JS">
                            Chose File</button>
                    </div>
                    <span class="mb-3 text-primary-ghopo">*file yang di upload adalah file berdasarkan template yang
                        disediakan</span>

                    <div class="d-flex justify-content-end mt-4">
                        <button type="button" class="btn me-3 btn-border-primary btn-lg"
                            data-kt-stepper-action="next">Batal</button>
                        <button type="button" class="btn btn-lg btn-add" data-kt-stepper-action="next">Simpan</button>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- modal import data activity -->
    <div class="modal fade" id="kt_modal_add_data_process" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered mw-450px">
            <div class="modal-content">
                <div class="modal-header">
                    <h2>Import Data Activity</h2>
                    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                        <span class="svg-icon svg-icon-1">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1"
                                    transform="rotate(-45 6 17.3137)" fill="currentColor" />
                                <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)"
                                    fill="currentColor" />
                            </svg>
                        </span>
                    </div>
                </div>
                <div class="modal-body py-lg-5 px-lg-5">
                    <div class="d-flex flex-column mb-5 fv-row form-group">
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="">Template</span>
                        </label>
                        <button type="button" class="btn w-100 btn-download-template btn-lg"
                            data-kt-stepper-action="next">
                            <img src="{{ image('icons/ghopo-icon/ghopo_download_icon.png') }}" alt="JS">
                            Download Template</button>
                    </div>
                    <div class="d-flex flex-column fv-row form-group">
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Upload File</span>
                        </label>
                        <button type="button" class="btn w-100 btn-download-template y btn-lg me-3"
                            data-kt-stepper-action="next">
                            <img src="{{ image('icons/ghopo-icon/ghopo_choose_icon.png') }}" alt="JS">
                            Chose File</button>
                    </div>
                    <span class="mb-3 text-primary-ghopo">*file yang di upload adalah file berdasarkan template yang
                        disediakan</span>

                    <div class="d-flex justify-content-end mt-4">
                        <button type="button" class="btn btn-border-primary btn-lg me-3"
                            data-kt-stepper-action="next">Batal</button>
                        <button type="button" class="btn btn-lg btn-add" data-kt-stepper-action="next">Simpan</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- modal Setting Periode -->
    <div class="modal fade" id="kt_modal_setting_periode" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered mw-450px">
            <div class="modal-content">
                <div class="modal-header">
                    <h2>Setting Periode</h2>
                    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                        <span class="svg-icon svg-icon-1">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1"
                                    transform="rotate(-45 6 17.3137)" fill="currentColor" />
                                <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)"
                                    fill="currentColor" />
                            </svg>
                        </span>
                    </div>
                </div>
                <div class="modal-body py-lg-5 px-lg-5">
                    <form action="" form class="mx-auto mw-500px w-100" novalidate="novalidate"
                        id="kt_modal_offer_a_deal_form">
                        <div class="fv-row mb-8">
                            <label class="required fs-6 fw-semibold mb-2">Tanggal Akhir</label>
                            <div class="position-relative d-flex align-items-center">
                                <!--begin::Icon-->
                                <!--begin::Svg Icon | path: icons/duotune/general/gen014.svg-->
                                <span class="svg-icon svg-icon-2 position-absolute mx-4">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path opacity="0.3"
                                            d="M21 22H3C2.4 22 2 21.6 2 21V5C2 4.4 2.4 4 3 4H21C21.6 4 22 4.4 22 5V21C22 21.6 21.6 22 21 22Z"
                                            fill="currentColor" />
                                        <path
                                            d="M6 6C5.4 6 5 5.6 5 5V3C5 2.4 5.4 2 6 2C6.6 2 7 2.4 7 3V5C7 5.6 6.6 6 6 6ZM11 5V3C11 2.4 10.6 2 10 2C9.4 2 9 2.4 9 3V5C9 5.6 9.4 6 10 6C10.6 6 11 5.6 11 5ZM15 5V3C15 2.4 14.6 2 14 2C13.4 2 13 2.4 13 3V5C13 5.6 13.4 6 14 6C14.6 6 15 5.6 15 5ZM19 5V3C19 2.4 18.6 2 18 2C17.4 2 17 2.4 17 3V5C17 5.6 17.4 6 18 6C18.6 6 19 5.6 19 5Z"
                                            fill="currentColor" />
                                        <path
                                            d="M8.8 13.1C9.2 13.1 9.5 13 9.7 12.8C9.9 12.6 10.1 12.3 10.1 11.9C10.1 11.6 10 11.3 9.8 11.1C9.6 10.9 9.3 10.8 9 10.8C8.8 10.8 8.59999 10.8 8.39999 10.9C8.19999 11 8.1 11.1 8 11.2C7.9 11.3 7.8 11.4 7.7 11.6C7.6 11.8 7.5 11.9 7.5 12.1C7.5 12.2 7.4 12.2 7.3 12.3C7.2 12.4 7.09999 12.4 6.89999 12.4C6.69999 12.4 6.6 12.3 6.5 12.2C6.4 12.1 6.3 11.9 6.3 11.7C6.3 11.5 6.4 11.3 6.5 11.1C6.6 10.9 6.8 10.7 7 10.5C7.2 10.3 7.49999 10.1 7.89999 10C8.29999 9.90003 8.60001 9.80003 9.10001 9.80003C9.50001 9.80003 9.80001 9.90003 10.1 10C10.4 10.1 10.7 10.3 10.9 10.4C11.1 10.5 11.3 10.8 11.4 11.1C11.5 11.4 11.6 11.6 11.6 11.9C11.6 12.3 11.5 12.6 11.3 12.9C11.1 13.2 10.9 13.5 10.6 13.7C10.9 13.9 11.2 14.1 11.4 14.3C11.6 14.5 11.8 14.7 11.9 15C12 15.3 12.1 15.5 12.1 15.8C12.1 16.2 12 16.5 11.9 16.8C11.8 17.1 11.5 17.4 11.3 17.7C11.1 18 10.7 18.2 10.3 18.3C9.9 18.4 9.5 18.5 9 18.5C8.5 18.5 8.1 18.4 7.7 18.2C7.3 18 7 17.8 6.8 17.6C6.6 17.4 6.4 17.1 6.3 16.8C6.2 16.5 6.10001 16.3 6.10001 16.1C6.10001 15.9 6.2 15.7 6.3 15.6C6.4 15.5 6.6 15.4 6.8 15.4C6.9 15.4 7.00001 15.4 7.10001 15.5C7.20001 15.6 7.3 15.6 7.3 15.7C7.5 16.2 7.7 16.6 8 16.9C8.3 17.2 8.6 17.3 9 17.3C9.2 17.3 9.5 17.2 9.7 17.1C9.9 17 10.1 16.8 10.3 16.6C10.5 16.4 10.5 16.1 10.5 15.8C10.5 15.3 10.4 15 10.1 14.7C9.80001 14.4 9.50001 14.3 9.10001 14.3C9.00001 14.3 8.9 14.3 8.7 14.3C8.5 14.3 8.39999 14.3 8.39999 14.3C8.19999 14.3 7.99999 14.2 7.89999 14.1C7.79999 14 7.7 13.8 7.7 13.7C7.7 13.5 7.79999 13.4 7.89999 13.2C7.99999 13 8.2 13 8.5 13H8.8V13.1ZM15.3 17.5V12.2C14.3 13 13.6 13.3 13.3 13.3C13.1 13.3 13 13.2 12.9 13.1C12.8 13 12.7 12.8 12.7 12.6C12.7 12.4 12.8 12.3 12.9 12.2C13 12.1 13.2 12 13.6 11.8C14.1 11.6 14.5 11.3 14.7 11.1C14.9 10.9 15.2 10.6 15.5 10.3C15.8 10 15.9 9.80003 15.9 9.70003C15.9 9.60003 16.1 9.60004 16.3 9.60004C16.5 9.60004 16.7 9.70003 16.8 9.80003C16.9 9.90003 17 10.2 17 10.5V17.2C17 18 16.7 18.4 16.2 18.4C16 18.4 15.8 18.3 15.6 18.2C15.4 18.1 15.3 17.8 15.3 17.5Z"
                                            fill="currentColor" />
                                    </svg>
                                </span>
                                <!--end::Svg Icon-->
                                <!--end::Icon-->
                                <!--begin::Datepicker-->
                                <input class="border form-control form-control-solid ps-12"
                                    placeholder="Pick date range" name="details_activation_date" />
                                <!--end::Datepicker-->
                            </div>
                        </div>
                        <div class="fv-row mb-8">
                            <label class="required fs-6 fw-semibold mb-2">Tanggal Akhir</label>
                            <div class="position-relative d-flex align-items-center">
                                <!--begin::Icon-->
                                <!--begin::Svg Icon | path: icons/duotune/general/gen014.svg-->
                                <span class="svg-icon svg-icon-2 position-absolute mx-4">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path opacity="0.3"
                                            d="M21 22H3C2.4 22 2 21.6 2 21V5C2 4.4 2.4 4 3 4H21C21.6 4 22 4.4 22 5V21C22 21.6 21.6 22 21 22Z"
                                            fill="currentColor" />
                                        <path
                                            d="M6 6C5.4 6 5 5.6 5 5V3C5 2.4 5.4 2 6 2C6.6 2 7 2.4 7 3V5C7 5.6 6.6 6 6 6ZM11 5V3C11 2.4 10.6 2 10 2C9.4 2 9 2.4 9 3V5C9 5.6 9.4 6 10 6C10.6 6 11 5.6 11 5ZM15 5V3C15 2.4 14.6 2 14 2C13.4 2 13 2.4 13 3V5C13 5.6 13.4 6 14 6C14.6 6 15 5.6 15 5ZM19 5V3C19 2.4 18.6 2 18 2C17.4 2 17 2.4 17 3V5C17 5.6 17.4 6 18 6C18.6 6 19 5.6 19 5Z"
                                            fill="currentColor" />
                                        <path
                                            d="M8.8 13.1C9.2 13.1 9.5 13 9.7 12.8C9.9 12.6 10.1 12.3 10.1 11.9C10.1 11.6 10 11.3 9.8 11.1C9.6 10.9 9.3 10.8 9 10.8C8.8 10.8 8.59999 10.8 8.39999 10.9C8.19999 11 8.1 11.1 8 11.2C7.9 11.3 7.8 11.4 7.7 11.6C7.6 11.8 7.5 11.9 7.5 12.1C7.5 12.2 7.4 12.2 7.3 12.3C7.2 12.4 7.09999 12.4 6.89999 12.4C6.69999 12.4 6.6 12.3 6.5 12.2C6.4 12.1 6.3 11.9 6.3 11.7C6.3 11.5 6.4 11.3 6.5 11.1C6.6 10.9 6.8 10.7 7 10.5C7.2 10.3 7.49999 10.1 7.89999 10C8.29999 9.90003 8.60001 9.80003 9.10001 9.80003C9.50001 9.80003 9.80001 9.90003 10.1 10C10.4 10.1 10.7 10.3 10.9 10.4C11.1 10.5 11.3 10.8 11.4 11.1C11.5 11.4 11.6 11.6 11.6 11.9C11.6 12.3 11.5 12.6 11.3 12.9C11.1 13.2 10.9 13.5 10.6 13.7C10.9 13.9 11.2 14.1 11.4 14.3C11.6 14.5 11.8 14.7 11.9 15C12 15.3 12.1 15.5 12.1 15.8C12.1 16.2 12 16.5 11.9 16.8C11.8 17.1 11.5 17.4 11.3 17.7C11.1 18 10.7 18.2 10.3 18.3C9.9 18.4 9.5 18.5 9 18.5C8.5 18.5 8.1 18.4 7.7 18.2C7.3 18 7 17.8 6.8 17.6C6.6 17.4 6.4 17.1 6.3 16.8C6.2 16.5 6.10001 16.3 6.10001 16.1C6.10001 15.9 6.2 15.7 6.3 15.6C6.4 15.5 6.6 15.4 6.8 15.4C6.9 15.4 7.00001 15.4 7.10001 15.5C7.20001 15.6 7.3 15.6 7.3 15.7C7.5 16.2 7.7 16.6 8 16.9C8.3 17.2 8.6 17.3 9 17.3C9.2 17.3 9.5 17.2 9.7 17.1C9.9 17 10.1 16.8 10.3 16.6C10.5 16.4 10.5 16.1 10.5 15.8C10.5 15.3 10.4 15 10.1 14.7C9.80001 14.4 9.50001 14.3 9.10001 14.3C9.00001 14.3 8.9 14.3 8.7 14.3C8.5 14.3 8.39999 14.3 8.39999 14.3C8.19999 14.3 7.99999 14.2 7.89999 14.1C7.79999 14 7.7 13.8 7.7 13.7C7.7 13.5 7.79999 13.4 7.89999 13.2C7.99999 13 8.2 13 8.5 13H8.8V13.1ZM15.3 17.5V12.2C14.3 13 13.6 13.3 13.3 13.3C13.1 13.3 13 13.2 12.9 13.1C12.8 13 12.7 12.8 12.7 12.6C12.7 12.4 12.8 12.3 12.9 12.2C13 12.1 13.2 12 13.6 11.8C14.1 11.6 14.5 11.3 14.7 11.1C14.9 10.9 15.2 10.6 15.5 10.3C15.8 10 15.9 9.80003 15.9 9.70003C15.9 9.60003 16.1 9.60004 16.3 9.60004C16.5 9.60004 16.7 9.70003 16.8 9.80003C16.9 9.90003 17 10.2 17 10.5V17.2C17 18 16.7 18.4 16.2 18.4C16 18.4 15.8 18.3 15.6 18.2C15.4 18.1 15.3 17.8 15.3 17.5Z"
                                            fill="currentColor" />
                                    </svg>
                                </span>
                                <!--end::Svg Icon-->
                                <!--end::Icon-->
                                <!--begin::Datepicker-->
                                <input class="border form-control form-control-solid ps-12"
                                    placeholder="Pick date range" name="details_activation_date" />
                                <!--end::Datepicker-->
                            </div>
                        </div>

                        <div class="d-flex justify-content-end mt-4">
                            <button type="button" class="btn btn-border-primary btn-lg me-3"
                                data-kt-stepper-action="next">Batal</button>
                            <button type="button" class="btn btn-lg btn-add"
                                data-kt-stepper-action="next">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-default-layout>
<script>
    $(function() {

        var modal = document.getElementById("myModalViewImg");

        var img1 = document.getElementById("myImg1");
        var img2 = document.getElementById("myImg2");
        var img3 = document.getElementById("myImg3");
        var modalImg = document.getElementById("img01");
        var captionText = document.getElementById("caption-view-img");

        img1.onclick = function(){
            modal.style.display = "block";
            modalImg.src = this.src;
            captionText.innerHTML = this.alt;
        }
        img2.onclick = function(){
            modal.style.display = "block";
            modalImg.src = this.src;
            captionText.innerHTML = this.alt;
        }
        img3.onclick = function(){
            modal.style.display = "block";
            modalImg.src = this.src;
            captionText.innerHTML = this.alt;
        }

        var span = document.getElementsByClassName("closeViewImg")[0];

        span.onclick = function() {
        modal.style.display = "none";
}
    });
</script>
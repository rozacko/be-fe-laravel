<x-default-layout>
  <div class="py-lg-5">
    <div class="row">
      <div class="col-md-6">
        <div class="information">
          <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">Maintenance
            Cost</h1>
          <ul class="breadcrumb breadcrumb-separatorless ">
            <li class="breadcrumb-item text-muted">
              <a href="/" class="text-muted text-hover-primary">Maintenance Cost</a>
            </li>
            <li class="breadcrumb-item text-muted">/</li>
            <li class="breadcrumb-item text-ghopo">Maintenance Cost</li>
          </ul>
        </div>
      </div>
      <div class="col-md-6">
        <div class="">
          <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-3">
              <select class="form-select h-25" data-control="select2" data-placeholder="Pilih tahun">
                <option></option>
                <option value="1">Option 1</option>
                <option value="2">Option 2</option>
              </select>
            </div>
            <div class="col-md-3">
              <select class="form-select h-25" data-control="select2" data-placeholder="Pilih bulan">
                <option></option>
                <option value="1">Option 1</option>
                <option value="2">Option 2</option>
              </select>
            </div>
            <div class="col-md-3">
              <button class="btn btn-history w-100">Tampilkan</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


  <div class="g-5 g-xl-10 mb-3 mb-xl-3">
    <div class="card-maintenance-cost">
      <div class="row">
        <div class="col-sm-3 col-custom">
          @include('partials/widgets/cards/_widget-klin-net-1')
        </div>
        <div class="col-sm-3 col-custom">
          @include('partials/widgets/cards/_widget-klin-net-2')
        </div>
        <div class="col-sm-4 col-custom">
          @include('partials/widgets/cards/_widget-klin-net-availability')
        </div>
        <div class="col-sm-2 col-custom">
          @include('partials/widgets/cards/_widget-clinker-production')
        </div>
      </div>
    </div>
  </div>

  <div class="g-5 g-xl-10 mt-4 mb-3 mb-xl-3">
    <div class="card-maintenance-cost">
      <div class="row">
        <div class="col-md-12 col-custom">
          @include('partials/widgets/cards/_widget-detail-pembiayaan')
        </div>
      </div>
    </div>

    <div class="g-5 g-xl-10 mt-4 mb-3 mb-xl-3">
      <div class="card-maintenance-cost">
        <div class="row">
          <div class="col-sm-3 col-custom">
            @include('partials/widgets/cards/_widget-plan-property')
          </div>
          <div class="col-sm-5 col-custom">
            @include('partials/widgets/cards/_widget-klin-net-availability-2')
          </div>
          <div class="col-sm-4 col-custom">
            @include('partials/widgets/cards/_widget-klin-ret-summary')
          </div>
        </div>
      </div>
    </div>

    <div class="g-5 g-xl-10 mt-4 mb-3 mb-xl-3">
      <div class="card-maintenance-cost">
        <div class="row">
          <div class="col-sm-4 col-custom">
            @include('partials/widgets/cards/_widget-perbandingan-nilai-inventory')
          </div>
          <div class="col-sm-8 col-custom">
            @include('partials/widgets/cards/_widget-klin-rate-tdp')
          </div>
        </div>
      </div>
    </div>

</x-default-layout>
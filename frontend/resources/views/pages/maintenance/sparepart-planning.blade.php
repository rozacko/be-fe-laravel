<x-default-layout>
  <div class="py-lg-5">
    <div class="row">
      <div class="col-md-6">
        <div class="information">
          <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">
            Sparepart Planning</h1>
          <ul class="breadcrumb breadcrumb-separatorless ">
            <li class="breadcrumb-item text-muted">
              <a href="/" class="text-muted text-hover-primary">Maintenance</a>
            </li>
            <li class="breadcrumb-item text-muted">/</li>
            <li class="breadcrumb-item text-ghopo">Sparepart Planning</li>
          </ul>
        </div>
      </div>
      <div class="col-md-6">
        <div class="">
          <div class="row">
            <div class="col-md-3">
              <select class="form-select h-25" data-control="select2" data-placeholder="Pilih plant">
                <option></option>
                <option value="1">Option 1</option>
                <option value="2">Option 2</option>
              </select>
            </div>
            <div class="col-md-3">
              <select class="form-select h-25" data-control="select2" data-placeholder="Pilih Tahun">
                @foreach(getYear() as $key => $value)
                @if ($value == date("Y") )
                <option value="{{$value}}" selected>{{$value}}</option>
                @else
                <option value="{{$value}}">{{$value}}</option>
                @endif
                @endforeach
              </select>
            </div>
            <div class="col-md-3">
              <select class="form-select h-25" data-control="select2" data-placeholder="Pilih Bulan">
                @foreach(getMonth() as $key => $value)
                @if ($value['month'] == date("n") )
                <option value="{{$value['month']}}" selected>{{$value['month_name']}}</option>
                @else
                <option value="{{$value['month']}}">{{$value['month_name']}}</option>
                @endif
                @endforeach
              </select>
            </div>
            <div class="col-md-3">
              <button class="btn btn-history w-100">Tampilkan</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


  <section class="card-sparepart-planning">
    {{-- non rutin --}}
    <div class="card-sparepart-planning__header-title g-5 g-xl-10 mb-3 mb-xl-3 mt-6">
      <div class="row">
        <div class="col-sm-12 col-custom">
          <h1>JASA NON RUTIN</h1>
        </div>
      </div>
    </div>

    <div class="card-sparepart-planning__body">
      <div class="non-rutin">
        <div class="row">
          <div class="col-md-2 col-custom">
            <div class="non-rutin__record">
              <div class="card">
                <div class="record-count">
                  <img src="{{ image('icons/ghopo-icon/ghopo_record_count_icon.png') }}" alt="JS">
                  <div>
                    <label for="">Record Count</label>
                  </div>
                  <div class="total-count">
                    47584337
                  </div>
                </div>
                <div class="devider my-6"></div>
                <div class="value">
                  <img src="{{ image('icons/ghopo-icon/ghopo_rocket_icon.png') }}" alt="JS">
                  <div>
                    <label for="">Nilai</label>
                  </div>
                  <div class="total-count">
                    47584337
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-custom">
            <div class="non-rutin__date">
              <div class="card">
                <div class="table-responsive">
                  <table class="table">
                    <thead>
                      <tr>
                        <th class="bg-black-300 align-middle text-center color-header-tabel">Tanggal</th>
                        <th class="bg-black-300 align-middle text-center color-header-tabel">Electrical</th>
                        <th class="bg-black-300 align-middle text-center color-header-tabel">Mekanikal</th>
                        <th class="bg-black-300 align-middle text-center color-header-tabel">Sipil</th>
                        <th class="bg-black-300 align-middle text-center color-header-tabel">Pabrik Gresik</th>
                      </tr>
                    </thead>
                    <tbody>

                      <tr>
                        <td class="text-center">May 2022</td>
                        <td class="text-center">Lorem Ipsum</td>
                        <td class="text-center">Lorem Ipsum</td>
                        <td class="text-center">Lorem Ipsum</td>
                        <td class="text-center">Lorem Ipsum</td>
                      </tr>
                      <tr>
                        <td class="text-center">May 2022</td>
                        <td class="text-center">Lorem Ipsum</td>
                        <td class="text-center">Lorem Ipsum</td>
                        <td class="text-center">Lorem Ipsum</td>
                        <td class="text-center">Lorem Ipsum</td>
                      </tr>
                      <tr>
                        <td class="text-center">May 2022</td>
                        <td class="text-center">Lorem Ipsum</td>
                        <td class="text-center">Lorem Ipsum</td>
                        <td class="text-center">Lorem Ipsum</td>
                        <td class="text-center">Lorem Ipsum</td>
                      </tr>
                      <tr>
                        <td class="text-center">May 2022</td>
                        <td class="text-center">Lorem Ipsum</td>
                        <td class="text-center">Lorem Ipsum</td>
                        <td class="text-center">Lorem Ipsum</td>
                        <td class="text-center">Lorem Ipsum</td>
                      </tr>
                      <tr>
                        <td class="text-center">May 2022</td>
                        <td class="text-center">Lorem Ipsum</td>
                        <td class="text-center">Lorem Ipsum</td>
                        <td class="text-center">Lorem Ipsum</td>
                        <td class="text-center">Lorem Ipsum</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-2 col-custom">
            <div class="non-rutin__month">
              @include('partials/widgets/cards/_widget-sparepart-month')
            </div>
          </div>
          <div class="col-md-2 col-custom">
            <div class="non-rutin__type">
              @include('partials/widgets/cards/_widget-sparepart-type')
            </div>
          </div>
        </div>
        <div class="row mt-4">
          <div class="col-md-5 col-custom">
            <div class="non-rutin__type-one">
              @include('partials/widgets/cards/_widget-sparepart-type-one')
            </div>
          </div>
          <div class="col-md-3 col-custom">
            <div class="non-rutin__type-two">
              @include('partials/widgets/cards/_widget-sparepart-type-two')
            </div>
          </div>
          <div class="col-md-4 col-custom">
            <div class="non-rutin__type-three">
              @include('partials/widgets/cards/_widget-sparepart-type-three')
            </div>
          </div>
        </div>
      </div>
    </div>
    {{-- end non rutin --}}

    {{-- jasa rutin --}}
    <div class="card-sparepart-planning__header-title g-5 g-xl-10 mb-3 mb-xl-3 mt-6">
      <div class="row">
        <div class="col-sm-12 col-custom">
          <h1>JASA RUTIN FIX</h1>
        </div>
      </div>
    </div>

    <div class="card-sparepart-planning__body">
      <div class="rutin">
        <div class="row">
          <div class="col-md-2 col-custom">
            <div class="rutin__record">
              <div class="card">
                <div class="record-count">
                  <img src="{{ image('icons/ghopo-icon/ghopo_record_count_icon.png') }}" alt="JS">
                  <div>
                    <label for="">Record Count</label>
                  </div>
                  <div class="total-count">
                    47584337
                  </div>
                </div>
                <div class="devider my-6"></div>
                <div class="value">
                  <img src="{{ image('icons/ghopo-icon/ghopo_rocket_icon.png') }}" alt="JS">
                  <div>
                    <label for="">Kontrak</label>
                  </div>
                  <div class="total-count">
                    47584337
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-custom">
            <div class="rutin__sembaran">
              @include('partials/widgets/cards/_widget-sparepart-sembaran-pengawasan')
            </div>
          </div>
          <div class="col-md-2 col-custom">
            <div class="rutin__sembaran">
              @include('partials/widgets/cards/_widget-sparepart-sembaran-plant')
            </div>
          </div>
          <div class="col-md-3 col-custom">
            <div class="rutin__sembaran">
              @include('partials/widgets/cards/_widget-sparepart-sembaran-nilai-kontrak')
            </div>
          </div>
          <div class="col-md-2 col-custom">
            <div class="rutin__wilayah">
              <div class="card">
                <div class="record-count">
                  <img src="{{ image('icons/ghopo-icon/ghopo_record_count_icon.png') }}" alt="JS">
                  <div>
                    <label for="">TUBAN</label>
                  </div>
                  <div class="total-count">
                    47584337
                  </div>
                </div>
                <div class="value">
                  <img src="{{ image('icons/ghopo-icon/ghopo_rocket_icon.png') }}" alt="JS">
                  <div>
                    <label for="">GRESIK</label>
                  </div>
                  <div class="total-count">
                    47584337
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row mt-4">
          <div class="col-md-12">
            <div class="card">
              <div class="table-responsive">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th class="font-weight-bold text-dark align-middle text-center color-header-tabel">Fisnish Date
                      </th>
                      <th class="font-weight-bold align-middle text-center color-header-tabel">Start Date</th>
                      <th class="font-weight-bold align-middle text-center color-header-tabel">Pekerjaan</th>
                      <th class="font-weight-bold align-middle text-center color-header-tabel">Vendor</th>
                      <th class="font-weight-bold align-middle text-center color-header-tabel">Kontrak</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td class="text-center">May 2022</td>
                      <td class="text-center">Lorem Ipsum</td>
                      <td class="text-center">Lorem Ipsum</td>
                      <td class="text-center">Lorem Ipsum</td>
                      <td class="text-center">Lorem Ipsum</td>
                    </tr>
                    <tr>
                      <td class="text-center">May 2022</td>
                      <td class="text-center">Lorem Ipsum</td>
                      <td class="text-center">Lorem Ipsum</td>
                      <td class="text-center">Lorem Ipsum</td>
                      <td class="text-center">Lorem Ipsum</td>
                    </tr>
                    <tr>
                      <td class="text-center">May 2022</td>
                      <td class="text-center">Lorem Ipsum</td>
                      <td class="text-center">Lorem Ipsum</td>
                      <td class="text-center">Lorem Ipsum</td>
                      <td class="text-center">Lorem Ipsum</td>
                    </tr>
                    <tr>
                      <td class="text-center">May 2022</td>
                      <td class="text-center">Lorem Ipsum</td>
                      <td class="text-center">Lorem Ipsum</td>
                      <td class="text-center">Lorem Ipsum</td>
                      <td class="text-center">Lorem Ipsum</td>
                    </tr>
                    <tr>
                      <td class="text-center">May 2022</td>
                      <td class="text-center">Lorem Ipsum</td>
                      <td class="text-center">Lorem Ipsum</td>
                      <td class="text-center">Lorem Ipsum</td>
                      <td class="text-center">Lorem Ipsum</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
    {{-- end jasa rutin --}}

    {{-- jasa overhoul --}}
    <div class="card-sparepart-planning__header-title g-5 g-xl-10 mb-3 mb-xl-3 mt-6">
      <div class="row">
        <div class="col-sm-12 col-custom">
          <h1>JASA OVERHAUL</h1>
        </div>
      </div>
    </div>

    <div class="card-sparepart-planning__body">
      <div class="overhoul">
        <div class="row">
          <div class="col-md-2 col-custom">
            <div class="overhoul__record">
              <div class="card">
                <div class="record-count">
                  <img src="{{ image('icons/ghopo-icon/ghopo_record_count_icon.png') }}" alt="JS">
                  <div>
                    <label for="">Record Count</label>
                  </div>
                  <div class="total-count">
                    47584337
                  </div>
                </div>
                <div class="devider my-6"></div>
                <div class="value">
                  <img src="{{ image('icons/ghopo-icon/ghopo_rocket_icon.png') }}" alt="JS">
                  <div>
                    <label for="">Kontrak</label>
                  </div>
                  <div class="total-count">
                    47584337
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4 col-custom">
            <div class="overhoul__sembaran">
              @include('partials/widgets/cards/_widget-sparepart-overhoul-sembaran-nilai-kontrak')
            </div>
          </div>
          <div class="col-md-4 col-custom">
            <div class="overhoul__sembaran">
              @include('partials/widgets/cards/_widget-sparepart-overhoul-sembaran-pengawasan')
            </div>
          </div>

          <div class="col-md-2 col-custom">
            <div class="overhoul__wilayah">
              <div class="card">
                <div class="record-count">
                  <img src="{{ image('icons/ghopo-icon/ghopo_record_count_icon.png') }}" alt="JS">
                  <div>
                    <label for="">KONTRAK AWAL</label>
                  </div>
                  <div class="total-count">
                    47584337
                  </div>
                </div>
                <div class="value">
                  <img src="{{ image('icons/ghopo-icon/ghopo_rocket_icon.png') }}" alt="JS">
                  <div>
                    <label for="">ADENSUM</label>
                  </div>
                  <div class="total-count">
                    47584337
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row mt-4">
          <div class="col-md-12">
            <div class="card">
              <div class="table-responsive">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th class="font-weight-bold text-dark align-middle text-center color-header-tabel">Fisnish Date
                      </th>
                      <th class="font-weight-bold align-middle text-center color-header-tabel">Start Date</th>
                      <th class="font-weight-bold align-middle text-center color-header-tabel">Pekerjaan</th>
                      <th class="font-weight-bold align-middle text-center color-header-tabel">Vendor</th>
                      <th class="font-weight-bold align-middle text-center color-header-tabel">Kontrak</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td class="text-center">May 2022</td>
                      <td class="text-center">Lorem Ipsum</td>
                      <td class="text-center">Lorem Ipsum</td>
                      <td class="text-center">Lorem Ipsum</td>
                      <td class="text-center">Lorem Ipsum</td>
                    </tr>
                    <tr>
                      <td class="text-center">May 2022</td>
                      <td class="text-center">Lorem Ipsum</td>
                      <td class="text-center">Lorem Ipsum</td>
                      <td class="text-center">Lorem Ipsum</td>
                      <td class="text-center">Lorem Ipsum</td>
                    </tr>
                    <tr>
                      <td class="text-center">May 2022</td>
                      <td class="text-center">Lorem Ipsum</td>
                      <td class="text-center">Lorem Ipsum</td>
                      <td class="text-center">Lorem Ipsum</td>
                      <td class="text-center">Lorem Ipsum</td>
                    </tr>
                    <tr>
                      <td class="text-center">May 2022</td>
                      <td class="text-center">Lorem Ipsum</td>
                      <td class="text-center">Lorem Ipsum</td>
                      <td class="text-center">Lorem Ipsum</td>
                      <td class="text-center">Lorem Ipsum</td>
                    </tr>
                    <tr>
                      <td class="text-center">May 2022</td>
                      <td class="text-center">Lorem Ipsum</td>
                      <td class="text-center">Lorem Ipsum</td>
                      <td class="text-center">Lorem Ipsum</td>
                      <td class="text-center">Lorem Ipsum</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
    {{-- end jasa overhoul --}}

    {{-- sparepart tahun ini dan tahun lalu --}}
    <div class="row">
      <div class="col-md-6">
        <div class="card-sparepart-planning__header-title g-5 g-xl-10 mb-3 mb-xl-3 mt-6">
          <div class="row">
            <div class="col-sm-12 col-custom">
              <h1>SPAREPARTS TAHUN INI (2022)</h1>
            </div>
          </div>
        </div>
        <div class="row tahun">
          <div class="col-md-12">
            <div class="tahun__type-one">
              @include('partials/widgets/cards/_widget-sparepart-tahun-type-one')
            </div>
          </div>
        </div>
        <div class="row tahun mt-3">
          <div class="col-md-6">
            <div class="tahun__type-two">
              @include('partials/widgets/cards/_widget-sparepart-tahun-type-two')
            </div>
          </div>
          <div class="col-md-6">
            <div class="tahun__type-three">
              @include('partials/widgets/cards/_widget-sparepart-tahun-type-three')
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="card-sparepart-planning__header-title g-5 g-xl-10 mb-3 mb-xl-3 mt-6">
          <div class="row">
            <div class="col-sm-12 col-custom">
              <h1>SPAREPARTS TAHUN LALU (2021)</h1>
            </div>
          </div>
        </div>
        <div class="row tahun">
          <div class="col-md-6">
            <div class="tahun__type-one">
              @include('partials/widgets/cards/_widget-sparepart-tahun-lalu-type-one')
            </div>
          </div>
          <div class="col-md-6">
            <div class="tahun__type-one">
              @include('partials/widgets/cards/_widget-sparepart-tahun-lalu-type-two')
            </div>
          </div>
        </div>
        <div class="row tahun mt-3">
          <div class="col-md-6">
            <div class="tahun__type-two">
              @include('partials/widgets/cards/_widget-sparepart-tahun-lalu-type-three')
            </div>
          </div>
          <div class="col-md-6">
            <div class="tahun__type-three">
              @include('partials/widgets/cards/_widget-sparepart-tahun-lalu-type-four')
            </div>
          </div>
        </div>
      </div>
    </div>
    {{-- end sparepart tahun ini dan tahun lalu --}}
  </section>




  <div class="row g-5 g-xl-10 mb-5 mb-xl-5">
    {{-- <div class="card-capex-report">
      <div class="row">
        <div class="col-sm-6 col-custom">
          <div>
            @include('partials/widgets/cards/_widget-capex-rencana-realisasi')
          </div>
          <div class="mt-2">
            @include('partials/widgets/cards/_widget-capex-progress-realisasi')
          </div>
        </div>
        <div class="col-sm-6 col-custom">
          @include('partials/widgets/cards/_widget-capex-detail-biaya-pemeliharaan')
        </div>
      </div>
    </div> --}}
  </div>



</x-default-layout>
<x-default-layout>
  <div class="py-lg-5">
    <div class="row">
      <div class="col-md-6">
        <div class="information">
          <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">
            Capex Report Summary</h1>
          <ul class="breadcrumb breadcrumb-separatorless ">
            <li class="breadcrumb-item text-muted">
              <a href="/" class="text-muted text-hover-primary">Maintenance</a>
            </li>
            <li class="breadcrumb-item text-muted">/</li>
            <li class="breadcrumb-item text-ghopo">Capex Report Summary</li>
          </ul>
        </div>
      </div>
      <div class="col-md-6">
        <div class="">
          <div class="row">
            <div class="col-md-3">
              <select class="form-select h-25" data-control="select2" data-placeholder="Pilih plant">
                <option></option>
                <option value="1">Option 1</option>
                <option value="2">Option 2</option>
              </select>
            </div>
            <div class="col-md-3">
              <select class="form-select h-25" data-control="select2" data-placeholder="Pilih Tahun">
                @foreach(getYear() as $key => $value)
                @if ($value == date("Y") )
                <option value="{{$value}}" selected>{{$value}}</option>
                @else
                <option value="{{$value}}">{{$value}}</option>
                @endif
                @endforeach
              </select>
            </div>
            <div class="col-md-3">
              <select class="form-select h-25" data-control="select2" data-placeholder="Pilih Bulan">
                @foreach(getMonth() as $key => $value)
                  @if ($value['month'] == date("n") )
                  <option value="{{$value['month']}}" selected>{{$value['month_name']}}</option>
                  @else
                  <option value="{{$value['month']}}">{{$value['month_name']}}</option>
                  @endif
                @endforeach
              </select>
            </div>
            <div class="col-md-3">
              <button class="btn btn-history w-100">Tampilkan</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


  <div class="row g-5 g-xl-10 mb-5 mb-xl-5">
    <div class="card-capex-report">
      <div class="row">
        <div class="col-sm-6 col-custom">
          <div>
            @include('partials/widgets/cards/_widget-capex-rencana-realisasi')
          </div>
          <div class="mt-2">
            @include('partials/widgets/cards/_widget-capex-progress-realisasi')
          </div>
        </div>
        <div class="col-sm-6 col-custom">
          @include('partials/widgets/cards/_widget-capex-detail-biaya-pemeliharaan')
        </div>
      </div>
    </div>
  </div>



</x-default-layout>
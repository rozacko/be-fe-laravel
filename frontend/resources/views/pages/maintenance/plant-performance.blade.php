<x-default-layout>
  <div class="flex-stack mb-0 mb-lg-n4 pt-5">
    <div class="row">
      <div class="col-sm-5">
        <div class="information">
          <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">Plant Performance</h1>
          <ul class="breadcrumb breadcrumb-separatorless ">
            <li class="breadcrumb-item text-muted">
              <a href="/" class="text-muted text-hover-primary">Maintenance</a>
            </li>
            <li class="breadcrumb-item text-muted">/</li>
            <li class="breadcrumb-item text-ghopo">Plant Performance</li>
          </ul>
        </div>
      </div>
      <div class="col-sm-7 d-flex justify-content-md-end flex-no-wrap px-5">
        <div class="filter">
          <div class="row">
            <div class="col mt-2">
              <select class="form-select h-25" data-control="select2" data-placeholder="Pilih Fasilitator">
                <option></option>
                <option value="1">Option 1</option>
                <option value="2">Option 2</option>
              </select>
            </div>
            <div class="col mt-2">
              <select class="form-select h-25" data-control="select2" data-placeholder="Pilih Bulan">
                @foreach(getMonth() as $key => $value)
                @if ($value['month'] == date("n") )
                <option value="{{$value['month']}}" selected>{{$value['month_name']}}</option>
                @else
                <option value="{{$value['month']}}">{{$value['month_name']}}</option>
                @endif
                @endforeach
              </select>
            </div>
            <div class="col mt-2">
              <select class="form-select h-25" data-control="select2" data-placeholder="Pilih Tahun">
                @foreach(getYear() as $key => $value)
                @if ($value == date("Y") )
                <option value="{{$value}}" selected>{{$value}}</option>
                @else
                <option value="{{$value}}">{{$value}}</option>
                @endif
                @endforeach
              </select>
            </div>
            <div class="col mt-2 card-breadcrumb-button ">
              <button class="btn btn-primary">Tampilkan</button>
            </div>
            <div class="col mt-2 card-breadcrumb-button">
              <button href="#" class="btn btn-add" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Tambah</button>
              <div class="menu menu-sub menu-sub-dropdown w-200px w-md-200px" data-kt-menu="true" id="kt_menu_63d780f5e6a12">
                <div class="separator border-gray-200"></div>
                <div class="">
                  <div class="text-left">
                    <button class="btn btn-sm btn-active-light-primary py-4 w-100" data-bs-toggle="modal" data-bs-target="#kt_modal_data_preparation" data-kt-menu-dismiss="true">Import Data Preparation</button>
                    <button class="btn btn-sm btn-active-light-primary py-4 w-100" data-bs-toggle="modal" data-bs-target="#kt_modal_add_data_process" data-kt-menu-dismiss="true">Import Data Activity</button>
                    <button class="btn btn-sm btn-active-light-primary py-4 w-100" data-bs-toggle="modal" data-bs-target="#kt_modal_create_app" data-kt-menu-dismiss="true">Add Data Proses</button>
                    <button class="btn btn-sm btn-active-light-primary py-4 w-100" data-bs-toggle="modal" data-bs-target="#kt_modal_setting_periode" data-kt-menu-dismiss="true">Setting Periode</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <div class="g-5 g-xl-10 mb-3 mt-10 mb-xl-3">
    <div class="card-maintenance-cost">
      <div class="row">
        <div class="col-sm-6 col-custom">
          @include('partials/widgets/cards/_widget-plant-performance-oee-kiln')
        </div>
        <div class="col-sm-6 col-custom">
          @include('partials/widgets/cards/_widget-plant-performance-avability-1-4')
        </div>
      </div>
    </div>
  </div>

  <div class="g-5 g-xl-10 mb-3 mt-5 mb-xl-3">
    <div class="card-maintenance-cost">
      <div class="row">
        <div class="col-sm-6 col-custom">
          @include('partials/widgets/cards/_widget-plant-performance-mtbf-kiln')
        </div>
        <div class="col-sm-6 col-custom">
          @include('partials/widgets/cards/_widget-plant-performance-downtime-1-4')
        </div>
      </div>
    </div>
  </div>

  <div class="g-5 g-xl-10 mb-3 mt-5 mb-xl-3">
    <div class="card-maintenance-cost">
      <div class="row">
        <div class="col-sm-6 col-custom">
          @include('partials/widgets/cards/_widget-plant-performance-pareto-durasi-upplant-downtime-kiln')
        </div>
        <div class="col-sm-6 col-custom">
          @include('partials/widgets/cards/_widget-plant-performance-pareto-frequensi-upplant-downtime-kiln')
        </div>
      </div>
    </div>
  </div>

  <div class="g-5 g-xl-10 mb-3 mt-5 mb-xl-3">
    <div class="card-maintenance-cost">
      <div class="row">
        @include('partials/widgets/cards/_widget-plant-performance-top-10-unit-kerja')
      </div>
    </div>
  </div>

  <!-- Available DownTime -->
  <div class="g-5 g-xl-10 mb-3 mt-5 mb-xl-3">
    <div class="card-maintenance-cost">
      <div class="col-sm-12">
        <div class="row">
          <div class="col-sm-8 col-custom">
            @include('partials/widgets/cards/_widget-plant-performance-available-downtime')
          </div>
          <div class="col-sm-4 col-custom">
            @include('partials/widgets/cards/_widget-plant-performance-backlog-order-type')
            @include('partials/widgets/cards/_widget-plant-performance-backlog-reason')
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="g-5 g-xl-10 mb-3 mb-xl-3 mt-6">
    <div class="card-dashboard-overhoul">
      <div class="row">
        <div class="col-sm-12">
          <div class="preparation">
            <h1>EVALUASI PENGELOLAAN NOTIFIKASI & MAINTENANCE ORDER GROUP OG HEAD PRODUCTION</h1>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- PERBANDINGAN PENGELOLAAN SAP PM SEMESTER 1 & SEMESTER 2 -->

  <div class="g-5 g-xl-10 mb-3 mt-5 mb-xl-3">
    <div class="card-maintenance-cost">
      <div class="col-sm-12">
        <div class="row">
          <div class="col-sm-4">
            @include('partials/widgets/cards/_widget-plant-performance-notification-aging')
          </div>
          <div class="col-sm-2">
            @include('partials/widgets/cards/_widget-plant-performance-notif-description-standart')
          </div>
          <div class="col-sm-2">
            @include('partials/widgets/cards/_widget-plant-performance-notif-maint-order')
          </div>
          <div class="col-sm-2">
            @include('partials/widgets/cards/_widget-plant-performance-notif-activity')
          </div>
          <div class="col-sm-2">
            @include('partials/widgets/cards/_widget-plant-performance-order-description-standart')
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- PERBANDINGAN PENGELOLAAN SAP PM SEMESTER 1 & SEMESTER 2 -->
  <div class="g-5 g-xl-10 mb-3 mt-5 mb-xl-3">
    <div class="card-maintenance-cost">
      <div class="col-sm-12">
        <div class="row">
          <div class="col-sm-4">
            @include('partials/widgets/cards/_widget-plant-performance-maintenance-aging')
          </div>
          <div class="col-sm-2">
            @include('partials/widgets/cards/_widget-plant-performance-notif-task-list-standart')
          </div>
          <div class="col-sm-2">
            @include('partials/widgets/cards/_widget-plant-performance-notif-duration')
          </div>
          <div class="col-sm-2">
            @include('partials/widgets/cards/_widget-plant-performance-notif-confirmation')
          </div>
          <div class="col-sm-2">
            @include('partials/widgets/cards/_widget-plant-performance-notif-actual-tanggal-kerja')
          </div>
        </div>
      </div>
    </div>
  </div>


  <!-- PERBANDINGAN PENGELOLAAN SAP PM SEMESTER 1 & SEMESTER 2 -->
  <div class="g-5 g-xl-10 mb-3 mt-5 mb-xl-3">
    <div class="card-maintenance-cost">
      <div class="col-sm-12">
        @include('partials/widgets/cards/_widget-plant-performance-perbandingan-pengelolaan')
      </div>
    </div>
  </div>

  <!-- Biaya Pemeliharan -->
  <div class="g-5 g-xl-10 mb-3 mt-5 mb-xl-3">
    <div class="card-maintenance-cost">
      <div class="col-sm-12">
        @include('partials/widgets/cards/_widget-plant-performance-biaya-pemeliharaan')
      </div>
    </div>
  </div>


</x-default-layout>
<x-default-layout>
  <div class="py-lg-5">
    <div class="row">
      <div class="col-md-6">
        <div class="information">
          <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">
            Production Asset Management</h1>
          <ul class="breadcrumb breadcrumb-separatorless ">
            <li class="breadcrumb-item text-muted">
              <a href="/" class="text-muted text-hover-primary">Maintenance</a>
            </li>
            <li class="breadcrumb-item text-muted">/</li>
            <li class="breadcrumb-item text-ghopo">Production Asset Management</li>
          </ul>
        </div>
      </div>
      <div class="col-md-6">
      </div>
    </div>
  </div>

  <div class="card h-md-100 mt-2">
    <div class="card-body pt-2">
      <ul class="nav nav-pills nav-pills-custom border-bottom">
        <li class="nav-item">
          <a class="nav-link border-0 rounded-0 d-flex justify-content-between flex-column flex-center overflow-hidden p-4 active"
            data-bs-toggle="pill" href="#kt_stats_refurbishment">
            <span class="nav-text text-gray-700 fw-bold fs-6 text-uppercase lh-1">Refurbishment</span>
            <span class="bullet-custom position-absolute bottom-0 w-100 h-2px bg-ghopo-primary"></span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link border-0 rounded-0 d-flex justify-content-between flex-column flex-center overflow-hidden p-4"
            data-bs-toggle="pill" href="#kt_stats_material_uitval">
            <span class="nav-text text-gray-700 fw-bold fs-6 text-uppercase lh-1">Material Uitval</span>
            <span class="bullet-custom position-absolute bottom-0 w-100 h-2px bg-ghopo-primary"></span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link border-0 rounded-0 d-flex justify-content-between flex-column flex-center overflow-hidden p-4"
            data-bs-toggle="pill" href="#kt_stats_master_data_aset">
            <span class="nav-text text-gray-700 fw-bold fs-6 text-uppercase lh-1">Master data Aset</span>
            <span class="bullet-custom position-absolute bottom-0 w-100 h-2px bg-ghopo-primary"></span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link border-0 rounded-0 d-flex justify-content-between flex-column flex-center overflow-hidden p-4"
            data-bs-toggle="pill" href="#kt_stats_asset_under_construction">
            <span class="nav-text text-gray-700 fw-bold fs-6 text-uppercase lh-1">Asset under construction</span>
            <span class="bullet-custom position-absolute bottom-0 w-100 h-2px bg-ghopo-primary"></span>
          </a>
        </li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane fade show active card-progres-capex-prioritas" id="kt_stats_refurbishment">
          <div class="py-lg-5">
            <div class="row">
              <div class="col-md-6">

              </div>
              <div class="col-md-6">
                <div class="">
                  <div class="row">
                    <div class="col-md-3">
                      <select class="form-select h-25" data-control="select2" data-placeholder="Pilih plant">
                        <option></option>
                        <option value="1">Option 1</option>
                        <option value="2">Option 2</option>
                      </select>
                    </div>
                    <div class="col-md-3">
                      <select class="form-select h-25" data-control="select2" data-placeholder="Pilih Tahun">
                        @foreach(getYear() as $key => $value)
                        @if ($value == date("Y") )
                        <option value="{{$value}}" selected>{{$value}}</option>
                        @else
                        <option value="{{$value}}">{{$value}}</option>
                        @endif
                        @endforeach
                      </select>
                    </div>
                    <div class="col-md-3">
                      <select class="form-select h-25" data-control="select2" data-placeholder="Pilih Bulan">
                        @foreach(getMonth() as $key => $value)
                        @if ($value['month'] == date("n") )
                        <option value="{{$value['month']}}" selected>{{$value['month_name']}}
                        </option>
                        @else
                        <option value="{{$value['month']}}">{{$value['month_name']}}</option>
                        @endif
                        @endforeach
                      </select>
                    </div>
                    <div class="col-md-3">
                      <button class="btn btn-history w-100">Tampilkan</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <section class="card-line-refurbishment mb-5">
            <div class="row">
              <div class="col-md-12">
                <label for="" class="font-weight-bold text-uppercase">Time Line Ware House Of Refurbishment</label>
              </div>
            </div>
            <div class="row ">
              <div class="col-md-12">
                <div class="table-responsive">
                  <table class="table table-striped data-table" id="tb_progress-item-capex">
                    <thead>
                      <tr>
                        <th rowspan="3" class="align-middle text-center color-header-tabel">No</th>
                        <th rowspan="3" class="align-middle text-center color-header-tabel">ACTIVITY</th>
                        <th colspan="12" class="align-middle text-center color-header-tabel">
                          Tahun 2022 </th>
                        <th rowspan="3" class="align-middle text-center color-header-tabel">PROGRESS</th>
                      </tr>
                      <tr>
                        <th class="align-middle text-center color-header-tabel">JAN</th>
                        <th class="align-middle text-center color-header-tabel">FEB</th>
                        <th class="align-middle text-center color-header-tabel">MAR</th>
                        <th class="align-middle text-center color-header-tabel">APR</th>
                        <th class="align-middle text-center color-header-tabel">MEI</th>
                        <th class="align-middle text-center color-header-tabel">JUN</th>
                        <th class="align-middle text-center color-header-tabel">JUL</th>
                        <th class="align-middle text-center color-header-tabel">AGU</th>
                        <th class="align-middle text-center color-header-tabel">SEP</th>
                        <th class="align-middle text-center color-header-tabel">OKT</th>
                        <th class="align-middle text-center color-header-tabel">NOV</th>
                        <th class="align-middle text-center color-header-tabel">DES</th>
                      </tr>
                      <tr>
                        <th class="mb-0 p-0">
                          <table class="table table-bordered data-table mb-0">
                            <tr>
                              <td>1</td>
                              <td>2</td>
                              <td>3</td>
                              <td>4</td>
                            </tr>
                          </table>
                        </th>
                        <th class="mb-0 p-0">
                          <table class="table table-bordered data-table mb-0">
                            <tr>
                              <td>1</td>
                              <td>2</td>
                              <td>3</td>
                              <td>4</td>
                            </tr>
                          </table>
                        </th>
                        <th class="mb-0 p-0">
                          <table class="table table-bordered data-table mb-0">
                            <tr>
                              <td>1</td>
                              <td>2</td>
                              <td>3</td>
                              <td>4</td>
                            </tr>
                          </table>
                        </th>
                        <th class="mb-0 p-0">
                          <table class="table table-bordered data-table mb-0">
                            <tr>
                              <td>1</td>
                              <td>2</td>
                              <td>3</td>
                              <td>4</td>
                            </tr>
                          </table>
                        </th>
                        <th class="mb-0 p-0">
                          <table class="table table-bordered data-table mb-0">
                            <tr>
                              <td>1</td>
                              <td>2</td>
                              <td>3</td>
                              <td>4</td>
                            </tr>
                          </table>
                        </th>
                        <th class="mb-0 p-0">
                          <table class="table table-bordered data-table mb-0">
                            <tr>
                              <td>1</td>
                              <td>2</td>
                              <td>3</td>
                              <td>4</td>
                            </tr>
                          </table>
                        </th>
                        <th class="mb-0 p-0">
                          <table class="table table-bordered data-table mb-0">
                            <tr>
                              <td>1</td>
                              <td>2</td>
                              <td>3</td>
                              <td>4</td>
                            </tr>
                          </table>
                        </th>
                        <th class="mb-0 p-0">
                          <table class="table table-bordered data-table mb-0">
                            <tr>
                              <td>1</td>
                              <td>2</td>
                              <td>3</td>
                              <td>4</td>
                            </tr>
                          </table>
                        </th>
                        <th class="mb-0 p-0">
                          <table class="table table-bordered data-table mb-0">
                            <tr>
                              <td>1</td>
                              <td>2</td>
                              <td>3</td>
                              <td>4</td>
                            </tr>
                          </table>
                        </th>
                        <th class="mb-0 p-0">
                          <table class="table table-bordered data-table mb-0">
                            <tr>
                              <td>1</td>
                              <td>2</td>
                              <td>3</td>
                              <td>4</td>
                            </tr>
                          </table>
                        </th>
                        <th class="mb-0 p-0">
                          <table class="table table-bordered data-table mb-0">
                            <tr>
                              <td>1</td>
                              <td>2</td>
                              <td>3</td>
                              <td>4</td>
                            </tr>
                          </table>
                        </th>
                        <th class="mb-0 p-0">
                          <table class="table table-bordered data-table mb-0">
                            <tr>
                              <td>1</td>
                              <td>2</td>
                              <td>3</td>
                              <td>4</td>
                            </tr>
                          </table>
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th class="align-middle text-center color-header-tabel">1</th>
                        <th class="align-middle text-left">SOSIALISASI</th>
                        <th class="mb-0 p-0" colspan=>
                          <table class="table data-table mb-0">
                            <tr>
                              <td class="bg-blue-1 custom-heigh-table"></td>
                              <td class="bg-blue-1 custom-heigh-table"></td>
                              <td class="bg-blue-1 custom-heigh-table"></td>
                              <td class="bg-blue-1 custom-heigh-table"></td>
                            </tr>
                          </table>
                        </th>
                        <th colspan="11"></th>
                        <th>CLOSE</th>
                      </tr>
                      <tr>
                        <th class="align-middle text-center color-header-tabel">2</th>
                        <th class="align-middle text-left ">GOLIVE</th>
                        <th></th>
                        <th class="mb-0 p-0">
                          <table class="table data-table mb-0">
                            <tr>
                              <td class="bg-blue-1 custom-heigh-table"></td>
                              <td class="bg-blue-1 custom-heigh-table"></td>
                              <td class="bg-blue-1 custom-heigh-table"></td>
                              <td class="bg-blue-1 custom-heigh-table"></td>
                            </tr>
                          </table>
                        </th>
                        <th colspan="10"></th>
                        <th>CLOSE</th>
                      </tr>
                      <tr>
                        <th class="align-middle text-center color-header-tabel">2</th>
                        <th class="align-middle text-left ">PROSES & OUT SPART PART</th>
                        <th></th>
                        <th></th>
                        <th class="mb-0 p-0">
                          <table class="table data-table mb-0">
                            <tr>
                              <td class="bg-blue-1 custom-heigh-table"></td>
                              <td class="bg-blue-1 custom-heigh-table"></td>
                              <td class="bg-blue-1 custom-heigh-table"></td>
                              <td class="bg-blue-1 custom-heigh-table"></td>
                            </tr>
                          </table>
                        </th>
                        <th class="mb-0 p-0">
                          <table class="table data-table mb-0">
                            <tr>
                              <td class="bg-blue-1 custom-heigh-table"></td>
                              <td class="bg-blue-1 custom-heigh-table"></td>
                              <td class="bg-blue-1 custom-heigh-table"></td>
                              <td class="bg-blue-1 custom-heigh-table"></td>
                            </tr>
                          </table>
                        </th>
                        <th class="mb-0 p-0">
                          <table class="table data-table mb-0">
                            <tr>
                              <td class="bg-blue-1 custom-heigh-table"></td>
                              <td class="bg-blue-1 custom-heigh-table"></td>
                              <td class="bg-blue-1 custom-heigh-table"></td>
                              <td class="bg-blue-1 custom-heigh-table"></td>
                            </tr>
                          </table>
                        </th>
                        <th class="mb-0 p-0">
                          <table class="table data-table mb-0">
                            <tr>
                              <td class="bg-blue-1 custom-heigh-table"></td>
                              <td class="bg-blue-1 custom-heigh-table"></td>
                              <td class="bg-blue-1 custom-heigh-table"></td>
                              <td class="bg-blue-1 custom-heigh-table"></td>
                            </tr>
                          </table>
                        </th>
                        <th class="mb-0 p-0">
                          <table class="table data-table mb-0">
                            <tr>
                              <td class="bg-blue-1 custom-heigh-table"></td>
                              <td class="bg-blue-1 custom-heigh-table"></td>
                              <td class="bg-blue-1 custom-heigh-table"></td>
                              <td class="bg-blue-1 custom-heigh-table"></td>
                            </tr>
                          </table>
                        </th>
                        <th class="mb-0 p-0">
                          <table class="table data-table mb-0">
                            <tr>
                              <td class="bg-blue-1 custom-heigh-table"></td>
                              <td class="bg-blue-1 custom-heigh-table"></td>
                              <td class="bg-blue-1 custom-heigh-table"></td>
                              <td class="bg-blue-1 custom-heigh-table"></td>
                            </tr>
                          </table>
                        </th>
                        <th class="mb-0 p-0">
                          <table class="table data-table mb-0">
                            <tr>
                              <td class="bg-blue-1 custom-heigh-table"></td>
                              <td class="bg-blue-1 custom-heigh-table"></td>
                              <td class="bg-blue-1 custom-heigh-table"></td>
                              <td class="bg-blue-1 custom-heigh-table"></td>
                            </tr>
                          </table>
                        </th>
                        <th class="mb-0 p-0">
                          <table class="table data-table mb-0">
                            <tr>
                              <td class="bg-blue-1 custom-heigh-table"></td>
                              <td class="bg-blue-1 custom-heigh-table"></td>
                              <td class="bg-blue-1 custom-heigh-table"></td>
                              <td class="bg-blue-1 custom-heigh-table"></td>
                            </tr>
                          </table>
                        </th>
                        <th class="mb-0 p-0">
                          <table class="table data-table mb-0">
                            <tr>
                              <td class="bg-blue-1 custom-heigh-table"></td>
                              <td class="bg-blue-1 custom-heigh-table"></td>
                              <td class="bg-blue-1 custom-heigh-table"></td>
                              <td class="bg-blue-1 custom-heigh-table"></td>
                            </tr>
                          </table>
                        </th>
                        <th class="mb-0 p-0">
                          <table class="table data-table mb-0">
                            <tr>
                              <td class="bg-blue-1 custom-heigh-table"></td>
                              <td class="bg-blue-1 custom-heigh-table"></td>
                              <td class="bg-blue-1 custom-heigh-table"></td>
                              <td class="bg-blue-1 custom-heigh-table"></td>
                            </tr>
                          </table>
                        </th>
                        <th class="align-middle text-left ">PROCESS</th>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>

          </section>

          <section class="card-reducer-refurbishment mb-5">
            <div class="row">
              <div class="col-md-12">
                <label for="" class="font-weight-bold text-uppercase">Data Reducer Ware House Of Refurbishment</label>
              </div>
            </div>
            <div class="row card-reducer-refurbishment">
              <div class="col-md-12">
                <div class="table-responsive">
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th colspan="12" class="align-middle text-center color-header-tabel">
                          RPM</th>
                      </tr>
                      <tr>
                        <th class="align-middle text-center color-header-tabel">NO</th>
                        <th class="align-middle text-center color-header-tabel">EQUIPMENT</th>
                        <th class="align-middle text-center color-header-tabel">TYPE/MODEL</th>
                        <th class="align-middle text-center color-header-tabel">POWER</th>
                        <th class="align-middle text-center color-header-tabel">N1</th>
                        <th class="align-middle text-center color-header-tabel">N2</th>
                        <th class="align-middle text-center color-header-tabel">RATIO (i)</th>
                        <th class="align-middle text-center color-header-tabel">MANUFACTURE</th>
                        <th class="align-middle text-center color-header-tabel">KONDISI</th>
                        <th class="align-middle text-center color-header-tabel">EX. SECTION</th>
                        <th class="align-middle text-center color-header-tabel">KETERANGAN</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th class="align-middle text-center color-header-tabel">1</th>
                        <th class="align-middle text-center color-header-tabel">624-6 BE1</th>
                        <th class="align-middle text-center color-header-tabel">B3 SH 11 D</th>
                        <th class="align-middle text-center color-header-tabel">123</th>
                        <th class="align-middle text-center color-header-tabel">994</th>
                        <th class="align-middle text-center color-header-tabel">738</th>
                        <th class="align-middle text-center color-header-tabel">23</th>
                        <th class="align-middle text-center color-header-tabel">FLENDER</th>
                        <th class="align-middle text-center color-header-tabel">READY</th>
                        <th class="align-middle text-center color-header-tabel">PM PACKER</th>
                        <th class="align-middle text-center color-header-tabel">-</th>

                      </tr>
                      <tr>
                        <th class="align-middle text-center color-header-tabel">2</th>
                        <th class="align-middle text-center color-header-tabel">624-6 BE1</th>
                        <th class="align-middle text-center color-header-tabel">B3 SH 11 D</th>
                        <th class="align-middle text-center color-header-tabel">123</th>
                        <th class="align-middle text-center color-header-tabel">994</th>
                        <th class="align-middle text-center color-header-tabel">738</th>
                        <th class="align-middle text-center color-header-tabel">23</th>
                        <th class="align-middle text-center color-header-tabel">FLENDER</th>
                        <th class="align-middle text-center color-header-tabel">READY</th>
                        <th class="align-middle text-center color-header-tabel">PM PACKER</th>
                        <th class="align-middle text-center color-header-tabel">-</th>

                      </tr>
                      <tr>
                        <th class="align-middle text-center color-header-tabel">3</th>
                        <th class="align-middle text-center color-header-tabel">624-6 BE1</th>
                        <th class="align-middle text-center color-header-tabel">B3 SH 11 D</th>
                        <th class="align-middle text-center color-header-tabel">123</th>
                        <th class="align-middle text-center color-header-tabel">994</th>
                        <th class="align-middle text-center color-header-tabel">738</th>
                        <th class="align-middle text-center color-header-tabel">23</th>
                        <th class="align-middle text-center color-header-tabel">FLENDER</th>
                        <th class="align-middle text-center color-header-tabel">READY</th>
                        <th class="align-middle text-center color-header-tabel">PM PACKER</th>
                        <th class="align-middle text-center color-header-tabel">-</th>

                      </tr>
                      <tr>
                        <th class="align-middle text-center color-header-tabel">4</th>
                        <th class="align-middle text-center color-header-tabel">624-6 BE1</th>
                        <th class="align-middle text-center color-header-tabel">B3 SH 11 D</th>
                        <th class="align-middle text-center color-header-tabel">123</th>
                        <th class="align-middle text-center color-header-tabel">994</th>
                        <th class="align-middle text-center color-header-tabel">738</th>
                        <th class="align-middle text-center color-header-tabel">23</th>
                        <th class="align-middle text-center color-header-tabel">FLENDER</th>
                        <th class="align-middle text-center color-header-tabel">READY</th>
                        <th class="align-middle text-center color-header-tabel">PM PACKER</th>
                        <th class="align-middle text-center color-header-tabel">-</th>

                      </tr>
                      <tr>
                        <th class="align-middle text-center color-header-tabel">5</th>
                        <th class="align-middle text-center color-header-tabel">624-6 BE1</th>
                        <th class="align-middle text-center color-header-tabel">B3 SH 11 D</th>
                        <th class="align-middle text-center color-header-tabel">123</th>
                        <th class="align-middle text-center color-header-tabel">994</th>
                        <th class="align-middle text-center color-header-tabel">738</th>
                        <th class="align-middle text-center color-header-tabel">23</th>
                        <th class="align-middle text-center color-header-tabel">FLENDER</th>
                        <th class="align-middle text-center color-header-tabel">READY</th>
                        <th class="align-middle text-center color-header-tabel">PM PACKER</th>
                        <th class="align-middle text-center color-header-tabel">-</th>

                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </section>

          <section class="card-listrik-refurbishment">
            <div class="row">
              <div class="col-md-12">
                <label for="" class="font-weight-bold text-uppercase">Data Motor Listrik Ware House Of
                  Refurbishment</label>
              </div>
            </div>
            <div class="row card-reducer-refurbishment">
              <div class="col-md-12">
                <div class="table-responsive">
                  <table class="table table-striped">
                    <thead>

                      <tr>
                        <th class="align-middle text-center color-header-tabel">NO</th>
                        <th class="align-middle text-center color-header-tabel">MERK/ MANUFAKTUR</th>
                        <th class="align-middle text-center color-header-tabel">FRAME</th>
                        <th class="align-middle text-center color-header-tabel">NO. SERI</th>
                        <th class="align-middle text-center color-header-tabel">POWER</th>
                        <th class="align-middle text-center color-header-tabel">TEGANGAN</th>
                        <th class="align-middle text-center color-header-tabel">ARUS</th>
                        <th class="align-middle text-center color-header-tabel">RPM</th>
                        <th class="align-middle text-center color-header-tabel">NAME PLATE</th>
                        <th class="align-middle text-center color-header-tabel">EX. SECTION</th>
                        <th class="align-middle text-center color-header-tabel">KETERANGAN</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th class="align-middle text-center color-header-tabel">1</th>
                        <th class="align-middle text-center color-header-tabel">624-6 BE1</th>
                        <th class="align-middle text-center color-header-tabel">B3 SH 11 D</th>
                        <th class="align-middle text-center color-header-tabel">123</th>
                        <th class="align-middle text-center color-header-tabel">994</th>
                        <th class="align-middle text-center color-header-tabel">738</th>
                        <th class="align-middle text-center color-header-tabel">23</th>
                        <th class="align-middle text-center color-header-tabel">FLENDER</th>
                        <th class="align-middle text-center color-header-tabel">READY</th>
                        <th class="align-middle text-center color-header-tabel">PM PACKER</th>
                        <th class="align-middle text-center color-header-tabel">-</th>

                      </tr>
                      <tr>
                        <th class="align-middle text-center color-header-tabel">2</th>
                        <th class="align-middle text-center color-header-tabel">624-6 BE1</th>
                        <th class="align-middle text-center color-header-tabel">B3 SH 11 D</th>
                        <th class="align-middle text-center color-header-tabel">123</th>
                        <th class="align-middle text-center color-header-tabel">994</th>
                        <th class="align-middle text-center color-header-tabel">738</th>
                        <th class="align-middle text-center color-header-tabel">23</th>
                        <th class="align-middle text-center color-header-tabel">FLENDER</th>
                        <th class="align-middle text-center color-header-tabel">READY</th>
                        <th class="align-middle text-center color-header-tabel">PM PACKER</th>
                        <th class="align-middle text-center color-header-tabel">-</th>

                      </tr>
                      <tr>
                        <th class="align-middle text-center color-header-tabel">3</th>
                        <th class="align-middle text-center color-header-tabel">624-6 BE1</th>
                        <th class="align-middle text-center color-header-tabel">B3 SH 11 D</th>
                        <th class="align-middle text-center color-header-tabel">123</th>
                        <th class="align-middle text-center color-header-tabel">994</th>
                        <th class="align-middle text-center color-header-tabel">738</th>
                        <th class="align-middle text-center color-header-tabel">23</th>
                        <th class="align-middle text-center color-header-tabel">FLENDER</th>
                        <th class="align-middle text-center color-header-tabel">READY</th>
                        <th class="align-middle text-center color-header-tabel">PM PACKER</th>
                        <th class="align-middle text-center color-header-tabel">-</th>

                      </tr>
                      <tr>
                        <th class="align-middle text-center color-header-tabel">4</th>
                        <th class="align-middle text-center color-header-tabel">624-6 BE1</th>
                        <th class="align-middle text-center color-header-tabel">B3 SH 11 D</th>
                        <th class="align-middle text-center color-header-tabel">123</th>
                        <th class="align-middle text-center color-header-tabel">994</th>
                        <th class="align-middle text-center color-header-tabel">738</th>
                        <th class="align-middle text-center color-header-tabel">23</th>
                        <th class="align-middle text-center color-header-tabel">FLENDER</th>
                        <th class="align-middle text-center color-header-tabel">READY</th>
                        <th class="align-middle text-center color-header-tabel">PM PACKER</th>
                        <th class="align-middle text-center color-header-tabel">-</th>

                      </tr>
                      <tr>
                        <th class="align-middle text-center color-header-tabel">5</th>
                        <th class="align-middle text-center color-header-tabel">624-6 BE1</th>
                        <th class="align-middle text-center color-header-tabel">B3 SH 11 D</th>
                        <th class="align-middle text-center color-header-tabel">123</th>
                        <th class="align-middle text-center color-header-tabel">994</th>
                        <th class="align-middle text-center color-header-tabel">738</th>
                        <th class="align-middle text-center color-header-tabel">23</th>
                        <th class="align-middle text-center color-header-tabel">FLENDER</th>
                        <th class="align-middle text-center color-header-tabel">READY</th>
                        <th class="align-middle text-center color-header-tabel">PM PACKER</th>
                        <th class="align-middle text-center color-header-tabel">-</th>

                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </section>
        </div>
        <div class="tab-pane fade card-progres-capex-prioritas" id="kt_stats_material_uitval">
          <div class="py-lg-5">
            <div class="row">
              <div class="col-md-6">

              </div>
              <div class="col-md-6">
                <div class="">
                  <div class="row">
                    <div class="col-md-3">
                      <select class="form-select h-25" data-control="select2" data-placeholder="Pilih plant">
                        <option></option>
                        <option value="1">Option 1</option>
                        <option value="2">Option 2</option>
                      </select>
                    </div>
                    <div class="col-md-3">
                      <select class="form-select h-25" data-control="select2" data-placeholder="Pilih Tahun">
                        @foreach(getYear() as $key => $value)
                        @if ($value == date("Y") )
                        <option value="{{$value}}" selected>{{$value}}</option>
                        @else
                        <option value="{{$value}}">{{$value}}</option>
                        @endif
                        @endforeach
                      </select>
                    </div>
                    <div class="col-md-3">
                      <select class="form-select h-25" data-control="select2" data-placeholder="Pilih Bulan">
                        @foreach(getMonth() as $key => $value)
                        @if ($value['month'] == date("n") )
                        <option value="{{$value['month']}}" selected>{{$value['month_name']}}
                        </option>
                        @else
                        <option value="{{$value['month']}}">{{$value['month_name']}}</option>
                        @endif
                        @endforeach
                      </select>
                    </div>
                    <div class="col-md-3">
                      <button class="btn btn-history w-100">Tampilkan</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <label for="" class="font-weight-bold text-uppercase">Material Uitval MC.9</label>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="table-responsive">
                <table class="table table-striped data-table" id="tb_progress-item-capex">
                  <thead>
                    <tr>
                      <th rowspan="2" class="align-middle text-center color-header-tabel">No</th>
                      <th rowspan="2" class="align-middle text-center color-header-tabel">ACTIVITY</th>
                      <th colspan="12" class="align-middle text-center color-header-tabel border-bottom">
                        Tahun 2022</th>
                    </tr>
                    <tr class="border-bottom">
                      <th class="align-middle text-center color-header-tabel">JAN</th>
                      <th class="align-middle text-center color-header-tabel">FEB</th>
                      <th class="align-middle text-center color-header-tabel">MAR</th>
                      <th class="align-middle text-center color-header-tabel">APR</th>
                      <th class="align-middle text-center color-header-tabel">MEI</th>
                      <th class="align-middle text-center color-header-tabel">JUN</th>
                      <th class="align-middle text-center color-header-tabel">JUL</th>
                      <th class="align-middle text-center color-header-tabel">AGU</th>
                      <th class="align-middle text-center color-header-tabel">SEP</th>
                      <th class="align-middle text-center color-header-tabel">OKT</th>
                      <th class="align-middle text-center color-header-tabel">NOV</th>
                      <th class="align-middle text-center color-header-tabel">DES</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th class="align-middle text-center color-header-tabel">1</th>
                      <th class="align-middle text-left color-header-tabel text-uppercase">TOTAL NILAI UTIVAL</th>
                      <th class="align-middle text-center color-header-tabel">50364</th>
                      <th class="align-middle text-center color-header-tabel">20796</th>
                      <th class="align-middle text-center color-header-tabel">20796</th>
                      <th class="align-middle text-center color-header-tabel">20796</th>
                      <th class="align-middle text-center color-header-tabel">20796</th>
                      <th class="align-middle text-center color-header-tabel">20796</th>
                      <th class="align-middle text-center color-header-tabel">20796</th>
                      <th class="align-middle text-center color-header-tabel">20796</th>
                      <th class="align-middle text-center color-header-tabel">20796</th>
                      <th class="align-middle text-center color-header-tabel">20796</th>
                      <th class="align-middle text-center color-header-tabel">20796</th>
                      <th class="align-middle text-center color-header-tabel">20796</th>
                    </tr>
                    <tr>
                      <th class="align-middle text-center color-header-tabel">2</th>
                      <th class="align-middle text-left color-header-tabel text-uppercase">PEMANFAATAN SPART PART UTIVAL
                        BY GI</th>
                      <th class="align-middle text-center color-header-tabel">50364</th>
                      <th class="align-middle text-center color-header-tabel">20796</th>
                      <th class="align-middle text-center color-header-tabel">20796</th>
                      <th class="align-middle text-center color-header-tabel">20796</th>
                      <th class="align-middle text-center color-header-tabel">20796</th>
                      <th class="align-middle text-center color-header-tabel">20796</th>
                      <th class="align-middle text-center color-header-tabel">20796</th>
                      <th class="align-middle text-center color-header-tabel">20796</th>
                      <th class="align-middle text-center color-header-tabel">20796</th>
                      <th class="align-middle text-center color-header-tabel">20796</th>
                      <th class="align-middle text-center color-header-tabel">20796</th>
                      <th class="align-middle text-center color-header-tabel">20796</th>
                    </tr>
                    <tr>
                      <th class="align-middle text-center color-header-tabel">3</th>
                      <th class="align-middle text-left color-header-tabel text-uppercase">UTIVAL AJUAN BARU</th>
                      <th class="align-middle text-center color-header-tabel">50364</th>
                      <th class="align-middle text-center color-header-tabel">20796</th>
                      <th class="align-middle text-center color-header-tabel">20796</th>
                      <th class="align-middle text-center color-header-tabel">20796</th>
                      <th class="align-middle text-center color-header-tabel">20796</th>
                      <th class="align-middle text-center color-header-tabel">20796</th>
                      <th class="align-middle text-center color-header-tabel">20796</th>
                      <th class="align-middle text-center color-header-tabel">20796</th>
                      <th class="align-middle text-center color-header-tabel">20796</th>
                      <th class="align-middle text-center color-header-tabel">20796</th>
                      <th class="align-middle text-center color-header-tabel">20796</th>
                      <th class="align-middle text-center color-header-tabel">20796</th>
                    </tr>
                    <tr>
                      <th class="align-middle text-center color-header-tabel">4</th>
                      <th class="align-middle text-left color-header-tabel text-uppercase">PEMANFAATAN SPART PART UTIVAL
                        BY WRITE OFF</th>
                      <th class="align-middle text-center color-header-tabel">50364</th>
                      <th class="align-middle text-center color-header-tabel">20796</th>
                      <th class="align-middle text-center color-header-tabel">20796</th>
                      <th class="align-middle text-center color-header-tabel">20796</th>
                      <th class="align-middle text-center color-header-tabel">20796</th>
                      <th class="align-middle text-center color-header-tabel">20796</th>
                      <th class="align-middle text-center color-header-tabel">20796</th>
                      <th class="align-middle text-center color-header-tabel">20796</th>
                      <th class="align-middle text-center color-header-tabel">20796</th>
                      <th class="align-middle text-center color-header-tabel">20796</th>
                      <th class="align-middle text-center color-header-tabel">20796</th>
                      <th class="align-middle text-center color-header-tabel">20796</th>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <div class="tab-pane fade card-progres-capex-prioritas" id="kt_stats_master_data_aset">
          <div class="py-lg-5">
            <div class="row">
              <div class="col-md-6">

              </div>
              <div class="col-md-6">
                <div class="">
                  <div class="row">
                    <div class="col-md-3">
                      <select class="form-select h-25" data-control="select2" data-placeholder="Pilih plant">
                        <option></option>
                        <option value="1">Option 1</option>
                        <option value="2">Option 2</option>
                      </select>
                    </div>
                    <div class="col-md-3">
                      <select class="form-select h-25" data-control="select2" data-placeholder="Pilih Tahun">
                        @foreach(getYear() as $key => $value)
                        @if ($value == date("Y") )
                        <option value="{{$value}}" selected>{{$value}}</option>
                        @else
                        <option value="{{$value}}">{{$value}}</option>
                        @endif
                        @endforeach
                      </select>
                    </div>
                    <div class="col-md-3">
                      <select class="form-select h-25" data-control="select2" data-placeholder="Pilih Bulan">
                        @foreach(getMonth() as $key => $value)
                        @if ($value['month'] == date("n") )
                        <option value="{{$value['month']}}" selected>{{$value['month_name']}}
                        </option>
                        @else
                        <option value="{{$value['month']}}">{{$value['month_name']}}</option>
                        @endif
                        @endforeach
                      </select>
                    </div>
                    <div class="col-md-3">
                      <button class="btn btn-history w-100">Tampilkan</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <label for="" class="font-weight-bold text-uppercase">Master data Aset Mesin Produksi</label>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="card">
                @include('partials/widgets/cards/_widget-asset-prod-quantity')
              </div>
            </div>
            <div class="col-md-6">
              <div class="card">
                @include('partials/widgets/cards/_widget-asset-prod-aquisition')
              </div>
            </div>
          </div>
          <div class="row mt-4">
            <div class="col-md-12">
              <label for="" class="font-weight-bold text-uppercase">Master data Aset Bangunan Produksi</label>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="card">
                <div class="table-responsive p-2">
                  <table class="table table-striped data-table" id="tb_progress-item-capex">
                    <thead>
                      <tr class="border-bottom">
                        <th class="align-middle text-center color-header-tabel">Tahun Kapitalis</th>
                        <th class="align-middle text-center color-header-tabel">Jumlah Asset Bangunan</th>
                        <th class="align-middle text-center color-header-tabel">Nilai Asset Bangunan (M)</th>
                    </thead>
                    <tbody>
                      <tr>
                        <th class="align-middle text-center color-header-tabel">2012</th>
                        <th class="align-middle text-left color-header-tabel text-uppercase">GENERAL ELECTRIC</th>
                        <th class="align-middle text-center color-header-tabel">CAGN-900</th>
                      </tr>
                      <tr>
                        <th class="align-middle text-center color-header-tabel">2013</th>
                        <th class="align-middle text-left color-header-tabel text-uppercase">GENERAL ELECTRIC</th>
                        <th class="align-middle text-center color-header-tabel">CAGN-900</th>
                      </tr>
                      <tr>
                        <th class="align-middle text-center color-header-tabel">2014</th>
                        <th class="align-middle text-left color-header-tabel text-uppercase">GENERAL ELECTRIC</th>
                        <th class="align-middle text-center color-header-tabel">CAGN-900</th>
                      </tr>
                      <tr>
                        <th class="align-middle text-center color-header-tabel">2015</th>
                        <th class="align-middle text-left color-header-tabel text-uppercase">GENERAL ELECTRIC</th>
                        <th class="align-middle text-center color-header-tabel">CAGN-900</th>
                      </tr>
                      <tr>
                        <th class="align-middle text-center color-header-tabel">2016</th>
                        <th class="align-middle text-left color-header-tabel text-uppercase">GENERAL ELECTRIC</th>
                        <th class="align-middle text-center color-header-tabel">CAGN-900</th>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card">
                @include('partials/widgets/cards/_widget-asset-prod-aquisition-two')
              </div>
            </div>
          </div>
        </div>
        <div class="tab-pane fade card-progres-capex-prioritas" id="kt_stats_asset_under_construction">
          <div class="py-lg-5">
            <div class="row">
              <div class="col-md-6">

              </div>
              <div class="col-md-6">
                <div class="">
                  <div class="row">
                    <div class="col-md-3">
                      <select class="form-select h-25" data-control="select2" data-placeholder="Pilih plant">
                        <option></option>
                        <option value="1">Option 1</option>
                        <option value="2">Option 2</option>
                      </select>
                    </div>
                    <div class="col-md-3">
                      <select class="form-select h-25" data-control="select2" data-placeholder="Pilih Tahun">
                        @foreach(getYear() as $key => $value)
                        @if ($value == date("Y") )
                        <option value="{{$value}}" selected>{{$value}}</option>
                        @else
                        <option value="{{$value}}">{{$value}}</option>
                        @endif
                        @endforeach
                      </select>
                    </div>
                    <div class="col-md-3">
                      <select class="form-select h-25" data-control="select2" data-placeholder="Pilih Bulan">
                        @foreach(getMonth() as $key => $value)
                        @if ($value['month'] == date("n") )
                        <option value="{{$value['month']}}" selected>{{$value['month_name']}}
                        </option>
                        @else
                        <option value="{{$value['month']}}">{{$value['month_name']}}</option>
                        @endif
                        @endforeach
                      </select>
                    </div>
                    <div class="col-md-3">
                      <button class="btn btn-history w-100">Tampilkan</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <label for="" class="font-weight-bold text-uppercase">Asset under construction</label>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="table-responsive">
                <table class="table table-striped data-table" id="tb_progress-item-capex">
                  <thead>
                    <tr class="border-bottom">
                      <th class="align-middle text-center color-header-tabel">No</th>
                      <th class="align-middle text-center color-header-tabel">PERIODE</th>
                      <th class="align-middle text-center color-header-tabel">KODE WBS</th>
                      <th class="align-middle text-center color-header-tabel">NAMA WBS</th>
                      <th class="align-middle text-center color-header-tabel">JENIS INVERTASI</th>
                      <th class="align-middle text-center color-header-tabel">USER</th>
                      <th class="align-middle text-center color-header-tabel">NILAI</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th class="align-middle text-center color-header-tabel">1</th>
                      <th class="align-middle text-left color-header-tabel text-uppercase">GENERAL ELECTRIC</th>
                      <th class="align-middle text-center color-header-tabel">CAGN-900</th>
                      <th class="align-middle text-center color-header-tabel">Pompa Water</th>
                      <th class="align-middle text-center color-header-tabel">Mesin</th>
                      <th class="align-middle text-center color-header-tabel">Section</th>
                      <th class="align-middle text-center color-header-tabel">78896541</th>
                    </tr>

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</x-default-layout>
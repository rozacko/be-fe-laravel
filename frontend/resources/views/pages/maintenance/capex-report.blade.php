<x-default-layout>
    <div class="py-lg-5">
        <div class="row">
            <div class="col-md-6">
                <div class="information">
                    <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">
                        Capex Report</h1>
                    <ul class="breadcrumb breadcrumb-separatorless ">
                        <li class="breadcrumb-item text-muted">
                            <a href="/" class="text-muted text-hover-primary">Maintenance</a>
                        </li>
                        <li class="breadcrumb-item text-muted">/</li>
                        <li class="breadcrumb-item text-ghopo">Capex Report</li>
                    </ul>
                </div>
            </div>
            <div class="col-md-6">
            </div>
        </div>
    </div>

    <div class="card h-md-100 mt-2">
        <div class="card-body pt-2">
            <ul class="nav nav-pills nav-pills-custom border-bottom">
                <li class="nav-item">
                    <a class="nav-link border-0 rounded-0 d-flex justify-content-between flex-column flex-center overflow-hidden p-4 active"
                        data-bs-toggle="pill" href="#kt_stats_capex_prioritas">
                        <span class="nav-text text-gray-700 fw-bold fs-6 lh-1">CAPEX PRIORITAS</span>
                        <span class="bullet-custom position-absolute bottom-0 w-100 h-2px bg-ghopo-primary"></span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link border-0 rounded-0 d-flex justify-content-between flex-column flex-center overflow-hidden p-4"
                        data-bs-toggle="pill" href="#kt_stats_per_items_capex">
                        <span class="nav-text text-gray-700 fw-bold fs-6 lh-1">PER ITEM CAPEX</span>
                        <span class="bullet-custom position-absolute bottom-0 w-100 h-2px bg-ghopo-primary"></span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade show active card-progres-capex-prioritas" id="kt_stats_capex_prioritas">
                    <div class="py-lg-5">
                        <div class="row">
                            <div class="col-md-6">

                            </div>
                            <div class="col-md-6">
                                <div class="">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <select class="form-select h-25" data-control="select2"
                                                data-placeholder="Pilih plant">
                                                <option></option>
                                                <option value="1">Option 1</option>
                                                <option value="2">Option 2</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <select class="form-select h-25" data-control="select2"
                                                data-placeholder="Pilih Tahun">
                                                @foreach(getYear() as $key => $value)
                                                @if ($value == date("Y") )
                                                <option value="{{$value}}" selected>{{$value}}</option>
                                                @else
                                                <option value="{{$value}}">{{$value}}</option>
                                                @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <select class="form-select h-25" data-control="select2"
                                                data-placeholder="Pilih Bulan">
                                                @foreach(getMonth() as $key => $value)
                                                @if ($value['month'] == date("n") )
                                                <option value="{{$value['month']}}" selected>{{$value['month_name']}}
                                                </option>
                                                @else
                                                <option value="{{$value['month']}}">{{$value['month_name']}}</option>
                                                @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <button class="btn btn-history w-100">Tampilkan</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label for="" class="font-weight-bold">PROGRES CAPEX PRIORITAS</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th class="align-middle text-center color-header-tabel">No</th>
                                            <th class="align-middle text-center color-header-tabel">WBS</th>
                                            <th class="align-middle text-center color-header-tabel">NAMA CAPEX</th>
                                            <th class="align-middle text-center color-header-tabel">TYPE</th>
                                            <th class="align-middle text-center color-header-tabel">NILAI</th>
                                            <th class="align-middle text-center color-header-tabel">ENGINEERING</th>
                                            <th class="align-middle text-center color-header-tabel">PROCUREMENT</th>
                                            <th class="align-middle text-center color-header-tabel">CONTRUCTION</th>
                                            <th class="align-middle text-center color-header-tabel">CLOSING /
                                                COMMISIONING</th>
                                            <th class="align-middle text-center color-header-tabel">KETERANGAN</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="bg-black-800">
                                            <td colspan="10">OPERATIONAL CAPEX</td>
                                        </tr>
                                        <tr class="border-bottom">
                                            <td colspan="10">CAPEX NEW</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Lorem Ipsum</td>
                                            <td>Lorem Ipsum</td>
                                            <td>1</td>
                                            <td>Lorem Ipsum</td>
                                            <td class="bg-green">10</td>
                                            <td class="bg-yellow">12</td>
                                            <td class="bg-yellow">14</td>
                                            <td class="bg-black-300">-</td>
                                            <td>Lorem Ipsum</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Lorem Ipsum</td>
                                            <td>Lorem Ipsum</td>
                                            <td>1</td>
                                            <td>Lorem Ipsum</td>
                                            <td class="bg-green">10</td>
                                            <td class="bg-yellow">12</td>
                                            <td class="bg-yellow">14</td>
                                            <td class="bg-black-300">-</td>
                                            <td>Lorem Ipsum</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Lorem Ipsum</td>
                                            <td>Lorem Ipsum</td>
                                            <td>1</td>
                                            <td>Lorem Ipsum</td>
                                            <td class="bg-green">10</td>
                                            <td class="bg-green">12</td>
                                            <td class="bg-green">14</td>
                                            <td class="bg-black-300">-</td>
                                            <td>Lorem Ipsum</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Lorem Ipsum</td>
                                            <td>Lorem Ipsum</td>
                                            <td>1</td>
                                            <td>Lorem Ipsum</td>
                                            <td class="bg-green">10</td>
                                            <td class="bg-green">12</td>
                                            <td class="bg-green">14</td>
                                            <td class="bg-black-300">-</td>
                                            <td>Lorem Ipsum</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Lorem Ipsum</td>
                                            <td>Lorem Ipsum</td>
                                            <td>1</td>
                                            <td>Lorem Ipsum</td>
                                            <td class="bg-green">10</td>
                                            <td class="bg-green">12</td>
                                            <td class="bg-green">14</td>
                                            <td class="bg-green">-</td>
                                            <td>Lorem Ipsum</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Lorem Ipsum</td>
                                            <td>Lorem Ipsum</td>
                                            <td>1</td>
                                            <td>Lorem Ipsum</td>
                                            <td class="bg-green">10</td>
                                            <td class="bg-green">12</td>
                                            <td class="bg-green">14</td>
                                            <td class="bg-green">-</td>
                                            <td>Lorem Ipsum</td>
                                        </tr>
                                        <tr class="border-bottom">
                                            <td colspan="10">CAPEX CARRY OVER</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Lorem Ipsum</td>
                                            <td>Lorem Ipsum</td>
                                            <td>1</td>
                                            <td>Lorem Ipsum</td>
                                            <td class="bg-green">10</td>
                                            <td class="bg-yellow">12</td>
                                            <td class="bg-yellow">14</td>
                                            <td class="bg-black-300">-</td>
                                            <td>Lorem Ipsum</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Lorem Ipsum</td>
                                            <td>Lorem Ipsum</td>
                                            <td>1</td>
                                            <td>Lorem Ipsum</td>
                                            <td class="bg-green">10</td>
                                            <td class="bg-yellow">12</td>
                                            <td class="bg-yellow">14</td>
                                            <td class="bg-black-300">-</td>
                                            <td>Lorem Ipsum</td>
                                        </tr>
                                        <tr class="bg-black-800">
                                            <td colspan="10">DEVELOPMENT CAPEX</td>
                                        </tr>
                                        <tr class="border-bottom">
                                            <td colspan="10">CAPEX NEW</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Lorem Ipsum</td>
                                            <td>Lorem Ipsum</td>
                                            <td>1</td>
                                            <td>Lorem Ipsum</td>
                                            <td class="bg-green">10</td>
                                            <td class="bg-yellow">12</td>
                                            <td class="bg-yellow">14</td>
                                            <td class="bg-black-300">-</td>
                                            <td>Lorem Ipsum</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Lorem Ipsum</td>
                                            <td>Lorem Ipsum</td>
                                            <td>1</td>
                                            <td>Lorem Ipsum</td>
                                            <td class="bg-green">10</td>
                                            <td class="bg-yellow">12</td>
                                            <td class="bg-yellow">14</td>
                                            <td class="bg-black-300">-</td>
                                            <td>Lorem Ipsum</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Lorem Ipsum</td>
                                            <td>Lorem Ipsum</td>
                                            <td>1</td>
                                            <td>Lorem Ipsum</td>
                                            <td class="bg-green">10</td>
                                            <td class="bg-green">12</td>
                                            <td class="bg-green">14</td>
                                            <td class="bg-black-300">-</td>
                                            <td>Lorem Ipsum</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Lorem Ipsum</td>
                                            <td>Lorem Ipsum</td>
                                            <td>1</td>
                                            <td>Lorem Ipsum</td>
                                            <td class="bg-green">10</td>
                                            <td class="bg-green">12</td>
                                            <td class="bg-green">14</td>
                                            <td class="bg-black-300">-</td>
                                            <td>Lorem Ipsum</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Lorem Ipsum</td>
                                            <td>Lorem Ipsum</td>
                                            <td>1</td>
                                            <td>Lorem Ipsum</td>
                                            <td class="bg-green">10</td>
                                            <td class="bg-green">12</td>
                                            <td class="bg-green">14</td>
                                            <td class="bg-green">-</td>
                                            <td>Lorem Ipsum</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Lorem Ipsum</td>
                                            <td>Lorem Ipsum</td>
                                            <td>1</td>
                                            <td>Lorem Ipsum</td>
                                            <td class="bg-green">10</td>
                                            <td class="bg-green">12</td>
                                            <td class="bg-green">14</td>
                                            <td class="bg-green">-</td>
                                            <td>Lorem Ipsum</td>
                                        </tr>
                                        <tr class="border-bottom">
                                            <td colspan="10">CAPEX CARRY OVER</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Lorem Ipsum</td>
                                            <td>Lorem Ipsum</td>
                                            <td>1</td>
                                            <td>Lorem Ipsum</td>
                                            <td class="bg-green">10</td>
                                            <td class="bg-yellow">12</td>
                                            <td class="bg-yellow">14</td>
                                            <td class="bg-black-300">-</td>
                                            <td>Lorem Ipsum</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Lorem Ipsum</td>
                                            <td>Lorem Ipsum</td>
                                            <td>1</td>
                                            <td>Lorem Ipsum</td>
                                            <td class="bg-green">10</td>
                                            <td class="bg-yellow">12</td>
                                            <td class="bg-yellow">14</td>
                                            <td class="bg-black-300">-</td>
                                            <td>Lorem Ipsum</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade card-progres-capex-prioritas" id="kt_stats_per_items_capex">
                    <div class="py-lg-5">
                        <div class="row">
                            <div class="col-md-6">

                            </div>
                            <div class="col-md-6">
                                <div class="">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <select class="form-select h-25" data-control="select2"
                                                data-placeholder="Pilih plant">
                                                <option></option>
                                                <option value="1">Option 1</option>
                                                <option value="2">Option 2</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <select class="form-select h-25" data-control="select2"
                                                data-placeholder="Pilih Tahun">
                                                @foreach(getYear() as $key => $value)
                                                @if ($value == date("Y") )
                                                <option value="{{$value}}" selected>{{$value}}</option>
                                                @else
                                                <option value="{{$value}}">{{$value}}</option>
                                                @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <select class="form-select h-25" data-control="select2"
                                                data-placeholder="Pilih Bulan">
                                                @foreach(getMonth() as $key => $value)
                                                @if ($value['month'] == date("n") )
                                                <option value="{{$value['month']}}" selected>{{$value['month_name']}}
                                                </option>
                                                @else
                                                <option value="{{$value['month']}}">{{$value['month_name']}}</option>
                                                @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <button class="btn btn-history w-100">Tampilkan</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label for="" class="font-weight-bold">PROGRES PER ITEM CAPEX</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordered data-table" id="tb_progress-item-capex">
                                    <thead>
                                        <tr>
                                            <th rowspan="2" class="align-middle text-center color-header-tabel">No</th>
                                            <th rowspan="2" class="align-middle text-center color-header-tabel">NAMA
                                                CAPEX</th>
                                            <th colspan="3" class="align-middle text-center color-header-tabel">
                                                INISIATOR</th>
                                            <th colspan="5" class="align-middle text-center color-header-tabel">
                                                REALISASI</th>
                                        </tr>
                                        <tr>

                                            <th class="align-middle text-center color-header-tabel">SEKSI</th>
                                            <th class="align-middle text-center color-header-tabel">BIRO</th>
                                            <th class="align-middle text-center color-header-tabel">DEPARTEMENT</th>
                                            <th class="align-middle text-center color-header-tabel">Engineering</th>
                                            <th class="align-middle text-center color-header-tabel">Proc.</th>
                                            <th class="align-middle text-center color-header-tabel">Delivery</th>
                                            <th class="align-middle text-center color-header-tabel">Closing</th>
                                            <th class="align-middle text-center color-header-tabel">Progres Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-default-layout>
<script>
    $(function() {

        var data = [
                    {
                        "no":"1",
                        "nama_capex":"Bahan Baku",
                        "seksi":"Operasi Utilitas",
                        "BIRO":"Produksi Bahan Baku",
                        "DEPARTEMENT":"Tambang & Bahan Baku",
                        "Engineering":"12345",
                        "Proc":"12345",
                        "Delivery":"12345",
                        "Closing":"12345",
                        "Progres Status":"CLOSE",
                    },
                    {
                        "no":"1",
                        "nama_capex":"Bahan Baku",
                        "seksi":"Operasi Utilitas",
                        "BIRO":"Produksi Bahan Baku",
                        "DEPARTEMENT":"Tambang & Bahan Baku",
                        "Engineering":"12345",
                        "Proc":"12345",
                        "Delivery":"12345",
                        "Closing":"12345",
                        "Progres Status":"PO RELEASE",
                    },
                    {
                        "no":"1",
                        "nama_capex":"Bahan Baku",
                        "seksi":"Operasi Utilitas",
                        "BIRO":"Produksi Bahan Baku",
                        "DEPARTEMENT":"Tambang & Bahan Baku",
                        "Engineering":"12345",
                        "Proc":"12345",
                        "Delivery":"12345",
                        "Closing":"12345",
                        "Progres Status":"TIDAK REALISASI",
                    },
                  
        ];

            var previous = "";

            $(document).ready(function() {

                var table = $("#tb_progress-item-capex").DataTable( {
                data: data,
                ordering: false,
                responsive: false,
                scrollX: true,
                aLengthMenu: [[5,10,20],[5,10,20]],
                columns: [{
                    data: "no",
                    name: "no"
                    },
                    {
                    data: "nama_capex",
                    name: "nama_capex"
                    },
                    {
                    data: "seksi",
                    name: "seksi"
                    }, 
                    {
                    data: "BIRO",
                    name: "BIRO"
                    },
                    {
                    data: "DEPARTEMENT",
                    name: "DEPARTEMENT"
                    },
                    {
                    data: "Engineering",
                    name: "Engineering"
                    },
                    {
                    data: "Proc",
                    name: "Proc"
                    },
                    {
                    data: "Delivery",
                    name: "Delivery"
                    },
                    {
                    data: "Closing",
                    name: "Closing"
                    },
                    {
                    data: "Progres Status",
                    name: "Progres Status"
                    },
                   
                ],
                // dom: "Bfrtip",
                // buttons: [
                //     {
                //     text: "Download",
                //     extend: "excelHtml5",
                //     className: "btn-primary",
                //     init: function( api, node, config) {
                //         $(node).removeClass("btn-secondary")
                //     },
                //     exportOptions: {
                //         columns: "thead th:not(.noExport)",
                //         rows: function (indx, rowData, domElement) {
                //             return $(domElement).css("display") != "none";
                //         }
                //     }
                // }
                // ],
                // "initComplete": function(settings, json) {
                //     processColumnSGA( $("#tb_progress-item-capex").DataTable() );
                //     processColumnseksi( $("#tb_progress-item-capex").DataTable() );
                //     processColumnFasilitator( $("#tb_progress-item-capex").DataTable() );
                // }
                } );

                // table.on( "draw", function () {
                // processColumnSGA( $("#tb_progress-item-capex").DataTable() );
                // processColumnseksi( $("#tb_progress-item-capex").DataTable() );
                // processColumnFasilitator( $("#tb_progress-item-capex").DataTable() );
                // } );

                // function processColumnSGA(tbl) {
                //     var selector_modifier = { order: "current", page: "current", search: "applied" }
                //     var previous = "";
                //     var officeNodes = tbl.column(12, selector_modifier).nodes();
                //     var Data_Target = tbl.column(11, selector_modifier).data();
                //     var officeData = tbl.column(12, selector_modifier).data();
                //         for (var i = 0; i < officeData.length; i++) {
                //             if (Data_Target[i] <= officeData[i]) {
                //                 officeNodes[i].setAttribute("style", "background-color:#91D04F;");
                //             }else{
                //                 officeNodes[i].setAttribute("style", "background-color:#F64752;");
                //             }
                //         }
                // }

                // function processColumnseksi(tbl) {
                //     var selector_modifier = { order: "current", page: "current", search: "applied" }
                //     var previous = "";
                //     var officeNodes = tbl.column(13, selector_modifier).nodes();
                //     var officeData = tbl.column(2, selector_modifier).data();
                //     var Data1 = tbl.column(13, selector_modifier).data();
                //     var Data_Target = tbl.column(11, selector_modifier).data();
                //     var total = tbl.column(15, selector_modifier).data();
                //         for (var i = 0; i < officeData.length; i++) {
                //             var current = officeData[i];
                //             if (current === previous) {
                //                 officeNodes[i].textContent = "";
                //                 officeNodes[i].setAttribute("style", "display:none;");
                //             } else {
                //                 officeNodes[i].setAttribute("rowspan", total[i]);
                //                 officeNodes[i].textContent = Data1[i];
                //                 if (Data_Target[i] <= Data1[i]) {
                //                     officeNodes[i].setAttribute("style", "background-color:#91D04F;");
                //                 }else{
                //                     officeNodes[i].setAttribute("style", "background-color:#F64752;");
                //                 }
                //             }
                //                 previous = current;
                //         }
                // }

                // function processColumnFasilitator(tbl) {
                //     var selector_modifier = { order: "current", page: "current", search: "applied" }

                //     var previous = "";
                //     var officeNodes = tbl.column(14, selector_modifier).nodes();
                //     var officeData = tbl.column(1, selector_modifier).data();
                //     var Data1 = tbl.column(14, selector_modifier).data();
                //     var Data_Target = tbl.column(11, selector_modifier).data();
                //     var Total = tbl.column(16, selector_modifier).data();
                //         for (var i = 0; i < officeData.length; i++) {

                //             var current = officeData[i];
                //             if (current === previous) {
                //                 officeNodes[i].textContent = "";
                //                 officeNodes[i].setAttribute("style", "display:none;");
                //             } else {
                //                 officeNodes[i].textContent = Data1[i];
                //                 officeNodes[i].setAttribute("rowspan", Total[i]);
                //                 officeNodes[i].setAttribute("style", "background-color:#91D04F;");
                //                 if (Data_Target[i] <= Data1[i]) {
                //                     officeNodes[i].setAttribute("style", "background-color:#91D04F;");
                //                 }else{
                //                     officeNodes[i].setAttribute("style", "background-color:#F64752;");
                //                 }
                //             }
                //                 previous = current;
                //         }
                // }

            });

    });
</script>
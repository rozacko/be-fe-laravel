<div class="modal-header pb-0 border-0 justify-content-end">
    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
        <span class="svg-icon svg-icon-1">
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1"
                    transform="rotate(-45 6 17.3137)" fill="currentColor" />
                <rect x="7.41422" y="6" width="16" height="2" rx="1"
                    transform="rotate(45 7.41422 6)" fill="currentColor" />
            </svg>
        </span>
    </div>
</div>
<div class="modal-body scroll-y px-10 px-lg-15 pt-0 pb-15">
    <div class="mb-13 text-center">
        <h1 class="mb-3">Detail Data Performance Notifikasi</h1>
    </div>
    <form enctype="multipart/form-data" id="form-detail">
        <div class="modal-body">
            @csrf
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    NOTIF. DATE
                </label>
                <input type="text" class="form-control form-control-solid" value="{{ $data->notif_date }}" disabled/>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    NOTIF. TIME
                </label>
                <input type="text" class="form-control form-control-solid" value="{{ $data->notif_time }}" disabled/>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    SYSTEM STATUS
                </label>
                <input type="text" class="form-control form-control-solid" value="{{ $data->system_status }}" disabled/>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    NOTIFICATION
                </label>
                <input type="text" class="form-control form-control-solid" value="{{ $data->notification }}" disabled/>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    FUNCTIONAL LOK.
                </label>
                <input type="text" class="form-control form-control-solid" value="{{ $data->functional_loc }}" disabled/>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    DESCRIPTION
                </label>
                <input type="text" class="form-control form-control-solid" value="{{ $data->description }}" disabled/>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    PRIORITY
                </label>
                <input type="text" class="form-control form-control-solid" value="{{ $data->priority }}" disabled/>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    NOTIF. TYPE
                </label>
                <input type="text" class="form-control form-control-solid" value="{{ $data->notification_type }}" disabled/>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    REPORT BY
                </label>
                <input type="text" class="form-control form-control-solid" value="{{ $data->reported_by }}" disabled/>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    MAIN WORK CTR
                </label>
                <input type="text" class="form-control form-control-solid" value="{{ $data->main_work_ctr }}" disabled/>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    PLANNER GROUP
                </label>
                <input type="text" class="form-control form-control-solid" value="{{ $data->planner_group }}" disabled/>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    PLANT SECTION
                </label>
                <input type="text" class="form-control form-control-solid" value="{{ $data->plant_section }}" disabled/>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    MAIN PLANT
                </label>
                <input type="text" class="form-control form-control-solid" value="{{ $data->main_plant }}" disabled/>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    EQUIPMENT
                </label>
                <input type="text" class="form-control form-control-solid" value="{{ $data->equipment }}" disabled/>
            </div>

            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    COST CENTER
                </label>
                <input type="text" class="form-control form-control-solid" value="{{ $data->cost_center }}" disabled/>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    CREATE ON
                </label>
                <input type="text" class="form-control form-control-solid" value="{{ $data->created_on }}" disabled/>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    TODAY
                </label>
                <input type="text" class="form-control form-control-solid" value="{{ $data->today }}" disabled/>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    NOTIF. AGIN
                </label>
                <input type="text" class="form-control form-control-solid" value="{{ $data->notif_aging }}" disabled/>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    DESC. STANDART
                </label>
                <input type="text" class="form-control form-control-solid" value="{{ $data->description_standar }}" disabled/>
            </div>

            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    NOTIF. KONVERT ORDER
                </label>
                <input type="text" class="form-control form-control-solid" value="{{ $data->notif_convert_to_order }}" disabled/>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    NOTIF CLOSE
                </label>
                <input type="text" class="form-control form-control-solid" value="{{ $data->notif_close }}" disabled/>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    KLASIFIKASI UK
                </label>
                <input type="text" class="form-control form-control-solid" value="{{ $data->klasifikasi_uk }}" disabled/>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">

</script>

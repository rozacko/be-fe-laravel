<div class="modal-header pb-0 border-0 justify-content-end">
    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
        <span class="svg-icon svg-icon-1">
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="currentColor" />
                <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="currentColor" />
            </svg>
        </span>
    </div>
</div>
<div class="modal-body scroll-y px-10 px-lg-15 pt-0 pb-15">
    <div class="mb-13 text-center">
        <h1 class="mb-3">Import Data</h1>
    </div>
    <div class="modal-body">
        <div class="d-flex flex-column mb-8 fv-row form-group">
            <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                Template
                <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Download Template"></i>
            </label>
            <a href="{{ url('performance-order/download-temp') }}" target="_blank" class="btn btn-secondary">
                <span class="indicator-label">Download Template</span>
            </a>
        </div>
    </div>
    <form method="POST" action="{{ route($route . '.store') }}" enctype="multipart/form-data" id="import-Perform-Order">
        <div class="modal-body">
        @csrf
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">Upload File</span>
                </label>
                <input type="file" name="upload_file" id="upload_file" style="display: flex !important;"/>
                <p class="mt-2" style="color: #1F7793;">*file yang di upload adalah file berdasarkan template yang disediakan</p>
            </div>
        </div>
        <div class="modal-footer">
            <div class="text-center">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                <button id="btn_submit" type="submit" class="btn btn-primary">
                    <span class="indicator-label">Submit</span>
                    <span class="indicator-progress">Please wait...
                    <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                </button>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    $(".select_option").select2({
        dropdownParent: $('#modal')
    });

    $("#import-Perform-Order").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.
        var form = $(this);
        var formData = new FormData();
        var actionUrl = form.attr('action');
        formData.append('upload_file', document.getElementById('upload_file').files[0]);
        $.ajax({
            type: "POST",
            url: actionUrl,
            data: formData,
            dataType: 'JSON',
            contentType: false,
            processData: false,
            beforeSend: function () {
                $('#btn_submit').attr('disabled', true).html("<i class='fa fa-spinner fa-spin' aria-hidden='true'></i> Processing");
            },
            success: function(data)
            {
                if(data.status == 'success'){
                    Swal.fire(
                        'Success',
                        data.message,
                        'success'
                    );
                    $('#modal').modal('hide');
                    table.draw();
                }else{
                    Swal.fire(
                        'Warning',
                        data.message,
                        'warning'
                    );
                }
            }
        }).always(function() {
            $('#btn_submit').attr('disabled', false).html("Submit");
        });
    });
</script>

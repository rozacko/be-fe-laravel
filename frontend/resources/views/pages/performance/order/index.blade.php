<x-default-layout>
    <div class="flex-stack mb-0 mb-lg-n4 pt-5">
        <div class="row">
            <div class="col-sm-3">
                <div class="information">
                    <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">Performance Order</h1>
                        <ul class="breadcrumb breadcrumb-separatorless ">
							<li class="breadcrumb-item text-muted">
								<a href="/" class="text-muted text-hover-primary">Data Koreksi</a>
							</li>
							<li class="breadcrumb-item text-muted">/</li>
							<li class="breadcrumb-item text-ghopo">{{$pagetitle}}</li>
                        </ul>
                </div>
            </div>
            <div class="col-sm-9 d-flex justify-content-md-end flex-no-wrap px-5">
                <div class="filter">
                    <div class="row">
                        {{-- <div class="col mt-2 dropdown-filter">
                            <select id="filter_organisasi" class="form-select h-25" data-control="select2" data-placeholder="Pilih Organisasi">
                                <option value="all">Pilih Organisasi</option>
                            </select>
                        </div> --}}
                        <div class="col mt-2 dropdown-filter">
                            <select id="filter_year" class="form-select h-25" data-control="select2" data-placeholder="Pilih Tahun">
                                @foreach(getYear() as $key => $value)
                                    @if ($value == date("Y") )
                                        <option value="{{$value}}" selected>{{$value}}</option>
                                    @else
                                        <option value="{{$value}}">{{$value}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col mt-2 dropdown-filter">
                            <select id="filter_month" class="form-select h-25" data-control="select2" data-placeholder="Pilih Bulan">
                                @foreach(getBulan() as $key => $value)
                                    @if ($value['month'] == date("m") )
                                        <option value="{{$value['month']}}" selected>{{$value['month_name']}}</option>
                                    @else
                                        <option value="{{$value['month']}}">{{$value['month_name']}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col mt-2 card-breadcrumb-button ">
                            <button class="btn btn-primary" id="filter-btn">Tampilkan</button>
                        </div>
                        <div class="col mt-2 card-breadcrumb-button">
                            <button type="button" class="btn btn-primary btn-add">Upload</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card card-flush mt-6 mt-xl-9">
        <div class="card-body mt-6 pt-0">
            <h3>{{$pagetitle}}</h3>
            <table class="table table-bordered data-table" id="tb_data-performance-order">
                <thead>
                    <tr>
                        <th class="align-middle text-center color-header-tabel">ORDER TYPE</th>
                        <th class="align-middle text-center color-header-tabel">ORDER</th>
                        <th class="align-middle text-center color-header-tabel">ORDER DESC.</th>
                        <th class="align-middle text-center color-header-tabel">SYSTEM STATUS</th>
                        <th class="align-middle text-center color-header-tabel">EQUIPMENT</th>
                        <th class="align-middle text-center color-header-tabel">EQUIPMENT DESC.</th>
                        <th class="align-middle text-center color-header-tabel">TOTAL ACT. COST</th>
                        <th class="align-middle text-center color-header-tabel">TANGGAL ACT.</th>
                        <th class="align-middle text-center color-header-tabel">ENTERED BY</th>
                        <th class="align-middle text-center color-header-tabel">PLANNER GROUP</th>
                        <th class="align-middle text-center color-header-tabel">PLANT SECTION</th>
                        <th class="align-middle text-center color-header-tabel">MAIN PLANT</th>
                        <th class="align-middle text-center color-header-tabel">COST CENTER</th>
                        <th class="align-middle text-center color-header-tabel">CREATE ON</th>
                        <th class="align-middle text-center color-header-tabel">Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>

    <div class="modal fade" id="modal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered mw-650px">
            <div class="modal-content rounded"></div>
        </div>
    </div>

    <div class="modal fade" id="modal-update" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered mw-650px">
        <div class="modal-content rounded"></div>
    </div>
  </div>
</x-default-layout>
<script>
    $(document).ready(function() {

    });

    $('#filter-btn').on('click',function() {
        table.draw();
    })


    var table = $('#tb_data-performance-order').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            "url": "{{ route($route.'.table') }}",
            'type': 'GET',
            'dataType': 'JSON',
            'data': function(d){
                d.tahun= $("#filter_year").val()
                d.bulan= $("#filter_month").val()
            }
        },
        responsive: false,
        scrollX: true,
        ordering: false,
        // paging: false,
        // data: data,
        columns: [
        {
        data: 'order_type',
        name: 'order_type'
        },
        {
        data: 'order',
        name: 'order'
        },
        {
        data: 'description',
        name: 'description',
        },
        {
        data: 'system_status',
        name: 'system_status'
        },
        {
        data: 'equipment',
        name: 'equipment'
        },
        {
        data: 'equipment_description',
        name: 'equipment_description'
        },
        {
        data: 'total_act_cost',
        name: 'total_act_cost'
        },
        {
        data: 'tanggal_actual',
        name: 'tanggal_actual'
        },
        {
        data: 'entered_by',
        name: 'entered_by'
        },
        {
        data: 'planner_group',
        name: 'planner_group'
        },
        {
        data: 'plant_section',
        name: 'plant_section'
        },
        {
        data: 'plant',
        name: 'plant'
        },
        {
        data: 'cost_center',
        name: 'cost_center'
        },
        {
        data: 'created_on',
        name: 'created_on'
        },
        {
            data: 'uuid',
            name: 'uuid',
            width: 150,
            render: function(data, type, full, meta) {
            let action =
            `<center>
            <button onClick="showOrder('${data}')" class="btn btn-outline btn-primary btn-sm"><i class="fas fa-eye"></i></button>&nbsp;
            <button onClick="deleteOrder('${data}')" class="btn btn-outline btn-danger btn-sm"><i class="fa fa-trash"></i></button>
            </center>`;
            return action;
            }
        }
        ],
    });

    setTimeout(function(){
        if ($('#alertNotif').length > 0) {
            $('#alertNotif').remove();
        }
    }, 5000)

    $('.btn-add').click(function (e) {
        e.preventDefault();
        var url = "{{ route($route.'.create') }}";
        $.get(url, function (html) {
            $('#modal .modal-content').html(html);
            $('#modal').modal('show');
        }).fail(function () {
            console.log('Gagal');
        });
    })

  // event button edit
    function showOrder(id) {
        event.preventDefault();
        var url = "{{ url('/'.$route . '/show'.'/:id') }}";
        url = url.replace(':id', id);
        $.get(url, function(html) {
            $('#modal-update .modal-content').html(html);
            $('#modal-update').modal('show');
        }).fail(function(xhr) {
            var {
                message
            } = xhr.responseJSON;
            Swal.fire({
                icon: 'error',
                title: message,
                showConfirmButton: false,
                timer: 3000
            })
        });
    }

  // event button delete
    function deleteOrder(id) {
        event.preventDefault();
        var url = "{{ url('/'.$route . '/delete'.'/:id') }}";
        url = url.replace(':id', id);
        Swal.fire({
            icon: 'warning',
            title: 'Apakah anda yakin?',
            text: "Data ini akan di hapus!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak',
            confirmButtonClass: 'btn btn-primary margin-right-10',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(isConfirm => {
            if (isConfirm.isConfirmed) {
                $.post(url, {_token: "{{ csrf_token() }}", _method: 'DELETE' }, function(res) {
                    if (res.status == 'success') {
                        Swal.fire(
                            'Success',
                            res.message,
                            'success'
                        );
                        table.draw();
                    } else {
                        Swal.fire(
                            'Gagal',
                            res.message,
                            'error'
                        );
                    }
                }, 'json');
            } else {
            }
        });
    }

</script>

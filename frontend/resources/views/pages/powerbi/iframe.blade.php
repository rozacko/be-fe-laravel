<x-default-layout>
    <div class="content d-flex flex-column flex-column-fluid powerBI pt-5" id="kt_content">
        <iframe title="" width="100%" height="100%" class="iframe iframe-powerbi"
        src="{{$powerbi_url}}" allowFullScreen="true"></iframe>
    </div>
</x-default-layout>
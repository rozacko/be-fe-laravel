<div class="modal-header pb-0 border-0 justify-content-end">
    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
        <span class="svg-icon svg-icon-1">
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="currentColor" />
                <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="currentColor" />
            </svg>
        </span>
    </div>
</div>
<div class="modal-body scroll-y px-10 px-lg-15 pt-0 pb-15">
    <div class="mb-13 text-center">
        <h1 class="mb-3">Create Data Inspection Item</h1>
        <div class="text-muted fw-semibold fs-5">If you need more info, please check
        <a href="#" class="fw-bold link-primary">Project Guidelines</a>.</div>
    </div>
    <form method="POST" action="#" enctype="multipart/form-data" id="modal-add">
        <div class="modal-body">
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    Area
                </label>
                <select class="form-select" data-placeholder="Enter Area" name="nm_area" data-control="select2">
                    <option></option>
                    <option value="1">Equipment 1</option>
                    <option value="2">Equipment 2</option>
                </select>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    Equipment
                </label>
                <select class="form-select" data-placeholder="Enter Equipment" name="kode_equipment" data-control="select2">
                    <option></option>
                    <option value="1">Equipment 1</option>
                    <option value="2">Equipment 2</option>
                </select>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    Condition
                </label>
                <select class="form-select" data-placeholder="Enter Condition" name="id_kondisi" data-control="select2">
                    <option></option>
                    <option value="1">Equipment 1</option>
                    <option value="2">Equipment 2</option>
                </select>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    Tanggal
                </label>
                <input type="date" class="form-control form-control-solid" placeholder="Enter Tanggal" name="create_date" />
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    Remark
                </label>
                <input type="text" class="form-control form-control-solid" placeholder="Enter Remark" name="remark" />
            </div>
        </div>
        <div class="modal-footer">
            <div class="text-center">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary">
                    <span class="indicator-label">Submit</span>
                </button>
            </div>
        </div>
    </form>
</div>

<script>
    $('#modal-add').submit(function(e){
        e.preventDefault()
        var formData = new FormData(e.target);
        const data = Object.fromEntries(formData)
        dataInspection.push(data)
        $('#modal-add').modal('hide')
        Swal.fire(
            'Success',
            'Add data sucessfully!',
            'success'
        );
        tableInspec(dataInspection, true)
    })
</script>

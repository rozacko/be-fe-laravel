<x-default-layout>
    <div class="g-5 g-xl-10 mb-3 mb-xl-3 pt-5">
    <div class="row">
        <div class="col-sm-3">
            <div class="information">
                <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">
                    Inspection
                </h1>
                <ul class="breadcrumb breadcrumb-separatorless ">
                    <li class="breadcrumb-item text-muted">
                        <a href="/plant-reability" class="text-muted text-hover-primary">Plant Reability</a>
                    </li>
                    <li class="breadcrumb-item text-muted">/</li>
                    <li class="breadcrumb-item text-ghopo">Inspection</li>
                </ul>
            </div>
        </div>
        @if ($name == 'developer')
            <div class="col-sm-9 d-flex justify-content-md-end flex-no-wrap">
                <div class="filter">
                    <div class="row">
                        <div class="col-6 col-lg col-md mt-2">
                            <select class="form-select h-25" data-control="select2" data-placeholder="Pilih Plant" id="filter-plant">
                            </select>
                        </div>
                        <div class="col-6 col-lg col-md mt-2">
                            <select class="form-select h-25" data-control="select2" data-placeholder="Pilih Bulan" id="filter-bulan">
                                @foreach(getMonth() as $key => $value)
                                @if ($value['month'] == date("n") )
                                    <option value="{{$value['month']}}" selected>{{$value['month_name']}}</option>
                                @else
                                    <option value="{{$value['month']}}">{{$value['month_name']}}</option>
                                @endif
                                @endforeach
                            </select>
                            </div>
                            <div class="col-6 col-lg col-md mt-2">
                            <select class="form-select h-25" data-control="select2" data-placeholder="Pilih Tahun" id="filter-tahun">
                                @foreach(getYear() as $key => $value)
                                @if ($value == date("Y") )
                                    <option value="{{$value}}" selected>{{$value}}</option>
                                @else
                                    <option value="{{$value}}">{{$value}}</option>
                                @endif
                                @endforeach
                            </select>
                            </div>
                        <div class="col-6 col-lg col-md mt-2">
                            <button onclick="changeFilterData()" class="btn btn-history w-100">Tampilkan</button>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
    </div>

    <div class="card">
    <div class="card-header p-3">
        <div class="card-title flex-column">
            <h3 class="fw-bold mb-1">Plant Inspection Transaction</h3>
        </div>
        @if ($name == 'developer')
            <div class="card-toolbar my-1">
                <a href="/inspection/tambah" class="btn btn-sm btn-primary float-right btn-add">
                    Tambah Data
                </a>
            </div>
        @endif
    </div>
    <div class="card-body pt-0">
        <table class="table table-bordered data-table" id="dt_inspec">
            <thead>
                <tr>
                    <th>No. Inspection</th>
                    <th>Desc. Inspection</th>
                    <th>Area</th>
                    <th>Fungsi</th>
                    <th>Plant</th>
                    <th>Opco</th>
                    <th>Date</th>
                    <th>Create by</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>

    </div>
    </div>

    <div class="modal fade" id="modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered mw-650px">
        <div class="modal-content rounded"></div>
    </div>
    </div>
</x-default-layout>

<script>
    let name = '{{ $name }}';
    $(document).ready(function () {
        filterPlant()
    })

    let dataInspection = [
    ];
    function tableInspec(data, redraw= false, name) {
        var table_inspec = $('#dt_inspec').DataTable({
        processing: true,
        ordering: true,
        responsive: false,
        scrollX: true,
        filter:false,
        paging: true,
        data: data,
        columns: [{
                data: 'kode_inspection',
                name: 'no_inspection'
            },
            {
                data: 'desc_inspection',
                name: 'desc_inspection'
            },
            {
                data: 'desc_area',
                name: 'desc_area'
            },
            {
                data: 'nama_fungsi',
                name: 'nama_fungsi'
            },
            {
                data: 'kode_plant',
                name: 'plant'
            },
            {
                data: 'kode_opco',
                name: 'opco'
            },
            {
                data: 'tgl_inspection',
                name: 'date'
            },
            {
                data: 'name',
                name: 'create_by'
            },
            {
                data: 'status',
                name: 'status',
                render: function(data, type, full, meta) {
                    var className = "";
                    if (data == 'APPROVE') {
                        className = 'badge badge-pill font-weight-bolder badge-success';
                    } else if (data == 'SUBMIT') {
                        className = 'badge badge-pill font-weight-bolder badge-warning';
                    }else if (data == 'DRAFT') {
                        className = 'badge badge-pill font-weight-bolder badge-secondary';
                    }else if (data == 'REJECTED') {
                        className = 'badge badge-pill font-weight-bolder badge-danger';
                    }
                    return '<span class="' + className + '">' + data + '</span>';
                }
            },
            {
                data: 'action',
                name: 'action'
            },
        ],
        rowCallback: function(row, data, index){
            kode = data.id
            if (name == 'developer') {
                if(data.status == 'APPROVE' || data.status == 'SUBMIT'){
                    className = 'btn btn-sm btn-primary float-right btn-history text-center w-100';
                    data = 'Lihat Item'
                    $(row).find('td:eq(9)').html('<a href="/inspection/detail/'+kode+'"'+
                    'class="' + className + '">' + data + '</a>');
                }
                else if(data.status == 'DRAFT' || data.status == 'REJECTED'){
                    className = 'btn btn-sm btn-primary float-right btn-history text-center w-100';
                    data = 'Edit'
                    $(row).find('td:eq(9)').html('<a href="/inspection/edit/'+kode+'"'+
                    'class="' + className + '">' + data + '</a>');
                }
            }
            else{
                if(data.status == 'SUBMIT'){
                    className = 'btn btn-sm btn-primary float-right btn-history text-center w-100';
                    data = 'Review'
                    $(row).find('td:eq(9)').html('<a href="/inspection/approve/'+kode+'"'+
                    'class="' + className + '">' + data + '</a>');
                }
            }
        }
        })
        if (redraw) {
            table_inspec.clear();
            table_inspec.rows.add(data);
            table_inspec.draw();
        };
    }

    function Datatable(name) {
        if (name == 'developer') {
            $.ajax({
                type: "GET",
                url: "{{ url('/'.$route . '/datatable') }}",
                data: {
                    'plant':$('#filter-plant').val(),
                    'bulan':$('#filter-bulan').val(),
                    'tahun':$('#filter-tahun').val(),
                },
                dataType: 'JSON',
                success: function(data){
                    dataInspection = data.data
                    tableInspec(dataInspection, true, name)
                }
            });
        }
        else{
            $.ajax({
                type: "GET",
                url: "{{ url('/'.$route . '/datatable-approve') }}",
                dataType: 'JSON',
                success: function(data){
                    dataInspection = data.data
                    tableInspec(dataInspection, true, name)
                }
            });
        }
    }

    function filterPlant() {
        $.ajax({
            type: "GET",
            url: "{{ url('/'.$route . '/plant') }}",
            dataType: 'JSON',
            contentType: false,
            processData: false,
            success: function(data){
                $.each(data.data, function (i, item) {
                    var newOption = new Option(item.nm_plant, item.kd_plant, false, false);
                    $('#filter-plant').append(newOption);
                });
                Datatable(name);
            }
        });
    }

    function changeFilterData() {
        Datatable(name)
    }

</script>
{{-- Extends layout --}}
<x-default-layout>
    <div class="g-5 g-xl-10 mb-3 mb-xl-3 pt-5">
        <div class="row">
            <div class="col-sm-3">
                <div class="information">
                    <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">
                        Inspection
                    </h1>
                    <ul class="breadcrumb breadcrumb-separatorless ">
                        <li class="breadcrumb-item text-muted">
                            <a href="/plant-reliability" class="text-muted text-hover-primary">Plant Reliability</a>
                        </li>
                        <li class="breadcrumb-item text-muted">/</li>
                        <li class="breadcrumb-item text-ghopo">
                            <a href="/inspection" class="text-muted text-hover-primary">Inspection</a>
                        </li>
                        <li class="breadcrumb-item text-muted">/</li>
                        <li class="breadcrumb-item text-ghopo">Edit Data</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <form method="POST" action="#" enctype="multipart/form-data" id="form-data">
            <div class="card-header p-3">
                <div class="card-title flex-column">
                    <h3 class="fw-bold mb-1">Plant Inspection Transaction</h3>
                </div>
            </div>
            <div class="card-body py-2">
                <div class="row py-1">
                    <div class="col-lg-4 col-md-4">
                        <div class="form-group">
                            <label class="fw-bold required" for="">No. Inspection :</label>
                            <input name="kode_inspection" type="text" class="form-control form-control-lg h-25" placeholder="Masukkan No. inspection" id="no_inspection">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <div class="form-group">
                            <label class="fw-bold required" for="">Desc. Inspection</label>
                            <input name="desc_inspection" type="text" class="form-control form-control-lg h-25" placeholder="Masukkan desc. inspection" id="desc_inspection">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <div class="form-group">
                            <label class="fw-bold required" for="">Plant</label>
                            <select name="id_plant" class="form-select h-25" data-control="select2" data-placeholder="Pilih Plant" id="filter-plant">
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row py-1">
                    <div class="col-lg-6 col-md-6">
                        <div class="form-group">
                            <label class="fw-bold required" for="">Area</label>
                            <select name="id_area" class="form-select h-25" data-control="select2" data-placeholder="Pilih Area" id="filter-area">
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="form-group">
                            <label class="fw-bold required" for="">Fungsi</label>
                            <select name="fungsi" class="form-select h-25" data-control="select2" data-placeholder="Pilih Fungsi" id="filter-fungsi">
                            </select>
                        </div>
                    </div>
                </div>
            </div>
    
            <div class="card-header p-3">
                <div class="card-title">
                    <h3 class="fw-bold mb-1">Inspection Item</h3>
                </div>
                <div class="float-right">
                    <button type="button" class="btn btn-sm btn-history float-right" id="btn-download">
                        Download Template
                    </button>
                    <button type="button" class="btn btn-sm btn-add float-right" id="btn-import">
                        Import
                    </button>
                </div>
            </div>
    
            <div class="card-body pt-0">
                <table class="table table-bordered data-table" id="dt_inspec_item">
                    <thead>
                        <tr>
                            <th>Area</th>
                            <th>Equip. Code</th>
                            <th>Equip. Name</th>
                            <th>Inspection Date</th>
                            <th>Condition</th>
                            <th>Remark</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
    
            <div class="card-header p-3">
                <div class="card-title">
                    <h3 class="fw-bold mb-1">Catatan</h3>
                </div>
            </div>
        
            <div class="card-body pt-0">
                <div class="row py-1">
                    <div class="col-lg-12 col-md-12">
                        <div class="form-group">
                            <label class="fw-bold required" for="">Catatan</label>
                            <textarea name="catatan" id="" cols="30" rows="2" class="wd-100 form-control form-control-lg"></textarea>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-header p-3">
                <div class="card-title">
                    <h3 class="fw-bold mb-1">Riwayat Catatan</h3>
                </div>
            </div>
            <div class="card-body pt-0">
                <table class="table table-bordered data-table" id="dt_inspec_note">
                    <thead>
                        <tr>
                            <th>Catatan</th>
                            <th>Date</th>
                            <th>Create By</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
    
            <div class="card-header p-3">
                <div class="ms-auto">
                    <a href="{{ url('/inspection') }}" class="btn btn-sm btn-history">
                        Kembali
                    </a>
                    <button type="button" class="btn btn-sm btn-secondary" id="btn-draft">
                        Draft
                    </button>
                    <button type="button" class="btn btn-sm btn-add" id="btn-submit">
                        Submit
                    </button>
                </div>
            </div>
        </form>
    </div>
    
    <div class="modal fade" id="modal-import" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered mw-650px">
            <div class="modal-content rounded"></div>
        </div>
    </div>

    <div class="modal fade" id="modal-add" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered mw-650px">
            <div class="modal-content rounded"></div>
        </div>
    </div>
    
</x-default-layout>

<script>
    $(document).ready(function () {
        $.ajax({
            url: "{{ url('/'.$route . '/detail-data') }}",
            type: 'POST',
            data: {
                'id':'{{$id}}'
            },
            dataType: 'JSON',
            success: function(data)
            {
                dataHeader = data.dataHeader
                $('#no_inspection').val(dataHeader.no_inspection)
                $('#desc_inspection').val(dataHeader.desc_inspection)

                filterPlant(dataHeader.kode_plant)
                filterArea(dataHeader.kode_area)

                dataItem = data.dataItem
                //maping data for store
                dataItem = dataItem.map(item => {
                    return {
                        nm_area: item.nama_area,
                        kode_equipment: item.kode_equipment,
                        desc_equipment: item.nama_equipment,
                        create_date: item.tgl_inspection,
                        id_kondisi: item.id_kondisi,
                        remark: item.remark,
                    };
                });
                dataInspection = dataItem
                tableInspec(dataItem, true)

                dataComment = data.dataComment
                inspecComment(dataComment, true)
            }
        });
    })

    let dataInspection = [
    ];
    function tableInspec(dataInspection, redraw= false) {
        var table_inspec = $('#dt_inspec_item').DataTable({
        processing: true,
        ordering: true,
        responsive: false,
        scrollX: true,
        filter:false,
        paging: true,
        data: dataInspection,
        columns: [{
                data: 'nm_area',
                name: 'area'
            },
            {
                data: 'kode_equipment',
                name: 'equip_code'
            },
            {
                data: 'desc_equipment',
                name: 'equip_name'
            },
            {
                data: 'create_date',
                name: 'inspection_date'
            },
            {
                data: 'id_kondisi',
                name: 'condition',
                render: function(data, type, full, meta) {
                    var className = "";
                    if (data == 1) {
                        data = 'GOOD';
                        className = 'badge badge-pill font-weight-bolder badge-success';
                    } else if (data == 2) {
                        data = 'LOW RISK';
                        className = 'badge badge-pill font-weight-bolder badge-warning';
                    }else if (data == 3) {
                        data = 'MED RISK';
                        className = 'badge badge-pill font-weight-bolder badge-warning';
                    }else if (data == 4) {
                        data = 'HIGH RISK';
                        className = 'badge badge-pill font-weight-bolder badge-danger';
                    }
                    return data;
                }
            },
            {
                data: 'remark',
                name: 'remark'
            },
            {
                data: 'kode_equipment',
                name: 'action',
                className: 'text-center',
                render: function(data, type, row) {
                    let button =
                        `<button onclick="btn_delete('${data}')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>`;
                    return button;
                }
            },
            ]
        })
        if (redraw) {
            table_inspec.clear();
            table_inspec.rows.add(dataInspection);
            table_inspec.draw();
        };
    }

    $('#btn-download').click(function(e){
        e.preventDefault();
        $.ajax({
            xhrFields: {
                responseType: 'blob',
            },
            url: "{{ $urlExport }}",
            type: 'POST',
            data: {
                area: $('#filter-area').val()
            },
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', '{{ $token }}');
            },
            success: function(result, status, xhr) {
            var disposition = xhr.getResponseHeader('content-disposition');
            var filename = ('Data Inspection Area.xlsx');

            var blob = new Blob([result], {
                type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
            });
            var link = document.createElement('a');
            link.href = window.URL.createObjectURL(blob);
            link.download = filename;

            document.body.appendChild(link);

            link.click();
            document.body.removeChild(link);
            }
        })
    })

    $('#btn-import').click(function (e) {
        e.preventDefault();
        var url = '/inspection/modal-import';
        $.get(url, function (html) {
            $('#modal-import .modal-content').html(html);
            $('#modal-import').modal('show');
        }).fail(function () {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Something went wrong!',
            })
        });
    })

    $('#btn-add').click(function (e) {
        e.preventDefault();
        var url = '/inspection/modal-add';
        $.get(url, function (html) {
            $('#modal-add .modal-content').html(html);
            $('#modal-add').modal('show');
        }).fail(function () {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Something went wrong!',
            })
        });
    })
    
    $('#btn-submit').click(function (e) {
        e.preventDefault();
        Swal.fire({
            icon: 'warning',
            title: 'Apakah Anda Yakin Data Sudah Sesuai ?',
            type: 'warning',
            showCancelButton: true,
            cancelButtonText: 'Batal',
            confirmButtonText: 'Simpan',
            confirmButtonClass: 'btn btn-primary margin-right-10',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false,
            reverseButtons: true
        }).then((result) => {
            if (result.isConfirmed) {
                var oldData = {}
                $('#form-data').serializeArray().forEach(item => {
                    oldData[item.name]=item.value
                });
                const data = {...oldData,
                    tgl_inspection: new Date().toISOString().slice(0, 10),
                    id_inspection: '{{ $id }}',
                    kode_opco:"7000",
                    status: "SUBMIT",
                    kode_plant: $('#filter-plant').find(":selected").text(),
                    desc_area: $('#filter-area').find(":selected").text(),
                    item:dataInspection
                }
                $.ajax({
                    url: "{{ url('/'.$route . '/store') }}",
                    type: 'POST',
                    data: data,
                    dataType: 'JSON',
                    success: function(data)
                    {
                        if(data.status == 'success'){
                            Swal.fire(
                                'Success',
                                data.message,
                                'success'
                            );
                            setTimeout(function(){
                                window.location.href = '/inspection';
                            }, 2000);
                        }else{
                            Swal.fire(
                                'Warning',
                                data.message,
                                'warning'
                            );
                        }
                    }
                });
            }
        });
    })

    $('#btn-draft').click(function (e) {
        e.preventDefault();
        Swal.fire({
            icon: 'warning',
            title: 'Apakah Anda Yakin Akan Menyimpan Data Sebagai Draft ?',
            type: 'warning',
            showCancelButton: true,
            cancelButtonText: 'Batal',
            confirmButtonText: 'Simpan',
            confirmButtonClass: 'btn btn-primary margin-right-10',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false,
            reverseButtons: true
        }).then((result) => {
            if (result.isConfirmed) {
                var oldData = {}
                $('#form-data').serializeArray().forEach(item => {
                    oldData[item.name]=item.value
                });
                const data = {...oldData,
                    tgl_inspection: new Date().toISOString().slice(0, 10),
                    id_inspection: '{{ $id }}',
                    kode_opco:"7000",
                    status: "DRAFT",
                    kode_plant: $('#filter-plant').find(":selected").text(),
                    desc_area: $('#filter-area').find(":selected").text(),
                    item:dataInspection
                }
                $.ajax({
                    url: "{{ url('/'.$route . '/store') }}",
                    type: 'POST',
                    data: data,
                    dataType: 'JSON',
                    success: function(data)
                    {
                        if(data.status == 'success'){
                            Swal.fire(
                                'Success',
                                data.message,
                                'success'
                            );
                            setTimeout(function(){
                                window.location.href = '/inspection';
                            }, 2000);
                        }else{
                            Swal.fire(
                                'Warning',
                                data.message,
                                'warning'
                            );
                        }
                    }
                });
            }
        });
    })

    function btn_delete(id) {
        dataInspection = dataInspection.filter(data=>data.kode_equipment != id)
        tableInspec(dataInspection, true)
    }

    function filterPlant(dataPlant) {
        $.ajax({
            type: "GET",
            url: "{{ url('/'.$route . '/plant') }}",
            dataType: 'JSON',
            contentType: false,
            processData: false,
            success: function(data){
                $.each(data.data, function (i, item) {
                    if (dataPlant == item.nm_plant) {
                        var newOption = new Option(item.nm_plant, item.kd_plant, false, true);
                        $('#filter-plant').append(newOption);
                    }else{
                        var newOption = new Option(item.nm_plant, item.kd_plant, false, false);
                        $('#filter-plant').append(newOption);
                    }
                });
            }
        });
    }

    function filterArea(dataArea) {
        $.ajax({
            type: "GET",
            url: "{{ url('/'.$route . '/area') }}",
            dataType: 'JSON',
            contentType: false,
            processData: false,
            success: function(data){
                $.each(data.data, function (i, item) {
                    if (dataArea == item.nm_area) {
                        var newOption = new Option(item.nm_area, item.id, false, true);
                        $('#filter-area').append(newOption);
                    }else{
                        var newOption = new Option(item.nm_area, item.id, false, false);
                        $('#filter-area').append(newOption);
                    }

                });
            }
        });
    }

    function inspecComment(data, redraw= false) {
        var tableInspecComment = $('#dt_inspec_note').DataTable({
        processing: true,
        ordering: true,
        responsive: false,
        scrollX: true,
        filter:false,
        paging: true,
        data: data,
        columns: [{
                    data: 'remark',
                    name: 'catatan'
                },
                {
                    data: 'created_at',
                    name: 'date',
                    render: function(data, type, full, meta) {
                        let date = new Date(data).toISOString().slice(0, 10);
                        return date;
                    }
                },
                {
                    data: 'name',
                    name: 'create_by'
                },
            ]
        })
        if (redraw) {
            tableInspecComment.clear();
            tableInspecComment.rows.add(data);
            tableInspecComment.draw();
        };
    }

</script>

{{-- Extends layout --}}
<x-default-layout>
    <div class="g-5 g-xl-10 mb-3 mb-xl-3 pt-5">
        <div class="row">
            <div class="col-sm-3">
                <div class="information">
                    <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">
                        Inspection
                    </h1>
                    <ul class="breadcrumb breadcrumb-separatorless ">
                        <li class="breadcrumb-item text-muted">
                            <a href="/plant-reliability" class="text-muted text-hover-primary">Plant Reliability</a>
                        </li>
                        <li class="breadcrumb-item text-muted">/</li>
                        <li class="breadcrumb-item text-ghopo">
                            <a href="/inspection" class="text-muted text-hover-primary">Inspection</a>
                        </li>
                        <li class="breadcrumb-item text-muted">/</li>
                        <li class="breadcrumb-item text-ghopo">Approve</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <form method="POST" action="#" enctype="multipart/form-data" id="form-data">
            <div class="card-header p-3">
                <div class="card-title flex-column">
                    <h3 class="fw-bold mb-1">Plant Inspection Transaction</h3>
                </div>
            </div>
            <div class="card-body py-2">
                <div class="row py-1">
                    <div class="col-lg-4 col-md-4">
                        <div class="form-group">
                            <label class="fw-bold required" for="">No. Inspection :</label>
                            <input name="kode_inspection" type="text" class="form-control form-control-lg h-25" placeholder="Masukkan No. inspection" id="no_inspection" disabled>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <div class="form-group">
                            <label class="fw-bold required" for="">Desc. Inspection </label>
                            <input name="desc_inspection" type="text" class="form-control form-control-lg h-25" placeholder="Masukkan desc. inspection" id="desc_inspection" disabled>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <div class="form-group">
                            <label class="fw-bold required" for="">Plant </label>
                            <input name="id_plant" type="text" class="form-control form-control-lg h-25" placeholder="Pilih Plant" id="kode_plant" disabled>
                        </div>
                    </div>
                </div>
                <div class="row py-1">
                    <div class="ccol-lg-4 col-md-4">
                        <div class="form-group">
                            <label class="fw-bold required" for="">Area </label>
                            <input name="id_area" type="text" class="form-control form-control-lg h-25" placeholder="Pilih Area" id="kode_area" disabled>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <div class="form-group">
                            <label class="fw-bold required required" for="">Fungsi</label>
                            <input name="fungsi" type="text" class="form-control form-control-lg h-25" placeholder="Pilih Fungsi" id="kode_fungsi" disabled>
                        </div>
                    </div>
                    <div class="ccol-lg-4 col-md-4">
                        <div class="form-group">
                            <label class="fw-bold" for="">Inspection Date</label>
                            <input name="tgl_inspection" type="text" class="form-control form-control-lg h-25" placeholder="Tanggal Inspection" id="tgl_inspection" disabled>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-header p-3">
                <div class="card-title">
                    <h3 class="fw-bold mb-1">Inspection Item</h3>
                </div>
            </div>
            <div class="card-body pt-0">
                <table class="table table-bordered data-table" id="dt_inspec_item">
                    <thead>
                        <tr>
                            <th>Area</th>
                            <th>Equip. Code</th>
                            <th>Equip. Name</th>
                            <th>Inspection Date</th>
                            <th>Condition</th>
                            <th>Remark</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>

            <div class="card-header p-3">
                <div class="card-title">
                    <h3 class="fw-bold mb-1">Summary</h3>
                </div>
            </div>
            <div class="card-body pt-0">
                <table class="table table-bordered data-table" id="dt_inspec_summary">
                    <thead>
                        <tr>
                            <th>Area</th>
                            <th>Good</th>
                            <th>Low Risk</th>
                            <th>Medium Risk</th>
                            <th>High Risk</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Total</th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>

            <div class="card-header p-3">
                <div class="card-title">
                    <h3 class="fw-bold mb-1">Riwayat Catatan</h3>
                </div>
            </div>
            <div class="card-body pt-0">
                <table class="table table-bordered data-table" id="dt_inspec_note">
                    <thead>
                        <tr>
                            <th>Catatan</th>
                            <th>Date</th>
                            <th>Create By</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>

            <div class="card-header p-3">
                <div class="ms-auto">
                    <a href="{{ url('/inspection') }}" class="btn btn-sm btn-history">
                        Kembali
                    </a>
                </div>
            </div>
        </form>
    </div>
    
    <div class="modal fade" id="modal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered mw-650px">
            <div class="modal-content rounded"></div>
        </div>
    </div>
    
</x-default-layout>

<script>
    $(document).ready(function() {
        $.ajax({
            url: "{{ url('/'.$route . '/detail-data') }}",
            type: 'POST',
            data: {
                'id':'{{$id}}'
            },
            dataType: 'JSON',
            success: function(data)
            {
                dataHeader = data.dataHeader
                $('#no_inspection').val(dataHeader.no_inspection)
                $('#desc_inspection').val(dataHeader.desc_inspection)
                $('#kode_plant').val(dataHeader.kode_plant)
                $('#kode_area').val(dataHeader.kode_area)
                $('#tgl_inspection').val(dataHeader.tgl_inspection)
                $('#kode_fungsi').val(dataHeader.nama_fungsi)

                dataItem = data.dataItem
                inspecItem(dataItem, true)

                dataSummary = data.dataSummary
                inspecSummary(dataSummary, true)

                dataComment = data.dataComment
                inspecComment(dataComment, true)
            }
        });
    });

    function inspecItem(data, redraw= false) {
        var tableInspecItem = $('#dt_inspec_item').DataTable({
        processing: true,
        ordering: true,
        responsive: false,
        scrollX: true,
        filter:false,
        paging: true,
        data: data,
        columns: [{
                data: 'nama_area',
                name: 'area'
            },
            {
                data: 'kode_equipment',
                name: 'equip_code'
            },
            {
                data: 'nama_equipment',
                name: 'equip_name'
            },
            {
                data: 'tgl_inspection',
                name: 'inspection_date'
            },
            {
                data: 'id_kondisi',
                name: 'condition',
                render: function(data, type, full, meta) {
                    var className = "";
                    if (data == 1) {
                        data = 'GOOD';
                        className = 'badge badge-pill font-weight-bolder badge-success';
                    } else if (data == 2) {
                        data = 'LOW RISK';
                        className = 'badge badge-pill font-weight-bolder badge-warning';
                    }else if (data == 3) {
                        data = 'MED RISK';
                        className = 'badge badge-pill font-weight-bolder badge-warning';
                    }else if (data == 4) {
                        data = 'HIGH RISK';
                        className = 'badge badge-pill font-weight-bolder badge-danger';
                    }
                    return data;
                }
            },
            {
                data: 'remark',
                name: 'remark'
            }
        ],
        })
        if (redraw) {
            tableInspecItem.clear();
            tableInspecItem.rows.add(data);
            tableInspecItem.draw();
        };
    }

    function inspecSummary(data, redraw= false) {
        var tableInspecSummary = $('#dt_inspec_summary').DataTable({
        processing: true,
        ordering: true,
        responsive: false,
        scrollX: true,
        filter:false,
        paging: true,
        data: data,
        columns: [{
                    data: 'area',
                    name: 'area'
                },
                {
                    data: 'good',
                    name: 'good'
                },
                {
                    data: 'low',
                    name: 'low_risk'
                },
                {
                    data: 'med',
                    name: 'med_risk'
                },
                {
                    data: 'high',
                    name: 'high_risk'
                }
            ],
            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api();
                nb_cols = api.columns().nodes().length;
                var j = 1;
                var sum_column = 0
                var list_total = []
                //get list sum column and sum all data
                while(j < nb_cols){
                var pageTotal = api
                        .column( j, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return Number(a) + Number(b);
                        }, 0 );
                
                sum_column += pageTotal
                list_total.push(pageTotal)
                j++;
                }
                j = 1;
                //show total each column and percentage
                while(j < nb_cols){
                var total = list_total[j-1]
                var percentage = ((list_total[j-1]/sum_column)*100).toFixed(1)
                // Update footer
                $( api.column( j ).footer() ).html(total+' ('+percentage+'%)');
                j++
                }
            }
        })
        if (redraw) {
            tableInspecSummary.clear();
            tableInspecSummary.rows.add(data);
            tableInspecSummary.draw();
        };
    }

    function inspecComment(data, redraw= false) {
        var tableInspecComment = $('#dt_inspec_note').DataTable({
        processing: true,
        ordering: true,
        responsive: false,
        scrollX: true,
        filter:false,
        paging: true,
        data: data,
        columns: [{
                    data: 'remark',
                    name: 'catatan'
                },
                {
                    data: 'created_at',
                    name: 'date',
                    render: function(data, type, full, meta) {
                        let date = new Date(data).toISOString().slice(0, 10);
                        return date;
                    }
                },
                {
                    data: 'name',
                    name: 'create_by'
                },
            ]
        })
        if (redraw) {
            tableInspecComment.clear();
            tableInspecComment.rows.add(data);
            tableInspecComment.draw();
        };
    }
</script>

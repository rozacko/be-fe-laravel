<div class="modal-header pb-0 border-0 justify-content-end">
    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
        <span class="svg-icon svg-icon-1">
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="currentColor" />
                <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="currentColor" />
            </svg>
        </span>
    </div>
</div>
<div class="modal-body scroll-y px-10 px-lg-15 pt-0 pb-15">
    <div class="mb-13 text-center">
        <h1 class="mb-3">Import Data</h1>
    </div>
    <form method="POST" action="{{ route($route . '.import') }}" enctype="multipart/form-data" id="modal-import">
        <div class="modal-body">
        @csrf
            <div class="d-flex flex-column mb-8 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">Upload File</span>
                </label>
                <input type="file" name="upload_file" style="display: flex !important;" id="upload_file"/>
                <p class="mt-2" style="color: #1F7793;">*file yang di upload adalah file berdasarkan template yang disediakan</p>
            </div>
        </div>
        <div class="modal-footer">
            <div class="text-center">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary">
                    <span class="indicator-label">Submit</span>
                </button>
            </div>
        </div>
    </form>
</div>


<script type="text/javascript">
    $("#modal-import").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.
        var formData = new FormData();
        formData.append('upload_file', document.getElementById('upload_file').files[0]);
        $.ajax({
            type: "POST",
            url: "{{ route($route . '.import') }}",
            data: formData,
            dataType: 'JSON',
            contentType: false,
            processData: false,
            success: function(data)
            {
                if(data.status == 'success'){
                    Swal.fire(
                        'Success',
                        data.message,
                        'success'
                    );
                    $('#modal-import').modal('hide');
                    dataInspection = data.data
                    tableInspec(dataInspection, true)
                }else{
                    Swal.fire(
                        'Warning',
                        data.message,
                        'warning'
                    );
                }
            }
        });
    });
</script>
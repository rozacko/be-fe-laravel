<x-auth-layout>
    <!--begin::Form-->
    <form class="form w-100" novalidate="novalidate" id="kt_sign_in_form" data-kt-redirect-url="/" action="client/login">
        @csrf
        <div class="block">
            <input hidden="true" id="apiurl" value="{{ url('client/login') }}">
        <div class="fv-row mb-8 text_input">
            {{-- <label class="hide"><i class="fa-solid fa-user"></i>Username</label> --}}
            <input type="text" placeholder="Username" name="username" autocomplete="off" class="form-control bg-transparent"/>
        </div>
        <div class="fv-row mb-3 text_input">
            {{-- <label class="hide"><i class="fa-solid fa-envelope"></i>Password</label> --}}
            <input type="password" placeholder="Password" name="password" autocomplete="off" class="form-control bg-transparent"/>
        </div>
        </div>
        <div class="d-grid mb-10">
            <div class="next-prev pop">
                <button type="submit" id="kt_sign_in_submit" type="button" class="next">
                    @include('partials/general/_button-indicator', ['label' => 'Login'])
                </button>
            </div>
        </div>
    </form>
</x-auth-layout>

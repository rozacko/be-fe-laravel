<x-default-layout>
    <div class="flex-stack mb-0 mb-lg-n4 pt-5">
        <div class="row">
            <div class="col">
                <div class="information">
                    <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{ $title }}</h1>
                        <ul class="breadcrumb breadcrumb-separatorless ">
                            <li class="breadcrumb-item text-muted">
                                <a href="/" class="text-muted text-hover-primary">{{ $title }}</a>
                            </li>
                            <li class="breadcrumb-item text-muted">/</li>
                            <li class="breadcrumb-item text-ghopo">Solar Panel</li>
                        </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="mt-5 g-5 g-xl-10 mb-3 mb-xl-3 text-center">
        <a href="https://isolarcloud.com/" target="_blank">
            <img src="{{ image('misc/solarpanel.png') }}" class="img-fluid">
        </a>
    </div>

</x-default-layout>

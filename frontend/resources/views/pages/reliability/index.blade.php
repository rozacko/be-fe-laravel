<x-default-layout>
  <div class="g-5 g-xl-10 mb-3 mb-xl-3 pt-5">
    <div class="row">
      <div class="col-sm-3">
        <div class="information mt-2">
          <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">
            Plant Reliability
          </h1>
          <ul class="breadcrumb breadcrumb-separatorless ">
                <li class="breadcrumb-item text-muted">
                  <a href="/" class="text-muted text-hover-primary">Plant Reliability</a>
                </li>
                <li class="breadcrumb-item text-muted">/</li>
                <li class="breadcrumb-item text-ghopo">Dashboard</li>
          </ul>
         </div>
      </div>
      <div class="col-sm-9 d-flex justify-content-md-end flex-no-wrap">
        <div class="filter">
            <div class="row">
              <div class="col-6 col-lg col-md mt-2">
                  <select class="form-select h-25" data-control="select2" data-placeholder="Pilih Plant" id="filter-plant">
                  </select>
              </div>
              <div class="col-6 col-lg col-md mt-2">
                  <select class="form-select h-25" data-control="select2" data-placeholder="Pilih Bulan" id="filter-bulan">
                      @foreach(getMonth() as $key => $value)
                      @if ($value['month'] == date("n") )
                          <option value="{{$value['month']}}" selected>{{$value['month_name']}}</option>
                      @else
                          <option value="{{$value['month']}}">{{$value['month_name']}}</option>
                      @endif
                      @endforeach
                  </select>
              </div>
              <div class="col-6 col-lg col-md mt-2">
                  <select class="form-select h-25" data-control="select2" data-placeholder="Pilih Tahun" id="filter-tahun">
                      @foreach(getYear() as $key => $value)
                      @if ($value == date("Y") )
                          <option value="{{$value}}" selected>{{$value}}</option>
                      @else
                          <option value="{{$value}}">{{$value}}</option>
                      @endif
                      @endforeach
                  </select>
              </div>
              <div class="col-6 col-lg col-md mt-2">
                <button class="btn btn-add w-100" onclick="changeFilterData();">Tampilkan</button>
              </div>
            </div>
        </div>
    </div>
    </div>
  </div>


  <div class="g-5 g-xl-10 mb-3 mb-xl-3">
    <div class="card-plant-reability">
      <div class="col-12 text-end">
        <div class="dot-group">
          <div class="dot bg-good"></div>
          <div class="dot-text">GOOD</div>
          <div class="dot bg-low-risk"></div>
          <div class="dot-text">LOW RISK</div>
          <div class="dot bg-medium-risk"></div>
          <div class="dot-text">MEDIUM RISK</div>
          <div class="dot bg-high-risk"></div>
          <div class="">HIGH RISK</div>
        </div>
      </div>
    </div>
  </div>

  <div class="g-5 g-xl-10 mb-3 mb-xl-3">
    <div class="card-plant-reability">
      <div class="row" id="equipment-card">
      </div>
    </div>
  </div>

  <div class="g-5 g-xl-10 mt-4 mb-3 mb-xl-3">
    <div class="card-plant-reability">
      <div class="row">
        <div class="col-sm-3 col-custom">
          @include('partials/widgets/cards/_widget-pie-plant-reability')
        </div>
        <div class="col-sm-9 col-custom">
          @include('partials/widgets/cards/_widget-table-plant-reability')
        </div>
      </div>
    </div>
  </div>
      
</x-default-layout>

<script>
  $(document).ready(function () {
    filterPlant()
    allCard()
  })

  function filterPlant() {
    $.ajax({
      type: "GET",
      url: "{{ url('/inspection/plant') }}",
      dataType: 'JSON',
      contentType: false,
      processData: false,
      success: function(data){
        $.each(data.data, function (i, item) {
          var newOption = new Option(item.nm_plant, item.kd_plant, false, false);
          $('#filter-plant').append(newOption);
        });
      }
    });
  }

  function changeFilterData() {
    var filter = "true";
    pieChart(filter);
    summaryReliability()
  }

  function allCard() {
    var filter = "false";
    pieChart(filter);
    summaryReliability()
  }

  function pieChart(filter) {
    $.ajax({
        type: "GET",
        url: "{{ url('/'.$route . '/pie') }}",
        data:{
          'plant':$('#filter-plant').val(),
          'bulan':$('#filter-bulan').val(),
          'tahun':$('#filter-tahun').val(),
        },
        dataType: 'JSON',
        success: function(data){
          var dataInspec = data.data;
          // Class definition
          var KTChartsPieReability = (function () {
            var chart = {
                self: null,
                rendered: false,
            };

            // Private methods
            var initChart = function (chart) {
              var element = document.getElementById("kt_charts_widget_pie_plant_reability");

              if (!element) {
                  return;
              }

              var height = parseInt(KTUtil.css(element, "height"));
              var options = {
                series: dataInspec.data,
                chart: {
                    id: "pie_chart_plant_reliability",
                    type: "donut",
                    height: height,
                },
                legend: {
                    position: "bottom",
                    horizontalAlign:'center',
                },
                plotOptions: {
                    pie: {
                        donut: {
                            size: '50%'
                        }
                    }
                },
                noData: {
                  text: 'Loading...'
                },
                colors: ["#5F8D4E", "#FCC888", "#D06224", '#990000'],
                labels: dataInspec.label
              };

              chart.self = new ApexCharts(element, options);

              // Set timeout to properly get the parent elements width
              setTimeout(function () {
                if (filter == "false") {
                    chart.self.render();
                }
                else {
                  ApexCharts.exec('pie_chart_plant_reliability', 'updateOptions', {
                    series: dataInspec.data,
                    labels: dataInspec.label
                  })
                }
                chart.rendered = true;
              }, 200);
            };

            // Public methods
            return {
              init: function () {
                  initChart(chart);

                  // Update chart on theme mode change
                  KTThemeMode.on("kt.thememode.change", function () {
                      if (chart.rendered) {
                          chart.self.destroy();
                      }

                      initChart(chart);
                  });
              },
            };
          })();

          // Webpack support
          if (typeof module !== "undefined") {
              module.exports = KTChartsPieReability;
          }

          // On document ready
          KTUtil.onDOMContentLoaded(function () {
              KTChartsPieReability.init();
          });
        }
    });
  }

  function summaryReliability() {
    $.ajax({
        type: "GET",
        url: "{{ url('/'.$route . '/summary') }}",
        data:{
          'plant':$('#filter-plant').val(),
          'bulan':$('#filter-bulan').val(),
          'tahun':$('#filter-tahun').val(),
        },
        dataType: 'JSON',
        success: function(data){
          tableInspec(data.data, true)
          barReliability(data.data)
        }
    });
  }

  function tableInspec(data, redraw= false) {
      var table_inspec = $('#dt_inspec').DataTable({
      processing: true,
      ordering: true,
      responsive: false,
      scrollX: true,
      filter:false,
      paging: false,
      data: data,
      columns: [{
              data: 'name',
              name: 'area'
          },
          {
              data: 'good',
              name: 'good'
          },
          {
              data: 'low',
              name: 'low_risk'
          },
          {
              data: 'medium',
              name: 'med_risk'
          },
          {
              data: 'high',
              name: 'high_risk'
          }
      ],
      "footerCallback": function ( row, data, start, end, display ) {
          var api = this.api();
          nb_cols = api.columns().nodes().length;
          var j = 1;
          var sum_column = 0
          var list_total = []
          //get list sum column and sum all data
          while(j < nb_cols){
            var pageTotal = api
                  .column( j, { page: 'current'} )
                  .data()
                  .reduce( function (a, b) {
                      return Number(a) + Number(b);
                  }, 0 );
            
            sum_column += pageTotal
            list_total.push(pageTotal)
            j++;
          }
          j = 1;
          //show total each column and percentage
          while(j < nb_cols){
            var total = list_total[j-1]
            if (sum_column <= 0) {
              var percentage = 0
            }
            else{
              var percentage = ((list_total[j-1]/sum_column)*100).toFixed(1)
            }
            // Update footer
            $( api.column( j ).footer() ).html(total+' ('+percentage+'%)');
            j++
          }
        }
      })
      if (redraw) {
          table_inspec.clear();
          table_inspec.rows.add(data);
          table_inspec.draw();
      };
  }

  function barReliability(data) {
    let render = ""
    for (let index = 0; index < data.length; index++) {
      
      render += progressBar({
        data: data[index],
        image: `/assets/media/svg/plant/${data[index].name}.svg`
      })
    }
    $('#equipment-card').html(render)
  }

  function progressBar({data, image}) {
    return `
    <div class="col-sm col-custom">
      <div class="card card-inspec-crusher">
        <img class="img-fluid card-img-top" src="${image}" alt="" srcset="">
        <div class="card-header">
          <span>${data.name}</span>
          <p class="mb-0">Last Update:</p>
          <p>${data.lastupdate == null? '-': data.lastupdate}</p>
        </div>
        <div class="px-1">
          <div class="progress-group">
            <div class="progress">
              <div class="progress-bar bg-good" role="progressbar"
              style="width: ${data.percent_good}%" aria-valuenow="${data.percent_good}" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <div class="progress-value">
              ${data.percent_good}%
            </div>
          </div>
          <div class="progress-group">
            <div class="progress">
              <div class="progress-bar bg-low-risk" role="progressbar"
              style="width: ${data.percent_low}%" aria-valuenow="${data.percent_low}" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <div class="progress-value">
              ${data.percent_low}%
            </div>
          </div>
          <div class="progress-group">
            <div class="progress">
              <div class="progress-bar bg-medium-risk" role="progressbar"
              style="width: ${data.percent_medium}%" aria-valuenow="${data.percent_medium}" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <div class="progress-value">
              ${data.percent_medium}%
            </div>
          </div>
          <div class="progress-group">
            <div class="progress">
              <div class="progress-bar bg-high-risk" role="progressbar"
              style="width: ${data.percent_high}%" aria-valuenow="${data.percent_high}" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <div class="progress-value">
              ${data.percent_high}%
            </div>
          </div>
        </div>
      </div>
    </div>
    `
  }

</script>

<x-default-layout>
    <!--begin::Row-->
    <div class="row g-5 g-xl-10 mb-5 mb-xl-10">
        <div class="card card-executive-summary">
            <div class="card-header">
                <div class="card-title">
                    <span>{{$pagetitle}}</span>
                </div>

                <div class="card-toolbar my-1">
                    <div class="me-6 my-1">
                        Tanggal Report
                    </div>
                    <div class="me-6 my-1">
                        <input type="text" id="filter_tanggal" class="form-control form-control-solid form-select-sm w-150px ps-9" value="{{date('d-m-Y', strtotime('yesterday'))}}">
                    </div>
                </div>
            </div>

            <div class="card-body">
                <div class="row">
                    @foreach(range(1, 4) as $plant)
                    <div class="col-xl-3">
                        <div class="table-responsive">
                            <table id="datatable-status-{{$plant}}" class="table table-row-bordered table-row-dashed gy-4 align-middle fw-bold dataTable no-footer" style="width:100%">
                                <thead class="fs-7 text-gray-400 text-uppercase">
                                    <tr>
                                        <th colspan="2">Tuban {{$plant}}</th>
                                    </tr>
                                    <tr>
                                        <th></th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="row">
                    <div class="dataTables_wrapper dt-bootstrap4 no-footer">
                        <div class="table-responsive">
                            <table id="datatable-report" class="table table-row-bordered table-row-dashed gy-4 align-middle fw-bold dataTable no-footer" style="width:100%">
                                <thead class="fs-7 text-gray-400 text-uppercase">
                                    <tr>
                                        <th width="30%"></th>
                                        <th width="15%">Tuban 1</th>
                                        <th width="15%">Tuban 2</th>
                                        <th width="15%">Tuban 3</th>
                                        <th width="15%">Tuban 4</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    @foreach(range(1, 4) as $plant)
                    <div class="col-xl-6">
                        <div id="silo_tuban_{{$plant}}"></div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</x-default-layout>

<script>
    var reportTable;
    @foreach(range(1, 4) as $plant)
    var runningTable_{{$plant}};
    var chartSilo_{{$plant}};
    var categoryAxis_{{$plant}};
    var valueAxis_{{$plant}};
    @foreach(['OPC','PCC','OPC Prem','PCC Prem'] as $cement_type)
    var series_{{str_replace(' ','',$cement_type)}}_{{$plant}};
    var bullet_{{str_replace(' ','',$cement_type)}}_{{$plant}};
    @endforeach
    var series_kosong_{{$plant}};
    @endforeach

    function format(d) {
        // `d` is the original data object for the row
        let html = '<div class="table-responsive"><table class="table table-row-bordered table-row-dashed gy-4 align-middle fw-bold dataTable no-footer" style="width:100%">';
        $.each(d.child, function(index, value) {
            html += '<tr class="table-secondary">' +
                '<td width="30%">&emsp;' + value.keterangan + '</td>' +
                '<td width="15%">' + value.tuban_1 + '</td>' +
                '<td width="15%">' + value.tuban_2 + '</td>' +
                '<td width="15%">' + value.tuban_3 + '</td>' +
                '<td width="15%">' + value.tuban_4 + '</td>' +
                '</tr>';
        });
        html += '</table></div>';
        return html;
    }


    $(document).ready(function() {
        reportTable = $('#datatable-report').DataTable({
            processing: true,
            serverSide: true,
            searching: false,
            order: [],
            ajax: {
                url: "{{route($route . '.table')}}",
                data: function(d) {
                    d.tgl_report = $('#filter_tanggal').val();
                    // d.custom = $('#myInput').val();
                    // etc
                },
            },
            columns: [{
                    data: 'keterangan',
                    name: 'keterangan',
                    "createdCell": function(td, cellData, rowData, row, col) {
                        if (typeof rowData.child !== 'undefined') {
                            $(td).addClass('dt-control dt-left')
                        }
                    }
                },
                {
                    data: 'Tuban 1',
                    name: 'Tuban 1',
                    render: function(data, type, row) {
                        return localeString(data);
                    }
                },
                {
                    data: 'Tuban 2',
                    name: 'Tuban 2',
                    render: function(data, type, row) {
                        return localeString(data);
                    }
                },
                {
                    data: 'Tuban 3',
                    name: 'Tuban 3',
                    render: function(data, type, row) {
                        return localeString(data);
                    }
                },
                {
                    data: 'Tuban 4',
                    name: 'Tuban 4',
                    render: function(data, type, row) {
                        return localeString(data);
                    }
                },
            ]
        });
        $('#datatable-report tbody').on('click', 'td.dt-control', function() {
            var tr = $(this).closest('tr');
            var row = reportTable.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                // Open this row
                row.child(format(row.data())).show();
                tr.addClass('shown');
            }
        });
        am4core.useTheme(am4themes_animated);
        @foreach(range(1, 4) as $plant)
        runningTable_{{$plant}} = $('#datatable-status-{{$plant}}').DataTable({
            processing: true,
            serverSide: true,
            searching: false,
            paging: false,
            order: [],
            ajax: {
                url: "{{route($route . '.status')}}",
                data: function(d) {
                    d.tgl_report = $('#filter_tanggal').val();
                    d.id_plant = '{{$plant}}';
                },
            },
            columns: [{
                    data: 'keterangan',
                    name: 'keterangan',
                },
                {
                    data: 'status',
                    name: 'status',
                    render: function(data, type, row) {
                        if(data=='TRUE'){
                            return `<span class="fa fa-power-off fa-lg" style="color:green"></span>`;
                        }
                        else
                            return `<span class="fa fa-power-off fa-lg" style="color:red"></span>`;
                    }
                },
            ]
        });
        initChart("{{$plant}}");
        generateChart("{{$plant}}");
        @endforeach
        $('#filter_tanggal').flatpickr({
            dateFormat: "d-m-Y",
            onChange: function(dt) {
                reportTable.ajax.reload();
            },
        });
    });

    function localeString(num) {
        if (!isNaN(num)) {
            return num.toLocaleString("id-Id", {
                maximumFractionDigits: 2
            });
        } else {
            if (num == 'FALSE') {
                return '<span class="fa fa-power-off fa-lg" style="color:red"></span>';
            } else {
                return '<span class="fa fa-power-off fa-lg" style="color:green"></span>';
            }
        }
    }

    function initChart(plant){
        window["chartSilo_"+plant] = am4core.create("silo_tuban_"+plant, am4charts.XYChart3D);
        window["chartSilo_"+plant].titles.create().text = "Stok Silo Tuban "+plant;

        window["categoryAxis_"+plant] = window["chartSilo_"+plant].xAxes.push(new am4charts.CategoryAxis());
        window["categoryAxis_"+plant].dataFields.category = "silo";
        window["categoryAxis_"+plant].renderer.grid.template.location = 0;
        window["categoryAxis_"+plant].renderer.grid.template.strokeOpacity = 0;

        window["valueAxis_"+plant] = window["chartSilo_"+plant].yAxes.push(new am4charts.ValueAxis());
        window["valueAxis_"+plant].renderer.grid.template.strokeOpacity = 0.2;
        window["valueAxis_"+plant].min = -10;
        window["valueAxis_"+plant].max = 20000;
        window["valueAxis_"+plant].strictMinMax = false;
        window["valueAxis_"+plant].renderer.baseGrid.disabled = true;
        window["valueAxis_"+plant].renderer.labels.template.adapter.add("text", function(text) {
            return text+" Ton"
        });

        @foreach(['OPC'=>'C92D2E','PCC'=>'2CB468','OPC Prem'=>'465A7D','PCC Prem'=>'A526A9'] as $type=>$color)        
        window["series_{{str_replace(' ','',$type)}}_"+plant] = window["chartSilo_"+plant].series.push(new am4charts.ConeSeries());
        window["series_{{str_replace(' ','',$type)}}_"+plant].dataFields.valueY = "{{$type}}";
        window["series_{{str_replace(' ','',$type)}}_"+plant].dataFields.categoryX = "silo";
        window["series_{{str_replace(' ','',$type)}}_"+plant].stacked = true;
        window["series_{{str_replace(' ','',$type)}}_"+plant].columns.template.width = am4core.percent(80);
        window["series_{{str_replace(' ','',$type)}}_"+plant].columns.template.fill = am4core.color("#{{$color}}");
        window["series_{{str_replace(' ','',$type)}}_"+plant].columns.template.stroke = am4core.color("#{{$color}}");
        window["series_{{str_replace(' ','',$type)}}_"+plant].columns.template.fillOpacity = 0.9;
        window["series_{{str_replace(' ','',$type)}}_"+plant].columns.template.strokeOpacity = 1;
        window["series_{{str_replace(' ','',$type)}}_"+plant].columns.template.strokeWidth = 2;

         window["bullet_{{str_replace(' ','',$type)}}_"+plant] =  window["series_{{str_replace(' ','',$type)}}_"+plant].bullets.push(new am4charts.LabelBullet())
         window["bullet_{{str_replace(' ','',$type)}}_"+plant].interactionsEnabled = false
         window["bullet_{{str_replace(' ','',$type)}}_"+plant].label.text = '{{$type}}: {valueY}' + ' Ton'
         window["bullet_{{str_replace(' ','',$type)}}_"+plant].label.fill = am4core.color('#ffffff')
        @endforeach

        window["series_kosong_"+plant] = window["chartSilo_"+plant].series.push(new am4charts.ConeSeries());
        window["series_kosong_"+plant].dataFields.valueY = "kosong";
        window["series_kosong_"+plant].dataFields.categoryX = "silo";
        window["series_kosong_"+plant].stacked = true;
        window["series_kosong_"+plant].columns.template.width = am4core.percent(80);
        window["series_kosong_"+plant].columns.template.fill = am4core.color("#000");
        window["series_kosong_"+plant].columns.template.fillOpacity = 0.1;
        window["series_kosong_"+plant].columns.template.stroke = am4core.color("#000");
        window["series_kosong_"+plant].columns.template.strokeOpacity = 0.2;
        window["series_kosong_"+plant].columns.template.strokeWidth = 2;
    }

    function generateChart(plant){
        $.get("{{route($route . '.silo')}}",{tgl_report : $('#filter_tanggal').val(), id_plant:plant},function(data){
            window["chartSilo_"+plant].data = data.series;
        },'json');
    }
</script>
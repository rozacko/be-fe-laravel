<x-default-layout>
    <!--begin::Row-->
    <div class="row g-5 g-xl-10 mb-5 mb-xl-10">
        <div class="card card-executive-summary">
            <div class="card-header">
                <div class="card-title">
                    <span>{{$pagetitle}}</span>
                </div>

                <div class="card-toolbar my-1">
                    <div class="me-6 my-1">
                        Tanggal Report
                    </div>
                    <div class="me-6 my-1">
                        <input type="text" id="filter_tanggal" class="form-control form-control-solid form-select-sm w-150px ps-9" value="{{date('d-m-Y', strtotime('yesterday'))}}">
                    </div>
                </div>
            </div>

            <div class="card-body">
                <div class="dataTables_wrapper dt-bootstrap4 no-footer">
                    <div class="table-responsive">
                        <table id="datatable-crusher" class="table table-row-bordered table-row-dashed gy-4 align-middle fw-bold dataTable no-footer" style="width:100%">
                            <thead class="fs-7 text-gray-400 text-uppercase">
                                <tr>
                                    <th></th>
                                    <th>Tuban 1</th>
                                    <th>Tuban 2</th>
                                    <th>Tuban 3</th>
                                    <th>Tuban 4</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-default-layout>

<script>
    var reportTable;
    $(document).ready(function () {
        reportTable = $('#datatable-crusher').DataTable({
            processing: true,
            serverSide: true,
            searching: false,
            order: [],
            ajax: {
                url: "{{route($route . '.table')}}",
                data: function (d) {
                    d.tgl_report = $('#filter_tanggal').val();
                    // d.custom = $('#myInput').val();
                    // etc
                },
            },
            columns: [
                {
                    data: 'keterangan',
                    name: 'keterangan'
                },
                {
                    data: 'Tuban 1',
                    name: 'Tuban 1',
                    render: function(data, type, row){
                        return localeString(data);
                    }
                },
                {
                    data: 'Tuban 2',
                    name: 'Tuban 2',
                    render: function(data, type, row){
                        return localeString(data);
                    }
                },
                {
                    data: 'Tuban 3',
                    name: 'Tuban 3',
                    render: function(data, type, row){
                        return localeString(data);
                    }
                },
                {
                    data: 'Tuban 4',
                    name: 'Tuban 4',
                    render: function(data, type, row){
                        return localeString(data);
                    }
                },
            ]
        });
        $('#filter_tanggal').flatpickr({
            dateFormat: "d-m-Y",
            onChange: function(dt){
                reportTable.ajax.reload();
            },
        });
    });

    function localeString(num){
        if(!isNaN(num)){
            return num.toLocaleString("id-Id",{maximumFractionDigits : 2});
        }
        else{
            if(num == 'OFF'){
                return '<span class="fa fa-power-off fa-lg" style="color:red"></span>';
            }
            else{
                return '<span class="fa fa-power-off fa-lg" style="color:green"></span>';
            }
        }
    }

</script>

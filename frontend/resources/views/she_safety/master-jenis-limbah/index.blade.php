{{-- Extends layout --}}
<x-default-layout>
    <div class="card card-flush mt-6 mt-xl-9">
        <div class="py-lg-5 p-3">
            <div class="row">
                <div class="col-md-6">
                    <div class="information">
                        <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">
                            {{ $pagetitle }}</h1>
                        <ul class="breadcrumb breadcrumb-separatorless ">
                            @foreach ($breadcrumb as $key => $value)
                                @if ($key === array_key_first($breadcrumb))
                                    <li class="breadcrumb-item text-muted">{{ $key . ' /' }}
                                    </li>
                                @endif

                                @if ($key === array_key_last($breadcrumb))
                                    <li class="breadcrumb-item text-ghopo">
                                        <a href="{{ $value }}"
                                            class="text-ghopo text-hover-primary">{{ $key }}</a>
                                    </li>
                                @endif
                            @endforeach

                        </ul>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="">
                        <div class="row d-flex justify-content-end">
                            <div class="col-md-3">
                                <select class="form-select h-25" data-control="select2" data-placeholder="Plant" id="filter_plant">
                                    <option></option>
                                    <option value="1">Plant 1</option>
                                    <option value="2">Plant 2</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <button class="btn btn-history w-100" id="get_data">Tampilkan</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-header p-3">
            <div class="card-title flex-column">
                <h3 class="fw-bold mb-1"></h3>
            </div>
            <div class="card-toolbar my-1">		      
                <button type="button" class="btn btn-sm btn-primary float-right btn-add ms-5">
                    Tambah Data
                </button>
            </div>
        </div>
        <div class="card-body pt-0">
            <table class="table table-bordered data-table" id="tb_apar">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Pabrik</th>
                        <th>Jenis Limbah B3</th>
                        <th>Sumber</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>

        </div>
    </div>

    <div class="modal fade" id="modal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered mw-1000px">
            <div class="modal-content rounded"></div>
        </div>
    </div>

</x-default-layout>

<script>
    var plant = $("#filter_plant").val();
    var url = '{{ route($route . '.table', ':plant') }}';
    var table = $('#tb_apar').DataTable({
        processing: true,
        serverSide: true,
        scrollX: true,
        ajax: url,
        columns: [{
                data: 'DT_RowIndex',
                name: 'DT_RowIndex'
            },
            {
                data: 'plant',
                name: 'plant'
            },
            {
                data: 'jenisLimbahB3',
                name: 'jenisLimbahB3'
            },
            {
                data: 'sumber',
                name: 'sumber'
            },
            {
                data: 'uuid',
                render: function(data, type, row, meta) {
                    if (data)
                        return '<a href="#" class="btn btn-primary btn-sm"><i class="fa fa-check" style="padding-right: 0px; font-size:10;"></i></a> <a href="#" class="btn btn-danger btn-sm"><i class="fa fa-close" style="padding-right: 0px; font-size:10;"></i></a>';
                    else
                        return '<a href="#" class="btn btn-danger btn-sm"><i class="fa fa-close" style="padding-right: 0px; font-size:10;"></i></a> ';
                }
            },
        ]
    });
    $(function() {
        $(".select_filter").select2();
        getdatatable();
    });

    function getdatatable() {
        table.ajax.url(url).load();
    }

    setTimeout(function() {
        if ($('#alertNotif').length > 0) {
            $('#alertNotif').remove();
        }
    }, 5000)

    $('.btn-add').click(function(e) {
        e.preventDefault();
        var url = "{{ route($route . '.create') }}";
        $.get(url, function(html) {
            $('#modal .modal-content').html(html);
            $('#modal').modal('show');
        }).fail(function() {
            Swal.fire(
                'Warning',
                'Something error',
                'warning'
            );
        });
    })

    $('#get_data').click(function(e) {
        e.preventDefault();
        var bulan = $("#filter_bulan").val();
        var tahun = $("#filter_tahun").val();
        var status = $("#filter_status").val();
        var url = '{{ route($route . '.table', [':bulan', ':tahun', ':status']) }}';
        url = url.replace(':bulan', bulan);
        url = url.replace(':tahun', tahun);
        url = url.replace(':status', status);
        table.ajax.url(url).load();
    })

    $('.btn-mdl_import').click(function(e) {
        e.preventDefault();
        var url = "{{ route($route . '.mdl_import') }}";
        $.get(url, function(html) {
            $('#modal .modal-content').html(html);
            $('#modal').modal('show');
        }).fail(function() {
            Swal.fire(
                'Warning',
                'Something error',
                'warning'
            );
        });
    })

    function show(id) {
        event.preventDefault();
        var url = '{{ route($route . '.edit', ':id') }}';
        url = url.replace(':id', id);
        $.get(url, function(html) {
            $('#modal .modal-content').html(html);
            $('#modal').modal('show');
        }).fail(function() {
            Swal.fire(
                'Warning',
                'Something error',
                'warning'
            );
        });
    }

    function btn_delete(id) {
        event.preventDefault();
        var url = '{{ route($route . '.destroy', ':id') }}';
        url = url.replace(':id', id);
        Swal.fire({
            icon: 'warning',
            title: 'Apakah anda yakin?',
            text: "Data ini akan di hapus!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak',
            confirmButtonClass: 'btn btn-primary margin-right-10',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function() {
            $.post(url, {
                _token: "{{ csrf_token() }}",
                _method: 'DELETE'
            }, function(res) {
                if (res.status == 'success') {
                    Swal.fire(
                        'Success',
                        res.message,
                        'success'
                    );
                    $('#tb_apar').DataTable().ajax.reload();
                } else {
                    Swal.fire(
                        'Gagal',
                        res.message,
                        'error'
                    );
                }
            }, 'json');
        });

        return false;
    }
</script>

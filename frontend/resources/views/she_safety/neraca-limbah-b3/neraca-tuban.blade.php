<div class="py-lg-5 mb-5">
    <div class="row">
        <div class="col-md-6">

        </div>
        <div class="col-md-6">
            <div class="">
                <div class="row">
                    <div class="col-md-4">
                        <select class="form-select" data-placeholder="Jenis Limbah">
                            <option></option>
                            <option value="1">Option 1</option>
                            <option value="2">Option 2</option>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <select class="form-select" data-placeholder="Pilih Bulan" id="filter_month{{ $plant }}">
                            @foreach (getMonth() as $key => $value)
                                @if ($value['month'] == date('n'))
                                    <option value="{{ $value['month'] }}" selected>{{ $value['month_name'] }}
                                    </option>
                                @else
                                    <option value="{{ $value['month'] }}">{{ $value['month_name'] }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-2">
                        <select class="form-select" data-placeholder="Pilih Tahun" id="filter_year{{ $plant }}">
                            @foreach (getYear() as $key => $value)
                                @if ($value == date('Y'))
                                    <option value="{{ $value }}" selected>{{ $value }}</option>
                                @else
                                    <option value="{{ $value }}">{{ $value }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-2">
                        <button class="btn btn-history w-100" onclick="filterTable()">Tampilkan</button>
                    </div>
                    <div class="col-md-2">
                        <button class="btn btn-history w-100 btn-add">Download</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<form action="#" method="post">
    @csrf
    <input type="hidden" value="0" name="last_sisa" id="last_sisa">
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered" id="tbl-tuban-{{ $plant }}">
                    <thead>
                        <tr>
                            <th class="align-middle text-center color-header-tabel" colspan="4">Limbah B3 Masuk ke
                                Fasilitas</th>
                            <th class="align-middle text-center color-header-tabel" colspan="4">Produk Keluar Dari
                                Penyimpanan</th>
                            <th class="align-middle text-center color-header-tabel">Sisa</th>
                        </tr>
                        <tr>
                            <th class="align-middle text-center color-header-tabel text-center">Tanggal</th>
                            <th class="align-middle text-center color-header-tabel">Sumber</th>
                            <th class="align-middle text-center color-header-tabel">Jumlah (ton)</th>
                            <th class="align-middle text-center color-header-tabel">Masa Simpan</th>
                            <th class="align-middle text-center color-header-tabel text-center">Tanggal</th>
                            <th class="align-middle text-center color-header-tabel">Sumber</th>
                            <th class="align-middle text-center color-header-tabel">Jumlah (ton)</th>
                            <th class="align-middle text-center color-header-tabel">Masa Simpan</th>
                            <th class="align-middle text-center color-header-tabel">Sisa yang ada di TPS</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row d-flex justify-content-end mb-5">
        <div class="col-md-2">
            <button class="btn btn-history w-100 btn-add">Simpan Perubahan</button>
        </div>
    </div>
</form>

<div class="card card-flush">
    <div class="card-body pt-0">
        <div class="row d-flex justify-content-center">
            <div class="col-12 col-sm-12 col-md col-lg-2 mt-1 card-top-over-houl">
                @include('she_safety.neraca-limbah-b3.chart.enviro-overview')
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        getFilterarea();
        getTable();
        generate(0);
    });

    function filterTable() {
        getTable();
    }

    function getTable() {
        let plant = "{{ $plant }}";

        var actionUrl = `{{ route($route . '.table', "${plant}") }}`;
        $.ajax({
            type: "get",
            url: actionUrl,
            success: function(data) {
                generateTable(data.data, plant);
                // console.log(data.data);
                generate(0, plant);
            }
        });
    }

    function generateTable(data, plant) {

        $('#tbl-tuban-{{ $plant }}').find('tbody').empty();

        var html = '';
        var tahun = $('#filter_year' + plant).val();
        var bulan = $('#filter_month' + plant).val();

        jumlah_hari = hitunghari(bulan, tahun);

        for (i = 0; i < jumlah_hari; i++) {
            days = i + 1;
            dateVar = `${bulan}/${days}/${tahun}`;

            var dates = new Date(dateVar);
            date = ((dates.getDate() > 9) ? dates.getDate() : ('0' + dates.getDate())) + '/' + ((dates.getMonth() > 8) ?
                (dates.getMonth() + 1) : ('0' + (dates.getMonth() + 1))) + '/' + dates.getFullYear();

            dateLimbahDb = dates.getFullYear() + '-' + ((dates.getMonth() > 8) ? (dates.getMonth() + 1) : ('0' + (dates
                .getMonth() + 1))) + '-' + ((dates.getDate() > 9) ? dates.getDate() : ('0' + dates.getDate()));


            var dateSimpanVar = new Date(dateVar);
            dateSimpanVar.setDate(dateSimpanVar.getDate() + 90)
            dateSimpan = ((dateSimpanVar.getDate() > 9) ? dateSimpanVar.getDate() : ('0' + dateSimpanVar.getDate())) +
                '/' + ((dateSimpanVar.getMonth() > 8) ? (dateSimpanVar.getMonth() + 1) : ('0' + (dateSimpanVar
                    .getMonth() + 1))) + '/' + dateSimpanVar.getFullYear();

            dateSimpanDb = dateSimpanVar.getFullYear() + '-' + ((dateSimpanVar.getMonth() > 8) ? (dateSimpanVar
                .getMonth() + 1) : ('0' + (dateSimpanVar
                .getMonth() + 1))) + '-' + ((dateSimpanVar.getDate() > 9) ? dateSimpanVar.getDate() : ('0' +
                dateSimpanVar.getDate()));

            dateCheck = dates.getFullYear() + '-' + ((dates.getMonth() > 8) ? (dates.getMonth() + 1) : ('0' + (dates
                .getMonth() + 1))) + '-' + ((dates.getDate() > 9) ? dates.getDate() : ('0' + dates.getDate()));

            const checkData = data.data.find((tanggal) => tanggal.tanggal_limbah == dateCheck);

            var sumber_limbah = '';
            var jumlah_limbah = 0;
            var sumber_penyimpanan = '';
            var jumlah_penyimpanan = 0;
            var sisa = 0;
            if (checkData) {
                sumber_limbah = checkData.sumber_limbah;
                jumlah_limbah = checkData.jumlah_limbah;
                sumber_penyimpanan = checkData.sumber_penyimpanan;
                jumlah_penyimpanan = checkData.jumlah_penyimpanan;
                sisa = checkData.sisa;
            }
            $('#last_sisa').val(data.sisa_terakhir);

            html += '<tr>';
            html += `<td>
                ${date}
                <input type="hidden" value="${dateLimbahDb}" name="tanggal_limbah[]">
                </td>`;
            html +=
                `<td><input type="text" class="form-control" aria-describedby="basic-addon1" name="sumber_limbah[]" id="sumber_limbah_${plant}_${i}" value="${sumber_limbah}" /></td>`;
            html +=
                `<td><input type="text" class="form-control" aria-describedby="basic-addon1" name="jumlah_limbah[]" id="jumlah_limbah_${plant}_${i}" value="${jumlah_limbah}" onchange="hitung(${i}, '${plant}')" /></td>`;
            html +=
                `<td>
                    ${dateSimpan}
                    <input type="hidden" value="${dateSimpanDb}" name="masa_simpan_limbah[]">
                </td>`;
            html += `<td>
                ${date}
                <input type="hidden" value="${date}" name="tanggal_penyimpanan[]">
                </td>`;
            html +=
                `<td><input type="text" class="form-control" aria-describedby="basic-addon1" name="sumber_penyimpanan[]" id="sumber_penyimpanan_${plant}_${i}" value="${sumber_penyimpanan}" /></td>`;
            html +=
                `<td><input type="text" class="form-control" aria-describedby="basic-addon1" name="jumlah_penyimpanan[]" id="jumlah_penyimpanan_${plant}_${i}" value="${jumlah_penyimpanan}" onchange="hitung(${i}, '${plant}')" ></td>`;
            html +=
                `<td>
                    ${dateSimpan}
                    <input type="hidden" value="${dateSimpan}" name="masa_simpan_produk[]">
                </td>`;
            html += `<td>
                    <div id="sisa_${plant}_${i}">${sisa}</div>
                    <input type="hidden" value="${sisa}" name="sisa[]" id="sisa_input_${plant}_${i}">
                </td>`;
            html += '<tr>';
        }

        html += '<tr>';
        html += `<td class="bg-black text-white text-center">Total</td>`;
        html += `<td class="bg-black text-white"></td>`;
        html += `<td class="bg-black text-white">
                    <div id="total_limbah_text_${plant}">0</div>
                    <input type="hidden" value="0" id="total_limbah_${plant}">
                </td>`;
        html += `<td class="bg-black text-white"></td>`;
        html += `<td class="bg-black text-white"></td>`;
        html += `<td class="bg-black text-white"></td>`;
        html += `<td class="bg-black text-white">
                    <div id="total_produk_text_${plant}">0</div>
                    <input type="hidden" value="0" id="total_produk_${plant}">
                </td>`;
        html += `<td class="bg-black text-white"></td>`;
        html += `<td class="bg-black text-white">
                    <div id="total_sisa_text_${plant}">0</div>
                    <input type="hidden" value="0" id="total_sisa_${plant}">
                </td>`;
        html += '</tr>';
        $('#tbl-tuban-'+plant).append(html);
    }

    var hitunghari = function(bulan, tahun) {

        return new Date(tahun, bulan, 0).getDate();
    }

    function hitung(key, plant) {
        let jumlah_penyimpanan = $('#jumlah_penyimpanan_' + plant + '_' + key).val();
        let jumlah_limbah = $('#jumlah_limbah_' + plant + '_' + key).val();

        if (isNaN(parseFloat(jumlah_penyimpanan)) || isNaN(parseFloat(jumlah_limbah))) {
            alert('Hanya format angka..');
            $('#jumlah_limbah_' + plant + '_' + key).val(0);
            $('#jumlah_penyimpanan_' + plant + '_' + key).val(0);
            $('#sisa_' + plant + '_' + key).html(0);
            $('#sisa_input_' + plant + '_' + key).val(0);
            return;
        }

        generate(key, plant);
    }

    function generate(key, plant) {

        var tahun = $('#filter_year' + plant).val();
        var bulan = $('#filter_month' + plant).val();

        jumlah_hari = hitunghari(bulan, tahun);
        let total_limbah = 0;
        let total_sisa = 0;
        let total_produk = 0;

        for (i = 0; i < jumlah_hari; i++) {

            if (i != 0) {
                last_key = i - 1;
                let last_sisa = $('#sisa_input_' + plant + '_' + last_key).val();

                if (last_sisa != 0) {
                    sisa = last_sisa;
                } else {
                    last_sisa = $('#sisa_input_' + plant + '_' + last_key).val();
                }
            } else {
                let last_sisa = $('#sisa_input_0').val();
                sisa = $('#last_sisa').val();
            }

            let jumlah_penyimpanan = $('#jumlah_penyimpanan_' + plant + '_' + i).val();

            let jumlah_limbah = $('#jumlah_limbah_' + plant + '_' + i).val();
            sisa_plus = parseFloat(sisa) + parseFloat(jumlah_limbah);

            if (sisa < jumlah_penyimpanan && jumlah_limbah < jumlah_penyimpanan) {
                alert('Sisa yang ada di TPS tidak boleh 0');
                $('#jumlah_penyimpanan_' + plant + '_' + i).val(0);
                return;
            }

            sisa = parseFloat(sisa_plus) - parseFloat(jumlah_penyimpanan);
            // console.log(sisa);
            $('#sisa_' + plant + '_' + i).html(sisa.toFixed(2));
            $('#sisa_input_' + plant + '_' + i).val(sisa.toFixed(2));

            total_limbah = parseFloat(total_limbah) + parseFloat(jumlah_limbah);
            total_produk = parseFloat(total_produk) + parseFloat(jumlah_penyimpanan);
            total_sisa = parseFloat(sisa);
        }


        $('#total_limbah_'+plant).val(total_limbah);
        $('#total_limbah_text_'+plant).html(total_limbah);

        $('#total_sisa_'+plant).val(total_sisa);
        $('#total_sisa_text_'+plant).html(total_sisa);

        $('#total_produk_'+plant).val(total_produk);
        $('#total_produk_text_'+plant).html(total_produk);

        chartOverview(plant);
    }

    function chartOverview(plant) {
        const total_limbah = $('#total_limbah_'+plant).val();
        const total_sisa = $('#total_sisa_'+plant).val();
        const total_produk = $('#total_produk_'+plant).val();

        let html = '';

        html += `<div id="kt_charts_widget_enviro_overview_${plant}" class="charts"></div>`;

        $('#chartsOverview_'+plant).html(html);

        if (total_limbah && total_produk && total_sisa) {
            getChartEnviroOverview(total_limbah, total_produk, total_sisa, plant);
        }

        // getChartEnviroOverview(total_limbah, total_produk, total_sisa);
        // getChartEnviroOverview(10, 20, 30);
    }

    function getFilterarea() {
        $.ajax({
            type: "GET",
            url: "{{ route($route_master . '.table') }}",
            dataType: 'JSON',
            beforeSend: function() {},
            success: function(data) {
                $.each(data.data, function(i, item) {
                    var newOption = new Option(item.nm_area, item.id, false, false);
                    $('#filter_area').append(newOption).trigger('change');
                });

            },
            error: function(xmlhttprequest, textstatus, message) {}
        });
    }
</script>

{{-- Extends layout --}}
<x-default-layout>
    <div class="py-lg-5 p-3">
        <div class="row">
            <div class="col-md-6">
                <div class="information">
                    <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">
                        {{ $pagetitle }}</h1>
                    <ul class="breadcrumb breadcrumb-separatorless ">
                        @foreach ($breadcrumb as $key => $value)
                            @if ($key === array_key_first($breadcrumb))
                                <li class="breadcrumb-item text-muted">{{ $key . ' /' }}
                                </li>
                            @endif

                            @if ($key === array_key_last($breadcrumb))
                                <li class="breadcrumb-item text-ghopo">
                                    <a href="{{ $value }}"
                                        class="text-ghopo text-hover-primary">{{ $key }}</a>
                                </li>
                            @endif
                        @endforeach

                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="card card-flush">
        <div class="card-body pt-0">
            <div class="row mt-3">
                <div class="col-md-6">
                    <div>
                        <h2>{{ $pagetitle }}</h2>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row d-flex justify-content-end">
                        <div class="col-md-6">
                            <div class="input-group mb-5">
                                <span class="input-group-text border-right-0"><i class="fa fa-search"></i></span>
                                <input type="text" class="form-control border-left-0"
                                    aria-describedby="basic-addon1" />
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <ul class="nav nav-pills nav-pills-custom border-bottom" id="tab-nav-header">
                <li class="nav-item">
                    <a class="nav-link border-0 rounded-0 d-flex justify-content-between flex-column flex-center overflow-hidden p-4 active"
                        data-bs-toggle="pill" href="#kt_stats_tuban" aria-controls="kt_stats_tuban">
                        <span class="nav-text text-gray-700 fw-bold fs-6 lh-1">TUBAN</span>
                        <span class="bullet-custom position-absolute bottom-0 w-100 h-2px bg-ghopo-primary"></span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link border-0 rounded-0 d-flex justify-content-between flex-column flex-center overflow-hidden p-4"
                        data-bs-toggle="pill" href="#kt_stats_gresik" aria-controls="kt_stats_gresik">
                        <span class="nav-text text-gray-700 fw-bold fs-6 lh-1">GRESIK</span>
                        <span class="bullet-custom position-absolute bottom-0 w-100 h-2px bg-ghopo-primary"></span>
                    </a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane fade show active card-progres-capex-prioritas" id="kt_stats_tuban"
                    data-url="{{ route("$route.get-table", 'tuban') }}">
                    {{-- @include('she_safety.neraca-limbah-b3.neraca-tuban') --}}
                </div>
                <div class="tab-pane fade card-progres-capex-prioritas" id="kt_stats_gresik"
                    data-url="{{ route("$route.get-table", 'gresik') }}">
                    {{-- @include('she_safety.neraca-limbah-b3.neraca-gresik') --}}
                </div>
            </div>

        </div>
    </div>

    <div class="modal fade" id="modal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered mw-1000px">
            <div class="modal-content rounded"></div>
        </div>
    </div>

</x-default-layout>

<script>

    $(document).ready(function() {
        reloadPage("kt_stats_tuban");
    });

    $('#tab-nav-header .nav-link').on('click', function() {
        var pageId = $(this).attr('aria-controls');
        reloadPage(pageId);
    });

    function reloadPage(pageId) {

        $('#' + pageId).html('');

        var dataUrl = $('#' + pageId).attr('data-url');

        // start_loading();
        $.ajax({
            type: "GET",
            url: dataUrl,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            success: function(data) {
                // stop_loading();

                $('#' + pageId).html(data);

                var scripts = $(data).find("script");

                if (scripts.length) {
                    $(scripts).each(function() {
                        if ($(this).attr("src")) {
                            $.getScript($(this).attr("src"));
                        } else {
                            window.eval($(this).html());
                        }
                    });
                }

            },
            error: function(data) {
                // stop_loading();
                console.log('Error:', data);

                // show_alert_dialog('99', data);
            }
        });
    }


</script>

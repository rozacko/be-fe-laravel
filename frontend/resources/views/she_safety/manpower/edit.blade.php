<div class="modal-header pb-0 border-0 justify-content-end">
    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">					
        <i class="fa fa-close" style="font-size:24px"></i>
    </div>
</div>
<div class="modal-body scroll-y px-10 px-lg-15 pt-0 pb-15">
    <div class="mb-13 text-center">
        <h1 class="mb-3">Edit Data</h1>
    </div>
    <form action="{{ route($route . '.update', $data->id) }}" enctype="multipart/form-data" id="idForm">
        <div class="modal-body">
        @csrf
        <div class="row d-flex mb-4 fv-row form-group">
                <div class="col-2">
                    <label class="d-flex align-items-center fs-3 fw-semibold mb-2">
                        <span class="required">Bulan</span>                    
                    </label>
                </div>
                <div class="col-3">
                    <select id="select_bulan" class="form-select h-25 select_option" data-placeholder="Pilih Plant" name="bulan">
                        <option value=""></option>
                        @foreach($bulan as $key => $value)                    
                            <option value="{{$value['data']}}" {{$value['data'] == $data->bulan ? "selected" : ""}}>{{$value['desc']}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-2">
                </div>
                <div class="col-2">
                    <label class="d-flex align-items-center fs-3 fw-semibold mb-2">
                        <span class="required">Tahun</span>                    
                    </label>
                </div>
                <div class="col-3">
                <select id="select_tahun" class="form-select h-25 select_option" data-placeholder="Pilih Plant" name="tahun">
                        <option value=""></option>
                        @foreach($tahun as $key => $value)                    
                            <option value="{{$value}}" {{$value == $data->tahun ? "selected" : ""}}>{{$value}}</option>
                        @endforeach
                    </select>
                </div>
            </div>                   
            <div class="d-flex flex-column mb-8 fv-row form-group">            
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">Nama Perusahaan</span>                    
                </label>
                <input type="text" class="form-control form-control-solid" placeholder="Enter Nama Perusahaan" name="nm_perusahaan" value="{{ $data->nm_perusahaan }}"/>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">            
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">Jumlah Pekerja</span>
                </label>
                <input type="number" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" class="form-control form-control-solid" name="jml_orang" value="{{ $data->jml_orang }}"/>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">            
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">Status</span>
                </label>
                <input type="text" class="form-control form-control-solid" placeholder="Enter Status" name="status" value="{{ $data->status }}"/>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">            
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">Keterangan</span>
                </label>
                <input type="text" class="form-control form-control-solid" placeholder="Enter Keterangan" name="keterangan" value="{{ $data->keterangan }}"/>
            </div>            
        </div>
        <div class="modal-footer">
            <div class="text-center">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary">
                    <span class="indicator-label">Submit</span>
                    <span class="indicator-progress">Please wait... 
                    <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                </button>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    $(".select_option").select2({
        dropdownParent: $('#modal')
    });
    $("#idForm").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.
        var form = $(this);
        var actionUrl = form.attr('action');
            $.ajax({
                type: "patch",
                url: actionUrl,
                data: form.serialize(), 
                success: function(data)
                {
                    if(data.status == 'success'){
                        Swal.fire(
                            'Success',
                            data.message,
                            'success'
                        );
                        $('#modal').modal('hide');
                        $('#tb_manpower').DataTable().ajax.reload();
                    }else{
                        Swal.fire(
                            'Warning',
                            data.message,
                            'warning'
                        );
                    }
                }
            });
        });
</script>

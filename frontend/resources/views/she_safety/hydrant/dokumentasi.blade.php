<div class="modal-header pb-0 border-0 justify-content-end">
    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">					
        <i class="fa fa-close" style="font-size:24px"></i>
    </div>
</div>
<div class="modal-body scroll-y px-10 px-lg-15 pt-0 pb-15">
    <div class="mb-13 text-center">
        <h1 class="mb-3">Dokumentasi</h1>
    </div>
    <form method="POST" action="{{ route($route . '.store_doc') }}" enctype="multipart/form-data" id="idFormDoc">
        <div class="modal-body">
        @csrf   
            <div class="row d-flex mb-8 fv-row form-group">
                <div class="col-5">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Deskripsi</span>
                        </label>
                        <input type="text" class="form-control " name="deskripsi" />
                        <input type="text" class="form-control " name="id_hydrant" value="{{$id}}" hidden/>
                    </div>
                </div>    
                <div class="col-2">
                </div>
                <div class="col-5">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            Dokumentasi 
                        </label>
                        <p class="mt-2" style="color: #1F7793;">*format file .jpg .png .jpeg</p>                    
                        <input type="file" id="default_file" name="upload_file" accept="image/png, image/jpeg, image/jpg" style="display: flex !important;"/>
                        <input type="text" id="file" name="file" hidden/>
                    </div>
                </div>            
            </div>          
        </div>
        <div class="modal-footer">
            <div class="text-center">
                <button type="submit" class="btn btn-primary">
                    <span class="indicator-label">Tambah Data</span>
                    <span class="indicator-progress">Please wait... 
                    <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                </button>
            </div>
        </div>
    </form>
    <table class="table table-bordered data-table" id="tb_dokumentasi">
        <thead>
            <tr>
            <th>No</th>
            <th>Deskripsi</th>
            <th>File</th>
            <th>Action</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    <div class="modal-footer">
        <div class="text-center">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>                
        </div>
    </div>
</div>


<script type="text/javascript">
    $(function() {
        var table = $('#tb_dokumentasi').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route($route.'.table_doc', $id) }}",
            columns: [{ 
                data: 'DT_RowIndex', 
                name: 'DT_RowIndex', 
                orderable: false, 
                searchable: false },
                {
                data: 'deskripsi',
                name: 'deskripsi'
                },
                {
                data: 'files',
                render : function(data, type, row, meta) {
                    if(data != '')
                        return '<a href="'+data+'" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-download" style="padding-right: 0px; font-size:10;"></i></a> ';
                    else
                        return '';
                }
                },
                {
                data: 'action',
                name: 'action',
                orderable: false,
                searchable: false,
                width: 150
                },
            ]
        });
    });

    $("#idFormDoc").submit(function(e) {
    e.preventDefault(); // avoid to execute the actual submit of the form.
    var form = $(this);
    var actionUrl = form.attr('action');
        $.ajax({
            type: "POST",
            url: actionUrl,
            data: form.serialize(), 
            success: function(data)
            {
                if(data.status == 'success'){
                    Swal.fire(
                        'Success',
                        data.message,
                        'success'
                    );                    
                    $('#tb_dokumentasi').DataTable().ajax.reload();
                }else{
                    Swal.fire(
                        'Warning',
                        data.message,
                        'warning'
                    );
                }
            }
        });
    });

    function btn_delete_doc(id){
        event.preventDefault(); 
        var url = '{{ route($route.".destroy_doc", ":id") }}';
        url = url.replace(':id', id);    
        Swal.fire({
            icon: 'warning',
            title: 'Apakah anda yakin?',
            text: "Data ini akan di hapus!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak',
            confirmButtonClass: 'btn btn-primary margin-right-10',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: true,
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function(isConfirm) {
            if (isConfirm.isConfirmed) {
                $.post(url, {_token: "{{ csrf_token() }}", _method: 'DELETE' }, function(res) {
                    if (res.status == 'success') {
                        Swal.fire(
                            'Success',
                            res.message,
                            'success'
                        );                    
                        $('#tb_dokumentasi').DataTable().ajax.reload();
                    } else {
                        Swal.fire(
                            'Gagal',
                            res.message,
                            'error'
                        );
                    }
                }, 'json');
            } else {
                Swal.fire(
                    'Cancelled',
                    'Your data is safe',
                    'error'
                );
            } 
        });

        return false;
    }

    $("#default_file").change(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form. 
        var input = this;
        var files = $(this)[0].files;
        var fd = new FormData();
        // Append data 
        fd.append('upload_file',files[0]);
        fd.append('_token', "{{ csrf_token() }}");
        var actionUrl = "{{ route($route . '.upload_file') }}";
        $.ajax({
            type: "POST",
            url: actionUrl,
            enctype: 'multipart/form-data',
            data: fd,
            cache:false,
            contentType: false,
            processData: false, 
            success: function(data)
            {
                if(data.status == 'success'){
                    Swal.fire(
                        'Success',
                        data.message,
                        'success'
                    );
                    $("#file").val(data.data);
                }else{
                    Swal.fire(
                        'Warning',
                        data.message,
                        'warning'
                    );
                }
            }
        });
    });
</script>

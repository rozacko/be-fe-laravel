<div class="modal-header pb-0 border-0 justify-content-end">
    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">					
        <i class="fa fa-close" style="font-size:24px"></i>
    </div>
</div>
<div class="modal-body scroll-y px-10 px-lg-15 pt-0 pb-15">
    <div class="mb-13 text-center">
        <h1 class="mb-3">Edit Data</h1>
    </div>
    <form action="{{ route($route . '.update', $data->id) }}" enctype="multipart/form-data" id="idForm">
        <div class="modal-body">            
        @csrf
            <div class="row d-flex mb-8 fv-row form-group">
                <div class="col-3">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Bulan</span>
                        </label>
                        <select id="select_bulan" class="form-select h-25 select_option" data-placeholder="Pilih Bulan" name="bulan">
                            <option value=""></option>
                            @foreach($bulan as $key => $value)                    
                                <option value="{{$value['data']}}" {{$value['data'] == $data->bulan ? "selected" : ""}}>{{$value['desc']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-2">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Tahun</span>
                        </label>
                        <select id="select_tahun" class="form-select h-25 select_option" data-placeholder="Pilih Tahun" name="tahun">
                            <option value=""></option>
                            @foreach($tahun as $key => $value)                    
                                <option value="{{$value}}" {{$value == $data->tahun ? "selected" : ""}}>{{$value}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-2">
                </div>
                <div class="col-5">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Main Pump</span>
                        </label>
                        <input type="text" class="form-control " name="main_pump" value="{{ $data->main_pump }}"/>
                    </div>
                </div>
            </div>
            <div class="row d-flex mb-8 fv-row form-group">
                <div class="col-5">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Lokasi</span>
                        </label>
                        <input type="text" class="form-control " name="lokasi" value="{{ $data->lokasi }}"/>
                    </div>
                </div>
                <div class="col-2">
                </div>
                <div class="col-5">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">No Pilar</span>
                        </label>
                        <input type="text" class="form-control " name="no_pilar" value="{{ $data->no_pilar }}"/>
                    </div>
                </div>
            </div>
            <div class="row d-flex mb-8 fv-row form-group">
                <div class="col-5">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Cek</span>
                        </label>
                        <input type="text" class="form-control " name="cek" value="{{ $data->cek }}"/>
                    </div>
                </div>
            </div>
            <div class="row d-flex mb-8 fv-row form-group">
                <div class="col-5">
                    <div class="align-items-center"> 
                        <h2><b>Pilar</b></h2>
                    </div>
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Body</span>
                        </label>
                        <select id="select_pilar" class="form-select h-25" data-placeholder="Pilih Status" name="p_body">
                            <option value=""></option>
                            @foreach($kondisi as $key => $value)                    
                                <option value="{{$value['id']}}"  {{$value['id'] == $data->p_body ? "selected" : ""}}>{{$value['desc']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-2">
                </div>
                <div class="col-5">                    
                    <div class="align-items-center"> 
                        <h2><b>Valve Kopling</b></h2>
                    </div>
                    <div class="row">
                        <div class="col-4">
                            <div class="d-flex flex-column mb-8 fv-row form-group">            
                                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                                    <span class="required">Kiri</span>
                                </label>
                                <select id="select_kiri" class="form-select h-25" data-placeholder="Pilih Status" name="v_kiri">
                                    <option value=""></option>
                                    @foreach($kondisi as $key => $value)                    
                                        <option value="{{$value['id']}}" {{$value['id'] == $data->v_kiri ? "selected" : ""}}>{{$value['desc']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="d-flex flex-column mb-8 fv-row form-group">            
                                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                                    <span class="required">Atas</span>
                                </label>
                                <select id="select_atas" class="form-select h-25" data-placeholder="Pilih Status" name="v_atas">
                                    <option value=""></option>
                                    @foreach($kondisi as $key => $value)                    
                                        <option value="{{$value['id']}}" {{$value['id'] == $data->v_atas ? "selected" : ""}}>{{$value['desc']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="d-flex flex-column mb-8 fv-row form-group">            
                                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                                    <span class="required">Kanan</span>
                                </label>
                                <select id="select_kanan" class="form-select h-25" data-placeholder="Pilih Status" name="v_kanan">
                                    <option value=""></option>
                                    @foreach($kondisi as $key => $value)                    
                                        <option value="{{$value['id']}}" {{$value['id'] == $data->v_kanan ? "selected" : ""}}>{{$value['desc']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>                    
                </div>
            </div>
            <div class="row d-flex mb-4 fv-row form-group">
                <div class="col-5">
                    <div class="d-flex flex-column mb-4 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            Keterangan
                        </label>
                        <textarea name="p_ket" cols="30" rows="2" class="wd-100 form-control form-control-lg">{{$data->p_ket}}</textarea>
                    </div>
                </div>
                <div class="col-2">
                </div>  
                <div class="col-5">
                    <div class="d-flex flex-column mb-4 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            Keterangan
                        </label>
                        <textarea name="v_ket" cols="30" rows="2" class="wd-100 form-control form-control-lg">{{$data->v_ket}}</textarea>
                    </div>
                </div>
            </div>
            <div class="row d-flex mb-8 fv-row form-group">
                <div class="col-5">
                    <div class="align-items-center"> 
                        <h2><b>Kopling</b></h2>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="d-flex flex-column mb-8 fv-row form-group">            
                                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                                    <span class="required">Kiri</span>
                                </label>
                                <select id="select_kiri_k" class="form-select h-25" data-placeholder="Pilih Status" name="k_kiri">
                                    <option value=""></option>
                                    @foreach($kondisi as $key => $value)                    
                                        <option value="{{$value['id']}}" {{$value['id'] == $data->k_kiri ? "selected" : ""}}>{{$value['desc']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="d-flex flex-column mb-8 fv-row form-group">            
                                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                                    <span class="required">Kanan</span>
                                </label>
                                <select id="select_kanan_k" class="form-select h-25" data-placeholder="Pilih Status" name="k_kanan">
                                    <option value=""></option>
                                    @foreach($kondisi as $key => $value)                    
                                        <option value="{{$value['id']}}" {{$value['id'] == $data->k_kanan ? "selected" : ""}}>{{$value['desc']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div> 
                </div>
                <div class="col-2">
                </div>
                <div class="col-5">                    
                    <div class="align-items-center"> 
                        <h2><b>Tutup Kopling</b></h2>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="d-flex flex-column mb-8 fv-row form-group">            
                                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                                    <span class="required">Kiri</span>
                                </label>
                                <select id="select_kiri_tk" class="form-select h-25" data-placeholder="Pilih Status" name="tk_kiri">
                                    <option value=""></option>
                                    @foreach($kondisi as $key => $value)                    
                                        <option value="{{$value['id']}}" {{$value['id'] == $data->tk_kiri ? "selected" : ""}}>{{$value['desc']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="d-flex flex-column mb-8 fv-row form-group">            
                                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                                    <span class="required">Kanan</span>
                                </label>
                                <select id="select_kanan_tk" class="form-select h-25" data-placeholder="Pilih Status" name="tk_kanan">
                                    <option value=""></option>
                                    @foreach($kondisi as $key => $value)                    
                                        <option value="{{$value['id']}}" {{$value['id'] == $data->tk_kanan ? "selected" : ""}}>{{$value['desc']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>                    
                </div>
            </div>
            <div class="row d-flex mb-4 fv-row form-group">
                <div class="col-5">
                    <div class="d-flex flex-column mb-4 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            Keterangan
                        </label>
                        <textarea name="k_ket" cols="30" rows="2" class="wd-100 form-control form-control-lg">{{$data->k_ket}}</textarea>
                    </div>
                </div>
                <div class="col-2">
                </div>  
                <div class="col-5">
                    <div class="d-flex flex-column mb-4 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            Keterangan
                        </label>
                        <textarea name="tk_ket" cols="30" rows="2" class="wd-100 form-control form-control-lg">{{$data->tk_ket}}</textarea>
                    </div>
                </div>
            </div>
            <div class="row d-flex mb-8 fv-row form-group">
                <div class="align-items-center"> 
                    <h2><b>Box</b></h2>
                </div>
                <div class="col-5">                    
                    <div class="row">
                        <div class="col-6">
                            <div class="d-flex flex-column mb-8 fv-row form-group">            
                                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                                    <span class="required">Casing</span>
                                </label>
                                <select id="select_casing" class="form-select h-25" data-placeholder="Pilih Status" name="b_casing">
                                    <option value=""></option>
                                    @foreach($kondisi as $key => $value)                    
                                        <option value="{{$value['id']}}" {{$value['id'] == $data->b_casing ? "selected" : ""}}>{{$value['desc']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="d-flex flex-column mb-8 fv-row form-group">            
                                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                                    <span class="required">Hose</span>
                                </label>
                                <select id="select_hose" class="form-select h-25" data-placeholder="Pilih Status" name="b_hose">
                                    <option value=""></option>
                                    @foreach($kondisi as $key => $value)                    
                                        <option value="{{$value['id']}}" {{$value['id'] == $data->b_hose ? "selected" : ""}}>{{$value['desc']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div> 
                </div>
                <div class="col-2">
                </div>
                <div class="col-5">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            Press Bar
                        </label>
                        <input type="number" id="press_bar" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" class="form-control" name="press_bar" value="{{ $data->press_bar }}" />
                    </div>
                </div>
            </div>
            <div class="row d-flex mb-8 fv-row form-group">
                <div class="col-5">
                    <div class="row">
                        <div class="col-6">
                            <div class="d-flex flex-column mb-8 fv-row form-group">            
                                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                                    <span class="required">Nozle</span>
                                </label>
                                <select id="select_nozle" class="form-select h-25" data-placeholder="Pilih Status" name="b_nozle">
                                    <option value=""></option>
                                    @foreach($kondisi as $key => $value)                    
                                        <option value="{{$value['id']}}" {{$value['id'] == $data->b_nozle ? "selected" : ""}}>{{$value['desc']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="d-flex flex-column mb-8 fv-row form-group">            
                                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                                    <span class="required">Kunci</span>
                                </label>
                                <select id="select_kunci" class="form-select h-25" data-placeholder="Pilih Status" name="b_kunci">
                                    <option value=""></option>
                                    @foreach($kondisi as $key => $value)                    
                                        <option value="{{$value['id']}}" {{$value['id'] == $data->b_kunci ? "selected" : ""}}>{{$value['desc']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div> 
                </div>
                <div class="col-2">
                </div>
                <div class="col-5">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            Keterangan
                        </label>
                        <input type="text" class="form-control form-control-solid" name="" value="{{ $data->pb_ket }}" readonly/>
                    </div>
                </div>
            </div>
            <div class="row d-flex mb-4 fv-row form-group">
                <div class="col-5">
                    <div class="d-flex flex-column mb-4 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            Keterangan
                        </label>
                        <textarea name="b_ket" cols="30" rows="2" class="wd-100 form-control form-control-lg">{{$data->b_ket}}</textarea>
                    </div>
                </div>
            </div>
            <div class="row d-flex mb-8 fv-row form-group">
                <div class="align-items-center"> 
                    <h2><b>Status</b></h2>
                </div>
                <div class="col-5">                    
                    <div class="row">
                        <div class="col-6">
                            <div class="d-flex flex-column mb-8 fv-row form-group">            
                                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                                    Pilar
                                </label>
                                <input type="text" class="form-control form-control-solid" name="" value="{{ $data->p_status }}" readonly/>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="d-flex flex-column mb-8 fv-row form-group">            
                                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                                    Valve
                                </label>
                                <input type="text" class="form-control form-control-solid" name="" value="{{ $data->v_status }}" readonly/>
                            </div>
                        </div>
                    </div> 
                </div>
                <div class="col-2">
                </div>
                <div class="col-5">
                <div class="align-items-center"> 
                        <h2><b></b></h2>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="d-flex flex-column mb-8 fv-row form-group">            
                                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                                    Kopling
                                </label>
                                <input type="text" class="form-control form-control-solid" name="" value="{{ $data->k_status }}" readonly/>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="d-flex flex-column mb-8 fv-row form-group">            
                                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                                    Kondisi
                                </label>
                                <input type="text" class="form-control form-control-solid" name="" value="{{ $data->status_kondisi }}" readonly/>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>    
        </div>
        <div class="modal-footer">
            <div class="text-center">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary">
                    <span class="indicator-label">Submit</span>
                    <span class="indicator-progress">Please wait... 
                    <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                </button>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    $(".select_option").select2({
        dropdownParent: $('#modal')
    });
    $("#idForm").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.
        var form = $(this);
        var actionUrl = form.attr('action');
        console.log(actionUrl);
            $.ajax({
                type: "patch",
                url: actionUrl,
                data: form.serialize(), 
                success: function(data)
                {
                    if(data.status == 'success'){
                        Swal.fire(
                            'Success',
                            data.message,
                            'success'
                        );
                        $('#modal').modal('hide');
                        $('#tb_hydrant').DataTable().ajax.reload();
                    }else{
                        Swal.fire(
                            'Warning',
                            data.message,
                            'warning'
                        );
                    }
                }
            });
        });
</script>

<div class="modal-header pb-0 border-0 justify-content-end">
    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">					
        <i class="fa fa-close" style="font-size:24px"></i>
    </div>
</div>
<div class="modal-body scroll-y px-10 px-lg-15 pt-0 pb-15">
    <div class="mb-13 text-center">
        <h1 class="mb-3">Edit Data</h1>
    </div>
    <form action="{{ route($route . '.update', $data->id) }}" enctype="multipart/form-data" id="idForm">
        <div class="modal-body">
        @csrf
            <div class="row d-flex mb-8 fv-row form-group">
                <div class="col-3">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Bulan</span>
                        </label>
                        <select id="select_bulan" class="form-select h-25 select_option" data-placeholder="Pilih Plant" name="bulan">
                            <option value=""></option>
                            @foreach($bulan as $key => $value)                    
                                <option value="{{$value['data']}}" {{$value['data'] == $data->bulan ? "selected" : ""}}>{{$value['desc']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-2">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Tahun</span>
                        </label>
                        <select id="select_tahun" class="form-select h-25 select_option" data-placeholder="Pilih Plant" name="tahun">
                            <option value=""></option>
                            @foreach($tahun as $key => $value)                    
                                <option value="{{$value}}" {{$value == $data->tahun ? "selected" : ""}}>{{$value}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-2">
                </div>
                <div class="col-5">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Status</span>
                        </label>
                        <select id="select_status" class="form-select h-25 select_option" data-placeholder="Pilih Status" name="status">
                            <option value=""></option>                            
                            <option value="BAIK" {{"BAIK" == $data->status ? "selected" : ""}}>BAIK</option>
                            <option value="HABIS" {{"HABIS" == $data->status ? "selected" : ""}}>HABIS</option>                            
                            <option value="REFILL" {{"REFILL" == $data->status ? "selected" : ""}}>REFILL</option>                            
                            <option value="PASANG BARU" {{"PASANG BARU" == $data->status ? "selected" : ""}}>PASANG BARU</option>                            
                            <option value="DITARIK" {{"DITARIK" == $data->status ? "selected" : ""}}>DITARIK</option>                            
                            <option value="DROP" {{"DROP" == $data->status ? "selected" : ""}}>DROP</option>                            
                            <option value="-" {{"-" == $data->status ? "selected" : ""}}>-</option>                            
                        </select>                       
                    </div>
                </div>    
            </div>
            <div class="row d-flex mb-8 fv-row form-group">                
                <div class="col-5">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Area</span>
                        </label>
                        <input type="text" class="form-control " name="area" value="{{ $data->area }}"/>
                    </div>
                </div>
                <div class="col-2">
                </div>
                <div class="col-5">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Jenis Apar</span>
                        </label>
                        <input type="text" class="form-control " name="jenis_apar" value="{{ $data->jenis_apar }}"/>
                    </div>
                </div>
            </div>
            <div class="row d-flex mb-8 fv-row form-group">                
                <div class="col-5">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Unit Kerja</span>
                        </label>
                        <input type="text" class="form-control " name="unit_kerja" value="{{ $data->unit_kerja }}"/>
                    </div>
                </div>                
            </div>
            <div class="row d-flex mb-8 fv-row form-group">                
                <div class="col-5">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Lokasi</span>
                        </label>
                        <input type="text" class="form-control " name="lokasi" value="{{ $data->lokasi }}"/>
                    </div>
                </div>
                <div class="col-2">
                </div>
                <div class="col-5">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            Map Baru
                        </label>
                        <input type="text" class="form-control " name="map_baru" value="{{ $data->map_baru }}"/>
                    </div>
                </div>
            </div>
            <div class="row d-flex mb-8 fv-row form-group">                
                <div class="col-5">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Clamp</span>
                        </label>
                        <input type="text" class="form-control " name="clamp" value="{{ $data->clamp }}"/>
                    </div>
                </div>
                <div class="col-2">
                </div>
                <div class="col-5">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Press (Bar)</span>
                        </label>
                        <input type="number" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" class="form-control " name="press_bar" value="{{ $data->press_bar }}"/>
                    </div>
                </div>
            </div>
            <div class="row d-flex mb-8 fv-row form-group">                                
                <div class="col-5">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Berat (Kg)</span>
                        </label>
                        <input type="number" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" class="form-control " name="berat" value="{{ $data->berat }}"/>
                    </div>
                </div>
            </div>
            <div class="row d-flex mb-8 fv-row form-group">                
                <div class="col-1">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="form-check form-check-custom flex-stack">                            
                            <input class="form-check-input" type="checkbox" value="1" name="hose" {{ $data->hose == true ? "checked" : ""}}/>
                            <span class="form-check-label text-gray-700 fs-6 fw-semibold ms-0 me-2">Hose</span>
                        </label>
                    </div>
                </div>
                <div class="col-1">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="form-check form-check-custom flex-stack">                            
                            <input class="form-check-input" type="checkbox" value="1" name="segel" {{ $data->segel == true ? "checked" : ""}}/>
                            <span class="form-check-label text-gray-700 fs-6 fw-semibold ms-0 me-2">Segel</span>
                        </label>
                    </div>
                </div>
                <div class="col-1">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="form-check form-check-custom flex-stack">                            
                            <input class="form-check-input" type="checkbox" value="1" name="sapot" {{ $data->sapot == true ? "checked" : ""}}/>
                            <span class="form-check-label text-gray-700 fs-6 fw-semibold ms-0 me-2">Sapot</span>
                        </label>
                    </div>
                </div>
                <div class="col-4">
                </div>
                <div class="col-3">
                    <div class="d-flex flex-column mb-8 fv-row form-group">                                    
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Masa Aktif Bulan</span>
                        </label>
                        <select id="select_bulan_aktf" class="form-select h-25" data-placeholder="Pilih Bulan" name="bulan_aktf">
                            <option value=""></option>
                            @foreach($bulan as $key => $value)      
                                <option value="{{$value['data']}}" {{$value['data'] == $data->bulan_aktf ? "selected" : ""}}>{{$value['desc']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-2">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Tahun</span>
                        </label>
                        <select id="select_tahun_aktf" class="form-select h-25" data-placeholder="Pilih Tahun" name="tahun_aktf">
                            <option value=""></option>
                            @foreach($tahun as $key => $value)
                                <option value="{{$value}}" {{$value == $data->tahun_aktf ? "selected" : ""}}>{{$value}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="row d-flex mb-8 fv-row form-group">
                <div class="col-5">
                    <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                        Dokumentasi
                    </label>
                    <p class="mt-2" style="color: #1F7793;">*format file .jpg .png .jpeg</p>
                    <input type="file" id="default_file" name="upload_file" accept="image/*" style="display: flex !important;"/>
                    <input type="text" id="file" name="file" value="{{ $data->file }}" hidden/>
                </div>
                <div class="col-2">
                </div>
                <div class="col-5">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            Link Google Drive
                        </label>
                        <input type="text" class="form-control " name="file_gdrive"  value="{{ $data->file_gdrive }}"/>
                    </div>
                </div>
            </div>    
            <div class="row d-flex mb-8 fv-row form-group">                
                <div class="col-5">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            Keterangan
                        </label>
                        <textarea name="keterangan" cols="30" rows="2" class="wd-100 form-control form-control-lg">{{ $data->keterangan }}</textarea>
                    </div>
                </div>
            </div>   
        </div>
        <div class="modal-footer">
            <div class="text-center">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary">
                    <span class="indicator-label">Submit</span>
                    <span class="indicator-progress">Please wait... 
                    <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                </button>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    $(".select_option").select2({
        dropdownParent: $('#modal')
    });
    $("#idForm").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.
        var form = $(this);
        var actionUrl = form.attr('action');
        $.ajax({
            type: "patch",
            url: actionUrl,
            data: form.serialize(), 
            success: function(data)
            {
                if(data.status == 'success'){
                    Swal.fire(
                        'Success',
                        data.message,
                        'success'
                    );
                    $('#modal').modal('hide');
                    $('#tb_apar').DataTable().ajax.reload();
                }else{
                    Swal.fire(
                        'Warning',
                        data.message,
                        'warning'
                    );
                }
            }
        });
    });

    $("#default_file").change(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form. 
        var input = this;
        var files = $(this)[0].files;
        var fd = new FormData();
        // Append data 
        fd.append('upload_file',files[0]);
        fd.append('_token', "{{ csrf_token() }}");
        var actionUrl = "{{ route($route . '.upload_file') }}";
        $.ajax({
            type: "POST",
            url: actionUrl,
            enctype: 'multipart/form-data',
            data: fd,
            cache:false,
            contentType: false,
            processData: false, 
            success: function(data)
            {
                if(data.status == 'success'){
                    Swal.fire(
                        'Success',
                        data.message,
                        'success'
                    );
                    $("#file").val(data.data);
                }else{
                    Swal.fire(
                        'Warning',
                        data.message,
                        'warning'
                    );
                }
            }
        });
    });
</script>

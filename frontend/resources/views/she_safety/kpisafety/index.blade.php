{{-- Extends layout --}}
<x-default-layout>
<div class="card card-flush mt-6 mt-xl-9">
    <div class="py-lg-5 p-3">
        <div class="row">
            <div class="col-md-6">
                <div class="information">
                     <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{$pagetitle}}</h1>                       
                     <ul class="breadcrumb breadcrumb-separatorless ">
                        @foreach($breadcrumb as $key => $value)
                            @if ($key === array_key_first($breadcrumb)) 
                                <li class="breadcrumb-item text-muted">{{$key . ' /'}}
                                </li>
                            @endif

                            @if ($key === array_key_last($breadcrumb)) 
                                <li class="breadcrumb-item text-ghopo">   
                                    <a href="{{$value}}" class="text-ghopo text-hover-primary">{{$key}}</a>
                                </li>
                            @endif                            
                        @endforeach
                        
                    </ul>
                </div>
            </div>
        </div>
    </div>
	<div class="card-body pt-0">
        <form method="POST" action="{{ route($route . '.store') }}" enctype="multipart/form-data" id="idForm">
            @csrf 
                <div class="row d-flex mb-4 fv-row form-group">                    
                    <div class="col-2">
                        <label class="d-flex align-items-center fs-3 fw-semibold mb-2">
                            <span class="required">Tahun</span>                    
                        </label>
                    </div>
                    <div class="col-3">
                        <select id="filter_tahun" class="form-select h-25 select_filter" data-placeholder="Pilih Tahun" name="tahun">
                            <option value=""></option>
                            @foreach($tahun as $key => $value)                    
                                <option value="{{$value}}" {{$value == date("Y") ? "selected" : ""}}>{{$value}}</option>
                            @endforeach
                        </select>
                    </div>
                </div> 
                <div class="row d-flex mb-4 fv-row form-group">                    
                    <div class="col-2">
                        <label class="d-flex align-items-center fs-3 fw-semibold mb-2">
                            <span class="required">Konstanta 1 (%)</span>                  
                        </label>
                    </div>
                    <div class="col-3">
                        <input type="number" id="kons1" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" class="form-control" name="kons1" />
                    </div>
                </div>
                <div class="row d-flex mb-4 fv-row form-group">                    
                    <div class="col-2">
                        <label class="d-flex align-items-center fs-3 fw-semibold mb-2">
                            <span class="required">Konstanta 2 (%)</span>                  
                        </label>
                    </div>
                    <div class="col-3">
                        <input type="number" id="kons2" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" class="form-control" name="kons2" />
                    </div>
                </div>
                <div class="row d-flex mb-4 fv-row form-group">                    
                    <div class="col-2">
                        <label class="d-flex align-items-center fs-3 fw-semibold mb-2">
                            <span class="required">Target LTISR</span>
                        </label>
                    </div>
                    <div class="col-3">
                        <input type="number" min="1" id="target1" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" class="form-control" name="target1" />
                    </div>
                </div>
                <div class="row d-flex mb-4 fv-row form-group">                    
                    <div class="col-2">
                        <label class="d-flex align-items-center fs-3 fw-semibold mb-2">
                            <span class="required">Target NoFA</span>
                        </label>
                    </div>
                    <div class="col-3">
                        <input type="number" min="1" id="target2" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" class="form-control" name="target2" />
                    </div>
                </div>                
                <div class="d-flex flex-column mb-8 fv-row text-center">
                    <div class="col-2">
                        <button type="submit" class="btn btn-primary">
                            <span class="indicator-label">Simpan Perubahan</span>
                            <span class="indicator-progress">Please wait... 
                            <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                        </button>
                    </div>
                </div>
            </div> 
        </form>
	</div>
</div>

</x-default-layout>

<script>    
    $(function() {
        $(".select_filter").select2();
        show();
    });

    $('#get_data').click(function (e) {
        e.preventDefault(); 
        show();
    })

    $("#filter_tahun").on('change', function(){
        show();
    });

    function show(){
        var tahun = $("#filter_tahun").val();
        var url = '{{ route($route.".show", ":tahun") }}';
        url = url.replace(':tahun', tahun);  
        $.get(url, function (ret) {
            $("#kons1").val(ret.kons1);
            $("#kons2").val(ret.kons2);
            $("#target1").val(ret.target1);
            $("#target2").val(ret.target2);
        }).fail(function () {
            Swal.fire(
                'Warning',
                'Something error',
                'warning'
            );
        });          
    }

    $("#idForm").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.
        var form = $(this);
        var actionUrl = form.attr('action');
        $.ajax({
            type: "POST",
            url: actionUrl,
            data: form.serialize(), 
            success: function(data)
            {
                if(data.status == 'success'){
                    Swal.fire(
                        'Success',
                        data.message,
                        'success'
                    );
                    document.getElementById("get_data").click();                    
                }else{
                    Swal.fire(
                        'Warning',
                        data.message,
                        'warning'
                    );
                }
            }
        });
    });
</script>

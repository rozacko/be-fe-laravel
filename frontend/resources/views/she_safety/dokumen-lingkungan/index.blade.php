{{-- Extends layout --}}
<x-default-layout>
    <div class="card card-flush mt-6 mt-xl-9">
        <div class="py-lg-5 p-3">
            <div class="row">
                <div class="col-md-6">
                    <div class="information">
                        <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">
                            {{ $pagetitle }}</h1>
                        <ul class="breadcrumb breadcrumb-separatorless ">
                            @foreach ($breadcrumb as $key => $value)
                                @if ($key === array_key_first($breadcrumb))
                                    <li class="breadcrumb-item text-muted">{{ $key . ' /' }}
                                    </li>
                                @endif

                                @if ($key === array_key_last($breadcrumb))
                                    <li class="breadcrumb-item text-ghopo">
                                        <a href="{{ $value }}"
                                            class="text-ghopo text-hover-primary">{{ $key }}</a>
                                    </li>
                                @endif
                            @endforeach

                        </ul>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="">
                        <div class="row d-flex justify-content-end">
                            <div class="col-md-3" id="loader_filter_area">
                                <select class="form-select" data-placeholder="Area" id="filter_area">
                                    @foreach ($area as $value)
                                        <option value="{{ $value->nama_area }}">{{ $value->nama_area }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-3">
                                <select class="form-select" data-placeholder="Pilih Tahun" id="filter_year">
                                    @foreach (getYear() as $key => $value)
                                        @if ($value == date('Y'))
                                            <option value="{{ $value }}" selected>{{ $value }}</option>
                                        @else
                                            <option value="{{ $value }}">{{ $value }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-3" id="loader_filter_jenis_dokumen">
                                <select class="form-select" data-placeholder="jenis_dokumen" id="jenis_dokumen">
                                    @foreach ($jenisDokumen as $value)
                                        <option value="{{ $value->id }}">{{ $value->nama_dokumen }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-3">
                                <button class="btn btn-history w-100" id="get_data">Tampilkan</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-header p-3">
            <div class="card-title flex-column">
                <h3 class="fw-bold mb-1"></h3>
            </div>
            <div class="card-toolbar my-1">
                <button type="button" class="btn btn-sm btn-primary float-right btn-add ms-5">
                    Tambah Data
                </button>
            </div>
        </div>
        <div class="card-body pt-0">
            <table class="table table-bordered data-table" id="tb_data">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Dokumen Lingkungan</th>
                        <th>Judul</th>
                        <th>Area</th>
                        <th>Ruang Lingkup</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>

        </div>
    </div>

    <div class="modal fade" id="modal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered mw-1000px">
            <div class="modal-content rounded"></div>
        </div>
    </div>

</x-default-layout>

<script>
    $(function() {
        $(".select_filter").select2();
    });

    let area = $('#filter_area').val();
    let tahun = $('#filter_year').val();
    var url = '{{ route($route . '.table', [':area', ':tahun']) }}';
    url = url.replace(':area', area);
    url = url.replace(':tahun', tahun);

    var table = $('#tb_data').DataTable({
        processing: true,
        serverSide: true,
        ajax: url,
        columns: [{
                data: 'DT_RowIndex',
                name: 'DT_RowIndex'
            },
            {
                data: 'no_dokumen',
                name: 'no_dokumen'
            },
            {
                data: 'judul',
                name: 'judul'
            },
            {
                data: 'nm_area',
                name: 'nm_area'
            },
            {
                data: 'ruang_lingkup',
                name: 'ruang_lingkup'
            },
            {
                data: 'uuid',
                render: function(data, type, row, meta) {
                    if (data)
                        return `<a href="javascript:;" onclick="edit('${data}')" class="btn btn-primary btn-sm"><i class="fa fa-edit" style="padding-right: 0px; font-size:10;"></i></a> <a href="#" class="btn btn-danger btn-sm" onclick="btn_delete('${data}')"><i class="fa fa-trash" style="padding-right: 0px; font-size:10;"></i></a>`;
                    else
                        return '<a href="#" class="btn btn-danger btn-sm"><i class="fa fa-close" style="padding-right: 0px; font-size:10;"></i></a> ';
                }
            },
        ]
    });

    setTimeout(function() {
        if ($('#alertNotif').length > 0) {
            $('#alertNotif').remove();
        }
    }, 5000)

    $('.btn-add').click(function(e) {
        e.preventDefault();
        var url = "{{ route($route . '.create') }}";
        $.get(url, function(html) {
            $('#modal .modal-content').html(html);
            $('#modal').modal('show');
        }).fail(function() {
            Swal.fire(
                'Warning',
                'Something error',
                'warning'
            );
        });
    })

    function edit(uuid) {
        var url = `{{ url('she_safety/dokumen-lingkungan/edit') }}/` + uuid;
        $.get(url, function(html) {
            $('#modal .modal-content').html(html);
            $('#modal').modal('show');
        }).fail(function() {
            Swal.fire(
                'Warning',
                'Something error',
                'warning'
            );
        });
    }

    $('#get_data').click(function(e) {
        e.preventDefault();
        let area = $('#filter_area').val();
        let tahun = $('#filter_year').val();
        var url = '{{ route($route . '.table', [':area', ':tahun']) }}';
        url = url.replace(':area', area);
        url = url.replace(':tahun', tahun);
        table.ajax.url(url).load();
    })

    function btn_delete(uuid) {
        event.preventDefault();
        var url = '{{ route($route . '.destroy', ':uuid') }}';
        url = url.replace(':uuid', uuid);
        Swal.fire({
            icon: 'warning',
            title: 'Apakah anda yakin?',
            text: "Data ini akan di hapus!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak',
            confirmButtonClass: 'btn btn-primary margin-right-10',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function() {
            $.post(url, {
                _token: "{{ csrf_token() }}",
                _method: 'DELETE'
            }, function(res) {
                if (res.status == 'success') {
                    Swal.fire(
                        'Success',
                        res.message,
                        'success'
                    );
                    $('#tb_data').DataTable().ajax.reload();
                } else {
                    Swal.fire(
                        'Gagal',
                        res.message,
                        'error'
                    );
                }
            }, 'json');
        });
    }
</script>

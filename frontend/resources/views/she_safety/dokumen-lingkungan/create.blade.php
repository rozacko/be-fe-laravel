<div class="modal-header pb-0 border-0 justify-content-end">
    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
        <i class="fa fa-close" style="font-size:24px"></i>
    </div>
</div>
<div class="modal-body scroll-y px-10 px-lg-15 pt-0 pb-15">
    <div class="mb-13 text-center">
        <h1 class="mb-3">Create Data</h1>
    </div>
    <form method="POST" action="{{ route($route . '.store') }}" enctype="multipart/form-data" id="idForm">
        <div class="modal-body">
            @csrf
            <div class="row d-flex fv-row form-group">
                <div class="col-8 mb-5">
                    <div class="d-flex flex-column fv-row form-group">
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Judul</span>
                        </label>
                        <input type="text" class="form-control " name="judul" id="judul" />
                    </div>
                </div>
                <div class="col-8 mb-5">
                    <div class="d-flex flex-column fv-row form-group">
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">No Dokumen</span>
                        </label>
                        <input type="text" class="form-control " name="no_dokumen" id="no_dokumen" />
                    </div>
                </div>
                <div class="col-8 mb-5">
                    <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                        <span class="required">Area</span>
                    </label>
                    <select class="form-select" data-placeholder="Pilih Area"
                        name="id_area" id="id_area">
                        @foreach ($area as $value)
                            <option value="{{ $value->id }}">{{ $value->nama_area }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-8 mb-5">
                    <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                        <span class="required">Ruang Lingkup</span>
                    </label>
                    <input type="text" class="form-control " name="ruang_lingkup" id="ruang_lingkup" />
                </div>
                <div class="col-8 mb-5">
                    <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                        <span class="required">Jenis Dokumen</span>
                    </label>
                    <select class="form-select" data-placeholder="jenis_dokumen" id="jenis_dokumen" name="jenis_dokumen">
                        @foreach ($jenisDokumen as $value)
                            <option value="{{ $value->nama_dokumen }}">{{ $value->nama_dokumen }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-8 mb-5">
                    <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                        <span class="required">Tahun</span>
                    </label>
                    <select class="form-select" data-placeholder="Pilih Tahun" id="filter_year" name="tahun">
                        @foreach (getYear() as $key => $value)
                            @if ($value == date('Y'))
                                <option value="{{ $value }}" selected>{{ $value }}</option>
                            @else
                                <option value="{{ $value }}">{{ $value }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="col-5 mb-5">
                    <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                        Upload Dokumen
                    </label>
                    @include('partials.general._form-upload', [
                        'filename' => '',
                        'filetitle' => 'Download',
                        'route' => $route,
                        'input_name' => 'filename'
                    ])
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <div class="text-center">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary btn-submit">
                    <span class="indicator-label">Submit</span>
                    <span class="indicator-progress">Please wait...
                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                </button>
            </div>
        </div>
    </form>
</div>


<script type="text/javascript">
    $(".select_option").select2({
        dropdownParent: $('#modal')
    });
    $("#idForm").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form. 
        var formData = new FormData(this);
        var form = $(this);
        var actionUrl = form.attr('action');
        $.ajax({
            type: "POST",
            url: actionUrl,
            enctype: 'multipart/form-data',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function(data) {
                console.log(data);
                console.log(data.status);
                if (data.status == 'success') {
                    Swal.fire(
                        'Success',
                        data.message,
                        'success'
                    );
                    $('#modal').modal('hide');
                } else {
                    Swal.fire(
                        'Warning',
                        data.message,
                        'warning'
                    );
                }
                $('#tb_data').DataTable().ajax.reload();
            }
        });
    });

    
</script>

<div class="modal-header pb-0 border-0 justify-content-end">
    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
        <i class="fa fa-close" style="font-size:24px"></i>
    </div>
</div>
<div class="modal-body scroll-y px-10 px-lg-15 pt-0 pb-15">
    <div class="mb-13 text-center">
        <h1 class="mb-3">Update Data</h1>
    </div>
    <form method="POST" action="{{ route($route . '.update', $data->uuid) }}" enctype="multipart/form-data"
        id="idForm">
        <div class="modal-body">
            @csrf
            @method('put')
            <div class="row d-flex fv-row form-group">
                <div class="col-8 mb-5">
                    <div class="d-flex flex-column fv-row form-group">
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Judul</span>
                        </label>
                        <input type="text" class="form-control " name="judul" id="judul"
                            value="{{ $data->judul ?? '' }}" />
                    </div>
                </div>
                <div class="col-8 mb-5">
                    <div class="d-flex flex-column fv-row form-group">
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">No Dokumen</span>
                        </label>
                        <input type="text" class="form-control " name="no_dokumen" id="no_dokumen"
                            value="{{ $data->no_dokumen ?? '' }}" />
                    </div>
                </div>
                <div class="col-8 mb-5">
                    <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                        <span class="required">Area</span>
                    </label>
                    <select class="form-select" data-placeholder="Pilih Area" name="id_area" id="id_area">
                        @foreach ($area as $val)
                            <option value="{{ $val->id }}" {{ $data->id_area == $val->id ? 'selected' : '' }}>
                                {{ $val->nama_area }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-8 mb-5">
                    <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                        <span class="required">Ruang Lingkup</span>
                    </label>
                    <input type="text" class="form-control " name="ruang_lingkup" id="ruang_lingkup"
                        value="{{ $data->ruang_lingkup ?? '' }}" />
                </div>
                <div class="col-8 mb-5">
                    <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                        <span class="required">Jenis Dokumen</span>
                    </label>
                    <select class="form-select" data-placeholder="jenis_dokumen" id="jenis_dokumen"
                        name="jenis_dokumen">
                        @foreach ($jenisDokumen as $val)
                            <option value="{{ $val->nama_dokumen }}"
                                {{ isset($data->jenis_dokumen) ? ($data->jenis_dokumen == $val->nama_dokumen ? 'selected' : '') : '' }}>
                                {{ $val->nama_dokumen }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-8 mb-5">
                    <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                        <span class="required">Tahun</span>
                    </label>
                    <select class="form-select" data-placeholder="Pilih Tahun" id="filter_year" name="tahun">
                        @foreach (getYear() as $key => $val)
                            @if ($val == date('Y') || $data->tahun == $val)
                                <option value="{{ $val }}" selected>{{ $val }}</option>
                            @else
                                <option value="{{ $val }}">{{ $val }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="col-5 mb-5">
                    <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                        Upload Dokumen
                    </label>
                    @include('partials.general._form-upload', [
                        'filename' => $data->filename,
                        'filetitle' => 'Download',
                        'route' => $route,
                        'input_name' => 'filename',
                    ])
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <div class="text-center">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary btn-loading">
                    <span class="indicator-label">Submit</span>
                    <span class="indicator-progress">Please wait...
                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                </button>
            </div>
        </div>
    </form>
</div>


<script type="text/javascript">
    $(".select_option").select2({
        dropdownParent: $('#modal')
    });

    $("#idForm").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form. 
        var formData = new FormData(this);
        var form = $(this);
        var actionUrl = form.attr('action');
        loading = $('.btn-loading').attr('disabled', true);
        $.ajax({
            type: "PUT",
            url: actionUrl,
            enctype: 'multipart/form-data',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data.status == 'success') {
                    Swal.fire(
                        'Success',
                        data.message,
                        'success'
                    );
                    $('#modal').modal('hide');
                } else {
                    Swal.fire(
                        'Warning',
                        data.message,
                        'warning'
                    );
                }
                $('#tb_data').DataTable().ajax.reload();
                loading = $('.btn-loading').attr('disabled', false);
            }
        });
    });
</script>

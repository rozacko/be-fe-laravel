<div class="modal-header pb-0 border-0 justify-content-end">
    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">					
        <i class="fa fa-close" style="font-size:24px"></i>
    </div>
</div>
<div class="modal-body scroll-y px-10 px-lg-15 pt-0 pb-15">
    <div class="mb-13 text-center">
        <h1 class="mb-3">Create Data</h1>
    </div>
    <form method="POST" action="{{ route($route . '.store') }}" enctype="multipart/form-data" id="idForm">
        <div class="modal-body">
        @csrf 
            <div class="row d-flex mb-4 fv-row form-group">
                <div class="col-2">
                    <label class="d-flex align-items-center fs-3 fw-semibold mb-2">
                        <span class="required">Bulan</span>                    
                    </label>
                </div>
                <div class="col-3">
                    <select id="select_bulan" class="form-select h-25 select_option" data-placeholder="Pilih Plant" name="bulan">
                        <option value=""></option>
                        @foreach($bulan as $key => $value)                    
                            <option value="{{$value['data']}}" {{$value['data'] == date("m") ? "selected" : ""}}>{{$value['desc']}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-2">
                </div>
                <div class="col-2">
                    <label class="d-flex align-items-center fs-3 fw-semibold mb-2">
                        <span class="required">Tahun</span>                    
                    </label>
                </div>
                <div class="col-3">
                <select id="select_tahun" class="form-select h-25 select_option" data-placeholder="Pilih Plant" name="tahun">
                        <option value=""></option>
                        @foreach($tahun as $key => $value)                    
                            <option value="{{$value}}" {{$value == date("Y") ? "selected" : ""}}>{{$value}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="card card-flush h-xl-100">
                <div class="card-header pt-5">
                    <div class="card-toolbar">
                        <ul class="nav" id="kt_chart_widget_8_tabs">
                            <li class="nav-item">
                                <a class="nav-link btn btn-sm btn-color-muted btn-active btn-active-light fw-bold px-4 me-1 active" data-bs-toggle="tab" id="mingguan_toggle" href="#mingguan_tab">Mingguan</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link btn btn-sm btn-color-muted btn-active btn-active-light fw-bold px-4 me-1" data-bs-toggle="tab" id="bulanan_toggle" href="#bulanan_tab">Bulanan</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="card-body pt-6">
                    <div class="tab-content">
                        <div class="tab-pane fade active show" id="mingguan_tab" role="tabpanel">
                            <div class="row d-flex mb-4 fv-row form-group">
                                <div class="col-5">
                                    <div class="align-items-center"> 
                                        <h2><b>CCR TUBAN 1 & 2</b></h2>
                                    </div>
                                </div>    
                                <div class="col-2">
                                </div>
                                <div class="col-5">
                                    <div class="align-items-center"> 
                                        <h2><b>CCR TUBAN 3 & 4</b></h2>
                                    </div>                                    
                                </div>            
                            </div>
                            @foreach($master as $k => $v) 
                            @if($v->tipe == 'mingguan')                
                            @php
                             $id_master = $v->id;
                            @endphp
                            <div class="row d-flex mb-4 fv-row form-group">
                                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                                    <h2><b>{{$v->parameter}}</b></h2>
                                </label>
                                @foreach(['1','2','3','4'] as $kk => $vv) 
                                <div class="col-1">
                                    <div class="d-flex flex-column mb-4 fv-row form-group">            
                                        <label class="form-check form-check-custom flex-stack">                            
                                            <input class="form-check-input" type="checkbox" value="1" name="m_ccr12_{{$vv}}[{{$id_master}}]"/>
                                            <span class="form-check-label text-gray-700 fs-6 fw-semibold ms-0 me-2">M{{$vv}}</span>
                                        </label>
                                    </div>
                                </div>                                
                                @endforeach
                                <div class="col-3">
                                </div>  
                                @foreach(['1','2','3','4'] as $kk => $vv) 
                                <div class="col-1">
                                    <div class="d-flex flex-column mb-4 fv-row form-group">            
                                        <label class="form-check form-check-custom flex-stack">                            
                                            <input class="form-check-input" type="checkbox" value="1" name="m_ccr34_{{$vv}}[{{$id_master}}]"/>
                                            <span class="form-check-label text-gray-700 fs-6 fw-semibold ms-0 me-2">M{{$vv}}</span>
                                        </label>
                                    </div>
                                </div>
                                @endforeach   
                            </div>
                            <div class="row d-flex mb-4 fv-row form-group">                                
                                <div class="col-5">
                                    <div class="d-flex flex-column mb-4 fv-row form-group">            
                                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                                            Keterangan
                                        </label>
                                        <textarea name="ket_12[{{$id_master}}]" cols="30" rows="2" class="wd-100 form-control form-control-lg"></textarea>
                                    </div>
                                </div>
                                <div class="col-2">
                                </div>  
                                <div class="col-5">
                                    <div class="d-flex flex-column mb-4 fv-row form-group">            
                                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                                            Keterangan
                                        </label>
                                        <textarea name="ket_34[{{$id_master}}]" cols="30" rows="2" class="wd-100 form-control form-control-lg"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row d-flex mb-4 fv-row form-group">
                                <div class="col-5">
                                    <div class="d-flex flex-column mb-4 fv-row form-group">            
                                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                                            <span class="required">Status</span>
                                        </label>
                                        <select id="status_12[{{$id_master}}]" class="form-select h-25" data-placeholder="Pilih Status" name="status_12_{{$id_master}}" required>
                                            <option value=""></option>                            
                                            <option value="OPEN">OPEN</option>                            
                                            <option value="CLOSE">CLOSE</option>                            
                                        </select>
                                    </div>                    
                                </div>
                                <div class="col-2">
                                </div>
                                <div class="col-5">
                                    <div class="d-flex flex-column mb-4 fv-row form-group">            
                                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                                            <span class="required">Status</span>
                                        </label>
                                        <select id="status_34[{{$id_master}}]" class="form-select h-25" data-placeholder="Pilih Status" name="status_34_{{$id_master}}" required>
                                            <option value=""></option>                            
                                            <option value="OPEN">OPEN</option>                            
                                            <option value="CLOSE">CLOSE</option>                            
                                        </select>
                                    </div>                    
                                </div>
                            </div>
                            @endif
                            @endforeach
                        </div>
                        <div class="tab-pane fade" id="bulanan_tab" role="tabpanel">
                            <div class="row d-flex mb-4 fv-row form-group">
                                <div class="col-5">
                                    <div class="align-items-center"> 
                                        <h2><b>CCR TUBAN 1 & 2</b></h2>
                                    </div>
                                </div>    
                                <div class="col-2">
                                </div>
                                <div class="col-5">
                                    <div class="align-items-center"> 
                                        <h2><b>CCR TUBAN 3 & 4</b></h2>
                                    </div>                                    
                                </div>            
                            </div>
                            @foreach($master as $k => $v) 
                            @if($v->tipe == 'bulanan')
                            @php
                             $id_master = $v->id;
                            @endphp
                            <div class="row d-flex mb-4 fv-row form-group">
                                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                                    <h2><b>{{$v->parameter}}</b></h2>
                                </label>
                                <div class="col-4">
                                    <div class="d-flex flex-column mb-4 fv-row form-group">            
                                        <label class="form-check form-check-custom flex-stack">                            
                                            <input class="form-check-input" type="checkbox" value="1" name="b_ccr12[{{$id_master}}]"/>
                                            <span class="form-check-label text-gray-700 fs-6 fw-semibold ms-0 me-2">Telah Melakukan Pengecekan</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-3">
                                </div>
                                <div class="col-4">
                                    <div class="d-flex flex-column mb-4 fv-row form-group">            
                                        <label class="form-check form-check-custom flex-stack">                            
                                            <input class="form-check-input" type="checkbox" value="1" name="b_ccr34[{{$id_master}}]"/>
                                            <span class="form-check-label text-gray-700 fs-6 fw-semibold ms-0 me-2">Telah Melakukan Pengecekan</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row d-flex mb-4 fv-row form-group">                                
                                <div class="col-5">
                                    <div class="d-flex flex-column mb-4 fv-row form-group">            
                                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                                            Keterangan
                                        </label>
                                        <textarea name="ket_12[{{$id_master}}]" cols="30" rows="2" class="wd-100 form-control form-control-lg"></textarea>
                                    </div>
                                </div>
                                <div class="col-2">
                                </div>  
                                <div class="col-5">
                                    <div class="d-flex flex-column mb-4 fv-row form-group">            
                                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                                            Keterangan
                                        </label>
                                        <textarea name="ket_34[{{$id_master}}]" cols="30" rows="2" class="wd-100 form-control form-control-lg"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row d-flex mb-4 fv-row form-group">
                                <div class="col-5">
                                    <div class="d-flex flex-column mb-4 fv-row form-group">            
                                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                                            <span class="required">Status</span>
                                        </label>
                                        <select id="status_12[{{$id_master}}]" class="form-select h-25" data-placeholder="Pilih Status" name="status_12_{{$id_master}}" required>
                                            <option value=""></option>                            
                                            <option value="OPEN">OPEN</option>                            
                                            <option value="CLOSE">CLOSE</option>                            
                                        </select>
                                    </div>                    
                                </div>
                                <div class="col-2">
                                </div>
                                <div class="col-5">
                                    <div class="d-flex flex-column mb-4 fv-row form-group">            
                                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                                            <span class="required">Status</span>
                                        </label>
                                        <select id="status_34[{{$id_master}}]" class="form-select h-25" data-placeholder="Pilih Status" name="status_34_{{$id_master}}" required>
                                            <option value=""></option>                            
                                            <option value="OPEN">OPEN</option>                            
                                            <option value="CLOSE">CLOSE</option>                            
                                        </select>
                                    </div>                    
                                </div>
                            </div>
                            @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>            
        </div> 
        <div class="modal-footer">
            <div class="text-center">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary">
                    <span class="indicator-label">Simpan Perubahan</span>
                    <span class="indicator-progress">Please wait... 
                    <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                </button>
            </div>
        </div>
    </form>
</div>


<script type="text/javascript">
    $(".select_option").select2({
        dropdownParent: $('#modal')
    });
    $("#idForm").submit(function(e) {
    e.preventDefault(); // avoid to execute the actual submit of the form.
    var form = $(this);
    var actionUrl = form.attr('action');
        $.ajax({
            type: "POST",
            url: actionUrl,
            data: form.serialize(), 
            success: function(data)
            {
                if(data.status == 'success'){
                    Swal.fire(
                        'Success',
                        data.message,
                        'success'
                    );
                    $('#modal').modal('hide');
                    $('#tb_fire_alarm').DataTable().ajax.reload();
                }else{
                    Swal.fire(
                        'Warning',
                        data.message,
                        'warning'
                    );
                }
            }
        });
    });
</script>

<div class="modal-header pb-0 border-0 justify-content-end">
    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">					
        <i class="fa fa-close" style="font-size:24px"></i>
    </div>
</div>
<div class="modal-body scroll-y px-10 px-lg-15 pt-0 pb-15">
    <div class="mb-13 text-center">
        <h1 class="mb-3">Create Data</h1>
    </div>
    <form method="POST" action="{{ route($route . '.store') }}" enctype="multipart/form-data" id="idForm">
        <div class="modal-body">        
        @csrf                     
            <div class="row d-flex mb-8 fv-row form-group">
                <div class="col-5">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Date of Occurrence</span>
                        </label>
                        <input type="date" class="form-control " name="tgl_doc" />
                    </div>
                </div>
                <div class="col-2">
                </div>
                <div class="col-5">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Status</span>
                        </label>
                        <select id="select_status" class="form-select h-25 select_option" data-placeholder="Pilih Status" name="status">
                            <option value=""></option>                            
                            <option value="OPEN">OPEN</option>                            
                            <option value="CLOSE">CLOSE</option>                            
                        </select>
                    </div>                    
                </div>
            </div>
            <div class="row d-flex mb-8 fv-row form-group">                
                <div class="col-5">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Accident</span>
                        </label>
                        <input type="text" class="form-control " name="accident" />
                    </div>
                </div>
                <div class="col-2">
                </div>
                <div class="col-5">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Location</span>
                        </label>
                        <input type="text" class="form-control " name="location" />
                    </div>
                </div>
            </div>
            <div class="row d-flex mb-8 fv-row form-group">
                <div class="col-5">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Person status</span>
                        </label>
                        <textarea name="person_status" cols="30" rows="2" class="wd-100 form-control form-control-lg"></textarea>
                    </div>
                </div>
                <div class="col-2">
                </div>
                <div class="col-5">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Investigation Team:</span>
                        </label>
                        <textarea name="investigation_team" cols="30" rows="2" class="wd-100 form-control form-control-lg"></textarea>
                    </div>
                </div>
            </div>
            <div class="row d-flex mb-8 fv-row form-group">
                <div class="col-5">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Report Prepared by</span>
                        </label>
                        <input type="text" class="form-control " name="report_prepared" />
                    </div>
                </div>
                <div class="col-2">
                </div>
                <div class="col-5">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Report Reviewed by</span>
                        </label>
                        <input type="text" class="form-control " name="report_reviewed" />
                    </div>
                </div>  
            </div>
            <div class="row d-flex mb-8 fv-row form-group">
                <div class="col-5">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Report Approved by</span>
                        </label>
                        <input type="text" class="form-control " name="report_approved" />
                    </div>
                </div> 
                <div class="col-2">
                </div>
                <div class="col-5">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Type</span>
                        </label>
                        <select id="select_tipe" class="form-select h-25" data-placeholder="Pilih Tipe" name="type">
                            <option value=""></option>
                            <option value="Near Miss">Near Miss</option>                            
                            <option value="First Aid Injury">First Aid Injury</option>                            
                            <option value="Medical Treatment Injury / Ringan">Medical Treatment Injury / Ringan</option>                            
                            <option value="Loss Time Injury / Berat">Loss Time Injury / Berat</option>                            
                            <option value="Fatality">Fatality</option>                            
                            <option value="Property Damage">Property Damage</option>                            
                        </select>
                    </div>                    
                </div>  
            </div>
            <div class="row d-flex mb-8 fv-row form-group">
                <div class="col-5">
                    <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">Document</span>
                    </label>
                    <p class="mt-2" style="color: #1F7793;">*format file .pdf .ppt</p>
                    <input type="file" id="default_file" name="upload_file" accept="application/pdf, application/ppt" style="display: flex !important;"/>
                    <input type="text" id="file" name="file" hidden/>
                </div>
                <div class="col-2">
                </div>
                <div class="col-5">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            Lost Work Day
                        </label>
                        <input type="number" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" class="form-control" name="lost_work_day" />
                    </div>                    
                </div>
            </div>
        </div> 
        <div class="modal-footer">
            <div class="text-center">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary">
                    <span class="indicator-label">Submit</span>
                    <span class="indicator-progress">Please wait... 
                    <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                </button>
            </div>
        </div>
    </form>
</div>


<script type="text/javascript">
     $(".select_option").select2({
        dropdownParent: $('#modal')
    });
    $("#idForm").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form. 
        var formData = new FormData(this);
        var form = $(this);
        var actionUrl = form.attr('action');
        $.ajax({
            type: "POST",
            url: actionUrl,
            enctype: 'multipart/form-data',
            data: formData,
            cache:false,
            contentType: false,
            processData: false, 
            success: function(data)
            {
                if(data.status == 'success'){
                    Swal.fire(
                        'Success',
                        data.message,
                        'success'
                    );
                    $('#modal').modal('hide');
                    $('#tb_accident').DataTable().ajax.reload();
                }else{
                    Swal.fire(
                        'Warning',
                        data.message,
                        'warning'
                    );
                }
            }
        });
    });

    $("#default_file").change(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form. 
        var input = this;
        var files = $(this)[0].files;
        var fd = new FormData();
        // Append data 
        fd.append('upload_file',files[0]);
        fd.append('_token', "{{ csrf_token() }}");
        var actionUrl = "{{ route($route . '.upload_file') }}";
        $.ajax({
            type: "POST",
            url: actionUrl,
            enctype: 'multipart/form-data',
            data: fd,
            cache:false,
            contentType: false,
            processData: false, 
            success: function(data)
            {
                if(data.status == 'success'){
                    Swal.fire(
                        'Success',
                        data.message,
                        'success'
                    );
                    $("#file").val(data.data);
                }else{
                    Swal.fire(
                        'Warning',
                        data.message,
                        'warning'
                    );
                }
            }
        });
    });
</script>

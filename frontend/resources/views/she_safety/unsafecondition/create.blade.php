<div class="modal-header pb-0 border-0 justify-content-end">
    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">					
        <i class="fa fa-close" style="font-size:24px"></i>
    </div>
</div>
<div class="modal-body scroll-y px-10 px-lg-15 pt-0 pb-15">
    <div class="mb-13 text-center">
        <h1 class="mb-3">Create Data</h1>                
    </div>
    <form method="POST" action="{{ route($route . '.store') }}" enctype="multipart/form-data" id="idForm">
        <div class="modal-body">        
        @csrf             
            <div class="row d-flex mb-8 fv-row form-group">
                <div class="col-3">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Bulan</span>
                        </label>
                        <select id="select_bulan" class="form-select h-25 select_option" data-placeholder="Pilih Plant" name="bulan">
                            <option value=""></option>
                            @foreach($bulan as $key => $value)                    
                                <option value="{{$value['data']}}" {{$value['data'] == date("m") ? "selected" : ""}}>{{$value['desc']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-2">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Tahun</span>
                        </label>
                        <select id="select_tahun" class="form-select h-25 select_option" data-placeholder="Pilih Plant" name="tahun">
                            <option value=""></option>
                            @foreach($tahun as $key => $value)                    
                                <option value="{{$value}}" {{$value == date("Y") ? "selected" : ""}}>{{$value}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-2">
                </div>
                <div class="col-5">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Status</span>
                        </label>
                        <select id="select_status" class="form-select h-25 select_option" data-placeholder="Pilih Status" name="status">
                            <option value=""></option>                            
                            <option value="OPEN">OPEN</option>                            
                            <option value="CLOSE">CLOSE</option>                            
                        </select>
                    </div>
                </div>    
            </div>
            <div class="row d-flex mb-8 fv-row form-group">                
                <div class="col-5">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Area</span>
                        </label>
                        <input type="text" class="form-control " name="area" />
                    </div>
                </div>
                <div class="col-2">
                </div>
                <div class="col-5">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">TPM / SGA</span>
                        </label>
                        <input type="text" class="form-control " name="tpm_sga" />
                    </div>
                </div>
            </div>
            <div class="row d-flex mb-8 fv-row form-group">                
                <div class="col-5">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">No Equipment</span>
                        </label>
                        <input type="text" class="form-control " name="no_equipment" />
                    </div>
                </div>                
            </div>
            <div class="row d-flex mb-8 fv-row form-group">                
                <div class="col-5">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Lokasi Kerja</span>
                        </label>
                        <textarea name="lokasi_kerja" cols="30" rows="2" class="wd-100 form-control form-control-lg"></textarea>
                    </div>
                </div>
                <div class="col-2">
                </div>
                <div class="col-5">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Uraian Temuan</span>
                        </label>
                        <textarea name="uraian_temuan" cols="30" rows="2" class="wd-100 form-control form-control-lg"></textarea>
                    </div>
                </div>
            </div>
            <div class="row d-flex mb-8 fv-row form-group">                
                <div class="col-5">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Saran tindak lanjut</span>
                        </label>
                        <textarea name="saran" cols="30" rows="2" class="wd-100 form-control form-control-lg"></textarea>
                    </div>
                </div>
                <div class="col-2">
                </div>
                <div class="col-5">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">PIC</span>
                        </label>
                        <input type="text" class="form-control " name="pic" />
                    </div>
                </div>
            </div>
            <div class="row d-flex mb-8 fv-row form-group">                
                <div class="col-5">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Unit Kerja</span>
                        </label>
                        <input type="text" class="form-control " name="unit_kerja" />
                    </div>
                </div>
                <div class="col-2">
                </div>
                <div class="col-5">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            ID Trans MSO
                        </label>
                        <input type="text" class="form-control " name="id_trans_mso" />
                    </div>
                </div>
            </div>
            <div class="row d-flex mb-8 fv-row form-group">                
                <div class="col-5">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            No Notifikasi
                        </label>
                        <input type="text" class="form-control " name="no_notif" />
                    </div>
                </div>                
            </div>
            <div class="row d-flex mb-8 fv-row form-group">
                <div class="col-5">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Tanggal Temuan</span>
                        </label>
                        <input type="date" class="form-control " name="tgl_temuan" />
                    </div>
                </div>    
                <div class="col-2">
                </div>
                <div class="col-5">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            Tanggal Target
                        </label>
                        <input type="date" class="form-control " name="tgl_target" />
                    </div>
                </div>            
            </div>
            <div class="row d-flex mb-8 fv-row form-group">
                <div class="col-5">
                    <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                        Foto Temuan
                    </label>
                    <p class="mt-2" style="color: #1F7793;">*format file .jpg .png .jpeg</p>
                    <input type="file" id="default_file_1" name="upload_file_1" accept="image/png, image/jpeg, image/jpg" style="display: flex !important;"/>
                    <input type="text" id="file_1" name="file_1" hidden/>
                </div>
                <div class="col-2">
                </div>
                <div class="col-5">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            Link Google Drive
                        </label>
                        <input type="text" class="form-control " name="file_gdrive_1" />                        
                    </div>
                </div>
            </div>
            <div class="row d-flex mb-8 fv-row form-group">
                <div class="col-2">
                    <div class="d-flex flex-column mb-8 fv-row form-group">                                
                        <label class="form-check form-check-custom flex-stack">
                            <input class="form-check-input" type="checkbox" value="1" name="cek_1"/>
                            <span class="form-check-label text-gray-700 fs-6 fw-semibold ms-0 me-2">Pengecekan 1</span>                            
                        </label>
                        
                    </div>
                </div>
                <div class="col-1">
                </div>
                <div class="col-2">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="form-check form-check-custom flex-stack">                            
                            <input class="form-check-input" type="checkbox" value="1" name="cek_2"/>
                            <span class="form-check-label text-gray-700 fs-6 fw-semibold ms-0 me-2">Pengecekan 2</span>
                        </label>
                    </div>
                </div>    
                <div class="col-2">
                </div>
                <div class="col-5">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            Tanggal Closing
                        </label>
                        <input type="date" class="form-control " name="tgl_closing" />
                    </div>
                </div>            
            </div>
            <div class="row d-flex mb-8 fv-row form-group">
                <div class="col-5">
                    <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                        Foto Closing
                    </label>
                    <p class="mt-2" style="color: #1F7793;">*format file .jpg .png .jpeg</p>
                    <input type="file" id="default_file_2" name="upload_file_2" accept="image/png, image/jpeg, image/jpg" style="display: flex !important;"/>
                    <input type="text" id="file_2" name="file_2" hidden/>
                </div>
                <div class="col-2">
                </div>
                <div class="col-5">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            Link Google Drive Closing
                        </label>
                        <input type="text" class="form-control " name="file_gdrive_2" />                        
                    </div>
                </div>
            </div>
            <div class="row d-flex mb-8 fv-row form-group">
                <div class="col-5">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            Keterangan
                        </label>
                        <textarea name="keterangan" cols="30" rows="2" class="wd-100 form-control form-control-lg"></textarea>
                    </div>
                </div>                
            </div>
        </div> 
        <div class="modal-footer">
            <div class="text-center">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary">
                    <span class="indicator-label">Submit</span>
                    <span class="indicator-progress">Please wait... 
                    <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                </button>
            </div>
        </div>
    </form>
</div>


<script type="text/javascript">
     $(".select_option").select2({
        dropdownParent: $('#modal')
    });
    $("#idForm").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form. 
        var formData = new FormData(this);
        var form = $(this);
        var actionUrl = form.attr('action');
        $.ajax({
            type: "POST",
            url: actionUrl,
            enctype: 'multipart/form-data',
            data: formData,
            cache:false,
            contentType: false,
            processData: false, 
            success: function(data)
            {
                if(data.status == 'success'){
                    Swal.fire(
                        'Success',
                        data.message,
                        'success'
                    );
                    $('#modal').modal('hide');
                    $('#tb_unsafe_con').DataTable().ajax.reload();
                }else{
                    Swal.fire(
                        'Warning',
                        data.message,
                        'warning'
                    );
                }
            }
        });
    });

    $("#default_file_1").change(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form. 
        var input = this;
        var files = $(this)[0].files;
        var fd = new FormData();
        // Append data 
        fd.append('upload_file',files[0]);
        fd.append('_token', "{{ csrf_token() }}");
        var actionUrl = "{{ route($route . '.upload_file') }}";
        $.ajax({
            type: "POST",
            url: actionUrl,
            enctype: 'multipart/form-data',
            data: fd,
            cache:false,
            contentType: false,
            processData: false, 
            success: function(data)
            {
                if(data.status == 'success'){
                    Swal.fire(
                        'Success',
                        data.message,
                        'success'
                    );
                    $("#file_1").val(data.data);
                }else{
                    Swal.fire(
                        'Warning',
                        data.message,
                        'warning'
                    );
                }
            }
        });
    });

    $("#default_file_2").change(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form. 
        var input = this;
        var files = $(this)[0].files;
        var fd = new FormData();
        // Append data 
        fd.append('upload_file',files[0]);
        fd.append('_token', "{{ csrf_token() }}");
        var actionUrl = "{{ route($route . '.upload_file') }}";
        $.ajax({
            type: "POST",
            url: actionUrl,
            enctype: 'multipart/form-data',
            data: fd,
            cache:false,
            contentType: false,
            processData: false, 
            success: function(data)
            {
                if(data.status == 'success'){
                    Swal.fire(
                        'Success',
                        data.message,
                        'success'
                    );
                    $("#file_2").val(data.data);
                }else{
                    Swal.fire(
                        'Warning',
                        data.message,
                        'warning'
                    );
                }
            }
        });
    });
</script>

<div class="modal-header pb-0 border-0 justify-content-end">
    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">					
        <i class="fa fa-close" style="font-size:24px"></i>
    </div>
</div>
<div class="modal-body scroll-y px-10 px-lg-15 pt-0 pb-15">
    <div class="mb-13 text-center">
        <h1 class="mb-3">Edit Data</h1>
    </div>
    <form action="{{ route($route . '.update', $data->id) }}" enctype="multipart/form-data" id="idForm">
        <div class="modal-body">
        @csrf
            <div class="d-flex flex-column mb-8 fv-row form-group">            
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">Tanggal Training</span>                    
                </label>
                <input type="date" class="form-control" name="tgl_training" value="{{ $data->tgl_training }}"/>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">            
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">Judul Training</span>                    
                </label>
                <input type="text" class="form-control" placeholder="Enter Judul Training" name="judul_training" value="{{ $data->judul_training }}"/>
            </div>
        <div class="modal-footer">
            <div class="text-center">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary">
                    <span class="indicator-label">Submit</span>
                    <span class="indicator-progress">Please wait... 
                    <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                </button>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    $(".select_option").select2({
        dropdownParent: $('#modal')
    });
    $("#idForm").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.
        var form = $(this);
        var actionUrl = form.attr('action');
            $.ajax({
                type: "patch",
                url: actionUrl,
                data: form.serialize(), 
                success: function(data)
                {
                    if(data.status == 'success'){
                        Swal.fire(
                            'Success',
                            data.message,
                            'success'
                        );
                        $('#modal').modal('hide');
                        $('#tb_training').DataTable().ajax.reload();
                    }else{
                        Swal.fire(
                            'Warning',
                            data.message,
                            'warning'
                        );
                    }
                }
            });
        });
</script>

<div class="modal-header pb-0 border-0 justify-content-end">
    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">					
        <i class="fa fa-close" style="font-size:24px"></i>
    </div>
</div>
<div class="modal-body scroll-y px-10 px-lg-15 pt-0 pb-15">
    <div class="mb-13 text-center">
        <h1 class="mb-3">Create Data</h1>
    </div>
    <form method="POST" action="{{ route($route . '.store_org') }}" enctype="multipart/form-data" id="idFormOrg">
        <div class="modal-body">
        @csrf   
            <div class="row d-flex mb-8 fv-row form-group">
                <div class="col-5">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">No. Pegawai</span>
                        </label>
                        <input type="text" class="form-control " name="no_pegawai" />
                        <input type="text" class="form-control " name="id_training" value="{{$id}}" hidden/>
                    </div>
                </div>    
                <div class="col-2">
                </div>
                <div class="col-5">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Nama Pegawai</span>
                        </label>
                        <input type="text" class="form-control " name="nm_pegawai" />
                    </div>
                </div>            
            </div>                
            <div class="row d-flex mb-8 fv-row form-group">
                <div class="col-5">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Departement</span>
                        </label>
                        <input type="text" class="form-control " name="departement" />
                    </div>
                </div>    
                <div class="col-2">
                </div>
                <div class="col-5">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Unit Kerja</span>
                        </label>
                        <input type="text" class="form-control " name="unit_kerja" />
                    </div>
                </div>
            </div> 
            <div class="row d-flex mb-8 fv-row form-group">
                <div class="col-5">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Band</span>
                        </label>
                        <input type="text" class="form-control " name="band" />
                    </div>
                </div>    
                <div class="col-2">
                </div>
                <div class="col-5">
                    <div class="d-flex flex-column mb-8 fv-row form-group">            
                        <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                            <span class="required">Jam Pelatihan</span>
                        </label>
                        <input type="number" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" class="form-control " name="jam_pelatihan" />
                    </div>
                </div>
            </div>           
        </div>
        <div class="modal-footer">
            <div class="text-center">
                <button type="submit" id="submit_form" class="btn btn-primary">
                    <span class="indicator-label">Tambah Data</span>
                    <span class="indicator-progress">Please wait... 
                    <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                </button>
            </div>
        </div>
    </form>
    <div class="row d-flex mb-8 fv-row form-group">
        <form method="POST" action="{{ route($route . '.import') }}" enctype="multipart/form-data" id="idFormImport">
            <div class="modal-body">
            @csrf 
                <div class="row d-flex mb-4 fv-row form-group">
                    <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                        <span class="">Template</span>
                    </label>
                    <a target="_blank" class="btn btn-warning float-end" href="{{ route($route . '.template') }}">Download Tempate</a>
                </div>
                <div class="row d-flex mb-4 fv-row form-group">
                    <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                        <span class="required">Upload File</span>
                    </label>
                    <input type="text" class="form-control " name="id_training" value="{{$id}}" hidden/>
                    <input type="file" name="upload_file" style="display: flex !important;"/>
                    <p class="mt-2" style="color: #1F7793;">*file yang di upload adalah file berdasarkan template yang disediakan</p>
                </div>
            </div> 
            <div class="modal-footer">
                <div class="text-center">
                    <button type="submit" id="submit_import" class="btn btn-primary">
                        <span class="indicator-label">Import</span>
                        <span class="indicator-progress">Please wait... 
                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                    </button>
                </div>
            </div>
        </form>
    </div>
    <table class="table table-bordered data-table" id="tb_training_org">
        <thead>
            <tr>
            <th>No</th>
            <th>No. Pegawai</th>
            <th>Nama Pegawai</th>                                
            <th>Departement</th>
            <th>Unit Kerja</th>
            <th>Band</th>
            <th>Jam Pelatihan</th>
            <th>Action</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    <div class="modal-footer">
            <div class="text-center">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>                
            </div>
        </div>
</div>


<script type="text/javascript">
    $(function() {
        var table = $('#tb_training_org').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route($route.'.table_org', $id) }}",
            columns: [{ 
                data: 'DT_RowIndex', 
                name: 'DT_RowIndex', 
                orderable: false, 
                searchable: false },
                {
                data: 'no_pegawai',
                name: 'no_pegawai'
                },
                {
                data: 'nm_pegawai',
                name: 'nm_pegawai'
                },
                {
                data: 'departement',
                name: 'departement'
                },
                {
                data: 'unit_kerja',
                name: 'unit_kerja'
                },
                {
                data: 'band',
                name: 'band'
                },
                {
                data: 'jam_pelatihan',
                name: 'jam_pelatihan'
                },
                {
                data: 'action',
                name: 'action',
                orderable: false,
                searchable: false,
                width: 150
                },
            ]
        });
    });

    $("#idFormOrg").submit(function(e) {
    e.preventDefault(); // avoid to execute the actual submit of the form.
    var form = $(this);
    var actionUrl = form.attr('action');
        $.ajax({
            type: "POST",
            url: actionUrl,
            data: form.serialize(), 
            success: function(data)
            {
                if(data.status == 'success'){
                    Swal.fire(
                        'Success',
                        data.message,
                        'success'
                    );
                    $('#tb_training').DataTable().ajax.reload();
                    $('#tb_training_org').DataTable().ajax.reload();
                }else{
                    Swal.fire(
                        'Warning',
                        data.message,
                        'warning'
                    );
                }
            }
        });
    });

    $("#idFormImport").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form. 
        var formData = new FormData(this);
        var form = $(this);
        var actionUrl = form.attr('action');
        $.ajax({
            type: "POST",
            url: actionUrl,
            enctype: 'multipart/form-data',
            data: formData,
            cache:false,
            contentType: false,
            processData: false, 
            success: function(data)
            {
                if(data.status == 'success'){
                    Swal.fire(
                        'Success',
                        data.message,
                        'success'
                    );
                    $('#tb_training_org').DataTable().ajax.reload();
                    $('#tb_training').DataTable().ajax.reload();
                }else{
                    Swal.fire(
                        'Warning',
                        data.message,
                        'warning'
                    );
                }
            }
        });
    });

    function btn_delete_org(id){
        event.preventDefault(); 
        var url = '{{ route($route.".destroy_org", ":id") }}';
        url = url.replace(':id', id);    
        Swal.fire({
            icon: 'warning',
            title: 'Apakah anda yakin?',
            text: "Data ini akan di hapus!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak',
            confirmButtonClass: 'btn btn-primary margin-right-10',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: true,
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function(isConfirm) {
            if (isConfirm.isConfirmed) {
                $.post(url, {_token: "{{ csrf_token() }}", _method: 'DELETE' }, function(res) {
                    if (res.status == 'success') {
                        Swal.fire(
                            'Success',
                            res.message,
                            'success'
                        );                    
                        $('#tb_training_org').DataTable().ajax.reload();
                        $('#tb_training').DataTable().ajax.reload();
                    } else {
                        Swal.fire(
                            'Gagal',
                            res.message,
                            'error'
                        );
                    }
                }, 'json');
            } else {
                Swal.fire(
                    'Cancelled',
                    'Your data is safe',
                    'error'
                );
            }            
        });

        return false;
    }
</script>

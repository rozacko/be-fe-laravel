{{-- Extends layout --}}
<x-default-layout>
<div class="card card-flush mt-6 mt-xl-9">
    <div class="py-lg-5 p-3">
        <div class="row">
            <div class="col-md-6">
                <div class="information">
                     <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{$pagetitle}}</h1>                       
                     <ul class="breadcrumb breadcrumb-separatorless ">
                        @foreach($breadcrumb as $key => $value)
                            @if ($key === array_key_first($breadcrumb)) 
                                <li class="breadcrumb-item text-muted">{{$key . ' /'}}
                                </li>
                            @endif

                            @if ($key === array_key_last($breadcrumb)) 
                                <li class="breadcrumb-item text-ghopo">   
                                    <a href="{{$value}}" class="text-ghopo text-hover-primary">{{$key}}</a>
                                </li>
                            @endif                            
                        @endforeach
                        
                    </ul>
                </div>
            </div>
            <div class="col-md-6">
                <div class="">
                    <div class="row">                    
                        <div class="col-md-4">
                            <select id="filter_bulan" class="form-select h-25 select_filter" data-placeholder="Pilih Bulan" name="bulan">
                                <option value=""></option>
                                @foreach($bulan as $key => $value)                    
                                    <option value="{{$value['data']}}" {{$value['data'] == date("m") ? "selected" : ""}}>{{$value['desc']}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-4">
                            <select id="filter_tahun" class="form-select h-25 select_filter" data-placeholder="Pilih Tahun" name="tahun">
                                <option value=""></option>
                                @foreach($tahun as $key => $value)                    
                                    <option value="{{$value}}" {{$value == date("Y") ? "selected" : ""}}>{{$value}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-4">
                            <button class="btn btn-history w-100" id="get_data">Tampilkan</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<div class="card-header p-3">
		<div class="card-title flex-column">			
		</div>
		<div class="card-toolbar my-1">		        
            <button type="button" class="btn btn-sm btn-primary float-right btn-add">
                Tambah Data
            </button>
		</div>
	</div>
	<div class="card-body pt-0">
        <table class="table table-bordered data-table" id="tb_training">
            <thead>
                <tr>
                <th>No</th>
                <th>Tanggal</th>
                <th>Judul</th>                                
                <th>Jumlah Peserta</th>
                <th>Data Peserta</th>
                <th>Action</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>

	</div>
</div>

<div class="modal fade" id="modal" tabindex="-1" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered mw-650px">
		<div class="modal-content rounded"></div>
	</div>
</div>

<div class="modal fade" id="modal2" tabindex="-1" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered mw-1000px">
		<div class="modal-content rounded"></div>
	</div>
</div>

</x-default-layout>

<script>
    var bulan = $("#filter_bulan").val();
    var tahun = $("#filter_tahun").val();
    var url = '{{ route($route.".table", [":bulan",":tahun"]) }}';
    url = url.replace(':bulan', bulan);
    url = url.replace(':tahun', tahun);
    var table = $('#tb_training').DataTable({
        processing: true,
        serverSide: true,
        ajax: url,
        columns: [{ 
                data: 'DT_RowIndex', 
                name: 'DT_RowIndex', 
                orderable: false, 
                searchable: false 
                },
            {
            data: 'tgl_training',
            name: 'tgl_training'
            },
            {
            data: 'judul_training',
            name: 'judul_training'
            },
            {
            data: 'jml_peserta',
            name: 'jml_peserta'
            },
            {
            data: 'id',
            render : function(data, type, row, meta) {
                return '<a href="#" onclick="data_org('+data+')" class="btn btn-primary btn-sm"><i class="fa fa-search" style="padding-right: 0px; font-size:10;"></i></a> ';
            }
            },
            {
            data: 'action',
            name: 'action',
            orderable: false,
            searchable: false,
            width: 150
            },
        ]
    });
    $(function() {
        $(".select_filter").select2();        
        getdatatable();
    });  

    function getdatatable(){        
        table.ajax.url(url).load();
    }

    setTimeout(function(){
        if ($('#alertNotif').length > 0) {
            $('#alertNotif').remove();
        }
    }, 5000) 

    $('.btn-add').click(function (e) {
        e.preventDefault();
        var url = "{{ route($route.'.create') }}";
        $.get(url, function (html) {
            $('#modal .modal-content').html(html);
            $('#modal').modal('show');            
        }).fail(function () {
            Swal.fire(
                'Warning',
                'Something error',
                'warning'
            );
        });
    })

    $('#get_data').click(function (e) {
        e.preventDefault(); 
        var bulan = $("#filter_bulan").val();
        var tahun = $("#filter_tahun").val();        
        var url = '{{ route($route.".table", [":bulan",":tahun"]) }}';
        url = url.replace(':bulan', bulan);
        url = url.replace(':tahun', tahun);
        table.ajax.url(url).load();
    }) 

    function data_org(id){
        event.preventDefault(); 
        var url = '{{ route($route.".data_org", ":id") }}';
        url = url.replace(':id', id);  
        $.get(url, function (html) {
            $('#modal2 .modal-content').html(html);
            $('#modal2').modal('show');
        }).fail(function () {
            Swal.fire(
                'Warning',
                'Something error',
                'warning'
            );
        });
    }

    function show(id){
        event.preventDefault(); 
        var url = '{{ route($route.".edit", ":id") }}';
        url = url.replace(':id', id);  
        $.get(url, function (html) {
            $('#modal .modal-content').html(html);
            $('#modal').modal('show');
        }).fail(function () {
            Swal.fire(
                'Warning',
                'Something error',
                'warning'
            );
        });          
    }

    function btn_delete(id){
        event.preventDefault(); 
        var url = '{{ route($route.".destroy", ":id") }}';
        url = url.replace(':id', id);    
        Swal.fire({
            icon: 'warning',
            title: 'Apakah anda yakin?',
            text: "Data ini akan di hapus!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak',
            confirmButtonClass: 'btn btn-primary margin-right-10',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: true,
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function(isConfirm) {
            if (isConfirm.isConfirmed) {
                $.post(url, {_token: "{{ csrf_token() }}", _method: 'DELETE' }, function(res) {
                    if (res.status == 'success') {
                        Swal.fire(
                            'Success',
                            res.message,
                            'success'
                        );                    
                        $('#tb_training').DataTable().ajax.reload();
                    } else {
                        Swal.fire(
                            'Gagal',
                            res.message,
                            'error'
                        );
                    }
                }, 'json');
            } else {
                Swal.fire(
                    'Cancelled',
                    'Your data is safe',
                    'error'
                );
            } 
        });

        return false;
    }
</script>

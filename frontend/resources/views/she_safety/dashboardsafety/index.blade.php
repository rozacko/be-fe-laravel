{{-- Extends layout --}}
<x-default-layout>
<style>
    
.circle {    
    margin-top: 25%;
    width: 250px;
    height: 250px;
    line-height: 250px;
    border-radius: 50%;
    font-size: 70px;
    color: #fff;
    text-align: center;
    background: #1F7793
}
</style>
<div class="card card-flush mt-6 mt-xl-9">
    <div class="py-lg-5 p-3">
        <div class="row">
            <div class="col-md-6">
                <div class="information">
                     <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{$pagetitle}}</h1>                       
                     <ul class="breadcrumb breadcrumb-separatorless ">
                        @foreach($breadcrumb as $key => $value)
                            @if ($key === array_key_first($breadcrumb)) 
                                <li class="breadcrumb-item text-muted">{{$key . ' /'}}
                                </li>
                            @endif

                            @if ($key === array_key_last($breadcrumb)) 
                                <li class="breadcrumb-item text-ghopo">   
                                    <a href="{{$value}}" class="text-ghopo text-hover-primary">{{$key}}</a>
                                </li>
                            @endif                            
                        @endforeach
                        
                    </ul>
                </div>
            </div>
            <div class="col-md-6">
                <div class="">
                    <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-3">
                        <select id="filter_bulan" class="form-select h-25 select_filter" data-placeholder="Pilih Bulan" name="bulan">
                            <option value="ALL" selected>ALL</option>
                            @foreach($bulan as $key => $value)                    
                                <option value="{{$value['data']}}">{{$value['desc']}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-3">
                        <select id="filter_tahun" class="form-select h-25 select_filter" data-placeholder="Pilih Tahun" name="tahun">
                            <option value=""></option>
                            @foreach($tahun as $key => $value)                    
                                <option value="{{$value}}" {{$value == date("Y") ? "selected" : ""}}>{{$value}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-3">
                        <button class="btn btn-history w-100" id="get_data">Tampilkan</button>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <div class="card card-flush mt-6 mt-xl-9">    
            <div class="py-lg-5 p-3">
                <div class="card-header">
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label fw-bold text-dark">Safety Performance</span>
                    </h3>
                </div>
                <div class="card-body pt-5 d-flex justify-content-center">
                    <div class="min-h-auto h-350px">
                        <div class="circle" id="data_dash_kpi"></div>                        
                    </div>
                </div>                        
            </div>
        </div>
    </div>
    <div class="col-md-9">
        <div class="card card-flush mt-6 mt-xl-9 card-dashboard-she-safety">    
            <div class="py-lg-5 p-3">
                <div class="card-header">
                    <div class="col-md-6">
                        <h3 class="card-title align-items-start flex-column">
                            <span class="card-label fw-bold text-dark">LTIFR & LTISR</span>
                        </h3>
                    </div>
                    <div class="col-md-6">
                        <div class="">
                            <div class="row">
                                <div class="col-md-6"></div>
                                <div class="col-md-6">                                    
                                    <select id="filter_status" class="form-select h-25 select_filter" data-placeholder="Pilih Status" name="status">
                                        <option value="Bulan">Bulan</option>                                    
                                        <option value="Tahun">Tahun</option>                                    
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div id="chart_ltifr_ltisr" class="charts"></div>
                </div>                      
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="card card-flush mt-6 mt-xl-9 card-dashboard-she-safety">    
            <div class="py-lg-5 p-3">
                <div class="card-header">
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label fw-bold text-dark">TOTAL MAN POWER</span>
                    </h3>
                </div>
                <div class="card-body">
                    <div id="chart_man_power" class="charts"></div>
                </div>                      
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card card-flush mt-6 mt-xl-9 card-dashboard-she-safety">    
            <div class="py-lg-5 p-3">
                <div class="card-header">
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label fw-bold text-dark">TOTAL MAN HOURS</span>
                    </h3>
                </div>
                <div class="card-body">
                    <div id="chart_man_hours" class="charts"></div>
                </div>                      
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="card card-flush mt-6 mt-xl-9 card-dashboard-she-safety">    
            <div class="py-lg-5 p-3">
                <div class="card-header">
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label fw-bold text-dark">UNSAFE ACTION</span>
                    </h3>
                </div>
                <div class="card-body">
                    <div id="chart_unsafe_action" class="charts"></div>
                </div>                      
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card card-flush mt-6 mt-xl-9 card-dashboard-she-safety">    
            <div class="py-lg-5 p-3">
                <div class="card-header">
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label fw-bold text-dark">UNSAFE CONDITION</span>
                    </h3>
                </div>
                <div class="card-body">
                    <div id="chart_unsafe_condition" class="charts"></div>
                </div>                      
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="card card-flush mt-6 mt-xl-9 card-dashboard-she-safety">    
            <div class="py-lg-5 p-3">
                <div class="card-header">
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label fw-bold text-dark">ACCIDENT REPORT</span>
                    </h3>
                </div>
                <div class="card-body">
                    <div id="chart_accident" class="charts"></div>
                </div>                      
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card card-flush mt-6 mt-xl-9 card-dashboard-she-safety">    
            <div class="py-lg-5 p-3">
                <div class="card-header">
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label fw-bold text-dark">FIRE ACCIDENT REPORT</span>
                    </h3>
                </div>
                <div class="card-body">
                    <div id="chart_accident_fire" class="charts"></div>
                </div>                      
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card card-flush mt-6 mt-xl-9 card-dashboard-she-safety">    
            <div class="py-lg-5 p-3">
                <div class="card-header">
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label fw-bold text-dark">SAFETY TRAINING</span>
                    </h3>
                </div>
                <div class="card-body">
                    <div id="chart_safety_training" class="charts"></div>
                </div>                      
            </div>
        </div>
    </div>
</div>

</x-default-layout>

<script>
    $(function() {
        $(".select_filter").select2();
        dash_kpi();
        dash_ltifr_ltisr();
        dash_man_power();
        dash_man_hours();
        dash_unsafe_aciton();
        dash_unsafe_condition();
        dash_accident();
        dash_accident_fire();
        dash_safety_training();
    }); 
    var options1 = {
        series: [],
        chart: {
            height: 350,
            type: 'line',
            dropShadow: {
                enabled: true,
                color: '#000',
                top: 18,
                left: 7,
                blur: 10,
                opacity: 0.2
            },
            toolbar: {
                show: false
            },            
        },
        colors: ['#77B6EA', '#545454'],
        dataLabels: {
            enabled: true,
        },
        stroke: {
            curve: 'smooth'
        },
        title: {
            text: 'Lost Time Injury Injury Rate (LTIFR) & Lost Time Injury Severity Rate (LTISR)',
            align: 'left'
        },
        grid: {
            borderColor: '#e7e7e7',
            row: {
                colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
                opacity: 0.5
            },
        },
        markers: {
            size: 1
        },
        xaxis: {
            categories: [],
            title: {
                text: ''
            }
        },
        yaxis: {
            title: {
                text: ''
            },
            min: 5,
            max: 40
        },
        legend: {
            position: 'top',
            horizontalAlign: 'right',
            floating: true,
            offsetY: -25,
            offsetX: -5
        }
    };
    $('#get_data').click(function (e) {
        e.preventDefault(); 
        dash_kpi();
        dash_ltifr_ltisr();
        dash_man_power();
        dash_man_hours();
        dash_unsafe_aciton();
        dash_unsafe_condition();
        dash_accident();
        dash_accident_fire();
        dash_safety_training();
    })
    
    function dash_kpi() {
        var bulan = $("#filter_bulan").val();
        var tahun = $("#filter_tahun").val();
        var url = '{{ route($route.".dash_kpi", [":bulan",":tahun"]) }}';
        url = url.replace(':bulan', bulan);
        url = url.replace(':tahun', tahun);
        $.get(url, function (res) {
            $('#data_dash_kpi').html(res.data);
        }).fail(function () {
            Swal.fire(
                'Warning',
                'Something error',
                'warning'
            );
        }); 
        
    }

    $("#filter_status").on('change', function(){
        dash_ltifr_ltisr()
    });
    function dash_ltifr_ltisr() {
        var element = document.getElementById('chart_ltifr_ltisr');
        if (!element) {
            return;
        }
        var chart = new ApexCharts(element, options1);
        chart.render();

        var bulan = $("#filter_bulan").val();
        var tahun = $("#filter_tahun").val();
        var status = $("#filter_status").val();
        var url = '{{ route($route.".dash_ltifr_ltisr", [":bulan",":tahun",":status"]) }}';
        url = url.replace(':bulan', bulan);
        url = url.replace(':tahun', tahun);
        url = url.replace(':status', status);
        $.get(url, function (res) {            
            chart.updateSeries([{
                name: "LTIFR",
                data: res.data.data1
            },
            {
                name: "LTISR",
                data: res.data.data2
            }]),
            chart.updateOptions({
                xaxis: {
                    categories: res.data.label
                }
            });
        }).fail(function () {
            Swal.fire(
                'Warning',
                'Something error',
                'warning'
            );
        }); 
    }

    var options2 = {
        series: [],
        chart: {
            type: 'bar',
            height: 200,
            events: {
                dataPointSelection: (event, chartContext, config) => {
                console.log(chartContext, config);
                }
            }
        },
        plotOptions: {
            bar: {
                borderRadius: 4,
                horizontal: true,
                distributed: true,
            }
        },
        colors: ['#FF0000','#02DFDE'],
        dataLabels: {
            enabled: false
        },
        xaxis: {
            categories: ['SIG', 'Contractor'],
        }
    };
    function dash_man_power() {
        var element = document.getElementById('chart_man_power');      
        if (!element) {
            return;
        }
        var chart = new ApexCharts(element, options2);
        chart.render();

        var bulan = $("#filter_bulan").val();
        var tahun = $("#filter_tahun").val();
        var url = '{{ route($route.".dash_man_power", [":bulan",":tahun"]) }}';
        url = url.replace(':bulan', bulan);
        url = url.replace(':tahun', tahun);
        $.get(url, function (res) {            
            chart.updateSeries([{
                name: 'Jumlah ',
                data: res.data
            }
            ]);
        }).fail(function () {
            Swal.fire(
                'Warning',
                'Something error',
                'warning'
            );
        }); 
    }
    function dash_man_hours() {
        var element = document.getElementById('chart_man_hours');
        if (!element) {
            return;
        }
        var chart = new ApexCharts(element, options2);
        chart.render();

        var bulan = $("#filter_bulan").val();
        var tahun = $("#filter_tahun").val();
        var url = '{{ route($route.".dash_man_hours", [":bulan",":tahun"]) }}';
        url = url.replace(':bulan', bulan);
        url = url.replace(':tahun', tahun);
        $.get(url, function (res) {            
            chart.updateSeries([{
                name: 'Jumlah ',
                data: res.data
            }
            ]);
        }).fail(function () {
            Swal.fire(
                'Warning',
                'Something error',
                'warning'
            );
        }); 
    }

    var options3 = {
            series: [],
            chart: {
            type: 'bar',
            height: 350
        },
        plotOptions: {
        bar: {
            horizontal: false,
            columnWidth: '55%',
            endingShape: 'rounded'
        },
        },
        colors: ['#00FF00', '#ff0000'],
        dataLabels: {
            enabled: false
        },
        stroke: {
            show: true,
            width: 2,
            colors: ['transparent']
        },
        xaxis: {
            categories: [],
        },
        fill: {
            opacity: 1
        },        
    };
    function dash_unsafe_aciton() {
        var element = document.getElementById('chart_unsafe_action');
        if (!element) {
            return;
        }
        var chart = new ApexCharts(element, options3);
        chart.render();

        var bulan = $("#filter_bulan").val();
        var tahun = $("#filter_tahun").val();
        var url = '{{ route($route.".dash_unsafe_action", [":bulan",":tahun"]) }}';
        url = url.replace(':bulan', bulan);
        url = url.replace(':tahun', tahun);
        $.get(url, function (res) {            
            chart.updateSeries([
                {
                    name: 'Close',
                    data: res.data.data1
                }, {
                    name: 'Open',
                    data: res.data.data2
                }, 
            ]),
            chart.updateOptions({
                xaxis: {
                    categories: res.data.bulan
                }
            });
        }).fail(function () {
            Swal.fire(
                'Warning',
                'Something error',
                'warning'
            );
        });
    }
    function dash_unsafe_condition() {
        var element = document.getElementById('chart_unsafe_condition');
        if (!element) {
            return;
        }
        var chart = new ApexCharts(element, options3);
        chart.render();

        var bulan = $("#filter_bulan").val();
        var tahun = $("#filter_tahun").val();
        var url = '{{ route($route.".dash_unsafe_condition", [":bulan",":tahun"]) }}';
        url = url.replace(':bulan', bulan);
        url = url.replace(':tahun', tahun);
        $.get(url, function (res) {            
            chart.updateSeries([
                {
                    name: 'Close',
                    data: res.data.data1
                }, {
                    name: 'Open',
                    data: res.data.data2
                }, 
            ]),
            chart.updateOptions({
                xaxis: {
                    categories: res.data.bulan
                }
            });
        }).fail(function () {
            Swal.fire(
                'Warning',
                'Something error',
                'warning'
            );
        });
    }
    function dash_accident() {
        var element = document.getElementById('chart_accident');
        if (!element) {
            return;
        }
        var chart = new ApexCharts(element, options3);
        chart.render();

        var bulan = $("#filter_bulan").val();
        var tahun = $("#filter_tahun").val();
        var url = '{{ route($route.".dash_accident", [":bulan",":tahun"]) }}';
        url = url.replace(':bulan', bulan);
        url = url.replace(':tahun', tahun);
        $.get(url, function (res) {            
            chart.updateSeries([
                {
                    name: 'Close',
                    data: res.data.data1
                }, {
                    name: 'Open',
                    data: res.data.data2
                }, 
            ]),
            chart.updateOptions({
                xaxis: {
                    categories: res.data.bulan
                }
            });
        }).fail(function () {
            Swal.fire(
                'Warning',
                'Something error',
                'warning'
            );
        });
    }
    function dash_accident_fire() {
        var element = document.getElementById('chart_accident_fire');
        if (!element) {
            return;
        }
        var chart = new ApexCharts(element, options3);
        chart.render();

        var bulan = $("#filter_bulan").val();
        var tahun = $("#filter_tahun").val();
        var url = '{{ route($route.".dash_accident_fire", [":bulan",":tahun"]) }}';
        url = url.replace(':bulan', bulan);
        url = url.replace(':tahun', tahun);
        $.get(url, function (res) {            
            chart.updateSeries([
                {
                    name: 'Close',
                    data: res.data.data1
                }, {
                    name: 'Open',
                    data: res.data.data2
                }, 
            ]),
            chart.updateOptions({
                xaxis: {
                    categories: res.data.bulan
                }
            });
        }).fail(function () {
            Swal.fire(
                'Warning',
                'Something error',
                'warning'
            );
        });
    }

    var options4 = {
        series: [],
        chart: {
            type: 'bar',
            height: 350
        },
        plotOptions: {
            bar: {
                borderRadius: 4,
                horizontal: true,
                distributed: true,
            }
        },
        colors: [
            '#d4526e',
            '#13d8aa',
            '#A5978B',
            '#2b908f',
            '#f9a3a4',
            '#90ee7e',
            '#f48024',
            '#69d2e7',
        ],
        dataLabels: {
            enabled: false
        },
        xaxis: {
            categories: [],
        }
    };
    function dash_safety_training() {
        var element = document.getElementById('chart_safety_training');
        if (!element) {
            return;
        }
        var chart = new ApexCharts(element, options4);
        chart.render();

        var bulan = $("#filter_bulan").val();
        var tahun = $("#filter_tahun").val();
        var url = '{{ route($route.".dash_safety_training", [":bulan",":tahun"]) }}';
        url = url.replace(':bulan', bulan);
        url = url.replace(':tahun', tahun);
        $.get(url, function (res) {            
            chart.updateSeries([{
                name: 'Jumlah ',
                data: res.data.data
            }
            ]);
            chart.updateOptions({
                xaxis: {
                    categories: res.data.label
                }
            });
        }).fail(function () {
            Swal.fire(
                'Warning',
                'Something error',
                'warning'
            );
        });
    }
</script>

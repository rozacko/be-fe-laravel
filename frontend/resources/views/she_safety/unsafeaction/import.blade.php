<div class="modal-header pb-0 border-0 justify-content-end">
    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">					
        <i class="fa fa-close" style="font-size:24px"></i>
    </div>
</div>
<div class="modal-body scroll-y px-10 px-lg-15 pt-0 pb-15">
    <div class="mb-13 text-center">
        <h1 class="mb-3">Import Data</h1>
    </div>
    <form method="POST" action="{{ route($route . '.import') }}" enctype="multipart/form-data" id="idFormImport">
        <div class="modal-body">
        @csrf 
            <div class="row d-flex mb-4 fv-row form-group">
                <div class="col-2">
                    <label class="d-flex align-items-center fs-3 fw-semibold mb-2">
                        <span class="required">Bulan</span>                    
                    </label>
                </div>
                <div class="col-3">
                    <select id="select_bulan" class="form-select h-25 select_option" data-placeholder="Pilih Plant" name="bulan">
                        <option value=""></option>
                        @foreach($bulan as $key => $value)                    
                            <option value="{{$value['data']}}" {{$value['data'] == date("m") ? "selected" : ""}}>{{$value['desc']}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-2">
                </div>
                <div class="col-2">
                    <label class="d-flex align-items-center fs-3 fw-semibold mb-2">
                        <span class="required">Tahun</span>                    
                    </label>
                </div>
                <div class="col-3">
                <select id="select_tahun" class="form-select h-25 select_option" data-placeholder="Pilih Plant" name="tahun">
                        <option value=""></option>
                        @foreach($tahun as $key => $value)                    
                            <option value="{{$value}}" {{$value == date("Y") ? "selected" : ""}}>{{$value}}</option>
                        @endforeach
                    </select>
                </div>
            </div> 
            <div class="row d-flex mb-4 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="">Template</span>
                </label>
                <a target="_blank" class="btn btn-warning float-end" href="{{ route($route . '.template') }}">Download Tempate</a>
            </div>
            <div class="row d-flex mb-4 fv-row form-group">
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">Upload File</span>
                </label>
                <input type="file" name="upload_file" style="display: flex !important;"/>
                <p class="mt-2" style="color: #1F7793;">*file yang di upload adalah file berdasarkan template yang disediakan</p>
            </div>
        </div> 
        <div class="modal-footer">
            <div class="text-center">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary">
                    <span class="indicator-label">Import</span>
                    <span class="indicator-progress">Please wait... 
                    <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                </button>
            </div>
        </div>
    </form>
</div>


<script type="text/javascript">
     $(".select_option").select2({
        dropdownParent: $('#modal')
    });
    $("#idFormImport").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form. 
        var formData = new FormData(this);
        var form = $(this);
        var actionUrl = form.attr('action');
        $.ajax({
            type: "POST",
            url: actionUrl,
            enctype: 'multipart/form-data',
            data: formData,
            cache:false,
            contentType: false,
            processData: false, 
            success: function(data)
            {
                if(data.status == 'success'){
                    Swal.fire(
                        'Success',
                        data.message,
                        'success'
                    );
                    $('#modal').modal('hide');
                    $('#tb_unsafe').DataTable().ajax.reload();
                }else{
                    Swal.fire(
                        'Warning',
                        data.message,
                        'warning'
                    );
                }
            }
        });
    });
</script>

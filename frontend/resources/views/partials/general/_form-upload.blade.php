
<input type="hidden" name="action_url" id="action_url_{{ $input_name }}" class="view_file" value="{{ route($route . '.upload-file') }}">
<div class="d-flex">
    <input type="file" class="form-control file_select input_file{{ $input_name }}" data-kode="{{ $input_name }}" accept="application/pdf, image/png"
        style="display: flex !important;height: 40px;">
    <a href="#" style="display: none"
        class="file_select_upload text-secondary btn btn-success btn-sm ms-3 btn-loading" data-kode="{{ $input_name }}" title="upload file"><i
            class="fa fa-upload"></i></a> &nbsp;
    <input type="hidden" name="filename" id="filename" class="view_file_{{ $input_name }}" value="">
</div>
<span style="display: none" class="badge badge-success file_select_success_{{ $input_name }}">File berhasil
    diupload</span>
@if ($filename)
    <div class="preview_{{ $input_name }}">
        <a href="#" class="file_download_{{ $input_name }}">{{$filetitle}}</a> &nbsp;
    </div>
@endif

<div class="card card-progress-realisasi-capex">
	<!--begin::Header-->
	<div class="card-header">
		<!--begin::Title-->
		<h3 class="card-title align-items-start flex-column">
			<span class="card-label fw-bold text-dark">PROGRES REALISASI CAPEX 2022</span>
		</h3>
		<!--end::Title-->

	</div>
	<!--end::Header-->
	<!--begin::Body-->
	<div class="card-body pt-5">
		<!--begin::Chart container-->
		<div id="kt_charts_widget_progress_realisasi_capex" class="charts"></div>
		<!--end::Chart container-->
	</div>
	<!--end::Body-->
</div>
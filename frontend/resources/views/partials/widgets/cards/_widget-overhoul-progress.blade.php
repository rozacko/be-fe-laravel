<div class="card card-overhoul-progress mb-2">
  <div class="progression">
    <label for="">Total <br> Progress</label>
    <div class="statistic">
      79%
    </div>
  </div>
</div>
<div class="card card-overhoul-progress">
    <div class="card-body">
      <div id="kt_charts_widget_overhoul_progress" class="charts"></div>
  </div>
</div>
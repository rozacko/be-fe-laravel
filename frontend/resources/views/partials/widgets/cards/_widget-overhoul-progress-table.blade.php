<div class="card card-overhoul-progress-table ">

<div class="card-body">
    <table class="w-100">
    
      <tbody >
        <tr>
          <td>PLANNED</td>
          <td id="sum_planned">0,4%</td>
          <td>START</td>
          <td id="start_periode">05 Januari 2023</td>
        </tr>
        <tr>
          <td>ACTUAL</td>
          <td id="sum_real">0,0%</td>
          <td>FINISH</td>
          <td id="end_periode">31 Januari 2023</td>
        </tr>
        <tr>
          <td>VARIANCE</td>
          <td id="sum_variance">96,8%</td>
          <td>DURATION</td>
          <td><span id="duration_periode"></span>  Days</td>
          <!-- <td>FEED TO FEED</td> -->

        </tr>
      
      </tbody>
     
    </table>
  </div>
</div>
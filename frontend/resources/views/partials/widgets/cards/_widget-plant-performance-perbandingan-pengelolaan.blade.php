<div class="card card-detail-biaya-pemeliharaan">
  <div class="card-header">
    <span>PERBANDINGAN PENGELOLAAN SAP PM SEMESTER 1 & SEMESTER 2</span>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table">
        <tbody>
          <tr>
            <th rowspan="2" style="text-align: center;" class="decsription-title border-bottom">POINT PENILAIAN</th>
            <td colspan="2" class="text-center border-bottom">PERBANDINGAN</td>
            <th rowspan="2" class="decsription-title centerAlign border-bottom">HASIL EVALUASI</th>
          </tr>
          <tr>
            <td class="text-center border-bottom">SEMESTER 1</td>
            <td class="text-center border-bottom">JILI - NOV</td>
          </tr>
          <tr>
            <td>Lorem Ipsum</td>
            <td>11.5%</td>
            <td><div class="d-flex align-items-center">11.5% <div class="circle" style="background: #da0806;"></div></div></td>
            <td>Lorem Ipsum</td>
          </tr>
          <tr>
            <td>Lorem Ipsum</td>
            <td>11.5%</td>
            <td><div class="d-flex align-items-center">11.5% <div class="circle" style="background: #da0806;"></div></div></td>
            <td>Lorem Ipsum</td>
          </tr>
          <tr>
            <td>Lorem Ipsum</td>
            <td>11.5%</td>
            <td><div class="d-flex align-items-center">11.5% <div class="circle" style="background: #da0806;"></div></div></td>
            <td>Lorem Ipsum</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>
<div class="card card-pie-plant-reability px-2">
  <div class="card-body">
    <table class="table table-bordered data-table" id="dt_inspec">
        <thead>
            <tr>
                <th>Area</th>
                <th>Good</th>
                <th>Low Risk</th>
                <th>Med Risk</th>
                <th>High Risk</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
        <tfoot>
            <tr>
                <th>Total</th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </tfoot>
    </table>
  </div>
</div>
<div class="card card-detail-biaya-pemeliharaan">
  <div class="card-header">
    <span>DETAIL BIAYA PEMELIHARAAN</span>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table">
        <tbody>
          <tr>
            <th colspan="2" rowspan="3" style="text-align: center;" class="decsription-title border-bottom">KODE UNIT KERJA</th>
            <th colspan="2" rowspan="3" class="decsription-title border-bottom">DESKRIPSI UNIT KERJA</th>
            <td colspan="3" class="text-center border-bottom">KETEPATAN PENGELOLA NOTIFIKASI</td>
            <td colspan="7" class="text-center border-bottom">KETE[ATAN PENGELOLAAN MAINTENANCE ORDER</td>
            <th rowspan="3" class="decsription-title centerAlign border-bottom">RESUME ALL PENGELOLA SAP MODULE PM UK GHOPO</th>
            <th rowspan="3" class="decsription-title centerAlign border-bottom">KLASISFIKASI PENGELOLAAN SAP PM</th>
          </tr>
          <tr>
            <td class="text-center" scope="col">% NOTIF DG DESKRIPSI STANDART</td>
            <td class="text-center">JUMLAH NNOTIF AGING</td>
            <td class="text-center">% NOTIF PROSES EKSEKUSI CONVERT MAINT. ORDER</td>
            <td class="text-center">% KESESUAIAN TYPE ORDER & AKTIFITAS</td>
            <td class="text-center">% ORDER DENGAN DESKRIPSI DTANDAR</td>
            <td class="text-center">% ORDER DENGAN TASK LIST STANDART </td>
            <td class="text-center">% PLANING RESOURCE & DURATION</td>
            <td class="text-center">ORDER CLOSE DENGAN KONFIRMASI</td>
            <td class="text-center">% KETEPATAN PLANING & ACTUAL KERJA</td>
            <td class="text-center">JUMLAH MAINTENANCE ORDER AGING</td>
          </tr>
          <tr>
            <td class="text-center border-bottom" style="color: yellow;"> TARGET : 100%</td>
            <td class="text-center border-bottom" style="color: yellow;"> TARGET : 100%</td>
            <td class="text-center border-bottom" style="color: yellow;"> TARGET : 100%</td>
            <td class="text-center border-bottom" style="color: yellow;"> TARGET : 100%</td>
            <td class="text-center border-bottom" style="color: yellow;"> TARGET : 100%</td>
            <td class="text-center border-bottom" style="color: yellow;"> TARGET : 100%</td>
            <td class="text-center border-bottom" style="color: yellow;"> TARGET : 100%</td>
            <td class="text-center border-bottom" style="color: yellow;"> TARGET : 100%</td>
            <td class="text-center border-bottom" style="color: yellow;"> TARGET : 100%</td>
            <td class="text-center border-bottom" style="color: yellow;"> TARGET : 100%</td>
          </tr>

          <tr>
            <th scope="row" colspan="2" style="text-align: center;">1</th>
            <th scope="row" colspan="2">BIAYA PEMBANGUNAN</th>
            <td class="danger-color">40</td>
            <td class="warning-color">40</td>
            <td class="active-color">40</td>
            <td class="danger-color">40</td>
            <td class="warning-color">40</td>
            <td class="danger-color">40</td>
            <td class="danger-color">40</td>
            <td class="active-color">40</td>
            <td class="active-color">40</td>
            <td class="warning-color">40</td>
            <td class="warning-color">40</td>
            <td class="warning-color">40</td>
          </tr>
          <tr>
            <th scope="row" colspan="2" style="text-align: center;">1</th>
            <th scope="row" colspan="2">PRODUKSI SEMEN</th>
            <td class="danger-color">40</td>
            <td class="warning-color">40</td>
            <td class="active-color">40</td>
            <td class="danger-color">40</td>
            <td class="warning-color">40</td>
            <td class="danger-color">40</td>
            <td class="danger-color">40</td>
            <td class="active-color">40</td>
            <td class="active-color">40</td>
            <td class="warning-color">40</td>
            <td class="warning-color">40</td>
            <td class="warning-color">40</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>
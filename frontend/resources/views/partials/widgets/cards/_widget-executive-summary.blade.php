<div class="card card-executive-summary">
  <div class="card-header mb-2">
    <div class="card-title">
      <span>Executive Summary</span>
    </div>
  </div>

  <div class="card-body">
    {{-- <div class="box-executive"> --}}
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col col-custom mt-3">
                        <div class="box-executive__content">
                            <img src="assets/media/logos/ghopo_mark_stuck.png" alt="logo mark oce">
                            <div class="box-executive__content_information">
                                <label for="oce">OEE</label>
                                <div id="oce-condition" class="condition">78 %</div>
                                <div id="oce-subs" class="sub-condition">100 % RKAP</div>
                            </div>
                        </div>
                    </div>
                    <div class="col col-custom mt-3">
                        <div class="box-executive__content">
                            <img src="assets/media/logos/ghopo_mark_up.png" alt="logo mark mtbf">
                            <div class="box-executive__content_information">
                                <label for="mtbf">MTBF</label>
                                <div id="mtbf-condition" class="condition">245 Jam</div>
                                <div id="mtbf-subs" class="sub-condition">106 % RKAP</div>
                            </div>
                        </div>
                    </div>
                    <div class="col col-custom mt-3">
                        <div class="box-executive__content">
                            <img src="assets/media/logos/ghopo_mark_up.png" alt="logo mark Clinker Production">
                            <div class="box-executive__content_information">
                                <label for="Clinker Production">Clinker Prod</label>
                                <div id="clinker-condition" class="condition">62,55 %</div>
                                <div id="clinker-subs" class="sub-condition">101 % RKAP</div>
                            </div>
                        </div>
                    </div>
                    <div class="col col-custom mt-3">
                        <div class="box-executive__content">
                            <img src="assets/media/logos/ghopo_mark_down.png" alt="logo mark tsr">
                            <div class="box-executive__content_information">
                                <label for="tsr">TSR</label>
                                <div id="tsr-condition" class="condition">2,92 %</div>
                                <div id="tsr-subs" class="sub-condition">45 % RKAP</div>
                            </div>
                        </div>
                    </div>
                    <div class="col col-custom mt-3">
                        <div class="box-executive__content">
                            <img src="assets/media/logos/ghopo_mark_up.png" alt="logo mark Mainteannce cost">
                                <div class="box-executive__content_information">
                                    <label for="Mainteannce cost">Maint. Cost</label>
                                    <div id="maintenance-condition" class="condition-maintenance">Rp 41,3 M-(110%)</div>
                                    <div id="maintenance-subs" class="sub-condition">45 % RKAP</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    {{-- </div> --}}
  </div>
</div>

<script>
  let oce = {
    condition: 78,
    subCondition: 100,
    condSuffix: '%',
    subSuffix: '% RKAP'
  }
  let mtbf = {
    condition: 245,
    subCondition: 106,
    condSuffix: 'Jam',
    subSuffix: '% RKAP'
  }
  let clinker = {
    condition: 62.55,
    subCondition: 101,
    condSuffix: '%',
    subSuffix: '% RKAP'
  }
  let tsr = {
    condition: 2.92,
    subCondition: 45,
    condSuffix: '%',
    subSuffix: '% RKAP'
  }
  let maintenance = {
    condition: 41.3,
    subCondition: 45,
    condSuffix: 'M',
    subSuffix: '% RKAP'
  }

  setInterval(function() {
    this.updateExecutiveSummary()
  }, 1000);

  function getOCE() {
    oce.condition = Math.floor(Math.random() * 201)
    oce.subCondition = Math.floor(Math.random() * 201)

    document.getElementById("oce-condition").innerHTML = oce.condition + ' ' + oce.condSuffix
    document.getElementById("oce-subs").innerHTML = oce.subCondition + ' ' + oce.subSuffix
  }

  function getMTBF() {
    mtbf.condition = Math.floor(Math.random() * 501)
    mtbf.subCondition = Math.floor(Math.random() * 201)

    document.getElementById("mtbf-condition").innerHTML = mtbf.condition + ' ' + mtbf.condSuffix
    document.getElementById("mtbf-subs").innerHTML = mtbf.subCondition + ' ' + mtbf.subSuffix
  }

  function getClinkerProd() {
    clinker.condition = Math.floor(Math.random() * 101)
    clinker.subCondition = Math.floor(Math.random() * 201)

    document.getElementById("clinker-condition").innerHTML = clinker.condition + ' ' + clinker.condSuffix
    document.getElementById("clinker-subs").innerHTML = clinker.subCondition + ' ' + clinker.subSuffix
  }

  function getTSR() {
    tsr.condition = Math.floor(Math.random() * 101)
    tsr.subCondition = Math.floor(Math.random() * 101)

    document.getElementById("tsr-condition").innerHTML = tsr.condition + ' ' + tsr.condSuffix
    document.getElementById("tsr-subs").innerHTML = tsr.subCondition + ' ' + tsr.subSuffix
  }

  function getMaintenance() {
    maintenance.condition = Math.floor(Math.random() * 501)
    maintenance.subCondition = Math.floor(Math.random() * 101)

    document.getElementById("maintenance-condition").innerHTML = maintenance.condition + ' ' + maintenance.condSuffix
    document.getElementById("maintenance-subs").innerHTML = maintenance.subCondition + ' ' + maintenance.subSuffix
  }

  function updateExecutiveSummary() {
    this.getOCE();
    this.getMTBF();
    this.getClinkerProd();
    this.getTSR();
    this.getMaintenance();
  }

</script>
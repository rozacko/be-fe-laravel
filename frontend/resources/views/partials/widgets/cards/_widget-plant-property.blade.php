	<div class="card card-plant-property">
										<!--begin::Header-->
										<div class="card-header">
											<!--begin::Title-->
											<h3 class="card-title align-items-start flex-column">
												<span class="card-label fw-bold text-dark">Kiln Rate Summary</span>
											</h3>
											<!--end::Title-->
										
										</div>
										<!--end::Header-->
										<!--begin::Body-->
										<div class="card-body pt-5">
											<!--begin::Chart container-->
											<div id="kt_charts_widget_plant_prop" class="min-h-auto h-300px"></div>
											<!--end::Chart container-->
										</div>
										<!--end::Body-->
									</div>
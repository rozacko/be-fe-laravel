<div class="card card-detail-biaya-pemeliharaan">
  <div class="card-header">
    <span>DETAIL BIAYA PEMELIHARAAN</span>
  </div>
  <div class="card-body">
   <div class="table-responsive">
     <table class="table">
      <tbody>
        <tr>
          <th colspan="2" rowspan="3" class="decsription-title border-bottom">DESKRIPSI</th>
          <td colspan="3" class="text-center border-bottom">BULAN INI</td>
          <td colspan="3" class="text-center border-bottom">S/D BULAN INI</td>
          <td colspan="5" class="text-center border-bottom">TOTAL ANGGARAN TAHUN INI</td>
        </tr>
        <tr>
          <th class="text-center border-bottom" scope="col">RKAP</th>
          <td class="text-center border-bottom">REALISASI</td>
          <td class="text-center border-bottom">%</td>
          <td class="text-center border-bottom">RKAP</td>
          <td class="text-center border-bottom">REALISASI</td>
          <td class="text-center border-bottom">%</td>
          <td class="text-center border-bottom">RKAP</td>
          <td class="text-center border-bottom">REALISASI</td>
          <td class="text-center border-bottom">PROGNOSA</td>
          <td class="text-center border-bottom" colspan="2">%</td>
        </tr>
        <tr>
          <th class="text-center border-bottom" scope="row">1</th>
          <td class="text-center border-bottom">2</td>
          <td class="text-center border-bottom">(2:1)</td>
          <td class="text-center border-bottom">3</td>
          <td class="text-center border-bottom">4</td>
          <td class="text-center border-bottom">(4:3)</td>
          <td class="text-center border-bottom">5</td>
          <td class="text-center border-bottom">6</td>
          <td class="text-center border-bottom">7</td>
          <td class="text-center border-bottom">(6:5)</td>
           <td class="text-center border-bottom">(7:5)</td>
        </tr>

        <tr>
          <th scope="row" colspan="2">BIAYA PEMBANGUNAN</th>
          <td>40</td>
          <td>40</td>
                    <td class="active">40</td>

          <td>40</td>
          <td>40</td>
                    <td class="active">40</td>

          <td>40</td>
          <td>40</td>
          <td>40</td>
          <td class="active">40</td>
          <td class="active">40</td>
        </tr>
        <tr>
          <th scope="row" colspan="2">PRODUKSI SEMEN</th>
          <td>40</td>
          <td>40</td>
                    <td class="active">40</td>

          <td>40</td>
          <td>40</td>
                    <td class="active">40</td>

          <td>40</td>
          <td>40</td>
          <td>40</td>
          <td class="active">40</td>
                  <td class="active">40</td>
        </tr>
        <tr>
          <th scope="row" colspan="2">PENJUALAN CLINKER</th>
          <td>40</td>
          <td>40</td>
                    <td class="active">40</td>

          <td>40</td>
          <td>40</td>
                    <td class="active">40</td>

          <td>40</td>
          <td>40</td>
          <td>40</td>
          <td class="active">40</td>
                    <td class="active">40</td>


        </tr>
        <tr>
          <th scope="row" colspan="2">INDEX Rp/TON</th>
          <td>40</td>
          <td>40</td>
          <td class="active">40</td>
          <td >40</td>
          <td>40</td>
                    <td class="active">40</td>

          <td>40</td>
          <td>40</td>
          <td>40</td>
          <td class="active">40</td>
          
          <td class="active">40</td>

        </tr>
           <tr>
          <th scope="row" colspan="2">RUPIAH THD USD</th>
          <td>40</td>
          <td>40</td>
                    <td class="active">40</td>

          <td>40</td>
          <td>40</td>
                    <td class="active">40</td>

          <td>40</td>
          <td >40</td>
          <td>40</td>
          <td class="active">40</td>
                    <td class="active">40</td>


        </tr>
           <tr>
          <th scope="row" colspan="2">INDEX USD/TON</th>
          <td>40</td>
          <td>40</td>
                    <td class="active">40</td>

          <td>40</td>
          <td>40</td>
                    <td class="active">40</td>

          <td>40</td>
          <td>40</td>
          <td>40</td>
          <td class="active">40</td>
          
          <td class="active">40</td>

        </tr>
      </tbody>
    </table>
   </div>
  </div>
</div>
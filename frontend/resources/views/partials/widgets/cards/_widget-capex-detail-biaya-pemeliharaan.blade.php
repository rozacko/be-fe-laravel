<div class="card card-detail-biaya-pemeliharaan">
  {{-- <div class="card-header">
    <span>Detail Biaya Pemeliharaan</span>
  </div> --}}
  <div class="card-body">
    <div class="table-responsive">
      <table class="table">
        <tbody>
          <tr>
            <th colspan="3" rowspan="3" class="decsription-title">DESKRIPSI</th>
            <td colspan="6" class="text-center border-bottom">PROGRES PENYERAPAN ANGGARAN CAPEX</td>
          </tr>
          <tr>
            <th scope="col" class="border-bottom">BUDGET
              2022</th>
            <td class="border-bottom">PLAVONT
              SPENDING</td>
            <td class="border-bottom">REALISASI SPENDING</td>
            <td class="border-bottom">PROGNOSE SPENDING</td>
            <td class="border-bottom">SPENDING (%)</td>
            <td class="border-bottom">PROGNOSE (%)</td>
          </tr>
          <tr>
            <td>a</td>
            <td>b</td>
            <td>c</td>
            <td>d</td>
            <td>% a</td>
            <td>% b</td>
          </tr>
          <tr class="bg-black-800 pl-2">
            <th scope="row" colspan="9">I. OPERATIONAL CAPEX</th>
          </tr>
          <tr>
            <th scope="row" colspan="4">Capex New</th>
            <td>12345</td>
            <td>12345</td>
            <td>12345</td>
            <td>12345</td>
            <td>12345</td>
          </tr>
          <tr>
            <th scope="row" colspan="4">Capex Over</th>
            <td>12345</td>
            <td>12345</td>
            <td>12345</td>
            <td>12345</td>
            <td>12345</td>
          </tr>
          <tr class="bg-black-400 pl-2">
            <th scope="row" colspan="3">Total</th>
            <td>12345</td>
            <td>12345</td>
            <td>12345</td>
            <td>12345</td>
            <td>12345</td>
            <td>12345</td>
          </tr>
          <tr class="bg-black-800 pl-2">
            <th scope="row" colspan="9">II. DEVELOPMENT CAPEX</th>
          </tr>
          <tr>
            <th scope="row" colspan="4">CAPEX NEW</th>
            <td>12345</td>
            <td>12345</td>
            <td>12345</td>
            <td>12345</td>
            <td>12345</td>
          </tr>
          <tr>
            <th scope="row" colspan="4">CAPEX OVER</th>
            <td>12345</td>
            <td>12345</td>
            <td>12345</td>
            <td>12345</td>
            <td>12345</td>
          </tr>
          <tr class="bg-black-400 pl-2">
            <th scope="row" colspan="4">TOTAL</th>
            <td>12345</td>
            <td>12345</td>
            <td>12345</td>
            <td>12345</td>
            <td>12345</td>
          </tr>
          <tr class="bg-black-200 pl-2">
            <th scope="row" colspan="4">GRAND TOTAL</th>
            <td>12345</td>
            <td>12345</td>
            <td>12345</td>
            <td>12345</td>
            <td>12345</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>
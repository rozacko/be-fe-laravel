<div class="card card-overhoul-post-biaya-opex">
  <div class="card-body">
  <table class="w-100" id="table_post_opex">
      <thead>
        <tr class="bg-black">
          <th colspan="3" class="text-center">Post Biaya OPEX</th>

        </tr>
         <tr class="bg-gray">
          <th class="text-center">Unit Kerja</th>
          <th class="text-center">Rencana</th>
          <th class="text-center">Realisasi</th>
        </tr>
        <tr class="bg-yellow">
          <td></td>
          <td class="text-right font-weight-bold" >Rp <span id="sum_rencana_opex"></span></td>
          <td class="text-right" >Rp <span id="sum_realisasi_opex"></span></td>
        </tr>
      </thead>
      <tbody >
       
      
      
      </tbody>
    </table>
</div>
</div>

<div class="card card-inspec-crusher">
  <img class="img-fluid card-img-top" src="{{ asset('assets/media/svg/plant/coal-mill.svg') }}" alt="" srcset="">
  <div class="card-header">
    <span>Coal Mill</span>
    <p>Last Update: 18-12-2022</p>
  </div>
  <div class="px-1">
    <div class="progress-group">
      <div class="progress">
        <div class="progress-bar bg-good" role="progressbar"
        style="width: 90%" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
      </div>
      <div class="progress-value">
        90%
      </div>
    </div>
    <div class="progress-group">
      <div class="progress">
        <div class="progress-bar bg-low-risk" role="progressbar"
        style="width: 8%" aria-valuenow="8" aria-valuemin="0" aria-valuemax="100"></div>
      </div>
      <div class="progress-value">
        8%
      </div>
    </div>
    <div class="progress-group">
      <div class="progress">
        <div class="progress-bar bg-medium-risk" role="progressbar"
        style="width: 2%" aria-valuenow="2" aria-valuemin="0" aria-valuemax="100"></div>
      </div>
      <div class="progress-value">
        2%
      </div>
    </div>
    <div class="progress-group">
      <div class="progress">
        <div class="progress-bar bg-high-risk" role="progressbar"
        style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
      </div>
      <div class="progress-value">
        0%
      </div>
    </div>
  </div>
</div>
<div class="card card-klin-rate-tdp ">
  <div class="card-header">
    <span>Kiln Rate (TPD)</span>
  </div>
  <div class="card-body">
    <div id="kt_charts_widget_klin_rate_tdp" class="charts"></div>
  </div>
</div>
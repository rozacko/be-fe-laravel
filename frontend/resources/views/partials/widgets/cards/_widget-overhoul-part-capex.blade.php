<div class="card card-overhoul-part-capex">
  <div class="card-body">
    <table id="table_part_capex" class="w-100">
      <thead>
        <tr>
          <th>PART CAPEX</th>
          <th>PRICE</th>
        </tr>
      </thead>
      <tbody>        
      </tbody>
      <tfoot>
         <tr>
          <th>TOTAL</th>
          <th></th>
        </tr>
      </tfoot>
    </table>
  </div>
</div>
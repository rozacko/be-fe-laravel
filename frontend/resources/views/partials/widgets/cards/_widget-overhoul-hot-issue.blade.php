<div class="card card-overhoul-hot-issue">
<div class="card-header">
  <img src="{{ image('misc/images/ghopo_hot_issue.png') }}" alt="sales">
</div>  
<div class="card-body" >
    <div class="table-responsive">
      <table class="w-100" id="table_overhaul_issue">
        <thead>
            <tr>
              <th>
                  Issues
              </th>
              <th>
                  Category
              </th>
            </tr>
        </thead>
        <tbody >
          
        </tbody>
      
      </table>
    </div>
  </div>
</div>
<div class="card-asset-production">
  <div class="card-header pl-0 pr-0 pt-2 pb-2">
    <span class="text-uppercase text-black font-weight-bold">AKUMULASI NILAI AKUISISI ASSET PRODUKSI (M)</span>
  </div>
  <div class="card-body p-0">
    <div id="kt_charts_widget_asset_prod_aquisition_two" class="charts"></div>
  </div>
</div>
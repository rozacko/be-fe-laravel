<div class="card card-overhoul-major-activity">
  <div class="card-body">
    <div class='table-responsive'>
      <table class="w-100" id="table_activity_major">
          <thead>
            <tr class="bg-black">
              <th>Major Activity</th>
              <th>% Planed</th>
              <th>% Actual</th>
              <th>% Variant</th>
            </tr>
          </thead>
          <tbody id="tbody_major">      
          </tbody>
        </table>
    </div>
  </div>
</div>

<div class="card card-overhoul-budget-opex">
  <div class="card-header flex">
    <div>
      <span>budget opex</span>
    </div>
    <div class="total">
      <div class="title">Total</div>
      <div class="nominal">IDR <span id="total_budget_opex"></span></div>
    </div>
  </div>
  <div class="card-body">
    <div id="chart_all_opex_donut" class="charts"></div>
  </div>
</div>
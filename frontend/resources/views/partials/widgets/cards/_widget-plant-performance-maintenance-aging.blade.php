<div class="card card-detail-biaya-pemeliharaan">
  <div class="card-header">
    <span>Total Maint Order : 24001</span>
    
  </div>
  <div class="card-body mt-5">
    <div class="row">
      <div class="col-lg-6">
        <span><b>Maintenance Order Aging</b></span>
        <div id="kt_charts_widget_plant_performance_maintenance_aging" class="charts"></div>
      </div>
      <div class="col-lg-6">
        <div class="row">
          <div class="col-sm-6">
            <p><b>TARGET</b></p>
            <p><b>5</b></p>
          </div>
          <div class="col-sm-6 text-right" style="float: right;">
            <p><b>REAL</b></p>
            <p><b>5</b></p>
          </div>
        </div>
        <div class="row">        
        <table class="table table-striped">
          <thead>
            <tr>
              <th colspan="4" style="background-color: red; text-align: center;">INDICATOR</th>
            </tr>
            <tr>
              <th class="text-center" style="background-color: aqua;">A</th>
              <th class="text-center" style="background-color: yellow;">B</th>
              <th class="text-center" style="background-color: green;">C</th>
              <th class="text-center" style="background-color: #ff9191;">D</th>
            </tr>
          </thead>
          <tbody>
            <tr>
                <td>10</td>
                <td>20</td>
                <td>30</td>
                <td>40</td>
              </tr>
          </tbody>
        </table>
        </div>
      </div>
    </div>
  </div>
</div>
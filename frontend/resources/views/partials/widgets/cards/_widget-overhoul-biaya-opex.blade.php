<div class="card card-overhoul-biaya-opex">
   <div class="card-body">
    <div class="content">
      <div class="title">Penggunaan Biaya OPEX</div>
      <div class="percentage ml-3"><span id="persen_real_opex"></span>%</div>
    </div>
  </div>
</div>


<div class="card card-overhoul-progress-commisioning">
    <div class="card-header">
        <div class="title text-center">PROGRESS COMMISIONING</div>
    </div>
    <div class="card-body">
        <table class="w-100">
          <thead>
            <tr class="bg-black">
              <th>AREA</th>
              <th>TOTAL</th>
              <th>OPEN</th>
              <th>CLOSE</th>
            </tr>
          
          </thead>
          <tbody >
            <tr>
              <td class="bg-blue">TOTAL</td>
              <td>98%</td>
              <td>0,05%</td>
              <td>0,05%</td>
            </tr>
            <tr>
              <td class="bg-red">OPEN</td>
            <td>98%</td>
              <td>0,05%</td>
              <td>0,05%</td>
            </tr>
            <tr>
              <td class="bg-green">CLOSE</td>
              <td>98%</td>
              <td>0,05%</td>
              <td>0,05%</td>
            </tr>
          
          </tbody>
        </table>



      </div>
</div>

<div class="card card-overhoul-unsave">
    <div class="card-header">
        <div class="title text-center">UNSAFE CONDITION</div>
    </div>
    <div class="card-body">
        <table class="w-100" id="table_unsafe_condition">
          <thead>
            <tr class="bg-black">
              <th>AREA</th>
              <th>JUMLAH TEMUAN</th>
              <th>OPEN</th>
              <th>CLOSE</th>
              <th>PROGRES</th>
            </tr>
          
          </thead>
          <tbody >
          </tbody>
          <tfoot>            
            <tr class="bg-yellow">
                <td>TOTAL</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
          </tfoot>
        </table>



      </div>
</div>
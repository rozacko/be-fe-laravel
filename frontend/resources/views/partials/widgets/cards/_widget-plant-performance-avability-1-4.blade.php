<div class="card" >
  <div class="card-header">
    
  </div>
  <div class="card-body mt-5">
    <div class="row">
      <div class="col-lg-4">
        <span style="padding-left:10px;"><b>AVABILITY KILN 1-4</b></span>
        <div id="kt_charts_widget_plant_performance_avability_klin" class="charts"></div>
      </div>
      <div class="col-lg-8">
          <div class="row">
              <span style="padding-left:10px;"><b>AVABILITY KILN (%)</b></span>
              <div style="padding-left:5%;" id="kt_charts_widget_plant_performance_avability_klin_vertical" class="charts" style="height:0 !important;"></div>
          </div>
          <div class="row">
          <div class="table-responsive">
              <table class="table table-bordered">
                <tbody>
                  <tr>
                    <td class="d-flex align-items-center ">
                      <div class="circle" style="background: #da0806;"></div> <div class="percent ml-2">TB1</div>
                    </td>
                    <td>10</td>
                    <td>20</td>
                    <td>30</td>
                    <td>40</td>
                    <td>50</td>
                    <td>60</td>
                    <td>70</td>
                    <td>80</td>
                    <td>90</td>
                    <td>10</td>
                    <td>11</td>
                    <td>12</td>
                    <td>13</td>
                  </tr>
                  <tr>
                    <td class="d-flex align-items-center ">
                      <div class="circle" style="background: #f2ff03;"></div> <div class="percent ml-2">TB2</div>
                    </td>
                    <td>10</td>
                    <td>20</td>
                    <td>30</td>
                    <td>40</td>
                    <td>50</td>
                    <td>60</td>
                    <td>70</td>
                    <td>80</td>
                    <td>90</td>
                    <td>10</td>
                    <td>11</td>
                    <td>12</td>
                    <td>13</td>
                  </tr>
                  <tr>
                    <td class="d-flex align-items-center ">
                      <div class="circle" style="background: #1a2a5e;"></div> <div class="percent ml-2">TB3</div>
                    </td>
                    <td>10</td>
                    <td>20</td>
                    <td>30</td>
                    <td>40</td>
                    <td>50</td>
                    <td>60</td>
                    <td>70</td>
                    <td>80</td>
                    <td>90</td>
                    <td>10</td>
                    <td>11</td>
                    <td>12</td>
                    <td>13</td>
                  </tr>
                  <tr>
                    <td class="d-flex align-items-center ">
                      <div class="circle" style="background: #da0806;"></div> <div class="percent ml-2">TB4</div>
                    </td>
                    <td>10</td>
                    <td>20</td>
                    <td>30</td>
                    <td>40</td>
                    <td>50</td>
                    <td>60</td>
                    <td>70</td>
                    <td>80</td>
                    <td>90</td>
                    <td>10</td>
                    <td>11</td>
                    <td>12</td>
                    <td>13</td>
                  </tr>
                  <tr>
                    <td class="d-flex align-items-center ">
                      <div class="circle" style="background: #d92372;"></div> <div class="percent ml-2">TB45</div>
                    </td>
                    <td>10</td>
                    <td>20</td>
                    <td>30</td>
                    <td>40</td>
                    <td>50</td>
                    <td>60</td>
                    <td>70</td>
                    <td>80</td>
                    <td>90</td>
                    <td>10</td>
                    <td>11</td>
                    <td>12</td>
                    <td>13</td>
                  </tr>

                    
                </tbody>
              </table>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
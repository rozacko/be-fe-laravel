<div class="card-coal-stock-flex">
  <div class="card card-coal-stock">
  <div class="card-body">
    <div class="box-coal">
      <div class="box-coal__content">
        <div class="row">
            <div class="col-12">
                <div class="row" style="text-align: center;">
                    <div class="col mt-3" >
                        <div class="title">
                            <label for="Coal Stock">Cement Stock</label>
                        </div>
                    </div>
                    <div class="col mt-3" >
                        <img src="assets/media/logos/ghopo_mark_up.png" alt="logo mark up">
                    </div>
                    <div class="col mt-3" >
                        <div class="box-coal__content_information">
                            <label for="Clinker Production">7000 ton - Sept</label>
                            <div class="devider"></div>
                            <div class="sub-condition">7000 ton - Agus</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="card card-coal-stock">
  <div class="card-body">
    <div class="box-coal">
      <div class="box-coal__content">
        <div class="row">
            <div class="col-12">
                <div class="row" style="text-align: center;">
                    <div class="col mt-3">
                        <div class="title">
                            <label for="Coal Stock">Roal Mill Stock</label>
                        </div>
                    </div>
                    <div class="col mt-3">
                        <img src="assets/media/logos/ghopo_mark_down.png" alt="logo mark down">
                    </div>
                    <div class="col mt-3">
                        <div class="box-coal__content_information">
                            <label for="Clinker Production">7000 ton - Sept</label>
                            <div class="devider"></div>
                            <div class="sub-condition">7000 ton - Agus</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
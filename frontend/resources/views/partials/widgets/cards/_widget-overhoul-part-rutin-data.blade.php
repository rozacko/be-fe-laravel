<div class="card card-overhoul-part-rutin-data">
  <div class="card-body">
    <div class="title text-center font-weight-bold mb-2">RUTIN</div>
    <div class="table-responsive">
      <table id="datatable-rutin" class="w-100">
        <thead>
          <tr>
            <th>UNIT KERJA</th>
            <th>NOT READY</th>
            <th>READY</th>
            <th>GRAND TOTAL</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
        <tfoot>
          <tr>
            <th>Grand Total</th>
            <th></th>
            <th></th>
            <th>100%</th>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>
</div>
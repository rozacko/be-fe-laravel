<div class="card card-volume">
  <div class="card-header">
    <div class="card-title">
      <span>
        Volume
      </span>
    </div>
  </div>

  <div class="card-body">
    <div class="box-volume">
      <div class="row">
        <div class="col-md-4">
              <div class="box-volume__content">
                <img src="assets/media/logos/ghopo_mark_up.png" alt="logo volume clinker production">
                <div class="box-volume__content_information">
                  <div for="" class="condition">Clinker Production</div>
                  <label class="">5232 ton</label>
                  <div class="sub-condition">45 % RKAP</div>
                </div>
              </div>

        </div>
        <div class="col-md-4">
           <div class="box-volume__content">
        <img src="assets/media/logos/ghopo_mark_up.png" alt="logo volume clinker production">
        <div class="box-volume__content_information">
          <div for="" class="condition">Cement Production</div>
          <label class="">245 ton</label>
          <div class="sub-condition">45 % RKAP</div>
        </div>
      </div>
        </div>
        <div class="col-md-4">
           <div class="box-volume__content">
        <img src="assets/media/logos/ghopo_mark_down.png" alt="logo volume clinker production">
        <div class="box-volume__content_information">
        <div for="" class="condition">Cement Sales</div>
          <label class="">245 ton</label>
          <div class="sub-condition">45 % RKAP</div>
        </div>
      </div>
        </div>
      </div>
     
      
     

     
    </div>
  </div>
</div>
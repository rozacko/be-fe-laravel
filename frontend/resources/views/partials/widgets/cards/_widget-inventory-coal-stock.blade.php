
<div class="card card-coal-stock-inventory">
  <div class="card-header">
    <div class="card-title">
      <span>Inventory</span>
    </div>
  </div>
  <div class="card-body pt-0">
    <div class="box-coal">
      <div class="box-coal__inventory">
        <div class="row">
            <div class="col-12">
                <div class="row" style="text-align: center;">
                    <div class="col mt-3" >
        <div class="title">
          <label for="Coal Stock">Coal Stock</label>
        </div>
                    </div>
                    <div class="col mt-3" >
        <img src="assets/media/logos/ghopo_mark_down.png" alt="logo mark down">
                    </div>
                    <div class="col mt-3" >
                    <div class="box-coal__inventory_information">
          <label for="Clinker Production">7000 ton - Sept</label>
          <div class="devider"></div>
          <div class="sub-condition">7000 ton - Agus</div>
        </div>
      </div>
    </div>
            </div>
        </div>
      </div>
    </div>
    <div class="card-body__information">
      <ul>
        <li>Stok Batu bara akhir septermber ada penurunan dibandingkan bulan agustus menjadi 75ribu ton</li>
        <li>Untuk bulan september komitment</li>
      </ul>

    </div>
  </div>
</div>
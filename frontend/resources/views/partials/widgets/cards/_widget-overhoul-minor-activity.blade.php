<div class="card card-overhoul-major-activity">
  <div class="card-body">
  <table class="w-100" id="table_minor_activity">
      <thead>
        <tr class="bg-black">
          <th>Minor Activity</th>
          <th>% Planed</th>
          <th>% Actual</th>
          <th>% Variant</th>
        </tr>
      </thead>
      <tbody id="tbody_minor">   
      </tbody>
    </table>
</div>
</div>

<div class="card">
  <div class="card-header mt-5">
    <span><b>AVAILABLE DOWNTIME</b></span>
  </div>
  <div class="card-body">
  <div class="table-responsive">
      <table class="table taable-responsive">
        <thead>
          <tr style="background-color: #616161;">
            <th style="color:white; font-weight: bold; text-align: center;">Description</th>
            <th style="color:white; font-weight: bold; text-align: center;">Plant</th>
            <th style="color:white; font-weight: bold; text-align: center;">Target</th>
            <th style="color:white; font-weight: bold; text-align: center;">Realisasi</th>
            <th style="color:white; font-weight: bold; text-align: center;">Prognosa</th>
            <th style="color:white; font-weight: bold; text-align: center;">Available</th>
            <th style="color:white; font-weight: bold; text-align: center;">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>OVERHAUL</td>
            <td>PLANT1</td>
            <td class="bg-target">1</td>
            <td class="bg-realisasi">1</td>
            <td class="bg-prognosa">1</td>
            <td class="bg-available">1</td>
            <td>Description 2</td>
          </tr>
          <tr>
            <td></td>
            <td>PLANT2</td>
            <td class="bg-target">2</td>
            <td class="bg-realisasi">2</td>
            <td class="bg-prognosa">2</td>
            <td class="bg-available">2</td>
            <td>Description 3</td>
          </tr>
          <tr>
            <td></td>
            <td style="font-weight: bold;">TOTAL</td>
            <td style="font-weight: bold;" class="bg-target">3</td>
            <td style="font-weight: bold;" class="bg-realisasi">3</td>
            <td style="font-weight: bold;" class="bg-prognosa">3</td>
            <td style="font-weight: bold;" class="bg-available">3</td>
            <td>Description 3</td>
          </tr>
          <tr>
            <td style="border-top: 1px solid;">OVERHAULA</td>
            <td>PLANT1</td>
            <td class="bg-target">1</td>
            <td class="bg-realisasi">1</td>
            <td class="bg-prognosa">1</td>
            <td class="bg-available">1</td>
            <td>Description 2</td>
          </tr>
          <tr>
            <td></td>
            <td>PLANT2</td>
            <td class="bg-target">2</td>
            <td class="bg-realisasi">2</td>
            <td class="bg-prognosa">2</td>
            <td class="bg-available">2</td>
            <td>Description 3</td>
          </tr>
          <tr>
            <td></td>
            <td style="font-weight: bold;">TOTAL</td>
            <td style="font-weight: bold;" class="bg-target">3</td>
            <td style="font-weight: bold;" class="bg-realisasi">3</td>
            <td style="font-weight: bold;" class="bg-prognosa">3</td>
            <td style="font-weight: bold;" class="bg-available">3</td>
            <td>Description 3</td>
          </tr>
          <tr>
            <td style="border-top: 1px solid;">OVERHAULA</td>
            <td>PLANT1</td>
            <td class="bg-target">1</td>
            <td class="bg-realisasi">1</td>
            <td class="bg-prognosa">1</td>
            <td class="bg-available">1</td>
            <td>Description 2</td>
          </tr>
          <tr>
            <td></td>
            <td>PLANT2</td>
            <td class="bg-target">2</td>
            <td class="bg-realisasi">2</td>
            <td class="bg-prognosa">2</td>
            <td class="bg-available">2</td>
            <td>Description 3</td>
          </tr>
          <tr>
            <td></td>
            <td style="font-weight: bold;">TOTAL</td>
            <td style="font-weight: bold;" class="bg-target">3</td>
            <td style="font-weight: bold;" class="bg-realisasi">3</td>
            <td style="font-weight: bold;" class="bg-prognosa">3</td>
            <td style="font-weight: bold;" class="bg-available">3</td>
            <td>Description 3</td>
          </tr>
          <tr>
            <td style="border-top: 1px solid;">OVERHAULA</td>
            <td>PLANT1</td>
            <td class="bg-target">1</td>
            <td class="bg-realisasi">1</td>
            <td class="bg-prognosa">1</td>
            <td class="bg-available">1</td>
            <td>Description 2</td>
          </tr>
          <tr>
            <td></td>
            <td>PLANT2</td>
            <td class="bg-target">2</td>
            <td class="bg-realisasi">2</td>
            <td class="bg-prognosa">2</td>
            <td class="bg-available">2</td>
            <td>Description 3</td>
          </tr>
          <tr>
            <td></td>
            <td style="font-weight: bold;">TOTAL</td>
            <td style="font-weight: bold;" class="bg-target">3</td>
            <td style="font-weight: bold;" class="bg-realisasi">3</td>
            <td style="font-weight: bold;" class="bg-prognosa">3</td>
            <td style="font-weight: bold;" class="bg-available">3</td>
            <td>Description 3</td>
          </tr>
          <tr>
            <td style="border-top: 1px solid;">OVERHAULA</td>
            <td>PLANT1</td>
            <td class="bg-target">1</td>
            <td class="bg-realisasi">1</td>
            <td class="bg-prognosa">1</td>
            <td class="bg-available">1</td>
            <td>Description 2</td>
          </tr>
          <tr>
            <td></td>
            <td>PLANT2</td>
            <td class="bg-target">2</td>
            <td class="bg-realisasi">2</td>
            <td class="bg-prognosa">2</td>
            <td class="bg-available">2</td>
            <td>Description 3</td>
          </tr>
          <tr>
            <td></td>
            <td style="font-weight: bold;">TOTAL</td>
            <td style="font-weight: bold;" class="bg-target">3</td>
            <td style="font-weight: bold;" class="bg-realisasi">3</td>
            <td style="font-weight: bold;" class="bg-prognosa">3</td>
            <td style="font-weight: bold;" class="bg-available">3</td>
            <td>Description 3</td>
          </tr>
        </tbody>
        <tfoot>
        <tr style="background-color: #616161;">
            <th style="color:white; font-weight: bold; text-align: center ;">Description</th>
            <th style="color:white; font-weight: bold; text-align: center ;">Plant</th>
            <th style="color:white; font-weight: bold; text-align: center ;">Target</th>
            <th style="color:white; font-weight: bold; text-align: center ;">Realisasi</th>
            <th style="color:white; font-weight: bold; text-align: center ;">Prognosa</th>
            <th style="color:white; font-weight: bold; text-align: center ;">Available</th>
            <th style="color:white; font-weight: bold; text-align: center ;">Description</th>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>
</div>

<div class="card card-klin-rate">
		<!--begin::Header-->
			<div class="card-header">
											<!--begin::Title-->
			<h3 class="card-title align-items-start flex-column">
				<span class="card-label fw-bold text-dark">Inventory</span>
			</h3>
											<!--end::Title-->
		</div>
										<!--end::Header-->
										<!--begin::Card body-->
										<div class="card-body d-flex justify-content-between flex-column pb-1 px-0">
										
											<!--begin::Chart-->
											<div id="kt_charts_widget_4" class="min-h-auto" style="height: 325px"></div>
											<!--end::Chart-->
										</div>
										<!--end::Card body-->
</div>
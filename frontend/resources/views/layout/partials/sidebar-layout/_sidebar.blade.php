<!--begin::sidebar-->
{{-- <div id="kt_app_sidebar" class="app-sidebar flex-column" data-kt-drawer="true" data-kt-drawer-name="app-sidebar"
	data-kt-drawer-activate="{default: true, lg: false}" data-kt-drawer-overlay="true" data-kt-drawer-width="225px"
	data-kt-drawer-direction="start" data-kt-drawer-toggle="#kt_app_sidebar_mobile_toggle">

	@include(config('settings.KT_THEME_LAYOUT_DIR').'/partials/sidebar-layout/sidebar/_logo')

	@include(config('settings.KT_THEME_LAYOUT_DIR').'/partials/sidebar-layout/sidebar/_menu')

	@include(config('settings.KT_THEME_LAYOUT_DIR').'/partials/sidebar-layout/sidebar/_footer')

</div> --}}
<!--end::sidebar-->


<div id="kt_aside" class="aside p-2" data-kt-drawer="true" data-kt-drawer-name="aside" data-kt-drawer-activate="true"
	data-kt-drawer-overlay="true" data-kt-drawer-width="{default:'200px', '300px': '285px'}"
	data-kt-drawer-direction="start" data-kt-drawer-toggle="#kt_aside_toggle">
	<!--begin::Aside menu-->
	<div class="aside-menu flex-column-fluid">
		<!--begin::Aside Menu-->
		<div class="hover-scroll-overlay-y my-5 me-n4 pe-4" id="kt_aside_menu_wrapper" data-kt-scroll="true"
			data-kt-scroll-activate="true" data-kt-scroll-height="auto" data-kt-scroll-dependencies="#kt_aside_footer"
			data-kt-scroll-wrappers="#kt_aside, #kt_aside_menu" data-kt-scroll-offset="2px">
			<!--begin::Menu-->
			<div
				class="menu menu-column menu-sub-indention menu-active-bg menu-state-primary menu-title-gray-700 fs-6 menu-rounded w-100 fw-semibold"
				id="#kt_aside_menu" data-kt-menu="true">
				<!--begin:Menu item-->
				<div data-kt-menu-trigger="click" class="menu-item here show menu-accordion">
					<!--begin:Menu link-->
					<span class="menu-link">
						<span class="menu-icon">
							<!--begin::Svg Icon | path: icons/duotune/general/gen025.svg-->
							<span class="svg-icon svg-icon-2">
								<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
									<rect x="2" y="2" width="9" height="9" rx="2" fill="currentColor" />
									<rect opacity="0.3" x="13" y="2" width="9" height="9" rx="2" fill="currentColor" />
									<rect opacity="0.3" x="13" y="13" width="9" height="9" rx="2" fill="currentColor" />
									<rect opacity="0.3" x="2" y="13" width="9" height="9" rx="2" fill="currentColor" />
								</svg>
							</span>
							<!--end::Svg Icon-->
						</span>
						<span class="menu-title">Dashboards</span>
						<span class="menu-arrow"></span>
					</span>
					<!--end:Menu link-->
					<!--begin:Menu sub-->
					<div class="menu-sub menu-sub-accordion">
						<!--begin:Menu item-->
						<div class="menu-item">
							<!--begin:Menu link-->
							<a class="{{ Route::current()->getName('dashboard') === 'dashboard' ? 'menu-link  active' : 'menu-link 	' }}"
								href="{{ route('dashboard') }}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">
									Dashboard
								</span>
							</a>
							<!--end:Menu link-->
						</div>
						<!--end:Menu item-->

					</div>
					<!--end:Menu sub-->
				</div>
				<!--end:Menu item-->
				<!--begin:Menu item-->
				<div class="menu-item pt-5">
					<!--begin:Menu content-->
					<div class="menu-content">
						<span class="menu-heading fw-bold text-uppercase fs-7">Pages</span>
					</div>
					<!--end:Menu content-->
				</div>
				<!--end:Menu item-->
				<!--begin:Menu item-->
				<div data-kt-menu-trigger="click" class="menu-item menu-accordion">
					<!--begin:Menu link-->
					<span class="menu-link">
						<span class="menu-icon">
							<!--begin::Svg Icon | path: icons/duotune/communication/com005.svg-->
							<span class="svg-icon svg-icon-2">
								<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path
										d="M20 14H18V10H20C20.6 10 21 10.4 21 11V13C21 13.6 20.6 14 20 14ZM21 19V17C21 16.4 20.6 16 20 16H18V20H20C20.6 20 21 19.6 21 19ZM21 7V5C21 4.4 20.6 4 20 4H18V8H20C20.6 8 21 7.6 21 7Z"
										fill="currentColor" />
									<path opacity="0.3"
										d="M17 22H3C2.4 22 2 21.6 2 21V3C2 2.4 2.4 2 3 2H17C17.6 2 18 2.4 18 3V21C18 21.6 17.6 22 17 22ZM10 7C8.9 7 8 7.9 8 9C8 10.1 8.9 11 10 11C11.1 11 12 10.1 12 9C12 7.9 11.1 7 10 7ZM13.3 16C14 16 14.5 15.3 14.3 14.7C13.7 13.2 12 12 10.1 12C8.10001 12 6.49999 13.1 5.89999 14.7C5.59999 15.3 6.19999 16 7.39999 16H13.3Z"
										fill="currentColor" />
								</svg>
							</span>
							<!--end::Svg Icon-->
						</span>
						<span class="menu-title">Maintenance</span>
						<span class="menu-arrow"></span>
					</span>
					<!--end:Menu link-->
					<!--begin:Menu sub-->
					<div class="menu-sub menu-sub-accordion">

						<!--begin:Menu item-->
						<div class="menu-item">
							<!--begin:Menu link-->
							<a class="{{ Route::current()->getName('project') === 'project' ? 'menu-link  active' : 'menu-link' }}"
								href="{{ route('project') }}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Projects</span>
							</a>
							<!--end:Menu link-->
						</div>

						<div class="menu-item">
							<a class="{{ Route::current()->getName('maintenance-cost') === 'maintenance-cost' ? 'menu-link  active' : 'menu-link' }}"
								href="{{ route('maintenance-cost') }}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Maintenance Cost</span>
							</a>
						</div>

						{{-- <div class="menu-item">
							<a class="{{ Route::current()->getName('capex-report-summary') === 'capex-report-summary' ? 'menu-link  active' : 'menu-link' }}"
								href="{{ route('capex-report-summary') }}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Capex Report Summary</span>
							</a>
						</div>
						<div class="menu-item">
							<a class="{{ Route::current()->getName('capex-report') === 'capex-report' ? 'menu-link  active' : 'menu-link' }}"
								href="{{ route('capex-report') }}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Capex Report</span>
							</a>
						</div> --}}

						<!-- <div class="menu-item">
											<a class="{{ Route::current()->getName('overhoul-report') === 'tpm-report' ? 'menu-link  active' : 'menu-link' }}" href="{{ route('overhoul-report') }}">
												<span class="menu-bullet">
													<span class="bullet bullet-dot"></span>
												</span>
												<span class="menu-title">Plant Performance</span>
											</a>
										</div> -->


					</div>
					<!--end:Menu sub-->
				</div>
				<!--end:Menu item-->
				<div data-kt-menu-trigger="click" class="menu-item menu-accordion">
					<!--begin:Menu link-->
					<span class="menu-link">
						<span class="menu-icon">
							<!--begin::Svg Icon | path: icons/duotune/communication/com005.svg-->
							<span class="svg-icon svg-icon-2">
								<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path
										d="M20 14H18V10H20C20.6 10 21 10.4 21 11V13C21 13.6 20.6 14 20 14ZM21 19V17C21 16.4 20.6 16 20 16H18V20H20C20.6 20 21 19.6 21 19ZM21 7V5C21 4.4 20.6 4 20 4H18V8H20C20.6 8 21 7.6 21 7Z"
										fill="currentColor" />
									<path opacity="0.3"
										d="M17 22H3C2.4 22 2 21.6 2 21V3C2 2.4 2.4 2 3 2H17C17.6 2 18 2.4 18 3V21C18 21.6 17.6 22 17 22ZM10 7C8.9 7 8 7.9 8 9C8 10.1 8.9 11 10 11C11.1 11 12 10.1 12 9C12 7.9 11.1 7 10 7ZM13.3 16C14 16 14.5 15.3 14.3 14.7C13.7 13.2 12 12 10.1 12C8.10001 12 6.49999 13.1 5.89999 14.7C5.59999 15.3 6.19999 16 7.39999 16H13.3Z"
										fill="currentColor" />
								</svg>
							</span>
							<!--end::Svg Icon-->
						</span>
						<span class="menu-title">Overhoul Report</span>
						<span class="menu-arrow"></span>
					</span>
					<!--end:Menu link-->
					<!--begin:Menu sub-->
					<div class="menu-sub menu-sub-accordion">

						<!--begin:Menu item-->
						<div class="menu-item">
							<!--begin:Menu link-->
							<a class="{{ Route::current()->getName('overhoul-report') === 'tpm-report' ? 'menu-link  active' : 'menu-link' }}"
								href="{{ route('overhoul-report') }}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Dashboard Overhoul</span>
							</a>
							<!--end:Menu link-->
						</div>

					</div>
					<!--end:Menu sub-->
				</div>
				{{-- Menu Tpm Report --}}
				<!--begin:Menu item-->
				<div data-kt-menu-trigger="click" class="menu-item menu-accordion">
					<!--begin:Menu link-->
					<span class="menu-link">
						<span class="menu-icon">
							<!--begin::Svg Icon | path: icons/duotune/communication/com005.svg-->
							<span class="svg-icon svg-icon-2">
								<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path
										d="M20 14H18V10H20C20.6 10 21 10.4 21 11V13C21 13.6 20.6 14 20 14ZM21 19V17C21 16.4 20.6 16 20 16H18V20H20C20.6 20 21 19.6 21 19ZM21 7V5C21 4.4 20.6 4 20 4H18V8H20C20.6 8 21 7.6 21 7Z"
										fill="currentColor" />
									<path opacity="0.3"
										d="M17 22H3C2.4 22 2 21.6 2 21V3C2 2.4 2.4 2 3 2H17C17.6 2 18 2.4 18 3V21C18 21.6 17.6 22 17 22ZM10 7C8.9 7 8 7.9 8 9C8 10.1 8.9 11 10 11C11.1 11 12 10.1 12 9C12 7.9 11.1 7 10 7ZM13.3 16C14 16 14.5 15.3 14.3 14.7C13.7 13.2 12 12 10.1 12C8.10001 12 6.49999 13.1 5.89999 14.7C5.59999 15.3 6.19999 16 7.39999 16H13.3Z"
										fill="currentColor" />
								</svg>
							</span>
							<!--end::Svg Icon-->
						</span>
						<span class="menu-title">TPM Report</span>
						<span class="menu-arrow"></span>
					</span>
					<!--end:Menu link-->
					<!--begin:Menu sub-->
					<div class="menu-sub menu-sub-accordion">

						<!--begin:Menu item-->
						<div class="menu-item">
							<!--begin:Menu link-->
							<a class="{{ Route::current()->getName('tpm-report.index') === 'tpm-report.index' ? 'menu-link  active' : 'menu-link' }}"
								href="{{ route('tpm-report.index') }}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Dashboard TPM</span>
							</a>
							<!--end:Menu link-->
						</div>

						<!--begin:Menu item-->
						<div class="menu-item">
							<!--begin:Menu link-->
							<a class="{{ Route::current()->getName('tpm-report-laporan-gugus.index') === 'tpm-report-laporan-gugus.index' ? 'menu-link  active' : 'menu-link' }}"
								href="{{ route('tpm-report-laporan-gugus.index') }}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Laporan Gugus</span>
							</a>
							<!--end:Menu link-->
						</div>

						<!--begin:Menu item-->
						<div class="menu-item">
							<!--begin:Menu link-->
							<a class="{{ Route::current()->getName('tpm-report-laporan-kpi-member-tpm.index') === 'tpm-report-laporan-kpi-member-tpm.index' ? 'menu-link  active' : 'menu-link' }}"
								href="{{ route('tpm-report-laporan-kpi-member-tpm.index') }}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Laporan KPI Member TPM</span>
							</a>
							<!--end:Menu link-->
						</div>

						<!--begin:Menu item-->
						<div class="menu-item">
							<a class="{{ Route::current()->getName('mkpitpm.index') === 'mkpitpm.index' ? 'menu-link  active' : 'menu-link' }}"
								href="{{ route('mkpitpm.index') }}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Data KPI Member TPM</span>
							</a>
						</div>

						<!--begin:Menu item-->
						<div class="menu-item">
							<a class="{{ Route::current()->getName('mgugustpm.index') === 'mgugustpm.index' ? 'menu-link  active' : 'menu-link' }}"
								href="{{ route('mgugustpm.index') }}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Data Gugus TPM</span>
							</a>
						</div>

						<!--begin:Menu item-->
						<div class="menu-item">
							<a class="{{ Route::current()->getName('mtpm.index') === 'mtpm.index' ? 'menu-link  active' : 'menu-link' }}"
								href="{{ route('mtpm.index') }}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Data Dashboard TPM</span>
							</a>
						</div>

					</div>
					<!--end:Menu sub-->
				</div>


				<div data-kt-menu-trigger="click" class="menu-item menu-accordion">
					<!--begin:Menu link-->
					<span class="menu-link">
						<span class="menu-icon">
							<!--begin::Svg Icon | path: icons/duotune/abstract/abs029.svg-->
							<span class="svg-icon svg-icon-2">
								<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path
										d="M6.5 11C8.98528 11 11 8.98528 11 6.5C11 4.01472 8.98528 2 6.5 2C4.01472 2 2 4.01472 2 6.5C2 8.98528 4.01472 11 6.5 11Z"
										fill="currentColor" />
									<path opacity="0.3"
										d="M13 6.5C13 4 15 2 17.5 2C20 2 22 4 22 6.5C22 9 20 11 17.5 11C15 11 13 9 13 6.5ZM6.5 22C9 22 11 20 11 17.5C11 15 9 13 6.5 13C4 13 2 15 2 17.5C2 20 4 22 6.5 22ZM17.5 22C20 22 22 20 22 17.5C22 15 20 13 17.5 13C15 13 13 15 13 17.5C13 20 15 22 17.5 22Z"
										fill="currentColor" />
								</svg>
							</span>
							<!--end::Svg Icon-->
						</span>
						<span class="menu-title">Master Data</span>
						<span class="menu-arrow"></span>
					</span>

					<!--end:Menu link-->
					<div class="menu-sub menu-sub-accordion">
						<div class="menu-item">
							<a class="{{ Route::current()->getName('masterdata.mplant.index') === 'masterdata.mplant.index' ? 'menu-link  active' : 'menu-link' }}"
								href="{{ route('masterdata.mplant.index') }}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Plant & Pabrik</span>
							</a>
						</div>
						<div class="menu-item">
							<a class="{{ Route::current()->getName('masterdata.mplantarea.index') === 'masterdata.mplantarea.index' ? 'menu-link  active' : 'menu-link' }}"
								href="{{ route('masterdata.mplantarea.index') }}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Plant Area</span>
							</a>
						</div>
						<div class="menu-item">
							<a class="{{ Route::current()->getName('masterdata.mcostcenter.index') === 'masterdata.mcostcenter.index' ? 'menu-link  active' : 'menu-link' }}"
								href="{{ route('masterdata.mcostcenter.index') }}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Cost Center</span>
							</a>
						</div>
						<div class="menu-item">
							<a class="{{ Route::current()->getName('masterdata.mcostelement.index') === 'masterdata.mcostelement.index' ? 'menu-link  active' : 'menu-link' }}"
								href="{{ route('masterdata.mcostelement.index') }}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Cost Element</span>
							</a>
						</div>
						<div class="menu-item">
							<a class="{{ Route::current()->getName('masterdata.mworkgroup.index') === 'masterdata.mworkgroup.index' ? 'menu-link  active' : 'menu-link' }}"
								href="{{ route('masterdata.mworkgroup.index') }}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Workgroup</span>
							</a>
						</div>
						<div class="menu-item">
							<a class="{{ Route::current()->getName('masterdata.mprojectcapex.index') === 'masterdata.mprojectcapex.index' ? 'menu-link  active' : 'menu-link' }}"
								href="{{ route('masterdata.mprojectcapex.index') }}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Project Capex</span>
							</a>
						</div>
						<div class="menu-item">
							<a class="{{ Route::current()->getName('masterdata.marea.index') === 'masterdata.marea.index' ? 'menu-link  active' : 'menu-link' }}"
								href="{{ route('masterdata.marea.index') }}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Area</span>
							</a>
						</div>
						<div class="menu-item">
							<a class="{{ Route::current()->getName('masterdata.mkondisi.index') === 'masterdata.mkondisi.index' ? 'menu-link  active' : 'menu-link' }}"
								href="{{ route('masterdata.mkondisi.index') }}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Kondisi</span>
							</a>
						</div>
						<div class="menu-item">
							<a class="{{ Route::current()->getName('masterdata.meqinspec.index') === 'masterdata.meqinspec.index' ? 'menu-link  active' : 'menu-link' }}"
								href="{{ route('masterdata.meqinspec.index') }}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Equipment Inspection</span>
							</a>
						</div>
					</div>
					<!--end:Menu sub-->
				</div>
				{!!myMenu()!!}
			</div>
			<!--end::Menu-->
		</div>
	</div>
	<!--end::Aside menu-->
	<!--begin::Footer-->

	<!--end::Footer-->
</div>
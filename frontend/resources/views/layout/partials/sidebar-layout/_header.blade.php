<div id="kt_app_header" class="app-header" data-kt-sticky="true" data-kt-sticky-name="header"
     data-kt-sticky-animation="false" data-kt-sticky-offset="{default: '200px', lg: '300px'}">
    <!--begin::Container-->
    <div class="app-container container-xxl d-flex align-items-center flex-lg-stack">
        <!--begin::Brand-->
        <div class="d-flex align-items-center flex-grow-1 flex-lg-grow-0 me-2 me-lg-5">
            <!--begin::Wrapper-->
            <div class="flex-grow-1">
                <!--begin::Aside toggle-->
                <button data-kt-toggle="true" data-kt-toggle-state="active" data-kt-toggle-target="body"
                        data-kt-toggle-name="app-sidebar-minimize"
                        class="btn btn-icon btn-color-gray-800 btn-active-color-primary aside-toggle
                        justify-content-start w-30px w-lg-40px"
                        id="kt_aside_toggle">
                    <!--begin::Svg Icon | path: icons/duotune/general/gen059.svg-->
                    <span class="svg-icon svg-icon-2 text-white">
											<svg width="16" height="15" viewBox="0 0 16 15" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
												<rect y="6" width="16" height="3" rx="1.5" fill="currentColor" />
												<rect opacity="0.3" y="12" width="8" height="3" rx="1.5"
                                                      fill="currentColor" />
												<rect opacity="0.3" width="12" height="3" rx="1.5"
                                                      fill="currentColor" />
											</svg>
										</span>
                    <!--end::Svg Icon-->
                </button>
                <!--end::Aside toggle-->
                <!--begin::Header Logo-->
                <a href="../../demo18/dist/index.html">
                    <img alt="Logo" src="{{ asset('assets/media/logos/logo-sig-white.svg') }}"
                         class="h-30px h-lg-35px" />
                </a>
                <!--end::Header Logo-->
            </div>
            <!--end::Wrapper-->
            <!--begin:Search-->
            <div class="d-flex align-items-center">
                <!--begin::Search-->
                <div id="kt_header_search" class="header-search d-flex align-items-center w-lg-400px d-none d-sm-block"
                     data-kt-search-keypress="true" data-kt-search-min-length="2" data-kt-search-enter="enter"
                     data-kt-search-layout="menu" data-kt-search-responsive="lg" data-kt-menu-trigger="auto"
                     data-kt-menu-permanent="true" data-kt-menu-placement="{default: 'bottom-end', lg: 'bottom-start'}">
                    <span class="project-name">Group Head of Plant Operation (GHoPO)</span>


                </div>
                <!--end::Search-->
            </div>
            <!--end:Search-->
        </div>
        <!--end::Brand-->
        <!--begin::Toolbar wrapper-->
        <div class="d-flex align-items-stretch flex-shrink-0">
            <div class="d-flex align-items-center ms-1 ms-lg-3 me-2">
									<span class="date-now">

									{{ date('H:i d/m/y') }}
										<i class="mr-2 bi bi-calendar-check fs-1x text-white"></i>
									</span>
            </div>
            <!-- Date -->
            <!--begin::Chat-->
            <div class="d-flex align-items-center ms-1 ms-lg-3">
                <!--begin::Drawer wrapper-->
                <div class="btn btn-icon btn-primary position-relative w-30px h-30px w-md-40px h-md-40px"
                     id="kt_drawer_chat_toggle">99+
                </div>
                <!--end::Drawer wrapper-->
            </div>
            <!--end::Chat-->
            <!--begin::User menu-->
            <div class="d-flex align-items-center ms-1 ms-lg-3">
                <!--begin::Menu wrapper-->
                <div
                    class="btn btn-color-gray-800 btn-icon btn-active-light-primary w-30px
                     h-30px w-md-40px h-md-40px position-relative btn btn-color-gray-800
                     btn-icon btn-active-light-primary w-30px h-30px w-md-40px h-md-40px"
                    data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                    <!--begin::Svg Icon | path: icons/duotune/communication/com013.svg-->
                    <span class="svg-icon svg-icon-2x text-white">
											<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
												<path
                                                    d="M6.28548 15.0861C7.34369 13.1814 9.35142 12 11.5304
                                                    12H12.4696C14.6486 12 16.6563 13.1814 17.7145 15.0861L19.3493
                                                    18.0287C20.0899 19.3618 19.1259 21 17.601 21H6.39903C4.87406 21
                                                    3.91012 19.3618 4.65071 18.0287L6.28548 15.0861Z"
                                                    fill="currentColor" />
												<rect opacity="0.3" x="8" y="3" width="8" height="8" rx="4"
                                                      fill="currentColor" />
											</svg>
										</span>
                    <!--end::Svg Icon-->
                </div>
                <div
                    class=" menu menu-sub menu-sub-dropdown
                    menu-column menu-rounded menu-gray-800 menu-state-bg
                    menu-state-color fw-semibold py-4 fs-6 w-275px"
                    data-kt-menu="true">
                    <!--begin::Menu item-->
                    <div class="menu-item px-3">
                        <div class="menu-content d-flex align-items-center px-3">
                            <!--begin::Avatar-->
                            <div class="symbol symbol-50px me-5">
                                <img alt="Logo" src="assets/media/avatars/300-1.jpg" />
                            </div>
                            <!--end::Avatar-->
                            <!--begin::Username-->
                            <div class="d-flex flex-column">
                                <div class="fw-bold d-flex align-items-center fs-5">Robiyanto
                                </div>
                                <div class="fw-semibold text-muted text-hover-primary fs-7">Superadmin</div>
                            </div>
                            <!--end::Username-->
                        </div>
                    </div>
                    <div class="separator my-2"></div>
                    <!--end::Menu separator-->
                    <!--begin::Menu item-->
                    <div class="menu-item px-5">
                        <a href="{{ route('profile') }}" class="menu-link px-5">My Profile</a>
                    </div>

                    <div class="separator my-2"></div>
                    <div class="menu-item px-5 my-1">
                        <a href="#" class="menu-link px-5">Account Settings</a>
                    </div>
                    <div class="menu-item px-5">
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                        <a href="{{ route('logout') }}" class="menu-link px-5"
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Sign
                            Out</a></div>
                    <!--end::Menu item-->
                </div>
            </div>

        </div>
    </div>
</div>

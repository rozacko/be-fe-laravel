@extends('layout.master')

@section('content')
   <div class="container-fluid auth-section auth-center-absolute">
            <div class="row d-flex align-items-center justify-content-center">
                <div class="col-md-8">
                    <div class="right-section-inner">
                        <img class="d-none d-lg-block logo_ghopo" src="{{ image('misc/ghopo_portal.png') }}" alt="" style="width: 35%;"/>
                        <img class="d-none d-lg-block logo_ghopo" src="{{ image('misc/gif_login_pabrik.gif') }}" alt=""/>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="right-section-inner">
                        <form novalidate="novalidate" id="kt_sign_in_form" data-kt-redirect-url="/" action="client/login">
                        <div class="wrapper show-section" style="display: contents;">
                            <section class="steps step-2 step-custom">
                                <div class="steps-inner">
                                <div class="main-heading">
                                        <img src="{{ image('misc/logo/sig-logo-black.png') }}" alt="sig">
                                        <label for="">
                                            Group Head of Plant Operation
                                        </label>
                                        <p>
                                            Please login using the registered username and password
                                        </p>
                                </div>

                                   {{ $slot }}
                                </div>
                            </section>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
<div class="page-loader flex-column bg-dark bg-opacity-25">
    <span class="spinner-border text-primary" role="status"></span>
    <span class="text-gray-800 fs-6 fw-semibold mt-5">Loading...</span>
</div>
@endsection


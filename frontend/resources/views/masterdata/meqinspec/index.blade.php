{{-- Extends layout --}}
<x-default-layout>
<div class="card card-flush mt-6 mt-xl-9">
	<div class="card-header p-3">
		<div class="card-title flex-column">
			<h3 class="fw-bold mb-1">{{$pagetitle}}</h3>
		</div>
		<div class="card-toolbar my-1">		        
            <button type="button" class="btn btn-sm btn-primary float-right btn-add">
                Tambah Data
            </button>
		</div>
	</div>
	<div class="card-body pt-0">
        <table class="table table-bordered data-table" id="tb_eqinspec">
            <thead>
                <tr>
                <th>No</th>
                <th>Kode Equipment</th>
                <th>Nama Equipment</th>
                <th>Area</th>
                <th>Plant</th>
                <th>Ref SAP</th>
                <th>Action</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>

	</div>
</div>

<div class="modal fade" id="modal" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered mw-650px">
		<div class="modal-content rounded"></div>
	</div>
</div>

</x-default-layout>

<script>
    $(function() {
        var table = $('#tb_eqinspec').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route($route.'.table') }}",
            columns: [{ 
                data: 'DT_RowIndex', 
                name: 'DT_RowIndex', 
                orderable: false, 
                searchable: false 
                },
                {
                data: 'kd_equipment',
                name: 'kd_equipment'
                },
                {
                data: 'nm_equipment',
                name: 'nm_equipment'
                },
                {
                data: 'nm_area',
                name: 'nm_area'
                },
                {
                data: 'nm_plant',
                name: 'nm_plant'
                },
                {
                data: 'ref_sap',
                name: 'ref_sap'
                },  
                {
                data: 'action',
                name: 'action',
                orderable: false,
                searchable: false,
                width: 150
                },
            ]
        });
    });  
    setTimeout(function(){
        if ($('#alertNotif').length > 0) {
            $('#alertNotif').remove();
        }
    }, 5000) 

    $('.btn-add').click(function (e) {
        e.preventDefault();
        var url = "{{ route($route.'.create') }}";
        $.get(url, function (html) {
            $('#modal .modal-content').html(html);
            $('#modal').modal('show');            
        }).fail(function () {
            console.log('Gagal');
        });
    })

    function show(id){
        event.preventDefault(); 
        var url = '{{ route($route.".edit", ":id") }}';
        url = url.replace(':id', id);  
        $.get(url, function (html) {
            $('#modal .modal-content').html(html);
            $('#modal').modal('show');
        }).fail(function () {
            console.log('Gagal');
        });          
    }

    function btn_delete(id){
        event.preventDefault(); 
        var url = '{{ route($route.".destroy", ":id") }}';
        url = url.replace(':id', id);    
        Swal.fire({
            icon: 'warning',
            title: 'Apakah anda yakin?',
            text: "Data ini akan di hapus!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak',
            confirmButtonClass: 'btn btn-primary margin-right-10',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: true,
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function(isConfirm) {
            if (isConfirm.isConfirmed) {
                $.post(url, {_token: "{{ csrf_token() }}", _method: 'DELETE' }, function(res) {
                    if (res.status == 'success') {
                        Swal.fire(
                            'Success',
                            res.message,
                            'success'
                        );                    
                        $('#tb_eqinspec').DataTable().ajax.reload();
                    } else {
                        Swal.fire(
                            'Gagal',
                            res.message,
                            'error'
                        );
                    }
                }, 'json');
            } else {
                Swal.fire(
                    'Cancelled',
                    'Your data is safe',
                    'error'
                );
            } 
        });

        return false;
    }
</script>

<div class="modal-header pb-0 border-0 justify-content-end">
    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">					
        <span class="svg-icon svg-icon-1">
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="currentColor" />
                <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="currentColor" />
            </svg>
        </span>
    </div>
</div>
<div class="modal-body scroll-y px-10 px-lg-15 pt-0 pb-15">
    <div class="mb-13 text-center">
        <h1 class="mb-3">Edit Data</h1>        
        <div class="text-muted fw-semibold fs-5">If you need more info, please check 
        <a href="#" class="fw-bold link-primary">Project Guidelines</a>.</div>
    </div>
    <form action="{{ route($route . '.update', $data->id) }}" enctype="multipart/form-data" id="idForm">
        <div class="modal-body">
        @csrf  
            <div class="d-flex flex-column mb-8 fv-row form-group">            
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">Cost Element</span>
                    <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Specify a Cost Element"></i>
                </label>
                <input type="text" class="form-control form-control-solid" placeholder="Enter Cost Element" name="cost_element" value="{{ $data->cost_element }}"/>
            </div>            
            <div class="d-flex flex-column mb-8 fv-row form-group">            
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">Cost Element Name</span>                    
                </label>
                <input type="text" class="form-control form-control-solid" placeholder="Enter Cost Element Name" name="cost_element_name" value="{{ $data->cost_element_name }}"/>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">            
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">Cost Element Group</span>                    
                </label>
                <input type="text" class="form-control form-control-solid" placeholder="Enter Cost Element Group" name="cost_element_group" value="{{ $data->cost_element_group }}"/>
            </div>  
            <div class="d-flex flex-column mb-8 fv-row form-group">            
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    <span class="required">Biaya Group</span>                    
                </label>
                <input type="text" class="form-control form-control-solid" placeholder="Enter Biaya Group" name="biaya_group" value="{{ $data->biaya_group }}"/>
            </div>
            <div class="d-flex flex-column mb-8 fv-row form-group">            
                <label class="d-flex align-items-center fs-6 fw-semibold mb-2">
                    Reference SAP
                </label>
                <input type="text" class="form-control form-control-solid" placeholder="Enter Ref SAP" name="sap" value="{{ $data->sap }}"/>
            </div>
        </div>
        <div class="modal-footer">
            <div class="text-center">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary">
                    <span class="indicator-label">Submit</span>
                    <span class="indicator-progress">Please wait... 
                    <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                </button>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    $("#idForm").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.
        var form = $(this);
        var actionUrl = form.attr('action');
            $.ajax({
                type: "patch",
                url: actionUrl,
                data: form.serialize(), 
                success: function(data)
                {
                    if(data.status == 'success'){
                        Swal.fire(
                            'Success',
                            data.message,
                            'success'
                        );
                        $('#modal').modal('hide');
                        $('#tb_costelement').DataTable().ajax.reload();
                    }else{
                        Swal.fire(
                            'Warning',
                            data.message,
                            'warning'
                        );
                    }
                }
            });
        });
</script>

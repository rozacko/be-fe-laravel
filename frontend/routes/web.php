<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CapexReportController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\DataOpcShipmentController;
use App\Http\Controllers\KPIManagementController;
use App\Http\Controllers\MaintenanceCostController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\MasterData\MPlantController;
use App\Http\Controllers\Synchronization\OpcController;
use App\Http\Controllers\MasterData\MCostcenterController;
use App\Http\Controllers\MasterData\MCostelementController;
use App\Http\Controllers\MasterData\MAreaController;
use App\Http\Controllers\MasterData\MKondisiController;
use App\Http\Controllers\MasterData\MWorkgroupController;
use App\Http\Controllers\MasterData\MPlantAreaController;
use App\Http\Controllers\MasterData\MEqInspectionController;
use App\Http\Controllers\MasterData\MProjectCapexController;
use App\Http\Controllers\MDataTPMController;
use App\Http\Controllers\Overhaul\OverhaulFinalReportController;
use App\Http\Controllers\Overhaul\OverhaulPreparationController;
use App\Http\Controllers\Overhaul\OverhaulProgressController;
use App\Http\Controllers\PlantReabilityController;
use App\Http\Controllers\OverhoulReportController;
use App\Http\Controllers\PlantInspectionController;
use App\Http\Controllers\PlantOverviewController;
use App\Http\Controllers\PlantPerformanceController;
use App\Http\Controllers\TPMReportController;
use App\Http\Controllers\PowerBIController;
use App\Http\Controllers\ProductionAssetManagementController;
use App\Http\Controllers\TPMReportLaporanGugusController;
use App\Http\Controllers\ProductionReportController;
use App\Http\Controllers\ProductionReportLaporanHarianController;
use App\Http\Controllers\ProductionReportValidasiHarprosController;
use App\Http\Controllers\TPMReportLaporanKPIMemberTPMController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\SparepartPlanningController;
use App\Http\Controllers\Synchronization\ShipmentController;
use App\Http\Controllers\she_safety\DashboardSafetyController;
use App\Http\Controllers\she_safety\ManPowerController;
use App\Http\Controllers\she_safety\ManHoursController;
use App\Http\Controllers\she_safety\AccidentReportController;
use App\Http\Controllers\she_safety\FireAccidentReportController;
use App\Http\Controllers\she_safety\UnsafeActionController;
use App\Http\Controllers\she_safety\UnsafeConditionController;
use App\Http\Controllers\she_safety\AparController;
use App\Http\Controllers\she_safety\FireAlarmController;
use App\Http\Controllers\she_safety\HydrantController;
use App\Http\Controllers\she_safety\SafetyTrainingController;

use App\Http\Controllers\she_safety\MasterJenisLimbahController;
use App\Http\Controllers\she_safety\DokumenLingkunganController;
use App\Http\Controllers\she_safety\MasterInspeskiLingkunganController;
use App\Http\Controllers\she_safety\NeracaLimbahB3Controller;

use App\Http\Controllers\she_proper\KebijakanLingkunganController;
use App\Http\Controllers\she_proper\StrukturOrganController;
use App\Http\Controllers\she_proper\ProgramLingkunganController;
use App\Http\Controllers\she_proper\PerizinanLingkunganController;
use App\Http\Controllers\she_proper\KpiProperController;
use App\Http\Controllers\she_proper\MatrikLingController;
use App\Http\Controllers\she_proper\SertifAnalisController;
use App\Http\Controllers\she_proper\SertifLingController;
use App\Http\Controllers\she_proper\KomPersonilController;
use App\Http\Controllers\she_proper\HasilPengukuranController;



use App\Http\Controllers\Vendor\UploadScheduleController;
use App\Http\Controllers\Vendor\RealisasiFingerprintController;
use App\Http\Controllers\Vendor\PlanningRealisasiFingerprintController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [DashboardController::class, 'index'])->middleware(['feauth']);

Route::get('/dashboard', [DashboardController::class, 'index'])
    ->middleware(['feauth'])
    ->name('dashboard');
// ============================ Dashboard Power BI ==============================
Route::group(['as' => 'powerbi.', 'prefix' => 'powerbi', 'middleware' => 'feauth'], function () {
    Route::get('/', ['as' => 'index', 'uses' => 'PowerBIController@index']);
});

// ============================ Koreksi KPI ==============================
Route::group(['as' => 'koreksi-kpi.', 'prefix' => 'koreksi-kpi', 'middleware' => 'feauth'], function () {
    Route::get('/', ['as' => 'index', 'uses' => 'KoreksiKPIController@index']);
    Route::get('/temp', ['as' => 'temp', 'uses' => 'KoreksiKPIController@temp']);
    Route::get('/create', ['as' => 'create', 'uses' => 'KoreksiKPIController@create']);
    Route::post('/store', ['as' => 'store', 'uses' => 'KoreksiKPIController@store']);
    Route::get('/datatable', ['as' => 'datatable', 'uses' => 'KoreksiKPIController@datatable']);
    Route::post('/download', ['as' => 'download', 'uses' => 'KoreksiKPIController@download']);
});

// ============================ Quality Management ==============================
Route::group(['as' => 'quality-management.', 'prefix' => 'quality-management', 'middleware' => 'feauth'], function () {
    Route::get('/', ['as' => 'index', 'uses' => 'QualityManagementController@index']);
    Route::get('/temp-rm', ['as' => 'tempRM', 'uses' => 'QualityManagementController@tempRM']);
    Route::get('/temp-kf', ['as' => 'tempKF', 'uses' => 'QualityManagementController@tempKF']);
    Route::get('/temp-ck', ['as' => 'tempCK', 'uses' => 'QualityManagementController@tempCK']);
    Route::get('/temp-se', ['as' => 'tempSE', 'uses' => 'QualityManagementController@tempSE']);
    Route::get('/temp-cm', ['as' => 'tempCM', 'uses' => 'QualityManagementController@tempCM']);
    Route::get('/temp-hm', ['as' => 'tempHM', 'uses' => 'QualityManagementController@tempHM']);
    Route::get('/edit-rm/{uuid}', ['as' => 'editRM','uses' => 'QualityManagementController@editRM']);
    Route::get('/edit-kf/{uuid}', ['as' => 'editKF','uses' => 'QualityManagementController@editKF']);
    Route::get('/edit-ck/{uuid}', ['as' => 'editCK','uses' => 'QualityManagementController@editCK']);
    Route::get('/edit-se/{uuid}', ['as' => 'editSE','uses' => 'QualityManagementController@editSE']);
    Route::get('/edit-cm/{uuid}', ['as' => 'editCM','uses' => 'QualityManagementController@editCM']);
    Route::get('/edit-hm/{uuid}', ['as' => 'editHM','uses' => 'QualityManagementController@editHM']);

    // ==== Modal ====
    Route::get('/modal-import-rm', ['as' => 'importRM', 'uses' => 'QualityManagementController@importRM']);
    Route::get('/modal-import-kf', ['as' => 'importKF', 'uses' => 'QualityManagementController@importKF']);
    Route::get('/modal-import-ck', ['as' => 'importCK', 'uses' => 'QualityManagementController@importCK']);
    Route::get('/modal-import-se', ['as' => 'importSE', 'uses' => 'QualityManagementController@importSE']);
    Route::get('/modal-import-cm', ['as' => 'importCM', 'uses' => 'QualityManagementController@importCM']);
    Route::get('/modal-import-hm', ['as' => 'importHM', 'uses' => 'QualityManagementController@importHM']);

    // ==== API ====
    Route::post('/store-rm', ['as' => 'storeRM', 'uses' => 'QualityManagementController@storeRM']);
    Route::post('/store-kf', ['as' => 'storeKF', 'uses' => 'QualityManagementController@storeKF']);
    Route::post('/store-ck', ['as' => 'storeCK', 'uses' => 'QualityManagementController@storeCK']);
    Route::post('/store-se', ['as' => 'storeSE', 'uses' => 'QualityManagementController@storeSE']);
    Route::post('/store-cm', ['as' => 'storeCM', 'uses' => 'QualityManagementController@storeCM']);
    Route::post('/store-hm', ['as' => 'storeHM', 'uses' => 'QualityManagementController@storeHM']);
    Route::get('/datatable-rm', ['as' => 'datatableRM', 'uses' => 'QualityManagementController@datatableRM']);
    Route::get('/datatable-kf', ['as' => 'datatableKF', 'uses' => 'QualityManagementController@datatableKF']);
    Route::get('/datatable-ck', ['as' => 'datatableCK', 'uses' => 'QualityManagementController@datatableCK']);
    Route::get('/datatable-se', ['as' => 'datatableSE', 'uses' => 'QualityManagementController@datatableSE']);
    Route::get('/datatable-cm', ['as' => 'datatableCM', 'uses' => 'QualityManagementController@datatableCM']);
    Route::get('/datatable-hm', ['as' => 'datatableHM', 'uses' => 'QualityManagementController@datatableHM']);
    Route::put('/edit-rm/{uuid}', ['as' => 'updateRM', 'uses' => 'QualityManagementController@updateRM']);
    Route::put('/edit-kf/{uuid}', ['as' => 'updateKF', 'uses' => 'QualityManagementController@updateKF']);
    Route::put('/edit-ck/{uuid}', ['as' => 'updateCK', 'uses' => 'QualityManagementController@updateCK']);
    Route::put('/edit-se/{uuid}', ['as' => 'updateSE', 'uses' => 'QualityManagementController@updateSE']);
    Route::put('/edit-cm/{uuid}', ['as' => 'updateCM', 'uses' => 'QualityManagementController@updateCM']);
    Route::put('/edit-hm/{uuid}', ['as' => 'updateHM', 'uses' => 'QualityManagementController@updateHM']);
    Route::delete('/delete-rm/{uuid}', ['as' => 'deleteRM', 'uses' => 'QualityManagementController@deleteRM']);
    Route::delete('/delete-kf/{uuid}', ['as' => 'deleteKF', 'uses' => 'QualityManagementController@deleteKF']);
    Route::delete('/delete-ck/{uuid}', ['as' => 'deleteCK', 'uses' => 'QualityManagementController@deleteCK']);
    Route::delete('/delete-se/{uuid}', ['as' => 'deleteSE', 'uses' => 'QualityManagementController@deleteSE']);
    Route::delete('/delete-cm/{uuid}', ['as' => 'deleteCM', 'uses' => 'QualityManagementController@deleteCM']);
    Route::delete('/delete-hm/{uuid}', ['as' => 'deleteHM', 'uses' => 'QualityManagementController@deleteHM']);
});

// ============================ Plant Reliability ==============================
Route::group(['as' => 'plant-reliability.', 'prefix' => 'plant-reliability', 'middleware' => 'feauth'], function () {
    Route::get('/', ['as' => 'index', 'uses' => 'PlantReabilityController@index']);

    // ==== API ====
    Route::get('/summary', ['as' => 'summary', 'uses' => 'PlantReabilityController@summary']);
    Route::get('/pie', ['as' => 'pie', 'uses' => 'PlantReabilityController@pie']);
});

// ============================ Plant Inspection ==============================
Route::group(['as' => 'inspection.', 'prefix' => 'inspection', 'middleware' => 'feauth'], function () {
    Route::get('/', ['as' => 'index', 'uses' => 'PlantInspectionController@index']);
    Route::get('/tambah', ['as' => 'add', 'uses' => 'PlantInspectionController@add']);
    Route::get('/approve/{id}', ['as' => 'approve', 'uses' => 'PlantInspectionController@approve']);
    Route::get('/detail/{id}', ['as' => 'detail', 'uses' => 'PlantInspectionController@detail']);
    Route::get('/edit/{id}', ['as' => 'edit', 'uses' => 'PlantInspectionController@edit']);

    // ==== Modal ====
    Route::get('/modal-add', ['as' => 'modalAdd', 'uses' => 'PlantInspectionController@modalAdd']);
    Route::get('/modal-import', ['as' => 'modalImport', 'uses' => 'PlantInspectionController@modalImport']);

    // ==== API ====
    Route::post('/import', ['as' => 'import', 'uses' => 'PlantInspectionController@import']);
    Route::get('/plant', ['as' => 'filterPlant', 'uses' => 'PlantInspectionController@filterPlant']);
    Route::get('/area', ['as' => 'filterArea', 'uses' => 'PlantInspectionController@filterArea']);
    Route::get('/fungsi', ['as' => 'filterFungsi', 'uses' => 'PlantInspectionController@filterFungsi']);
    Route::post('/store', ['as' => 'store', 'uses' => 'PlantInspectionController@store']);
    Route::get('/datatable', ['as' => 'datatable', 'uses' => 'PlantInspectionController@datatable']);
    Route::get('/datatable-approve', ['as' => 'datatableApprove', 'uses' => 'PlantInspectionController@datatableApprove']);
    Route::post('/detail-data', ['as' => 'detailData', 'uses' => 'PlantInspectionController@detailData']);
    Route::post('/store-approve', ['as' => 'storeApprove', 'uses' => 'PlantInspectionController@storeApprove']);
});
// ============================ Data OPC Shipment ==============================
Route::get('/data-opc-shipment', [DataOpcShipmentController::class, 'index'])
    ->middleware(['feauth'])
    ->name('data-opc-shipment');
// ============================ KPI Management ==============================
Route::get('/kpi-management', [KPIManagementController::class, 'index'])
    ->middleware(['feauth'])
    ->name('kpi-management');
// ============================ Maintenance ==============================
Route::get('/maintenance-cost', [MaintenanceCostController::class, 'index'])
    ->middleware(['feauth'])
    ->name('maintenance-cost');
Route::get('/production-asset-management', [ProductionAssetManagementController::class, 'index'])
    ->middleware(['feauth'])
    ->name('production-asset-management');
Route::get('/sparepart-planning', [SparepartPlanningController::class, 'index'])
    ->middleware(['feauth'])
    ->name('sparepart-planning');

Route::get('/capex-report-summary', [
    CapexReportController::class,
    'indexCapexSummary',
])
    ->middleware(['feauth'])
    ->name('capex-report-summary');

Route::get('/capex-report', [CapexReportController::class, 'indexCapex'])
    ->middleware(['feauth'])
    ->name('capex-report');

Route::get('/project', [ProjectController::class, 'index'])
    ->middleware(['feauth'])
    ->name('project');
Route::get('maintenance/plant-performance', [
    PlantPerformanceController::class,
    'index',
])
    ->middleware(['feauth'])
    ->name('plant-performance');
    
// RKAP PERFROMANACE
Route::group([ 'prefix' => 'rkap-prognose', 'middleware' => 'feauth'], function () {
    Route::get('/plant-performance','Rkap\PlantPerformanceController@index')->name('pages.rkap.plant-performance');
    Route::get('/plant-performance/table', 'Rkap\PlantPerformanceController@table')->name('pages.rkap.plant-performance.table');
    Route::get('/plant-performance/download-template', 'Rkap\PlantPerformanceController@downloadTemplate')->name('pages.rkap.plant-performance.download');
    Route::get('/plant-performance/download', 'Rkap\PlantPerformanceController@download')->name('pages.rkap.plant-performance.downloadexport');
    Route::post('/plant-performance/import', 'Rkap\PlantPerformanceController@import')->name('pages.rkap.plant-performance.import');
    Route::get('/plant-performance/show/{uuid}', 'Rkap\PlantPerformanceController@show')->name('pages.rkap.plant-performance.show');
    Route::put('/plant-performance/update/{uuid}', 'Rkap\PlantPerformanceController@update')->name('pages.rkap.plant-performance.update');
    
    Route::get('/maintenance-performance','Rkap\MaintenancePerformanceController@index')->name('pages.rkap.maintenance-performance.index');
    Route::get('/maintenance-performance/download-template', 'Rkap\MaintenancePerformanceController@downloadTemplate')->name('pages.rkap.maintenance-performance.download');
    Route::get('/maintenance-performance/table','Rkap\MaintenancePerformanceController@table')->name('pages.rkap.maintenance-performance.table');
    Route::post('/maintenance-performance/import','Rkap\MaintenancePerformanceController@import')->name('pages.rkap.maintenance-performance.import');
    Route::get('/maintenance-performance/show/{uuid}','Rkap\MaintenancePerformanceController@show')->name('pages.rkap.maintenance-performance.show');
    Route::put('/maintenance-performance/update/{uuid}','Rkap\MaintenancePerformanceController@update')->name('pages.rkap.maintenance-performance.update');
    
    Route::get('/prognose-performance','Rkap\PrognosePerformanceController@index')->name('pages.rkap.prognose-performance.index');
    Route::get('/prognose-performance/download-template', 'Rkap\PrognosePerformanceController@downloadTemplate')->name('pages.rkap.plant-performance.download');
    Route::get('/prognose-performance/table','Rkap\PrognosePerformanceController@table')->name('pages.rkap.prognose-performance.table');
    Route::post('/prognose-performance/import','Rkap\PrognosePerformanceController@import')->name('pages.rkap.prognose-performance.import');
    Route::get('/prognose-performance/show/{uuid}','Rkap\PrognosePerformanceController@show')->name('pages.rkap.prognose-performance.show');
    Route::put('/prognose-performance/update/{uuid}','Rkap\PrognosePerformanceController@update')->name('pages.rkap.prognose-performance.update');
    
    Route::get('/prognose-maintenance','Rkap\PrognoseMaintenanceController@index')->name('pages.rkap.prognose-maintenance.index');
    Route::get('/prognose-maintenance/download-template', 'Rkap\PrognoseMaintenanceController@downloadTemplate')->name('pages.rkap.plant-performance.download');
    Route::get('/prognose-maintenance/table','Rkap\PrognoseMaintenanceController@table')->name('pages.rkap.prognose-maintenance.table');
    Route::post('/prognose-maintenance/import','Rkap\PrognoseMaintenanceController@import')->name('pages.rkap.prognose-maintenance.import');
    Route::get('/prognose-maintenance/show/{uuid}','Rkap\PrognoseMaintenanceController@show')->name('pages.rkap.prognose-maintenance.show');
    Route::put('/prognose-maintenance/update/{uuid}','Rkap\PrognoseMaintenanceController@update')->name('pages.rkap.prognose-maintenance.update');

    // RKAP CAPEX
Route::group([ 'prefix' => 'rkap-capex', 'middleware' => 'feauth'], function () {
    Route::get('/','Rkap\CapexController@index')->name('pages.rkap.capex');
    Route::get('/table', 'Rkap\CapexController@table')->name('pages.rkap.capex.table');
    Route::get('/project', 'Rkap\CapexController@getProject')->name('pages.rkap.capex.project');
    Route::get('/download-template', 'Rkap\CapexController@downloadTemplate');
    Route::post('/import', 'Rkap\CapexController@import')->name('pages.rkap.capex.import');
    Route::get('/show/{uuid}', 'Rkap\CapexController@show')->name('pages.rkap.capex.show');
    Route::put('/update/{uuid}', 'Rkap\CapexController@update')->name('pages.rkap.capex.update');
    Route::delete('/{uuid}', 'Rkap\CapexController@destroy')->name('pages.rkap.capex.destroy');
});

Route::group([ 'prefix' => 'prognose-capex', 'middleware' => 'feauth'], function () {
    Route::get('/','Rkap\PrognoseCapexController@index')->name('pages.rkap.prognose-capex');
    Route::get('/table', 'Rkap\PrognoseCapexController@table')->name('pages.rkap.prognose-capex.table');
    Route::get('/project', 'Rkap\PrognoseCapexController@getProject')->name('pages.rkap.prognose-capex.project');
    Route::get('/download-template', 'Rkap\PrognoseCapexController@downloadTemplate')->name('pages.rkap.prognose-capex.download');
    Route::post('/import', 'Rkap\PrognoseCapexController@import')->name('pages.rkap.prognose-capex.import');
    Route::get('/show/{uuid}', 'Rkap\PrognoseCapexController@show')->name('pages.rkap.prognose-capex.show');
    Route::put('/update/{uuid}', 'Rkap\PrognoseCapexController@update')->name('pages.rkap.prognose-capex.update');
    Route::delete('/{uuid}', 'Rkap\PrognoseCapexController@destroy')->name('pages.rkap.prognose-capex.destroy');
});

});



// ============================ TPM Report ==============================
Route::group(['as' => 'tpm-report.', 'prefix' => 'tpm-report', 'middleware' => 'feauth'], function () {
    Route::get('/', ['as' => 'index', 'uses' => 'TPMReportController@index']);
    Route::get('/scoreimplementation', ['as' => 'scoreimplementation', 'uses' => 'TPMReportController@scoreimplementation']);
    Route::get('/focusedimprovement', ['as' => 'focusedimprovement', 'uses' => 'TPMReportController@focusedimprovement']);
    Route::get('/focusarea', ['as' => 'focusarea', 'uses' => 'TPMReportController@focusarea']);
    Route::get('/genbasga', ['as' => 'genbasga', 'uses' => 'TPMReportController@genbasga']);
    Route::get('/partisipasimember', ['as' => 'partisipasimember', 'uses' => 'TPMReportController@partisipasiMember']);
});

Route::group(['as' => 'tpm-report-laporan-gugus.', 'prefix' => 'tpm-report-laporan-gugus', 'middleware' => 'feauth'], function () {
    Route::get('/', ['as' => 'index', 'uses' => 'TPMReportLaporanGugusController@index']);
    Route::get('/table', ['as' => 'table', 'uses' => 'TPMReportLaporanGugusController@table']);
    Route::get('/FilterFasilitator', ['as' => 'FilterFasilitator', 'uses' => 'TPMReportLaporanGugusController@FilterFasilitator']);
    Route::get('/FilterGugus', ['as' => 'FilterGugus', 'uses' => 'TPMReportLaporanGugusController@FilterGugus']);
    Route::post('/downloadExcel', ['as' => 'downloadExcel', 'uses' => 'TPMReportLaporanGugusController@DownloadExcel']);
});

Route::group(['as' => 'tpm-report-laporan-kpi-member-tpm.', 'prefix' => 'tpm-report-laporan-kpi-member-tpm', 'middleware' => 'feauth'], function () {
    Route::get('/', ['as' => 'index', 'uses' => 'TPMReportLaporanKPIMemberTPMController@index']);
    Route::get('/table', ['as' => 'table', 'uses' => 'TPMReportLaporanKPIMemberTPMController@table']);
    Route::get('/FilterOrganisasi', ['as' => 'FilterOrganisasi', 'uses' => 'TPMReportLaporanKPIMemberTPMController@FilterOrganisasi']);
    Route::get('/FilterUnitKerja', ['as' => 'FilterUnitKerja', 'uses' => 'TPMReportLaporanKPIMemberTPMController@FilterUnitKerja']);
});

Route::group(['as' => 'mkpitpm.', 'prefix' => 'mkpitpm', 'middleware' => 'feauth'], function () {
    Route::get('/', ['as' => 'index', 'uses' => 'MDataKPIMemberTPMController@index']);
    Route::get('/create', ['as' => 'create', 'uses' => 'MDataKPIMemberTPMController@create']);
    Route::get('/table', ['as' => 'table', 'uses' => 'MDataKPIMemberTPMController@table']);
    Route::post('/store', ['as' => 'store', 'uses' => 'MDataKPIMemberTPMController@store']);
    Route::get('/download-temp', ['as' => 'download', 'uses' => 'MDataKPIMemberTPMController@DownloadTemplate']);
    Route::get('/FilterOrganisasi', ['as' => 'FilterOrganisasi', 'uses' => 'MDataKPIMemberTPMController@FilterOrganisasi']);
    Route::get('/FilterUnitKerja', ['as' => 'FilterUnitKerja', 'uses' => 'MDataKPIMemberTPMController@FilterUnitKerja']);
});

Route::group(['as' => 'mgugustpm.', 'prefix' => 'mgugustpm', 'middleware' => 'feauth'], function () {
    Route::get('/', ['as' => 'index', 'uses' => 'MDataGugusTPMController@index']);
    Route::get('/create', ['as' => 'create', 'uses' => 'MDataGugusTPMController@create']);
    Route::get('/table', ['as' => 'table', 'uses' => 'MDataGugusTPMController@table']);
    Route::post('/store', ['as' => 'store', 'uses' => 'MDataGugusTPMController@store']);
    Route::get('/download-temp', ['as' => 'download', 'uses' => 'MDataGugusTPMController@DownloadTemplate']);
    Route::get('/FilterFasilitator', ['as' => 'FilterFasilitator', 'uses' => 'MDataGugusTPMController@FilterFasilitator']);
    Route::get('/FilterGugus', ['as' => 'FilterGugus', 'uses' => 'MDataGugusTPMController@FilterGugus']);
});

Route::group(['as' => 'mtpm.', 'prefix' => 'mtpm', 'middleware' => 'feauth'], function () {
    Route::get('/', ['as' => 'index', 'uses' => 'MDataTPMController@index']);
    Route::get('/table', ['as' => 'table', 'uses' => 'MDataTPMController@table']);
    Route::get('/create', ['as' => 'create', 'uses' => 'MDataTPMController@create']);
    Route::post('/store', ['as' => 'store', 'uses' => 'MDataTPMController@store']);
    Route::get('/download-temp', ['as' => 'download', 'uses' => 'MDataTPMController@DownloadTemplate']);
});


Route::prefix('/vendor-management')->group(__DIR__.DIRECTORY_SEPARATOR.'web'.DIRECTORY_SEPARATOR.'VendorRoutes.php');

// ============================ Production Report ==============================
Route::get('/production-report', [ProductionReportController::class, 'index'])
    ->middleware(['feauth'])
    ->name('production-report');

Route::get('/production-report-laporan-harian', [
    ProductionReportLaporanHarianController::class,
    'index',
])
    ->middleware(['feauth'])
    ->name('production-report-laporan-harian');

Route::get('/production-report-validasi-harpros', [
    ProductionReportValidasiHarprosController::class,
    'index',
])
    ->middleware(['feauth'])
    ->name('production-report-validasi-harpros');

// ============================ Plant Overview ==============================
Route::get('/plantoverview-solarplant', [PlantOverviewController::class, 'index'])
    ->middleware(['feauth'])
    ->name('plantoverview-solarplant');

// ============================ Overhoul Report ==============================
Route::get('/overhoul-report', [OverhoulReportController::class, 'index'])
    ->middleware(['feauth'])
    ->name('overhoul-report');

// ============================ RKAP Inventory ==============================
Route::group(['as' => 'rkap-inventory.', 'prefix' => 'rkap-inventory', 'middleware' => 'feauth'], function () {
    Route::get('/', ['as' => 'index', 'uses' => 'RKAPInventoryController@index']);
    Route::get('/create', ['as' => 'create', 'uses' => 'RKAPInventoryController@create']);
    Route::post('/store', ['as' => 'store', 'uses' => 'RKAPInventoryController@store']);
    Route::get('/table', ['as' => 'table', 'uses' => 'RKAPInventoryController@table']);
    Route::get('/edit/{uuid}', ['as' => 'edit','uses' => 'RKAPInventoryController@edit']);
    Route::put('/edit/{uuid}', ['as' => 'edit','uses' => 'RKAPInventoryController@update']);
    Route::get('/download-temp', ['as' => 'download', 'uses' => 'RKAPInventoryController@DownloadTemplate']);
    Route::delete('/delete/{uuid}', ['as' => 'delete', 'uses' => 'RKAPInventoryController@destroy']);
});

// ============================ Prognose Inventory ==============================
Route::group(['as' => 'prognose-inventory.', 'prefix' => 'prognose-inventory', 'middleware' => 'feauth'], function () {
    Route::get('/', ['as' => 'index', 'uses' => 'PrognoseInventoryController@index']);
    Route::get('/create', ['as' => 'create', 'uses' => 'PrognoseInventoryController@create']);
    Route::post('/store', ['as' => 'store', 'uses' => 'PrognoseInventoryController@store']);
    Route::get('/table', ['as' => 'table', 'uses' => 'PrognoseInventoryController@table']);
    Route::get('/edit/{uuid}', ['as' => 'edit','uses' => 'PrognoseInventoryController@edit']);
    Route::put('/edit/{uuid}', ['as' => 'edit','uses' => 'PrognoseInventoryController@update']);
    Route::get('/download-temp', ['as' => 'download', 'uses' => 'PrognoseInventoryController@DownloadTemplate']);
    Route::delete('/delete/{uuid}', ['as' => 'delete', 'uses' => 'PrognoseInventoryController@destroy']);
});

// ============================ Koreksi Nilai Inventory ==============================
Route::group(['as' => 'koreksi-nilai-inventory.', 'prefix' => 'koreksi-nilai-inventory', 'middleware' => 'feauth'], function () {
    Route::get('/', ['as' => 'index', 'uses' => 'KoreksiNilaiInventoryController@index']);
    Route::get('/create', ['as' => 'create', 'uses' => 'KoreksiNilaiInventoryController@create']);
    Route::post('/store', ['as' => 'store', 'uses' => 'KoreksiNilaiInventoryController@store']);
    Route::get('/table', ['as' => 'table', 'uses' => 'KoreksiNilaiInventoryController@table']);
    Route::get('/edit/{uuid}', ['as' => 'edit','uses' => 'KoreksiNilaiInventoryController@edit']);
    Route::put('/edit/{uuid}', ['as' => 'edit','uses' => 'KoreksiNilaiInventoryController@update']);
    Route::get('/download-temp', ['as' => 'download', 'uses' => 'KoreksiNilaiInventoryController@DownloadTemplate']);
    Route::delete('/delete/{uuid}', ['as' => 'delete', 'uses' => 'KoreksiNilaiInventoryController@destroy']);
});

// ============================ Performance Order ==============================
Route::group(['as' => 'performance-order.', 'prefix' => 'performance-order', 'middleware' => 'feauth'], function () {
    Route::get('/', ['as' => 'index', 'uses' => 'PerformanceOrderController@index']);
    Route::get('/create', ['as' => 'create', 'uses' => 'PerformanceOrderController@create']);
    Route::post('/store', ['as' => 'store', 'uses' => 'PerformanceOrderController@store']);
    Route::get('/table', ['as' => 'table', 'uses' => 'PerformanceOrderController@table']);
    Route::get('/show/{uuid}', ['as' => 'show','uses' => 'PerformanceOrderController@show']);
    Route::get('/download-temp', ['as' => 'download', 'uses' => 'PerformanceOrderController@DownloadTemplate']);
    Route::delete('/delete/{uuid}', ['as' => 'delete', 'uses' => 'PerformanceOrderController@destroy']);
});

// ============================ Performance Notifikasi ==============================
Route::group(['as' => 'performance-notifikasi.', 'prefix' => 'performance-notifikasi', 'middleware' => 'feauth'], function () {
    Route::get('/', ['as' => 'index', 'uses' => 'PerformanceNotifikasiController@index']);
    Route::get('/create', ['as' => 'create', 'uses' => 'PerformanceNotifikasiController@create']);
    Route::post('/store', ['as' => 'store', 'uses' => 'PerformanceNotifikasiController@store']);
    Route::get('/table', ['as' => 'table', 'uses' => 'PerformanceNotifikasiController@table']);
    Route::get('/show/{uuid}', ['as' => 'show','uses' => 'PerformanceNotifikasiController@show']);
    Route::get('/download-temp', ['as' => 'download', 'uses' => 'PerformanceNotifikasiController@DownloadTemplate']);
    Route::delete('/delete/{uuid}', ['as' => 'delete', 'uses' => 'PerformanceNotifikasiController@destroy']);
});

Route::get('/error', function () {
    abort(500);
});

Route::prefix('overhaul')->group(function() {
    Route::prefix('preparation')->group(function(){
        Route::get('/', [OverhaulPreparationController::class, 'index'])->name('overhaul.preparation');
        Route::post('/upload/part', [OverhaulPreparationController::class, ''])->name('overhaul.preparation.upload.part');
        Route::get('/partchart', [OverhaulPreparationController::class, 'getChartPart'])->name('overhaul.preparation.partchart');
        Route::get('/parttable', [OverhaulPreparationController::class, 'getTablePart'])->name('overhaul.preparation.parttable');
        Route::get('/timeline', [OverhaulPreparationController::class, 'getTimeline'])->name('overhaul.preparation.timeline');
        Route::get('/import', [OverhaulPreparationController::class, 'getTemplate'])->name('overhaul.preparation.import');
        Route::post('/import', [OverhaulPreparationController::class, 'uploadData']);
        Route::get('/budget', [OverhaulPreparationController::class, 'getTemplateBudget'])->name('overhaul.preparation.budget');
        Route::post('/budget', [OverhaulPreparationController::class, 'uploadDataBudget']);
        Route::get('/budgetuk',[OverhaulPreparationController::class, 'budgetUk'])->name('overhaul.preparation.budgetuk');
        Route::get('/partchartbudget',[OverhaulPreparationController::class, 'partDonutChart'])->name('overhaul.preparation.partchartbudget');
        Route::get('/opexdonut',[OverhaulPreparationController::class, 'opexDonut'])->name('overhaul.preparation.opexdonut');
        Route::get('/tablecapex',[OverhaulPreparationController::class, 'tableCapex'])->name('overhaul.preparation.tablecapex');
        Route::get('/tableissue',[OverhaulPreparationController::class, 'tableIssue'])->name('overhaul.preparation.tableissue');
        
    });

    Route::prefix('progress')->group(function(){
        Route::get('/', [OverhaulProgressController::class, 'index'])->name('overhaul.progress');
        Route::get('/periode/{id_plant}',[OverhaulProgressController::class, 'getPeriode']);
        Route::post('/periode', [OverhaulProgressController::class, 'addPeriode'])->name('overhaul.periode');
        Route::get('/import/progress/{plant}', [OverhaulProgressController::class, 'templateProgress']);
        Route::get('/import/activity', [OverhaulProgressController::class, 'templateActivity']);
        Route::post('/import/progress', [OverhaulProgressController::class, 'uploadProgress'])->name('overhaul.progress.import.progress');
        Route::post('/import/activity', [OverhaulProgressController::class, 'uploadActivity'])->name('overhaul.progress.import.activity');
        Route::post('/progress',[OverhaulProgressController::class, 'addProgress'])->name('overhaul.progress.progress');
        Route::post('/activity',[OverhaulProgressController::class, 'addActivity'])->name('overhaul.progress.activity');
        Route::get('/major',[OverhaulProgressController::class, 'progressMajor']);
        Route::get('/minor',[OverhaulProgressController::class, 'progressMinor']);
        Route::get('/activity/{plant}',[OverhaulProgressController::class, 'getActivity']);
        Route::get('/dokumentasi/{plant}', [OverhaulProgressController::class, 'getDokumentasi']);
        Route::post('/dokumentasi', [OverhaulProgressController::class, 'addDokumentasi'])->name('overhaul.progress.dokumentasi');
        Route::get('/chart/{plant}', [OverhaulProgressController::class, 'getChart'])->name('overhaul.progress.chart');
        Route::get('/unsafe',[OverhaulProgressController::class, 'unsafe'])->name('overhaul.progress.unsafe');
        Route::get('/opex',[OverhaulProgressController::class, 'opex'])->name('overhaul.progress.opex');
    });

    Route::prefix('finalreport')->group(function(){
        Route::get('/', [OverhaulFinalReportController::class, 'index'])->name('overhaul.finalreport');
        Route::get('/table', [OverhaulFinalReportController::class, 'table'])->name('overhaul.finalreport.table');
        Route::post('/add', [OverhaulFinalReportController::class, 'add'])->name('overhaul.finalreport.add');
        Route::delete('/delete/{id}', [OverhaulFinalReportController::class, 'destroy'])->name('overhaul.finalreport.delete');
    });

})->middleware(['feauth']);

Route::prefix('opc')->group(function(){
    Route::get('/', [OpcController::class, 'index'])->name('opc');
    Route::get('/datatable', [OpcController::class, 'datatable'])->name('opc.datatable');
    Route::post('/syncdata', [OpcController::class, 'syncData'])->name('opc.syncdata');
})->middleware(['feauth']);

Route::prefix('shipment')
    ->group(function () {
        Route::get('/', [ShipmentController::class, 'index'])->name('shipment');
        Route::get('/datatable', [
            ShipmentController::class,
            'datatable',
        ])->name('shipment.datatable');
        Route::post('/syncdata', [ShipmentController::class, 'syncData'])->name(
            'shipment.syncdata'
        );
    })
    ->middleware(['feauth']);

Route::post('client/login', [AuthController::class, 'store']);

Route::group(
    [
        'as' => 'masterdata.',
        'prefix' => 'masterdata',
        'namespace' => 'masterdata',
    ],
    function () {
        #Master Plant
        Route::group(['as' => 'mplant.', 'prefix' => 'mplant'], function () {
            Route::get('/', [
                'as' => 'index',
                'uses' => 'MPlantController@index',
            ]);
            Route::get('/table', [
                'as' => 'table',
                'uses' => 'MPlantController@table',
            ]);
            Route::get('/create', [
                'as' => 'create',
                'uses' => 'MPlantController@create',
            ]);
            Route::post('/store', [
                'as' => 'store',
                'uses' => 'MPlantController@store',
            ]);
            Route::get('edit/{id}', [
                'as' => 'edit',
                'uses' => 'MPlantController@edit',
            ]);
            Route::patch('/update', [
                'as' => 'update',
                'uses' => 'MPlantController@update',
            ]);
            Route::delete('/{id}/delete', [
                'as' => 'destroy',
                'uses' => 'MPlantController@destroy',
            ]);
        });
        Route::resource('mplant', 'MPlantController')->except([
            'index',
            'store',
            'create',
            'edit',
        ]);

        #Master Plant
        Route::group(
            ['as' => 'mplantarea.', 'prefix' => 'mplantarea'],
            function () {
                Route::get('/', [
                    'as' => 'index',
                    'uses' => 'MPlantAreaController@index',
                ]);
                Route::get('/table', [
                    'as' => 'table',
                    'uses' => 'MPlantAreaController@table',
                ]);
                Route::get('/create', [
                    'as' => 'create',
                    'uses' => 'MPlantAreaController@create',
                ]);
                Route::post('/store', [
                    'as' => 'store',
                    'uses' => 'MPlantAreaController@store',
                ]);
                Route::get('edit/{id}', [
                    'as' => 'edit',
                    'uses' => 'MPlantAreaController@edit',
                ]);
                Route::patch('/update', [
                    'as' => 'update',
                    'uses' => 'MPlantAreaController@update',
                ]);
                Route::delete('/{id}/delete', [
                    'as' => 'destroy',
                    'uses' => 'MPlantAreaController@destroy',
                ]);
            }
        );
        Route::resource('mplantarea', 'MPlantAreaController')->except([
            'index',
            'store',
            'create',
            'edit',
        ]);

        #Master CostCenter
        Route::group(
            ['as' => 'mcostcenter.', 'prefix' => 'mcostcenter'],
            function () {
                Route::get('/', [
                    'as' => 'index',
                    'uses' => 'MCostcenterController@index',
                ]);
                Route::get('/table', [
                    'as' => 'table',
                    'uses' => 'MCostcenterController@table',
                ]);
                Route::get('/create', [
                    'as' => 'create',
                    'uses' => 'MCostcenterController@create',
                ]);
                Route::post('/store', [
                    'as' => 'store',
                    'uses' => 'MCostcenterController@store',
                ]);
                Route::get('edit/{id}', [
                    'as' => 'edit',
                    'uses' => 'MCostcenterController@edit',
                ]);
                Route::patch('/update', [
                    'as' => 'update',
                    'uses' => 'MCostcenterController@update',
                ]);
                Route::delete('/{id}/delete', [
                    'as' => 'destroy',
                    'uses' => 'MCostcenterController@destroy',
                ]);
            }
        );
        Route::resource('mcostcenter', 'MCostcenterController')->except([
            'index',
            'store',
            'create',
            'edit',
        ]);

        #Master CostElement
        Route::group(
            ['as' => 'mcostelement.', 'prefix' => 'mcostelement'],
            function () {
                Route::get('/', [
                    'as' => 'index',
                    'uses' => 'MCostelementController@index',
                ]);
                Route::get('/table', [
                    'as' => 'table',
                    'uses' => 'MCostelementController@table',
                ]);
                Route::get('/create', [
                    'as' => 'create',
                    'uses' => 'MCostelementController@create',
                ]);
                Route::post('/store', [
                    'as' => 'store',
                    'uses' => 'MCostelementController@store',
                ]);
                Route::get('edit/{id}', [
                    'as' => 'edit',
                    'uses' => 'MCostelementController@edit',
                ]);
                Route::patch('/update', [
                    'as' => 'update',
                    'uses' => 'MCostelementController@update',
                ]);
                Route::delete('/{id}/delete', [
                    'as' => 'destroy',
                    'uses' => 'MCostelementController@destroy',
                ]);
            }
        );
        Route::resource('mcostelement', 'MCostelementController')->except([
            'index',
            'store',
            'create',
            'edit',
        ]);

        #Master Area
        Route::group(['as' => 'marea.', 'prefix' => 'marea'], function () {
            Route::get('/', [
                'as' => 'index',
                'uses' => 'MAreaController@index',
            ]);
            Route::get('/table', [
                'as' => 'table',
                'uses' => 'MAreaController@table',
            ]);
            Route::get('/create', [
                'as' => 'create',
                'uses' => 'MAreaController@create',
            ]);
            Route::post('/store', [
                'as' => 'store',
                'uses' => 'MAreaController@store',
            ]);
            Route::get('edit/{id}', [
                'as' => 'edit',
                'uses' => 'MAreaController@edit',
            ]);
            Route::patch('/update', [
                'as' => 'update',
                'uses' => 'MAreaController@update',
            ]);
            Route::delete('/{id}/delete', [
                'as' => 'destroy',
                'uses' => 'MAreaController@destroy',
            ]);
        });
        Route::resource('marea', 'MAreaController')->except([
            'index',
            'store',
            'create',
            'edit',
        ]);

        #Master Workgroup
        Route::group(
            ['as' => 'mworkgroup.', 'prefix' => 'mworkgroup'],
            function () {
                Route::get('/', [
                    'as' => 'index',
                    'uses' => 'MWorkgroupController@index',
                ]);
                Route::get('/table', [
                    'as' => 'table',
                    'uses' => 'MWorkgroupController@table',
                ]);
                Route::get('/create', [
                    'as' => 'create',
                    'uses' => 'MWorkgroupController@create',
                ]);
                Route::post('/store', [
                    'as' => 'store',
                    'uses' => 'MWorkgroupController@store',
                ]);
                Route::get('edit/{id}', [
                    'as' => 'edit',
                    'uses' => 'MWorkgroupController@edit',
                ]);
                Route::patch('/update', [
                    'as' => 'update',
                    'uses' => 'MWorkgroupController@update',
                ]);
                Route::delete('/{id}/delete', [
                    'as' => 'destroy',
                    'uses' => 'MWorkgroupController@destroy',
                ]);
            }
        );
        Route::resource('mworkgroup', 'MWorkgroupController')->except([
            'index',
            'store',
            'create',
            'edit',
        ]);

        #Master Project Capex
        Route::group(
            ['as' => 'mprojectcapex.', 'prefix' => 'mprojectcapex'],
            function () {
                Route::get('/', [
                    'as' => 'index',
                    'uses' => 'MProjectCapexController@index',
                ]);
                Route::get('/table', [
                    'as' => 'table',
                    'uses' => 'MProjectCapexController@table',
                ]);
                Route::get('/create', [
                    'as' => 'create',
                    'uses' => 'MProjectCapexController@create',
                ]);
                Route::post('/store', [
                    'as' => 'store',
                    'uses' => 'MProjectCapexController@store',
                ]);
                Route::get('edit/{id}', [
                    'as' => 'edit',
                    'uses' => 'MProjectCapexController@edit',
                ]);
                Route::patch('/update', [
                    'as' => 'update',
                    'uses' => 'MProjectCapexController@update',
                ]);
                Route::delete('/{id}/delete', [
                    'as' => 'destroy',
                    'uses' => 'MProjectCapexController@destroy',
                ]);
            }
        );
        Route::resource('mprojectcapex', 'MProjectCapexController')->except([
            'index',
            'store',
            'create',
            'edit',
        ]);

        #Master Kondisi
        Route::group(
            ['as' => 'mkondisi.', 'prefix' => 'mkondisi'],
            function () {
                Route::get('/', [
                    'as' => 'index',
                    'uses' => 'MKondisiController@index',
                ]);
                Route::get('/table', [
                    'as' => 'table',
                    'uses' => 'MKondisiController@table',
                ]);
                Route::get('/create', [
                    'as' => 'create',
                    'uses' => 'MKondisiController@create',
                ]);
                Route::post('/store', [
                    'as' => 'store',
                    'uses' => 'MKondisiController@store',
                ]);
                Route::get('edit/{id}', [
                    'as' => 'edit',
                    'uses' => 'MKondisiController@edit',
                ]);
                Route::patch('/update', [
                    'as' => 'update',
                    'uses' => 'MKondisiController@update',
                ]);
                Route::delete('/{id}/delete', [
                    'as' => 'destroy',
                    'uses' => 'MKondisiController@destroy',
                ]);
            }
        );
        Route::resource('mkondisi', 'MKondisiController')->except([
            'index',
            'store',
            'create',
            'edit',
        ]);

        #Master EQ Inspection
        Route::group(
            ['as' => 'meqinspec.', 'prefix' => 'meqinspec'],
            function () {
                Route::get('/', [
                    'as' => 'index',
                    'uses' => 'MEqInspectionController@index',
                ]);
                Route::get('/table', [
                    'as' => 'table',
                    'uses' => 'MEqInspectionController@table',
                ]);
                Route::get('/create', [
                    'as' => 'create',
                    'uses' => 'MEqInspectionController@create',
                ]);
                Route::post('/store', [
                    'as' => 'store',
                    'uses' => 'MEqInspectionController@store',
                ]);
                Route::get('edit/{id}', [
                    'as' => 'edit',
                    'uses' => 'MEqInspectionController@edit',
                ]);
                Route::patch('/update', [
                    'as' => 'update',
                    'uses' => 'MEqInspectionController@update',
                ]);
                Route::delete('/{id}/delete', [
                    'as' => 'destroy',
                    'uses' => 'MEqInspectionController@destroy',
                ]);
            }
        );
        Route::resource('meqinspec', 'MEqInspectionController')->except([
            'index',
            'store',
            'create',
            'edit',
        ]);
    }
);

#Profile
Route::get('/profile', [ProfileController::class, 'index'])
    ->middleware(['feauth'])
    ->name('profile');
Route::group(
    [
        'as' => 'configuration.',
        'prefix' => 'configuration',
        'namespace' => 'Configuration',
        'middleware' => ['feauth'],
    ],
    function () {
        Route::group(['as' => 'user.', 'prefix' => 'user'], function () {
            Route::get('/', [
                'as' => 'index',
                'uses' => 'UserController@index',
            ]);
            Route::get('/table', [
                'as' => 'table',
                'uses' => 'UserController@table',
            ]);
            Route::get('/create', [
                'as' => 'create',
                'uses' => 'UserController@create',
            ]);
            Route::post('/store', [
                'as' => 'store',
                'uses' => 'UserController@store',
            ]);
            Route::get('/{uuid}/edit', [
                'as' => 'edit',
                'uses' => 'UserController@edit',
            ]);
            Route::put('/{uuid}', [
                'as' => 'update',
                'uses' => 'UserController@update',
            ]);
            Route::delete('/{uuid}', [
                'as' => 'destroy',
                'uses' => 'UserController@destroy',
            ]);
        });

        Route::group(['as' => 'role.', 'prefix' => 'role'], function () {
            Route::get('/', [
                'as' => 'index',
                'uses' => 'RoleController@index',
            ]);
            Route::get('/table', [
                'as' => 'table',
                'uses' => 'RoleController@table',
            ]);
            Route::get('/create', [
                'as' => 'create',
                'uses' => 'RoleController@create',
            ]);
            Route::post('/store', [
                'as' => 'store',
                'uses' => 'RoleController@store',
            ]);
            Route::post('/addUser', [
                'as' => 'addUser',
                'uses' => 'RoleController@addUser',
            ]);
            Route::post('/destroyUser', [
                'as' => 'destroyUser',
                'uses' => 'RoleController@destroyUser',
            ]);
            Route::post('/addPermission', [
                'as' => 'addPermission',
                'uses' => 'RoleController@addPermission',
            ]);
            Route::get('/{uuid}/edit', [
                'as' => 'edit',
                'uses' => 'RoleController@edit',
            ]);
            Route::get('/{uuid}/detail', [
                'as' => 'detail',
                'uses' => 'RoleController@detail',
            ]);
            Route::get('/{uuid}/user', [
                'as' => 'user',
                'uses' => 'RoleController@user',
            ]);
            Route::get('/{uuid}/permission', [
                'as' => 'permission',
                'uses' => 'RoleController@permission',
            ]);
            Route::put('/{uuid}', [
                'as' => 'update',
                'uses' => 'RoleController@update',
            ]);
            Route::delete('/{uuid}', [
                'as' => 'destroy',
                'uses' => 'RoleController@destroy',
            ]);
        });
    }
);

Route::group(
    [
        'as' => 'she_safety.',
        'prefix' => 'she_safety',
        'namespace' => 'she_safety',
    ],
    function () {
        #Safety
        Route::group(['as' => 'dashboardsafety.', 'prefix' => 'dashboardsafety'], function () {
            Route::get('/', [
                'as' => 'index',
                'uses' => 'DashboardSafetyController@index',
            ]);
            Route::get('/dash_kpi/{bulan}/{tahun}', [
                'as' => 'dash_kpi',
                'uses' => 'DashboardSafetyController@dash_kpi',
            ]);
            Route::get('/dash_ltifr_ltisr/{bulan}/{tahun}/{status}', [
                'as' => 'dash_ltifr_ltisr',
                'uses' => 'DashboardSafetyController@dash_ltifr_ltisr',
            ]);
            Route::get('/dash_man_power/{bulan}/{tahun}', [
                'as' => 'dash_man_power',
                'uses' => 'DashboardSafetyController@dash_man_power',
            ]);
            Route::get('/dash_man_hours/{bulan}/{tahun}', [
                'as' => 'dash_man_hours',
                'uses' => 'DashboardSafetyController@dash_man_hours',
            ]);
            Route::get('/dash_unsafe_action/{bulan}/{tahun}', [
                'as' => 'dash_unsafe_action',
                'uses' => 'DashboardSafetyController@dash_unsafe_action',
            ]);
            Route::get('/dash_unsafe_condition/{bulan}/{tahun}', [
                'as' => 'dash_unsafe_condition',
                'uses' => 'DashboardSafetyController@dash_unsafe_condition',
            ]);
            Route::get('/dash_accident/{bulan}/{tahun}', [
                'as' => 'dash_accident',
                'uses' => 'DashboardSafetyController@dash_accident',
            ]);
            Route::get('/dash_accident_fire/{bulan}/{tahun}', [
                'as' => 'dash_accident_fire',
                'uses' => 'DashboardSafetyController@dash_accident_fire',
            ]);
            Route::get('/dash_safety_training/{bulan}/{tahun}', [
                'as' => 'dash_safety_training',
                'uses' => 'DashboardSafetyController@dash_safety_training',
            ]);
        });


        #Man Power
        Route::group(['as' => 'kpisafety.', 'prefix' => 'kpisafety'], function () {
            Route::get('/', [
                'as' => 'index',
                'uses' => 'KpiSafetyController@index',
            ]);
            Route::post('/store', [
                'as' => 'store',
                'uses' => 'KpiSafetyController@store',
            ]);
            Route::get('show/{tahun}', [
                'as' => 'show',
                'uses' => 'KpiSafetyController@show',
            ]);            
        });

        #Man Power
        Route::group(['as' => 'manpower.', 'prefix' => 'manpower'], function () {
            Route::get('/', [
                'as' => 'index',
                'uses' => 'ManPowerController@index',
            ]);
            Route::get('/table/{bulan}/{tahun}', [
                'as' => 'table',
                'uses' => 'ManPowerController@table',
            ]);
            Route::get('/create', [
                'as' => 'create',
                'uses' => 'ManPowerController@create',
            ]);
            Route::post('/store', [
                'as' => 'store',
                'uses' => 'ManPowerController@store',
            ]);
            Route::get('edit/{id}', [
                'as' => 'edit',
                'uses' => 'ManPowerController@edit',
            ]);
            Route::patch('/update/{id}', [
                'as' => 'update',
                'uses' => 'ManPowerController@update',
            ]);
            Route::delete('/{id}/delete', [
                'as' => 'destroy',
                'uses' => 'ManPowerController@destroy',
            ]);
            Route::get('/mdl_import', [
                'as' => 'mdl_import',
                'uses' => 'ManPowerController@mdl_import',
            ]);
            Route::get('/template', [
                'as' => 'template',
                'uses' => 'ManPowerController@template',
            ]);
            Route::post('/import', [
                'as' => 'import',
                'uses' => 'ManPowerController@import',
            ]);
        });

        #Man Hours
        Route::group(['as' => 'manhours.', 'prefix' => 'manhours'], function () {
            Route::get('/', [
                'as' => 'index',
                'uses' => 'ManHoursController@index',
            ]);
            Route::get('/table/{bulan}/{tahun}', [
                'as' => 'table',
                'uses' => 'ManHoursController@table',
            ]);
            Route::get('/create', [
                'as' => 'create',
                'uses' => 'ManHoursController@create',
            ]);
            Route::post('/store', [
                'as' => 'store',
                'uses' => 'ManHoursController@store',
            ]);
            Route::get('edit/{id}', [
                'as' => 'edit',
                'uses' => 'ManHoursController@edit',
            ]);
            Route::patch('/update/{id}', [
                'as' => 'update',
                'uses' => 'ManHoursController@update',
            ]);
            Route::delete('/{id}/delete', [
                'as' => 'destroy',
                'uses' => 'ManHoursController@destroy',
            ]);
        });

        #Unsafe Action
        Route::group(['as' => 'unsafeaction.', 'prefix' => 'unsafeaction'], function () {
            Route::get('/', [
                'as' => 'index',
                'uses' => 'UnsafeActionController@index',
            ]);
            Route::get('/table/{bulan}/{tahun}/{status}', [
                'as' => 'table',
                'uses' => 'UnsafeActionController@table',
            ]);
            Route::get('/create', [
                'as' => 'create',
                'uses' => 'UnsafeActionController@create',
            ]);
            Route::post('/store', [
                'as' => 'store',
                'uses' => 'UnsafeActionController@store',
            ]);
            Route::post('/upload_file', [
                'as' => 'upload_file',
                'uses' => 'UnsafeActionController@upload_file',
            ]);
            Route::get('edit/{id}', [
                'as' => 'edit',
                'uses' => 'UnsafeActionController@edit',
            ]);
            Route::patch('/update/{id}', [
                'as' => 'update',
                'uses' => 'UnsafeActionController@update',
            ]);
            Route::delete('/{id}/delete', [
                'as' => 'destroy',
                'uses' => 'UnsafeActionController@destroy',
            ]);
            Route::get('/mdl_import', [
                'as' => 'mdl_import',
                'uses' => 'UnsafeActionController@mdl_import',
            ]);
            Route::get('/template', [
                'as' => 'template',
                'uses' => 'UnsafeActionController@template',
            ]);
            Route::post('/import', [
                'as' => 'import',
                'uses' => 'UnsafeActionController@import',
            ]);
        });

        #Unsafe Condition
        Route::group(['as' => 'unsafecondition.', 'prefix' => 'unsafecondition'], function () {
            Route::get('/', [
                'as' => 'index',
                'uses' => 'UnsafeConditionController@index',
            ]);
            Route::get('/table/{bulan}/{tahun}/{status}', [
                'as' => 'table',
                'uses' => 'UnsafeConditionController@table',
            ]);
            Route::get('/create', [
                'as' => 'create',
                'uses' => 'UnsafeConditionController@create',
            ]);
            Route::post('/store', [
                'as' => 'store',
                'uses' => 'UnsafeConditionController@store',
            ]);
            Route::post('/upload_file', [
                'as' => 'upload_file',
                'uses' => 'UnsafeConditionController@upload_file',
            ]);
            Route::get('edit/{id}', [
                'as' => 'edit',
                'uses' => 'UnsafeConditionController@edit',
            ]);
            Route::patch('/update/{id}', [
                'as' => 'update',
                'uses' => 'UnsafeConditionController@update',
            ]);
            Route::delete('/{id}/delete', [
                'as' => 'destroy',
                'uses' => 'UnsafeConditionController@destroy',
            ]);
            Route::get('/mdl_import', [
                'as' => 'mdl_import',
                'uses' => 'UnsafeConditionController@mdl_import',
            ]);
            Route::get('/template', [
                'as' => 'template',
                'uses' => 'UnsafeConditionController@template',
            ]);
            Route::post('/import', [
                'as' => 'import',
                'uses' => 'UnsafeConditionController@import',
            ]);
        });

        #Accident Report
        Route::group(['as' => 'accidentreport.', 'prefix' => 'accidentreport'], function () {
            Route::get('/', [
                'as' => 'index',
                'uses' => 'AccidentReportController@index',
            ]);
            Route::get('/table/{bulan}/{tahun}/{status}', [
                'as' => 'table',
                'uses' => 'AccidentReportController@table',
            ]);
            Route::get('/create', [
                'as' => 'create',
                'uses' => 'AccidentReportController@create',
            ]);
            Route::post('/store', [
                'as' => 'store',
                'uses' => 'AccidentReportController@store',
            ]);
            Route::post('/upload_file', [
                'as' => 'upload_file',
                'uses' => 'AccidentReportController@upload_file',
            ]);
            Route::get('edit/{id}', [
                'as' => 'edit',
                'uses' => 'AccidentReportController@edit',
            ]);
            Route::patch('/update/{id}', [
                'as' => 'update',
                'uses' => 'AccidentReportController@update',
            ]);            
            Route::delete('/{id}/delete', [
                'as' => 'destroy',
                'uses' => 'AccidentReportController@destroy',
            ]);
        });

        #Fire Accident Report
        Route::group(['as' => 'fireaccidentreport.', 'prefix' => 'fireaccidentreport'], function () {
            Route::get('/', [
                'as' => 'index',
                'uses' => 'FireAccidentReportController@index',
            ]);
            Route::get('/table/{bulan}/{tahun}/{status}', [
                'as' => 'table',
                'uses' => 'FireAccidentReportController@table',
            ]);
            Route::get('/create', [
                'as' => 'create',
                'uses' => 'FireAccidentReportController@create',
            ]);
            Route::post('/store', [
                'as' => 'store',
                'uses' => 'FireAccidentReportController@store',
            ]);
            Route::post('/upload_file', [
                'as' => 'upload_file',
                'uses' => 'FireAccidentReportController@upload_file',
            ]);
            Route::get('edit/{id}', [
                'as' => 'edit',
                'uses' => 'FireAccidentReportController@edit',
            ]);
            Route::patch('/update/{id}', [
                'as' => 'update',
                'uses' => 'FireAccidentReportController@update',
            ]);  
            Route::delete('/{id}/delete', [
                'as' => 'destroy',
                'uses' => 'FireAccidentReportController@destroy',
            ]);
        });

        #Safety Training
        Route::group(['as' => 'safetytraining.', 'prefix' => 'safetytraining'], function () {
            Route::get('/', [
                'as' => 'index',
                'uses' => 'SafetyTrainingController@index',
            ]);
            Route::get('/table/{bulan}/{tahun}', [
                'as' => 'table',
                'uses' => 'SafetyTrainingController@table',
            ]);
            Route::get('/table_org/{id}', [
                'as' => 'table_org',
                'uses' => 'SafetyTrainingController@table_org',
            ]);
            Route::get('/create', [
                'as' => 'create',
                'uses' => 'SafetyTrainingController@create',
            ]);
            Route::post('/store', [
                'as' => 'store',
                'uses' => 'SafetyTrainingController@store',
            ]);
            Route::post('/store_org', [
                'as' => 'store_org',
                'uses' => 'SafetyTrainingController@store_org',
            ]);
            Route::get('edit/{id}', [
                'as' => 'edit',
                'uses' => 'SafetyTrainingController@edit',
            ]);
            Route::get('data_org/{id}', [
                'as' => 'data_org',
                'uses' => 'SafetyTrainingController@data_org',
            ]);
            Route::patch('/update/{id}', [
                'as' => 'update',
                'uses' => 'SafetyTrainingController@update',
            ]);
            Route::delete('/{id}/delete', [
                'as' => 'destroy',
                'uses' => 'SafetyTrainingController@destroy',
            ]);
            Route::delete('/{id}/delete_org', [
                'as' => 'destroy_org',
                'uses' => 'SafetyTrainingController@destroy_org',
            ]);
            Route::get('/template', [
                'as' => 'template',
                'uses' => 'SafetyTrainingController@template',
            ]);
            Route::post('/import', [
                'as' => 'import',
                'uses' => 'SafetyTrainingController@import',
            ]);
        });

        #Apar
        Route::group(['as' => 'apar.', 'prefix' => 'apar'], function () {
            Route::get('/', [
                'as' => 'index',
                'uses' => 'AparController@index',
            ]);
            Route::get('/table/{bulan}/{tahun}/{status}', [
                'as' => 'table',
                'uses' => 'AparController@table',
            ]);
            Route::get('/create', [
                'as' => 'create',
                'uses' => 'AparController@create',
            ]);
            Route::post('/store', [
                'as' => 'store',
                'uses' => 'AparController@store',
            ]);
            Route::post('/upload_file', [
                'as' => 'upload_file',
                'uses' => 'AparController@upload_file',
            ]);
            Route::get('edit/{id}', [
                'as' => 'edit',
                'uses' => 'AparController@edit',
            ]);
            Route::patch('/update/{id}', [
                'as' => 'update',
                'uses' => 'AparController@update',
            ]);
            Route::delete('/{id}/delete', [
                'as' => 'destroy',
                'uses' => 'AparController@destroy',
            ]);
            Route::get('/mdl_import', [
                'as' => 'mdl_import',
                'uses' => 'AparController@mdl_import',
            ]);
            Route::get('/template', [
                'as' => 'template',
                'uses' => 'AparController@template',
            ]);
            Route::post('/import', [
                'as' => 'import',
                'uses' => 'AparController@import',
            ]);
        });

        #Fire Alarm
        Route::group(['as' => 'firealarm.', 'prefix' => 'firealarm'], function () {
            Route::get('/', [
                'as' => 'index',
                'uses' => 'FireAlarmController@index',
            ]);
            Route::get('/table/{bulan}/{tahun}', [
                'as' => 'table',
                'uses' => 'FireAlarmController@table',
            ]);
            Route::get('/table_doc/{id}', [
                'as' => 'table_doc',
                'uses' => 'FireAlarmController@table_doc',
            ]);
            Route::get('/dokumentasi/{id}', [
                'as' => 'dokumentasi',
                'uses' => 'FireAlarmController@dokumentasi',
            ]);
            Route::get('/create', [
                'as' => 'create',
                'uses' => 'FireAlarmController@create',
            ]);
            Route::post('/store', [
                'as' => 'store',
                'uses' => 'FireAlarmController@store',
            ]);
            Route::post('/store_doc', [
                'as' => 'store_doc',
                'uses' => 'FireAlarmController@store_doc',
            ]);
            Route::post('/upload_file', [
                'as' => 'upload_file',
                'uses' => 'FireAlarmController@upload_file',
            ]);
            Route::get('edit/{id}', [
                'as' => 'edit',
                'uses' => 'FireAlarmController@edit',
            ]);
            Route::patch('/update/{id}', [
                'as' => 'update',
                'uses' => 'FireAlarmController@update',
            ]);
            Route::delete('/{id}/delete', [
                'as' => 'destroy',
                'uses' => 'FireAlarmController@destroy',
            ]);
            Route::delete('/{id}/delete_doc', [
                'as' => 'destroy_doc',
                'uses' => 'FireAlarmController@destroy_doc',
            ]);
            Route::get('/mdl_import', [
                'as' => 'mdl_import',
                'uses' => 'FireAlarmController@mdl_import',
            ]);
            Route::get('/template', [
                'as' => 'template',
                'uses' => 'FireAlarmController@template',
            ]);
            Route::post('/import', [
                'as' => 'import',
                'uses' => 'FireAlarmController@import',
            ]);
        });

        #Hydrant
        Route::group(['as' => 'hydrant.', 'prefix' => 'hydrant'], function () {
            Route::get('/', [
                'as' => 'index',
                'uses' => 'HydrantController@index',
            ]);
            Route::get('/table/{bulan}/{tahun}', [
                'as' => 'table',
                'uses' => 'HydrantController@table',
            ]);
            Route::get('/table_doc/{id}', [
                'as' => 'table_doc',
                'uses' => 'HydrantController@table_doc',
            ]);
            Route::get('/dokumentasi/{id}', [
                'as' => 'dokumentasi',
                'uses' => 'HydrantController@dokumentasi',
            ]);
            Route::get('/create', [
                'as' => 'create',
                'uses' => 'HydrantController@create',
            ]);
            Route::post('/store', [
                'as' => 'store',
                'uses' => 'HydrantController@store',
            ]);
            Route::post('/store_doc', [
                'as' => 'store_doc',
                'uses' => 'HydrantController@store_doc',
            ]);
            Route::post('/upload_file', [
                'as' => 'upload_file',
                'uses' => 'HydrantController@upload_file',
            ]);
            Route::get('edit/{id}', [
                'as' => 'edit',
                'uses' => 'HydrantController@edit',
            ]);
            Route::patch('/update/{id}', [
                'as' => 'update',
                'uses' => 'HydrantController@update',
            ]);
            Route::delete('/{id}/delete', [
                'as' => 'destroy',
                'uses' => 'HydrantController@destroy',
            ]);
            Route::delete('/{id}/delete_doc', [
                'as' => 'destroy_doc',
                'uses' => 'HydrantController@destroy_doc',
            ]);
            Route::get('/mdl_import', [
                'as' => 'mdl_import',
                'uses' => 'HydrantController@mdl_import',
            ]);
            Route::get('/template', [
                'as' => 'template',
                'uses' => 'HydrantController@template',
            ]);
            Route::post('/import', [
                'as' => 'import',
                'uses' => 'HydrantController@import',
            ]);
        });
        

        #Master Jenis Limbah
        Route::group(['as' => 'master-jenis-limbah.', 'prefix' => 'master-jenis-limbah'], function () {
            Route::get('/', [
                'as' => 'index',
                'uses' => 'MasterJenisLimbahController@index',
            ]);
            Route::get('/table/{plant}', [
                'as' => 'table',
                'uses' => 'MasterJenisLimbahController@table',
            ]);
            Route::get('/create', [
                'as' => 'create',
                'uses' => 'MasterJenisLimbahController@create',
            ]);
            Route::get('/edit', [
                'as' => 'edit',
                'uses' => 'MasterJenisLimbahController@edit',
            ]);
            Route::get('/destroy', [
                'as' => 'destroy',
                'uses' => 'MasterJenisLimbahController@destroy',
            ]);
            Route::get('/store', [
                'as' => 'store',
                'uses' => 'MasterJenisLimbahController@store',
            ]);
            Route::get('/mdl_import', [
                'as' => 'mdl_import',
                'uses' => 'AparController@mdl_import',
            ]);
        });

        #Master Dokumen Lingkungan
        Route::group(['as' => 'dokumen-lingkungan.', 'prefix' => 'dokumen-lingkungan'], function () {
            Route::get('/', [
                'as' => 'index',
                'uses' => 'DokumenLingkunganController@index',
            ]);
            Route::get('/table/{area}/{tahun}', [
                'as' => 'table',
                'uses' => 'DokumenLingkunganController@table',
            ]);
            Route::get('/create', [
                'as' => 'create',
                'uses' => 'DokumenLingkunganController@create',
            ]);
            Route::get('/edit/{uuid}', [
                'as' => 'edit',
                'uses' => 'DokumenLingkunganController@edit',
            ]);
            Route::put('/update/{uuid}', [
                'as' => 'update',
                'uses' => 'DokumenLingkunganController@update',
            ]);
            Route::delete('/destroy/{uuid}', [
                'as' => 'destroy',
                'uses' => 'DokumenLingkunganController@destroy',
            ]);
            Route::post('/store', [
                'as' => 'store',
                'uses' => 'DokumenLingkunganController@store',
            ]);
            Route::get('/ddl/area', [
                'as' => 'dokumen-lingkungan-get-area',
                'uses' => 'DokumenLingkunganController@getArea',
            ]);
            Route::post('/upload-file', [
                'as' => 'upload-file',
                'uses' => 'DokumenLingkunganController@uploadFile',
            ]);
            Route::get('/mdl_import', [
                'as' => 'mdl_import',
                'uses' => 'AparController@mdl_import',
            ]);
        });

        #Master Inspeksi Lingkungan
        Route::group(['as' => 'master-inspeksi-lingkungan.', 'prefix' => 'master-inspeksi-lingkungan'], function () {
            Route::get('/', [
                'as' => 'index',
                'uses' => 'MasterInspeskiLingkunganController@index',
            ]);
            Route::get('/table/{plant}', [
                'as' => 'table',
                'uses' => 'MasterInspeskiLingkunganController@table',
            ]);
            Route::get('/create', [
                'as' => 'create',
                'uses' => 'MasterInspeskiLingkunganController@create',
            ]);
            Route::get('/edit', [
                'as' => 'edit',
                'uses' => 'MasterInspeskiLingkunganController@edit',
            ]);
            Route::get('/destroy', [
                'as' => 'destroy',
                'uses' => 'MasterInspeskiLingkunganController@destroy',
            ]);
            Route::get('/store', [
                'as' => 'store',
                'uses' => 'MasterInspeskiLingkunganController@store',
            ]);
            Route::get('/mdl_import', [
                'as' => 'mdl_import',
                'uses' => 'AparController@mdl_import',
            ]);
        });

        #Neraca Limbah B3
        Route::group(['as' => 'neraca-limbah-b3.', 'prefix' => 'neraca-limbah-b3'], function () {
            Route::get('/', [
                'as' => 'index',
                'uses' => 'NeracaLimbahB3Controller@index',
            ]);
            Route::get('/table/{plant}', [
                'as' => 'table',
                'uses' => 'NeracaLimbahB3Controller@table',
            ]);
            Route::get('/create', [
                'as' => 'create',
                'uses' => 'NeracaLimbahB3Controller@create',
            ]);
            Route::get('/edit', [
                'as' => 'edit',
                'uses' => 'NeracaLimbahB3Controller@edit',
            ]);
            Route::get('/destroy', [
                'as' => 'destroy',
                'uses' => 'NeracaLimbahB3Controller@destroy',
            ]);
            Route::get('/store', [
                'as' => 'store',
                'uses' => 'NeracaLimbahB3Controller@store',
            ]);
            Route::get('/mdl_import', [
                'as' => 'mdl_import',
                'uses' => 'AparController@mdl_import',
            ]);
            Route::get('/get-table/{plant}', [
                'as' => 'get-table',
                'uses' => 'NeracaLimbahB3Controller@getTable',
            ]);
        });
    }
);

Route::group(
    [
        'as' => 'production.',
        'prefix' => 'production',
        'namespace' => 'production',
    ],
    function () {
        #Crusher
        Route::group(['as' => 'crusher.', 'prefix' => 'crusher'], function () {
            Route::get('/', [
                'as' => 'index',
                'uses' => 'CrusherReportsController@index',
            ]);
            Route::get('/table', [
                'as' => 'table',
                'uses' => 'CrusherReportsController@table',
            ]);
        });
        #Rawmill
        Route::group(['as' => 'rawmill.', 'prefix' => 'rawmill'], function () {
            Route::get('/', [
                'as' => 'index',
                'uses' => 'RawmillReportsController@index',
            ]);
            Route::get('/table', [
                'as' => 'table',
                'uses' => 'RawmillReportsController@table',
            ]);
        });
        #Clinker
        Route::group(['as' => 'clinker.', 'prefix' => 'clinker'], function () {
            Route::get('/', [
                'as' => 'index',
                'uses' => 'ClinkerReportsController@index',
            ]);
            Route::get('/table', [
                'as' => 'table',
                'uses' => 'ClinkerReportsController@table',
            ]);
        });
        #Coal Mill
        Route::group(['as' => 'coalmill.', 'prefix' => 'coalmill'], function () {
            Route::get('/', [
                'as' => 'index',
                'uses' => 'CoalMillReportsController@index',
            ]);
            Route::get('/table', [
                'as' => 'table',
                'uses' => 'CoalMillReportsController@table',
            ]);
        });

         #Cement
         Route::group(['as' => 'cement.', 'prefix' => 'cement'], function () {
            Route::get('/', [
                'as' => 'index',
                'uses' => 'CementReportsController@index',
            ]);
            Route::get('/table', [
                'as' => 'table',
                'uses' => 'CementReportsController@table',
            ]);
        });
        #Packer
        Route::group(['as' => 'packer.', 'prefix' => 'packer'], function () {
            Route::get('/', [
                'as' => 'index',
                'uses' => 'PackerReportsController@index',
            ]);
            Route::get('/table', [
                'as' => 'table',
                'uses' => 'PackerReportsController@table',
            ]);
            Route::get('/status', [
                'as' => 'status',
                'uses' => 'PackerReportsController@status',
            ]);
            Route::get('/silo', [
                'as' => 'silo',
                'uses' => 'PackerReportsController@silo',
            ]);
        });
        #Production Index
        Route::group(['as' => 'prodindex.', 'prefix' => 'prodindex'], function () {
            Route::get('/', [
                'as' => 'index',
                'uses' => 'ProdutctionIndexReportsController@index',
            ]);
            Route::get('/table', [
                'as' => 'table',
                'uses' => 'ProdutctionIndexReportsController@table',
            ]);
        });
         #WHRPG
         Route::group(['as' => 'whrpg.', 'prefix' => 'whrpg'], function () {
            Route::get('/', [
                'as' => 'index',
                'uses' => 'WhrpgReportsController@index',
            ]);
            Route::get('/table', [
                'as' => 'table',
                'uses' => 'WhrpgReportsController@table',
            ]);
        });
    }
);

Route::group(
    [
        'as' => 'she_proper.',
        'prefix' => 'she_proper',
        'namespace' => 'she_proper',
    ],
    function () {
        #Kebijakan Lingkungan
        Route::group(['as' => 'kebijakan_ling.', 'prefix' => 'kebijakan_ling'], function () {
            Route::get('/', [
                'as' => 'index',
                'uses' => 'KebijakanLingkunganController@index',
            ]);
            Route::get('/table/{tahun}', [
                'as' => 'table',
                'uses' => 'KebijakanLingkunganController@table',
            ]);
            Route::get('/create', [
                'as' => 'create',
                'uses' => 'KebijakanLingkunganController@create',
            ]);
            Route::post('/store', [
                'as' => 'store',
                'uses' => 'KebijakanLingkunganController@store',
            ]);
            Route::post('/upload_file', [
                'as' => 'upload_file',
                'uses' => 'KebijakanLingkunganController@upload_file',
            ]);
            Route::get('edit/{id}', [
                'as' => 'edit',
                'uses' => 'KebijakanLingkunganController@edit',
            ]);
            Route::patch('/update/{id}', [
                'as' => 'update',
                'uses' => 'KebijakanLingkunganController@update',
            ]);            
            Route::delete('/{id}/delete', [
                'as' => 'destroy',
                'uses' => 'KebijakanLingkunganController@destroy',
            ]);
        }); 
        
        #Struktur Organisasi
        Route::group(['as' => 'struktur_organ.', 'prefix' => 'struktur_organ'], function () {
            Route::get('/', [
                'as' => 'index',
                'uses' => 'StrukturOrganController@index',
            ]);
            Route::get('/table/{tahun}', [
                'as' => 'table',
                'uses' => 'StrukturOrganController@table',
            ]);
            Route::get('/create', [
                'as' => 'create',
                'uses' => 'StrukturOrganController@create',
            ]);
            Route::post('/store', [
                'as' => 'store',
                'uses' => 'StrukturOrganController@store',
            ]);
            Route::post('/upload_file', [
                'as' => 'upload_file',
                'uses' => 'StrukturOrganController@upload_file',
            ]);
            Route::get('edit/{id}', [
                'as' => 'edit',
                'uses' => 'StrukturOrganController@edit',
            ]);
            Route::patch('/update/{id}', [
                'as' => 'update',
                'uses' => 'StrukturOrganController@update',
            ]);            
            Route::delete('/{id}/delete', [
                'as' => 'destroy',
                'uses' => 'StrukturOrganController@destroy',
            ]);
        }); 

        #Program Lingkungan
        Route::group(['as' => 'program_ling.', 'prefix' => 'program_ling'], function () {
            Route::get('/', [
                'as' => 'index',
                'uses' => 'ProgramLingkunganController@index',
            ]);
            Route::get('/table/{tahun}', [
                'as' => 'table',
                'uses' => 'ProgramLingkunganController@table',
            ]);
            Route::get('/table_is/{id}', [
                'as' => 'table_is',
                'uses' => 'ProgramLingkunganController@table_is',
            ]);
            Route::get('/create', [
                'as' => 'create',
                'uses' => 'ProgramLingkunganController@create',
            ]);
            Route::post('/store', [
                'as' => 'store',
                'uses' => 'ProgramLingkunganController@store',
            ]);
            Route::post('/store_is', [
                'as' => 'store_is',
                'uses' => 'ProgramLingkunganController@store_is',
            ]);
            Route::get('edit/{id}', [
                'as' => 'edit',
                'uses' => 'ProgramLingkunganController@edit',
            ]);
            Route::get('data_is/{id}', [
                'as' => 'data_is',
                'uses' => 'ProgramLingkunganController@data_is',
            ]);
            Route::patch('/update/{id}', [
                'as' => 'update',
                'uses' => 'ProgramLingkunganController@update',
            ]);
            Route::delete('/{id}/delete', [
                'as' => 'destroy',
                'uses' => 'ProgramLingkunganController@destroy',
            ]);
            Route::delete('/{id}/delete_is', [
                'as' => 'destroy_is',
                'uses' => 'ProgramLingkunganController@destroy_is',
            ]);            
        });

        #Perizinan Lingkungan
        Route::group(['as' => 'izin_ling.', 'prefix' => 'izin_ling'], function () {
            Route::get('/', [
                'as' => 'index',
                'uses' => 'PerizinanLingkunganController@index',
            ]);
            Route::get('/table/{tahun}', [
                'as' => 'table',
                'uses' => 'PerizinanLingkunganController@table',
            ]);
            Route::get('/create', [
                'as' => 'create',
                'uses' => 'PerizinanLingkunganController@create',
            ]);
            Route::post('/store', [
                'as' => 'store',
                'uses' => 'PerizinanLingkunganController@store',
            ]);            
            Route::get('edit/{id}', [
                'as' => 'edit',
                'uses' => 'PerizinanLingkunganController@edit',
            ]);
            Route::patch('/update/{id}', [
                'as' => 'update',
                'uses' => 'PerizinanLingkunganController@update',
            ]);
            Route::delete('/{id}/delete', [
                'as' => 'destroy',
                'uses' => 'PerizinanLingkunganController@destroy',
            ]);
            Route::get('data_file/{id}', [
                'as' => 'data_file',
                'uses' => 'PerizinanLingkunganController@data_file',
            ]);
            Route::get('/table_file/{id}', [
                'as' => 'table_file',
                'uses' => 'PerizinanLingkunganController@table_file',
            ]);
            Route::post('/upload_file', [
                'as' => 'upload_file',
                'uses' => 'PerizinanLingkunganController@upload_file',
            ]);
            Route::post('/store_file', [
                'as' => 'store_file',
                'uses' => 'PerizinanLingkunganController@store_file',
            ]);
            Route::delete('/{id}/delete_file', [
                'as' => 'destroy_file',
                'uses' => 'PerizinanLingkunganController@destroy_file',
            ]);
        });

        #Program Lingkungan
        Route::group(['as' => 'program_ling.', 'prefix' => 'program_ling'], function () {
            Route::get('/', [
                'as' => 'index',
                'uses' => 'ProgramLingkunganController@index',
            ]);
            Route::get('/table/{tahun}', [
                'as' => 'table',
                'uses' => 'ProgramLingkunganController@table',
            ]);
            Route::get('/table_is/{id}', [
                'as' => 'table_is',
                'uses' => 'ProgramLingkunganController@table_is',
            ]);
            Route::get('/create', [
                'as' => 'create',
                'uses' => 'ProgramLingkunganController@create',
            ]);
            Route::post('/store', [
                'as' => 'store',
                'uses' => 'ProgramLingkunganController@store',
            ]);
            Route::post('/store_is', [
                'as' => 'store_is',
                'uses' => 'ProgramLingkunganController@store_is',
            ]);
            Route::get('edit/{id}', [
                'as' => 'edit',
                'uses' => 'ProgramLingkunganController@edit',
            ]);
            Route::get('data_is/{id}', [
                'as' => 'data_is',
                'uses' => 'ProgramLingkunganController@data_is',
            ]);
            Route::patch('/update/{id}', [
                'as' => 'update',
                'uses' => 'ProgramLingkunganController@update',
            ]);
            Route::delete('/{id}/delete', [
                'as' => 'destroy',
                'uses' => 'ProgramLingkunganController@destroy',
            ]);
            Route::delete('/{id}/delete_is', [
                'as' => 'destroy_is',
                'uses' => 'ProgramLingkunganController@destroy_is',
            ]);            
        });

        #KPI Proper
        Route::group(['as' => 'kpiproper.', 'prefix' => 'kpiproper'], function () {
            Route::get('/', [
                'as' => 'index',
                'uses' => 'KpiProperController@index',
            ]);
            Route::get('/table/{tahun}', [
                'as' => 'table',
                'uses' => 'KpiProperController@table',
            ]);
            Route::get('/create', [
                'as' => 'create',
                'uses' => 'KpiProperController@create',
            ]);
            Route::post('/store', [
                'as' => 'store',
                'uses' => 'KpiProperController@store',
            ]);
            Route::get('edit/{id}', [
                'as' => 'edit',
                'uses' => 'KpiProperController@edit',
            ]);
            Route::patch('/update/{id}', [
                'as' => 'update',
                'uses' => 'KpiProperController@update',
            ]);
            Route::delete('/{id}/delete', [
                'as' => 'destroy',
                'uses' => 'KpiProperController@destroy',
            ]);
            Route::get('/mdl_import', [
                'as' => 'mdl_import',
                'uses' => 'KpiProperController@mdl_import',
            ]);
            Route::get('/template', [
                'as' => 'template',
                'uses' => 'KpiProperController@template',
            ]);
            Route::post('/import', [
                'as' => 'import',
                'uses' => 'KpiProperController@import',
            ]);
        });

        #Matrik Peraturan Perundangan Lingkungan
        Route::group(['as' => 'matrik_ling.', 'prefix' => 'matrik_ling'], function () {
            Route::get('/', [
                'as' => 'index',
                'uses' => 'MatrikLingController@index',
            ]);
            Route::get('/table/{tahun}', [
                'as' => 'table',
                'uses' => 'MatrikLingController@table',
            ]);
            Route::get('/create', [
                'as' => 'create',
                'uses' => 'MatrikLingController@create',
            ]);
            Route::post('/store', [
                'as' => 'store',
                'uses' => 'MatrikLingController@store',
            ]);
            Route::get('edit/{id}', [
                'as' => 'edit',
                'uses' => 'MatrikLingController@edit',
            ]);
            Route::patch('/update/{id}', [
                'as' => 'update',
                'uses' => 'MatrikLingController@update',
            ]);            
            Route::delete('/{id}/delete', [
                'as' => 'destroy',
                'uses' => 'MatrikLingController@destroy',
            ]);
        });

        #Sertifikat Analisis
        Route::group(['as' => 'sertif_analis.', 'prefix' => 'sertif_analis'], function () {
            Route::get('/', [
                'as' => 'index',
                'uses' => 'SertifAnalisController@index',
            ]);
            Route::get('/table/{tahun}', [
                'as' => 'table',
                'uses' => 'SertifAnalisController@table',
            ]);
            Route::get('/create', [
                'as' => 'create',
                'uses' => 'SertifAnalisController@create',
            ]);
            Route::post('/store', [
                'as' => 'store',
                'uses' => 'SertifAnalisController@store',
            ]);
            Route::post('/upload_file', [
                'as' => 'upload_file',
                'uses' => 'SertifAnalisController@upload_file',
            ]);
            Route::get('edit/{id}', [
                'as' => 'edit',
                'uses' => 'SertifAnalisController@edit',
            ]);
            Route::patch('/update/{id}', [
                'as' => 'update',
                'uses' => 'SertifAnalisController@update',
            ]);            
            Route::delete('/{id}/delete', [
                'as' => 'destroy',
                'uses' => 'SertifAnalisController@destroy',
            ]);
        });

        #Sertifikat/penghargaan Lingkungan
        Route::group(['as' => 'sertif_ling.', 'prefix' => 'sertif_ling'], function () {
            Route::get('/', [
                'as' => 'index',
                'uses' => 'SertifLingController@index',
            ]);
            Route::get('/table/{tahun}', [
                'as' => 'table',
                'uses' => 'SertifLingController@table',
            ]);
            Route::get('/create', [
                'as' => 'create',
                'uses' => 'SertifLingController@create',
            ]);
            Route::post('/store', [
                'as' => 'store',
                'uses' => 'SertifLingController@store',
            ]);
            Route::post('/upload_file', [
                'as' => 'upload_file',
                'uses' => 'SertifLingController@upload_file',
            ]);
            Route::get('edit/{id}', [
                'as' => 'edit',
                'uses' => 'SertifLingController@edit',
            ]);
            Route::patch('/update/{id}', [
                'as' => 'update',
                'uses' => 'SertifLingController@update',
            ]);            
            Route::delete('/{id}/delete', [
                'as' => 'destroy',
                'uses' => 'SertifLingController@destroy',
            ]);
        });

        #Kompetensi Personil
        Route::group(['as' => 'kom_personil.', 'prefix' => 'kom_personil'], function () {
            Route::get('/', [
                'as' => 'index',
                'uses' => 'KomPersonilController@index',
            ]);
            Route::get('/table/{tahun}', [
                'as' => 'table',
                'uses' => 'KomPersonilController@table',
            ]);
            Route::get('/create', [
                'as' => 'create',
                'uses' => 'KomPersonilController@create',
            ]);
            Route::post('/store', [
                'as' => 'store',
                'uses' => 'KomPersonilController@store',
            ]);            
            Route::get('edit/{id}', [
                'as' => 'edit',
                'uses' => 'KomPersonilController@edit',
            ]);
            Route::patch('/update/{id}', [
                'as' => 'update',
                'uses' => 'KomPersonilController@update',
            ]);
            Route::delete('/{id}/delete', [
                'as' => 'destroy',
                'uses' => 'KomPersonilController@destroy',
            ]);
            Route::get('data_file/{id}', [
                'as' => 'data_file',
                'uses' => 'KomPersonilController@data_file',
            ]);
            Route::get('/table_file/{id}', [
                'as' => 'table_file',
                'uses' => 'KomPersonilController@table_file',
            ]);
            Route::post('/upload_file', [
                'as' => 'upload_file',
                'uses' => 'KomPersonilController@upload_file',
            ]);
            Route::post('/store_file', [
                'as' => 'store_file',
                'uses' => 'KomPersonilController@store_file',
            ]);
            Route::delete('/{id}/delete_file', [
                'as' => 'destroy_file',
                'uses' => 'KomPersonilController@destroy_file',
            ]);
        });

        #Pengukuran
        Route::group(['as' => 'hasil_pengukuran.', 'prefix' => 'hasil_pengukuran'], function () {
            Route::get('/', [
                'as' => 'index',
                'uses' => 'HasilPengukuranController@index',
            ]);
            Route::get('/table/{tahun}', [
                'as' => 'table',
                'uses' => 'HasilPengukuranController@table',
            ]);
            Route::get('/create', [
                'as' => 'create',
                'uses' => 'HasilPengukuranController@create',
            ]);
            Route::post('/store', [
                'as' => 'store',
                'uses' => 'HasilPengukuranController@store',
            ]);
            Route::get('edit/{id}', [
                'as' => 'edit',
                'uses' => 'HasilPengukuranController@edit',
            ]);
            Route::patch('/update/{id}', [
                'as' => 'update',
                'uses' => 'HasilPengukuranController@update',
            ]);            
            Route::delete('/{id}/delete', [
                'as' => 'destroy',
                'uses' => 'HasilPengukuranController@destroy',
            ]);
        });
    }
);

require __DIR__ . '/auth.php';

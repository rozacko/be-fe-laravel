<?php

Route::name('upload-schedule.')->prefix('/upload-schedule')->middleware(['feauth'])->group(function ()
{
    Route::get('/', 'Vendor\UploadScheduleController@index')->name('index');
    Route::get('/create', 'Vendor\UploadScheduleController@create')->name('create');
    Route::post('/datatable', 'Vendor\UploadScheduleController@table')->name('table');
    Route::get('/downloadTemplate/{type}', 'Vendor\UploadScheduleController@DownloadTemplate')->name('downloadTemplate');
    Route::post('/import', ['as' => 'import', 'uses' => 'Vendor\UploadScheduleController@import']);
});

Route::name('realisasi-fingerprint.')->prefix('/realisasi-fingerprint')->middleware(['feauth'])->group(function ()
{
    Route::get('/', 'Vendor\RealisasiFingerprintController@index')->name('index');
    Route::get('/create', 'Vendor\RealisasiFingerprintController@create')->name('create');
    Route::get('/datatable/{bulan}/{tahun}', 'Vendor\RealisasiFingerprintController@table')->name('table');
    Route::post('/downloadExcel', 'Vendor\RealisasiFingerprintController@DownloadExcel')->name('downloadExcel');
    Route::get('/downloadTemplate/{type}', 'Vendor\RealisasiFingerprintController@DownloadTemplate')->name('downloadTemplate');
    Route::post('/import', ['as' => 'import', 'uses' => 'Vendor\RealisasiFingerprintController@import']);
});

Route::name('planning-realisasi-fingerprint.')->prefix('/planning-realisasi-fingerprint')->middleware(['feauth'])->group(function ()
{
    Route::get('/', 'Vendor\PlanningRealisasiFingerprintController@index')->name('index');
    Route::get('/datatable/{bulan}/{tahun}', 'Vendor\PlanningRealisasiFingerprintController@table')->name('table');
    Route::get('/downloadTemplate', 'Vendor\PlanningRealisasiFingerprintController@DownloadTemplate')->name('downloadTemplate');
});
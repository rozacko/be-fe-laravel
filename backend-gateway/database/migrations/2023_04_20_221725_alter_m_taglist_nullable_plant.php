<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table("m_taglist", function(Blueprint $table){
            $table->integer('id_plant')->nullable(true)->change();
            $table->string('hourly_type')->default('average');
            $table->float('hourly_factor')->default(1);
            $table->string('daily_type')->default('average');
            $table->float('daily_factor')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table("m_taglist", function(Blueprint $table){
            $table->integer('id_plant')->nullable(false)->change();
            $table->dropColumn('hourly_type');
            $table->dropColumn('hourly_factor');
            $table->dropColumn('daily_type');
            $table->dropColumn('daily_factor');
        });
    }
};

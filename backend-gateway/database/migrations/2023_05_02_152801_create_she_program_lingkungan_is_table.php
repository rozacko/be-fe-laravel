<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('she_program_lingkungan_is', function (Blueprint $table) {
            $table->id();
            $table->integer('id_program');
            $table->longText('inisiatif_strategis');
            $table->string('start_date');
            $table->string('end_date');
            $table->string('created_by');
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('she_program_lingkungan_is');
    }
};

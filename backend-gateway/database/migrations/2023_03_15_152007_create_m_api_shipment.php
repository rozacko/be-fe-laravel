<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_api_shipment', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid')->default(DB::raw('public.uuid_generate_v4()'));
            $table->string('api_name');
            $table->string('api_url');
            $table->string('api_method')->default('POST');
            $table->text('api_params')->nullable(true);
            $table->boolean('status')->default(true);
            $table->string('created_by');
            $table->string('updated_by')->nullable(true);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_api_shipment');
    }
};

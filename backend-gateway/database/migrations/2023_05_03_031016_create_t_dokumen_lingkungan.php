<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_dokumen_lingkungan', function (Blueprint $table) {
            $table->id();
            $table->uuid();
            $table->unsignedBigInteger('area_id');
            $table->string('nm_area', 255)->nullable();
            $table->string('judul', 255)->nullable();
            $table->string('no_dokumen', 255)->nullable();
            $table->string('ruang_lingkup', 255)->nullable();
            $table->string('jenis_dokumen', 255)->nullable();
            $table->string('filename', 255)->nullable();
            $table->string('created_by', 255)->nullable();
            $table->string('updated_by', 255)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_dokumen_lingkungan');
    }
};

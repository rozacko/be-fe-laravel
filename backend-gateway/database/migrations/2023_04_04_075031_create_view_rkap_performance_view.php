<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('
        create or replace view view_rkap_performance  AS select trp.id, trp.uuid,trp.id_plant, mp2.nm_plant as plant_name ,trp.tahun ,trp.id_parameter,mp.name as paramter_name , items.*,trp.status,trp.created_by ,trp.created_at ,trp.updated_by ,trp.updated_at  from 
t_rkap_performance trp 
left join m_parameter mp ON mp.id = trp.id_parameter 
left join m_plant mp2 on mp2.id = trp.id_plant 
left join (
SELECT A.id_rkap_performance,
    SUM(nilai_rkap) FILTER (WHERE bulan =  1) jan,
    SUM(nilai_rkap) FILTER (WHERE bulan =  2) feb,
    SUM(nilai_rkap) FILTER (WHERE bulan =  3) mar,
    SUM(nilai_rkap) FILTER (WHERE bulan =  4) apr,
    SUM(nilai_rkap) FILTER (WHERE bulan =  5) mei,
    SUM(nilai_rkap) FILTER (WHERE bulan =  6) jun,
    SUM(nilai_rkap) FILTER (WHERE bulan =  7) jul,
    SUM(nilai_rkap) FILTER (WHERE bulan =  8) ags,
    SUM(nilai_rkap) FILTER (WHERE bulan =  9) sep,
    SUM(nilai_rkap) FILTER (WHERE bulan =  10) okt,
    SUM(nilai_rkap) FILTER (WHERE bulan =  11) nov,
    SUM(nilai_rkap) FILTER (WHERE bulan =  12) des
      FROM (SELECT trpi .*          
              FROM t_rkap_performance_items trpi  
           ) A
     GROUP BY A.id_rkap_performance) items on trp.id=items.id_rkap_performance');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('view_rkap_performance_view');
    }
};

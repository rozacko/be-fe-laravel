<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sap_sync_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('sap_sync_config_id');
            $table->string('config_name');
            $table->integer('year');
            $table->integer('month');
            $table->enum('status', ['success', 'process', 'fail']);
            $table->string('tcode');
            $table->text('parameter');
            $table->string('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sap_sync_logs');
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('m_taglist', function($table) {
            $table->enum('sync_period',['Monthly','Weekly','Daily'])->nullable(true);
            $table->string('sync_time')->default("07:00");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('m_taglist', function($table) {
            $table->dropColumn('sync_time');
            $table->dropColumn('sync_period');
        });
    }
};

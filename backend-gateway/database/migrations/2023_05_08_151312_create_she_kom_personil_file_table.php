<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('she_kom_personil_file', function (Blueprint $table) {
            $table->id();
            $table->integer('id_kompetensi');
            $table->string('no_peg');
            $table->string('nm_pegawai');
            $table->string('nm_sertifikat');
            $table->string('nm_penerbit');
            $table->date('end_date');
            $table->string('file');
            $table->string('created_by');
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('she_kom_personil_file');
    }
};

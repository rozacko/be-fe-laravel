<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_capex', function (Blueprint $table) {
            $table->id();
            $table->string('level', 30);
            $table->string('no_project');
            $table->string('wbs', 60);
            $table->string('description')->nullable();
            $table->string('kode_opco', 30);
            $table->date('tanggal');
            $table->string('tahun', 4);
            $table->string('kode_plant', 30);
            $table->string('inv_reason')->nullable();
            $table->string('rs_cost')->nullable();
            $table->string('pr_respon')->nullable();
            $table->string('npr_respon')->nullable();
            $table->string('pspri')->nullable();
            $table->string('prctr')->nullable();
            $table->decimal('actual', 12, 2)->nullable();
            $table->decimal('budget', 12, 2)->nullable();
            $table->decimal('commit', 12, 2)->nullable();
            $table->decimal('avail', 12, 2)->nullable();
            $table->decimal('rmorp', 12, 2)->nullable();
            $table->decimal('plan_cost', 12, 2)->nullable();
            $table->decimal('assig_cost', 12, 2)->nullable();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_capex');
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_neraca_limbahb3', function (Blueprint $table) {
            $table->id();
            $table->uuid();
            $table->string("plant");
            $table->unsignedInteger("id_jenis_limbah");
            $table->date('tanggal_masuk');
            $table->string('sumber_masuk', 255)->nullable();
            $table->unsignedInteger('jml_ton_masuk')->default(0);
            $table->date('masa_simpan_masuk');
            $table->date('tanggal_keluar');
            $table->string('sumber_keluar');
            $table->unsignedInteger('jml_ton_keluar')->default(0);
            $table->date('masa_simpan_keluar');
            $table->unsignedInteger('sisa_tps');
            $table->string('created_by', 255)->nullable();
            $table->string('updated_by', 255)->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_jenis_limbah')->references('id')->on('m_jenis_limbah_enviro');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_neraca_limbahb3');
    }
};

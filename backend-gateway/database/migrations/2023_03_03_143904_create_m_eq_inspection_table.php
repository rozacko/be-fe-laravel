<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_eq_inspection', function (Blueprint $table) {
            $table->id();
            $table->string('kd_equipment');
            $table->string('nm_equipment');
            $table->integer('area_id');
            $table->integer('plant_id');
            $table->string('ref_sap')->nullable();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_eq_inspection');
    }
};

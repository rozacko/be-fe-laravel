<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_inspeksi_lingkungan', function (Blueprint $table) {
            $table->id();
            $table->uuid();
            $table->string('departemen', 255)->nullable();
            $table->string('area', 255)->nullable();
            $table->string('equipment_no', 255)->nullable();
            $table->string('uraian_temuan', 255)->nullable();
            $table->date('tanggal_inspeksi')->nullable();
            $table->date('tanggal_closing')->nullable();
            $table->string('status', 255)->nullable();
            $table->string('foto_temuan', 255)->nullable();
            $table->string('foto_closing', 255)->nullable();
            $table->string('devisi_area', 255)->nullable();
            $table->string('unit_kerja', 255)->nullable();
            $table->string('evaluasi', 255)->nullable();
            $table->string('created_by', 255)->nullable();
            $table->string('updated_by', 255)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_inspeksi_lingkungan');
    }
};

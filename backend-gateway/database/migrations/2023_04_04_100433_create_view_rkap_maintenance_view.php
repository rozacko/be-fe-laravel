<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('create or replace
        view view_rkap_maintenance as
        select
            trm.id,
            trm.uuid,
            trm.code_opco ,
            trm.tahun,
            trm.jenis_rkap ,
            items.*,
            trm.status,
            trm.created_by ,
            trm.created_at ,
            trm.updated_by ,
            trm.updated_at
        from
            t_rkap_maintenance trm
        left join (
            select
                A.id_rkap_maintenance,
                SUM(nilai_rkap) filter (
                where bulan = 1) jan,
                SUM(nilai_rkap) filter (
                where bulan = 2) feb,
                SUM(nilai_rkap) filter (
                where bulan = 3) mar,
                SUM(nilai_rkap) filter (
                where bulan = 4) apr,
                SUM(nilai_rkap) filter (
                where bulan = 5) mei,
                SUM(nilai_rkap) filter (
                where bulan = 6) jun,
                SUM(nilai_rkap) filter (
                where bulan = 7) jul,
                SUM(nilai_rkap) filter (
                where bulan = 8) ags,
                SUM(nilai_rkap) filter (
                where bulan = 9) sep,
                SUM(nilai_rkap) filter (
                where bulan = 10) okt,
                SUM(nilai_rkap) filter (
                where bulan = 11) nov,
                SUM(nilai_rkap) filter (
                where bulan = 12) des
            from
                (
                select
                    trmi .*
                from
                    t_rkap_maintenance_items trmi
                   ) A
            group by
                A.id_rkap_maintenance) items on
            trm.id = items.id_rkap_maintenance');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('view_rkap_maintenance');
    }
};

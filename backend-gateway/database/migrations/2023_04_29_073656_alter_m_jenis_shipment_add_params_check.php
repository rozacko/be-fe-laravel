<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table("m_jenis_shipment", function(Blueprint $table){
            $table->string('shipment_params_check')->default("and");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table("m_jenis_shipment", function(Blueprint $table){
            $table->dropColumn('shipment_params_check');
        });
    }
};

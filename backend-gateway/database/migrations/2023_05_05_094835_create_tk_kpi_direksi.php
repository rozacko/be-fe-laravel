<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tk_kpi_direksi', function (Blueprint $table) {
            $table->id();
            $table->string('kpi',250);
            $table->string('kpi_group',300);
            $table->string('tahun',4);
            $table->string('bulan',250);
            $table->string('unit',250);
            $table->string('polarisasi',250);
            $table->string('target',250)->nullable();
            $table->string('prognose',250)->nullable();
            $table->string('realisasi',250)->nullable();
            $table->string('bobot',250)->nullable();
            $table->string('nilai',250)->nullable();
            $table->string('nilai_akhir',250)->nullable();
            $table->string('persentase',250)->nullable();
            $table->string('created_by');
            $table->string('updated_by')->nullable(true);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tk_kpi_direksi');
    }
};

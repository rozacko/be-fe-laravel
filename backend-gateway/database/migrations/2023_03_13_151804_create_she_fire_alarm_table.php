<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('she_fire_alarm', function (Blueprint $table) {
            $table->id();
            $table->integer('parameter_id');
            $table->string('bulan');
            $table->string('tahun');
            $table->boolean('dt_m')->default(false);
            $table->boolean('dt_w1')->default(false);
            $table->boolean('dt_w2')->default(false);
            $table->boolean('dt_w3')->default(false);
            $table->boolean('dt_w4')->default(false);
            $table->string('status')->nullable();
            $table->string('keterangan')->nullable();
            $table->string('ccr_tuban')->nullable();
            $table->string('created_by');
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('she_fire_alarm');
    }
};

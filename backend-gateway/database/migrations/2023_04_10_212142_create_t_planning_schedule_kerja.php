<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_planning_schedule_kerja', function (Blueprint $table) {
            $table->id('id_planning_schedule_kerja');
            $table->uuid();
            $table->string('no_pegawai', 30);
            $table->string('nm_pegawai', 100);
            $table->string('spesifikasi', 60);
            $table->string('perusahaan', 100);
            $table->integer('tahun');
            $table->integer('bulan');
            $table->string('tgl_1', 30)->nullable();
            $table->string('tgl_2', 30)->nullable();
            $table->string('tgl_3', 30)->nullable();
            $table->string('tgl_4', 30)->nullable();
            $table->string('tgl_5', 30)->nullable();
            $table->string('tgl_6', 30)->nullable();
            $table->string('tgl_7', 30)->nullable();
            $table->string('tgl_8', 30)->nullable();
            $table->string('tgl_9', 30)->nullable();
            $table->string('tgl_10', 30)->nullable();
            $table->string('tgl_11', 30)->nullable();
            $table->string('tgl_12', 30)->nullable();
            $table->string('tgl_13', 30)->nullable();
            $table->string('tgl_14', 30)->nullable();
            $table->string('tgl_15', 30)->nullable();
            $table->string('tgl_16', 30)->nullable();
            $table->string('tgl_17', 30)->nullable();
            $table->string('tgl_18', 30)->nullable();
            $table->string('tgl_19', 30)->nullable();
            $table->string('tgl_20', 30)->nullable();
            $table->string('tgl_21', 30)->nullable();
            $table->string('tgl_22', 30)->nullable();
            $table->string('tgl_23', 30)->nullable();
            $table->string('tgl_24', 30)->nullable();
            $table->string('tgl_25', 30)->nullable();
            $table->string('tgl_26', 30)->nullable();
            $table->string('tgl_27', 30)->nullable();
            $table->string('tgl_28', 30)->nullable();
            $table->string('tgl_29', 30)->nullable();
            $table->string('tgl_30', 30)->nullable();
            $table->string('tgl_31', 30)->nullable();
            $table->string('created_by', 255)->nullable();
            $table->string('updated_by', 255)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_planning_schedule_kerja');
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('she_sertif_analis', function (Blueprint $table) {
            $table->id();
            $table->string('tahun');
            $table->string('start_bulan');
            $table->string('start_tahun');
            $table->string('end_bulan');
            $table->string('end_tahun');
            $table->string('nm_sertifikat');
            $table->string('file');
            $table->string('created_by');
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('she_sertif_analis');
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table("t_status_sync_shipment", function(Blueprint $table){
            $table->smallInteger('num_tried')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table("t_status_sync_shipment", function(Blueprint $table){
            $table->dropColumn('num_tried');
        });
    }
};

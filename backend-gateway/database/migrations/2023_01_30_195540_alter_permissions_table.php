<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permissions', function (Blueprint $table) {
            $table->string('name')->unique()->change();
            $table->string('unique_code')->unique();
            $table->string('feature_id', 10);
            $table->foreign('feature_id')
                ->references('id')
                ->on('features');
            $table->char('action_id', 2);
            $table->foreign('action_id')
                ->references('id')
                ->on('actions');
            $table->char('status', 1)->default('y');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permissions', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
    }
};

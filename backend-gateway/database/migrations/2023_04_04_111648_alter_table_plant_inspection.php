<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("t_plant_inspection_comment", function(Blueprint $table){
            $table->string('action')->nullable(true);
        });

        Schema::table("t_plant_inspection", function(Blueprint $table){
            $table->string('remark')->nullable(true)->change();
            $table->string('id_plant')->nullable(true)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('t_plant_inspection_comment', 'action'))
        {
            Schema::table('t_plant_inspection_comment', function (Blueprint $table)
            {
                $table->dropColumn('action');
            });
        }
    }
};
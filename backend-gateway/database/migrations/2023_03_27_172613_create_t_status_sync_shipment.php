<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_status_sync_shipment', function (Blueprint $table) {
            $table->id();
            $table->integer('id_api_shipment');
            $table->date('tgl_penarikan')->default(DB::raw("CURRENT_DATE - 1"));
            $table->boolean('status')->default(false);
            $table->string('created_by');
            $table->string('updated_by')->nullable(true);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_status_sync_shipment');
    }
};

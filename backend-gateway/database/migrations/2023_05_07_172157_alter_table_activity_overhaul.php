<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('t_overhaul_activity', function(Blueprint $table){
            $table->string('order_no')->nullable(true);
            $table->string('jenis_pekerjaan')->default('PATCHJOB');
            $table->string('judul_kontrak',1000)->nullable(true);
            $table->string('budget_category')->default('Opex');
            $table->date('tgl_masuk')->nullable(true);
            $table->float('total_manpower')->default('0.00');
            $table->float('total_consumable')->default('0.00');
            $table->float('total_tool')->default('0.00');
            $table->date('order_date')->default(DB::raw("CURRENT_DATE"));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('t_overhaul_activity', function(Blueprint $table){
            $table->dropColumn('order_no');
            $table->dropColumn('jenis_pekerjaan');//->default('PATCHJOB');
            $table->dropColumn('judul_kontrak');
            $table->dropColumn('tgl_masuk');
            $table->dropColumn('total_manpower');//->default('0.00');
            $table->dropColumn('total_consumable');//->default('0.00');
            $table->dropColumn('total_tool');//->default('0.00');
            $table->dropColumn('order_date');//->default(DB::raw("CURRENT_DATE"));
        });
    }
};

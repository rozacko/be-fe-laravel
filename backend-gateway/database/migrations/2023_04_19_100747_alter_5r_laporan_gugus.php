<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('t_laporan_gugus', 'r2'))
        {
            Schema::table("t_laporan_gugus", function(Blueprint $table){
                $table->decimal('r2',8,2)->nullable();
            });
        }
        if (!Schema::hasColumn('t_laporan_gugus', 'r3'))
        {
            Schema::table("t_laporan_gugus", function(Blueprint $table){
                $table->decimal('r3',8,2)->nullable();
            });
        }
        if (!Schema::hasColumn('t_laporan_gugus', 'r4'))
        {
            Schema::table("t_laporan_gugus", function(Blueprint $table){
                $table->decimal('r4',8,2)->nullable();
            });
        }
        if (!Schema::hasColumn('t_laporan_gugus', 'r5'))
        {
            Schema::table("t_laporan_gugus", function(Blueprint $table){
                $table->decimal('r5',8,2)->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::hasColumn('t_laporan_gugus', 'r2'))
        {
            Schema::table("t_laporan_gugus", function(Blueprint $table){
                $table->dropColumn('r2');
            });
        }
        if (!Schema::hasColumn('t_laporan_gugus', 'r3'))
        {
            Schema::table("t_laporan_gugus", function(Blueprint $table){
                $table->dropColumn('r3');
            });
        }
        if (!Schema::hasColumn('t_laporan_gugus', 'r4'))
        {
            Schema::table("t_laporan_gugus", function(Blueprint $table){
                $table->dropColumn('r4');
            });
        }
        if (!Schema::hasColumn('t_laporan_gugus', 'r5'))
        {
            Schema::table("t_laporan_gugus", function(Blueprint $table){
                $table->dropColumn('r5');
            });
        }
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_overhaul_activity', function (Blueprint $table) {
            $table->id();
            $table->integer('id_plant');
            $table->integer('id_area');
            $table->string('activity_name');
            $table->date('start_date')->default(DB::raw('CURRENT_DATE'));
            $table->date('end_date')->default(DB::raw('CURRENT_DATE+25'));
            $table->enum('activity_category',['Major','Minor'])->default('Major');
            $table->string('created_by');
            $table->string('updated_by')->nullable(true);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_overhaul_activity');
    }
};

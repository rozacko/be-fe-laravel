<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('
        create or replace view view_prognose_performance  AS select tpp.id, tpp.uuid, mp2.nm_plant as plant_name ,tpp.tahun ,tpp.id_parameter,mp.name as paramter_name , items.*,tpp.created_by ,tpp.created_at ,tpp.updated_by ,tpp.updated_at,tpp.id_plant from 
            t_prognose_performance tpp 
            left join m_parameter mp ON mp.id = tpp.id_parameter 
            left join m_plant mp2 on mp2.id = tpp.id_plant 
            left join (
            SELECT A.id_prognose_performance,
                SUM(nilai_rkap) FILTER (WHERE bulan =  1) jan,
                SUM(nilai_rkap) FILTER (WHERE bulan =  2) feb,
                SUM(nilai_rkap) FILTER (WHERE bulan =  3) mar,
                SUM(nilai_rkap) FILTER (WHERE bulan =  4) apr,
                SUM(nilai_rkap) FILTER (WHERE bulan =  5) mei,
                SUM(nilai_rkap) FILTER (WHERE bulan =  6) jun,
                SUM(nilai_rkap) FILTER (WHERE bulan =  7) jul,
                SUM(nilai_rkap) FILTER (WHERE bulan =  8) ags,
                SUM(nilai_rkap) FILTER (WHERE bulan =  9) sep,
                SUM(nilai_rkap) FILTER (WHERE bulan =  10) okt,
                SUM(nilai_rkap) FILTER (WHERE bulan =  11) nov,
                SUM(nilai_rkap) FILTER (WHERE bulan =  12) des
                FROM (SELECT tppi .*          
                        FROM t_prognose_performance_items tppi  
                    ) A
                GROUP BY A.id_prognose_performance) items on tpp.id=items.id_prognose_performance
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('view_prognose_performance_view');
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('she_unsafe_action', function (Blueprint $table) {
            $table->id();
            $table->string('bulan');
            $table->string('tahun');
            $table->string('nm_perusahaan');
            $table->longText('uraian_temuan');
            $table->longText('lokasi_kejadian');
            $table->date('tgl_temuan');
            $table->double('jml_temuan');
            $table->string('pengawas')->nullable();
            $table->longText('identifikasi');
            $table->longText('tindakan');
            $table->longText('keterangan')->nullable();
            $table->string('file')->nullable();
            $table->string('file_gdrive')->nullable();
            $table->string('status');
            $table->string('created_by');
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('she_unsafe_action');
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('t_dokumen_lingkungan')) {
            Schema::table('t_dokumen_lingkungan', function (Blueprint $table) {
                if (!Schema::hasColumn('t_dokumen_lingkungan', 'tahun')) {
                    $table->string('tahun')->nullable();
                }
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_laporan_gugus', function (Blueprint $table) {
            $table->bigIncrements('id_laporan_gugus');
            $table->string('bulan',2)->nullable();
            $table->string('tahun',4)->nullable();
            $table->string('fasilitator',250)->nullable();
            $table->string('gugus',250)->nullable();
            $table->string('sga',250)->nullable();
            $table->decimal('commitment_management',8,2)->nullable();
            $table->decimal('papan_control',8,2)->nullable();
            $table->decimal('she',8,2)->nullable();
            $table->decimal('5r',8,2)->nullable();
            $table->decimal('autonomous_maintenance',8,2)->nullable();
            $table->decimal('planed_maintenance',8,2)->nullable();
            $table->decimal('focused_improvement',8,2)->nullable();
            $table->decimal('target',8,2)->nullable();
            $table->decimal('nilai_SGA',8,2)->nullable();
            $table->decimal('nilai_gugus',8,2)->nullable();
            $table->decimal('nilai_fasilitator',8,2)->nullable();
            $table->string('created_by');
            $table->string('updated_by')->nullable(true);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_laporan_gugus');
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_sparepart_jasa', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid')->default(DB::raw('public.uuid_generate_v4()'));
            $table->date('tanggal');
            $table->string('pr');
            $table->string('kode_plant')->nullable();
            $table->string('nama_plant')->nullable();
            $table->string('pekerjaan');
            $table->string('estimator');
            $table->float('nilai')->nullable();
            $table->date('engineering_tanggal')->nullable();
            $table->string('engineering_status')->nullable();
            $table->date('rfq_tanggal')->nullable();
            $table->string('rfq_status')->nullable();
            $table->string('po_tanggal')->nullable();
            $table->string('po_status')->nullable();
            $table->string('status_pekerjaan')->nullable();
            $table->string('vendor')->nullable();
            $table->float('kontrak')->nullable();
            $table->date('start_date')->nullable();
            $table->date('finish_date')->nullable();
            $table->date('pr_ke_estimator')->nullable();
            $table->date('app_p_sawab')->nullable();
            $table->date('app_p_didit')->nullable();
            $table->date('app_p_zaini')->nullable();
            $table->string('nama_estimator')->nullable();
            $table->string('pengadaan')->nullable();
            $table->string('user')->nullable();
            $table->string('keterangan')->nullable();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->string('deleted_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_sparepart_jasa');
    }
};

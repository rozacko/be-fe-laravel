<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_jenis_limbah', function (Blueprint $table) {
            $table->id();
            $table->uuid();
            $table->unsignedBigInteger('plant_id');
            $table->string('jenis_limbah_b3', 255)->nullable();
            $table->string('sumber', 255)->nullable();
            $table->string('created_by', 255)->nullable();
            $table->string('updated_by', 255)->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('plant_id')
                ->references('id')
                ->on('m_plant')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_jenis_limbah');
    }
};

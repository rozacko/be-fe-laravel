<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('she_izin_lingkungan', function (Blueprint $table) {
            $table->id();
            $table->string('tahun');                 
            $table->string('nm_perizinan');
            $table->string('sub_perizinan');
            $table->string('p_tuban',1);
            $table->string('p_gresik',1);
            $table->string('pelabuhan_tuban',1);
            $table->string('pelabuhan_gresik',1);
            $table->string('tambang_tuban',1);
            $table->longText('keterangan');
            $table->string('created_by');
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('she_izin_lingkungan');
    }
};

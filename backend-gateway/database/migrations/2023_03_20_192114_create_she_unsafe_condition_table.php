<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('she_unsafe_condition', function (Blueprint $table) {
            $table->id();
            $table->string('bulan');
            $table->string('tahun');
            $table->string('area');
            $table->string('tpm_sga');
            $table->string('no_equipment');
            $table->longText('lokasi_kerja');
            $table->longText('uraian_temuan');
            $table->longText('saran');
            $table->string('pic');
            $table->string('unit_kerja');
            $table->string('id_trans_mso')->nullable();
            $table->string('no_notif')->nullable();
            $table->date('tgl_temuan');
            $table->date('tgl_target')->nullable();
            $table->date('tgl_closing')->nullable();
            $table->boolean('cek_1')->default(false);
            $table->boolean('cek_2')->default(false);
            $table->string('file_1')->nullable();
            $table->string('file_gdrive_1')->nullable();
            $table->string('file_2')->nullable();
            $table->string('file_gdrive_2')->nullable();
            $table->string('status');
            $table->longText('keterangan')->nullable();
            $table->string('created_by');
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('she_unsafe_condition');
    }
};

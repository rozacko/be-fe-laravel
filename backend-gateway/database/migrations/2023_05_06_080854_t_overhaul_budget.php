<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('t_overhaul_budget', function(Blueprint $table){
            $table->id();
            $table->uuid('uuid')->default(DB::raw('public.uuid_generate_v4()'));
            $table->integer('id_plant');
            $table->integer('tahun');
            $table->string('unit_kerja');
            $table->float('budget_capex')->nullable(true);
            $table->float('budget_opex')->nullable(true);
            $table->string('created_by')->nullable(true);
            $table->string('updated_by')->nullable(true);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('t_overhaul_budget');
    }
};

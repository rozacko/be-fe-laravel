<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('create or replace
        view view_rkap_prognose_maintenance as
        select
            trpm.id,
            trpm.uuid,
            trpm.code_opco ,
            trpm.jenis_rkap ,
            trpm.tahun ,
            items.*,
            trpm.status,
            trpm.created_by ,
            trpm.created_at ,
            trpm.updated_by ,
            trpm.updated_at
        from
            t_rkap_prognose_maintenance trpm
        left join (
            select
                A.id_rkap_prognose_maintenance,
                SUM(nilai_rkap) filter (
                where bulan = 1) jan,
                SUM(nilai_rkap) filter (
                where bulan = 2) feb,
                SUM(nilai_rkap) filter (
                where bulan = 3) mar,
                SUM(nilai_rkap) filter (
                where bulan = 4) apr,
                SUM(nilai_rkap) filter (
                where bulan = 5) mei,
                SUM(nilai_rkap) filter (
                where bulan = 6) jun,
                SUM(nilai_rkap) filter (
                where bulan = 7) jul,
                SUM(nilai_rkap) filter (
                where bulan = 8) ags,
                SUM(nilai_rkap) filter (
                where bulan = 9) sep,
                SUM(nilai_rkap) filter (
                where bulan = 10) okt,
                SUM(nilai_rkap) filter (
                where bulan = 11) nov,
                SUM(nilai_rkap) filter (
                where bulan = 12) des
            from
                (
                select
                    trpmi .*
                from
                    t_rkap_prognose_maintenance_items trpmi
                   ) A
            group by
                A.id_rkap_prognose_maintenance) items on
            trpm.id = items.id_rkap_prognose_maintenance');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('view_rkap_prognose_maintenance');
    }
};

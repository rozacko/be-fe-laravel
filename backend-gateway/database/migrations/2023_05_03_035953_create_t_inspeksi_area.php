<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('t_inspeksi_area', function (Blueprint $table) {
            $table->id();
            $table->uuid();
            $table->unsignedBigInteger('area_id');
            $table->string('nm_area', 255)->nullable();
            $table->string('equipment', 255)->nullable();
            $table->string('uraian_temuan', 255)->nullable();
            $table->date('tanggal')->nullable()->default(NULL);
            $table->string('foto_temuan', 255)->nullable();
            $table->string('foto_closing', 255)->nullable();
            $table->string('divisi_area', 255)->nullable();
            $table->string('unit_kerja', 255)->nullable();
            $table->string('evaluasi', 255)->nullable();
            $table->string('status', 255)->nullable();
            $table->string('created_by', 255)->nullable();
            $table->string('updated_by', 255)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_inspeksi_area');
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_opc_daily', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid')->default(DB::raw('public.uuid_generate_v4()'));
            $table->integer('id_opc');
            $table->timestamp('sync_date')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->double('tag_value');
            $table->double('msdr_value')->nullable(true);
            $table->double('harpros_value')->nullable(true);
            $table->string('created_by');
            $table->string('updated_by')->nullable(true);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_opc_daily');
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('t_realisasi_capex', function (Blueprint $table) {
            $table->id();
            $table->string('no_project');
            $table->string('no_wbs');
            $table->string('item_capex')->nullable(true);
            $table->string('unit_kerja')->nullable(true);
            $table->string('pm')->nullable(true);
            $table->string('tipe')->nullable(true);
            $table->string('kai')->nullable(true);
            $table->string('op_dev')->nullable(true);
            $table->float('nilai_investasi')->nullable(true);
            $table->float('committed')->nullable(true)->default(0);
            $table->float('pelampauan')->nullable(true)->default(0);
            $table->float('real_spending')->nullable(true)->default(0);
            $table->string('network')->nullable(true);
            $table->string('reservasi')->nullable(true);
            $table->string('no_pr')->nullable(true);
            $table->float('nilai_pr')->nullable(true)->default(0);
            $table->string('no_po')->nullable(true);
            $table->date('doc_date')->nullable(true);
            $table->date('del_date')->nullable(true);
            $table->float('nilai_po')->nullable(true)->default(0);
            $table->date('tgl_gr')->nullable(true);
            $table->float('spending_gr')->nullable(true)->default(0);
            $table->float('progres_fisik')->nullable(true)->default(0);
            $table->float('progres_report_terakhir')->nullable(true)->default(0);
            $table->float('nilai_auc')->nullable(true)->default(0);
            $table->string('status_bast')->nullable(true);
            $table->string('status_wbs')->nullable(true);
            $table->string('keterangan')->nullable(true);
            $table->string('created_by');
            $table->string('updated_by')->nullable(true);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('t_realisasi_capex');
    }
};
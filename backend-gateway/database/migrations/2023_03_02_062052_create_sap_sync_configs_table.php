<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sap_sync_configs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('type');
            $table->string('tcode');
            $table->text('parameter');
            $table->enum('schedule', ['minutely', 'everyfiveminute', 'hourly', 'daily', 'monthly']);
            $table->integer('at_date')->nullable();
            $table->time('at_time', 0)->nullable();
            $table->enum('status', ['active', 'non_active']);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sap_sync_configs');
    }
};

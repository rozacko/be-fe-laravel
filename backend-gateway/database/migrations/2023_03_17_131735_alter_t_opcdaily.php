<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::rename("t_opc_daily","t_penarikan_opc");
        Schema::table("t_penarikan_opc", function(Blueprint $table){
            $table->renameColumn("sync_date","waktu_penarikan");
            $table->string('tag_value')->nullable(true)->change();
            $table->string('msdr_value')->change();
            $table->string('harpros_value')->change();
            $table->boolean("status")->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};

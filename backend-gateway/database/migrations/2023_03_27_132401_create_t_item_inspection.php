<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_item_inspection', function (Blueprint $table) {
            $table->id();
            $table->integer('id_inspection');
            $table->date('tgl_inspection');
            $table->integer('id_equipment');
            $table->string('kode_equipment');
            $table->string('nama_equipment');
            $table->integer('id_area');
            $table->string('nama_area');
            $table->integer('id_kondisi');
            $table->string('kode_kondisi');
            $table->string('remark');
            $table->string('created_by');
            $table->string('updated_by')->nullable(true);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_item_inspection');
    }
};

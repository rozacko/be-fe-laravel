<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_laporan_tpm', function (Blueprint $table) {
            $table->bigIncrements('id_laporan_tpm');
            $table->string('deskripsi',60)->nullable();
            $table->string('parameter',30)->nullable();
            $table->string('tahun',4)->nullable();
            $table->decimal('jan',8,2)->nullable();
            $table->decimal('feb',8,2)->nullable();
            $table->decimal('mar',8,2)->nullable();
            $table->decimal('q1',8,2)->nullable();
            $table->decimal('apr',8,2)->nullable();
            $table->decimal('mei',8,2)->nullable();
            $table->decimal('jun',8,2)->nullable();
            $table->decimal('q2',8,2)->nullable();
            $table->decimal('s1',8,2)->nullable();
            $table->decimal('jul',8,2)->nullable();
            $table->decimal('aug',8,2)->nullable();
            $table->decimal('sep',8,2)->nullable();
            $table->decimal('q3',8,2)->nullable();
            $table->decimal('okt',8,2)->nullable();
            $table->decimal('nov',8,2)->nullable();
            $table->decimal('des',8,2)->nullable();
            $table->decimal('q4',8,2)->nullable();
            $table->decimal('s2',8,2)->nullable();
            $table->decimal('nilai_akhir',8,2)->nullable();
            $table->string('created_by');
            $table->string('updated_by')->nullable(true);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_laporan_tpm');
    }
};

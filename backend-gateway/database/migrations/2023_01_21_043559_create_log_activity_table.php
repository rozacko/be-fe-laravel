<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_activities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid')->default(DB::raw('public.uuid_generate_v4()'));
            $table->text('url');
            $table->string('name')->nullable();
            $table->string('method');
            $table->string('ip');
            $table->string('agent')->nullable();
            $table->bigInteger('user_id')->nullable();
            $table->string('http_code')->nullable();
            $table->string('status')->nullable();
            $table->string('message')->nullable();
            $table->string('errors')->nullable();
            $table->string('data')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_activities');
    }
};

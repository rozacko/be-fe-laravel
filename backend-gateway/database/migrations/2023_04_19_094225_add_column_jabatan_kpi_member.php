<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('t_laporan_kpi_member', 'jabatan'))
        {
            Schema::table("t_laporan_kpi_member", function(Blueprint $table){
                $table->string('jabatan',250)->nullable(true);
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('t_laporan_kpi_member', 'jabatan'))
        {
            Schema::table('t_laporan_kpi_member', function (Blueprint $table)
            {
                $table->dropColumn('jabatan');
            });
        }
    }
};

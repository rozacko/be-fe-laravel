<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_cogm_flash_report', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid')->default(DB::raw('public.uuid_generate_v4()'));
            $table->integer('id_plant')->nullable(true);
            $table->year('tahun');
            $table->integer('bulan');
            $table->char('id_parameter',4);
            $table->foreign('id_parameter')
                ->references('id')
                ->on('m_parameter')
                ->onUpdate('cascade');
            $table->float('nilai_rkap');
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_cogm_flash_report');
    }
};

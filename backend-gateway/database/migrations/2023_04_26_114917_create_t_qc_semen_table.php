<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_qc_semen', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid')->default(DB::raw('public.uuid_generate_v4()'));
            $table->string('quality');
            $table->string('type');
            $table->integer('plant_id');
            $table->date('tanggal');
            $table->integer('jam');
            $table->string('so3');
            $table->string('fcao');
            $table->string('blaine');
            $table->string('res45');
            $table->string('loi');
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_qc_semen');
    }
};

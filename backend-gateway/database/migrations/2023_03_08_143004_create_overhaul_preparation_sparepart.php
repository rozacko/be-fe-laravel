<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_overhaul_preparation_sparepart', function (Blueprint $table) {
            $table->id();
            $table->integer('id_plant');
            $table->integer('id_unit_kerja');
            $table->string('kode_material');
            $table->string('nama_material');
            $table->enum('category',['Rutin','Non-Rutin'])->default('Rutin');
            $table->enum('status',['N/A','PO','PR','READY','RESERVASI'])->default('N/A');
            $table->string('reservasi_no')->nullable(true);
            $table->string('pr_no')->nullable(true);
            $table->string('po_no')->nullable(true);
            $table->date('po_delivery_date')->nullable(true);
            $table->text('notes')->nullable(true);
            $table->string('created_by');
            $table->string('updated_by')->nullable(true);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_overhaul_preparation_sparepart');
    }
};

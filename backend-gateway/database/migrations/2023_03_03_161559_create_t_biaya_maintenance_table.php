<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_biaya_maintenance', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('no_dokumen', 30);
            $table->string('kode_opco', 30);
            $table->string('row', 30)->nullable();
            $table->string('cost_center', 30)->nullable();
            $table->string('cost_center_name', 100)->nullable();
            $table->string('cost_element', 30)->nullable();
            $table->string('cost_element_name', 100)->nullable();
            $table->date('tanggal')->nullable()->default(NULL);
            $table->float('biaya', 8, 5);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_biaya_maintenance');
    }
};

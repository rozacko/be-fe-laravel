<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('t_overhaul_preparation_sparepart', function(Blueprint $table){
            $table->float('total_price')->default(0.00);
            $table->string('budget')->default('Opex');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('t_overhaul_preparation_sparepart', function(Blueprint $table){
            $table->dropColumn('total_price');
            $table->dropColumn('budget');
        });
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_presensi_pegawai', function (Blueprint $table) {
            $table->id('id_presensi_pegawai');
            $table->uuid();
            $table->string('no_pegawai', 30);
            $table->string('nama_pegawai', 100);
            $table->time('time')->nullable();
            $table->string('state', 30)->nullable();
            $table->string('new_state', 30)->nullable();
            $table->string('exception', 100)->nullable();
            $table->string('preference', 60)->nullable();
            $table->date('tanggal')->nullable();
            $table->string('event', 255)->nullable();
            $table->string('created_by', 255)->nullable();
            $table->string('updated_by', 255)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_presensi_pegawai');
    }
};

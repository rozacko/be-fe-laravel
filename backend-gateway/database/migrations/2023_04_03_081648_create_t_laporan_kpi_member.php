<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_laporan_kpi_member', function (Blueprint $table) {
            $table->bigIncrements('id_laporan_kpi');
            $table->string('tahun',4)->nullable();
            $table->string('no_pegawai',30)->nullable();
            $table->string('nama_pegawai',100)->nullable();
            $table->string('nama_organisasi',250)->nullable();
            $table->string('unit_kerja',250)->nullable();
            $table->decimal('target_point',8,2)->nullable();
            $table->decimal('q1',8,2)->nullable();
            $table->decimal('q2',8,2)->nullable();
            $table->decimal('q3',8,2)->nullable();
            $table->decimal('q4',8,2)->nullable();
            $table->decimal('s1',8,2)->nullable();
            $table->decimal('s2',8,2)->nullable();
            $table->decimal('nilai_akhir',8,2)->nullable();
            $table->string('created_by');
            $table->string('updated_by')->nullable(true);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_laporan_kpi_member');
    }
};

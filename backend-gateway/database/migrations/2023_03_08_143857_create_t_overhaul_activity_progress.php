<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_overhaul_activity_progress', function (Blueprint $table) {
            $table->id();
            $table->integer('id_activity');
            $table->date('progress_date')->default(DB::raw("CURRENT_DATE"));
            $table->double('plan_cumulative')->default('0.00');
            $table->double('real_cumulative')->default('0.00');
            $table->string('created_by');
            $table->string('updated_by')->nullable(true);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_overhaul_activity_progress');
    }
};

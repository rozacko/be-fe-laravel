<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_penarikan_shipment', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid')->default(DB::raw('public.uuid_generate_v4()'));
            $table->integer('id_jenis_shipment');
            $table->integer('id_plant')->nullable(true);
            $table->date('shipment_date')->default(DB::raw('CURRENT_DATE'));
            $table->string('data_value')->nullable(true);
            $table->string('msdr_value')->nullable(true);
            $table->string('harpros_value')->nullable(true);
            $table->boolean('status')->default(true);
            $table->string('created_by');
            $table->string('updated_by')->nullable(true);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_penarikan_shipment');
    }
};

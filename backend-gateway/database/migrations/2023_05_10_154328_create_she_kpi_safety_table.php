<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('she_kpi_safety', function (Blueprint $table) {
            $table->id();
            $table->string('tahun');
            $table->float('kons1');
            $table->float('kons2');
            $table->float('target1');
            $table->float('target2');
            $table->string('created_by');
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('she_kpi_safety');
    }
};

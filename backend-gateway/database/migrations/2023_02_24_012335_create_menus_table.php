<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid')->default(DB::raw('public.uuid_generate_v4()'));
            $table->string('name');
            $table->string('description')->nullable();
            $table->BigInteger('permission_id');
            $table->string('url');
            $table->integer('order_no');
            $table->string('icon')->nullable();
            $table->integer('parent_id')->default(0);
            $table->char('status', 1)->default('n');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
};

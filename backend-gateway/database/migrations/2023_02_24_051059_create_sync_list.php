<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_taglist', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid')->default(DB::raw('public.uuid_generate_v4()'));
            $table->integer('id_equipment');
            $table->integer('id_plant');
            $table->integer('id_api');
            $table->string('tag_name');
            $table->string('created_by');
            $table->string('updated_by')->nullable(true);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_taglist');
    }
};

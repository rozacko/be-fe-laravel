<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_capex_item', function (Blueprint $table) {
            $table->id();
            $table->string('no_project');
            $table->string('no_wbs');
            $table->string('nama_capex')->nullable(true);
            $table->string('tipe')->nullable(true);
            $table->string('jenis')->nullable(true);
            $table->float('nilai')->nullable(true);
            $table->string('inisiator_seksi')->nullable(true);
            $table->string('inisiator_biro')->nullable(true);
            $table->string('inisiator_dept')->nullable(true);
            $table->float('realisasi_engineering')->nullable(true)->default(0);
            $table->float('realisasi_procurement')->nullable(true)->default(0);
            $table->float('realisasi_delivery')->nullable(true)->default(0);
            $table->float('realisasi_closing')->nullable(true)->default(0);
            $table->string('progress_status')->nullable(true);
            $table->string('keterangan')->nullable(true);
            $table->string('created_by');
            $table->string('updated_by')->nullable(true);
            $table->softDeletes();
            $table->timestamps();





        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_capex_item');
    }
};
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_notifikasi', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid')->default(DB::raw('public.uuid_generate_v4()'));
            $table->date('notif_date');
            $table->time('notif_time');
            $table->string('system_status');
            $table->string('notification');
            $table->string('order')->nullable();
            $table->string('functional_loc');
            $table->string('description');
            $table->string('priority');
            $table->string('notification_type');
            $table->string('reported_by');
            $table->string('created_user')->nullable();
            $table->string('changed_user')->nullable();
            $table->string('main_work_ctr')->nullable();
            $table->string('planner_group')->nullable();
            $table->string('plant_section')->nullable();
            $table->string('main_plant');
            $table->string('equipment');
            $table->string('cost_center');
            $table->date('reference_date');
            $table->date('created_on');
            $table->date('today');
            $table->string('umur_notif');
            $table->string('notif_aging');
            $table->string('description_standar');
            $table->string('notif_convert_to_order');
            $table->string('notif_close');
            $table->string('klasifikasi_uk');
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->string('deleted_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_notifikasi');
    }
};

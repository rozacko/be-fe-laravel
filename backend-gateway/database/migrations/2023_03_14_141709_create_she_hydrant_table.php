<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('she_hydrant', function (Blueprint $table) {
            $table->id();
            $table->string('bulan');
            $table->string('tahun');
            $table->string('main_pump');
            $table->string('lokasi');
            $table->string('no_pilar');
            $table->string('cek');
            $table->string('p_body');
            $table->longText('p_ket')->nullable();
            $table->string('p_status');
            $table->string('v_kiri');
            $table->string('v_atas');
            $table->string('v_kanan');
            $table->longText('v_ket')->nullable();
            $table->string('v_status');
            $table->string('k_kiri');
            $table->string('k_kanan');
            $table->longText('k_ket')->nullable();
            $table->string('k_status');
            $table->string('tk_kiri');
            $table->string('tk_kanan');
            $table->longText('tk_ket')->nullable();
            $table->string('tk_status')->nullable();
            $table->string('b_casing');
            $table->string('b_hose');
            $table->string('b_nozle');
            $table->string('b_kunci');
            $table->longText('b_ket')->nullable();
            $table->double('press_bar')->nullable();
            $table->string('pb_ket')->nullable();
            $table->string('status_kondisi')->nullable();
            $table->string('created_by');
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('she_hydrant');
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('she_accident_report', function (Blueprint $table) {
            $table->id();
            $table->date('tgl_doc');            
            $table->longText('accident');
            $table->longText('location');
            $table->longText('person_status');
            $table->longText('investigation_team');
            $table->string('report_prepared');
            $table->string('report_reviewed');
            $table->string('report_approved');
            $table->string('file');
            $table->string('status');
            $table->string('created_by');
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('she_accident_report');
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('m_opcapi', function(Blueprint $table){
            $table->string('method')->default('POST');
            $table->text('params')->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('m_opcapi',function(Blueprint $table){
           $table->dropColumn(['method','params']); 
        });
    }
};

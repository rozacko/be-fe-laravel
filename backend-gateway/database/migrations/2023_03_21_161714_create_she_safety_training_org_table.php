<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('she_safety_training_org', function (Blueprint $table) {
            $table->id();
            $table->integer('id_training');
            $table->string('no_pegawai');
            $table->string('nm_pegawai');
            $table->string('departement');
            $table->string('unit_kerja');
            $table->string('band');
            $table->double('jam_pelatihan');
            $table->string('created_by');
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('she_safety_training_org');
    }
};

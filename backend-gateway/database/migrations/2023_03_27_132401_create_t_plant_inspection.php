<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_plant_inspection', function (Blueprint $table) {
            $table->id();
            $table->integer('id_plant');
            $table->string('kode_inspection');
            $table->date('tgl_inspection');
            $table->string('desc_inspection');
            $table->string('kode_opco');
            $table->string('kode_plant');
            $table->string('status');
            $table->integer('id_area');
            $table->string('desc_area');
            $table->string('remark');
            $table->string('created_by');
            $table->string('updated_by')->nullable(true);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_plant_inspection');
    }
};

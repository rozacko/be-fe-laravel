<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('she_kpi_proper', function (Blueprint $table) {
            $table->id();
            $table->string('tahun');
            $table->string('kebijakan');
            $table->string('satuan')->nullable();
            $table->float('target');
            $table->float('realisasi');
            $table->float('kpi');
            $table->string('created_by');
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('she_kpi_proper');
    }
};

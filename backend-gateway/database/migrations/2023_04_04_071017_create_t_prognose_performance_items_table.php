<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_prognose_performance_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_prognose_performance');
            $table->foreign('id_prognose_performance')
                ->references('id')
                ->on('t_prognose_performance')
                ->onUpdate('cascade');
            $table->integer('bulan');
            $table->float('nilai_rkap');
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_prognose_performance_items');
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_order_task', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid')->default(DB::raw('public.uuid_generate_v4()'));
            $table->string('order_type')->nullable();
            $table->string('order')->nullable();
            $table->string('description')->nullable();
            $table->string('system_status')->nullable();
            $table->string('equipment')->nullable();
            $table->string('equipment_description')->nullable();
            $table->float('total_act_cost')->nullable();
            $table->date('tanggal_actual')->nullable();
            $table->date('created_on');
            $table->string('entered_by')->nullable();
            $table->string('planner_group')->nullable();
            $table->string('plant_section');
            $table->string('res_cost_center');
            $table->string('planning_plant');
            $table->string('functional_loc');
            $table->string('cost_center');
            $table->string('user_status');
            $table->date('basic_start_date');
            $table->date('basic_fin_date');
            $table->string('status_order');
            $table->string('reason');
            $table->string('description_uk');
            $table->string('sub_area_proses');
            $table->string('plant');
            $table->string('area_proses');
            $table->integer('bulan');
            $table->year('tahun');
            $table->string('kode_area_proses');
            $table->string('order_confirmation')->nullable();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->string('deleted_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_order_task');
    }
};

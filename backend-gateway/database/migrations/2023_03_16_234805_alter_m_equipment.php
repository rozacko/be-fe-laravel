<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::rename("m_equipment", "m_jenis_opc");
        Schema::table("m_jenis_opc", function(Blueprint $table){
            $table->renameColumn('equipment_name', 'jenis_opc');
            $table->renameColumn('id_category','id_area');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};

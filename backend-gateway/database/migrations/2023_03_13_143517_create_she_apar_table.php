<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('she_apar', function (Blueprint $table) {
            $table->id();
            $table->string('bulan');
            $table->string('tahun');
            $table->string('area');
            $table->string('jenis_apar');
            $table->string('unit_kerja');
            $table->string('lokasi');
            $table->string('map_baru')->default(false);
            $table->string('clamp')->default(false);
            $table->double('press_bar');
            $table->boolean('hose')->default(false);
            $table->boolean('segel')->default(false);
            $table->boolean('sapot')->default(false);
            $table->double('berat');
            $table->string('bulan_aktf');
            $table->string('tahun_aktf');
            $table->string('status');
            $table->string('file')->nullable();
            $table->string('file_gdrive')->nullable();       
            $table->longText('keterangan')->nullable();
            $table->string('created_by');
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('she_apar');
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("ALTER TABLE t_overhaul_preparation_sparepart DROP CONSTRAINT t_overhaul_preparation_sparepart_status_check");
        Schema::table('t_overhaul_preparation_sparepart', function(Blueprint $table){
            $table->string('ovh_year')->default(DB::raw("extract('year' from current_date)::varchar"));   
            $table->dropColumn('id_unit_kerja');        
            $table->string('nm_unit_kerja')->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('t_overhaul_preparation_sparepart', function(Blueprint $table){
            $table->dropColumn('ovh_year');  
            $table->integer('id_unit_kerja')->nullable(true);    
            $table->dropColumn('nm_unit_kerja');     
        });
    }
};

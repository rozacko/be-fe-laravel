<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('she_matrik_ling', function (Blueprint $table) {
            $table->id();
            $table->string('tahun');
            $table->string('jenis');
            $table->string('nomor');
            $table->longText('peraturan');
            $table->string('emisi_udara',1)->nullable();
            $table->string('pembuangan_air',1)->nullable();
            $table->string('pembuangan_tanah',1)->nullable();
            $table->string('sda',1)->nullable();
            $table->string('penggunaan_energi',1)->nullable();
            $table->string('pancaran_energi',1)->nullable();
            $table->string('limbah',1)->nullable();
            $table->string('atribut_fisik',1)->nullable();
            $table->string('k3',1)->nullable();
            $table->string('taat',1)->nullable();
            $table->longText('keterangan')->nullable();
            $table->string('created_by');
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('she_matrik_ling');
    }
};

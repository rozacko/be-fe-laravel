<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_overhaul_preparation_mptool', function (Blueprint $table) {
            $table->id();
            $table->integer('id_plant');
            $table->integer('id_area');
            $table->string('order_no');
            $table->string('activity_name');
            $table->enum('jenis_pekerjaan',['PATCHJOB','NON-OVH'])->default('PATCHJOB');
            $table->integer('id_unitkerja');
            $table->string('judul_kontrak',1000);
            $table->date('tgl_masuk')->nullable(true);
            $table->double('total_manpower')->default('0.00');
            $table->double('total_consumable')->default('0.00');
            $table->double('total_tool')->default('0.00');
            $table->date('order_date')->default(DB::raw("CURRENT_DATE"));
            $table->string('created_by');
            $table->string('updated_by')->nullable(true);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_overhaul_preparation_mptool');
    }
};

<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Ramsey\Uuid\Uuid;
use App\Models\MAreaEnviro;

class MAreaEnviroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['nama_area' => 'Gresik', "created_by"=>"system","updated_by"=>"system"],
            ['nama_area' => 'Tuban', "created_by"=>"system","updated_by"=>"system"],
            ['nama_area' => 'Ciwandan', "created_by"=>"system","updated_by"=>"system"],
            ['nama_area' => 'Banjarmasin', "created_by"=>"system","updated_by"=>"system"],
            ['nama_area' => 'Sorong', "created_by"=>"system","updated_by"=>"system"],
            ['nama_area' => 'Balikpapan', "created_by"=>"system","updated_by"=>"system"],
            ['nama_area' => 'Banyuwangi', "created_by"=>"system","updated_by"=>"system"],
        ];

        foreach ($data as $value) {
            MAreaEnviro::updateOrCreate(['nama_area' => $value['nama_area']], $value);
        }
    }
}

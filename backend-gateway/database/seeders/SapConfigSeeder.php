<?php

namespace Database\Seeders;

use App\Models\SapSyncConfig;
use Illuminate\Database\Seeder;

class SapConfigSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'RFC OPEX',
                'type' => 'biaya',
                'tcode' => 'Z_ZCMM_KSB1',
                'parameter' => '{"I_KOKRS":"SGG","LR_KOSTL":[{"SIGN":"I","OPTION":"EQ","LOW":"3102000000","HIGH":""}],"LR_KSTAR":[{"SIGN":"I","OPTION":"EQ","LOW":"0065110009","HIGH":""}],"I_BUDAT_FROM":"20180801","I_BUDAT_TO":"20180831"}',
                'schedule' => 'daily',
                'at_time' => '00:01:00',
                'status' => 'active'
            ],
            [
                'name' => 'RFC CAPEX',
                'type' => 'capex',
                'tcode' => 'ZCPSHBP_S_ALR_FM',
                'parameter' => '{"I_PBUKR":"7000","I_GJAHR":"2021","T_PSPHI":[{"SIGN":"I","OPTION":"EQ","LOW":"P2-21003","HIGH":""}]}',
                'schedule' => 'daily',
                'at_time' => '00:01:00',
                'status' => 'active'
            ],
        ];

        foreach ($data as $value) {
            SapSyncConfig::updateOrCreate(['name' => $value['name'], 'type' => $value['type']], $value);
        }
    }
}

<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\Route;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'name' => 'admin',
            'username' => 'admin',
            'email' => 'admin@example.com',
            'password' => app('hash')->make('admin'),
        ];
        $model = User::updateOrCreate(['username' => $data['username']], $data);
        /**
         * get role administartor then synced
         */
        $role = Role::where('code', 'administrator')->first();
        $model->syncRoles([$role->id]);

        $data = [
            'name' => 'developer',
            'username' => 'developer',
            'email' => 'developer@example.com',
            'password' => app('hash')->make('developer'),
            'is_developer' => 'y'
        ];
        $model = User::updateOrCreate(['username' => $data['username']], $data);
        /**
         * get role developer then synced
         */
        $role = Role::where('code', 'developer')->first();
        $model->syncRoles([$role->id]);
    }
}

<?php

namespace Database\Seeders;

use App\Models\MFungsi;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class MFungsiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $kondisi = [
            [
                'nama_fungsi' => 'Prediktif', 
            ],
            [
                'nama_fungsi' => 'Mechanical', 
            ],
            [
                'nama_fungsi' => 'Elins-DCS', 
            ],
            [
                'nama_fungsi' => 'Operation', 
            ],
            [
                'nama_fungsi' => 'Supporting', 
            ],
            [
                'nama_fungsi' => 'WHRPG', 
            ],
        ];
        
        foreach($kondisi as $key=>$item){
            $item['created_by'] = 2;
            MFungsi::updateOrCreate(['nama_fungsi' => $item['nama_fungsi']], $item);
        }
    }
}

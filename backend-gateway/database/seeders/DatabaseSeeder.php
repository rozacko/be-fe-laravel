<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ActionSeeder::class);
        $this->call(RouteSeeder::class);
        $this->call(ModuleSeeder::class);
        $this->call(FeatureSeeder::class);
        $this->call(PermissionSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(MenuSeeder::class);
        $this->call(MAreaEnviroSeeder::class);
        $this->call(MJenisDokumenSeeder::class);
        $this->call(MJenisLimbahEnviroSeeder::class);
    }
}

<?php

namespace Database\Seeders;

use App\Models\Action;
use Illuminate\Database\Seeder;

class ActionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $actions = [
            [
                'id' => 'CR',
                'name' => 'Create'
            ],
            [
                'id' => 'RE',
                'name' => 'Read'
            ],
            [
                'id' => 'UP',
                'name' => 'Update'
            ],
            [
                'id' => 'DE',
                'name' => 'Delete'
            ],
        ];
        foreach ($actions as $value) {
            Action::updateOrCreate(['id' => $value['id']], $value);
        }

        $this->command->info("Action berhasil ditambahkan");
    }
}

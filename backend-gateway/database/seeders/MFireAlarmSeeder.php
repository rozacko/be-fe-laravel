<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\SHEMFireAlarm;

class MFireAlarmSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['id' => '1', 'parameter' => 'Membunyikan alarm secara simulasi', 'tipe' => 'mingguan','created_by' => '1'],
            ['id' => '2', 'parameter' => 'Memeriksa kerja lonceng', 'tipe' => 'mingguan','created_by' => '1'],
            ['id' => '3', 'parameter' => 'Memeriksa tegangan dan keadaan baterai', 'tipe' => 'mingguan','created_by' => '1'],
            ['id' => '4', 'parameter' => 'Memeriksa seluruh sistem alarm', 'tipe' => 'mingguan','created_by' => '1'],
            ['id' => '5', 'parameter' => 'Menciptakan kebakaran simulasi', 'tipe' => 'bulanan','created_by' => '1'],
            ['id' => '6', 'parameter' => 'Lampu-lampu indikator', 'tipe' => 'bulanan','created_by' => '1'],
            ['id' => '7', 'parameter' => 'Fasilitas penyediaan sumber tenaga darurat', 'tipe' => 'bulanan','created_by' => '1'],
            ['id' => '8', 'parameter' => 'Mencoba dengan kondisi gangguan terhadap sistem', 'tipe' => 'bulanan','created_by' => '1'],
            ['id' => '9', 'parameter' => 'Kondisi dan kebersihan panel indikator', 'tipe' => 'bulanan','created_by' => '1'],
        ];
        foreach ($data as $value) {
            SHEMFireAlarm::updateOrCreate(['id' => $value['id']], $value);
        }
    }
}

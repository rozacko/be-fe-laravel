<?php

namespace Database\Seeders;

use App\Models\MJenisLimbahEnviro;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class MJenisLimbahEnviroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['nama_limbah' => 'Oli Bekas','kode_limbah'=> 'B105D', "created_by"=>"system","updated_by"=>"system"],
            ['nama_limbah' => 'Aki Bekas','kode_limbah'=> 'A102D', "created_by"=>"system","updated_by"=>"system"],
            ['nama_limbah' => 'Majun Bekas','kode_limbah'=> 'B110D', "created_by"=>"system","updated_by"=>"system"],
            ['nama_limbah' => 'Filter Bekas dari Fasilitas Pengendalian Pencemaran Udara','kode_limbah'=> 'B109D', "created_by"=>"system","updated_by"=>"system"],
            ['nama_limbah' => 'Lampu TL Bekas','kode_limbah'=> 'B107D', "created_by"=>"system","updated_by"=>"system"],
            ['nama_limbah' => 'Limbah Laboratorium','kode_limbah'=> 'A106D', "created_by"=>"system","updated_by"=>"system"],
            ['nama_limbah' => 'Kemasan Bekas B3','kode_limbah'=> 'B104D', "created_by"=>"system","updated_by"=>"system"],
            ['nama_limbah' => 'Refraktory Bekas','kode_limbah'=> 'B417', "created_by"=>"system","updated_by"=>"system"],
            ['nama_limbah' => 'Filter Oli Bekas','kode_limbah'=> 'B110D', "created_by"=>"system","updated_by"=>"system"],
            ['nama_limbah' => 'Toner Bekas','kode_limbah'=> 'B353-1', "created_by"=>"system","updated_by"=>"system"],
            ['nama_limbah' => 'Limbah Elektronik (PCB)','kode_limbah'=> 'B107D', "created_by"=>"system","updated_by"=>"system"],
            ['nama_limbah' => 'Refrigerant Bekas dari Peralatan Elektronik','kode_limbah'=> 'A11D', "created_by"=>"system","updated_by"=>"system"],
            ['nama_limbah' => 'Limbah Terkontaminasi B3','kode_limbah'=> 'A108D', "created_by"=>"system","updated_by"=>"system"],
            ['nama_limbah' => 'Sludge Logam (Gram Terkontaminasi)','kode_limbah'=> 'A345-2', "created_by"=>"system","updated_by"=>"system"],
            ['nama_limbah' => 'Scrap Timah Solder','kode_limbah'=> 'B323-4', "created_by"=>"system","updated_by"=>"system"],
        ];

        foreach ($data as $value) {
            MJenisLimbahEnviro::updateOrCreate(['nama_limbah' => $value['nama_limbah']], $value);
        }
    }
}

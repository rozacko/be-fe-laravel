<?php

namespace Database\Seeders;

use App\Models\Module;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['id' => 'M-UM', 'name' => 'User Management'],

            //Overhaul Module
            ['id' => 'OVH', 'name' => 'Overhaul Reports'],

            //OPC Module
            ['id' => 'M-OPC', 'name' => 'OPC Management'],
        ];
        foreach ($data as $value) {
            Module::updateOrCreate(['id' => $value['id']], $value);
        }
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Route;
use App\Models\Permission;
use App\Models\M_EquipmentCategory;

class EquipmentCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $data = ['Crusher Production', 'Rawmill Production', 'Coalmill Production','Clinker Production','Cement Production','Packer Production','WHRPG','Stock Silo'];

        foreach($data as $k=>$v){
            M_EquipmentCategory::create(['category_name' => $v,'created_by' => '2']);
        }

        

    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\MPlant;
use App\Models\T_OverhaulPeriode;

class OverhaulSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Seed periode overhaul
        $plant = MPlant::all();
        foreach($plant as $plnt){
            T_OverhaulPeriode::create(['id_plant' => $plnt->id, 'created_by' => '2']);
        }
    }
}

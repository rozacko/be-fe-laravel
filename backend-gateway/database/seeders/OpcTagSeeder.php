<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\M_OpcApi;
use App\Models\M_TagList;
use App\Models\M_JenisOpc;
use App\Models\MArea;
use App\Models\MPlant;
use Exception;
use Illuminate\Support\Facades\DB;

class OpcTagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $data = json_decode("[
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"CRUSHER\",
              \"equipment\": \"Prod. BK Crusher #1\",
              \"TUBAN 1\": \"Tuban1New.A241BW2FT014\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"Tuban3.Limestone_feed\",
              \"TUBAN 4\": \"Tuban4.Limestone_feed\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"CRUSHER\",
              \"equipment\": \"Jam Op. BK Crusher #1\",
              \"sync_period\": \"Minutely\",
              \"daily_type\" : \"summary\",
              \"hourly_type\": \"summary\",
              \"hourly_factor\" : \"60\",
              \"TUBAN 1\": \"Tuban1New.A231FE1M01\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"Tuban3.SCR1_JOP\",
              \"TUBAN 4\": \"Tuban4.SCR4_limestone_JOP\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"CRUSHER\",
              \"equipment\": \"Prod. BK Crusher #2\",
              \"TUBAN 1\": \"Tuban1New.A241BW2FT014\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"Tuban3.Limestone_feed\",
              \"TUBAN 4\": \"#N/A\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"CRUSHER\",
              \"equipment\": \"Jam Op. BK Crusher #2\",
              \"sync_period\": \"Minutely\",
              \"daily_type\" : \"summary\",
              \"hourly_type\": \"summary\",
              \"hourly_factor\" : \"60\",
              \"TUBAN 1\": \"Tuban1New.A231FE2M01\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"Tuban3.SCR2_JOP\",
              \"TUBAN 4\": \"#N/A\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"CRUSHER\",
              \"equipment\": \"Prod. Clay Crusher\",
              \"TUBAN 1\": \"#N/A\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"Tuban3.clay_feed\",
              \"TUBAN 4\": \"Tuban4.Clay_feed\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"CRUSHER\",
              \"equipment\": \"Jam Op. Clay Crusher\",
              \"sync_period\": \"Minutely\",
              \"daily_type\" : \"summary\",
              \"hourly_type\": \"summary\",
              \"hourly_factor\" : \"60\",
              \"TUBAN 1\": \"#N/A\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"Tuban3.SCR1_clay_JOP\",
              \"TUBAN 4\": \"Tuban4.SCR4_Clay_JOP\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"CRUSHER\",
              \"equipment\": \"KWH Clay Crusher\",
              \"TUBAN 1\": \"#N/A\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"#N/A\",
              \"TUBAN 4\": \"Tuban4.Clay_KWH\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"CRUSHER\",
              \"equipment\": \"KWH Crusher Batukapur\",
              \"TUBAN 1\": \"#N/A\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"#N/A\",
              \"TUBAN 4\": \"Tuban4.Limestone_Kwh\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"RAWMILL\",
              \"equipment\": \"Prod. Roller Mill\",
              \"TUBAN 1\": \"#N/A\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"Tuban3.SRM3_PROD\",
              \"TUBAN 4\": \"Tuban4.334PX01_TOT\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"RAWMILL\",
              \"equipment\": \"Jam Op. Roller Mill\",
              \"sync_period\": \"Minutely\",
              \"daily_type\" : \"summary\",
              \"hourly_type\": \"summary\",
              \"hourly_factor\" : \"60\",
              \"TUBAN 1\": \"Tuban1.RM1_Motor\",
              \"TUBAN 2\": \"Tuban2.RM2_Motor\",
              \"TUBAN 3\": \"Tuban_RKC_Shift.TB3_JOP_RM\",
              \"TUBAN 4\": \"Tuban_RKC_Shift.TB4_JOP_RM\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"RAWMILL\",
              \"equipment\": \"Total Feed Roller Mill\",
              \"TUBAN 1\": \"Tuban1.RM1_Feed\",
              \"TUBAN 2\": \"Tuban2.RM2_Feed\",
              \"TUBAN 3\": \"#N/A\",
              \"TUBAN 4\": \"Tuban4.Feedrate\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"RAWMILL\",
              \"equipment\": \"RM Feed BK Mixed\",
              \"TUBAN 1\": \"#N/A\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"Tuban3.Z_333WF1FT110\",
              \"TUBAN 4\": \"Tuban4.334BC02A01F01\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"RAWMILL\",
              \"equipment\": \"RM Feed BK Pure\",
              \"TUBAN 1\": \"#N/A\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"Tuban3.Z_333WF2FT112\",
              \"TUBAN 4\": \"Tuban4.334BC01A01F01\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"RAWMILL\",
              \"equipment\": \"RM Feed P Silika\",
              \"TUBAN 1\": \"#N/A\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"Tuban3.Z_333WF3FT114\",
              \"TUBAN 4\": \"Tuban 3/4 Accessories.RM4_Silica_Feeder\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"RAWMILL\",
              \"equipment\": \"RM Feed P Besi\",
              \"TUBAN 1\": \"#N/A\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"Tuban3.Z_333WF3FT115A\",
              \"TUBAN 4\": \"Tuban4.334WF01A01F01\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"RAWMILL\",
              \"equipment\": \"KWH Roller Mill\",
              \"TUBAN 1\": \"#N/A\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"Tuban3.Z_343RM1JT127\",
              \"TUBAN 4\": \"#N/A\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"RAWMILL\",
              \"equipment\": \"Pemakaian IDO RM #1\",
              \"TUBAN 1\": \"#N/A\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"#N/A\",
              \"TUBAN 4\": \"#N/A\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"KILN\",
              \"equipment\": \"Prod. Terak\",
              \"TUBAN 1\": \"#N/A\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"Tuban_RKC_Shift.TB3_PROD_TERAK\",
              \"TUBAN 4\": \"Tuban4.CLINKER_PROD\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"KILN\",
              \"equipment\": \"Jam Operasi I L C\",
              \"sync_period\": \"Minutely\",
              \"daily_type\" : \"summary\",
              \"hourly_type\": \"summary\",
              \"hourly_factor\" : \"60\",
              \"TUBAN 1\": \"#N/A\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"Tuban3.SK3_ILC_JOP\",
              \"TUBAN 4\": \"#N/A\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"KILN\",
              \"equipment\": \"Jam Operasi S L C\",
              \"sync_period\": \"Minutely\",
              \"daily_type\" : \"summary\",
              \"hourly_type\": \"summary\",
              \"hourly_factor\" : \"60\",
              \"TUBAN 1\": \"#N/A\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"Tuban3.SK3_SLC_JOP\",
              \"TUBAN 4\": \"#N/A\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"KILN\",
              \"equipment\": \"Total Coal Kiln\",
              \"TUBAN 1\": \"#N/A\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"Tuban3.PW1_Feed_Kiln\",
              \"TUBAN 4\": \"Tuban4.PW1_Feed_Kiln\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"KILN\",
              \"equipment\": \"Total Coal ILC\",
              \"TUBAN 1\": \"#N/A\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"Tuban3.PW2_Feed_Calciner\",
              \"TUBAN 4\": \"Tuban4.PW2_Feed_Calciner\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"KILN\",
              \"equipment\": \"Total Coal SLC\",
              \"TUBAN 1\": \"#N/A\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"Tuban3.PW3_Feed_Calciner1\",
              \"TUBAN 4\": \"Tuban4.PW3_Feed_Calciner1\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"KILN\",
              \"equipment\": \"Pemakaian IDO/Solar Kiln\",
              \"TUBAN 1\": \"#N/A\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"Tuban3.VT1_Feed_Klin\",
              \"TUBAN 4\": \"Tuban4.BU03_Feed_kiln\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"KILN\",
              \"equipment\": \"Total IDO ILC\",
              \"TUBAN 1\": \"#N/A\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"Tuban3.VT4_Feed_ILC\",
              \"TUBAN 4\": \"Tuban4.BU03_Calciner\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"KILN\",
              \"equipment\": \"Total IDO SLC\",
              \"TUBAN 1\": \"#N/A\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"Tuban3.VT3_Feed_SLC\",
              \"TUBAN 4\": \"#N/A\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"KILN\",
              \"equipment\": \"Umpan Kiln\",
              \"TUBAN 1\": \"Tuban_Prod_Report.ACTUAL_RATE_KL1\",
              \"TUBAN 2\": \"Tuban_Prod_Report.ACTUAL_RATE_KL2\",
              \"TUBAN 3\": \"Tuban3.TOTAL_FEED\",
              \"TUBAN 4\": \"Tuban4.SK4_TOTAL_FEED\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"KILN\",
              \"equipment\": \"Umpan I L C\",
              \"TUBAN 1\": \"#N/A\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"Tuban3.ILC_FEED\",
              \"TUBAN 4\": \"#N/A\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"KILN\",
              \"equipment\": \"Umpan S L C\",
              \"TUBAN 1\": \"#N/A\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"Tuban3.SLC_FEED\",
              \"TUBAN 4\": \"#N/A\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"KILN\",
              \"equipment\": \"KWH Kiln Total\",
              \"TUBAN 1\": \"#N/A\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"#N/A\",
              \"TUBAN 4\": \"#N/A\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"COAL MILL\",
              \"equipment\": \"Jam Operasi Coal Mill\",
              \"sync_period\": \"Minutely\",
              \"daily_type\" : \"summary\",
              \"hourly_type\": \"summary\",
              \"hourly_factor\" : \"60\",
              \"TUBAN 1\": \"Tuban1.CM1_Motor\",
              \"TUBAN 2\": \"Tuban2.CM2_Motor\",
              \"TUBAN 3\": \"Tuban_RKC_Shift.TB3_JOP_CM\",
              \"TUBAN 4\": \"Tuban4.OP_Daily\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"COAL MILL\",
              \"equipment\": \"KWH Coal Mill\",
              \"TUBAN 1\": \"#N/A\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"#N/A\",
              \"TUBAN 4\": \"#N/A\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"COAL MILL\",
              \"equipment\": \"Jam Supply CM 0 ke\",
              \"sync_period\": \"Minutely\",
              \"daily_type\" : \"summary\",
              \"hourly_type\": \"summary\",
              \"hourly_factor\" : \"60\",
              \"TUBAN 1\": \"#N/A\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"#N/A\",
              \"TUBAN 4\": \"#N/A\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"COAL MILL\",
              \"equipment\": \"Produksi Coal Mill\",
              \"TUBAN 1\": \"#N/A\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"#N/A\",
              \"TUBAN 4\": \"Tuban4.474RM01A01Q41\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"COAL MILL\",
              \"equipment\": \"Pemakaian Sekam\",
              \"TUBAN 1\": \"#N/A\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"Tuban_RKC_Shift.TB3_TONAGE_BBA\",
              \"TUBAN 4\": \"#N/A\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"COAL MILL\",
              \"equipment\": \"Jam Operasi Sekam\",
              \"sync_period\": \"Minutely\",
              \"daily_type\" : \"summary\",
              \"hourly_type\": \"summary\",
              \"hourly_factor\" : \"60\",
              \"TUBAN 1\": \"#N/A\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"#N/A\",
              \"TUBAN 4\": \"#N/A\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"COAL MILL\",
              \"equipment\": \"Feed Raw Coal\",
              \"TUBAN 1\": \"#N/A\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"Tuban3.Feed_Raw_Coal\",
              \"TUBAN 4\": \"Tuban4.474RM01F01\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"COAL MILL\",
              \"equipment\": \"Stok Umpan\",
              \"TUBAN 1\": \"#N/A\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"Tuban3.Raw_Coal_Bin1\",
              \"TUBAN 4\": \"Tuban4.Raw_Coal_Bin1\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"FINISH MILL\",
              \"equipment\": \"Prod. Semen FM #1\",
              \"TUBAN 1\": \"#N/A\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"Tuban_Prod_Report.ACTUAL_RATE_FM5\",
              \"TUBAN 4\": \"Tuban4.FM7_PROD\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"FINISH MILL\",
              \"equipment\": \"Pemakaian Terak FM #1\",
              \"TUBAN 1\": \"Tuban12HMI.Z541WF3FT795\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"#N/A\",
              \"TUBAN 4\": \"Tuban4.547WF01FT01F01\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"FINISH MILL\",
              \"equipment\": \"Pemakaian Gypsum FM #1\",
              \"TUBAN 1\": \"Tuban12HMI.Z541WF1FT793\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"#N/A\",
              \"TUBAN 4\": \"Tuban4.547WF02FT01F21\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"FINISH MILL\",
              \"equipment\": \"Pemakaian Trass FM #1\",
              \"TUBAN 1\": \"Tuban12HMI.Z541WF2FT794\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"#N/A\",
              \"TUBAN 4\": \"Tuban4.547WF03FT01F21\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"FINISH MILL\",
              \"equipment\": \"Pemakaian BKapur FM #1\",
              \"TUBAN 1\": \"Tuban12HMI.Z541WF5FT796\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"#N/A\",
              \"TUBAN 4\": \"Tuban4.547WF04FT01F21\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"FINISH MILL\",
              \"equipment\": \"Pamakaian Fly Ash FM#1\",
              \"TUBAN 1\": \"#N/A\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"#N/A\",
              \"TUBAN 4\": \"Tuban4.537FM01FT01F01\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"FINISH MILL\",
              \"equipment\": \"Pemakaian Dust FM#1\",
              \"TUBAN 1\": \"#N/A\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"#N/A\",
              \"TUBAN 4\": \"#N/A\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"FINISH MILL\",
              \"equipment\": \"Jam Operasi FM #1\",
              \"sync_period\": \"Minutely\",
              \"daily_type\" : \"summary\",
              \"hourly_type\": \"summary\",
              \"hourly_factor\" : \"60\",
              \"TUBAN 1\": \"Tuban12HMI.Z541MM1M01\",
              \"TUBAN 2\": \"Tuban2.FM3_Motor\",
              \"TUBAN 3\": \"Tuban3.SFM5_MILL_JOP\",
              \"TUBAN 4\": \"Tuban4.547GRINDINGMODE\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"FINISH MILL\",
              \"equipment\": \"KWH Finish Mill #1\",
              \"TUBAN 1\": \"#N/A\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"#N/A\",
              \"TUBAN 4\": \"#N/A\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"FINISH MILL\",
              \"equipment\": \"Jam Operasi HRC FM #1\",
              \"sync_period\": \"Minutely\",
              \"daily_type\" : \"summary\",
              \"hourly_type\": \"summary\",
              \"hourly_factor\" : \"60\",
              \"TUBAN 1\": \"#N/A\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"Tuban3.SFM5_HRC_JOP\",
              \"TUBAN 4\": \"#N/A\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"FINISH MILL\",
              \"equipment\": \"Prod Finish Mill #2\",
              \"TUBAN 1\": \"#N/A\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"Tuban_Prod_Report.ACTUAL_RATE_FM6\",
              \"TUBAN 4\": \"Tuban_Prod_Report.ACTUAL_RATE_FM8\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"FINISH MILL\",
              \"equipment\": \"Pemakaian Terak FM #2\",
              \"TUBAN 1\": \"#N/A\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"Tuban3.Rate_WF3\",
              \"TUBAN 4\": \"Tuban4.548WF01FT01F01\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"FINISH MILL\",
              \"equipment\": \"Pemakaian Gypsum FM #2\",
              \"TUBAN 1\": \"#N/A\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"Tuban3.Rate_WF1\",
              \"TUBAN 4\": \"Tuban4.548WF02FT01F21\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"FINISH MILL\",
              \"equipment\": \"Pemakaian Trass FM #2\",
              \"TUBAN 1\": \"#N/A\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"Tuban3.Rate_WF2\",
              \"TUBAN 4\": \"Tuban4.548WF03FT01F21\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"FINISH MILL\",
              \"equipment\": \"Pemakaian BKapur FM #2\",
              \"TUBAN 1\": \"#N/A\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"Tuban3.Rate_WF2A\",
              \"TUBAN 4\": \"Tuban4.548WF04FT01F21\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"FINISH MILL\",
              \"equipment\": \"Pamakaian Fly Ash FM#2\",
              \"TUBAN 1\": \"#N/A\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"Tuban3.Rate_Fly_Ash\",
              \"TUBAN 4\": \"Tuban4.538FM01FT01F01\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"FINISH MILL\",
              \"equipment\": \"Pemakaian Dust FM#2\",
              \"TUBAN 1\": \"#N/A\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"Tuban3.Rate_Dust\",
              \"TUBAN 4\": \"#N/A\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"FINISH MILL\",
              \"equipment\": \"Jam Operasi FM #2\",
              \"sync_period\": \"Minutely\",
              \"daily_type\" : \"summary\",
              \"hourly_type\": \"summary\",
              \"hourly_factor\" : \"60\",
              \"TUBAN 1\": \"Tuban1.FM2_Motor\",
              \"TUBAN 2\": \"Tuban2.FM4_Motor\",
              \"TUBAN 3\": \"Tuban3.SFM6_MILL_JOP\",
              \"TUBAN 4\": \"#N/A\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"FINISH MILL\",
              \"equipment\": \"KWH Finish Mill #2\",
              \"TUBAN 1\": \"#N/A\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"#N/A\",
              \"TUBAN 4\": \"#N/A\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"FINISH MILL\",
              \"equipment\": \"Jam Operasi HRC FM #2\",
              \"sync_period\": \"Minutely\",
              \"daily_type\" : \"summary\",
              \"hourly_type\": \"summary\",
              \"hourly_factor\" : \"60\",
              \"TUBAN 1\": \"#N/A\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"Tuban3.SFM6_HRC_JOP\",
              \"TUBAN 4\": \"#N/A\"
            },

            {
              \"api_type\": \"OPC REST\",
              \"category\": \"RAWMILL\",
              \"periode\": \"Minutely\",
              \"equipment\": \"Status Running Roller MILL\",
              \"TUBAN 1\": \"Tuban1.RM1_Motor\",
              \"TUBAN 2\": \"Tuban2.RM2_Motor\",
              \"TUBAN 3\": \"Tuban3.SFM6_HRC_JOP\",
              \"TUBAN 4\": \"#N/A\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"RAWMILL\",
              \"periode\": \"Daily\",
              \"equipment\": \"KWH FN4 & FN5 RM\",
              \"TUBAN 1\": \"#N/A\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"#N/A\",
              \"TUBAN 4\": \"#N/A\"
            },
            {
              \"api_type\": \"OPC REST\",
              \"category\": \"RAWMILL\",
              \"periode\": \"Daily\",
              \"equipment\": \"KWH Aux RM\",
              \"TUBAN 1\": \"#N/A\",
              \"TUBAN 2\": \"#N/A\",
              \"TUBAN 3\": \"#N/A\",
              \"TUBAN 4\": \"#N/A\"
            }
          ]",true);
          
        DB::beginTransaction();
        $plants = MPlant::all();

        foreach($data as $key=>$item){
            $id_api = M_OpcApi::where('api_name',$item['api_type'])->first()->id;
            $id_area = MArea::where('nm_area','ilike','%'.$item['category'].'%')->first();
            if(is_null($id_area)){
                $this->command->info($item['category']);exit;
            }            
            $id_area = $id_area->id;
            $equipment = M_JenisOpc::where(['id_area' => $id_area,'jenis_opc'=>$item['equipment']])->first();

            $equipment = M_JenisOpc::updateOrCreate(['id_area' => $id_area,'jenis_opc'=>$item['equipment']],
                            ['id_area' => $id_area,'jenis_opc'=>$item['equipment'],'created_by' => '2', 'updated_by'=>'2']);

            foreach($plants as $plant){
              $params =  ['id_plant' => $plant->id, 'id_jenis_opc' => $equipment->id,
              'id_api' => $id_api, 'tag_name' => $item[strtoupper($plant->nm_plant)],
              'created_by' => '2', 'updated_by' => '2'];

              if(isset($item['periode'])){
                 $params['sync_period'] = $item['periode'];
              }

              if(isset($item['hourly_type'])){
                $params['hourly_type'] = $item['hourly_type'];
              }

              if(isset($item['hourly_factor'])){
                  $params['hourly_factor'] = $item['hourly_factor'];
              }

              if(isset($item['daily_type'])){
                $params['daily_type'] = $item['daily_type'];
              }

              if(isset($item['daily_factor'])){
                  $params['daily_factor'] = $item['daily_factor'];
              }

              if(isset($item['sync_period'])){
                $params['sync_period'] = $item['sync_period'];
            }

              $tag = M_TagList::updateOrCreate(['id_plant' => $plant->id, 'id_jenis_opc' => $equipment->id],
                   $params);
            }
        }

        DB::commit();

        $this->command->info("Tag OPC Berhasil ditambahkan");
    }
}

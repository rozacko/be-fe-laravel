<?php

namespace Database\Seeders;

use App\Models\M_ApiShipment;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\M_JenisShipment;
use App\Models\M_JenisShipmentParams;
use App\Models\MPlant;

class JenisParamsShipmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Timbang Bahan Params
        $api = M_ApiShipment::where('api_name','Shipment Inbound')->first();
        $data = [
            [
                'jenis' => ['jenis_shipment' => 'TRASS'],
                'params' => [['attr' => 'MAT_DESC', 'condition' => 'contain', 'value' => 'TRASS']]
            ],
            [
                'jenis' => ['jenis_shipment' => 'GYPSUM PURIFIED TOTAL'],
                'params' => [['attr' => 'MAT_DESC', 'condition' => 'contain', 'value' => 'GYPSUM PURIFIED'],]
            ],
            [
                'jenis' => ['jenis_shipment' => 'GYPSUM PURIFIED TANJUNG JATI'],
                'params' => [['attr' => 'MAT_DESC', 'condition' => 'contain', 'value' => 'GYPSUM PURIFIED'],
                            ['attr' => 'MAT_DESC', 'condition' => 'contain', 'value' => 'TJ'],]
            ],
            [
                'jenis' => ['jenis_shipment' => 'GYPSUM PURIFIED Smelthing'],
                'params' => [['attr' => 'MAT_DESC', 'condition' => 'contain', 'value' => 'GYPSUM PURIFIED'],
                             ['attr' => 'MAT_DESC', 'condition' => 'contain', 'value' => 'SMELTING'],]
            ],
            [
                'jenis' => ['jenis_shipment' => 'GYPSUM NATURAL'],
                'params' => [['attr' => 'MAT_DESC', 'condition' => 'contain', 'value' => 'GYPSUM'],
                            ['attr' => 'MAT_DESC', 'condition' => 'not', 'value' => 'PURIFIED'],]
            ],
            [
                'jenis' => ['jenis_shipment' => 'FLY ASH'],
                'params' => [['attr' => 'MAT_DESC', 'condition' => 'contain', 'value' => 'FLY ASH']]
            ],
            [
                'jenis' => ['jenis_shipment' => 'TERAK BELI'],
                'params' => [['attr' => 'MAT_DESC', 'condition' => 'contain', 'value' => 'TERAK']]
            ],
            [
                'jenis' => ['jenis_shipment' => 'PASIR SILIKA'],
                'params' => [['attr' => 'MAT_DESC', 'condition' => 'contain', 'value' => 'SILIKA']]
            ],
            [
                'jenis' => ['jenis_shipment' => 'COPPER SLAG'],
                'params' => [['attr' => 'MAT_DESC', 'condition' => 'contain', 'value' => 'COPPER SLAG']]
            ],
            [
                'jenis' => ['jenis_shipment' => 'PASIR BESI'],
                'params' => [['attr' => 'MAT_DESC', 'condition' => 'contain', 'value' => 'BESI']]
            ],
            [
                'jenis' => ['jenis_shipment' => 'BATUBARA TOTAL'],
                'params' => [['attr' => 'MAT_DESC', 'condition' => 'contain', 'value' => 'BARA'],]
            ],
            [
                'jenis' => ['jenis_shipment' => 'BATUBARA: MEDIUM CALORY'],
                'params' => [['attr' => 'MAT_DESC', 'condition' => 'contain', 'value' => 'BARA'], 
                            ['attr' => 'MAT_DESC', 'condition' => 'contain', 'value' => 'MEDIUM'],]
            ],
            [
                'jenis' => ['jenis_shipment' => 'BATUBARA: LOW CALORY'],
                'params' => [['attr' => 'MAT_DESC', 'condition' => 'contain', 'value' => 'BARA'], 
                            ['attr' => 'MAT_DESC', 'condition' => 'contain', 'value' => 'LOW']]
            ],
            [
                'jenis' => ['jenis_shipment' => 'I.D.O/SOLAR'],
                'params' => [['attr' => 'MAT_DESC', 'condition' => 'contain', 'value' => 'SOLAR']]
            ],
            [
                'jenis' => ['jenis_shipment' => 'COCOPEAT'],
                'params' => [['attr' => 'MAT_DESC', 'condition' => 'contain', 'value' => 'COCOPEAT']]
            ],
            [
                'jenis' => ['jenis_shipment' => 'SEKAM PADI'],
                'params' => [['attr' => 'MAT_DESC', 'condition' => 'contain', 'value' => 'SEKAM']]
            ],
            [
                'jenis' => ['jenis_shipment' => 'SERBUK GERGAJI'],
                'params' => [['attr' => 'MAT_DESC', 'condition' => 'contain', 'value' => 'GERGAJI']]
            ],
            [
                'jenis' => ['jenis_shipment' => 'TOBACCO'],
                'params' => [['attr' => 'MAT_DESC', 'condition' => 'contain', 'value' => 'TOBACCO']]
            ],
            [
                'jenis' => ['jenis_shipment' => 'BURNING FURNACE SLAG'],
                'params' => [['attr' => 'MAT_DESC', 'condition' => 'contain', 'value' => 'FURNACE']]
            ],
            [
                'jenis' => ['jenis_shipment' => 'TOTAL MATERIAL ALTERNATIF'],
                'params' => [['attr' => 'MAT_DESC', 'condition' => 'contain', 'value' => 'GERGAJI']]
            ],
            [
                'jenis' => ['jenis_shipment' => 'BOTTOM ASH'],
                'params' => [['attr' => 'MAT_DESC', 'condition' => 'contain', 'value' => 'BOTTOM ASH']]
            ],
            [
                'jenis' => ['jenis_shipment' => 'PAPERSLUGDE'],
                'params' => [['attr' => 'MAT_DESC', 'condition' => 'contain', 'value' => 'PAPER']]
            ],
            [
                'jenis' => ['jenis_shipment' => 'SPENT EARTH'],
                'params' => [['attr' => 'MAT_DESC', 'condition' => 'contain', 'value' => 'SPENT EARTH']]
            ],
            [
                'jenis' => ['jenis_shipment' => 'DLL'],
                'params' => [
                                ['attr' => 'MAT_DESC', 'condition' => 'not', 'value' => 'SPENT EARTH'],
                                ['attr' => 'MAT_DESC', 'condition' => 'not', 'value' => 'GYPSUM'],
                                ['attr' => 'MAT_DESC', 'condition' => 'not', 'value' => 'PAPER'],
                                ['attr' => 'MAT_DESC', 'condition' => 'not', 'value' => 'BESI'],
                                ['attr' => 'MAT_DESC', 'condition' => 'not', 'value' => 'SILIKA'],
                                ['attr' => 'MAT_DESC', 'condition' => 'not', 'value' => 'SEKAM'],
                                ['attr' => 'MAT_DESC', 'condition' => 'not', 'value' => 'GERGAJI'],
                                ['attr' => 'MAT_DESC', 'condition' => 'not', 'value' => 'FURNACE'],
                                ['attr' => 'MAT_DESC', 'condition' => 'not', 'value' => 'TOBACCO'],
                                ['attr' => 'MAT_DESC', 'condition' => 'not', 'value' => 'SOLAR'],
                                ['attr' => 'MAT_DESC', 'condition' => 'not', 'value' => 'COCOPEAT'],
                                ['attr' => 'MAT_DESC', 'condition' => 'not', 'value' => 'BARA'],
                                ['attr' => 'MAT_DESC', 'condition' => 'not', 'value' => 'COPPER'],
                                ['attr' => 'MAT_DESC', 'condition' => 'not', 'value' => 'TERAK'],
                                ['attr' => 'MAT_DESC', 'condition' => 'not', 'value' => 'TRASS'],
                                ['attr' => 'MAT_DESC', 'condition' => 'not', 'value' => 'ASH'],
                            ]
            ],
            
        ];

        foreach($data as $value){
            $value['jenis']['id_api_shipment'] = $api->id;
            $value['jenis']['created_by'] = '2';
            $value['jenis']['shipment_params'] = json_encode($value['params']);
            $model = M_JenisShipment::updateOrCreate(['jenis_shipment' => $value['jenis']['jenis_shipment']], 
                                                        $value['jenis']);
           
        }

        //packer params
        $api = M_ApiShipment::where('api_name','Shipment Realisasi')->first();
        //Input Conveyor Plant
        $_conv_plant = [
                [1,8,[70,80,93,90],'Tuban 1'],
                [11,19,[81],'Tuban 2'],
                [21,25,[72,92,82,83],'Tuban 3'],
                [31,34,[84,94],'Tuban 4'],
        ];

        foreach($_conv_plant as $con){
            $_plant = MPlant::where('nm_plant', $con[3])->first();
            foreach(range($con[0],$con[1]) as $k=>$v){
                $jenis = ['jenis_shipment' => 'Packer '.($k+1), 
                            // 'shipment_param_attr' => 'CONVEYOR', 
                            'id_plant' => $_plant->id, 
                            'id_api_shipment' => $api->id,
                            'created_by' => '2'];

                $params = [['attr' => 'CONVEYOR', 'condition' => 'contain', 'value' => $v]];
                $jenis['shipment_params'] = json_encode($params);
                $model = M_JenisShipment::updateOrCreate(['jenis_shipment' => $jenis['jenis_shipment'],
                                                        'id_plant' => $_plant->id], $jenis);

                // $params = ['id_jenis_shipment' => $model->id, 'condition' => 'contain', 'value' => $v];
                // $_model = M_JenisShipmentParams::updateOrCreate($params, $params);
            }
            $_param = [];
            foreach($con[2] as $_conv){
                $_param[] = ['attr' => 'CONVEYOR', 'condition' => 'contain', 'value' => $_conv];
            }
            $jenis = ['jenis_shipment' => 'Conveyor Curah '.$con[3], 
                    // 'shipment_param_attr' => 'CONVEYOR', 
                    'id_plant' => $_plant->id, 
                    'id_api_shipment' => $api->id,
                    'created_by' => '2'];

            $jenis['shipment_params'] = json_encode($_param);
            $model = M_JenisShipment::updateOrCreate(['jenis_shipment' => $jenis['jenis_shipment'],
                                                    'id_plant' => $_plant->id], $jenis);
        }

        $_release = ['Release' =>  [['attr' => 'PRODUK', 'condition' => 'contain', 'value' => '*']],
                    'Curah' =>  [['attr' => 'PRODUK', 'condition' => 'contain', 'value' => 'CURAH']],
                    'Zak' =>  [['attr' => 'PRODUK', 'condition' => 'contain', 'value' => 'ZAK'],]];

        $_brand = ['SG' => ["CEMENT BAG,PASTED:WOVEN;STD;1P;PCC50 MP ",
                            "CEMENT BAG;PASTED:WOVEN;BBLV;1P;PCC5OMM",
                            "CEMENT BAG;PASTED:WOVEN;STD;1P;PCC40 MH",
                            "CEMENT BAG;PASTED;WOVEN;PR;1P;PCC40 MP",
                            "CEMENT BAG;PST;INV;PRM;1P;PCC50KG;MP;SG",
                            "CEMENT BAG;PST;INV;STD;1P;PCC50KG;PP;SG",
                            "CEMENT BAG;PST;KRAFT;2P;PCC40KG;HH;SG",
                            "CEMENT BAG;PST;KRAFT;2P;PCC40KG;MH;SG",
                            "CEMENT BAG;PST;KRAFT;2P;PCC40KG;MM;SG",
                            "CEMENT BAG;PST;KRAFT;2P;PCC50KG;HH;SG",
                            "CEMENT BAG;PST;KRAFT;2P;PCC50KG;MH;SG",
                            "CEMENT BAG;PST;KRAFT;2P;PCC50KG;MM;SG",
                            "CEMENT BAG;PST;WV;STD;4P;PCC40KG;PP;SG",], 
                  'SP'=> ["CEMENT BAG;PST;INV;STD;1P;PCC50KG;H;SP",
                         "CEMENT BAG;PST;VVV;STD;1P;PCC40KG;H;SP",], 
                  'Dynamix' => ["CEMENT BAG;PST;KRAFT;2P;PCC40KG;DYX;SBI",
                            "CEMENT BAG;PST;ON;STD;1P;PCC40KG;DYX;SBI",], 
                  'ST' => ["CEMENT BAG;PST;KRAFT;2P;PCC40KG;ST",
                            "CEMENT BAG;PST;KRAFT;2P;PCC50KG;ST",
                            "CEMENT BAG;PST;WV;STD4P;PCC40KG;ST",
                            "CEMENT BAG;PST;WV;STD4P;PCC50KG;ST",
                            "CEMENT BAG;SW;WV;STD;3P;PCC40KG;ST",],];

        $_ppcopc = [
                    'PPC Curah' => [
                                    ['attr' => 'PRODUK', 'condition' => 'contain', 'value' => 'CURAH'],
                                    ['attr' => 'PRODUK', 'condition' => 'contain', 'value' => 'EZPRO'],
                                    ['attr' => 'MAT_DESC', 'condition' => 'not', 'value' => 'ASH'],
                                ],
                    'OPC Curah' => [
                                    ['attr' => 'PRODUK', 'condition' => 'contain', 'value' => 'CURAH'],
                                    ['attr' => 'PRODUK', 'condition' => 'contain', 'value' => 'ULTRAPRO'],
                                    ['attr' => 'MAT_DESC', 'condition' => 'not', 'value' => 'ASH'],
                                ],
                    'PPC Premium' => [
                                    ['attr' => 'PRODUK', 'condition' => 'contain', 'value' => 'CURAH'],
                                    ['attr' => 'PRODUK', 'condition' => 'contain', 'value' => 'PWRPRO'],
                                    ['attr' => 'MAT_DESC', 'condition' => 'not', 'value' => 'ASH'],
                                ],
                    'OPC Premium' => [
                                    ['attr' => 'PRODUK', 'condition' => 'contain', 'value' => 'CURAH'],
                                    ['attr' => 'PRODUK', 'condition' => 'contain', 'value' => 'SPRINTPRO'],
                                    ['attr' => 'MAT_DESC', 'condition' => 'not', 'value' => 'ASH'],
                            ],
                    ];

        $_plants = MPlant::all();

        foreach($_plants as $_plant){
            foreach($_release as $k=>$v){
                $jenis = [
                            'jenis_shipment' => 'Total '.$k, 
                            // 'shipment_param_attr' => 'PRODUK', 
                            'id_plant' => $_plant->id,
                            'id_api_shipment' => $api->id,
                            'shipment_params' => json_encode($v)
                        ];
                $model = M_JenisShipment::updateOrCreate($jenis, $jenis);

                $params = [['attr' => 'PRODUK', 'condition' => 'contain', 'value' => $v]];
            }

            foreach($_brand as $k=>$v){
                $jenis = [
                            'jenis_shipment' => 'Release Semen Per Brand '.$k, 
                            // 'shipment_param_attr' => 'NM KANTONG', 
                            'id_plant' => $_plant->id,
                            'id_api_shipment' => $api->id,
                            'shipment_params_check' => 'or',
                        ];

                $_param = [];
                foreach($v as $_bag){
                    $_param[] = ['attr' => 'NM KANTONG', 'condition' => 'contain', 'value' => $_bag];
                }

                $jenis['shipment_params'] = json_encode($_param);
                $model = M_JenisShipment::updateOrCreate(['jenis_shipment' => $jenis['jenis_shipment'],
                                                        'id_plant' => $_plant->id], $jenis);
            }

            foreach($_ppcopc as $k=>$v){
                $jenis = [
                            'jenis_shipment' => 'Release '.$k, 
                            'id_plant' => $_plant->id,
                            'id_api_shipment' => $api->id ,
                            'shipment_params' => json_encode($v)
                        ];
                $model = M_JenisShipment::updateOrCreate($jenis, $jenis);
            }
        }
    }
}

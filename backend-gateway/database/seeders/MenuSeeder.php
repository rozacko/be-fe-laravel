<?php

namespace Database\Seeders;

use App\Models\Menu;
use Illuminate\Database\Seeder;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menus = [
            [
                'id' => 1,
                'name' => 'Dashboard',
                'description' => 'dashboard',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => '#',
                'order_no' => 1,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 0
            ],
            [
                'id' => 2,
                'name' => 'Home',
                'description' => 'home',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => '#',
                'order_no' => 2,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 1
            ],
            [
                'id' => 3,
                'name' => 'KPI Dashboard',
                'description' => 'KPI dashboard',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => '#',
                'order_no' => 3,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 0
            ],
            [
                'id' => 4,
                'name' => 'KPI Management',
                'description' => 'kpi management',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => '#',
                'order_no' => 4,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 3
            ],
            [
                'id' => 5,
                'name' => 'Power BI',
                'description' => 'power bi',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => '#',
                'order_no' => 5,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 0
            ],
            [
                'id' => 6,
                'name' => 'Maintenance',
                'description' => 'maintenance',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => '#',
                'order_no' => 6,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 0
            ],
            [
                'id' => 7,
                'name' => 'Plant Performance',
                'description' => 'Plant Performance',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => '#',
                'order_no' => 7,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 6
            ],
            [
                'id' => 8,
                'name' => 'Maintenance Cost',
                'description' => 'maintenance cost',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => '#',
                'order_no' => 8,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 6
            ],
            [
                'id' => 9,
                'name' => 'Capex Report',
                'description' => 'capex report',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => '#',
                'order_no' => 9,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 6
            ],
            [
                'id' => 10,
                'name' => 'Sparepart Planning',
                'description' => 'sparepart planning',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => '#',
                'order_no' => 10,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 6
            ],
            [
                'id' => 11,
                'name' => 'Production Asset Management',
                'description' => 'production asset management',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => '#',
                'order_no' => 11,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 6
            ],
            [
                'id' => 12,
                'name' => 'Overhoul Report',
                'description' => 'Overhoul Report',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => '#',
                'order_no' => 12,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 0
            ],
            [
                'id' => 13,
                'name' => 'Dashboard Preparation',
                'description' => 'Dashboard Preparation',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'overhaul/preparation',
                'order_no' => 13,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 12
            ],
            [
                'id' => 14,
                'name' => 'Final Report',
                'description' => 'Final Report',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'overhaul/finalreport',
                'order_no' => 15,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 12
            ],
            [
                'id' => 15,
                'name' => 'Dashboard Daily Progress',
                'description' => 'Dashboard Daily Progress',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'overhaul/progress',
                'order_no' => 14,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 12
            ],
            [
                'id' => 16,
                'name' => 'Production Report',
                'description' => 'production report',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => '#',
                'order_no' => 16,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 0
            ],
            [
                'id' => 17,
                'name' => 'Crusher Production',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'production/crusher',
                'order_no' => 17,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 16
            ],
            [
                'id' => 18,
                'name' => 'Rawmill Production',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'production/rawmill',
                'order_no' => 18,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 16
            ],
            [
                'id' => 19,
                'name' => 'Clinker Production',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'production/clinker',
                'order_no' => 19,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 16
            ],
            [
                'id' => 20,
                'name' => 'Coal Mill Production',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'production/coalmill',
                'order_no' => 20,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 16
            ],
            [
                'id' => 21,
                'name' => 'Cement Production',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'production/cement',
                'order_no' => 21,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 16
            ],
            [
                'id' => 22,
                'name' => 'Packer Production',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'production/packer',
                'order_no' => 22,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 16
            ],
            [
                'id' => 23,
                'name' => 'Production Index',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'production/prodindex',
                'order_no' => 23,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 16
            ],
            [
                'id' => 24,
                'name' => 'WHRPG',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'production/whrpg',
                'order_no' => 24,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 16
            ],
            [
                'id' => 25,
                'name' => 'Dashboard Quality Management',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'quality-management',
                'order_no' => 25,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 0
            ],            
            [
                'id' => 27,
                'name' => 'SHE Safety Performance',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => '#',
                'order_no' => 27,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 0
            ],
            [
                'id' => 28,
                'name' => 'SHE Enviro Digital',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => '#',
                'order_no' => 28,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 0
            ],
            [
                'id' => 29,
                'name' => 'SHE Proper',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => '#',
                'order_no' => 29,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 0
            ],
            [
                'id' => 30,
                'name' => 'Plant Overview',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => '#',
                'order_no' => 30,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 0
            ],
            [
                'id' => 31,
                'name' => 'Realtime Plant Overview',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => '#',
                'order_no' => 31,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 30
            ],
            [
                'id' => 32,
                'name' => 'Stok Silo',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => '#',
                'order_no' => 32,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 30
            ],
            [
                'id' => 33,
                'name' => 'Solar Panel',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => '#',
                'order_no' => 33,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 30
            ],
            [
                'id' => 34,
                'name' => 'TPM Report',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => '#',
                'order_no' => 34,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 0
            ],
            [
                'id' => 35,
                'name' => 'TPM Dashboard',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'tpm-report',
                'order_no' => 35,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 34
            ],
            [
                'id' => 36,
                'name' => 'Laporan Gugus',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'tpm-report-laporan-gugus',
                'order_no' => 36,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 34
            ],
            [
                'id' => 37,
                'name' => 'Laporan KPI Member TPM',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'tpm-report-laporan-kpi-member-tpm',
                'order_no' => 37,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 34
            ],
            [
                'id' => 38,
                'name' => 'Data Dashboard TPM',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'mtpm',
                'order_no' => 38,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 34
            ],
            [
                'id' => 39,
                'name' => 'Plant Reliability',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => '#',
                'order_no' => 39,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 0
            ],
            [
                'id' => 40,
                'name' => 'Dashboard Plant Reliability',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'plant-reliability',
                'order_no' => 40,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 39
            ],
            [
                'id' => 41,
                'name' => 'Process Inspection',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'inspection',
                'order_no' => 41,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 39
            ],
            [
                'id' => 42,
                'name' => 'Vendor Management',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => '#',
                'order_no' => 42,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 0
            ],
            [
                'id' => 43,
                'name' => 'Upload Planning Schedule Worktime',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'vendor-management/upload-schedule',
                'order_no' => 43,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 42
            ],
            [
                'id' => 44,
                'name' => 'Data Realisasi Absensi Outsourcing',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'vendor-management/realisasi-fingerprint',
                'order_no' => 44,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 42
            ],
            [
                'id' => 45,
                'name' => 'Report Planning & Realisasi Worktime',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'vendor-management/planning-realisasi-fingerprint',
                'order_no' => 45,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 42
            ],
            [
                'id' => 46,
                'name' => 'Data RKAP & Prognose',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => '#',
                'order_no' => 46,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 0
            ],
            [
                'id' => 47,
                'name' => 'RKAP Plant Performance',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'rkap-prognose/plant-performance',
                'order_no' => 47,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 46
            ],
            [
                'id' => 48,
                'name' => 'RKAP Plant Maintenance',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'rkap-prognose/maintenance-performance',
                'order_no' => 48,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 46
            ],
            [
                'id' => 49,
                'name' => 'RKAP Capex',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'rkap-prognose/rkap-capex',
                'order_no' => 49,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 46
            ],
            [
                'id' => 50,
                'name' => 'Data Koreksi',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => '#',
                'order_no' => 50,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 0
            ],
            [
                'id' => 51,
                'name' => 'Data Plant Performance',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => '#',
                'order_no' => 51,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 50
            ],
            [
                'id' => 52,
                'name' => 'Data Report Capex',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => '#',
                'order_no' => 52,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 50
            ],
            [
                'id' => 53,
                'name' => 'Data Maintenance',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => '#',
                'order_no' => 53,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 50
            ],
            [
                'id' => 54,
                'name' => 'Data Production',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => '#',
                'order_no' => 54,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 50
            ],
            [
                'id' => 55,
                'name' => 'Master Data',
                'description' => 'master data',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => '#',
                'order_no' => 55,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 0
            ],
            [
                'id' => 56,
                'name' => 'Plant & Pabrik',
                'description' => 'Plant & Pabrik',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'masterdata/mplant/',
                'order_no' => 56,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 55
            ],
            [
                'id' => 57,
                'name' => 'Plant Area',
                'description' => 'Plant Area',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'masterdata/mplantarea/',
                'order_no' => 57,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 55
            ],
            [
                'id' => 58,
                'name' => 'Cost Center',
                'description' => 'Cost Center',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'masterdata/mcostcenter/',
                'order_no' => 58,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 55
            ],
            [
                'id' => 59,
                'name' => 'Cost Element',
                'description' => 'Cost Element',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'masterdata/mcostelement/',
                'order_no' => 59,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 55
            ],
            [
                'id' => 60,
                'name' => 'Worgroup',
                'description' => 'Worgroup',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'masterdata/mworkgroup/',
                'order_no' => 60,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 55
            ],
            [
                'id' => 61,
                'name' => 'Project Capex',
                'description' => 'Project Capex',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'masterdata/mprojectcapex/',
                'order_no' => 61,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 55
            ],
            [
                'id' => 62,
                'name' => 'Area',
                'description' => 'Area',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'masterdata/marea/',
                'order_no' => 62,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 55
            ],
            [
                'id' => 63,
                'name' => 'Kondisi Inspection',
                'description' => 'Kondisi Inspection',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'masterdata/mkondisi/',
                'order_no' => 63,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 55
            ],
            [
                'id' => 64,
                'name' => 'Equipment Inspection',
                'description' => 'Equipment Inspection',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'masterdata/meqinspec/',
                'order_no' => 64,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 55
            ],
            [
                'id' => 67,
                'name' => 'Configuration',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => '#',
                'order_no' => 67,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 0
            ],
            [
                'id' => 68,
                'name' => 'User Management',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'configuration/user',
                'order_no' => 68,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 67
            ],
            [
                'id' => 69,
                'name' => 'Role Management',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'configuration/role',
                'order_no' => 69,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 67
            ],
            [
                'id' => 70,
                'name' => 'Dashboard Power BI',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => '#',
                'order_no' => 70,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 67
            ],
            [
                'id' => 71,
                'name' => 'Dashboard Safety Performance',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'she_safety/dashboardsafety/',
                'order_no' => 1,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 27
            ],
            [
                'id' => 72,
                'name' => 'Man Power',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'she_safety/manpower/',
                'order_no' => 2,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 27
            ],
            [
                'id' => 73,
                'name' => 'Man Hours',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'she_safety/manhours/',
                'order_no' => 3,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 27
            ],
            [
                'id' => 74,
                'name' => 'Unsafe Action',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'she_safety/unsafeaction/',
                'order_no' => 4,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 27
            ],
            [
                'id' => 75,
                'name' => 'Unsafe Condition',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'she_safety/unsafecondition/',
                'order_no' => 5,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 27
            ],
            [
                'id' => 76,
                'name' => 'Accident Report',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'she_safety/accidentreport/',
                'order_no' => 6,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 27
            ],
            [
                'id' => 77,
                'name' => 'Fire Accident',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'she_safety/fireaccidentreport/',
                'order_no' => 7,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 27
            ],
            [
                'id' => 78,
                'name' => 'Safety Training',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'she_safety/safetytraining/',
                'order_no' => 8,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 27
            ],            
            [
                'id' => 80,
                'name' => 'APAR',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'she_safety/apar/',
                'order_no' => 9,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 27
            ],
            [
                'id' => 81,
                'name' => 'Hydrant',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'she_safety/hydrant/',
                'order_no' => 10,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 27
            ],
            [
                'id' => 82,
                'name' => 'Fire Alarm',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'she_safety/firealarm/',
                'order_no' => 11,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 27
            ],
            [
                'id' => 83,
                'name' => 'RKAP Prognose Performance',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'rkap-prognose/prognose-performance',
                'order_no' => 48,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 46
            ],
            [
                'id' => 84,
                'name' => 'RKAP Prognose Maintenance',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'rkap-prognose/prognose-maintenance',
                'order_no' => 48,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 46
            ],
            [
                'id' => 85,
                'name' => 'Data COGM',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => '#',
                'order_no' => 56,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 50
            ],
            [
                'id' => 86,
                'name' => 'Kebijakan Lingkungan',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'she_proper/kebijakan_ling/',
                'order_no' => 1,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 29
            ],
            [
                'id' => 87,
                'name' => 'Struktur Organisasi',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'she_proper/struktur_organ/',
                'order_no' => 2,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 29
            ],
            [
                'id' => 88,
                'name' => 'KPI Proper & Industri Hijau',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'she_proper/kpiproper/',
                'order_no' => 3,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 29
            ],
            [
                'id' => 89,
                'name' => 'Program Lingkungan',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'she_proper/program_ling/',
                'order_no' => 4,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 29
            ],
            [
                'id' => 90,
                'name' => 'Perizinan Lingkungan',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'she_proper/izin_ling/',
                'order_no' => 5,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 29
            ],
            [
                'id' => 91,
                'name' => 'Data Gugus TPM',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'mgugustpm',
                'order_no' => 91,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 34
            ],
            [
                'id' => 92,
                'name' => 'Data KPI Member TPM',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'mkpitpm',
                'order_no' => 92,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 34
            ],
            [
                'id' => 93,
                'name' => 'Matrik Peraturan Perundangan',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'she_proper/matrik_ling/',
                'order_no' => 6,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 29
            ],
            [
                'id' => 94,
                'name' => 'Certificate of Analysis',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'she_proper/sertif_analis/',
                'order_no' => 7,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 29
            ],
            [
                'id' => 95,
                'name' => 'Monitoring CEMS Holding',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'she_proper/cems_holding/',
                'order_no' => 8,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 29
            ],
            [
                'id' => 96,
                'name' => 'Sertifikat/Penghargaan Lingkungan',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'she_proper/sertif_ling/',
                'order_no' => 9,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 29
            ],
            [
                'id' => 97,
                'name' => 'Kompetensi Personil',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'she_proper/kom_personil/',
                'order_no' => 10,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 29
            ],
            [
                'id' => 98,
                'name' => 'Performance Notifikasi',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'performance-notifikasi',
                'order_no' => 1,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 50
            ],
            [
                'id' => 99,
                'name' => 'Performance Order',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'performance-order',
                'order_no' => 2,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 50
            ],
            [
                'id' => 100,
                'name' => 'Neraca Limbah B3',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'she_safety/neraca-limbah-b3',
                'order_no' => 100,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 28
            ],
            [
                'id' => 101,
                'name' => 'Dokumen Lingkungan',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'she_safety/dokumen-lingkungan',
                'order_no' => 101,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 28
            ],
            [
                'id' => 102,
                'name' => 'Master Jenis Limbah',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'she_safety/master-jenis-limbah',
                'order_no' => 102,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 28
            ],[
                'id' => 103,
                'name' => 'Prognose Capex',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'rkap-prognose/prognose-capex',
                'order_no' => 103,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 46
            ],[
                'id' => 104,
                'name' => 'KPI Safety',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => 'she_safety/kpisafety',
                'order_no' => 12,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 27
            ]
        ];
        foreach ($menus as $value) {
            Menu::updateOrCreate(['id' => $value['id']], $value);
        }

        $menu_delete = [  
            [
                'id' => 26,
                'name' => 'SHE & Proper',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => '#',
                'order_no' => 26,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 0
            ],          
            [
                'id' => 65,
                'name' => 'Master Area Equipment Inspection',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => '#',
                'order_no' => 66,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 55
            ],
            [
                'id' => 66,
                'name' => 'Master Area Equipment Inspection',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => '#',
                'order_no' => 66,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 55
            ],
            [
                'id' => 79,
                'name' => 'Fire Equipment',
                'description' => '-',
                'permission_id' => 1, //default permission, must be updated once permission has been created
                'url' => '#',
                'order_no' => 9,
                'icon' => 'fas fa-bookmark',
                'parent_id' => 27
            ],
        ];

        foreach ($menu_delete as $value) {
            Menu::where("id", $value["id"])->delete();
        }

        $this->command->info("Menu berhasil ditambahkan");
    }
}

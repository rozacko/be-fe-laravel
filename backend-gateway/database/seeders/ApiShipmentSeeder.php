<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\M_ApiShipment;

class ApiShipmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $data = [
            [
                'api_name' => 'Shipment Realisasi',
                'api_url' => 'http://csms2-api.sig.id/sdonline/service/get_realisasi.php',
                'api_method' => 'POST',
                'api_params' => '{"token":"EKH0U6XSMB","X_TGL1":"{{_tgl_sync}}{{Y-m-d}}","X_TGL2":"{{_tgl_sync}}{{Y-m-d}}","X_VKORG":"7000","X_WERKS":"7403","X_STATUS":"70"}',
                'created_by' => '2'
            ],
            [
                'api_name' => 'Shipment Inbound',
                'api_url' => 'https://csms2-api.sig.id/sdonline/service/get_inbound.php',
                'api_method' => 'POST',
                'api_params' => '{"token":"A5UO54RZX7","tgl_create1":"{{_tgl_sync}}{{d-m-Y}}","tgl_create2":"{{_tgl_sync}}{{d-m-Y}}","plant":"7702","company":"7000"}',
                'created_by' => '2'
            ],
        ];

        foreach($data as $dt){
            M_ApiShipment::updateOrCreate(['api_name' => $dt['api_name']], $dt);
        }
    }
}

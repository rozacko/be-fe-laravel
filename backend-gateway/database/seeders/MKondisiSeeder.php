<?php

namespace Database\Seeders;

use App\Models\MKondisi;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class MKondisiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $kondisi = [
            [
                'kd_kondisi' => '1', 
                'nm_kondisi' => 'GOOD',
                'warna' => '00FF00',
            ],
            [
                'kd_kondisi' => '2', 
                'nm_kondisi' => 'LOW RISK',
                'warna' => 'FFFF00',
            ],
            [
                'kd_kondisi' => '3', 
                'nm_kondisi' => 'MED RISK',
                'warna' => 'FF7F00',
            ],
            [
                'kd_kondisi' => '4', 
                'nm_kondisi' => 'HIGH RISK',
                'warna' => 'FF0000',
            ],
        ];

        foreach($kondisi as $key=>$item){
            $item['created_by'] = 2;
            MKondisi::updateOrCreate(['kd_kondisi' => $item['kd_kondisi']], $item);
        }
    }
}

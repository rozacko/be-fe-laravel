<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * permission for developer
         */
        $data = [
            'code' => 'developer',
            'name' => 'Developer',
            'guard_name' => 'api'
        ];
        $model = Role::updateOrCreate(['name' => $data['name']], $data);
        $permissions = ['create_user', 'view_user', 'update_user', 'delete_user', 'create_role', 'view_role', 'update_role', 'delete_role',
                        'create_opc','view_opc','update_opc','create_overhaul_prep','view_overhaul_prep','update_overhaul_prep','create_overhaul_prog',
                        'view_overhaul_prog','update_overhaul_prog'];
        $permissionId = Permission::whereIn('unique_code', $permissions)->get()->pluck('id');
        $this->command->info($model);
        $model->syncPermissions($permissionId);

        /**
         * permission for admin
         */
        $data = [
            'code' => 'administrator',
            'name' => 'Administrator',
            'guard_name' => 'api'
        ];
        $model = Role::updateOrCreate(['name' => $data['name']], $data);
        $permissions = ['create_user', 'view_user', 'update_user', 'delete_user', 'create_role', 'view_role', 'update_role', 'delete_role', 
                        'create_opc','view_opc','update_opc','create_overhaul_prep','view_overhaul_prep','update_overhaul_prep','create_overhaul_prog',
                        'view_overhaul_prog','update_overhaul_prog'];
        $permissionId = Permission::whereIn('unique_code', $permissions)->get()->pluck('id');
        $this->command->info($model);
        $model->syncPermissions($permissionId);
    }
}

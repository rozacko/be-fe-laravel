<?php

namespace Database\Seeders;

use App\Models\Route;
use Illuminate\Database\Seeder;

class RouteSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $routes = [
            ["unique_code" => "get_all_user", "name" => "get all user", "http_method" => "get", "url" => "user", "path" => "Users\UserController@index", "middleware" => null],
            ["unique_code" => "get_detail_user", "name" => "get detail user", "http_method" => "get", "url" => "user/{uuid}", "path" => "Users\UserController@show", "middleware" => null],
            ["unique_code" => "create_user", "name" => "create user", "http_method" => "post", "url" => "user", "path" => "Users\UserController@store", "middleware" => null],
            ["unique_code" => "update_user", "name" => "update user", "http_method" => "put", "url" => "user/{uuid}", "path" => "Users\UserController@update", "middleware" => null],
            ["unique_code" => "delete_user", "name" => "delete user", "http_method" => "delete", "url" => "user/{uuid}", "path" => "Users\UserController@destroy", "middleware" => null],
            ["unique_code" => "sync_users_role", "name" => "sync user's role", "http_method" => "post", "url" => "user/{uuid}/role", "path" => "Users\UserController@syncRole", "middleware" => null],

            ["unique_code" => "get_all_action", "name" => "get all action", "http_method" => "get", "url" => "action", "path" => "Users\ActionController@index", "middleware" => null],
            ["unique_code" => "get_detail_action", "name" => "get detail action", "http_method" => "get", "url" => "action/{uuid}", "path" => "Users\ActionController@show", "middleware" => null],

            ["unique_code" => "get_all_role", "name" => "get all role", "http_method" => "get", "url" => "role", "path" => "Users\RoleController@index", "middleware" => null],
            ["unique_code" => "get_detail_role", "name" => "get detail role", "http_method" => "get", "url" => "role/{uuid}", "path" => "Users\RoleController@show", "middleware" => null],
            ["unique_code" => "create_role", "name" => "create role", "http_method" => "post", "url" => "role", "path" => "Users\RoleController@store", "middleware" => null],
            ["unique_code" => "update_role", "name" => "update role", "http_method" => "put", "url" => "role/{uuid}", "path" => "Users\RoleController@update", "middleware" => null],
            ["unique_code" => "delete_role", "name" => "delete role", "http_method" => "delete", "url" => "role/{uuid}", "path" => "Users\RoleController@destroy", "middleware" => null],
            ["unique_code" => "add_role_permission", "name" => "add role's permission", "http_method" => "post", "url" => "role/{uuid}/permission", "path" => "Users\RoleController@addPermission", "middleware" => null],
            ["unique_code" => "get_role_permission", "name" => "get role's permission", "http_method" => "get", "url" => "role/{uuid}/permission", "path" => "Users\RoleController@getPermission", "middleware" => null],
            ["unique_code" => "add_user_to_role", "name" => "add user to role", "http_method" => "post", "url" => "role/{uuid}/user", "path" => "Users\RoleController@addUser", "middleware" => null],
            ["unique_code" => "remove_user_from_role", "name" => "remove user from role", "http_method" => "delete", "url" => "role/{uuid}/user", "path" => "Users\RoleController@removeUser", "middleware" => null],

            // Master Data
            // Master Plant Area
            ["unique_code" => "get_all_plant_area", "name" => "get all plant area", "http_method" => "get", "url" => "plant_area", "path" => "MasterData\MPlantAreaController@index", "middleware" => null],
            ["unique_code" => "get_detail_plant_area", "name" => "get detail plant area", "http_method" => "get", "url" => "plant_area/{id}", "path" => "MasterData\MPlantAreaController@show", "middleware" => null],
            ["unique_code" => "create_plant_area", "name" => "create plant area", "http_method" => "post", "url" => "plant_area", "path" => "MasterData\MPlantAreaController@store", "middleware" => null],
            ["unique_code" => "update_plant_area", "name" => "update plant area", "http_method" => "put", "url" => "plant_area/{id}", "path" => "MasterData\MPlantAreaController@update", "middleware" => null],
            ["unique_code" => "delete_plant_area", "name" => "delete plant area", "http_method" => "delete", "url" => "plant_area/{id}", "path" => "MasterData\MPlantAreaController@destroy", "middleware" => null],
            // End Master Plant Area
            // Master Costcenter
            ["unique_code" => "get_all_costcenter", "name" => "get all costcenter", "http_method" => "get", "url" => "costcenter", "path" => "MasterData\MCostcenterController@index", "middleware" => null],
            ["unique_code" => "get_detail_costcenter", "name" => "get detail costcenter", "http_method" => "get", "url" => "costcenter/{id}", "path" => "MasterData\MCostcenterController@show", "middleware" => null],
            ["unique_code" => "create_costcenter", "name" => "create costcenter", "http_method" => "post", "url" => "costcenter", "path" => "MasterData\MCostcenterController@store", "middleware" => null],
            ["unique_code" => "update_costcenter", "name" => "update costcenter", "http_method" => "put", "url" => "costcenter/{id}", "path" => "MasterData\MCostcenterController@update", "middleware" => null],
            ["unique_code" => "delete_costcenter", "name" => "delete costcenter", "http_method" => "delete", "url" => "costcenter/{id}", "path" => "MasterData\MCostcenterController@destroy", "middleware" => null],
            // End Master Costcenter
            // Master Costelement
            ["unique_code" => "get_all_costelement", "name" => "get all costelement", "http_method" => "get", "url" => "costelement", "path" => "MasterData\MCostelementController@index", "middleware" => null],
            ["unique_code" => "get_detail_costelement", "name" => "get detail costelement", "http_method" => "get", "url" => "costelement/{id}", "path" => "MasterData\MCostelementController@show", "middleware" => null],
            ["unique_code" => "create_costelement", "name" => "create costelement", "http_method" => "post", "url" => "costelement", "path" => "MasterData\MCostelementController@store", "middleware" => null],
            ["unique_code" => "update_costelement", "name" => "update costelement", "http_method" => "put", "url" => "costelement/{id}", "path" => "MasterData\MCostelementController@update", "middleware" => null],
            ["unique_code" => "delete_costelement", "name" => "delete costelement", "http_method" => "delete", "url" => "costelement/{id}", "path" => "MasterData\MCostelementController@destroy", "middleware" => null],
            // End Master Costelement
            // Master Plant
            ["unique_code" => "get_all_plant", "name" => "get all plant", "http_method" => "get", "url" => "plant", "path" => "MasterData\MPlantController@index", "middleware" => null],
            ["unique_code" => "get_detail_plant", "name" => "get detail plant", "http_method" => "get", "url" => "plant/{id}", "path" => "MasterData\MPlantController@show", "middleware" => null],
            ["unique_code" => "create_plant", "name" => "create plant", "http_method" => "post", "url" => "plant", "path" => "MasterData\MPlantController@store", "middleware" => null],
            ["unique_code" => "update_plant", "name" => "update plant", "http_method" => "put", "url" => "plant/{id}", "path" => "MasterData\MPlantController@update", "middleware" => null],
            ["unique_code" => "delete_plant", "name" => "delete plant", "http_method" => "delete", "url" => "plant/{id}", "path" => "MasterData\MPlantController@destroy", "middleware" => null],
            // End Master Plant
            // Master Workgroup
            ["unique_code" => "get_all_workgroup", "name" => "get all workgroup", "http_method" => "get", "url" => "workgroup", "path" => "MasterData\MWorkgroupController@index", "middleware" => null],
            ["unique_code" => "get_detail_workgroup", "name" => "get detail workgroup", "http_method" => "get", "url" => "workgroup/{id}", "path" => "MasterData\MWorkgroupController@show", "middleware" => null],
            ["unique_code" => "create_workgroup", "name" => "create workgroup", "http_method" => "post", "url" => "workgroup", "path" => "MasterData\MWorkgroupController@store", "middleware" => null],
            ["unique_code" => "update_workgroup", "name" => "update workgroup", "http_method" => "put", "url" => "workgroup/{id}", "path" => "MasterData\MWorkgroupController@update", "middleware" => null],
            ["unique_code" => "delete_workgroup", "name" => "delete workgroup", "http_method" => "delete", "url" => "workgroup/{id}", "path" => "MasterData\MWorkgroupController@destroy", "middleware" => null],
            // End Master Workgroup
            // Master Project Capex
            ["unique_code" => "get_all_capex", "name" => "get all capex", "http_method" => "get", "url" => "projectcapex", "path" => "MasterData\MProjectCapexController@index", "middleware" => null],
            ["unique_code" => "get_detail_capex", "name" => "get detail capex", "http_method" => "get", "url" => "projectcapex/{id}", "path" => "MasterData\MProjectCapexController@show", "middleware" => null],
            ["unique_code" => "create_capex", "name" => "create capex", "http_method" => "post", "url" => "projectcapex", "path" => "MasterData\MProjectCapexController@store", "middleware" => null],
            ["unique_code" => "update_capex", "name" => "update capex", "http_method" => "put", "url" => "projectcapex/{id}", "path" => "MasterData\MProjectCapexController@update", "middleware" => null],
            ["unique_code" => "delete_capex", "name" => "delete capex", "http_method" => "delete", "url" => "projectcapex/{id}", "path" => "MasterData\MProjectCapexController@destroy", "middleware" => null],
            // End Master Project Capex
            // Master Equipment Inspection
            ["unique_code" => "get_all_eqinspection", "name" => "get all eqinspection", "http_method" => "get", "url" => "eqinspection", "path" => "MasterData\MEqInspectionController@index", "middleware" => null],
            ["unique_code" => "get_detail_eqinspection", "name" => "get detail eqinspection", "http_method" => "get", "url" => "eqinspection/{id}", "path" => "MasterData\MEqInspectionController@show", "middleware" => null],
            ["unique_code" => "create_eqinspection", "name" => "create eqinspection", "http_method" => "post", "url" => "eqinspection", "path" => "MasterData\MEqInspectionController@store", "middleware" => null],
            ["unique_code" => "update_eqinspection", "name" => "update eqinspection", "http_method" => "put", "url" => "eqinspection/{id}", "path" => "MasterData\MEqInspectionController@update", "middleware" => null],
            ["unique_code" => "delete_eqinspection", "name" => "delete eqinspection", "http_method" => "delete", "url" => "eqinspection/{id}", "path" => "MasterData\MEqInspectionController@destroy", "middleware" => null],
            // End Master Equipment Inspection
            // Master Kondisi
            ["unique_code" => "get_all_kondisi", "name" => "get all kondisi", "http_method" => "get", "url" => "kondisi", "path" => "MasterData\MKondisiController@index", "middleware" => null],
            ["unique_code" => "get_detail_kondisi", "name" => "get detail kondisi", "http_method" => "get", "url" => "kondisi/{id}", "path" => "MasterData\MKondisiController@show", "middleware" => null],
            ["unique_code" => "create_kondisi", "name" => "create kondisi", "http_method" => "post", "url" => "kondisi", "path" => "MasterData\MKondisiController@store", "middleware" => null],
            ["unique_code" => "update_kondisi", "name" => "update kondisi", "http_method" => "put", "url" => "kondisi/{id}", "path" => "MasterData\MKondisiController@update", "middleware" => null],
            ["unique_code" => "delete_kondisi", "name" => "delete kondisi", "http_method" => "delete", "url" => "kondisi/{id}", "path" => "MasterData\MKondisiController@destroy", "middleware" => null],
            // End Master Kondisi
            // Master Area
            ["unique_code" => "get_all_area", "name" => "get all area", "http_method" => "get", "url" => "area", "path" => "MasterData\MAreaController@index", "middleware" => null],
            ["unique_code" => "get_detail_area", "name" => "get detail area", "http_method" => "get", "url" => "area/{id}", "path" => "MasterData\MAreaController@show", "middleware" => null],
            ["unique_code" => "create_area", "name" => "create area", "http_method" => "post", "url" => "area", "path" => "MasterData\MAreaController@store", "middleware" => null],
            ["unique_code" => "update_area", "name" => "update area", "http_method" => "put", "url" => "area/{id}", "path" => "MasterData\MAreaController@update", "middleware" => null],
            ["unique_code" => "delete_area", "name" => "delete area", "http_method" => "delete", "url" => "area/{id}", "path" => "MasterData\MAreaController@destroy", "middleware" => null],
            // End Master Area
            // OPC Sync
            ["unique_code" => "get_all_opc_tag", "name" => "get all opc tag", "http_method" => "get", "url" => "opc", "path" => "Synchronization\SyncOpcController@index", "middleware" => null],
            ["unique_code" => "get_detail_tag", "name" => "get detail tag", "http_method" => "get", "url" => "opc/{uuid}", "path" => "Synchronization\SyncOpcController@show", "middleware" => null],
            ["unique_code" => "get_opc_data", "name" => "get opc data", "http_method" => "get", "url" => "opc/getData", "path" => "Synchronization\SyncOpcController@getData", "middleware" => null],
            ["unique_code" => "update_opc_tag", "name" => "update opc tag", "http_method" => "put", "url" => "opc/{uuid}", "path" => "Synchronization\SyncOpcController@update", "middleware" => null],
            ["unique_code" => "sync_opc_tag", "name" => "sync opc tag", "http_method" => "post", "url" => "opc/synctag", "path" => "Synchronization\SyncOpcController@syncTag", "middleware" => null],
            ["unique_code" => "sync_opc_daily", "name" => "sync opc daily", "http_method" => "post", "url" => "opc/syncdaily", "path" => "Synchronization\SyncOpcController@syncDaily", "middleware" => null],
            ["unique_code" => "sync_opc_hourly", "name" => "sync opc hourly", "http_method" => "post", "url" => "opc/synchourly", "path" => "Synchronization\SyncOpcController@syncHourly", "middleware" => null],
            ["unique_code" => "sync_opc_minutely", "name" => "sync opc minutely", "http_method" => "post", "url" => "opc/syncminutely", "path" => "Synchronization\SyncOpcController@syncMinutely", "middleware" => null],
            // End
            //Overhaul Preparation
            ["unique_code" => "get_overhaul_opex_prep", "name" => "get budget opex overhaul preparation", "http_method" => "get", "url" => "overhaul/preparation/opex", "path" => "Overhaul\OverhaulPreparationController@index", "middleware" => null],
            ["unique_code" => "get_overhaul_chart_part_prep", "name" => "get chart overahul preparation part", "http_method" => "get", "url" => "overhaul/preparation/partchart/", "path" => "Overhaul\OverhaulPreparationController@getChartPart", "middleware" => null],
            ["unique_code" => "get_overhaul_table_part_prep", "name" => "get table overahul preparation part", "http_method" => "get", "url" => "overhaul/preparation/parttable/", "path" => "Overhaul\OverhaulPreparationController@getTablePart", "middleware" => null],
            ["unique_code" => "get_overhaul_timeline", "name" => "get overhaul timeline", "http_method" => "get", "url" => "overhaul/preparation/timeline", "path" => "Overhaul\OverhaulPreparationController@timeLine", "middleware" => null],
            ["unique_code" => "template_overhaul_preparation", "name" => "template overhaul preparation ", "http_method" => "get", "url" => "overhaul/preparation/import", "path" => "Overhaul\OverhaulPreparationController@templatePreparation", "middleware" => null],
            ["unique_code" => "import_overhaul_preparation", "name" => "upload data overhaul preparation ", "http_method" => "post", "url" => "overhaul/preparation/import", "path" => "Overhaul\OverhaulPreparationController@uploadData", "middleware" => null],
            ["unique_code" => "template_overhaul_budget", "name" => "template overhaul budget preparation ", "http_method" => "get", "url" => "overhaul/preparation/budget", "path" => "Overhaul\OverhaulPreparationController@templateBudget", "middleware" => null],
            ["unique_code" => "import_overhaul_budget", "name" => "upload data overhaul budget preparation ", "http_method" => "post", "url" => "overhaul/preparation/budget", "path" => "Overhaul\OverhaulPreparationController@uploadBudget", "middleware" => null],
            ["unique_code" => "get_data_overhaul_chart_budget", "name" => "get chart overhaul budget preparation ", "http_method" => "get", "url" => "overhaul/preparation/barchartbudget", "path" => "Overhaul\OverhaulPreparationController@barChartBudget", "middleware" => null],
            ["unique_code" => "get_data_overhaul_donut_budget", "name" => "get donut overhaul budget preparation ", "http_method" => "get", "url" => "overhaul/preparation/partchartbudget", "path" => "Overhaul\OverhaulPreparationController@donutChartBudget", "middleware" => null],
            ["unique_code" => "get_data_overhaul_donut_opex", "name" => "get donut overhaul opex preparation ", "http_method" => "get", "url" => "overhaul/preparation/budgetopexdonut", "path" => "Overhaul\OverhaulPreparationController@budgetOpexChart", "middleware" => null],
            ["unique_code" => "get_data_overhaul_table_capex", "name" => "get donut overhaul table capex ", "http_method" => "get", "url" => "overhaul/preparation/tablecapex", "path" => "Overhaul\OverhaulPreparationController@tableCapex", "middleware" => null],
            ["unique_code" => "get_data_overhaul_table_issue", "name" => "get donut overhaul table issue ", "http_method" => "get", "url" => "overhaul/preparation/tableissue", "path" => "Overhaul\OverhaulPreparationController@tableIssue", "middleware" => null],

            // End Overhaul Preparation
            // Overhaul Progress
            ["unique_code" => "add_overhaul_periode", "name" => "add overhaul periode", "http_method" => "post", "url" => "overhaul/progress/periode", "path" => "Overhaul\OverhaulProgressController@storeDataPeriode", "middleware" => null],
            ["unique_code" => "get_overhaul_periode", "name" => "get overahul periode", "http_method" => "get", "url" => "overhaul/progress/periode/{plant}", "path" => "Overhaul\OverhaulProgressController@getDataPeriode", "middleware" => null],
            ["unique_code" => "get_overhaul_progress_major", "name" => "get table overahul progress major", "http_method" => "get", "url" => "overhaul/progress/major", "path" => "Overhaul\OverhaulProgressController@tableMajor", "middleware" => null],
            ["unique_code" => "get_overhaul_progress_minor", "name" => "get table overahul progress minor", "http_method" => "get", "url" => "overhaul/progress/minor", "path" => "Overhaul\OverhaulProgressController@tableMinor", "middleware" => null],
            ["unique_code" => "get_overhaul_activity", "name" => "get all overahul activity", "http_method" => "get", "url" => "overhaul/progress/activity/{plant}", "path" => "Overhaul\OverhaulProgressController@getPeriodeActivity", "middleware" => null],
            ["unique_code" => "add_overhaul_activity", "name" => "add overhaul activity", "http_method" => "post", "url" => "overhaul/progress/activity", "path" => "Overhaul\OverhaulProgressController@storeDataActivity", "middleware" => null],
            ["unique_code" => "add_overhaul_activity_progress", "name" => "add overhaul activity progress", "http_method" => "post", "url" => "overhaul/progress/progress", "path" => "Overhaul\OverhaulProgressController@storeDataProgress", "middleware" => null],
            ["unique_code" => "get_overhaul_progress_template", "name" => "get all overahul progress template", "http_method" => "get", "url" => "overhaul/progress/template/progress/{plant}", "path" => "Overhaul\OverhaulProgressController@getProgressTemplate", "middleware" => null],
            ["unique_code" => "upload_overhaul_progress", "name" => "upload overhaul progress", "http_method" => "post", "url" => "overhaul/progress/upload/progress", "path" => "Overhaul\OverhaulProgressController@uploadprogress", "middleware" => null],
            ["unique_code" => "get_overhaul_progress_timeline", "name" => "get all overahul progress timeline", "http_method" => "get", "url" => "overhaul/progress/timeline/{plant}", "path" => "Overhaul\OverhaulProgressController@getTimelineImage", "middleware" => null],
            ["unique_code" => "upload_overhaul_timeline", "name" => "upload overhaul progress timeline", "http_method" => "post", "url" => "overhaul/progress/timeline", "path" => "Overhaul\OverhaulProgressController@uploadTimeline", "middleware" => null],
            ["unique_code" => "get_overhaul_progress_dokumentasi", "name" => "get all overahul progress dokumentasi", "http_method" => "get", "url" => "overhaul/progress/dokumentasi/{plant}", "path" => "Overhaul\OverhaulProgressController@getDokumentasi", "middleware" => null],
            ["unique_code" => "get_overhaul_progress_chart", "name" => "get all overahul progress chart", "http_method" => "get", "url" => "overhaul/progress/chart/{plant}", "path" => "Overhaul\OverhaulProgressController@getChart", "middleware" => null],
            ["unique_code" => "get_overhaul_progress_unsafe", "name" => "get all overahul progress unsafe", "http_method" => "get", "url" => "overhaul/progress/unsafe", "path" => "Overhaul\OverhaulProgressController@getUnsafe", "middleware" => null],
            ["unique_code" => "get_overhaul_progress_opex", "name" => "get all overahul progress opex", "http_method" => "get", "url" => "overhaul/progress/opex", "path" => "Overhaul\OverhaulProgressController@getOpex", "middleware" => null],
            // End Overhaul Progress

            // Overhaul Final Report
            ["unique_code" => "get_final_report_overhaul", "name" => "get final report overhaul", "http_method" => "get", "url" => "overhaul/finalreport/", "path" => "Overhaul\OverhaulFinalReportController@index", "middleware" => null],
            ["unique_code" => "add_final_report_overhaul", "name" => "add final report overhaul", "http_method" => "post", "url" => "overhaul/finalreport/", "path" => "Overhaul\OverhaulFinalReportController@add", "middleware" => null],
            ["unique_code" => "delete_final_report_overhaul", "name" => "delete final report overhaul", "http_method" => "delete", "url" => "overhaul/finalreport/delete/{id}", "path" => "Overhaul\OverhaulFinalReportController@destroy", "middleware" => null],
            //End Overhaul Final Report

            //Import Report TPM
            ["unique_code" => "import_report_tpm", "name" => "import report tpm", "http_method" => "post", "url" => "tpm/importreport", "path" => "Tpm\TpmController@storeImportReport", "middleware" => null],
            ["unique_code" => "dashboard_tpm_implementation", "name" => "Dashboard TPM Implementation", "http_method" => "get", "url" => "tpm/scoreimplementation", "path" => "Tpm\TpmController@scoreImplementation", "middleware" => null],
            ["unique_code" => "focused_improvement", "name" => "Dashboard Focused Improvement", "http_method" => "get", "url" => "tpm/focusedimprovement", "path" => "Tpm\TpmController@focusedImprovement", "middleware" => null],
            ["unique_code" => "focus_area", "name" => "Dashboard Focus Area", "http_method" => "get", "url" => "tpm/focusarea", "path" => "Tpm\TpmController@focusArea", "middleware" => null],
            ["unique_code" => "genba_sga", "name" => "Dashboard Genba SGA", "http_method" => "get", "url" => "tpm/genbasga", "path" => "Tpm\TpmController@genbaSga", "middleware" => null],
            ["unique_code" => "partisipasi_member", "name" => "Dashboard Partisipasi Member TPM", "http_method" => "get", "url" => "tpm/partisipasiMember", "path" => "Tpm\TpmController@partisipasiMember", "middleware" => null],
            ["unique_code" => "download_template_tpm", "name" => "Download Template TPM", "http_method" => "get", "url" => "tpm/downloadTemplate", "path" => "Tpm\TpmController@downloadTemplate", "middleware" => null],
            ["unique_code" => "laporan_gugus", "name" => "List Laporan Gugus", "http_method" => "get", "url" => "tpm/listLaporangugus", "path" => "Tpm\GugusController@listLaporangugus", "middleware" => null],
            ["unique_code" => "laporan_kpi_member", "name" => "List Laporan KPI Member", "http_method" => "get", "url" => "tpm/listLaporankpimember", "path" => "Tpm\KPIMemberController@listLaporankpimember", "middleware" => null],
            ["unique_code" => "import_report_gugus", "name" => "import report gugus", "http_method" => "post", "url" => "tpm/importreportgugus", "path" => "Tpm\GugusController@storeImportReport", "middleware" => null],
            ["unique_code" => "import_report_kpi_member", "name" => "import report KPI Member", "http_method" => "post", "url" => "tpm/importreportkpimember", "path" => "Tpm\KPIMemberController@storeImportReport", "middleware" => null],
            ["unique_code" => "list_dashboard_tpm", "name" => "list dashboard TPM", "http_method" => "get", "url" => "tpm/listdashboardtpm", "path" => "Tpm\TpmController@listTPM", "middleware" => null],
            // End Report TPM
            // filter tpm
            ["unique_code" => "filter_fasilitator", "name" => "Filter Fasilitator", "http_method" => "get", "url" => "tpm/filterFasilitator", "path" => "Tpm\FilterController@filterFasilitator", "middleware" => null],
            ["unique_code" => "filter_gugus", "name" => "Filter Gugus", "http_method" => "get", "url" => "tpm/filterGugus", "path" => "Tpm\FilterController@filterGugus", "middleware" => null],
            ["unique_code" => "filter_organisasi", "name" => "Filter Organisasi", "http_method" => "get", "url" => "tpm/filterOrganisasi", "path" => "Tpm\FilterController@filterOrganisasi", "middleware" => null],
            ["unique_code" => "filter_unitkerja", "name" => "Filter Unit Kerja", "http_method" => "get", "url" => "tpm/filterUnitKerja", "path" => "Tpm\FilterController@filterUnitKerja", "middleware" => null],
            // end tpm
            // kpi direksi
            ["unique_code" => "import_kpi_direksi", "name" => "import kpi direksi", "http_method" => "post", "url" => "kpidireksi/import", "path" => "KpiDireksi\KpiDireksiController@storeImport", "middleware" => null],
            ["unique_code" => "list_kpi_direksi", "name" => "list kpi direksi", "http_method" => "get", "url" => "kpidireksi/list", "path" => "KpiDireksi\KpiDireksiController@list", "middleware" => null],
            ["unique_code" => "export_kpi_direksi", "name" => "export kpi direksi", "http_method" => "post", "url" => "kpidireksi/export", "path" => "KpiDireksi\KpiDireksiController@exportKPI", "middleware" => null],
            // end kpi direksi

            // export gugus
            ["unique_code" => "export_gugus", "name" => "Export Laporan Gugus", "http_method" => "post", "url" => "tpm/exportGugus", "path" => "Tpm\GugusController@exportGugus", "middleware" => null],

            //end gugus
            // Shipment Mgmt
            ["unique_code" => "get_shipment_list", "name" => "get all shipment list", "http_method" => "get", "url" => "shipment", "path" => "Synchronization\ShipmentSyncController@index", "middleware" => null],
            ["unique_code" => "sync_shipment_mgmt", "name" => "sync shipment mgmt", "http_method" => "post", "url" => "shipment/sync", "path" => "Synchronization\ShipmentSyncController@syncApi", "middleware" => null],
            // End Shipment Mgmt

            // RKAP PERFORMANCE IMPORT
            ["unique_code" => "rkap_performance_import_template", "name" => "import rkap performance template", "http_method" => "post", "url" => "rkap/rkap-performance-import", "path" => "Rkap\RkapPerformanceController@import", "middleware" => null],
            ["unique_code" => "rkap_performance_index", "name" => "rkap performance list datatable", "http_method" => "get", "url" => "rkap/rkap-performance", "path" => "Rkap\RkapPerformanceController@index", "middleware" => null],
            ["unique_code" => "rkap_performance_show", "name" => "Show rkap performance list datatable", "http_method" => "get", "url" => "rkap/rkap-performance/{id}", "path" => "Rkap\RkapPerformanceController@show", "middleware" => null],
            ["unique_code" => "rkap_performance_update", "name" => "Update rkap performance", "http_method" => "put", "url" => "rkap/rkap-performance/{id}", "path" => "Rkap\RkapPerformanceController@update", "middleware" => null],
            ["unique_code" => "rkap_performance_download", "name" => "Download rkap performance", "http_method" => "get", "url" => "rkap/rkap-performance-download", "path" => "Rkap\RkapPerformanceController@download", "middleware" => null],

            ["unique_code" => "rkap_prognose_performance_import_template", "name" => "import prognose performance template", "http_method" => "post", "url" => "rkap/prognose-performance-import", "path" => "Rkap\RkapPrognoseController@import", "middleware" => null],
            ["unique_code" => "rkap_prognose_performance_index", "name" => "list datatable prognose performance", "http_method" => "get", "url" => "rkap/prognose-performance", "path" => "Rkap\RkapPrognoseController@index", "middleware" => null],
            ["unique_code" => "rkap_prognose_performance_show", "name" => "show data prognose performance", "http_method" => "get", "url" => "rkap/prognose-performance/{id}", "path" => "Rkap\RkapPrognoseController@show", "middleware" => null],
            ["unique_code" => "rkap_prognose_performance_update", "name" => "Update data prognose performance", "http_method" => "put", "url" => "rkap/prognose-performance/{id}", "path" => "Rkap\RkapPrognoseController@update", "middleware" => null],
            ["unique_code" => "rkap_prognose_performance_download", "name" => "download prognose performance", "http_method" => "get", "url" => "rkap/prognose-performance-download", "path" => "Rkap\RkapPrognoseController@download", "middleware" => null],


            ["unique_code" => "rkap_inventory_store_import", "name" => "Rkap Inventory Store Import File ", "http_method" => "post", "url" => "rkap-prognose/rkap-inventory-import", "path" => "Rkap\RkapInventoryController@import", "middleware" => null],
            ["unique_code" => "rkap_inventory_index", "name" => "Rkap Inventory List DataTable", "http_method" => "get", "url" => "rkap-prognose/rkap-inventory", "path" => "Rkap\RkapInventoryController@index", "middleware" => null],
            ["unique_code" => "rkap_inventory_show", "name" => "Rkap Inventory Show Detail Data", "http_method" => "get", "url" => "rkap-prognose/rkap-inventory/{uuid}", "path" => "Rkap\RkapInventoryController@show", "middleware" => null],
            ["unique_code" => "rkap_inventory_store", "name" => "Rkap Inventory Store / Create Data", "http_method" => "post", "url" => "rkap-prognose/rkap-inventory", "path" => "Rkap\RkapInventoryController@store", "middleware" => null],
            ["unique_code" => "rkap_inventory_update", "name" => "Rkap Inventory Update", "http_method" => "put", "url" => "rkap-prognose/rkap-inventory/{uuid}", "path" => "Rkap\RkapInventoryController@update", "middleware" => null],
            ["unique_code" => "rkap_inventory_destroy", "name" => "Rkap Inventory Destroy / Delete", "http_method" => "delete", "url" => "rkap-prognose/rkap-inventory/{uuid}", "path" => "Rkap\RkapInventoryController@destroy", "middleware" => null],
            ["unique_code" => "rkap_inventory_export", "name" => "Rkap Inventory Export", "http_method" => "get", "url" => "rkap-prognose/rkap-inventory-download", "path" => "Rkap\RkapInventoryController@download", "middleware" => null],

            ["unique_code" => "prognose_inventory_store_import", "name" => "Prognose Inventory Store Import File ", "http_method" => "post", "url" => "rkap-prognose/prognose-inventory-import", "path" => "Rkap\PrognoseInventoryController@import", "middleware" => null],
            ["unique_code" => "prognose_inventory_index", "name" => "Prognose Inventory List DataTable", "http_method" => "get", "url" => "rkap-prognose/prognose-inventory", "path" => "Rkap\PrognoseInventoryController@index", "middleware" => null],
            ["unique_code" => "prognose_inventory_show", "name" => "Prognose Inventory Show Detail Data", "http_method" => "get", "url" => "rkap-prognose/prognose-inventory/{uuid}", "path" => "Rkap\PrognoseInventoryController@show", "middleware" => null],
            ["unique_code" => "prognose_inventory_store", "name" => "Prognose Inventory Store / Create Data", "http_method" => "post", "url" => "rkap-prognose/prognose-inventory", "path" => "Rkap\PrognoseInventoryController@store", "middleware" => null],
            ["unique_code" => "prognose_inventory_update", "name" => "Prognose Inventory Update", "http_method" => "put", "url" => "rkap-prognose/prognose-inventory/{uuid}", "path" => "Rkap\PrognoseInventoryController@update", "middleware" => null],
            ["unique_code" => "prognose_inventory_destroy", "name" => "Prognose Inventory Destroy / Delete", "http_method" => "delete", "url" => "rkap-prognose/prognose-inventory/{uuid}", "path" => "Rkap\PrognoseInventoryController@destroy", "middleware" => null],
            ["unique_code" => "prognose_inventory_export", "name" => "Prognose Inventory Export", "http_method" => "get", "url" => "rkap-prognose/prognose-inventory-download", "path" => "Rkap\PrognoseInventoryController@download", "middleware" => null],
            // END RKAP PERFORMANCE IMPORT

            // DATA KOREKSI - DATA COGM
            ["unique_code" => "data_koreksi_cogm_index", "name" => "data koreksi cogm index", "http_method" => "get", "url" => "data-koreksi/cogm", "path" => "DataKoreksi\CogmController@index", "middleware" => null],
            ["unique_code" => "data_koreksi_cogm_show", "name" => "data koreksi cogm show", "http_method" => "get", "url" => "data-koreksi/cogm/{uuid}", "path" => "DataKoreksi\CogmController@show", "middleware" => null],
            ["unique_code" => "data_koreksi_cogm_update", "name" => "data koreksi cogm update", "http_method" => "put", "url" => "data-koreksi/cogm/{uuid}", "path" => "DataKoreksi\CogmController@update", "middleware" => null],
            ["unique_code" => "data_koreksi_cogm_delete", "name" => "data koreksi cogm delete", "http_method" => "delete", "url" => "data-koreksi/cogm/{uuid}", "path" => "DataKoreksi\CogmController@destroy", "middleware" => null],
            ["unique_code" => "data_koreksi_cogm_import", "name" => "data koreksi cogm import", "http_method" => "post", "url" => "data-koreksi/cogm-import", "path" => "DataKoreksi\CogmController@import", "middleware" => null],

            ["unique_code" => "data_koreksi_nilai_inventory_import", "name" => "data koreksi Nilai Inventory import", "http_method" => "post", "url" => "data-koreksi/nilai-inventory-import", "path" => "DataKoreksi\NilaiInventoryController@import", "middleware" => null],
            ["unique_code" => "data_koreksi_nilai_inventory_index", "name" => "data koreksi Nilai Inventory List DataTable", "http_method" => "get", "url" => "data-koreksi/nilai-inventory", "path" => "DataKoreksi\NilaiInventoryController@index", "middleware" => null],
            ["unique_code" => "data_koreksi_nilai_inventory_show", "name" => "data koreksi Nilai Inventory Show Detail", "http_method" => "get", "url" => "data-koreksi/nilai-inventory/{uuid}", "path" => "DataKoreksi\NilaiInventoryController@show", "middleware" => null],
            ["unique_code" => "data_koreksi_nilai_inventory_create", "name" => "data koreksi Nilai Inventory Create Data", "http_method" => "post", "url" => "data-koreksi/nilai-inventory", "path" => "DataKoreksi\NilaiInventoryController@store", "middleware" => null],
            ["unique_code" => "data_koreksi_nilai_inventory_update", "name" => "data koreksi Nilai Inventory Update Data", "http_method" => "put", "url" => "data-koreksi/nilai-inventory/{uuid}", "path" => "DataKoreksi\NilaiInventoryController@update", "middleware" => null],
            ["unique_code" => "data_koreksi_nilai_inventory_delete", "name" => "data koreksi Nilai Inventory Delete Data", "http_method" => "delete", "url" => "data-koreksi/nilai-inventory/{uuid}", "path" => "DataKoreksi\NilaiInventoryController@destroy", "middleware" => null],
            ["unique_code" => "data_koreksi_nilai_inventory_download", "name" => "data koreksi Nilai Inventory Download Data", "http_method" => "get", "url" => "data-koreksi/nilai-inventory-download", "path" => "DataKoreksi\NilaiInventoryController@download", "middleware" => null],

            ["unique_code" => "data_koreksi_order_task_index", "name" => "data koreksi order-task Index", "http_method" => "get", "url" => "data-koreksi/order-task", "path" => "DataKoreksi\OrderTaskController@index", "middleware" => null],
            ["unique_code" => "data_koreksi_order_task_show", "name" => "data koreksi order-task show", "http_method" => "get", "url" => "data-koreksi/order-task/{uuid}", "path" => "DataKoreksi\OrderTaskController@show", "middleware" => null],
            ["unique_code" => "data_koreksi_order_task_import", "name" => "data koreksi order-task import", "http_method" => "post", "url" => "data-koreksi/order-task-import", "path" => "DataKoreksi\OrderTaskController@import", "middleware" => null],
            // ["unique_code" => "data_koreksi_order_task_create", "name" => "data koreksi order-task create", "http_method" => "post", "url" => "data-koreksi/order-task", "path" => "DataKoreksi\OrderTaskController@store", "middleware" => null],
            // ["unique_code" => "data_koreksi_order_task_update", "name" => "data koreksi order-task update", "http_method" => "put", "url" => "data-koreksi/order-task/{uuid}", "path" => "DataKoreksi\OrderTaskController@update", "middleware" => null],
            ["unique_code" => "data_koreksi_order_task_delete", "name" => "data koreksi order-task delete", "http_method" => "delete", "url" => "data-koreksi/order-task/{uuid}", "path" => "DataKoreksi\OrderTaskController@destroy", "middleware" => null],
            ["unique_code" => "data_koreksi_order_task_download", "name" => "data koreksi order-task download", "http_method" => "get", "url" => "data-koreksi/order-task-download", "path" => "DataKoreksi\OrderTaskController@download", "middleware" => null],

            ["unique_code" => "data_koreksi_notifikasi_index", "name" => "data koreksi notifikasi Index", "http_method" => "get", "url" => "data-koreksi/notifikasi", "path" => "DataKoreksi\NotifikasiController@index", "middleware" => null],
            ["unique_code" => "data_koreksi_notifikasi_show", "name" => "data koreksi notifikasi show", "http_method" => "get", "url" => "data-koreksi/notifikasi/{uuid}", "path" => "DataKoreksi\NotifikasiController@show", "middleware" => null],
            ["unique_code" => "data_koreksi_notifikasi_import", "name" => "data koreksi notifikasi import", "http_method" => "post", "url" => "data-koreksi/notifikasi-import", "path" => "DataKoreksi\NotifikasiController@import", "middleware" => null],
            // ["unique_code" => "data_koreksi_notifikasi_create", "name" => "data koreksi notifikasi create", "http_method" => "post", "url" => "data-koreksi/notifikasi", "path" => "DataKoreksi\NotifikasiController@store", "middleware" => null],
            // ["unique_code" => "data_koreksi_notifikasi_update", "name" => "data koreksi notifikasi update", "http_method" => "put", "url" => "data-koreksi/notifikasi/{uuid}", "path" => "DataKoreksi\NotifikasiController@update", "middleware" => null],
            ["unique_code" => "data_koreksi_notifikasi_delete", "name" => "data koreksi notifikasi delete", "http_method" => "delete", "url" => "data-koreksi/notifikasi/{uuid}", "path" => "DataKoreksi\NotifikasiController@destroy", "middleware" => null],
            ["unique_code" => "data_koreksi_notifikasi_download", "name" => "data koreksi notifikasi download", "http_method" => "get", "url" => "data-koreksi/notifikasi-download", "path" => "DataKoreksi\NotifikasiController@download", "middleware" => null],

            ["unique_code" => "data_koreksi_sparepart_jasa_index", "name" => "data koreksi Spare Part Jasa Index", "http_method" => "get", "url" => "data-koreksi/sparepart-jasa", "path" => "DataKoreksi\SparepartJasaController@index", "middleware" => null],
            ["unique_code" => "data_koreksi_sparepart_jasa_show", "name" => "data koreksi Spare Part Jasa show", "http_method" => "get", "url" => "data-koreksi/sparepart-jasa/{uuid}", "path" => "DataKoreksi\SparepartJasaController@show", "middleware" => null],
            ["unique_code" => "data_koreksi_sparepart_jasa_import", "name" => "data koreksi Spare Part Jasa import", "http_method" => "post", "url" => "data-koreksi/sparepart-jasa-import", "path" => "DataKoreksi\SparepartJasaController@import", "middleware" => null],
            // ["unique_code" => "data_koreksi_sparepart_jasa_create", "name" => "data koreksi Spare Part Jasa create", "http_method" => "post", "url" => "data-koreksi/sparepart-jasa", "path" => "DataKoreksi\SparepartJasaController@store", "middleware" => null],
            // ["unique_code" => "data_koreksi_sparepart_jasa_update", "name" => "data koreksi Spare Part Jasa update", "http_method" => "put", "url" => "data-koreksi/sparepart-jasa/{uuid}", "path" => "DataKoreksi\SparepartJasaController@update", "middleware" => null],
            ["unique_code" => "data_koreksi_sparepart_jasa_delete", "name" => "data koreksi Spare Part Jasa delete", "http_method" => "delete", "url" => "data-koreksi/sparepart-jasa/{uuid}", "path" => "DataKoreksi\SparepartJasaController@destroy", "middleware" => null],
            ["unique_code" => "data_koreksi_sparepart_jasa_download", "name" => "data koreksi Spare Part Jasa download", "http_method" => "get", "url" => "data-koreksi/sparepart-jasa-download", "path" => "DataKoreksi\SparepartJasaController@download", "middleware" => null],

            // RKAP MAINTENANCE
            ["unique_code" => "rkap_maintenance_import", "name" => "import rkap Maintenance template", "http_method" => "post", "url" => "rkap/rkap-maintenance-import", "path" => "Rkap\RkapMaintenanceController@import", "middleware" => null],
            ["unique_code" => "rkap_maintenance_index", "name" => "rkap Maintenance list datatable", "http_method" => "get", "url" => "rkap/rkap-maintenance", "path" => "Rkap\RkapMaintenanceController@index", "middleware" => null],
            ["unique_code" => "rkap_maintenance_show", "name" => "Show rkap Maintenance list datatable", "http_method" => "get", "url" => "rkap/rkap-maintenance/{id}", "path" => "Rkap\RkapMaintenanceController@show", "middleware" => null],
            ["unique_code" => "rkap_maintenance_update", "name" => "Update rkap Maintenance", "http_method" => "get", "url" => "rkap/rkap-maintenance/{id}", "path" => "Rkap\RkapMaintenanceController@update", "middleware" => null],
            ["unique_code" => "rkap_maintenance_download", "name" => "Downlaod rkap Maintenance", "http_method" => "get", "url" => "rkap/rkap-maintenance-download", "path" => "Rkap\RkapMaintenanceController@download", "middleware" => null],

            ["unique_code" => "rkap_prognose_maintenance_import", "name" => "import rkap Prognose Maintenance template", "http_method" => "post", "url" => "rkap/rkap-prognose-maintenance-import", "path" => "Rkap\PrognoseMaintenanceController@import", "middleware" => null],
            ["unique_code" => "rkap_prognose_maintenance_index", "name" => "rkap Prognose Maintenance list datatable", "http_method" => "get", "url" => "rkap/rkap-prognose-maintenance", "path" => "Rkap\PrognoseMaintenanceController@index", "middleware" => null],
            ["unique_code" => "rkap_prognose_maintenance_show", "name" => "Show rkap Prognose Maintenance list datatable", "http_method" => "get", "url" => "rkap/rkap-prognose-maintenance/{id}", "path" => "Rkap\PrognoseMaintenanceController@show", "middleware" => null],
            ["unique_code" => "rkap_prognose_maintenance_update", "name" => "Update rkap Prognose Maintenance", "http_method" => "get", "url" => "rkap/rkap-prognose-maintenance/{id}", "path" => "Rkap\PrognoseMaintenanceController@update", "middleware" => null],
            ["unique_code" => "rkap_prognose_maintenance_download", "name" => "Download rkap Prognose Maintenance", "http_method" => "get", "url" => "rkap/rkap-prognose-maintenance-download", "path" => "Rkap\PrognoseMaintenanceController@download", "middleware" => null],
            // END RKAP MAINTENANCE

            //CAPEX
            ["unique_code" => "rkap_capex_import", "name" => "Import Rkap Capex Template", "http_method" => "post", "url" => "rkap/rkap-capex-import", "path" => "Capex\RkapCapexController@import", "middleware" => null],
            ["unique_code" => "rkap_capex_index", "name" => "Rkap Capex List Datatable", "http_method" => "get", "url" => "rkap/rkap-capex", "path" => "Capex\RkapCapexController@index", "middleware" => null],
            ["unique_code" => "rkap_capex_get_project", "name" => "Rkap Capex Get No Project", "http_method" => "get", "url" => "rkap/rkap-capex-project", "path" => "Capex\RkapCapexController@getProject", "middleware" => null],
            ["unique_code" => "rkap_capex_show", "name" => "Show Rkap Capex List Datatable", "http_method" => "get", "url" => "rkap/rkap-capex/{id}", "path" => "Capex\RkapCapexController@show", "middleware" => null],
            ["unique_code" => "rkap_capex_update", "name" => "Update Rkap Capex", "http_method" => "put", "url" => "rkap/rkap-capex/{id}", "path" => "Capex\RkapCapexController@update", "middleware" => null],
            ["unique_code" => "rkap_capex_delete", "name" => "Delete Rkap Capex", "http_method" => "delete", "url" => "rkap/rkap-capex/{uuid}", "path" => "Capex\RkapCapexController@destroy", "middleware" => null],
            ["unique_code" => "rkap_capex_download", "name" => "Download Export Rkap Capex", "http_method" => "get", "url" => "rkap/rkap-capex-download", "path" => "Capex\RkapCapexController@download", "middleware" => null],

            ["unique_code" => "prognose_capex_import", "name" => "Import Prognose Capex Template", "http_method" => "post", "url" => "rkap/prognose-capex-import", "path" => "Capex\PrognoseCapexController@import", "middleware" => null],
            ["unique_code" => "prognose_capex_index", "name" => "Prognose Capex List Datatable", "http_method" => "get", "url" => "rkap/prognose-capex", "path" => "Capex\PrognoseCapexController@index", "middleware" => null],
            ["unique_code" => "prognose_capex_get_project", "name" => "Prognose Capex Get No Project", "http_method" => "get", "url" => "rkap/prognose-capex-project", "path" => "Capex\PrognoseCapexController@getProject", "middleware" => null],
            ["unique_code" => "prognose_capex_show", "name" => "Show Prognose Capex List Datatable", "http_method" => "get", "url" => "rkap/prognose-capex/{id}", "path" => "Capex\PrognoseCapexController@show", "middleware" => null],
            ["unique_code" => "prognose_capex_update", "name" => "Update Prognose Capex", "http_method" => "put", "url" => "rkap/prognose-capex/{id}", "path" => "Capex\PrognoseCapexController@update", "middleware" => null],
            ["unique_code" => "prognose_capex_delete", "name" => "Delete Prognose Capex", "http_method" => "delete", "url" => "rkap/prognose-capex/{uuid}", "path" => "Capex\PrognoseCapexController@destroy", "middleware" => null],
            ["unique_code" => "prognose_capex_download", "name" => "Download Export Prognose Capex", "http_method" => "get", "url" => "rkap/prognose-capex-download", "path" => "Capex\PrognoseCapexController@download", "middleware" => null],


            // Plant Reability
            ["unique_code" => "store_plant_inspection_header", "name" => "store plant inspection header", "http_method" => "post", "url" => "plant_reability", "path" => "Pr\PlantReabilityController@store", "middleware" => null],
            ["unique_code" => "get_plant_inspection_header", "name" => "Get plant inspection header", "http_method" => "get", "url" => "plant_reability/get-data", "path" => "Pr\PlantReabilityController@index", "middleware" => null],
            ["unique_code" => "get_plant_inspection_detail", "name" => "Get plant inspection detail", "http_method" => "get", "url" => "plant_reability/get-detail/{id}", "path" => "Pr\PlantReabilityController@getDetail", "middleware" => null],

            ["unique_code" => "get_template_item_incpection", "name" => "Get item inspection template", "http_method" => "post", "url" => "plant_reability/download-template", "path" => "Pr\PlantReabilityController@getItemInspectionTemplate", "middleware" => null],
            ["unique_code" => "read_template_item_incpection", "name" => "Read item inspection template", "http_method" => "post", "url" => "plant_reability/read-template/", "path" => "Pr\PlantReabilityController@readItemInspectionTemplate", "middleware" => null],


            ["unique_code" => "get_plant_inspection_approval", "name" => "Get plant inspection header approval", "http_method" => "get", "url" => "plant_reability/get-data-approval", "path" => "Pr\PlantReabilityController@index_approval", "middleware" => null],
            ["unique_code" => "approval_plant_inspection", "name" => "Approval plant inspection", "http_method" => "post", "url" => "plant_reability/approval", "path" => "Pr\PlantReabilityController@approval", "middleware" => null],

            ["unique_code" => "get_summary_plant_inspection", "name" => "Get summary plant inspection", "http_method" => "get", "url" => "plant_reability/get-summary", "path" => "Pr\PlantReabilityController@getInspectionSummary", "middleware" => null],
            ["unique_code" => "get_pie_summary_plant_inspection", "name" => "Get pie plant inspection", "http_method" => "get", "url" => "plant_reability/get-pie-summary", "path" => "Pr\PlantReabilityController@getPieInspectionSummary", "middleware" => null],
            ["unique_code" => "get_m_fungsi", "name" => "Get Master Fungsi", "http_method" => "get", "url" => "plant_reability/get-fungsi", "path" => "Pr\PlantReabilityController@getFungsi", "middleware" => null],
            // End Plant Reability

            // QC
            ["unique_code" => "import_qc_raw_meal", "name" => "Import Qc Raw Meal", "http_method" => "post", "url" => "qc/raw-meal-import", "path" => "Qc\QcRmController@import", "middleware" => null],
            ["unique_code" => "list_qc_raw_meal", "name" => "List Qc Raw Meal", "http_method" => "get", "url" => "qc/raw-meal", "path" => "Qc\QcRmController@index", "middleware" => null],
            ["unique_code" => "show_qc_raw_meal", "name" => "Show Qc Raw Meal", "http_method" => "get", "url" => "qc/raw-meal/{uuid}", "path" => "Qc\QcRmController@show", "middleware" => null],
            ["unique_code" => "update_qc_raw_meal", "name" => "Update Qc Raw Meal", "http_method" => "put", "url" => "qc/raw-meal/{uuid}", "path" => "Qc\QcRmController@update", "middleware" => null],
            ["unique_code" => "delete_qc_raw_meal", "name" => "Delete Qc Raw Meal", "http_method" => "delete", "url" => "qc/raw-meal/{uuid}", "path" => "Qc\QcRmController@destroy", "middleware" => null],
            ["unique_code" => "downlaod_qc_raw_meal", "name" => "Download Qc Raw Meal", "http_method" => "get", "url" => "qc/raw-meal-download", "path" => "Qc\QcRmController@download", "middleware" => null],

            ["unique_code" => "import_qc_kiln_feed", "name" => "Import Qc Kiln Feed", "http_method" => "post", "url" => "qc/kiln-feed-import", "path" => "Qc\QcKfController@import", "middleware" => null],
            ["unique_code" => "list_qc_kiln_feed", "name" => "List Qc Kiln Feed", "http_method" => "get", "url" => "qc/kiln-feed", "path" => "Qc\QcKfController@index", "middleware" => null],
            ["unique_code" => "show_qc_kiln_feed", "name" => "Show Qc Kiln Feed", "http_method" => "get", "url" => "qc/kiln-feed/{uuid}", "path" => "Qc\QcKfController@show", "middleware" => null],
            ["unique_code" => "update_qc_kiln_feed", "name" => "Update Qc Kiln Feed", "http_method" => "put", "url" => "qc/kiln-feed/{uuid}", "path" => "Qc\QcKfController@update", "middleware" => null],
            ["unique_code" => "delete_qc_kiln_feed", "name" => "Delete Qc Kiln Feed", "http_method" => "delete", "url" => "qc/kiln-feed/{uuid}", "path" => "Qc\QcKfController@destroy", "middleware" => null],
            ["unique_code" => "dowload_qc_kiln_feed", "name" => "Dowload Qc Kiln Feed", "http_method" => "get", "url" => "qc/kiln-feed-download", "path" => "Qc\QcKfController@download", "middleware" => null],

            ["unique_code" => "import_qc_clk", "name" => "Import Qc Clk", "http_method" => "post", "url" => "qc/clinker-import", "path" => "Qc\QcClkController@import", "middleware" => null],
            ["unique_code" => "list_qc_clk", "name" => "List Qc Clk", "http_method" => "get", "url" => "qc/clinker", "path" => "Qc\QcClkController@index", "middleware" => null],
            ["unique_code" => "show_qc_clk", "name" => "Show Qc Clk", "http_method" => "get", "url" => "qc/clinker/{uuid}", "path" => "Qc\QcClkController@show", "middleware" => null],
            ["unique_code" => "update_qc_clk", "name" => "Update Qc Clk", "http_method" => "put", "url" => "qc/clinker/{uuid}", "path" => "Qc\QcClkController@update", "middleware" => null],
            ["unique_code" => "delete_qc_clk", "name" => "Delete Qc Clk", "http_method" => "delete", "url" => "qc/clinker/{uuid}", "path" => "Qc\QcClkController@destroy", "middleware" => null],
            ["unique_code" => "download_qc_clk", "name" => "Download Export Qc Clk", "http_method" => "get", "url" => "qc/clinker-download", "path" => "Qc\QcClkController@download", "middleware" => null],

            ["unique_code" => "import_qc_semen", "name" => "Import Qc Semen", "http_method" => "post", "url" => "qc/quality-cement-import", "path" => "Qc\QcSemenController@import", "middleware" => null],
            ["unique_code" => "list_qc_semen", "name" => "List Qc Semen", "http_method" => "get", "url" => "qc/quality-cement", "path" => "Qc\QcSemenController@index", "middleware" => null],
            ["unique_code" => "show_qc_semen", "name" => "Show Qc Semen", "http_method" => "get", "url" => "qc/quality-cement/{uuid}", "path" => "Qc\QcSemenController@show", "middleware" => null],
            ["unique_code" => "update_qc_semen", "name" => "Update Qc Semen", "http_method" => "put", "url" => "qc/quality-cement/{uuid}", "path" => "Qc\QcSemenController@update", "middleware" => null],
            ["unique_code" => "delete_qc_semen", "name" => "Delete Qc Semen", "http_method" => "delete", "url" => "qc/quality-cement/{uuid}", "path" => "Qc\QcSemenController@destroy", "middleware" => null],
            ["unique_code" => "download_qc_semen", "name" => "Download Qc Semen", "http_method" => "get", "url" => "qc/quality-cement-download", "path" => "Qc\QcSemenController@download", "middleware" => null],

            ["unique_code" => "import_qc_coal", "name" => "Import Qc Coal", "http_method" => "post", "url" => "qc/coal-import", "path" => "Qc\QcCoalController@import", "middleware" => null],
            ["unique_code" => "list_qc_coal", "name" => "List Qc Coal", "http_method" => "get", "url" => "qc/coal", "path" => "Qc\QcCoalController@index", "middleware" => null],
            ["unique_code" => "show_qc_coal", "name" => "Show Qc Coal", "http_method" => "get", "url" => "qc/coal/{uuid}", "path" => "Qc\QcCoalController@show", "middleware" => null],
            ["unique_code" => "update_qc_coal", "name" => "update Qc Coal", "http_method" => "put", "url" => "qc/coal/{uuid}", "path" => "Qc\QcCoalController@update", "middleware" => null],
            ["unique_code" => "delete_qc_coal", "name" => "Delete Qc Coal", "http_method" => "delete", "url" => "qc/coal/{uuid}", "path" => "Qc\QcCoalController@destroy", "middleware" => null],
            ["unique_code" => "download_qc_coal", "name" => "Download Qc Coal", "http_method" => "get", "url" => "qc/coal-download", "path" => "Qc\QcCoalController@download", "middleware" => null],

            ["unique_code" => "import_qc_hot_meal", "name" => "Import Qc Hot Meal", "http_method" => "post", "url" => "qc/hot-meal-import", "path" => "Qc\QcHotMealController@import", "middleware" => null],
            ["unique_code" => "list_qc_hot_meal", "name" => "List Qc Hot Meal", "http_method" => "get", "url" => "qc/hot-meal", "path" => "Qc\QcHotMealController@index", "middleware" => null],
            ["unique_code" => "show_qc_hot_meal", "name" => "Show Qc Hot Meal", "http_method" => "get", "url" => "qc/hot-meal/{uuid}", "path" => "Qc\QcHotMealController@show", "middleware" => null],
            ["unique_code" => "update_qc_hot_meal", "name" => "Update Qc Hot Meal", "http_method" => "put", "url" => "qc/hot-meal/{uuid}", "path" => "Qc\QcHotMealController@update", "middleware" => null],
            ["unique_code" => "delete_qc_hot_meal", "name" => "Delete Qc Hot Meal", "http_method" => "delete", "url" => "qc/hot-meal/{uuid}", "path" => "Qc\QcHotMealController@destroy", "middleware" => null],
            ["unique_code" => "download_qc_hot_meal", "name" => "Download Qc Hot Meal", "http_method" => "get", "url" => "qc/hot-meal-download", "path" => "Qc\QcHotMealController@download", "middleware" => null],
            // End

            // SHE Safety Report
            //Dashboard
            ["unique_code" => "get_kpi", "name" => "get kpi", "http_method" => "get", "url" => "dashboard_kpi", "path" => "SHESafetyPerm\SHEDashboardController@dashboard_kpi", "middleware" => null],
            ["unique_code" => "get_ltifr_ltisr", "name" => "get ltifr_ltisr", "http_method" => "get", "url" => "dashboard_ltifr_ltisr", "path" => "SHESafetyPerm\SHEDashboardController@dashboard_ltifr_ltisr", "middleware" => null],
            ["unique_code" => "get_man_power", "name" => "get man_power", "http_method" => "get", "url" => "dashboard_man_power", "path" => "SHESafetyPerm\SHEDashboardController@dashboard_man_power", "middleware" => null],
            ["unique_code" => "get_man_hours", "name" => "get man_hours", "http_method" => "get", "url" => "dashboard_man_hours", "path" => "SHESafetyPerm\SHEDashboardController@dashboard_man_hours", "middleware" => null],
            ["unique_code" => "get_unsafe_action", "name" => "get unsafe_action", "http_method" => "get", "url" => "dashboard_unsafe_action", "path" => "SHESafetyPerm\SHEDashboardController@dashboard_unsafe_action", "middleware" => null],
            ["unique_code" => "get_unsafe_condition", "name" => "get unsafe_condition", "http_method" => "get", "url" => "dashboard_unsafe_condition", "path" => "SHESafetyPerm\SHEDashboardController@dashboard_unsafe_condition", "middleware" => null],
            ["unique_code" => "get_accident", "name" => "get accident", "http_method" => "get", "url" => "dashboard_accident", "path" => "SHESafetyPerm\SHEDashboardController@dashboard_accident", "middleware" => null],
            ["unique_code" => "get_accident_fire", "name" => "get accident_fire", "http_method" => "get", "url" => "dashboard_accident_fire", "path" => "SHESafetyPerm\SHEDashboardController@dashboard_accident_fire", "middleware" => null],
            ["unique_code" => "get_apar", "name" => "get apar", "http_method" => "get", "url" => "dashboard_apar", "path" => "SHESafetyPerm\SHEDashboardController@dashboard_apar", "middleware" => null],
            ["unique_code" => "get_hydrant", "name" => "get hydrant", "http_method" => "get", "url" => "dashboard_hydrant", "path" => "SHESafetyPerm\SHEDashboardController@dashboard_hydrant", "middleware" => null],
            ["unique_code" => "get_fire_alarm", "name" => "get fire_alarm", "http_method" => "get", "url" => "dashboard_fire_alarm", "path" => "SHESafetyPerm\SHEDashboardController@dashboard_fire_alarm", "middleware" => null],
            ["unique_code" => "get_safety_training", "name" => "get safety_training", "http_method" => "get", "url" => "dashboard_safety_training", "path" => "SHESafetyPerm\SHEDashboardController@dashboard_safety_training", "middleware" => null],
            //End Dashboard


            // KPI Safety
            ["unique_code" => "get_all_kpisafety", "name" => "get all kpisafety", "http_method" => "get", "url" => "kpisafety/{tahun}", "path" => "SHESafetyPerm\SHEKpiSafetyController@index", "middleware" => null],
            ["unique_code" => "create_kpisafety", "name" => "create kpisafety", "http_method" => "post", "url" => "kpisafety", "path" => "SHESafetyPerm\SHEKpiSafetyController@store", "middleware" => null],            
            // End KPI Safety

            // Man Power
            ["unique_code" => "get_all_manpower", "name" => "get all manpower", "http_method" => "get", "url" => "manpower", "path" => "SHESafetyPerm\SHEManPowerController@index", "middleware" => null],
            ["unique_code" => "get_detail_manpower", "name" => "get detail manpower", "http_method" => "get", "url" => "manpower/{id}", "path" => "SHESafetyPerm\SHEManPowerController@show", "middleware" => null],
            ["unique_code" => "create_manpower", "name" => "create manpower", "http_method" => "post", "url" => "manpower", "path" => "SHESafetyPerm\SHEManPowerController@store", "middleware" => null],
            ["unique_code" => "update_manpower", "name" => "update manpower", "http_method" => "put", "url" => "manpower/{id}", "path" => "SHESafetyPerm\SHEManPowerController@update", "middleware" => null],
            ["unique_code" => "delete_manpower", "name" => "delete manpower", "http_method" => "delete", "url" => "manpower/{id}", "path" => "SHESafetyPerm\SHEManPowerController@destroy", "middleware" => null],
            ["unique_code" => "template_manpower", "name" => "template manpower", "http_method" => "get", "url" => "manpower_template/", "path" => "SHESafetyPerm\SHEManPowerController@template", "middleware" => null],
            ["unique_code" => "import_manpower", "name" => "import manpower", "http_method" => "post", "url" => "manpower_import/", "path" => "SHESafetyPerm\SHEManPowerController@import", "middleware" => null],
            // End Man Power

            // Man Hours
            ["unique_code" => "get_all_manhours", "name" => "get all manhours", "http_method" => "get", "url" => "manhours", "path" => "SHESafetyPerm\SHEManHoursController@index", "middleware" => null],
            ["unique_code" => "get_detail_manhours", "name" => "get detail manhours", "http_method" => "get", "url" => "manhours/{id}", "path" => "SHESafetyPerm\SHEManHoursController@show", "middleware" => null],
            ["unique_code" => "create_manhours", "name" => "create manhours", "http_method" => "post", "url" => "manhours", "path" => "SHESafetyPerm\SHEManHoursController@store", "middleware" => null],
            ["unique_code" => "update_manhours", "name" => "update manhours", "http_method" => "put", "url" => "manhours/{id}", "path" => "SHESafetyPerm\SHEManHoursController@update", "middleware" => null],
            ["unique_code" => "delete_manhours", "name" => "delete manhours", "http_method" => "delete", "url" => "manhours/{id}", "path" => "SHESafetyPerm\SHEManHoursController@destroy", "middleware" => null],
            // End Man Hours

            // Unsafe Action
            ["unique_code" => "get_all_unsafeaction", "name" => "get all unsafeaction", "http_method" => "get", "url" => "unsafeaction", "path" => "SHESafetyPerm\SHEUnsafeActionController@index", "middleware" => null],
            ["unique_code" => "get_detail_unsafeaction", "name" => "get detail unsafeaction", "http_method" => "get", "url" => "unsafeaction/{id}", "path" => "SHESafetyPerm\SHEUnsafeActionController@show", "middleware" => null],
            ["unique_code" => "create_unsafeaction", "name" => "create unsafeaction", "http_method" => "post", "url" => "unsafeaction", "path" => "SHESafetyPerm\SHEUnsafeActionController@store", "middleware" => null],
            ["unique_code" => "update_unsafeaction", "name" => "update unsafeaction", "http_method" => "put", "url" => "unsafeaction/{id}", "path" => "SHESafetyPerm\SHEUnsafeActionController@update", "middleware" => null],
            ["unique_code" => "delete_unsafeaction", "name" => "delete unsafeaction", "http_method" => "delete", "url" => "unsafeaction/{id}", "path" => "SHESafetyPerm\SHEUnsafeActionController@destroy", "middleware" => null],
            ["unique_code" => "template_unsafeaction", "name" => "template unsafeaction", "http_method" => "get", "url" => "unsafeaction_template/", "path" => "SHESafetyPerm\SHEUnsafeActionController@template", "middleware" => null],
            ["unique_code" => "import_unsafeaction", "name" => "import unsafeaction", "http_method" => "post", "url" => "unsafeaction_import/", "path" => "SHESafetyPerm\SHEUnsafeActionController@import", "middleware" => null],
            ["unique_code" => "upload_unsafeaction", "name" => "upload unsafeaction", "http_method" => "post", "url" => "file_unsafeaction", "path" => "SHESafetyPerm\SHEUnsafeActionController@upload_file", "middleware" => null],
            // End Unsafe Action

            // Unsafe Condition
            ["unique_code" => "get_all_unsafecondition", "name" => "get all unsafecondition", "http_method" => "get", "url" => "unsafecondition", "path" => "SHESafetyPerm\SHEUnsafeConditionController@index", "middleware" => null],
            ["unique_code" => "get_detail_unsafecondition", "name" => "get detail unsafecondition", "http_method" => "get", "url" => "unsafecondition/{id}", "path" => "SHESafetyPerm\SHEUnsafeConditionController@show", "middleware" => null],
            ["unique_code" => "create_unsafecondition", "name" => "create unsafecondition", "http_method" => "post", "url" => "unsafecondition", "path" => "SHESafetyPerm\SHEUnsafeConditionController@store", "middleware" => null],
            ["unique_code" => "update_unsafecondition", "name" => "update unsafecondition", "http_method" => "put", "url" => "unsafecondition/{id}", "path" => "SHESafetyPerm\SHEUnsafeConditionController@update", "middleware" => null],
            ["unique_code" => "delete_unsafecondition", "name" => "delete unsafecondition", "http_method" => "delete", "url" => "unsafecondition/{id}", "path" => "SHESafetyPerm\SHEUnsafeConditionController@destroy", "middleware" => null],
            ["unique_code" => "template_unsafecondition", "name" => "template unsafecondition", "http_method" => "get", "url" => "unsafecondition_template/", "path" => "SHESafetyPerm\SHEUnsafeConditionController@template", "middleware" => null],
            ["unique_code" => "import_unsafecondition", "name" => "import unsafecondition", "http_method" => "post", "url" => "unsafecondition_import/", "path" => "SHESafetyPerm\SHEUnsafeConditionController@import", "middleware" => null],
            ["unique_code" => "upload_unsafecondition", "name" => "upload unsafecondition", "http_method" => "post", "url" => "file_unsafecondition", "path" => "SHESafetyPerm\SHEUnsafeConditionController@upload_file", "middleware" => null],
            // End Unsafe Condition

            // Accident Report
            ["unique_code" => "get_all_accidentreport", "name" => "get all accidentreport", "http_method" => "get", "url" => "accidentreport", "path" => "SHESafetyPerm\SHEAccidentReportController@index", "middleware" => null],
            ["unique_code" => "get_detail_accidentreport", "name" => "get detail accidentreport", "http_method" => "get", "url" => "accidentreport/{id}", "path" => "SHESafetyPerm\SHEAccidentReportController@show", "middleware" => null],
            ["unique_code" => "create_accidentreport", "name" => "create accidentreport", "http_method" => "post", "url" => "accidentreport", "path" => "SHESafetyPerm\SHEAccidentReportController@store", "middleware" => null],
            ["unique_code" => "update_accidentreport", "name" => "update accidentreport", "http_method" => "put", "url" => "accidentreport/{id}", "path" => "SHESafetyPerm\SHEAccidentReportController@update", "middleware" => null],
            ["unique_code" => "delete_accidentreport", "name" => "delete accidentreport", "http_method" => "delete", "url" => "accidentreport/{id}", "path" => "SHESafetyPerm\SHEAccidentReportController@destroy", "middleware" => null],
            ["unique_code" => "upload_accidentreport", "name" => "upload accidentreport", "http_method" => "post", "url" => "file_accidentreport", "path" => "SHESafetyPerm\SHEAccidentReportController@upload_file", "middleware" => null],
            // End Accident Report

            // Fire Accident Report
            ["unique_code" => "get_all_fireaccidentreport", "name" => "get all fireaccidentreport", "http_method" => "get", "url" => "fireaccidentreport", "path" => "SHESafetyPerm\SHEFireAccidentReportController@index", "middleware" => null],
            ["unique_code" => "get_detail_fireaccidentreport", "name" => "get detail fireaccidentreport", "http_method" => "get", "url" => "fireaccidentreport/{id}", "path" => "SHESafetyPerm\SHEFireAccidentReportController@show", "middleware" => null],
            ["unique_code" => "create_fireaccidentreport", "name" => "create fireaccidentreport", "http_method" => "post", "url" => "fireaccidentreport", "path" => "SHESafetyPerm\SHEFireAccidentReportController@store", "middleware" => null],
            ["unique_code" => "update_fireaccidentreport", "name" => "update fireaccidentreport", "http_method" => "put", "url" => "fireaccidentreport/{id}", "path" => "SHESafetyPerm\SHEFireAccidentReportController@update", "middleware" => null],
            ["unique_code" => "delete_fireaccidentreport", "name" => "delete fireaccidentreport", "http_method" => "delete", "url" => "fireaccidentreport/{id}", "path" => "SHESafetyPerm\SHEFireAccidentReportController@destroy", "middleware" => null],
            ["unique_code" => "upload_fireaccidentreport", "name" => "upload fireaccidentreport", "http_method" => "post", "url" => "file_fireaccidentreport", "path" => "SHESafetyPerm\SHEFireAccidentReportController@upload_file", "middleware" => null],
            // End Fire Accident Report

            // Safety Training
            ["unique_code" => "get_all_safetytraining", "name" => "get all safetytraining", "http_method" => "get", "url" => "safetytraining", "path" => "SHESafetyPerm\SHESafetyTrainingController@index", "middleware" => null],
            ["unique_code" => "get_detail_safetytraining", "name" => "get detail safetytraining", "http_method" => "get", "url" => "safetytraining/{id}", "path" => "SHESafetyPerm\SHESafetyTrainingController@show", "middleware" => null],
            ["unique_code" => "create_safetytraining", "name" => "create safetytraining", "http_method" => "post", "url" => "safetytraining", "path" => "SHESafetyPerm\SHESafetyTrainingController@store", "middleware" => null],
            ["unique_code" => "update_safetytraining", "name" => "update safetytraining", "http_method" => "put", "url" => "safetytraining/{id}", "path" => "SHESafetyPerm\SHESafetyTrainingController@update", "middleware" => null],
            ["unique_code" => "delete_safetytraining", "name" => "delete safetytraining", "http_method" => "delete", "url" => "safetytraining/{id}", "path" => "SHESafetyPerm\SHESafetyTrainingController@destroy", "middleware" => null],
            // End Safety Training

            // Safety Training Org
            ["unique_code" => "get_all_safetytrainingorg", "name" => "get all safetytrainingorg", "http_method" => "get", "url" => "safetytrainingorg", "path" => "SHESafetyPerm\SHESafetyTrainingOrgController@index", "middleware" => null],
            ["unique_code" => "get_detail_safetytrainingorg", "name" => "get detail safetytrainingorg", "http_method" => "get", "url" => "safetytrainingorg/{id}", "path" => "SHESafetyPerm\SHESafetyTrainingOrgController@show", "middleware" => null],
            ["unique_code" => "create_safetytrainingorg", "name" => "create safetytrainingorg", "http_method" => "post", "url" => "safetytrainingorg", "path" => "SHESafetyPerm\SHESafetyTrainingOrgController@store", "middleware" => null],
            ["unique_code" => "update_safetytrainingorg", "name" => "update safetytrainingorg", "http_method" => "put", "url" => "safetytrainingorg/{id}", "path" => "SHESafetyPerm\SHESafetyTrainingOrgController@update", "middleware" => null],
            ["unique_code" => "delete_safetytrainingorg", "name" => "delete safetytrainingorg", "http_method" => "delete", "url" => "safetytrainingorg/{id}", "path" => "SHESafetyPerm\SHESafetyTrainingOrgController@destroy", "middleware" => null],
            ["unique_code" => "template_safetytrainingorg", "name" => "template safetytrainingorg", "http_method" => "get", "url" => "safetytrainingorg_template/", "path" => "SHESafetyPerm\SHESafetyTrainingOrgController@template", "middleware" => null],
            ["unique_code" => "import_safetytrainingorg", "name" => "import safetytrainingorg", "http_method" => "post", "url" => "safetytrainingorg_import/", "path" => "SHESafetyPerm\SHESafetyTrainingOrgController@import", "middleware" => null],
            // End Safety Training Org

            // Apar
            ["unique_code" => "get_all_apar", "name" => "get all apar", "http_method" => "get", "url" => "apar", "path" => "SHESafetyPerm\SHEAparController@index", "middleware" => null],
            ["unique_code" => "get_detail_apar", "name" => "get detail apar", "http_method" => "get", "url" => "apar/{id}", "path" => "SHESafetyPerm\SHEAparController@show", "middleware" => null],
            ["unique_code" => "create_apar", "name" => "create apar", "http_method" => "post", "url" => "apar", "path" => "SHESafetyPerm\SHEAparController@store", "middleware" => null],
            ["unique_code" => "update_apar", "name" => "update apar", "http_method" => "put", "url" => "apar/{id}", "path" => "SHESafetyPerm\SHEAparController@update", "middleware" => null],
            ["unique_code" => "delete_apar", "name" => "delete apar", "http_method" => "delete", "url" => "apar/{id}", "path" => "SHESafetyPerm\SHEAparController@destroy", "middleware" => null],
            ["unique_code" => "upload_apar", "name" => "upload apar", "http_method" => "post", "url" => "file_apar", "path" => "SHESafetyPerm\SHEAparController@upload_file", "middleware" => null],
            ["unique_code" => "template_apar", "name" => "template apar", "http_method" => "get", "url" => "apar_template/", "path" => "SHESafetyPerm\SHEAparController@template", "middleware" => null],
            ["unique_code" => "import_apar", "name" => "import apar", "http_method" => "post", "url" => "apar_import/", "path" => "SHESafetyPerm\SHEAparController@import", "middleware" => null],
            // End Apar

            // Hydrant
            ["unique_code" => "get_all_hydrant", "name" => "get all hydrant", "http_method" => "get", "url" => "hydrant", "path" => "SHESafetyPerm\SHEHydrantController@index", "middleware" => null],
            ["unique_code" => "get_detail_hydrant", "name" => "get detail hydrant", "http_method" => "get", "url" => "hydrant/{id}", "path" => "SHESafetyPerm\SHEHydrantController@show", "middleware" => null],
            ["unique_code" => "create_hydrant", "name" => "create hydrant", "http_method" => "post", "url" => "hydrant", "path" => "SHESafetyPerm\SHEHydrantController@store", "middleware" => null],
            ["unique_code" => "update_hydrant", "name" => "update hydrant", "http_method" => "put", "url" => "hydrant/{id}", "path" => "SHESafetyPerm\SHEHydrantController@update", "middleware" => null],
            ["unique_code" => "delete_hydrant", "name" => "delete hydrant", "http_method" => "delete", "url" => "hydrant/{id}", "path" => "SHESafetyPerm\SHEHydrantController@destroy", "middleware" => null],
            ["unique_code" => "create_hydrant_file", "name" => "create hydrant_file", "http_method" => "post", "url" => "hydrant_file", "path" => "SHESafetyPerm\SHEHydrantController@store_file", "middleware" => null],
            ["unique_code" => "get_detail_hydrant_file", "name" => "get detail hydrant_file", "http_method" => "get", "url" => "hydrant_file", "path" => "SHESafetyPerm\SHEHydrantController@show_file", "middleware" => null],
            ["unique_code" => "delete_hydrant_file", "name" => "delete hydrant_file", "http_method" => "delete", "url" => "hydrant_file/{id}", "path" => "SHESafetyPerm\SHEHydrantController@destroy_file", "middleware" => null],
            ["unique_code" => "upload_hydrant", "name" => "upload hydrant", "http_method" => "post", "url" => "file_hydrant", "path" => "SHESafetyPerm\SHEHydrantController@upload_file", "middleware" => null],
            ["unique_code" => "template_hydrant", "name" => "template hydrant", "http_method" => "get", "url" => "hydrant_template/", "path" => "SHESafetyPerm\SHEHydrantController@template", "middleware" => null],
            ["unique_code" => "import_hydrant", "name" => "import hydrant", "http_method" => "post", "url" => "hydrant_import/", "path" => "SHESafetyPerm\SHEHydrantController@import", "middleware" => null],
            // End Hydrant

            // Fire Alarm
            ["unique_code" => "get_all_firealarm", "name" => "get all firealarm", "http_method" => "get", "url" => "firealarm", "path" => "SHESafetyPerm\SHEFireAlarmController@index", "middleware" => null],
            ["unique_code" => "get_all_mfirealarm", "name" => "get all master firealarm", "http_method" => "get", "url" => "mfirealarm", "path" => "SHESafetyPerm\SHEFireAlarmController@show_master", "middleware" => null],
            ["unique_code" => "get_detail_firealarm", "name" => "get detail firealarm", "http_method" => "get", "url" => "firealarm/{id}", "path" => "SHESafetyPerm\SHEFireAlarmController@show", "middleware" => null],
            ["unique_code" => "create_firealarm", "name" => "create firealarm", "http_method" => "post", "url" => "firealarm", "path" => "SHESafetyPerm\SHEFireAlarmController@store", "middleware" => null],
            ["unique_code" => "update_firealarm", "name" => "update firealarm", "http_method" => "put", "url" => "firealarm/{id}", "path" => "SHESafetyPerm\SHEFireAlarmController@update", "middleware" => null],
            ["unique_code" => "delete_firealarm", "name" => "delete firealarm", "http_method" => "delete", "url" => "firealarm/{id}", "path" => "SHESafetyPerm\SHEFireAlarmController@destroy", "middleware" => null],
            ["unique_code" => "template_firealarm", "name" => "template firealarm", "http_method" => "get", "url" => "firealarm_template/", "path" => "SHESafetyPerm\SHEFireAlarmController@template", "middleware" => null],
            ["unique_code" => "import_firealarm", "name" => "import firealarm", "http_method" => "post", "url" => "firealarm_import/", "path" => "SHESafetyPerm\SHEFireAlarmController@import", "middleware" => null],
            // End Fire Alarm

            // Fire Alarm File
            ["unique_code" => "get_all_firealarmfile", "name" => "get all firealarmfile", "http_method" => "get", "url" => "firealarmfile", "path" => "SHESafetyPerm\SHEFireAlarmFileController@index", "middleware" => null],
            ["unique_code" => "get_detail_firealarmfile", "name" => "get detail firealarmfile", "http_method" => "get", "url" => "firealarmfile/{id}", "path" => "SHESafetyPerm\SHEFireAlarmFileController@show", "middleware" => null],
            ["unique_code" => "create_firealarmfile", "name" => "create firealarmfile", "http_method" => "post", "url" => "firealarmfile", "path" => "SHESafetyPerm\SHEFireAlarmFileController@store", "middleware" => null],
            ["unique_code" => "update_firealarmfile", "name" => "update firealarmfile", "http_method" => "put", "url" => "firealarmfile/{id}", "path" => "SHESafetyPerm\SHEFireAlarmFileController@update", "middleware" => null],
            ["unique_code" => "delete_firealarmfile", "name" => "delete firealarmfile", "http_method" => "delete", "url" => "firealarmfile/{id}", "path" => "SHESafetyPerm\SHEFireAlarmFileController@destroy", "middleware" => null],
            ["unique_code" => "upload_firealarmfile", "name" => "upload firealarmfile", "http_method" => "post", "url" => "file_firealarmfile", "path" => "SHESafetyPerm\SHEFireAlarmFileController@upload_file", "middleware" => null],
            // End Fire Alarm File
            // End SHE Safety Report

            //Production Reports
            ["unique_code" => "get_crusher_reports", "name" => "get crusher reports", "http_method" => "get", "url" => "production/crusher", "path" => "Production\CrusherReportsController@index", "middleware" => null],
            ["unique_code" => "get_rawmill_reports", "name" => "get rawmill reports", "http_method" => "get", "url" => "production/rawmill", "path" => "Production\RawmillReportsController@index", "middleware" => null],
            ["unique_code" => "get_clinker_reports", "name" => "get clinker reports", "http_method" => "get", "url" => "production/clinker", "path" => "Production\ClinkerReportsController@index", "middleware" => null],
            ["unique_code" => "get_coalmill_reports", "name" => "get coalmill reports", "http_method" => "get", "url" => "production/coalmill", "path" => "Production\CoalMillReportsController@index", "middleware" => null],
            ["unique_code" => "get_cement_reports", "name" => "get cement reports", "http_method" => "get", "url" => "production/cement", "path" => "Production\CementReportsController@index", "middleware" => null],
            ["unique_code" => "get_packer_reports", "name" => "get packer reports", "http_method" => "get", "url" => "production/packer", "path" => "Production\PackerReportsController@index", "middleware" => null],
            ["unique_code" => "get_packer_status", "name" => "get packer status", "http_method" => "get", "url" => "production/packer/status", "path" => "Production\PackerReportsController@status", "middleware" => null],
            ["unique_code" => "get_silo_reports", "name" => "get silo stock reports", "http_method" => "get", "url" => "production/packer/silo", "path" => "Production\PackerReportsController@silo", "middleware" => null],
            ["unique_code" => "get_production_index", "name" => "get production index reports", "http_method" => "get", "url" => "production/prodindex", "path" => "Production\ProductionIndexReportsController@index", "middleware" => null],
            ["unique_code" => "get_whrpg_reports", "name" => "get whrpg reports", "http_method" => "get", "url" => "production/whrpg", "path" => "Production\WhrpgReportsController@index", "middleware" => null],

            // Vendor Management
            ["unique_code" => "get_all_vm_planning_list", "name" => "get all vm planning list", "http_method" => "get", "url" => "upload-schedule/list", "path" => "VendorManagement\PlanningSchedulleController@index", "middleware" => null],
            ["unique_code" => "import_planning", "name" => "import planning", "http_method" => "post", "url" => "upload-schedule", "path" => 'VendorManagement\ImportDataController@importPlanning', "middleware" => null],
            ["unique_code" => "get_all_vm_realisasi_list", "name" => "get all vm realisasi list", "http_method" => "get", "url" => "realisasi-fingerprint", "path" => "VendorManagement\RealisasiScheduleController@index", "middleware" => null],
            ["unique_code" => "delete_vm_realisasi", "name" => "delete vm realisasi", "http_method" => "delete", "url" => "realisasi-fingerprint/{uuid}", "path" => "VendorManagement\RealisasiScheduleController@delete", "middleware" => null],
            ["unique_code" => "import_realisasi", "name" => "import realisasi", "http_method" => "post", "url" => "upload-realisasi-fingerprint", "path" => 'VendorManagement\ImportDataController@importrealisasi', "middleware" => null],
            ["unique_code" => "get_all_vm_planning_actual_list", "name" => "get all vm planning actual list", "http_method" => "get", "url" => "planning-realisasi-fingerprint", "path" => "VendorManagement\PlanningSchedulleController@planningActual", "middleware" => null],
            ["unique_code" => "download_vm_planning_list", "name" => "download vm planning list", "http_method" => "post", "url" => "upload-schedule/download-excel", "path" => "VendorManagement\PlanningSchedulleController@downloadPlanning", "middleware" => null],
            ["unique_code" => "download_vm_realisasi_list", "name" => "download vm realisasi list", "http_method" => "post", "url" => "realisasi-fingerprint/download-excel", "path" => "VendorManagement\RealisasiScheduleController@downloadRealisasi", "middleware" => null],
            ["unique_code" => "download_vm_planning_realisasi_list", "name" => "download vm planning realisasi list", "http_method" => "post", "url" => "planning-realisasi-fingerprint/download-excel", "path" => "VendorManagement\PlanningSchedulleController@downloadPlanningActual", "middleware" => null],
            ["unique_code" => "detail_vm_realisasi_list", "name" => "detail vm realisasi", "http_method" => "get", "url" => "realisasi-fingerprint/{uuid}", "path" => "VendorManagement\RealisasiScheduleController@show", "middleware" => null],

            // belum ada
            ["unique_code" => "update_vm_realisasi", "name" => "update vm realisasi", "http_method" => "put", "url" => "realisasi-fingerprint", "path" => "VendorManagement\RealisasiScheduleController@update", "middleware" => null],

            // Master Jenis Limbah B3
            ["unique_code" => "get_master_jenis_limbah_list", "name" => "get master jenis limbah list", "http_method" => "get", "url" => "master-jenis-limbah/list", "path" => "LimbahB3\MasterJenisLimbahB3Controller@index", "middleware" => null],
            ["unique_code" => "store_master_jenis_limbah", "name" => "store master jenis limbah", "http_method" => "post", "url" => "master-jenis-limbah/store", "path" => "LimbahB3\MasterJenisLimbahB3Controller@store", "middleware" => null],
            ["unique_code" => "detail_master_jenis_limbah", "name" => "detail master jenis limbah", "http_method" => "get", "url" => "master-jenis-limbah/edit/{uuid}", "path" => "LimbahB3\MasterJenisLimbahB3Controller@show", "middleware" => null],
            ["unique_code" => "update_master_jenis_limbah", "name" => "update master jenis limbah", "http_method" => "put", "url" => "master-jenis-limbah/update/{uuid}", "path" => "LimbahB3\MasterJenisLimbahB3Controller@update", "middleware" => null],
            ["unique_code" => "delete_master_jenis_limbah", "name" => "delete master jenis limbah", "http_method" => "delete", "url" => "master-jenis-limbah/delete/{uuid}", "path" => "LimbahB3\MasterJenisLimbahB3Controller@delete", "middleware" => null],

            // Master Dokumen Lingkungan
            ["unique_code" => "get_dokumen_lingkungan_list", "name" => "get dokumen lingkungan list", "http_method" => "get", "url" => "dokumen-lingkungan/list", "path" => "DokumenLingkungan\MasterDokumenLingkunganController@index", "middleware" => null],
            ["unique_code" => "store_dokumen_lingkungan", "name" => "store dokumen lingkungan", "http_method" => "post", "url" => "dokumen-lingkungan/store", "path" => "DokumenLingkungan\MasterDokumenLingkunganController@store", "middleware" => null],
            ["unique_code" => "detail_dokumen_lingkungan", "name" => "detail dokumen lingkungan", "http_method" => "get", "url" => "dokumen-lingkungan/edit/{uuid}", "path" => "DokumenLingkungan\MasterDokumenLingkunganController@show", "middleware" => null],
            ["unique_code" => "update_dokumen_lingkungan", "name" => "update dokumen lingkungan", "http_method" => "put", "url" => "dokumen-lingkungan/update/{uuid}", "path" => "DokumenLingkungan\MasterDokumenLingkunganController@update", "middleware" => null],
            ["unique_code" => "delete_dokumen_lingkungan", "name" => "delete dokumen lingkungan", "http_method" => "delete", "url" => "dokumen-lingkungan/delete/{uuid}", "path" => "DokumenLingkungan\MasterDokumenLingkunganController@delete", "middleware" => null],
            ["unique_code" => "get_dokumen_lingkungan_area_list", "name" => "get dokumen lingkungan area list", "http_method" => "get", "url" => "dokumen-lingkungan/ddl/area", "path" => "DokumenLingkungan\MasterDDLController@listArea", "middleware" => null],
            ["unique_code" => "get_dokumen_lingkungan_jenisdokumen_list", "name" => "get dokumen lingkungan jenis dokumen list", "http_method" => "get", "url" => "dokumen-lingkungan/ddl/jenisdokumen", "path" => "DokumenLingkungan\MasterDDLController@listJenisDokumen", "middleware" => null],
            ["unique_code" => "upload_file_dokumen_lingkungan", "name" => "upload file dokumen lingkungan", "http_method" => "post", "url" => "dokumen-lingkungan/upload", "path" => "DokumenLingkungan\MasterDokumenLingkunganController@uploadfile", "middleware" => null],

            // Master Inspeksi Area
            ["unique_code" => "get_inspeksi_area_list", "name" => "get inspeksi area list", "http_method" => "get", "url" => "inspeksi-area/list", "path" => "InspeksiArea\MasterInspeksiAreaController@index", "middleware" => null],
            ["unique_code" => "store_inspeksi_area", "name" => "store inspeksi area", "http_method" => "post", "url" => "inspeksi-area/store", "path" => "InspeksiArea\MasterInspeksiAreaController@store", "middleware" => null],
            ["unique_code" => "detail_inspeksi_area", "name" => "detail inspeksi area", "http_method" => "get", "url" => "inspeksi-area/edit/{uuid}", "path" => "InspeksiArea\MasterInspeksiAreaController@show", "middleware" => null],
            ["unique_code" => "update_inspeksi_area", "name" => "update inspeksi area", "http_method" => "put", "url" => "inspeksi-area/update/{uuid}", "path" => "InspeksiArea\MasterInspeksiAreaController@update", "middleware" => null],
            ["unique_code" => "delete_inspeksi_area", "name" => "delete inspeksi area", "http_method" => "delete", "url" => "inspeksi-area/delete/{uuid}", "path" => "InspeksiArea\MasterInspeksiAreaController@delete", "middleware" => null],

            //SHE-Proper
            // Kebijakan Linkungan
            ["unique_code" => "get_all_kebijakan_ling", "name" => "get all kebijakan_ling", "http_method" => "get", "url" => "kebijakan_ling", "path" => "SHEProper\SHEKebijakanLingController@index", "middleware" => null],
            ["unique_code" => "get_detail_kebijakan_ling", "name" => "get detail kebijakan_ling", "http_method" => "get", "url" => "kebijakan_ling/{id}", "path" => "SHEProper\SHEKebijakanLingController@show", "middleware" => null],
            ["unique_code" => "create_kebijakan_ling", "name" => "create kebijakan_ling", "http_method" => "post", "url" => "kebijakan_ling", "path" => "SHEProper\SHEKebijakanLingController@store", "middleware" => null],
            ["unique_code" => "update_kebijakan_ling", "name" => "update kebijakan_ling", "http_method" => "put", "url" => "kebijakan_ling/{id}", "path" => "SHEProper\SHEKebijakanLingController@update", "middleware" => null],
            ["unique_code" => "delete_kebijakan_ling", "name" => "delete kebijakan_ling", "http_method" => "delete", "url" => "kebijakan_ling/{id}", "path" => "SHEProper\SHEKebijakanLingController@destroy", "middleware" => null],
            ["unique_code" => "upload_kebijakan_ling", "name" => "upload kebijakan_ling", "http_method" => "post", "url" => "file_kebijakan_ling", "path" => "SHEProper\SHEKebijakanLingController@upload_file", "middleware" => null],
            // End Kebijakan Lingkungan

            // Struktur Organisasi
            ["unique_code" => "get_all_struktur_organ", "name" => "get all struktur_organ", "http_method" => "get", "url" => "struktur_organ", "path" => "SHEProper\SHEStrukturOrganController@index", "middleware" => null],
            ["unique_code" => "get_detail_struktur_organ", "name" => "get detail struktur_organ", "http_method" => "get", "url" => "struktur_organ/{id}", "path" => "SHEProper\SHEStrukturOrganController@show", "middleware" => null],
            ["unique_code" => "create_struktur_organ", "name" => "create struktur_organ", "http_method" => "post", "url" => "struktur_organ", "path" => "SHEProper\SHEStrukturOrganController@store", "middleware" => null],
            ["unique_code" => "update_struktur_organ", "name" => "update struktur_organ", "http_method" => "put", "url" => "struktur_organ/{id}", "path" => "SHEProper\SHEStrukturOrganController@update", "middleware" => null],
            ["unique_code" => "delete_struktur_organ", "name" => "delete struktur_organ", "http_method" => "delete", "url" => "struktur_organ/{id}", "path" => "SHEProper\SHEStrukturOrganController@destroy", "middleware" => null],
            ["unique_code" => "upload_struktur_organ", "name" => "upload struktur_organ", "http_method" => "post", "url" => "file_struktur_organ", "path" => "SHEProper\SHEStrukturOrganController@upload_file", "middleware" => null],
            // End Struktur Organisasi

            // KPI Proper
            ["unique_code" => "get_all_kpiproper", "name" => "get all kpiproper", "http_method" => "get", "url" => "kpiproper", "path" => "SHEProper\SHEKpiProperController@index", "middleware" => null],
            ["unique_code" => "get_detail_kpiproper", "name" => "get detail kpiproper", "http_method" => "get", "url" => "kpiproper/{id}", "path" => "SHEProper\SHEKpiProperController@show", "middleware" => null],
            ["unique_code" => "create_kpiproper", "name" => "create kpiproper", "http_method" => "post", "url" => "kpiproper", "path" => "SHEProper\SHEKpiProperController@store", "middleware" => null],
            ["unique_code" => "update_kpiproper", "name" => "update kpiproper", "http_method" => "put", "url" => "kpiproper/{id}", "path" => "SHEProper\SHEKpiProperController@update", "middleware" => null],
            ["unique_code" => "delete_kpiproper", "name" => "delete kpiproper", "http_method" => "delete", "url" => "kpiproper/{id}", "path" => "SHEProper\SHEKpiProperController@destroy", "middleware" => null],
            ["unique_code" => "template_kpiproper", "name" => "template kpiproper", "http_method" => "get", "url" => "kpiproper_template/", "path" => "SHEProper\SHEKpiProperController@template", "middleware" => null],
            ["unique_code" => "import_kpiproper", "name" => "import kpiproper", "http_method" => "post", "url" => "kpiproper_import/", "path" => "SHEProper\SHEKpiProperController@import", "middleware" => null],
            // End KPI Proper

            // Program Linkungan
            ["unique_code" => "get_all_program_ling", "name" => "get all program_ling", "http_method" => "get", "url" => "program_ling", "path" => "SHEProper\SHEProgramLingController@index", "middleware" => null],
            ["unique_code" => "get_detail_program_ling", "name" => "get detail program_ling", "http_method" => "get", "url" => "program_ling/{id}", "path" => "SHEProper\SHEProgramLingController@show", "middleware" => null],
            ["unique_code" => "create_program_ling", "name" => "create program_ling", "http_method" => "post", "url" => "program_ling", "path" => "SHEProper\SHEProgramLingController@store", "middleware" => null],
            ["unique_code" => "update_program_ling", "name" => "update program_ling", "http_method" => "put", "url" => "program_ling/{id}", "path" => "SHEProper\SHEProgramLingController@update", "middleware" => null],
            ["unique_code" => "delete_program_ling", "name" => "delete program_ling", "http_method" => "delete", "url" => "program_ling/{id}", "path" => "SHEProper\SHEProgramLingController@destroy", "middleware" => null],
            // End Program Lingkungan

            // Program Linkungan Inisiatif Strategis
            ["unique_code" => "get_all_program_ling_is", "name" => "get all program_ling_is", "http_method" => "get", "url" => "program_ling_is", "path" => "SHEProper\SHEProgramLingIsController@index", "middleware" => null],
            ["unique_code" => "get_detail_program_ling_is", "name" => "get detail program_ling_is", "http_method" => "get", "url" => "program_ling_is/{id}", "path" => "SHEProper\SHEProgramLingIsController@show", "middleware" => null],
            ["unique_code" => "create_program_ling_is", "name" => "create program_ling_is", "http_method" => "post", "url" => "program_ling_is", "path" => "SHEProper\SHEProgramLingIsController@store", "middleware" => null],
            ["unique_code" => "update_program_ling_is", "name" => "update program_ling_is", "http_method" => "put", "url" => "program_ling_is/{id}", "path" => "SHEProper\SHEProgramLingIsController@update", "middleware" => null],
            ["unique_code" => "delete_program_ling_is", "name" => "delete program_ling_is", "http_method" => "delete", "url" => "program_ling_is/{id}", "path" => "SHEProper\SHEProgramLingIsController@destroy", "middleware" => null],
            // End Program Linkungan Inisiatif Strategis

            // Perizinan Linkungan
            ["unique_code" => "get_all_izin_ling", "name" => "get all izin_ling", "http_method" => "get", "url" => "izin_ling", "path" => "SHEProper\SHEIzinLingController@index", "middleware" => null],
            ["unique_code" => "get_detail_izin_ling", "name" => "get detail izin_ling", "http_method" => "get", "url" => "izin_ling/{id}", "path" => "SHEProper\SHEIzinLingController@show", "middleware" => null],
            ["unique_code" => "create_izin_ling", "name" => "create izin_ling", "http_method" => "post", "url" => "izin_ling", "path" => "SHEProper\SHEIzinLingController@store", "middleware" => null],
            ["unique_code" => "update_izin_ling", "name" => "update izin_ling", "http_method" => "put", "url" => "izin_ling/{id}", "path" => "SHEProper\SHEIzinLingController@update", "middleware" => null],
            ["unique_code" => "delete_izin_ling", "name" => "delete izin_ling", "http_method" => "delete", "url" => "izin_ling/{id}", "path" => "SHEProper\SHEIzinLingController@destroy", "middleware" => null],
            // End Perizinan Lingkungan

            // Perizinan Linkungan File
            ["unique_code" => "get_all_izin_ling_file", "name" => "get all izin_ling_file", "http_method" => "get", "url" => "izin_ling_file", "path" => "SHEProper\SHEIzinLingFileController@index", "middleware" => null],
            ["unique_code" => "get_detail_izin_ling_file", "name" => "get detail izin_ling_file", "http_method" => "get", "url" => "izin_ling_file/{id}", "path" => "SHEProper\SHEIzinLingFileController@show", "middleware" => null],
            ["unique_code" => "create_izin_ling_file", "name" => "create izin_ling_file", "http_method" => "post", "url" => "izin_ling_file", "path" => "SHEProper\SHEIzinLingFileController@store", "middleware" => null],
            ["unique_code" => "update_izin_ling_file", "name" => "update izin_ling_file", "http_method" => "put", "url" => "izin_ling_file/{id}", "path" => "SHEProper\SHEIzinLingFileController@update", "middleware" => null],
            ["unique_code" => "delete_izin_ling_file", "name" => "delete izin_ling_file", "http_method" => "delete", "url" => "izin_ling_file/{id}", "path" => "SHEProper\SHEIzinLingFileController@destroy", "middleware" => null],
            ["unique_code" => "upload_izin_ling_file", "name" => "upload izin_ling_file", "http_method" => "post", "url" => "file_izin_ling_file", "path" => "SHEProper\SHEIzinLingFileController@upload_file", "middleware" => null],
            // End Perizinan Lingkungan File

            // Matrik Peraturan Perundangan Lingkungan
            ["unique_code" => "get_all_matrik_ling", "name" => "get all matrik_ling", "http_method" => "get", "url" => "matrik_ling", "path" => "SHEProper\SHEMatrikLingController@index", "middleware" => null],
            ["unique_code" => "get_detail_matrik_ling", "name" => "get detail matrik_ling", "http_method" => "get", "url" => "matrik_ling/{id}", "path" => "SHEProper\SHEMatrikLingController@show", "middleware" => null],
            ["unique_code" => "create_matrik_ling", "name" => "create matrik_ling", "http_method" => "post", "url" => "matrik_ling", "path" => "SHEProper\SHEMatrikLingController@store", "middleware" => null],
            ["unique_code" => "update_matrik_ling", "name" => "update matrik_ling", "http_method" => "put", "url" => "matrik_ling/{id}", "path" => "SHEProper\SHEMatrikLingController@update", "middleware" => null],
            ["unique_code" => "delete_matrik_ling", "name" => "delete matrik_ling", "http_method" => "delete", "url" => "matrik_ling/{id}", "path" => "SHEProper\SHEMatrikLingController@destroy", "middleware" => null],
            // End Matrik Peraturan Perundangan Lingkungan

            // Certificate of Analysis
            ["unique_code" => "get_all_sertif_analis", "name" => "get all sertif_analis", "http_method" => "get", "url" => "sertif_analis", "path" => "SHEProper\SHESertifAnalisController@index", "middleware" => null],
            ["unique_code" => "get_detail_sertif_analis", "name" => "get detail sertif_analis", "http_method" => "get", "url" => "sertif_analis/{id}", "path" => "SHEProper\SHESertifAnalisController@show", "middleware" => null],
            ["unique_code" => "create_sertif_analis", "name" => "create sertif_analis", "http_method" => "post", "url" => "sertif_analis", "path" => "SHEProper\SHESertifAnalisController@store", "middleware" => null],
            ["unique_code" => "update_sertif_analis", "name" => "update sertif_analis", "http_method" => "put", "url" => "sertif_analis/{id}", "path" => "SHEProper\SHESertifAnalisController@update", "middleware" => null],
            ["unique_code" => "delete_sertif_analis", "name" => "delete sertif_analis", "http_method" => "delete", "url" => "sertif_analis/{id}", "path" => "SHEProper\SHESertifAnalisController@destroy", "middleware" => null],
            ["unique_code" => "upload_sertif_analis", "name" => "upload sertif_analis", "http_method" => "post", "url" => "file_sertif_analis", "path" => "SHEProper\SHESertifAnalisController@upload_file", "middleware" => null],
            // End Certificate of Analysis

            // Sertifikat/penghargaan lingkungan
            ["unique_code" => "get_all_sertif_ling", "name" => "get all sertif_ling", "http_method" => "get", "url" => "sertif_ling", "path" => "SHEProper\SHESertifLingController@index", "middleware" => null],
            ["unique_code" => "get_detail_sertif_ling", "name" => "get detail sertif_ling", "http_method" => "get", "url" => "sertif_ling/{id}", "path" => "SHEProper\SHESertifLingController@show", "middleware" => null],
            ["unique_code" => "create_sertif_ling", "name" => "create sertif_ling", "http_method" => "post", "url" => "sertif_ling", "path" => "SHEProper\SHESertifLingController@store", "middleware" => null],
            ["unique_code" => "update_sertif_ling", "name" => "update sertif_ling", "http_method" => "put", "url" => "sertif_ling/{id}", "path" => "SHEProper\SHESertifLingController@update", "middleware" => null],
            ["unique_code" => "delete_sertif_ling", "name" => "delete sertif_ling", "http_method" => "delete", "url" => "sertif_ling/{id}", "path" => "SHEProper\SHESertifLingController@destroy", "middleware" => null],
            ["unique_code" => "upload_sertif_ling", "name" => "upload sertif_ling", "http_method" => "post", "url" => "file_sertif_ling", "path" => "SHEProper\SHESertifLingController@upload_file", "middleware" => null],
            // End Sertifikat/penghargaan lingkungan

            // Kompetensi Personil
            ["unique_code" => "get_all_kom_personil", "name" => "get all kom_personil", "http_method" => "get", "url" => "kom_personil", "path" => "SHEProper\SHEKomPersonilController@index", "middleware" => null],
            ["unique_code" => "get_detail_kom_personil", "name" => "get detail kom_personil", "http_method" => "get", "url" => "kom_personil/{id}", "path" => "SHEProper\SHEKomPersonilController@show", "middleware" => null],
            ["unique_code" => "create_kom_personil", "name" => "create kom_personil", "http_method" => "post", "url" => "kom_personil", "path" => "SHEProper\SHEKomPersonilController@store", "middleware" => null],
            ["unique_code" => "update_kom_personil", "name" => "update kom_personil", "http_method" => "put", "url" => "kom_personil/{id}", "path" => "SHEProper\SHEKomPersonilController@update", "middleware" => null],
            ["unique_code" => "delete_kom_personil", "name" => "delete kom_personil", "http_method" => "delete", "url" => "kom_personil/{id}", "path" => "SHEProper\SHEKomPersonilController@destroy", "middleware" => null],
            // End Kompetensi Personil

            // Kompetensi Personil File
            ["unique_code" => "get_all_kom_personil_file", "name" => "get all kom_personil_file", "http_method" => "get", "url" => "kom_personil_file", "path" => "SHEProper\SHEKomPersonilFileController@index", "middleware" => null],
            ["unique_code" => "get_detail_kom_personil_file", "name" => "get detail kom_personil_file", "http_method" => "get", "url" => "kom_personil_file/{id}", "path" => "SHEProper\SHEKomPersonilFileController@show", "middleware" => null],
            ["unique_code" => "create_kom_personil_file", "name" => "create kom_personil_file", "http_method" => "post", "url" => "kom_personil_file", "path" => "SHEProper\SHEKomPersonilFileController@store", "middleware" => null],
            ["unique_code" => "update_kom_personil_file", "name" => "update kom_personil_file", "http_method" => "put", "url" => "kom_personil_file/{id}", "path" => "SHEProper\SHEKomPersonilFileController@update", "middleware" => null],
            ["unique_code" => "delete_kom_personil_file", "name" => "delete kom_personil_file", "http_method" => "delete", "url" => "kom_personil_file/{id}", "path" => "SHEProper\SHEKomPersonilFileController@destroy", "middleware" => null],
            ["unique_code" => "upload_kom_personil_file", "name" => "upload kom_personil_file", "http_method" => "post", "url" => "file_kom_personil_file", "path" => "SHEProper\SHEKomPersonilFileController@upload_file", "middleware" => null],
            // End Kompetensi Personil File
            //end SHE-Proper

            //Realisasi Capex
            ["unique_code" => "upload_realisasi_capex", "name" => "Upload Realisasi Capex", "http_method" => "post", "url" => "realisasi-capex/upload/", "path" => "RealisasiCapex\RealisasiCapexController@upload", "middleware" => null],
            ["unique_code" => "get_realisasi_capex", "name" => "Get Realisasi Capex", "http_method" => "get", "url" => "realisasi-capex/get-datatable/", "path" => "RealisasiCapex\RealisasiCapexController@index", "middleware" => null],
            ["unique_code" => "get_progress_realisasi", "name" => "Progress Realisasi", "http_method" => "get", "url" => "realisasi-capex/get-progress-realisasi", "path" => "RealisasiCapex\RealisasiCapexController@getBarProgressRealisasiCapex", "middleware" => null],
            //End Realisasi Capex
            //Capex Item
            ["unique_code" => "upload_capex_item", "name" => "Upload Capex Item", "http_method" => "post", "url" => "capex-item/upload/", "path" => "RealisasiCapex\RealisasiCapexController@uploaditem", "middleware" => null],
            ["unique_code" => "get_capex_item", "name" => "Get Capex Item", "http_method" => "get", "url" => "capex-item/get-datatable/", "path" => "RealisasiCapex\RealisasiCapexController@indexitem", "middleware" => null],
            //End Capex Item

            // Neraca Limbah B3
            ["unique_code" => "get_neraca_lingkungan_list", "name" => "get neraca lingkungan list", "http_method" => "post", "url" => "neraca-limbah-b3/list", "path" => "LimbahB3\NeracaLimbahB3Controller@index", "middleware" => null],
            ["unique_code" => "export_neraca_lingkungan", "name" => "export neraca lingkungan", "http_method" => "get", "url" => "neraca-limbah-b3/export", "path" => "LimbahB3\NeracaLimbahB3Controller@exportData", "middleware" => null],
            ["unique_code" => "store_neraca_lingkungan", "name" => "store neraca lingkungan list", "http_method" => "post", "url" => "neraca-limbah-b3/store", "path" => "LimbahB3\NeracaLimbahB3Controller@store", "middleware" => null],

            // Inspeksi Lingkungan
            ["unique_code" => "get_inspeksi_lingkungan_list", "name" => "get inspeksi lingkungan list", "http_method" => "post", "url" => "inspeksi-lingkungan/list", "path" => "InspeksiLingkungan\InspeksiLingkunganController@index", "middleware" => null],
            ["unique_code" => "delete_inspeksi_lingkungan", "name" => "delete inspeksi lingkungan", "http_method" => "delete", "url" => "inspeksi-lingkungan/delete/{uuid}", "path" => "InspeksiLingkungan\InspeksiLingkunganController@delete", "middleware" => null],
            ["unique_code" => "detail_inspeksi_lingkungan", "name" => "detail inspeksi lingkungan", "http_method" => "get", "url" => "inspeksi-lingkungan/edit/{uuid}", "path" => "InspeksiLingkungan\InspeksiLingkunganController@show", "middleware" => null],
            ["unique_code" => "store_inspeksi_lingkungan", "name" => "store inspeksi lingkungan", "http_method" => "post", "url" => "inspeksi-lingkungan/store", "path" => "InspeksiLingkungan\InspeksiLingkunganController@store", "middleware" => null],
            ["unique_code" => "update_inspeksi_lingkungan", "name" => "update inspeksi lingkungan", "http_method" => "post", "url" => "inspeksi-lingkungan/update/{uuid}", "path" => "InspeksiLingkungan\InspeksiLingkunganController@update", "middleware" => null],
            ["unique_code" => "download_template_inspeksi_lingkungan", "name" => "download template inspeksi lingkungan", "http_method" => "get", "url" => "inspeksi-lingkungan/download/template", "path" => "InspeksiLingkungan\InspeksiLingkunganController@downloadTemplate", "middleware" => null],
            ["unique_code" => "import_inspeksi_lingkungan", "name" => "import inspeksi lingkungan", "http_method" => "post", "url" => "upload-inspeksi-lingkungan", "path" => "InspeksiLingkungan\InspeksiLingkunganController@ImportInspeksiLingkungan", "middleware" => null],
            ["unique_code" => "dashboard_inspeksi_lingkungan", "name" => "dashboard inspeksi lingkungan", "http_method" => "post", "url" => "inspeksi-lingkungan/dashboard", "path" => "InspeksiLingkungan\InspeksiLingkunganController@dashboard", "middleware" => null],

        ];

        foreach ($routes as $route) {
            Route::updateOrCreate(
                ["unique_code" => $route["unique_code"], "name" => $route["name"]],
                $route
            );
        }

        $routes_delete = [
            // Master Kategori Project Capex
            ["unique_code" => "get_all_kate_capex", "name" => "get all kategori capex", "http_method" => "get", "url" => "kateprojectcapex", "path" => "MasterData\MKateProjectCapexController@index", "middleware" => null],
            ["unique_code" => "get_detail_kate_capex", "name" => "get detail kategori capex", "http_method" => "get", "url" => "kateprojectcapex/{id}", "path" => "MasterData\MKateProjectCapexController@show", "middleware" => null],
            ["unique_code" => "create_kate_capex", "name" => "create kategori capex", "http_method" => "post", "url" => "kateprojectcapex", "path" => "MasterData\MKateProjectCapexController@store", "middleware" => null],
            ["unique_code" => "update_kate_capex", "name" => "update kategori capex", "http_method" => "put", "url" => "kateprojectcapex/{id}", "path" => "MasterData\MKateProjectCapexController@update", "middleware" => null],
            ["unique_code" => "delete_kate_capex", "name" => "delete kategori capex", "http_method" => "delete", "url" => "kateprojectcapex/{id}", "path" => "MasterData\MKateProjectCapexController@destroy", "middleware" => null],
            // End Kategori Master Project Capex

            //Overhaul
            ["unique_code" => "template_overhaul_prep_part", "name" => "template overhaul preparation part", "http_method" => "get", "url" => "overhaul/preparation/sparepart", "path" => "Overhaul\OverhaulPreparationController@templateSparepart", "middleware" => null],
            ["unique_code" => "import_overhaul_prep_part", "name" => "upload data overhaul preparation part", "http_method" => "post", "url" => "overhaul/preparation/sparepart", "path" => "Overhaul\OverhaulPreparationController@uploadSparepart", "middleware" => null],
            ["unique_code" => "template_overhaul_prep_cost", "name" => "template biaya overhaul preparation", "http_method" => "get", "url" => "overhaul/preparation/biaya", "path" => "Overhaul\OverhaulPreparationController@templateBiaya", "middleware" => null],
            ["unique_code" => "import_overhaul_prep_cost", "name" => "upload data biaya overhaul preparation", "http_method" => "post", "url" => "overhaul/preparation/biaya", "path" => "Overhaul\OverhaulPreparationController@uploadBiaya", "middleware" => null],

            ["unique_code" => "get_overhaul_activity_template", "name" => "get all overahul activity template", "http_method" => "get", "url" => "overhaul/progress/template/activity", "path" => "Overhaul\OverhaulProgressController@getActivityTemplate", "middleware" => null],
            ["unique_code" => "upload_overhaul_activity", "name" => "upload overhaul activity", "http_method" => "post", "url" => "overhaul/progress/upload/activity", "path" => "Overhaul\OverhaulProgressController@uploadActivity", "middleware" => null],

        ];

        foreach ($routes_delete as $route) {
            Route::where("unique_code", $route["unique_code"])->delete();
        }

        $this->command->info("Route berhasil ditambahkan");
    }
}

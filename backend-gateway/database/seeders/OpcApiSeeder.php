<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\M_OpcApi;
use App\Models\Route;
use App\Models\Permission;
use App\Models\Module;
use App\Models\Feature;

class OpcApiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $apis = [
            [
                'api_name' => 'OPC REST', 
                'api_url' => 'http://10.15.5.126:58725/OPCREST/getdata',
                'params' => 'params',
                'method' => 'GET',
            ],
            [
                'api_name' => 'OPC Tuban12', 
                'api_url' => 'http://opctuban12/',
                'params' => 'params',
                'method' => 'POST',
            ],
            [
                'api_name' => 'OPC Tuban34', 
                'api_url' => 'http://opctuban34/',
                'params' => 'params',
                'method' => 'POST',
            ],
            [
                'api_name' => 'OPC WHRPG', 
                'api_url' => 'http://opctubanwhrpg/',
                'params' => 'params',
                'method' => 'POST',
            ],
            [
                'api_name' => 'PlgWeb Tuban 3', 
                'api_url' => 'http://10.6.15.30/PlgWebHome/PlantGuideReports/PdfRoutine/PPE_Summary_Sched_{{tgl}}_A_day_Daily.xls',
                'params' => 'params',
                'method' => 'GET',
            ],
            [
                'api_name' => 'PlgWeb Tuban 4', 
                'api_url' => 'http://10.6.15.11/PlgWebHome/PlantGuideReports/PdfRoutine/PPE_Sum_Sched_{{tgl}}_A_day_Daily.xls',
                'params' => 'params',
                'method' => 'GET',
            ],
        ];
        
        foreach($apis as $key=>$item){
            $item['created_by'] = 2;
            M_OpcApi::updateOrCreate(['api_name' => $item['api_name']], $item);
        }
    }
}

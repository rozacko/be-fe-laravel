<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\Route;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            //User Permission
            ['permission' => ['unique_code' => 'create_user', 'name' => 'create user', 'guard_name' => 'api', 'feature_id' => 'FM-USR', 'action_id' => 'CR'], 'route' => ['create_user', 'add_users_role']],
            ['permission' => ['unique_code' => 'view_user', 'name' => 'view user', 'guard_name' => 'api', 'feature_id' => 'FM-USR', 'action_id' => 'RE'], 'route' => ['get_all_user', 'get_detail_user']],
            ['permission' => ['unique_code' => 'update_user', 'name' => 'update user', 'guard_name' => 'api', 'feature_id' => 'FM-USR', 'action_id' => 'UP'], 'route' => ['update_user']],
            ['permission' => ['unique_code' => 'delete_user', 'name' => 'delete user', 'guard_name' => 'api', 'feature_id' => 'FM-USR', 'action_id' => 'DE'], 'route' => ['delete_user']],
            ['permission' => ['unique_code' => 'create_role', 'name' => 'create role', 'guard_name' => 'api', 'feature_id' => 'FM-ROL', 'action_id' => 'CR'], 'route' => ['create_role', 'add_role_permission', 'add_user_to_role', 'remove_user_from_role']],
            ['permission' => ['unique_code' => 'view_role', 'name' => 'view role', 'guard_name' => 'api', 'feature_id' => 'FM-ROL', 'action_id' => 'RE'], 'route' => ['get_all_role', 'get_detail_role', 'get_role_permission']],
            ['permission' => ['unique_code' => 'update_role', 'name' => 'update role', 'guard_name' => 'api', 'feature_id' => 'FM-ROL', 'action_id' => 'UP'], 'route' => ['update_role']],
            ['permission' => ['unique_code' => 'delete_role', 'name' => 'delete role', 'guard_name' => 'api', 'feature_id' => 'FM-ROL', 'action_id' => 'DE'], 'route' => ['delete_role']],
            //End User Permission
            //OPC Permission
            ['permission' => ['unique_code' => 'create_opc', 'name' => 'create opc', 'guard_name' => 'api', 'feature_id' => 'FM-OPC', 'action_id' => 'CR'], 'route' => ['get_detail_tag']],
            ['permission' => ['unique_code' => 'view_opc', 'name' => 'view opc', 'guard_name' => 'api', 'feature_id' => 'FM-OPC', 'action_id' => 'RE'], 'route' => ['get_all_opc_tag', 'get_detail_tag']],
            ['permission' => ['unique_code' => 'update_opc', 'name' => 'update opc', 'guard_name' => 'api', 'feature_id' => 'FM-OPC', 'action_id' => 'UP'], 'route' => ['get_opc_data','update_opc_tag']],
            //End OPC Permission
            //Overhaul Prep Permission
            ['permission' => ['unique_code' => 'create_overhaul_prep', 'name' => 'create overhaul preparation', 'guard_name' => 'api', 'feature_id' => 'FOV-PRE', 'action_id' => 'CR'], 'route' => ['get_detail_tag']],
            ['permission' => ['unique_code' => 'view_overhaul_prep', 'name' => 'view overhaul preparation', 'guard_name' => 'api', 'feature_id' => 'FOV-PRE', 'action_id' => 'RE'], 'route' => ['get_all_opc_tag', 'get_detail_tag']],
            ['permission' => ['unique_code' => 'update_overhaul_prep', 'name' => 'update overhaul preparation', 'guard_name' => 'api', 'feature_id' => 'FOV-PRE', 'action_id' => 'UP'], 'route' => ['get_opc_data','update_opc_tag','sync_opc_tag']],
            //End Overhaul Prep Permisison
            //Overhaul Progress Permission
            ['permission' => ['unique_code' => 'create_overhaul_prog', 'name' => 'create overhaul progress', 'guard_name' => 'api', 'feature_id' => 'FOV-PRO', 'action_id' => 'CR'], 'route' => ['add_overhaul_periode','add_overhaul_activity']],
            ['permission' => ['unique_code' => 'view_overhaul_prog', 'name' => 'view overhaul progress', 'guard_name' => 'api', 'feature_id' => 'FOV-PRO', 'action_id' => 'RE'], 'route' => ['get_overhaul_periode','get_overhaul_progress_table','get_overhaul_activity',]],
            ['permission' => ['unique_code' => 'update_overhaul_prog', 'name' => 'update overhaul progress', 'guard_name' => 'api', 'feature_id' => 'FOV-PRO', 'action_id' => 'UP'], 'route' => ['add_overhaul_activity_progress']],
            //End Overhaul Progress Permisison
        ];
        foreach ($data as $value) {
            $model = Permission::updateOrCreate(
                ["unique_code" => $value["permission"]["unique_code"], "name" => $value["permission"]["name"]],
                $value["permission"]
            );
            $route = Route::whereIn('unique_code', $value['route'])->get()->pluck('id');
            $model->routes()->sync($route);
        }
    }
}

<?php

namespace Database\Seeders;

use App\Models\Feature;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class FeatureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['id' => 'FM-USR', 'name' => 'User', 'module_id' => 'M-UM'],
            ['id' => 'FM-ROL', 'name' => 'Role & Permission', 'module_id' => 'M-UM'],

            //Overhaul Feature
            ['id' => 'FOV-PRE', 'name' => 'Overhaul Preparation', 'module_id' => 'OVH'],
            ['id' => 'FOV-PRO', 'name' => 'Overhaul Progress', 'module_id' => 'OVH'],
            ['id' => 'FOV-FIN', 'name' => 'Overhaul Final Report', 'module_id' => 'OVH'],

            //OPC Feature
            ['id' => 'FM-OPC', 'name' => 'OPC Synchronization', 'module_id' => 'M-OPC'],
        ];

        foreach ($data as $value) {
            Feature::updateOrCreate(['id' => $value['id']], $value);
        }
    }
}

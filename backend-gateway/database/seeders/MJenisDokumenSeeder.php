<?php

namespace Database\Seeders;

use App\Models\MJenisDokumenEnviro;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class MJenisDokumenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['nama_dokumen' => 'AMDAL', "created_by"=>"system","updated_by"=>"system"],
            ['nama_dokumen' => 'Matriks RKL-RPL', "created_by"=>"system","updated_by"=>"system"],
            ['nama_dokumen' => 'Persetujuan Lingkungan', "created_by"=>"system","updated_by"=>"system"],
            ['nama_dokumen' => 'UKL-UPL', "created_by"=>"system","updated_by"=>"system"],
            ['nama_dokumen' => 'Dokumen Lain-lain', "created_by"=>"system","updated_by"=>"system"],
        ];

        foreach ($data as $value) {
            MJenisDokumenEnviro::updateOrCreate(['nama_dokumen' => $value['nama_dokumen']], $value);
        }
    }
}

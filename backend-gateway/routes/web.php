<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return response()->json(['message' => "It's works!"]);
});
Route::get('/config', function () {
    return redirect('config/log_activity');
});
Route::group(['namespace' => 'App\Http\Controllers\Configuration', 'prefix' => 'config'], function () {
    Route::get('login', 'AuthController@index')->name('web.login.page');
    Route::post('login', 'AuthController@store')->name('web.login.store');

    Route::middleware(['auth'])->group(function () {
        Route::get('logout', 'AuthController@destroy')->name('web.logout');

        Route::get('log_activity', 'LogActivityController@index')->name('web.log_activity');
        Route::get('route', 'RouteController@index')->name('web.route');
        Route::get('action', 'ActionController@index')->name('web.action');
        Route::get('module', 'ModuleController@index')->name('web.module');
        Route::get('module/{uuid}/features', 'ModuleController@features')->name('web.module.feature');
        Route::get('feature/{uuid}/permissions', 'FeatureController@permission')->name('web.feature.permission');

        Route::get('smtp', 'SmtpController@index')->name('web.smtp');
        Route::get('smtp/{uuid}/edit', 'SmtpController@edit')->name('web.smtp.edit');
        Route::get('smtp/create', 'SmtpController@create')->name('web.smtp.create');
        Route::post('smtp', 'SmtpController@store')->name('web.smtp.store');
        Route::put('smtp/{uuid}/active', 'SmtpController@active')->name('web.smtp.active');
        Route::put('smtp/{uuid}', 'SmtpController@update')->name('web.smtp.update');
        Route::delete('smtp/{uuid}', 'SmtpController@destroy')->name('web.smtp.destroy');

        Route::get('mail_template', 'MailTemplateController@index')->name('web.mail_template');
        Route::get('mail_template/{uuid}/edit', 'MailTemplateController@edit')->name('web.mail_template.edit');
        Route::get('mail_template/{uuid}/detail', 'MailTemplateController@show')->name('web.mail_template.show');
        Route::get('mail_template/create', 'MailTemplateController@create')->name('web.mail_template.create');
        Route::post('mail_template', 'MailTemplateController@store')->name('web.mail_template.store');
        Route::put('mail_template/{uuid}', 'MailTemplateController@update')->name('web.mail_template.update');
        Route::delete('mail_template/{uuid}', 'MailTemplateController@destroy')->name('web.mail_template.destroy');

        Route::get('failed_job', 'FailedJobController@index')->name('web.failed_job');
        Route::get("failed_job/{id}", "FailedJobController@retry")->name('web.failed_job.retry');

        Route::get('permission', 'PermissionController@index')->name('web.permission');

        Route::get("menu", "MenuController@index")->name('web.menu');
        Route::get("menu/list", "MenuController@list");
        Route::post("menu", "MenuController@store");
        Route::get("menu/{uuid}", "MenuController@show");
        Route::put("menu/{uuid}/edit", "MenuController@update");
        Route::put("menu/sequence", "MenuController@updateSequence");
        Route::delete("menu/{uuid}", "MenuController@destroy");
        Route::get("menu/{id}/add", "MenuController@create");

        Route::get("sap/log", "SapSyncLogController@index")->name('web.sap.log');
        Route::get("sap/resync/{id}", "SapSyncLogController@reSync")->name('web.sap.resync');
        Route::get("sap/config", "SapSyncConfigController@index")->name('web.sap.config');
        Route::post("sap/config", "SapSyncConfigController@store")->name('web.sap.config.store');
        Route::post("sap/config/test", "SapSyncConfigController@testSync")->name('web.sap.config.test');
        Route::post("sap/config/sync-manual", "SapSyncConfigController@manualSync")->name('web.sap.config.sync');
        Route::get("sap/config/{id}", "SapSyncConfigController@show")->name('web.sap.config.show');
        Route::put("sap/config/{id}", "SapSyncConfigController@update")->name('web.sap.config.update');
        Route::delete("sap/config/{id}", "SapSyncConfigController@destroy")->name('web.sap.config.destroy');
        
        Route::get('opc', 'OpcController@index')->name('web.opc');
        Route::get("opc/list", "OpcController@list")->name('web.opc.list');
        Route::get('opc/{uuid}/edit', 'OpcController@edit')->name('web.opc.edit');
        Route::get('opc/{uuid}/detail', 'OpcController@show')->name('web.opc.show');
        Route::get('opc/create', 'OpcController@create')->name('web.opc.create');
        Route::post('opc', 'OpcController@store')->name('web.opc.store');
        Route::put('opc/{uuid}', 'OpcController@update')->name('web.opc.update');
        Route::delete('opc/{uuid}', 'OpcController@destroy')->name('web.opc.destroy');

        Route::get('jenisopc', 'JenisOpcController@index')->name('web.jenisopc');
        Route::get("jenisopc/list", "JenisOpcController@list")->name('web.jenisopc.list');
        Route::get('jenisopc/{uuid}/edit', 'JenisOpcController@edit')->name('web.jenisopc.edit');
        Route::get('jenisopc/{uuid}/detail', 'JenisOpcController@show')->name('web.jenisopc.show');
        Route::get('jenisopc/create', 'JenisOpcController@create')->name('web.jenisopc.create');
        Route::post('jenisopc', 'JenisOpcController@store')->name('web.jenisopc.store');
        Route::put('jenisopc/{uuid}', 'JenisOpcController@update')->name('web.jenisopc.update');
        Route::delete('jenisopc/{uuid}', 'JenisOpcController@destroy')->name('web.jenisopc.destroy');
        Route::get('jenisopc/{area}/jenisopc','JenisOpcController@getJenisOpc');

        Route::get('opcapi', 'OpcApiController@index')->name('web.opcapi');
        Route::get("opcapi/list", "OpcApiController@list")->name('web.opcapi.list');
        Route::get('opcapi/{uuid}/edit', 'OpcApiController@edit')->name('web.opcapi.edit');
        Route::get('opcapi/{uuid}/detail', 'OpcApiController@show')->name('web.opcapi.show');
        Route::put('opcapi/{uuid}', 'OpcApiController@update')->name('web.opcapi.update');

        Route::get('shipmentapi', 'ApiShipmentController@index')->name('web.shipmentapi');
        Route::get("shipmentapi/list", "ApiShipmentController@list")->name('web.shipmentapi.list');
        Route::get('shipmentapi/{uuid}/edit', 'ApiShipmentController@edit')->name('web.shipmentapi.edit');
        Route::get('shipmentapi/{uuid}/detail', 'ApiShipmentController@show')->name('web.shipmentapi.show');
        Route::put('shipmentapi/{uuid}', 'ApiShipmentController@update')->name('web.shipmentapi.update');

         
        Route::get('shipment', 'ShipmentController@index')->name('web.shipment');
        Route::get("shipment/list", "ShipmentController@list")->name('web.shipment.list');
        Route::get('shipment/{uuid}/edit', 'ShipmentController@edit')->name('web.shipment.edit');
        Route::get('shipment/{uuid}/detail', 'ShipmentController@show')->name('web.shipment.show');
        Route::get('shipment/create', 'ShipmentController@create')->name('web.shipment.create');
        Route::post('shipment', 'ShipmentController@store')->name('web.shipment.store');
        Route::put('shipment/{uuid}', 'ShipmentController@update')->name('web.shipment.update');
        Route::delete('shipment/{uuid}', 'ShipmentController@destroy')->name('web.shipment.destroy');
    });
});

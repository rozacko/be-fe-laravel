<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/test','App\Http\Controllers\DataKoreksi\CogmController@import')->middleware(['jwt.verify', 'jwt.regenerate']);
Route::group(['namespace' => 'App\Http\Controllers'], function () {
    Route::post('login', 'ApiController@authenticate')->name('login');
    Route::post('register', 'ApiController@register')->name('register');
    Route::get('tpm/downloadLaporan/{filename}','Tpm\GugusController@downloadGugus');
    Route::get('vendormanagement/template/{file}', 'VendorManagement\ImportDataController@downloadTemplate');
    Route::post('file/delete', 'DokumenLingkungan\MasterDokumenLingkunganController@deleteFile');
    
    Route::group(['middleware' => ['jwt.verify', 'jwt.regenerate']], function () {
        Route::get('logout', 'ApiController@logout')->name('logout');
        Route::get('me/menu', 'Users\MenuController@index')->name('me.menu');
        $routes = routes();
        if ($routes) {
            foreach ($routes as $route) {
                $middleware = [];
                if ($route['middleware'] != "") {
                    $middleware = explode(";", $route['middleware']);
                }
                if (isset($route['permission']) && count($route['permission'])) {
                    $permission = "permissionz:" . implode("|", $route['permission']);
                    $middleware[] = $permission;
                }
                Route::{$route['http_method']}("{$route['url']}", "{$route['path']}")->name("{$route['name']}")->middleware($middleware);
            }
        }
    });
});

<x-layout>
    @section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Create OPC Tag</h4>
                </div>
                <div class="card-body">
                    <form action="{{route('web.opc.store')}}" id="form-create" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="row mb-5">
                            <div class="col-xs-12 form-group">
                                <label for="id_plant">Plant <span class="text-danger">*</span></label>
                                <select class="form-control select2" name="id_plant" id="id_plant" style="width: 100%;">
                                    <option class="form-control" value="">Select Plant</option>
                                    @foreach ($plant as $key => $item)
                                        <option class="form-control" value="{{ $item['id'] }}">{{ $item['nm_plant'] }} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row mb-5">
                            <div class="col-xs-12 form-group">
                                <label for="id_area">Area <span class="text-danger">*</span></label>
                                <select class="form-control select2" name="id_area" id="id_area" style="width: 100%;" onclick="changeEquipment()">
                                    <option class="form-control" value="">Select Area</option>
                                    @foreach ($area as $key => $item)
                                        <option class="form-control" value="{{ $item['id'] }}">{{ $item['nm_area'] }} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row mb-5">
                            <div class="col-xs-12 form-group">
                                <label for="id_jenis_opc">Jenis OPC <span class="text-danger">*</span></label>
                                <select class="form-control select2" name="id_jenis_opc" id="id_jenis_opc" style="width: 100%;">
                                    <option class="form-control" value="">Select Equipment</option>
                                </select>
                            </div>
                        </div>

                        <div class="row mb-5">
                            <div class="col-xs-12 form-group">
                                <label for="id_api">API <span class="text-danger">*</span></label>
                                <select class="form-control select2" name="id_api" id="id_api" style="width: 100%;">
                                    <option class="form-control" value="">Select API</option>
                                    @foreach ($api as $key => $item)
                                        <option class="form-control" value="{{ $item['id'] }}">{{ $item['api_name'] }} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row mb-5">
                            <div class="col-xs-12 form-group">
                                <label for="tag_name">Tag Name<span class="text-danger">*</span></label>
                                <input type="text" name="tag_name" id="tag_name" style="width: 100%;"/>
                            </div>
                        </div>

                        <div class="row mb-5">
                            <div class="col-xs-12 form-group">
                                <label for="sync_period">Sync Periode <span class="text-danger">*</span></label>
                                <select class="form-control select2" name="sync_period" id="sync_period" style="width: 100%;">
                                    <option class="form-control" value="">Select Sync Periode</option>
                                    <option class="form-control" value="Daily">Daily</option>
                                    <option class="form-control" value="Weekly">Weekly</option>
                                    <option class="form-control" value="Monthly">Monthly</option>
                                    <option class="form-control" value="Hourly">Hourly</option>
                                    <option class="form-control" value="Minutely">Minutely</option>
                                </select>
                            </div>
                        </div>

                        <div class="row mb-5">
                            <div class="col-xs-12 form-group">
                                <label for="sync_time">Sync Time <span class="text-danger">*</span></label>
                                <input type="time" name="sync_time" id="sync_time" style="width: 100%;"/>
                            </div>
                        </div>

                        <div class="row mb-5">
                            <div class="col-xs-12 form-group">
                                <button type="submit" id="btn-store" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
    @endsection
    @push('scripts')
    <script>
        function changeEquipment(){
            const id_area = $('#id_area').val();
            $.get("{{ url('config/jenisopc') }}/"+id_area+"/jenisopc", 
            function(data){
               let html = '<option class="form-control" value="">Select Jenis OPC</option>';
               $.each(data.data, function(idx, val){
                    html += '<option class="form-control" value="'+val.id+'">'+val.jenis_opc+'</option>';
               })
               $('#id_jenis_opc').html(html);
            },'json');
        }
    </script>
    @endpush
</x-layout>
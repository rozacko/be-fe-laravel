<x-layout>
    @section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Data OPC Tag</h4>
                </div>
                <div class="card-body">
                    <form id="form-create" method="POST">
                    <div class="row mb-5">
                            <div class="col-xs-12 form-group">
                                <label for="id_category">Plant <span class="text-danger">*</span></label>
                                <select class="form-control select2" name="id_category" id="id_category" style="width: 100%;" disabled>
                                    <option class="form-control" value="">Select Plant</option>
                                    @foreach ($plant as $key => $item)
                                        <option class="form-control" value="{{ $item['id'] }}" {{($item['id']==$data->id_plant ? 'selected' : '')}}>{{ $item['nm_plant'] }} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row mb-5">
                            <div class="col-xs-12 form-group">
                                <label for="id_area">Area <span class="text-danger">*</span></label>
                                <select class="form-control select2" name="id_area" id="id_area" style="width: 100%;" disabled>
                                    <option class="form-control" value="">Select Area</option>
                                    @foreach ($area as $key => $item)
                                        <option class="form-control" value="{{ $item['id'] }}" {{($item['id']==$data->jenis->id_area ? 'selected' : '')}}>{{ $item['nm_area'] }} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row mb-5">
                            <div class="col-xs-12 form-group">
                                <label for="id_jenis_opc">Jenis OPC <span class="text-danger">*</span></label>
                                <select class="form-control select2" name="id_jenis_opc" id="id_jenis_opc" style="width: 100%;" disabled>
                                    <option class="form-control" value="">Select Jenis OPC</option>
                                    @foreach ($jenisopc as $key => $item)
                                        <option class="form-control" value="{{ $item['id'] }}" {{($item['id']==$data->id_jenis_opc ? 'selected' : '')}}>{{ $item['jenis_opc'] }} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row mb-5">
                            <div class="col-xs-12 form-group">
                                <label for="id_api">API <span class="text-danger">*</span></label>
                                <select class="form-control select2" name="id_api" id="id_api" style="width: 100%;" disabled>
                                    <option class="form-control" value="">Select API</option>
                                    @foreach ($api as $key => $item)
                                        <option class="form-control" value="{{ $item['id'] }}" {{($item['id']==$data->id_api ? 'selected' : '')}}>{{ $item['api_name'] }} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row mb-5">
                            <div class="col-xs-12 form-group">
                                <label for="tag_name">Tag Name<span class="text-danger">*</span></label>
                                <input type="text" name="tag_name" id="tag_name" value ="{{$data->tag_name}}" style="width: 100%;" disabled/>
                            </div>
                        </div>

                        <div class="row mb-5">
                            <div class="col-xs-12 form-group">
                                <label for="sync_period">Sync Periode <span class="text-danger">*</span></label>
                                <select class="form-control select2" name="sync_period" id="sync_period" style="width: 100%;" disabled>
                                    <option class="form-control" value="">Select Sync Periode</option>
                                    <option class="form-control" value="Daily" {{$data->sync_period=='Daily' ? 'selected' : ''}}>Daily</option>
                                    <option class="form-control" value="Weekly" {{$data->sync_period=='Weekly' ? 'selected' : ''}}>Weekly</option>
                                    <option class="form-control" value="Monthly" {{$data->sync_period=='Monthly' ? 'selected' : ''}}>Monthly</option>
                                    <option class="form-control" value="Hourly" {{$data->sync_period=='Hourly' ? 'selected' : ''}}>Hourly</option>
                                    <option class="form-control" value="Minutely" {{$data->sync_period=='Minutely' ? 'selected' : ''}}>Minutely</option>
                                </select>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
    @endsection
    @push('scripts')
    @endpush
</x-layout>
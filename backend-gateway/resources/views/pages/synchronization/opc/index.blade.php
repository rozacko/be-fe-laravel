<x-layout>
    @section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">List OPC Tag</h4>
                    <div class="card-toolbar">
                        <div class="d-flex justify-content-end" data-kt-user-table-toolbar="base">
                            <a href="{{route('web.opc.create')}}" type="button" class="btn btn-primary">
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="data-table-fixed-columns" class="table table-row-bordered gy-5">
                            <thead>
                                <tr>
                                    <th class="text-nowrap">Plant</th>
                                    <th class="text-nowrap">Jenis OPC</th>
                                    <th class="text-nowrap">API</th>
                                    <th class="text-nowrap">Tag Name</th>
                                    <th class="text-nowrap no-sort">#</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Route</h4>
                    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                        <span class="svg-icon svg-icon-1">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black" />
                                <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black" />
                            </svg>
                        </span>
                    </div>
                </div>
                <div class="modal-body">

                </div>
            </div>
        </div>
    </div>

    @endsection
    @push('scripts')
    <script type="text/javascript">
        var datatable;
        $(document).ready(function() {
            datatable = $('#data-table-fixed-columns').DataTable({
                "language": {
                    "lengthMenu": "Show _MENU_",
                },
                "dom": "<'row'" +
                    "<'col-sm-6 d-flex align-items-center justify-conten-start'l>" +
                    "<'col-sm-6 d-flex align-items-center justify-content-end'f>" +
                    ">" +

                    "<'table-responsive'tr>" +

                    "<'row'" +
                    "<'col-sm-12 col-md-5 d-flex align-items-center justify-content-center justify-content-md-start'i>" +
                    "<'col-sm-12 col-md-7 d-flex align-items-center justify-content-center justify-content-md-end'p>" +
                    ">",
                searchDelay: 500,
                serverSide: true,
                processing: true,
                ajax: "{{route('web.opc.list')}}",
                columns: [{
                    data: 'plant'
                }, 
                {
                    data: 'jenis_opc'
                }, 
                {
                    data: 'api_name'
                }, 
                {
                    data: 'tag_name'
                }, 
                {
                    data: null,
                    render: function(r, v, i) {
                        return `
                            <div class="btn-group">
                                <a href="{{ url('config/opc') }}/${r.uuid}/detail" type="button" class="btn btn-sm btn-info">
                                    <i class="fa fa-info-circle"></i>
                                </a>&nbsp;
                                <a href="{{ url('config/opc') }}/${r.uuid}/edit" type="button" class="btn btn-sm btn-warning">
                                    <i class="fa fa-edit"></i>
                                </a>&nbsp;
                                <button type="button" onclick="destroy('${r.uuid}')" class="btn btn-sm btn-danger">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </div>
                        `;
                    }
                }]
            });
        });

        function destroy(uuid) {
            Swal.fire({
                title: 'Are you sure?',
                text: 'You will not be able to recover this data!',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes',
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                            url: "{{url('/config/opc')}}/" + uuid,
                            method: "POST",
                            data: {
                                _method: "DELETE",
                                _token: "{{ csrf_token() }}",
                            }
                        })
                        .done(function(response) {
                            var {
                                message
                            } = response;
                            Swal.fire({
                                icon: 'success',
                                title: message,
                                showConfirmButton: false,
                                timer: 1500
                            })
                            datatable.ajax.reload();
                        })
                        .fail(function(xhr) {
                            var {
                                message
                            } = xhr.responseJSON;
                            Swal.fire({
                                icon: 'error',
                                title: message,
                                showConfirmButton: false,
                                timer: 1500
                            })
                        })
                }
            })
        }
    </script>

    @endpush
</x-layout>
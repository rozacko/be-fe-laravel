<x-layout>
    @section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Edit Jenis OPC</h4>
                </div>
                <div class="card-body">
                    <form action="{{url('/config/jenisopc').'/'.$data->uuid}}" id="form-create" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="_method" value="PUT" />
                        <div class="row mb-5">
                            <div class="col-xs-12 form-group">
                                <label for="id_area">Area <span class="text-danger">*</span></label>
                                <select class="form-control select2" name="id_area" id="id_area" style="width: 100%;">
                                    <option class="form-control" value="">Select Area</option>
                                    @foreach ($area as $key => $item)
                                        <option class="form-control" value="{{ $item['id'] }}" {{($item['id']==$data->id_area ? 'selected' : '')}}>{{ $item['nm_area'] }} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row mb-5">
                            <div class="col-xs-12 form-group">
                                <label for="jenis_opc">Equipment Name<span class="text-danger">*</span></label>
                                <input type="text" name="jenis_opc" id="jenis_opc" value="{{$data->jenis_opc}}" style="width: 100%;"/>
                            </div>
                        </div>
                        <div class="row mb-5">
                            <div class="col-xs-12 form-group">
                                <button type="submit" id="btn-store" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
    @endsection
    @push('scripts')
    @endpush
</x-layout>
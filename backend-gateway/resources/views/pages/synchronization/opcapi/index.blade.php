<x-layout>
    @section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">List OPC API</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="data-table-fixed-columns" class="table table-row-bordered gy-5">
                            <thead>
                                <tr>
                                    <th class="text-nowrap">API Name</th>
                                    <th class="text-nowrap">API URL</th>
                                    <th class="text-nowrap no-sort">#</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Route</h4>
                    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                        <span class="svg-icon svg-icon-1">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black" />
                                <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black" />
                            </svg>
                        </span>
                    </div>
                </div>
                <div class="modal-body">

                </div>
            </div>
        </div>
    </div>

    @endsection
    @push('scripts')
    <script type="text/javascript">
        var datatable;
        $(document).ready(function() {
            datatable = $('#data-table-fixed-columns').DataTable({
                "language": {
                    "lengthMenu": "Show _MENU_",
                },
                "dom": "<'row'" +
                    "<'col-sm-6 d-flex align-items-center justify-conten-start'l>" +
                    "<'col-sm-6 d-flex align-items-center justify-content-end'f>" +
                    ">" +

                    "<'table-responsive'tr>" +

                    "<'row'" +
                    "<'col-sm-12 col-md-5 d-flex align-items-center justify-content-center justify-content-md-start'i>" +
                    "<'col-sm-12 col-md-7 d-flex align-items-center justify-content-center justify-content-md-end'p>" +
                    ">",
                searchDelay: 500,
                serverSide: true,
                processing: true,
                ajax: "{{route('web.opcapi.list')}}",
                columns: [{
                    data: 'api_name'
                }, 
                {   data: 'api_url'},
                {
                    data: null,
                    render: function(r, v, i) {
                        return `
                            <div class="btn-group">
                                <a href="{{ url('config/opcapi') }}/${r.uuid}/detail" type="button" class="btn btn-sm btn-info">
                                    <i class="fa fa-info-circle"></i>
                                </a>&nbsp;
                                <a href="{{ url('config/opcapi') }}/${r.uuid}/edit" type="button" class="btn btn-sm btn-warning">
                                    <i class="fa fa-edit"></i>
                                </a>&nbsp;
                            </div>
                        `;
                    }
                }]
            });
        });

    </script>

    @endpush
</x-layout>
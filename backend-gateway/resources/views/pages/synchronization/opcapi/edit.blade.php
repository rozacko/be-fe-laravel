<x-layout>
    @section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Edit OPC API</h4>
                </div>
                <div class="card-body">
                    <form action="{{url('/config/opcapi').'/'.$uuid}}" id="form-create" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="_method" value="PUT" />

                        <div class="row mb-5">
                            <div class="col-xs-12 form-group">
                                <label for="api_name">API Name<span class="text-danger">*</span></label>
                                <input type="text" name="api_name" id="api_name" value="{{$api_name}}" style="width: 100%;"/>
                            </div>
                        </div>

                        <div class="row mb-5">
                            <div class="col-xs-12 form-group">
                                <label for="api_url">API URL<span class="text-danger">*</span></label>
                                <input type="text" name="api_url" id="api_url"  value="{{$api_url}}" style="width: 100%;"/>
                            </div>
                        </div>
                        <div class="row mb-5">
                            <div class="col-xs-12 form-group">
                                <button type="submit" id="btn-store" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
    @endsection
    @push('scripts')
    @endpush
</x-layout>
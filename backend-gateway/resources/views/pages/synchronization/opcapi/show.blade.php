<x-layout>
    @section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Data OPC API</h4>
                </div>
                <div class="card-body">
                    <form id="form-create" method="POST">
                        <div class="row mb-5">
                            <div class="col-xs-12 form-group">
                                <label for="api_name">API Name<span class="text-danger">*</span></label>
                                <input type="text" name="api_name" id="api_name" value="{{$api_name}}" style="width: 100%;" disabled/>
                            </div>
                        </div>

                        <div class="row mb-5">
                            <div class="col-xs-12 form-group">
                                <label for="api_url">API URL<span class="text-danger">*</span></label>
                                <input type="text" name="api_url" id="api_url"  value="{{$api_url}}" style="width: 100%;" disabled/>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
    @endsection
    @push('scripts')
    @endpush
</x-layout>
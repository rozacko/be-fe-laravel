<form>
    <input type="hidden" name="_method" value="PUT">
    <input type="hidden" name="uuid" value="{{ $menu->uuid }}">
    <div class="row mb-5">
        <div class="col-md-12 form-group">
            <label for="name">ID <span class="text-danger">*</span></label>
            <input type="number" class="form-control" disabled value="{{ $menu->id }}">
        </div>
    </div>
    <div class="row mb-5">
        <div class="col-md-12 form-group">
            <label for="name">Name <span class="text-danger">*</span></label>
            <input type="text" class="form-control" id="name" value="{{ $menu->name }}" name="name">
        </div>
    </div>
    <div class="row mb-5">
        <div class="col-md-12 form-group">
            <label for="description">Description</label>
            <input type="text" class="form-control" id="description" value="{{ $menu->description }}" name="description">
        </div>
    </div>
    <div class="row mb-5">
        <div class="col-md-12 form-group">
            <label for="url">URL <span class="text-danger">*</span></label>
            <input type="text" class="form-control" id="url" value="{{ $menu->url }}" name="url">
        </div>
    </div>
    <div class="row mb-5">
        <div class="col-md-12 form-group">
            <label for="icon">Icon</label>
            <input type="text" class="form-control" id="icon" value="{{ $menu->icon }}" name="icon">
        </div>
    </div>
    <div class="row mb-5">
        <div class="col-md-12 form-group">
            <label for="url">PowerBI URL</label>
            <input type="text" class="form-control" id="powerbi_url" value="{{ $menu->powerbi_url }}" name="powerbi_url">
        </div>
    </div>
    <div class="row mb-5">
        <div class="col-md-12 form-group">
            <label for="name">Permission Name <span class="text-danger">*</span></label>
            <select class="form-control" id="permission" name="permission_id">
                @if($menu->permission_id)
                <option value="{{ $menu->permission_id }}" selected>
                    {{ $menu->module_name }} > {{ $menu->feature_name }} > {{ $menu->action_name }}
                </option>
                @else
                <option></option>
                @endif
            </select>
        </div>
    </div>
    <div class="row mb-5">
        <div class="col-md-12 pull-right">
            <button class="btn btn-primary" id="btn-update" type="button">
                <i class="fas fa-spinner fa-spin spinner-btn"></i>&nbsp;Update
            </button>
        </div>
    </div>
</form>

<script type="text/javascript">
    $(function() {
        $(".spinner-btn").hide();
        $("#permission").select2({
            dropdownParent: $("#modal-menu .modal-content"),
            ajax: {
                headers: {
                    "Accept": "text/json",
                    "Content-Type": "application/json",
                },
                url: '/config/permission/',
                data: function(params) {
                    var query = {
                        name: params.term
                    }
                    return query;
                },
                processResults: function(data) {
                    if (data.data && data.data.length == 0) {
                        $('#permission').val(null).trigger('change');
                    }
                    return {
                        results: data.data.map(item => {
                            return {
                                id: item.id,
                                text: item.module_name + " > " + item.feature_name + " > " + item.action_name
                            };
                        })
                    };
                }
            },
            placeholder: "No Permission"
        });
    })
</script>
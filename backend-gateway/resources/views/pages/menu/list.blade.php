@php
    function render_list($menus) {
        $html = '';
        if(is_array($menus)){
            $html .= '<ol class="dd-list">';
            foreach ($menus as $key => $menu) {
                $html .= '<li class="dd-item" data-id="'.$menu['id'].'">';
                    $html .= '<div class="dd-handle"><i class="' . $menu["icon"] . '"></i> ' . $menu["name"] . ' &emsp;&emsp;<small style="font-weight:100;">Permission: '.ucwords($menu['permission']['name']).'</small></div>';
                    $html .= '<div class="dd-action pull-right">';
                        $html .= '<button type="button" data-id="'.$menu["id"].'" class="btn btn-primary btn-sm btn-add"><i class="fa fa-plus fa-fw"></i></button>&nbsp;';
                        $html .= '<button type="button" data-uuid="'.$menu["uuid"].'" class="btn btn-warning btn-sm btn-edit"><i class="fa fa-edit fa-fw"></i></button>&nbsp;';
                        $html .= '<button type="button" data-uuid="'.$menu["uuid"].'" class="btn btn-danger btn-sm btn-delete"><i class="fa fa-trash fa-fw"></i></a>';
                    $html .= '</div>';
                    if(isset($menu["children"])){
                        $html .= render_list($menu["children"]);
                    }
                $html .= "</li>";
            }
            $html .= '</ol>';
        }
        return $html;
    }
    echo render_list($model);
@endphp

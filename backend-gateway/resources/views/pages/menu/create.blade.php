<form id="form-create">
    <div class="row mb-5">
        <div class="col-md-12 form-group">
            <label for="name">ID <span class="text-danger">*</span></label>
            <input type="number" class="form-control" id="id" name="id">
        </div>
    </div>
    <div class="row mb-5">
        <div class="col-md-12 form-group">
            <label for="name">Parent</label>
            <select name="parent_id" class="form-control">
                <option value="0">No Parent</option>
                @if ($menu)
                <option value="{{ $menu->id }}" selected>{{ $menu->name }}</option>
                @endif
            </select>
        </div>
    </div>
    <div class="row mb-5">
        <div class="col-md-12 form-group">
            <label for="name">Name <span class="text-danger">*</span></label>
            <input type="text" class="form-control" id="name" name="name">
        </div>
    </div>
    <div class="row mb-5">
        <div class="col-md-12 form-group">
            <label for="description">Description</label>
            <input type="text" class="form-control" id="description" name="description">
        </div>
    </div>
    <div class="row mb-5">
        <div class="col-md-12 form-group">
            <label for="url">URL <span class="text-danger">*</span></label>
            <input type="text" class="form-control" id="url" name="url">
        </div>
    </div>
    <div class="row mb-5">
        <div class="col-md-12 form-group">
            <label for="icon">Icon</label>
            <input type="text" class="form-control" id="icon" name="icon">
        </div>
    </div>
    <div class="row mb-5">
        <div class="col-md-12 form-group">
            <label for="url">PowerBI URL</label>
            <input type="text" class="form-control" id="powerbi_url" name="powerbi_url">
        </div>
    </div>
    <div class="row mb-5">
        <div class="col-md-12 form-group">
            <label for="name">Permission Name <span class="text-danger">*</span></label>
            <select class="form-select form-select-solid" data-kt-select2="true" data-placeholder="Select option" data-allow-clear="true" id="permission" name="permission_id">
                <option></option>
            </select>
        </div>
    </div>
    <div class="row mb-5">
        <div class="col-xs-12 form-group">
            <button type="button" id="btn-save" class="btn btn-primary"><i class="fas fa-spinner fa-spin spinner-btn"></i> Save</button>
        </div>
    </div>
</form>
<script type="text/javascript">
    $(function() {
        $(".spinner-btn").hide();
        $("#permission").select2({
            dropdownParent: $("#modal-menu .modal-content"),
            ajax: {
                headers: {
                    "Accept": "text/json",
                    "Content-Type": "application/json",
                },
                url: '/config/permission/',
                data: function(params) {
                    var query = {
                        name: params.term
                    }
                    return query;
                },
                processResults: function(data) {
                    if (data.data && data.data.length==0) {
                        $('#permission').val(null).trigger('change');
                    }
                    return {
                        results: data.data.map(item => {
                            return {
                                id: item.id,
                                text: item.module_name + " > " + item.feature_name + " > " + item.action_name
                            };
                        })
                    };
                }
            },
            placeholder: "No Permission"
        });
    })
</script>

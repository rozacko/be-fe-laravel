<x-layout>
    @push('css')
    <link href="{{ url('/assets/plugins/nestable/jquery.nestable.min.css') }}" rel="stylesheet" />
    <style type="text/css">
        .mjs-nestedSortable-error {
            background: #fbe3e4;
            border-color: transparent;
        }

        #tree {
            width: 550px;
            margin: 0;
        }

        ol {
            padding-left: 15px;
        }

        ol.sortable,
        ol.sortable ol {
            list-style-type: none;
        }

        .sortable li div:not(.btn-group) {
            border: 1px solid #d4d4d4;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            cursor: move;
            border-color: #D4D4D4 #D4D4D4 #BCBCBC;
            margin: 10px;
            padding: 10px;
        }

        li.mjs-nestedSortable-collapsed.mjs-nestedSortable-hovering div {
            border-color: #999;
        }

        .disclose,
        .expandEditor {
            cursor: pointer;
            width: 20px;
            display: none;
        }

        .sortable li.mjs-nestedSortable-collapsed>ol {
            display: none;
        }

        .sortable li.mjs-nestedSortable-branch>div>.disclose {
            display: inline-block;
        }

        .sortable span.ui-icon {
            display: inline-block;
            margin: 0;
            padding: 0;
        }

        .dd-item>button {
            margin: 10px 0;
        }
        .dd-handle {
            height: 40px;
            padding: 10px 10px;
        }

        .dd-action {
            padding: 3px 10px;
        }
    </style>
    @endpush

    @section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">List Menu</h4>
                    <div class="card-toolbar">
                        <div class="d-flex justify-content-end" data-kt-user-table-toolbar="base">
                            <button type="button" class="btn btn-primary btn-sm btn-add">
                                <i class="fa fa-plus"></i> Create
                            </button>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="list" class="dd">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-menu">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"></h4>
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        <i class="fa fa-times"></i>
                    </div>
                </div>
                <div class="modal-body">
                </div>
            </div>
        </div>
    </div>
    @endsection

    @push('scripts')
    <script src="{{ url('/assets/plugins/nestable/jquery.nestable.min.js') }}"></script>
    <script type="text/javascript">
        $(function() {
            render_list();

            function render_list() {
                $.ajax({
                        url: `/config/menu/list`,
                        method: "GET"
                    })
                    .done(function(res) {
                        $("#list").html(res);
                        $('.dd').nestable({
                            callback: function(l, e) {
                                // l is the main container
                                // e is the element that was moved
                                var sequence = $('.dd').nestable('serialize');
                                update_sequence(sequence);
                            }
                        });
                    });
            }

            function update_sequence(seq) {
                $.ajax({
                        url: `/config/menu/sequence`,
                        method: "POST",
                        data: {
                            _method: "PUT",
                            _token: "{{ csrf_token() }}",
                            sequence: seq
                        },
                    })
                    .done(function(response) {
                        var {
                            message
                        } = response;
                        Swal.fire({
                            icon: 'success',
                            title: message,
                            showConfirmButton: false,
                            timer: 1500
                        })
                    })
                    .fail(function(xhr) {
                        var {
                            message
                        } = xhr.responseJSON;
                        Swal.fire({
                            icon: 'error',
                            title: message,
                            showConfirmButton: false,
                            timer: 1500
                        })
                    });
            }

            $(document).on("click", ".btn-add", function() {
                    var id = $(this).data("id") || 0;
                    $("#modal-menu").find(".modal-title").text("Create Menu");
                    $.get("/config/menu/" + id + "/add", function(res) {
                        $("#modal-menu").find(".modal-body").html(res);
                        $("#modal-menu").modal('show');
                    })
                })
                .on("click", ".btn-edit", function() {
                    $("#modal-menu").find(".modal-title").text("Edit Menu");
                    $.get("/config/menu/" + $(this).data("uuid"), function(res) {
                        $("#modal-menu").find(".modal-body").html(res);
                        $("#modal-menu").modal('show');
                    })
                })
                .on("click", ".btn-delete", function() {
                    var uuid = $(this).data("uuid");
                    Swal.fire({
                        title: 'Are you sure?',
                        text: 'You will not be able to recover this data!',
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                    }).then((result) => {
                        if (result.isConfirmed) {
                            $.ajax({
                                    url: "{{url('/config/menu')}}/" + uuid,
                                    method: "POST",
                                    data: {
                                        _method: "DELETE",
                                        _token: "{{ csrf_token() }}",
                                    }
                                })
                                .done(function(response) {
                                    var {
                                        message
                                    } = response;
                                    Swal.fire({
                                        icon: 'success',
                                        title: message,
                                        showConfirmButton: false,
                                        timer: 1500
                                    })
                                    render_list();
                                })
                                .fail(function(xhr) {
                                    var {
                                        message
                                    } = xhr.responseJSON;
                                    Swal.fire({
                                        icon: 'error',
                                        title: message,
                                        showConfirmButton: false,
                                        timer: 1500
                                    })
                                })
                        }
                    })
                })


            $("#modal-menu").on("click", "#btn-update", function(e) {
                var frm = $(this).closest("form");
                var uuid = frm.find("[name=uuid]").val();
                var form = frm.serializeArray();
                form.push({
                    name: '_token',
                    value: "{{ csrf_token() }}"
                });
                form.push({
                    name: '_method',
                    value: "PUT"
                });
                $(".spinner-btn").show();
                $("#btn-update").attr('disabled', true);
                $(".is-invalid").removeClass("is-invalid");
                $(".invalid-feedback").remove();
                $.ajax({
                        url: `/config/menu/${uuid}/edit`,
                        method: "POST",
                        data: form,
                    })
                    .done(function(response) {
                        render_list();
                        $("#modal-menu").modal("hide");
                        var {
                            message
                        } = response;
                        Swal.fire({
                            icon: 'success',
                            title: message,
                            showConfirmButton: false,
                            timer: 1500
                        })
                    })
                    .fail(function(xhr) {
                        $(".spinner-btn").hide();
                        $("#btn-update").attr('disabled', false);
                        var {
                            errors,
                            message
                        } = xhr.responseJSON;
                        $.each(errors, function(i, v) {
                            var message = '<div class="invalid-feedback">' + v + '</div>';
                            $('[name="' + i + '"]').addClass("is-invalid");
                            $('[name="' + i + '"]').after(message);
                        });
                        Swal.fire({
                            icon: 'error',
                            title: message,
                            showConfirmButton: false,
                            timer: 1500
                        })
                    });
            })

            $("#modal-menu").on("click", "#btn-save", function(e) {
                var form = $("#form-create").serializeArray();
                form.push({
                    name: '_token',
                    value: "{{ csrf_token() }}"
                });
                $(".spinner-btn").show();
                $("#btn-save").attr('disabled', true);
                $(".is-invalid").removeClass("is-invalid");
                $(".invalid-feedback").remove();
                $.ajax({
                        url: `/config/menu/`,
                        method: "POST",
                        data: form,
                    })
                    .done(function(response) {
                        render_list();
                        $("#modal-menu").modal("hide");
                        var {
                            message
                        } = response;
                        Swal.fire({
                            icon: 'success',
                            title: message,
                            showConfirmButton: false,
                            timer: 1500
                        })
                    })
                    .fail(function(xhr) {
                        $(".spinner-btn").hide();
                        $("#btn-save").attr('disabled', false);
                        var {
                            errors,
                            message
                        } = xhr.responseJSON;
                        $.each(errors, function(i, v) {
                            var message = '<div class="invalid-feedback">' + v + '</div>';
                            $('[name="' + i + '"]').addClass("is-invalid");
                            $('[name="' + i + '"]').after(message);
                        });
                        Swal.fire({
                            icon: 'error',
                            title: message,
                            showConfirmButton: false,
                            timer: 1500
                        })
                    })
            })
        });
    </script>
    @endpush
</x-layout>
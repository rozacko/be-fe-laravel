<x-layout>
    @section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">List Log Activity</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="data-table-fixed-columns" class="table table-row-bordered gy-5">
                            <thead>
                                <tr>
                                    <th class="text-nowrap">ID</th>
                                    <th class="text-nowrap">Connection</th>
                                    <th class="text-nowrap">Queue</th>
                                    <th class="text-nowrap">Class</th>
                                    <th class="text-nowrap">Exception</th>
                                    <th class="text-nowrap">Failed At</th>
                                    <th class="text-nowrap no-sort">#</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection

    @push('scripts')

    <script type="text/javascript">
        var datatable;
        $(document).ready(function() {

            datatable = $('#data-table-fixed-columns').DataTable({
                "language": {
                    "lengthMenu": "Show _MENU_",
                },
                "dom": "<'row'" +
                    "<'col-sm-6 d-flex align-items-center justify-conten-start'l>" +
                    "<'col-sm-6 d-flex align-items-center justify-content-end'f>" +
                    ">" +

                    "<'table-responsive'tr>" +

                    "<'row'" +
                    "<'col-sm-12 col-md-5 d-flex align-items-center justify-content-center justify-content-md-start'i>" +
                    "<'col-sm-12 col-md-7 d-flex align-items-center justify-content-center justify-content-md-end'p>" +
                    ">",
                searchDelay: 500,
                serverSide: true,
                processing: true,
                ajax: "{{url('config/failed_job')}}",
                order: [
                    [0, "desc"]
                ],
                columnDefs: [{
                    "targets": "no-sort",
                    "orderable": false,
                    "searchable": false,
                }],
                columns: [{
                    data: 'id'
                }, {
                    data: 'connection'
                }, {
                    data: 'queue'
                }, {
                    data: 'class',
                    name: 'payload',
                }, {
                    data: 'short_exception',
                    name: 'exception',
                }, {
                    data: 'failed_at'
                }, {
                    data: null,
                    render: function(r, v, i) {
                        return `
                            <div class="btn-group">
                                <button type="button" id="${r.id}" onclick="retry('${r.id}')" class="btn btn-xs btn-info">
                                    <i class="fa fa-sync"></i>
                                </button>
                            </div>
                        `;
                    }
                }]
            });
        });

        function retry(id) {
            $("#" + id).find(".fa-sync").addClass("fa-spin");
            $.ajax({
                url: "{{url('/config/failed_job')}}/" + id,
                method: "GET"
            }).done(function(response) {
                var {
                    message
                } = response;
                Swal.fire({
                    icon: 'success',
                    title: message,
                    showConfirmButton: false,
                    timer: 1500
                })
                $("#" + id).find(".fa-sync").removeClass("fa-spin");
                datatable.ajax.reload();
            }).fail(function(xhr) {
                $("#" + id).find(".fa-sync").removeClass("fa-spin");
                var {
                    message
                } = xhr.responseJSON;
                $.gritter.add({
                    title: 'Error!',
                    text: message,
                    class_name: 'gritter-danger'
                });
            })
        }
    </script>
    @endpush

</x-layout>
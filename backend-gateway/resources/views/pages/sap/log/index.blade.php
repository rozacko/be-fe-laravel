<x-layout>
    @push('css')
    <style>
        .capitalize {
            text-transform: capitalize;
        }
    </style>
    @endpush
    @section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">List SAP Sync Log</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="data-table-fixed-columns" class="table table-row-bordered gy-5">
                            <thead>
                                <tr>
                                    <th class="text-nowrap">RFC</th>
                                    <th class="text-nowrap">Year</th>
                                    <th class="text-nowrap">Month</th>
                                    <th class="text-nowrap">Sync Time</th>
                                    <th class="text-nowrap">Status</th>
                                    <th class="text-nowrap">Note</th>
                                    <th class="text-nowrap">Action</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end row -->
    @endsection

    @push('scripts')

    <script type="text/javascript">
        var datatable;
        $(document).ready(function() {
            $(document).on('click', '.resync-btn', function() {
                let id = $(this).data('id');
                let button = $(this);
                $.ajax({
                    type: 'GET',
                    url: `{{url('config/sap/resync')}}/${id}`,
                    beforeSend: function() {
                        button.attr('disabled', true).html(
                            "<i class='fa fa-spinner fa-spin'></i>");
                    }
                }).done(function(data) {
                    Swal.fire({
                        icon: 'success',
                        title: data.message,
                        showConfirmButton: false,
                        timer: 1500
                    })
                }).fail(function(data) {
                    Swal.fire({
                        icon: 'error',
                        title: data.responseJSON.message,
                        showConfirmButton: false,
                        timer: 1500
                    })
                }).always(function() {
                    datatable.ajax.reload();
                    button.attr('disabled', false).html("Resync");
                });
            })

            datatable = $('#data-table-fixed-columns').DataTable({
                "language": {
                    "lengthMenu": "Show _MENU_",
                },
                "dom": "<'row'" +
                    "<'col-sm-6 d-flex align-items-center justify-conten-start'l>" +
                    "<'col-sm-6 d-flex align-items-center justify-content-end'f>" +
                    ">" +

                    "<'table-responsive'tr>" +

                    "<'row'" +
                    "<'col-sm-12 col-md-5 d-flex align-items-center justify-content-center justify-content-md-start'i>" +
                    "<'col-sm-12 col-md-7 d-flex align-items-center justify-content-center justify-content-md-end'p>" +
                    ">",
                searchDelay: 500,
                serverSide: true,
                processing: true,
                ajax: "{{url('config/sap/log')}}",
                columns: [{
                    data: 'config_name',
                    name: 'config_name'
                }, {
                    data: 'year',
                    name: 'year'
                }, {
                    data: 'month',
                    name: 'month',
                    render: function(data, type, full, meta) {
                        return moment().month(data - 1).format('MMMM');
                    }
                }, {
                    data: 'created_at',
                    name: 'created_at',
                    render: function(data, type, full, meta) {
                        return moment(data).format('DD-MMM-YYYY HH:mm:ss');
                    }
                }, {
                    data: 'status',
                    name: 'status',
                    render: function(data, type, full, meta) {
                        var color_text = "";
                        if (data == 'fail') {
                            color_text = 'badge-danger';
                        } else if (data == 'success') {
                            color_text = 'badge-success';
                        } else if (data == 'process') {
                            color_text = 'badge-primary';
                        }

                        return '<span class="capitalize badge ' + color_text + '">' + data.replace('_', ' ') + '</span>';
                    }
                }, {
                    data: 'note',
                    name: 'note'
                }, {
                    data: 'id',
                    name: 'id',
                    orderable: false,
                    render: function(data, type, full, meta) {
                        return '<button class="btn btn-sm btn-info resync-btn" data-id="' + data + '">Resync</button>';
                    }

                }],
                order: [
                    [3, 'desc']
                ]
            });
        });
    </script>
    @endpush

</x-layout>
<x-layout>
    @push('css')
    <style>
        .capitalize {
            text-transform: capitalize;
        }
    </style>
    @endpush
    @section('content')
    <div class="row mb-5">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">List SAP Sync Config</h4>
                    <div class="card-toolbar">
                        <div class="d-flex justify-content-end" data-kt-user-table-toolbar="base">
                            <button id="create-config-manual" name="create-config-manual" class="btn btn-info font-weight-bold mt-2 mb-2">
                                <i class="fa fa-recycle"></i> Sync Manual
                            </button>&nbsp;&nbsp;
                            <button id="create-config" name="create-config" class="btn btn-primary font-weight-bold mt-2 mb-2">
                                <i class="fa fa-plus"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="data-table-fixed-columns" class="table table-row-bordered gy-5">
                            <thead>
                                <tr>
                                    <th class="text-nowrap">#</th>
                                    <th class="text-nowrap">Config Name</th>
                                    <th class="text-nowrap">Schedule Time</th>
                                    <th class="text-nowrap">Status</th>
                                    <th class="text-nowrap">Action</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-config" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-config-title"></h5>
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        <i class="fa fa-times"></i>
                    </div>
                </div>
                <div class="modal-body">
                    <form role="form" class="form" name="formmenus" id="form-config" enctype="multipart/formdata" method="">
                        <div class="modal-body">
                            <div class="form-group row mb-5">
                                <label class="col-lg-3 col-form-label">SAP RFC Name<span class="text-danger">*</span></label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" id="name" name="name" />
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <label class="col-lg-3 col-form-label">RFC Type<span class="text-danger">*</span></label>
                                <div class="col-lg-9">
                                    <select class="form-control select2" name="type" id="type" style="width: 100%;">
                                        <option class="form-control" value="">Select API Type</option>
                                        @foreach ($types as $key => $type)
                                        <option class="form-control" value="{{ $key }}">{{ $type }}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <label class="col-lg-3 col-form-label">Nama RFC<span class="text-danger">*</span></label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" id="tcode" name="tcode" />
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <label class="col-lg-3 col-form-label">Parameter<span class="text-danger">*</span></label>
                                <div class="col-lg-9">
                                    <textarea class="form-control" id="parameter" name="parameter" rows="10"></textarea>
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <label class="col-lg-3 col-form-label">Schedule<span class="text-danger">*</span></label>
                                <div class="col-lg-9">
                                    <select class="form-control select2" name="schedule" id="schedule" style="width: 100%;">
                                        <option class="form-control" value="">Select Schedule</option>
                                        @foreach ($schedules as $key => $schedule)
                                        <option class="form-control" value="{{ $key }}">{{ $schedule }}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <label class="col-lg-3 col-form-label">At Date</label>
                                <div class="col-lg-9">
                                    <input type="number" class="form-control" id="at_date" name="at_date" />
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <label class="col-lg-3 col-form-label">At Time</label>
                                <div class="col-lg-9">
                                    <input type="time" class="form-control" id="at_time" name="at_time" />
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <label class="col-lg-3 col-form-label">Status<span class="text-danger">*</span></label>
                                <div class="col-lg-9">
                                    <select class="form-control select2" name="status" id="status" style="width: 100%;">
                                        <option class="form-control" value=''>Select Status</option>
                                        @foreach ($statuses as $key => $statuse)
                                        <option class="form-control" value="{{ $key }}">{{ $statuse }}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="test-btn" class="btn btn-info btn-sm font-weight-bold float-left"><i class="fa fa-recycle"></i> Test Sync</button>
                    <button type="button" class="btn btn-sm btn-light-danger font-weight-bold" data-bs-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>
                    <button type="submit" id="submit-btn" data-id="" class="btn btn-sm btn-success font-weight-bold"><i class="fa fa-save"></i> Save</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-config-manual" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-config-manual-title">Manual Synchronize</h5>
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        <i class="fa fa-times"></i>
                    </div>
                </div>
                <div class="modal-body">
                    <form role="form" class="form" name="formmenus" id="form-config-manual" enctype="multipart/formdata" method="">
                        <div class="modal-body">
                            <div class="form-group row mb-5">
                                <label class="col-lg-3 col-form-label">Config SAP<span class="text-danger">*</span></label>
                                <div class="col-lg-9">
                                    <select class="form-control select2" name="id" id="id" style="width: 100%;">
                                        <option class="form-control" value="">Select Config SAP</option>
                                        @foreach ($configs as $key => $config)
                                        <option class="form-control" value="{{ $config->id }}">{{ $config->name }}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <label class="col-lg-3 col-form-label">Tanggal Awal<span class="text-danger">*</span></label>
                                <div class="col-lg-9">
                                    <input type="date" class="form-control" id="start_date" name="start_date" />
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <label class="col-lg-3 col-form-label">Tanggal Akhir<span class="text-danger">*</span></label>
                                <div class="col-lg-9">
                                    <input type="date" class="form-control" id="end_date" name="end_date" />
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="test-btn-sync-manual" class="btn btn-info btn-sm font-weight-bold float-left"><i class="fa fa-recycle"></i> Test Sync</button>
                </div>
            </div>
        </div>
    </div>

    @endsection

    @push('scripts')

    <script type="text/javascript">
        var datatable;
        $(document).ready(function() {
            datatable = $('#data-table-fixed-columns').DataTable({
                "language": {
                    "lengthMenu": "Show _MENU_",
                },
                "dom": "<'row'" +
                    "<'col-sm-6 d-flex align-items-center justify-conten-start'l>" +
                    "<'col-sm-6 d-flex align-items-center justify-content-end'f>" +
                    ">" +

                    "<'table-responsive'tr>" +

                    "<'row'" +
                    "<'col-sm-12 col-md-5 d-flex align-items-center justify-content-center justify-content-md-start'i>" +
                    "<'col-sm-12 col-md-7 d-flex align-items-center justify-content-center justify-content-md-end'p>" +
                    ">",
                searchDelay: 500,
                serverSide: true,
                processing: true,
                ajax: "{{url('config/sap/config')}}",
                columns: [{
                    data: 'id',
                    name: 'id'
                }, {
                    data: 'name',
                    name: 'name'
                }, {
                    data: "schedule",
                    name: "schedule",
                    className: "capitalize"
                }, {
                    data: 'status',
                    name: 'status',
                    render: function(data, type, full, meta) {
                        var color_text = "";
                        if (data == 'non_active') {
                            color_text = 'badge-danger';
                        } else if (data == 'active') {
                            color_text = 'badge-success';
                        }

                        return '<span class="capitalize badge ' + color_text + '">' + data.replace('_', ' ') + '</span>';
                    }
                }, {
                    data: 'id',
                    name: 'id',
                    orderable: false,
                    render: function(data, type, full, meta) {
                        return "<center>" +
                            "<button type='button' class='edit-btn btn btn-sm btn-warning ' title='Edit' data-toggle='tooltip' data-id=" +
                            data + " ><i class='fa fa-edit'></i> </button>  " +
                            "<button type='button' class='delete-btn btn btn-sm btn-danger' title='Delete' data-toggle='tooltip' alt='' data-id=" +
                            data + " ><i class='fa fa-trash'></i></button>  " +
                            "</center>";
                    }
                }]
            });

            $(document).on('click', '#create-config', function() {
                $('#submit-btn').data("id", "");
                $('#modal-config-title').text('Create Sync SAP Config');
                $('#modal-config').modal('show');
                clearForm();
            });

            $(document).on('click', '#create-config-manual', function() {
                $('#modal-config-manual').modal('show');
            });

            $(document).on('click', '#submit-btn', function() {
                var formData = new FormData($("#form-config")[0]);
                var method = "POST";
                let id = $("#submit-btn").data("id");

                if (typeof id == "undefined" || id == "") {
                    var url = `{{url('config/sap/config')}}`;
                } else {
                    var url = `{{url('config/sap/config')}}/${id}`;
                    formData.append("_method", "PUT");
                }

                $.ajax({
                    type: method,
                    url: url,
                    data: formData,
                    dataType: 'JSON',
                    contentType: false,
                    processData: false,
                    beforeSend: function() {
                        $(`.form-control`).removeClass('is-invalid');
                        $(`.invalid-feedback`).remove();
                        $('#submit-btn').attr('disabled', true).html(
                            "<i class='fa fa-spinner fa-spin'></i> processing");
                    }
                }).done(function(data) {
                    $("#modal-config").modal('hide');
                    Swal.fire({
                        icon: 'success',
                        title: data.message,
                        showConfirmButton: false,
                        timer: 3000
                    })
                    $("#submit-btn").data("id", "");
                    clearForm();
                    datatable.ajax.reload();
                }).fail(function(data) {
                    Swal.fire({
                        icon: 'error',
                        title: data.responseJSON.message,
                        showConfirmButton: false,
                        timer: 3000
                    })
                    $.each(data.responseJSON.errors, function(index, value) {
                        if ($(`input[name='${index}']`)) {
                            $(`input[name='${index}']`).addClass(`is-invalid`);
                            $(`input[name='${index}']`).after(
                                `<div class="invalid-feedback">${value}</div>`);
                        }
                    });
                }).always(function() {
                    $('#submit-btn').attr('disabled', false).html(
                        "<i class='fa fa-save'></i> Save");
                });
            });

            $(document).on('click', '#test-btn', function() {
                var formData = new FormData($("#form-config")[0]);
                var method = "POST";
                var url = `{{url('config/sap/config')}}/test`;

                $.ajax({
                    type: method,
                    url: url,
                    data: formData,
                    dataType: 'JSON',
                    contentType: false,
                    processData: false,
                    beforeSend: function() {
                        $(`.form-control`).removeClass('is-invalid');
                        $(`.invalid-feedback`).remove();
                        $('#test-btn').attr('disabled', true).html(
                            "<i class='fa fa-spinner fa-spin'></i> processing");
                    }
                }).done(function(data) {
                    Swal.fire({
                        icon: 'success',
                        title: data.message,
                        showConfirmButton: false,
                        timer: 3000
                    });
                }).fail(function(data) {
                    Swal.fire({
                        icon: 'error',
                        title: data.responseJSON.message,
                        showConfirmButton: false,
                        timer: 3000
                    })
                }).always(function() {
                    $('#test-btn').attr('disabled', false).html(
                        "<i class='fa fa-recycle'></i> Test Sync");
                });
            });

            $(document).on('click', '.edit-btn', function() {
                $.ajax({
                    type: 'GET',
                    url: "{{url('config/sap/config')}}/" + $(this).data('id'),
                    beforeSend: function() {
                        clearForm();
                    }
                }).done(function(res) {
                    if (res.status == 'success') {
                        let data = res.data;
                        $('#name').val(data.name);
                        $('#type').val(data.type).trigger('change');
                        $('#kode_opco').val(data.kode_opco).trigger('change');
                        $('#tcode').val(data.tcode);
                        $('#parameter').val(data.parameter);
                        $('#schedule').val(data.schedule).trigger('change');
                        $('#at_date').val(data.at_date);
                        $('#at_time').val(data.at_time);
                        $('#status').val(data.status).trigger('change');
                        $("#submit-btn").data("id", data.id);
                    }
                }).fail(function(data) {
                    Swal.fire({
                        icon: 'error',
                        title: data.responseJSON.message,
                        showConfirmButton: false,
                        timer: 3000
                    })
                    $.each(data.responseJSON.errors, function(index, value) {
                        Swal.fire({
                            icon: 'error',
                            title: value,
                            showConfirmButton: false,
                            timer: 3000
                        })
                    });
                }).always(function() {
                    $('#modal-config-title').text('Edit Sync SAP Config');
                    $('#modal-config').modal('show');
                });
            });

            $(document).on('click', '.delete-btn', function() {
                Swal.fire({
                        title: 'Are you sure?',
                        text: 'You will not be able to recover this data!',
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                    })
                    .then(isConfirm => {
                        if (isConfirm.isConfirmed) {
                            $.ajax({
                                    type: 'DELETE',
                                    data: {
                                        "_token": "{{csrf_token()}}"
                                    },
                                    url: "{{url('config/sap/config')}}/" + $(this).data('id'),
                                })
                                .done(function(data) {
                                    Swal.fire({
                                        icon: 'success',
                                        title: data.message,
                                        showConfirmButton: false,
                                        timer: 3000
                                    })
                                })
                                .fail(function(data) {
                                    Swal.fire({
                                        icon: 'error',
                                        title: data.responseJSON.message,
                                        showConfirmButton: false,
                                        timer: 3000
                                    })
                                    $.each(data.responseJSON.messages, function(index, value) {
                                        Swal.fire({
                                            icon: 'error',
                                            title: value,
                                            showConfirmButton: false,
                                            timer: 3000
                                        })
                                    });
                                })
                                .always(function() {
                                    datatable.ajax.reload();
                                });
                        }
                    });
            });

            $(document).on('change', '#schedule', function() {
                $('#at_date').val('');
                $('#at_time').val('');
            });

            $(document).on('click', '#test-btn-sync-manual', function() {
                var formData = new FormData($("#form-config-manual")[0]);
                var method = "POST";
                var url = `{{url('config/sap/config')}}/sync-manual`;

                $.ajax({
                    type: method,
                    url: url,
                    data: formData,
                    dataType: 'JSON',
                    contentType: false,
                    processData: false,
                    beforeSend: function() {
                        $(`.form-control`).removeClass('is-invalid');
                        $(`.invalid-feedback`).remove();
                        $('#test-btn-sync-manual').attr('disabled', true).html(
                            "<i class='fa fa-spinner fa-spin'></i> processing");
                    }
                }).done(function(data) {
                    Swal.fire({
                        icon: 'success',
                        title: data.message,
                        showConfirmButton: false,
                        timer: 3000
                    });
                }).fail(function(data) {
                    Swal.fire({
                        icon: 'error',
                        title: data.responseJSON.message,
                        showConfirmButton: false,
                        timer: 3000
                    })
                }).always(function() {
                    $('#test-btn-sync-manual').attr('disabled', false).html(
                        "<i class='fa fa-rotate'></i> Test Sync");
                });
            });

        });

        function clearForm() {
            $(".form-control").removeClass('is-invalid');
            $(".invalid-feedback").remove();
            $("#type").val('').trigger('change');
            $("#schedule").val('').trigger('change');
            $("#status").val('').trigger('change');
            $('#form-config').trigger("reset");
            $('#test-btn').attr('disabled', false).html("<i class='fa fa-recycle'></i> Test Sync");
        }
    </script>
    @endpush

</x-layout>
<x-layout>
    @section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">List Route</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="data-table-fixed-columns" class="table table-row-bordered gy-5">
                            <thead>
                                <tr>
                                    <th class="text-nowrap">Name</th>
                                    <th class="text-nowrap">Method(s)</th>
                                    <th class="text-nowrap">URL</th>
                                    <th class="text-nowrap">Path</th>
                                    <th class="text-nowrap no-sort">Middleware</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
    @push('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#data-table-fixed-columns').DataTable({
                "language": {
                    "lengthMenu": "Show _MENU_",
                },
                "dom": "<'row'" +
                    "<'col-sm-6 d-flex align-items-center justify-conten-start'l>" +
                    "<'col-sm-6 d-flex align-items-center justify-content-end'f>" +
                    ">" +

                    "<'table-responsive'tr>" +

                    "<'row'" +
                    "<'col-sm-12 col-md-5 d-flex align-items-center justify-content-center justify-content-md-start'i>" +
                    "<'col-sm-12 col-md-7 d-flex align-items-center justify-content-center justify-content-md-end'p>" +
                    ">",
                searchDelay: 500,
                serverSide: true,
                processing: true,
                ajax: "{{url('config/route')}}",
                columns: [{
                    data: 'name',
                    name: 'name'
                }, {
                    data: 'http_method',
                    name: 'http_method'
                }, {
                    data: 'url',
                    name: 'url'
                }, {
                    data: 'path',
                    name: 'path'
                }, {
                    data: null,
                    orderable: false,
                    render: function(data, type, row) {
                        var middleware = '';
                        if (data.middleware != null && data.middleware != "") {
                            middleware = `<span class="badge badge-success">${ data.middleware }</span>&nbsp;`;
                        }
                        $.each(data.permissions, function(key, val) {
                            middleware += `<span class="badge badge-success">permissions:${ val.name }</span>&nbsp;`;
                        })
                        return middleware;
                    }
                }]
            });
        });
    </script>

    @endpush
</x-layout>
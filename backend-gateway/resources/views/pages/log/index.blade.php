<x-layout>
    @section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">List Log Activity</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="data-table-fixed-columns" class="table table-row-bordered gy-5">
                            <thead>
                                <tr>
                                    <th class="text-nowrap">Name</th>
                                    <th class="text-nowrap">URL</th>
                                    <th class="text-nowrap">Method(s)</th>
                                    <th class="text-nowrap">IP</th>
                                    <th class="text-nowrap">Agent</th>
                                    <th class="text-nowrap">User ID</th>
                                    <th class="text-nowrap">User Name</th>
                                    <th class="text-nowrap">HTTP Code</th>
                                    <th class="text-nowrap">Created Date</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end row -->
    @endsection

    @push('scripts')

    <script type="text/javascript">
        $(document).ready(function() {

            $('#data-table-fixed-columns').DataTable({
                "language": {
                    "lengthMenu": "Show _MENU_",
                },
                "dom": "<'row'" +
                    "<'col-sm-6 d-flex align-items-center justify-conten-start'l>" +
                    "<'col-sm-6 d-flex align-items-center justify-content-end'f>" +
                    ">" +

                    "<'table-responsive'tr>" +

                    "<'row'" +
                    "<'col-sm-12 col-md-5 d-flex align-items-center justify-content-center justify-content-md-start'i>" +
                    "<'col-sm-12 col-md-7 d-flex align-items-center justify-content-center justify-content-md-end'p>" +
                    ">",
                searchDelay: 500,
                serverSide: true,
                processing: true,
                ajax: "{{url('config/log_activity')}}",
                columns: [{
                    data: 'name',
                    name: 'name'
                }, {
                    data: 'url',
                    name: 'url'
                }, {
                    data: 'method',
                    name: 'method'
                }, {
                    data: 'ip',
                    name: 'ip'
                }, {
                    data: 'agent',
                    name: 'agent'
                }, {
                    data: 'user_id',
                    name: 'user_id'
                }, {
                    data: 'user_name',
                    name: 'users.name'
                }, {
                    data: 'http_code',
                    name: 'http_code'
                }, {
                    data: 'created_at',
                    name: 'created_at'
                }, ]
            });
        });
    </script>
    @endpush

</x-layout>
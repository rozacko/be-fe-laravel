<x-layout>
    @section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Email Template</h4>
                </div>
                <div class="card-body">
                    <form action="#" id="form-create" method="POST">
                        <div class="row mb-5">
                            <div class="col-xs-12 form-group">
                                <label for="name">ID <span class="text-danger">*</span></label>
                                <input type="text" name="id" class="form-control" value="{{ $id }}" disabled />
                            </div>
                        </div>
                        <div class="row mb-5">
                            <div class="col-xs-12 form-group">
                                <label for="name">Subject <span class="text-danger">*</span></label>
                                <input type="text" name="subject" class="form-control" value="{{ $subject }}" disabled />
                            </div>
                        </div>
                        <div class="row mb-5">
                            <div class="col-xs-12 form-group">
                                <label for="url">Template <span class="text-danger">*</span></label>
                                <textarea id="template" name="template" rows="30" disabled>{{ $template }}</textarea>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @endsection
    @push('scripts')
    <script src="{{url('assets/plugins/tinymce/tinymce.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            tinymce.init({
                selector: '#template'
            });
            tinymce.activeEditor.mode.set("readonly");
        })
    </script>
    @endpush
</x-layout>
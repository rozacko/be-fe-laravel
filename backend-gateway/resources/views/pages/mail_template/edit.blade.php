<x-layout>
    @section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Edit Email Template</h4>
                </div>
                <div class="card-body">
                    <form action="{{url('/config/mail_template').'/'.$uuid}}" id="form-create" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="_method" value="PUT" />
                        <div class="row mb-5">
                            <div class="col-xs-12 form-group">
                                <label for="name">ID <span class="text-danger">*</span></label>
                                <input type="text" name="id" class="form-control" value="{{ $id }}" disabled />
                            </div>
                        </div>
                        <div class="row mb-5">
                            <div class="col-xs-12 form-group">
                                <label for="name">Subject <span class="text-danger">*</span></label>
                                <input type="text" name="subject" class="form-control" value="{{ $subject }}" required />
                            </div>
                        </div>
                        <div class="row mb-5">
                            <div class="col-xs-12 form-group">
                                <label for="url">Template <span class="text-danger">*</span></label>
                                <textarea id="template" name="template" rows="30">{{ $template }}</textarea>
                            </div>
                        </div>
                        <div class="row mb-5">
                            <div class="col-xs-12 form-group">
                                <button type="submit" id="btn-store" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
    @endsection
    @push('scripts')
    <script src="{{url('assets/plugins/tinymce/tinymce.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            tinymce.init({
                selector: '#template'
            });
        })
    </script>
    @endpush
</x-layout>
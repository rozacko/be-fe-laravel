<x-layout>
    @section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Edit Shipment API</h4>
                </div>
                <div class="card-body">
                    <form action="{{url('/config/shipmentapi').'/'.$uuid}}" id="form-create" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="_method" value="PUT" />

                        <div class="row mb-5">
                            <div class="col-xs-12 form-group">
                                <label for="api_name">API Name<span class="text-danger">*</span></label>
                                <input type="text" name="api_name" id="api_name" value="{{$api_name}}" style="width: 100%;" readonly/>
                            </div>
                        </div>

                        <div class="row mb-5">
                            <div class="col-xs-12 form-group">
                                <label for="api_url">API URL<span class="text-danger">*</span></label>
                                <input type="text" name="api_url" id="api_url"  value="{{$api_url}}" style="width: 100%;"/>
                            </div>
                        </div>

                        <div class="row mb-5">
                            <div class="col-xs-12 form-group">
                                <label for="api_method">API METHOD<span class="text-danger">*</span></label>
                                <input type="text" name="api_method" id="api_method"  value="{{$api_method}}" style="width: 100%;"/>
                            </div>
                        </div>

                        <div class="row mb-5">
                            <div class="col-xs-12 form-group">
                                <label for="url">API Parameter <span class="text-danger">*</span></label>
                            </div>
                        </div>

                        <div class="table-responsive">
                            <table id="table_param">
                                <thead>
                                    <tr>
                                        <th>
                                            Key
                                        </th>
                                        <th>
                                            Value
                                        </th>
                                        <th>
                                            <button type="button" onclick="addRow()" class="btn btn-sm btn-success">Add Params</button>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach(json_decode($api_params, true) as $key=>$value)
                                    <tr>
                                        <td>
                                            <input type="text" name="_params[key][]" id="_params[key][]" value="{{$key}}" style="width: 100%;"/>
                                        </td>
                                        <td>
                                            <input type="text" name="_params[value][]" id="_params[value][]" value="{{$value}}" style="width: 100%;"/>
                                        </td>
                                        <td>
                                            <button type="button" onclick="delRow(this)" class="btn btn-sm btn-danger">Delete</button>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="row mb-5">
                            <div class="col-xs-12 form-group">
                                <button type="submit" id="btn-store" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
    @endsection
    @push('scripts')
    <script>
        var rowNum = {{sizeof(json_decode($api_params, true))}};
        function addRow(){
            rowNum += 1;
            const html = `<tr>
                            <td>
                                <input type="text" name="_params[key][]" id="_params[key][]" style="width: 100%;"/>
                            </td>
                            <td>
                                <input type="text" name="_params[value][]" id="_params[value][]" style="width: 100%;"/>
                            </td>
                            <td>
                                <button type="button" onclick="delRow(this)" class="btn btn-sm btn-danger">Delete</button>
                            </td>
                        </tr>`;
            $("#table_param").find('tbody').append(html);
        }

        function delRow(elm){
            elm.closest("tr").remove();
        }
    </script>
    @endpush
</x-layout>
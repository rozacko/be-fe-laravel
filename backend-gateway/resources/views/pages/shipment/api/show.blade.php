<x-layout>
    @section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Data Shipment API</h4>
                </div>
                <div class="card-body">
                    <form id="form-create" method="POST">
                        <div class="row mb-5">
                            <div class="col-xs-12 form-group">
                                <label for="api_name">API Name<span class="text-danger">*</span></label>
                                <input type="text" name="api_name" id="api_name" value="{{$api_name}}" style="width: 100%;" disabled/>
                            </div>
                        </div>

                        <div class="row mb-5">
                            <div class="col-xs-12 form-group">
                                <label for="api_url">API URL<span class="text-danger">*</span></label>
                                <input type="text" name="api_url" id="api_url"  value="{{$api_url}}" style="width: 100%;" disabled/>
                            </div>
                        </div>

                        <div class="row mb-5">
                            <div class="col-xs-12 form-group">
                                <label for="api_method">API METHOD<span class="text-danger">*</span></label>
                                <input type="text" name="api_method" id="api_method"  value="{{$api_method}}" style="width: 100%;" disabled/>
                            </div>
                        </div>

                        <div class="row mb-5">
                            <div class="col-xs-12 form-group">
                                <label for="url">API Parameter <span class="text-danger">*</span></label>
                            </div>
                        </div>

                        <div class="table-responsive">
                            <table id="table_param">
                                <thead>
                                    <tr>
                                        <th>
                                            Key
                                        </th>
                                        <th>
                                            Value
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach(json_decode($api_params, true) as $key=>$value)
                                    <tr>
                                        <td>
                                            <input type="text" name="_params[key][]" id="_params[key][]" value="{{$key}}" style="width: 100%;"/>
                                        </td>
                                        <td>
                                            <input type="text" name="_params[value][]" id="_params[value][]" value="{{$value}}" style="width: 100%;"/>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
    @endsection
    @push('scripts')
    @endpush
</x-layout>
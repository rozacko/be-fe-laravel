<x-layout>
    @section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Data Jenis Shipment</h4>
                </div>
                <div class="card-body">
                <form>

                        <div class="row mb-5">
                            <div class="col-xs-12 form-group">
                                <label for="id_plant">Plant</span></label>
                                <select class="form-control select2" name="id_plant" id="id_plant" style="width: 100%;" disabled>
                                    <option class="form-control" value="">Select Plant</option>
                                    @foreach ($plant as $key => $item)
                                        <option class="form-control" value="{{ $item['id'] }}" {{($item['id']==$data->id_plant ? 'selected':'')}} >{{ $item['nm_plant'] }} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row mb-5">
                            <div class="col-xs-12 form-group">
                                <label for="id_api_shipment">API <span class="text-danger">*</span></label>
                                <select class="form-control select2" name="id_api_shipment" id="id_api_shipment" style="width: 100%;" disabled>
                                    <option class="form-control" value="">Select API</option>
                                    @foreach ($api as $key => $item)
                                        <option class="form-control" value="{{ $item['id'] }}" {{($item['id']==$data->id_api_shipment ? 'selected':'')}} >{{ $item['api_name'] }} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row mb-5">
                            <div class="col-xs-12 form-group">
                                <label for="jenis_shipment">Jenis Shipment<span class="text-danger">*</span></label>
                                <input type="text" name="jenis_shipment" id="jenis_shipment" value="{{$data->jenis_shipment}}" style="width: 100%;" readonly/>
                            </div>
                        </div>

                        <div class="row mb-5">
                            <div class="col-xs-12 form-group">
                                <label for="sync_period">Shipment Parameters<span class="text-danger">*</span></label>                              
                            </div>
                        </div>

                        <div class="table-responsive">
                            <table id="table_param">
                                <thead>
                                    <tr>
                                        <th>
                                            Attributes
                                        </th>
                                        <th>
                                            Queries
                                        </th>
                                        <th>
                                            Value
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach(json_decode($data->shipment_params, true) as $key=>$value)
                                    <tr>
                                        <td>
                                            <input type="text" name="shipment_params[0][attr]" id="shipment_params[0][attr]" value="{{$value['attr']}}" style="width: 100%;" readonly/>
                                        </td>
                                        <td>
                                            <select class="form-control select2" name="shipment_params[0][condition]" id="shipment_params[0][condition]" style="width: 100%;" disabled>
                                                <option class="form-control" value="contain" {{($value['condition'] == 'contain' ? 'selected' : '')}} >Contains</option>
                                                <option class="form-control" value="not" {{($value['condition'] == 'not' ? 'selected' : '')}} >Do Not Contains</option>
                                            </select>
                                        </td>
                                        <td>
                                            <input type="text" name="shipment_params[0][value]" id="shipment_params[0][value]" value="{{$value['value']}}" style="width: 100%;" readonly/>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
    @endsection
    @push('scripts')
    <script>
    </script>
    @endpush
</x-layout>
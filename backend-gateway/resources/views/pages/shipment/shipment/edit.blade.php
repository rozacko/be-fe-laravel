<x-layout>
    @section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Edit Jenis Shipment</h4>
                </div>
                <div class="card-body">
                <form action="{{url('/config/opc').'/'.$data->uuid}}" id="form-create" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="_method" value="PUT" />

                        <div class="row mb-5">
                            <div class="col-xs-12 form-group">
                                <label for="id_plant">Plant</span></label>
                                <select class="form-control select2" name="id_plant" id="id_plant" style="width: 100%;">
                                    <option class="form-control" value="">Select Plant</option>
                                    @foreach ($plant as $key => $item)
                                        <option class="form-control" value="{{ $item['id'] }}" {{($item['id']==$data->id_plant ? 'selected':'')}} >{{ $item['nm_plant'] }} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row mb-5">
                            <div class="col-xs-12 form-group">
                                <label for="id_api_shipment">API <span class="text-danger">*</span></label>
                                <select class="form-control select2" name="id_api_shipment" id="id_api_shipment" style="width: 100%;">
                                    <option class="form-control" value="">Select API</option>
                                    @foreach ($api as $key => $item)
                                        <option class="form-control" value="{{ $item['id'] }}" {{($item['id']==$data->id_api_shipment ? 'selected':'')}} >{{ $item['api_name'] }} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row mb-5">
                            <div class="col-xs-12 form-group">
                                <label for="jenis_shipment">Jenis Shipment<span class="text-danger">*</span></label>
                                <input type="text" name="jenis_shipment" id="jenis_shipment" value="{{$data->jenis_shipment}}" style="width: 100%;"/>
                            </div>
                        </div>

                        <div class="row mb-5">
                            <div class="col-xs-12 form-group">
                                <label for="sync_period">Shipment Parameters<span class="text-danger">*</span></label>                              
                            </div>
                        </div>

                        <div class="table-responsive">
                            <table id="table_param">
                                <thead>
                                    <tr>
                                        <th>
                                            Attributes
                                        </th>
                                        <th>
                                            Queries
                                        </th>
                                        <th>
                                            Value
                                        </th>
                                        <th>
                                            <button type="button" onclick="addRow()" class="btn btn-sm btn-success">Add Params</button>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach(json_decode($data->shipment_params, true) as $key=>$value)
                                    <tr>
                                        <td>
                                            <input type="text" name="shipment_params[$key][attr]" id="shipment_params[$key][attr]" value="{{$value['attr']}}" style="width: 100%;"/>
                                        </td>
                                        <td>
                                            <select class="form-control select2" name="shipment_params[$key][condition]" id="shipment_params[$key][condition]" style="width: 100%;">
                                                <option class="form-control" value="contain" {{($value['condition'] == 'contain' ? 'selected' : '')}} >Contains</option>
                                                <option class="form-control" value="not" {{($value['condition'] == 'not' ? 'selected' : '')}} >Do Not Contains</option>
                                            </select>
                                        </td>
                                        <td>
                                            <input type="text" name="shipment_params[$key][value]" id="shipment_params[$key][value]" value="{{$value['value']}}" style="width: 100%;"/>
                                        </td>
                                        <td>
                                            <button type="button" onclick="delRow(this)" class="btn btn-sm btn-danger">Delete</button>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="row mb-5">
                            <div class="col-xs-12 form-group">
                                <button type="submit" id="btn-store" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
    @endsection
    @push('scripts')
    <script>
        var rowNum = {{sizeof(json_decode($data->shipment_params, true))}};
        function addRow(){
            rowNum += 1;
            const html = `<tr>
                            <td>
                                <input type="text" name="shipment_params[`+rowNum+`][attr]" id="shipment_params[`+rowNum+`][attr]" style="width: 100%;"/>
                            </td>
                            <td>
                                <select class="form-control select2" name="shipment_params[`+rowNum+`][condition]" id="shipment_params[`+rowNum+`][condition]" style="width: 100%;">
                                    <option class="form-control" value="contain" selected>Contains</option>
                                    <option class="form-control" value="not">Do Not Contains</option>
                                </select>
                            </td>
                            <td>
                                <input type="text" name="shipment_params[`+rowNum+`][value]" id="shipment_params[`+rowNum+`][value]" style="width: 100%;"/>
                            </td>
                            <td>
                                <button type="button" onclick="delRow(this)" class="btn btn-sm btn-danger">Delete</button>
                            </td>
                        </tr>`;
            $("#table_param").find('tbody').append(html);
        }

        function delRow(elm){
            elm.closest("tr").remove();
        }
    </script>
    @endpush
</x-layout>
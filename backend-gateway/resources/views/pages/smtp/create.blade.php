<form action="#" id="form-create">
    <div class="row mb-5">
        <div class="col-xs-12 form-group">
            <label class="mb-2" for="name">Host <span class="text-danger">*</span></label>
            <input type="text" name="host" class="form-control" />
        </div>
    </div>
    <div class="row mb-5">
        <div class="col-xs-12 form-group">
            <label class="mb-2" for="url">Port <span class="text-danger">*</span></label>
            <input type="number" name="port" class="form-control" />
        </div>
    </div>
    <div class="row mb-5">
        <div class="col-xs-12 form-group">
            <label class="mb-2" for="url">Username <span class="text-danger">*</span></label>
            <input type="text" name="username" class="form-control" />
        </div>
    </div>
    <div class="row mb-5">
        <div class="col-xs-12 form-group">
            <label class="mb-2" for="password">Password <span class="text-danger">*</span></label>
            <input type="text" id="password" name="password" class="form-control" />
        </div>
    </div>
    <div class="row mb-5">
        <div class="col-xs-12 form-group">
            <label class="mb-2" for="encryption">Encryption <span class="text-danger">*</span></label>
            <input type="text" name="encryption" class="form-control" />
        </div>
    </div>
    <div class="row mb-5">
        <div class="col-xs-12 form-group">
            <button type="button" id="btn-create" class="btn btn-primary" onclick="store()"><i class="fas fa-spinner fa-spin spinner-btn"></i> Create</button>
        </div>
    </div>
</form>
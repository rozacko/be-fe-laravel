<form action="#" id="form-update">
    <div class="row mb-5">
        <div class="col-xs-12 form-group">
            <label class="mb-2" for="name">Host <span class="text-danger">*</span></label>
            <input type="text" name="host" class="form-control" value="{{ $host ?? '' }}" />
        </div>
    </div>
    <div class="row mb-5">
        <div class="col-xs-12 form-group">
            <label class="mb-2" for="url">Port <span class="text-danger">*</span></label>
            <input type="number" name="port" class="form-control" value="{{ $port ?? '' }}" />
        </div>
    </div>
    <div class="row mb-5">
        <div class="col-xs-12 form-group">
            <label class="mb-2" for="url">Username <span class="text-danger">*</span></label>
            <input type="text" name="username" class="form-control" value="{{ $username ?? '' }}" />
        </div>
    </div>
    <div class="row mb-5">
        <div class="col-xs-12 form-group">
            <label class="mb-2" for="password">Password <span class="text-danger">*</span></label>
            <input type="text" id="password" name="password" class="form-control" value="{{ $password ?? '' }}" />
        </div>
    </div>
    <div class="row mb-5">
        <div class="col-xs-12 form-group">
            <label class="mb-2" for="encryption">Encryption <span class="text-danger">*</span></label>
            <input type="text" name="encryption" class="form-control" value="{{ $encryption ?? '' }}" />
        </div>
    </div>
    <div class="row mb-5">
        <div class="col-xs-12 form-group">
            <button type="button" id="btn-update" class="btn btn-primary" onclick="update('{{ $uuid }}')"><i class="fas fa-spinner fa-spin spinner-btn"></i> Update</button>
        </div>
    </div>
</form>
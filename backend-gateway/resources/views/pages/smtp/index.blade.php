<x-layout>
    @section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">List SMTP Connection</h4>
                    <div class="card-toolbar">
                        <div class="d-flex justify-content-end" data-kt-user-table-toolbar="base">
                            <button type="button" class="btn btn-primary" onclick="create()">
                                <i class="fa fa-plus"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="data-table-fixed-columns" class="table table-row-bordered gy-5">
                            <thead>
                                <tr>
                                    <th class="text-nowrap">Host</th>
                                    <th class="text-nowrap">Port</th>
                                    <th class="text-nowrap">Username</th>
                                    <th class="text-nowrap">Password</th>
                                    <th class="text-nowrap">Encryption</th>
                                    <th class="text-nowrap">Status</th>
                                    <th class="text-nowrap no-sort">#</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Route</h4>
                    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                        <span class="svg-icon svg-icon-1">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black" />
                                <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black" />
                            </svg>
                        </span>
                    </div>
                </div>
                <div class="modal-body">

                </div>
            </div>
        </div>
    </div>

    @endsection
    @push('scripts')
    <script type="text/javascript">
        var datatable;
        $(document).ready(function() {
            datatable = $('#data-table-fixed-columns').DataTable({
                "language": {
                    "lengthMenu": "Show _MENU_",
                },
                "dom": "<'row'" +
                    "<'col-sm-6 d-flex align-items-center justify-conten-start'l>" +
                    "<'col-sm-6 d-flex align-items-center justify-content-end'f>" +
                    ">" +

                    "<'table-responsive'tr>" +

                    "<'row'" +
                    "<'col-sm-12 col-md-5 d-flex align-items-center justify-content-center justify-content-md-start'i>" +
                    "<'col-sm-12 col-md-7 d-flex align-items-center justify-content-center justify-content-md-end'p>" +
                    ">",
                searchDelay: 500,
                serverSide: true,
                processing: true,
                ajax: "{{route('web.smtp')}}",
                columns: [{
                    data: 'host'
                }, {
                    data: 'port'
                }, {
                    data: 'username'
                }, {
                    data: 'password'
                }, {
                    data: 'encryption'
                }, {
                    data: null,
                    render: function(r, v, i) {
                        var button = '';
                        if (r.status == "n") {
                            return `
                            <button type="button" onclick="active('${r.uuid}')" class="btn btn-sm btn-primary">
                                <i class="fas fa-check-circle"></i> Set Active
                            </button>
                        `
                        } else if (r.status == "y") {
                            return `<button type="button" class="btn btn-sm btn-success">
                                <i class="fas fa-check"></i> Active
                            </button>`
                        };
                        return button;
                    }
                }, {
                    data: null,
                    render: function(r, v, i) {
                        return `
                            <div class="btn-group">
                                <button type="button" onclick="edit('${r.uuid}')" class="btn btn-sm btn-warning">
                                    <i class="fa fa-edit"></i>
                                </button>&nbsp;
                                <button type="button" onclick="destroy('${r.uuid}')" class="btn btn-sm btn-danger">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </div>
                        `;
                    }
                }]
            });
        });

        function create() {
            $.ajax({
                url: "{{url('/config/smtp/create')}}",
                method: "GET"
            }).done(function(response) {
                $('.modal-title').html("Create Connection");
                $('.modal-body').html(response);
                $(".spinner-btn").hide();
                $('#modal').modal('show');
            })
        }

        function store() {
            var form = $("#form-create").serializeArray();
            $(".spinner-btn").show();
            $("#btn-store").attr('disabled', true);
            $(".is-invalid").removeClass("is-invalid");
            $(".invalid-feedback").remove();
            form.push({
                name: '_token',
                value: "{{ csrf_token() }}"
            });
            $.ajax({
                    url: "{{url('/config/smtp')}}",
                    method: "POST",
                    data: form
                })
                .done(function(response) {
                    datatable.ajax.reload();
                    $("#modal").modal('hide');
                    var {
                        message
                    } = response;
                    Swal.fire({
                        icon: 'success',
                        title: message,
                        showConfirmButton: false,
                        timer: 1500
                    })
                })
                .fail(function(xhr) {
                    $(".spinner-btn").hide();
                    $("#btn-store").attr('disabled', false);
                    var {
                        errors,
                        message
                    } = xhr.responseJSON;
                    $.each(errors, function(i, v) {
                        var message = '<div class="invalid-feedback">' + v + '</div>';
                        $('[name="' + i + '"]').addClass("is-invalid");
                        $('[name="' + i + '"]').after(message);
                    });
                    $.gritter.add({
                        title: 'Error!',
                        text: message,
                        class_name: 'gritter-danger'
                    });
                })
        }

        function edit(uuid) {
            $(".spinner-btn").show();
            $.ajax({
                url: "{{url('/config/smtp')}}/" + uuid + "/edit",
                method: "GET"
            }).done(function(response) {
                $('.modal-title').html("Update Connection");
                $('.modal-body').html(response);
                $(".spinner-btn").hide();
                $('#modal').modal('show');
            })
        }

        function update(uuid) {
            $(".spinner-btn").show();
            $("#btn-update").attr('disabled', true);
            $(".is-invalid").removeClass("is-invalid");
            $(".invalid-feedback").remove();
            var form = $("#form-update").serializeArray();
            form.push({
                name: '_method',
                value: 'PUT'
            });
            form.push({
                name: '_token',
                value: "{{ csrf_token() }}"
            });
            $.ajax({
                    url: "{{url('/config/smtp')}}/" + uuid,
                    method: "POST",
                    data: form
                })
                .done(function(response) {
                    datatable.ajax.reload();
                    $("#modal").modal('hide');
                    var {
                        message
                    } = response;
                    Swal.fire({
                        icon: 'success',
                        title: message,
                        showConfirmButton: false,
                        timer: 1500
                    })
                })
                .fail(function(xhr) {
                    $(".spinner-btn").hide();
                    $("#btn-update").attr('disabled', false);
                    var {
                        errors,
                        message
                    } = xhr.responseJSON;
                    $.each(errors, function(i, v) {
                        var message = '<div class="invalid-feedback">' + v + '</div>';
                        $('[name="' + i + '"]').addClass("is-invalid");
                        $('[name="' + i + '"]').after(message);
                    });
                    Swal.fire({
                        icon: 'error',
                        title: message,
                        showConfirmButton: false,
                        timer: 1500
                    })
                })
        }

        function destroy(uuid) {
            Swal.fire({
                title: 'Are you sure?',
                text: 'You will not be able to recover this data!',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes',
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                            url: "{{url('/config/smtp')}}/" + uuid,
                            method: "POST",
                            data: {
                                _method: "DELETE",
                                _token: "{{ csrf_token() }}",
                            }
                        })
                        .done(function(response) {
                            var {
                                message
                            } = response;
                            Swal.fire({
                                icon: 'success',
                                title: message,
                                showConfirmButton: false,
                                timer: 1500
                            })
                            datatable.ajax.reload();
                        })
                        .fail(function(xhr) {
                            var {
                                message
                            } = xhr.responseJSON;
                            Swal.fire({
                                icon: 'error',
                                title: message,
                                showConfirmButton: false,
                                timer: 1500
                            })
                        })
                }
            })
        }

        function active(uuid) {
            Swal.fire({
                title: 'Are you sure?',
                showCancelButton: true,
                confirmButtonText: 'Yes',
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                            url: "{{url('/config/smtp')}}/" + uuid + "/active",
                            method: "POST",
                            data: {
                                _method: "PUT",
                                _token: "{{ csrf_token() }}",
                            }
                        })
                        .done(function(response) {
                            var {
                                message
                            } = response;
                            Swal.fire({
                                icon: 'success',
                                title: message,
                                showConfirmButton: false,
                                timer: 1500
                            })
                            datatable.ajax.reload();
                        })
                        .fail(function(xhr) {
                            var {
                                message
                            } = xhr.responseJSON;
                            Swal.fire({
                                icon: 'error',
                                title: message,
                                showConfirmButton: false,
                                timer: 1500
                            })
                        })
                }
            })
        }
    </script>

    @endpush
</x-layout>
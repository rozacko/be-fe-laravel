<x-layout>
    @section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Permissions of {{$name}} Feature</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="data-table-fixed-columns" class="table table-row-bordered gy-5">
                            <thead>
                                <tr>
                                    <th class="text-nowrap">Name</th>
                                    <th class="text-nowrap">Action</th>
                                    <th class="text-nowrap">Route</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
    @push('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#data-table-fixed-columns').DataTable({
                "language": {
                    "lengthMenu": "Show _MENU_",
                },
                "dom": "<'row'" +
                    "<'col-sm-6 d-flex align-items-center justify-conten-start'l>" +
                    "<'col-sm-6 d-flex align-items-center justify-content-end'f>" +
                    ">" +

                    "<'table-responsive'tr>" +

                    "<'row'" +
                    "<'col-sm-12 col-md-5 d-flex align-items-center justify-content-center justify-content-md-start'i>" +
                    "<'col-sm-12 col-md-7 d-flex align-items-center justify-content-center justify-content-md-end'p>" +
                    ">",
                searchDelay: 500,
                serverSide: true,
                processing: true,
                ajax: '{{url("config/feature/{$uuid}/permissions")}}',
                columns: [{
                    data: 'name',
                    name: 'name'
                }, {
                    data: 'action_id',
                    name: 'action_id'
                }, {
                    data: 'routes',
                    render: function(v, i, r) {
                        var route = "";
                        $.each(v, function(key, val) {
                            route += `<span class="badge badge-success">${ val.name }</span>&nbsp;`;
                            if (key % 5 == 0 && key != 0) {
                                route += `<br/>`;
                            }
                        })
                        return route;
                    }
                }]
            });
        });
    </script>

    @endpush
</x-layout>
<table>
    <tr>
        <td colspan="4" style="text-align:center;"><strong>Limbah B3 Masuk Ke Fasilitas</strong></td>
        <td colspan="4" style="text-align:center;"><strong>Produk Keluar Dari Penyimpanan</strong></td>
        <td colspan="1" style="text-align:center;"><strong>Sisa</strong></td>
    </tr>
    <tr>
        <td>Tanggal</td>
        <td>Sumber</td>
        <td>Jumlah (ton)</td>
        <td>Masa Simpan</td>
        <td>Tanggal</td>
        <td>Sumber</td>
        <td>Jumlah (ton)</td>
        <td>Masa Simpan</td>
        <td>Sisa yang Ada di TPS</td>
    </tr>
    @php $i=1;$fasilitator="";$gugus=""; @endphp
    @foreach($data['data'] as $key => $row)
    <tr>
        <td>
            {{ \Carbon\Carbon::createFromFormat('Y-m-d', $row->tanggal_masuk)->format('d-m-Y') }}
        </td>
        <td>{{$row->sumber_masuk}}</td>
        <td>{{$row->jml_ton_masuk}}</td>
        <td>
            {{ \Carbon\Carbon::createFromFormat('Y-m-d', $row->masa_simpan_masuk)->format('d-m-Y') }}
        </td>
        <td>
            {{ \Carbon\Carbon::createFromFormat('Y-m-d', $row->tanggal_keluar)->format('d-m-Y') }}
        </td>
        <td>{{$row->sumber_keluar}}</td>
        <td>{{$row->jml_ton_keluar}}</td>
        <td>
            {{ \Carbon\Carbon::createFromFormat('Y-m-d', $row->masa_simpan_keluar)->format('d-m-Y') }}
        </td>
        <td>{{$row->sisa_tps}}</td>
    </tr>
    @php $i++ @endphp
    @endforeach
    <tr>
        <td style="font-weight: bold;">Total</td>
        <td></td>
        <td style="font-weight: bold;">{{$data['data_total'][0]['ton_masuk']}}</td>
        <td></td>
        <td></td>
        <td></td>
        <td style="font-weight: bold;">{{$data['data_total'][0]['ton_keluar']}}</td>
        <td></td>
        <td style="font-weight: bold;">{{$data['data_total'][0]['ton_sisa_tps']}}</td>
    </tr>
</table>

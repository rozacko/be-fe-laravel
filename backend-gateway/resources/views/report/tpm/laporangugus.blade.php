<table>
    <tr>
        <td colspan="15" style="text-align:left;"><strong>Laporan Gugus TPM GHoPO</strong></td>
    </tr>
    <tr>
        <td colspan="2" style="text-align:left;"><strong>Bulan {{$data['tanggal']}}</strong></td>
    </tr>
    <tr><td colspan="15">&nbsp;</td></tr>
    <tr>
        <td>No</td>
        <td rowspan="2">Fasilitator</td>
        <td rowspan="2">Gugus</td>
        <td rowspan="2">SGA</td>
        <td rowspan="2">Commitment Management</td>
        <td rowspan="2">Papan Kontrol</td>
        <td rowspan="2">Safety, Health &amp; Environment</td>
        <td colspan="5">5R</td>
        <td rowspan="2">Autonomous Maintenance</td>
        <td rowspan="2">Planned Maintenance</td>
        <td rowspan="2">Focused Improvement</td>
        <td rowspan="2">Target</td>
        <td rowspan="2">Nilai SGA</td>
        <td rowspan="2">Nilai Gugus</td>
        <td rowspan="2">Nilai Fasilitator</td>
    </tr>
    <tr>
        <td>R1</td>
        <td>R2</td>
        <td>R3</td>
        <td>R4</td>
        <td>R5</td>
    </tr>
    @php $i=1;$fasilitator="";$gugus=""; @endphp
    @foreach($data['model'] as $key => $row)

    <tr>
        <td>{{$i}}</td>
        <td>{{$row->fasilitator}}</td>
        <td>{{$row->gugus}}</td>
        <td>{{$row->sga}}</td>
        <td>{{$row->commitment_management}}</td>
        <td>{{$row->papan_control}}</td>
        <td>{{$row->she}}</td>
        <td>{{$row['5r']}}</td>
        <td>{{$row->r2}}</td>
        <td>{{$row->r3}}</td>
        <td>{{$row->r4}}</td>
        <td>{{$row->r5}}</td>
        <td>{{$row->autonomous_maintenance}}</td>
        <td>{{$row->planed_maintenance}}</td>
        <td>{{$row->focused_improvement}}</td>
        <td>{{$row->target}}</td>
        <td>{{$row->nilai_SGA}}</td>
        <td>{{$row->nilai_gugus}}</td>
        <td>{{$row->nilai_fasilitator}}</td>
    </tr>
    @php $i++ @endphp
    @endforeach
</table>

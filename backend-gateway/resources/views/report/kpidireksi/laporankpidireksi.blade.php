<table>
    <tr>
        <td colspan="15" style="text-align:left;"><strong>Laporan KPI Direksi</strong></td>
    </tr>
    <tr>
        <td colspan="2" style="text-align:left;"><strong>Bulan {{$data['tanggal']}}</strong></td>
    </tr>
    <tr><td colspan="15">&nbsp;</td></tr>
    <tr>
        <td>No</td>
        <td>Key Performance Indicator</td>
        <td>KPI Group</td>
        <td>Unit</td>
        <td>Polarisasi</td>
        <td>Target</td>
        <td>Prognose</td>
        <td>Real</td>
        <td>Capaian KPI</td>
    </tr>
    @php $i=1; @endphp
    @foreach($data['model'] as $key => $row)

    <tr>
        <td>{{$i}}</td>
        <td>{{$row->kpi}}</td>
        <td>{{$row->kpi_group}}</td>
        <td>{{$row->unit}}</td>
        <td>{{$row->polarisasi}}</td>
        <td>{{$row->target}}</td>
        <td>{{$row->prognose}}</td>
        <td>{{$row->realisasi}}</td>
        <td>{{$row->persentase}}</td>
    </tr>
    @php $i++ @endphp
    @endforeach
</table>

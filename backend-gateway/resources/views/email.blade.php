<table align="center" cellspacing="0" width="520" cellpadding="0" style="padding-bottom:20px">
    <tbody>
        <tr>
            <td>
                <table cellspacing="0" width="520" cellpadding="0" style="margin-top:0px;margin-bottom:0px;margin-left:20px;margin-right:20px">
                    <tbody>
                        <tr width="520">
                            <td width="520" height="52" style="border-bottom:1px solid #eaeaea;padding-top:10px">
                                <table>
                                    <tbody>
                                        <tr width="520">
                                            <td width="50" id="m_-5247147174199893146logo">
                                                <img id="m_-5247147174199893146amazonLogoImg" alt="Amazon Logo Image" width="100" height="40" src="https://zoom.us/account/branding/p/e8849f2f-cc59-4549-b0f3-acf1cb68d35b.png" class="CToWUd">
                                            </td>
                                            <td width="150" id="m_-5247147174199893146logo">
                                                <img id="m_-5247147174199893146amazonLogoImg" alt="Amazon Logo Image" width="220" height="27" src="https://viral.pupuk-indonesia.com/assets/img/pi_group.png" class="CToWUd">
                                            </td>
                                            <td width="320" style="font-size:17px;text-align:right;padding-left:0px;padding-bottom:0px;padding-right:0px;padding-top:2px;font-family:'Amazon Ember',Arial,sans-serif">&nbsp;</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size:17px;font-family:'Amazon Ember',Arial,sans-serif;padding-top:15px;padding-bottom:0px;padding-left:0px;padding-right:1px">
                                {!! $content !!}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>

<?php

/*
 * english
 */
return [
    'login-success'             => 'User successfully login.',
    'login-fail'                => 'Username or Password is wrong.',
    'logout-success'            => 'User successfully logged out.',
    'logout-fail'               => 'Failed to logout, please try again.',
    'create-success'            => 'Data has been successfully created.',
    'create-fail'               => 'Unable to create!.',
    'update-success'            => 'Data has been successfully updated.',
    'update-fail'               => 'Unable to update!.',
    'update-no-changed'         => 'No data changed',
    'delete-success'            => 'Data has been successfully deleted.',
    'delete-fail'               => 'Unable to delete!.',
    'show-import-success'       => 'Data has been successfully show.',
    'show-import-fail'          => 'Unable to show data!.',
    'import-success'            => 'Data has been successfully imported.',
    'import-fail'               => 'Unable to import!.',
    'upload-success'            => 'Data has been successfully uploaded.',
    'upload-fail'               => 'Unable to upload!.',
    'read-success'              => 'Get data successfully.',
    'read-fail'                 => 'No data found.',
    'validation-fail'           => 'Validation failed',
    'uuid-fail'                 => 'UUID given is not a valid format',
    'sap-sync-success'          => 'SAP sync success',
    'sap-sync-fail'             => 'SAP sync failed',
    'email-send-success'        => 'Email has been successfully sent.',
    'email-send-fail'           => 'Unable to send email!.',
    'user-not-activated'        => 'User has not been activated.',
    'spatie-not-login'          => 'User is not logged in.',
    'spatie-not-permission'     => 'User does not have the right permissions.',
    'jwt-invalid'               => 'Token is Invalid.',
    'jwt-expired'               => 'Token is Expired.',
    'jwt-not-found'             => 'Authorization Token not found.',
    'page-not-found'            => 'Sorry, the route you are looking for could not be found.',
    'method-not-allowed'        => 'Method not allowed.',
    'validation-template-fail'  => 'Template Is Wrong',
    'inline-fail'               => 'Inline ',
    'contains-formula'          => 'contains formula, please fill with text value'
];

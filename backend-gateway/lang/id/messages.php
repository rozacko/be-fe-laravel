<?php

/*
 * indonesia
 */
return [
    'login-success'             => 'User berhasil login.',
    'login-fail'                => 'Username atau Password salah.',
    'logout-success'            => 'User berhasil logout.',
    'logout-fail'               => 'Gagal logout, silahkan coba lagi.',
    'create-success'            => 'Data telah berhasil ditambahkan.',
    'create-fail'               => 'Gagal menambahkan data!.',
    'update-success'            => 'Data telah berhasil diubah.',
    'update-fail'               => 'Gagal mengubah data!.',
    'update-no-changed'         => 'Tidak ada perubahan data.',
    'delete-success'            => 'Data telah berhasil dihapus.',
    'delete-fail'               => 'Gagal menghapus data!.',
    'show-import-success'       => 'Data telah berhasil ditampilkan.',
    'show-import-fail'          => 'Gagal menampilkan data!.',
    'import-success'            => 'Data telah berhasil diimpor.',
    'import-fail'               => 'Gagal mengimpor data!.',
    'upload-success'            => 'Data telah berhasil diupload.',
    'upload-fail'               => 'Gagal mengupload data!.',
    'read-success'              => 'Berhasil mendapatkan data.',
    'read-fail'                 => 'Tidak ditemukan data.',
    'validation-fail'           => 'Gagal validasi',
    'uuid-fail'                 => 'UUID yang diberikan bukan format yang valid',
    'sap-sync-success'          => 'Sync SAP berhasil',
    'sap-sync-fail'             => 'Sync SAP gagal',
    'email-send-success'        => 'Email telah berhasil dikirim.',
    'email-send-fail'           => 'Gagal mengirim email!.',
    'user-not-activated'        => 'User belum diaktivasi.',
    'spatie-not-login'          => 'Pengguna belum login.',
    'spatie-not-permission'     => 'Pengguna tidak memiliki akses.',
    'jwt-invalid'               => 'Token tidak valid.',
    'jwt-expired'               => 'Token kadaluarsa.',
    'jwt-not-found'             => 'Token Otorisasi tidak ditemukan.',
    'page-not-found'            => 'Maaf, route tidak ditemukan.',
    'method-not-allowed'        => 'Metode tidak diizinkan.',
    'validation-template-fail'  => 'Template Upload Salah',
    'inline-fail'               => 'Pada Baris Ke ',
    'contains-formula'          => 'mengandung rumus (formula), Silahkan isi dengan tipe text'
];

# Backend GHOPO

## System Requirement
- PHP 8.0 or Above
- Composer

## Installation & Configuration
- Clone the repository
- install depedencies 
`composer install`
- copy .env.example to .env
- generate key
`php artisan key:generate`
- run database migration and seed 
`php artisan migrate --seed`
- Generate JWT Secret key
`php artisan jwt:secret`

## Running the Server
`php artisan serve`

## Helpers  
### Response  
Located in `app\Helpers\Response.php`, this helper is for standardize API Response format
~~~php  
  return response()->json(responseSuccess('Successfully created', Response::HTTP_CREATED);
~~~  

## Middleware
- **JWT Middleware** `jwt.verify`  
This middleware is for authenticate token & handling jwt exception 
- **JWT Refresh** `jwt.regenerate`  
It will refresh token automatically on request, for enable this middleware, set variable `JWT_REFRESH=true`
- **Localization**  
It's API middleware for multilingual, translated phrases located in `lang` and request headers must containt `Accept-Language`  
example usage: 
~~~php
__('messages.update-success')
~~~

- **Logging Activity**  
For logging all user activity, it can be accessed in configuration page

## Configuration page
It can be accessed by `http://127.0.0.1:8000/config`  
- Activity Log
- Error Log
- API Documentation
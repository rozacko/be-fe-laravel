<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Response;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of exception types with their corresponding custom log levels.
     *
     * @var array<class-string<\Throwable>, \Psr\Log\LogLevel::*>
     */
    protected $levels = [
        //
    ];

    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<\Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });

        $this->renderable(function (Exception $exception, $request) {
            if ($request->is('api/*')) {
                if ($exception instanceof NotFoundHttpException) {
                    if ($exception->getPrevious() && $exception->getPrevious() instanceof ModelNotFoundException) {
                        $response = responseFail(__('messages.read-fail'));
                        return response()->json($response, Response::HTTP_NOT_FOUND, [], JSON_PRETTY_PRINT);
                    }
                    $response = responseFail(__('messages.page-not-found'));
                    return response()->json($response, Response::HTTP_NOT_FOUND, [], JSON_PRETTY_PRINT);
                } else if ($exception instanceof MethodNotAllowedHttpException) {
                    $response = responseFail(__('messages.method-not-allowed'));
                    return response()->json($response, Response::HTTP_METHOD_NOT_ALLOWED, [], JSON_PRETTY_PRINT);
                }
            }
        });
    }
}

<?php

namespace App\Console\Commands;

use App\Services\SyncSapService;
use Illuminate\Console\Command;

class SyncSapCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'synchronize:sap {--type=} {--id=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Synchronize SAP';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $type = $this->option('type');
        $id = $this->option('id');

        (new SyncSapService)->execute($id, $type);
    }
}

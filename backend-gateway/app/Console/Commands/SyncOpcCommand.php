<?php

namespace App\Console\Commands;

use App\Services\SyncOpcService;
use Illuminate\Console\Command;

class SyncOpcCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'synchronize:opc {--type=} {--action=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync OPC';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $type = $this->option('type');
        $action = $this->option('action');

        $response = (new SyncOpcService)->{$action}($type);
        $this->info($response);
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\M_ApiShipment;
use App\Services\SyncShipmentService;

class SyncShipmentCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'synchronize:shipment {--type=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync Data Shipment Management';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $type = $this->option('type');

        $api = M_ApiShipment::where('api_name','Shipment '.ucwords($type))->first();
        if(is_null($api)){
            $this->info("API Tidak Ditemukan!");
        }
        else{
            $response = (new SyncShipmentService)->{strtolower(preg_replace("/[^A-Za-z0-9]/", "_", $api->api_name))}($api);
            $this->info($response);
        }
    }
}

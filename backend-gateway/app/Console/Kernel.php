<?php

namespace App\Console;

use App\Models\SapSyncConfig;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Services\SyncOpcService;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();
        $configs = SapSyncConfig::where(['status' => 'active'])->get();
        foreach ($configs as $config) {
            $command = "synchronize:sap -v --type=$config->type --id=$config->id";
            if ($config->schedule == 'minutely') {
                $schedule->command($command)->everyMinute()->withoutOverlapping();
            } elseif ($config->schedule == 'everyfiveminute') {
                $schedule->command($command)->everyFiveMinutes()->withoutOverlapping();
            } elseif ($config->schedule == 'hourly') {
                $schedule->command($command)->hourly()->withoutOverlapping();
            } elseif ($config->schedule == 'daily') {
                $schedule->command($command)->dailyAt($config->at_time)->withoutOverlapping();
            } elseif ($config->schedule == 'monthly') {
                $schedule->command($command)->monthlyOn($config->at_date, $config->at_time)->withoutOverlapping();
            }
        }

        $schedule->command("synchronize:opc --type=MINUTELY --action=syncOpcList")->everyMinute()->withoutOverlapping();

        $schedule->command("synchronize:opc --type=HOURLY --action=averageValue")->hourlyAt("10")->withoutOverlapping();
        $schedule->command("synchronize:opc --type=DAILY --action=averageValue")->dailyAt("00:20")->withoutOverlapping();

        $schedule->command("synchronize:opc --type=HOURLY --action=summaryValue")->hourlyAt("15")->withoutOverlapping();
        $schedule->command("synchronize:opc --type=DAILY --action=summaryValue")->dailyAt("00:25")->withoutOverlapping();

        $schedule->command("synchronize:opc --type=3 --action=plgWeb")->dailyAt("00:30")->withoutOverlapping();
        $schedule->command("synchronize:opc --type=4 --action=plgWeb")->dailyAt("00:45")->withoutOverlapping();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}

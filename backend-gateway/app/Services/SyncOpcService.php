<?php

namespace App\Services;

use App\Imports\PlgWebImport;
use Illuminate\Support\Facades\DB;
use App\Models\MArea;
use App\Models\MPlant;
use App\Models\M_OpcApi;
use App\Models\M_JenisOpc;
use App\Models\M_TagList;
use App\Models\T_PenarikanOpc;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Storage;

class SyncOpcService
{
    public function syncOpcList($type='MINUTELY'){

        DB::beginTransaction();
        try{
            $areas = MArea::all();
            $plants = MPlant::all();
            $api = M_OpcApi::where('api_name','OPC REST')->first();
            foreach($areas as $area){
                foreach($plants as $plant){
                    $_tag = [];
                    $_opc = M_JenisOpc::with(['tags' => function($query) use($plant, $type) {
                                    $query->where('id_plant', $plant->id)
                                    ->where(DB::raw('upper(sync_period)'),strtoupper($type))
                                    ->whereNot('tag_name','ilike','%N/A%');
                            }])
                            ->whereHas('tags',function($query) use($plant, $type) {
                                $query->where('id_plant', $plant->id)
                                ->where(DB::raw('upper(sync_period)'),strtoupper($type))
                                ->whereNot('tag_name','ilike','%N/A%');
                            })
                            ->where('id_area',$area->id)
                            ->get();
                    foreach($_opc as $_opc_){
                        if(count($_opc_->tags) > 0){
                           $tag = $_opc_->tags[0];
                           $_tag[] = ['name' => $tag->tag_name, 'props' => [['name' => 'Value']]]; 
                        }
                    }
                    $getTag = ['tags' => $_tag, 'status' => 'OK', 'message' => '', 'token' => env('OPC_REST')];

                    $params_opc = [
                        'message' => json_encode($getTag),
                        '_' => '1469589103720'
                    ];

                    $client = new Client();
                    $_param_type = ['GET' => 'query', 'POST' => 'form_params'];
                    $response = $client->request($api->method, $api->api_url, ['query' => $params_opc]);
                    $result = (string)$response->getBody();
                    $_data = json_decode(substr($result, 1, strlen($result)-3),true);

                    $data_tag = $_data['tags'];
                    foreach($data_tag as $dt_tag){
                        $tag_name = $dt_tag['name'];
                        $_props = $dt_tag['props'][0];
                        $_param_penarikan = [];
                        $_param_penarikan['id_tag'] = M_TagList::where('tag_name',$tag_name)->first()->id;
                        $_param_penarikan['waktu_penarikan'] = strtoupper($type)=='DAILY' ? date('Y-m-d', strtotime('yesterday')) : date('Y-m-d H:i:s');
                        $_param_penarikan['status'] = FALSE;
                        $_param_penarikan['created_by'] = '2';
                        if(isset($_props['val'])){
                            $_param_penarikan['tag_value'] = $_props['val'];
                            $_param_penarikan['status'] = TRUE;
                        }

                        if(strtoupper($type)=='DAILY'){
                            $_penarikan = T_PenarikanOpc::updateOrCreate(
                                            ['id_tag' => M_TagList::where('tag_name',$tag_name)->first()->id, 
                                             'waktu_penarikan' => date('Y-m-d', strtotime('yesterday'))], $_param_penarikan);
                        }
                        else{
                            $_penarikan = T_PenarikanOpc::updateOrCreate(
                                            ['id_tag' => M_TagList::where('tag_name',$tag_name)->first()->id, 
                                             'waktu_penarikan' => date('Y-m-d')], $_param_penarikan);
                        }
                    }

                }
                
            }
            
            DB::commit();
            return "Success";
        }
        catch(\Exception $ex){
            DB::rollBack();
            return $ex->getMessage();
        }
    }

    public function averageValue($type='HOURLY'){
        $arr_type = ['DAILY' => 'DAY', 'HOURLY' => 'HOUR'];
        $period = $arr_type[$type];
        DB::beginTransaction();
        try{
            $data = T_PenarikanOpc::select('id_tag',
                        DB::raw("avg(case when upper(tag_value)='TRUE' then '1' when upper(tag_value)='FALSE' then '0' else tag_value end::float) as tag_value"),
                        DB::raw("date_trunc('{$period}', waktu_penarikan) as waktu_penarikan"))
                    ->with(['opc' => function($query) use ($type) {
                        $query->where('sync_period','<>','Daily')
                                ->where(strtolower($type).'_type','=','average');
                    }])
                    ->groupBy('id_tag',DB::raw("date_trunc('{$period}', waktu_penarikan)"))
                    ->whereHas('opc', function($query) use ($type) {
                        $query->where('sync_period','<>','Daily')
                                ->where(strtolower($type).'_type','=','average');
                    })
                    ->where(DB::raw("date_trunc('{$period}', waktu_penarikan)"),'=',DB::raw("date_trunc('{$period}', now()::date - interval '1 {$period}')"))
                    ->get();

            foreach($data as $row){
                $deleted = T_PenarikanOpc::where('id_tag', $row->id_tag)
                        ->where(DB::raw("date_trunc('{$period}', waktu_penarikan)"),'=',DB::raw("date_trunc('{$period}', now()::date - interval '1 {$period}')"))
                        ->delete();

                $_param_penarikan = [];
                $_param_penarikan['id_tag'] = $row->id_tag;
                $_param_penarikan['waktu_penarikan'] = strtoupper($period)=='DAY' ? date('Y-m-d', strtotime('yesterday')) : date('Y-m-d H:', strtotime('-1 {$period}'));;
                $_param_penarikan['status'] = TRUE;
                $_param_penarikan['created_by'] = '2';
                $_param_penarikan['tag_value'] = (float) $row->tag_value / $row->opc->{strtolower($type)."_factor"};

                $_penarikan = T_PenarikanOpc::updateOrCreate(
                            ['id_tag' => $row->id_tag, 
                             'waktu_penarikan' => $_param_penarikan['waktu_penarikan']], $_param_penarikan);
            }

            
            DB::commit();
            return "Success";
        }
        catch(\Exception $ex){
            DB::rollBack();
            return $ex->getMessage();
        }
    }

    public function summaryValue($type='HOURLY'){
        $arr_type = ['DAILY' => 'DAY', 'HOURLY' => 'HOUR'];
        $period = $arr_type[$type];
        DB::beginTransaction();
        try{
            $data = T_PenarikanOpc::select('id_tag',
                        DB::raw("sum(case when upper(tag_value)='TRUE' then '1' when upper(tag_value)='FALSE' then '0' else tag_value end::float) as tag_value"),
                        DB::raw("date_trunc('{$period}', waktu_penarikan) as waktu_penarikan"))
                    ->with(['opc' => function($query) use ($type) {
                        $query->where('sync_period','<>','Daily')
                                ->where(strtolower($type).'_type','=','summary');
                    }])
                    ->groupBy('id_tag',DB::raw("date_trunc('{$period}', waktu_penarikan)"))
                    ->whereHas('opc', function($query) use ($type) {
                        $query->where('sync_period','<>','Daily')
                                ->where(strtolower($type).'_type','=','summary');
                    })
                    ->where(DB::raw("date_trunc('{$period}', waktu_penarikan)"),'=',DB::raw("date_trunc('{$period}', now()::date - interval '1 {$period}')"))
                    ->get();

            foreach($data as $row){
                $deleted = T_PenarikanOpc::where('id_tag', $row->id_tag)
                        ->where(DB::raw("date_trunc('{$period}', waktu_penarikan)"),'=',DB::raw("date_trunc('{$period}', now()::date - interval '1 {$period}')"))
                        ->delete();

                $_param_penarikan = [];
                $_param_penarikan['id_tag'] = $row->id_tag;
                $_param_penarikan['waktu_penarikan'] = strtoupper($period)=='DAY' ? date('Y-m-d', strtotime('yesterday')) : date('Y-m-d H:', strtotime('-1 {$period}'));;
                $_param_penarikan['status'] = TRUE;
                $_param_penarikan['created_by'] = '2';
                $_param_penarikan['tag_value'] = (float) $row->tag_value / $row->opc->{strtolower($type)."_factor"};

                $_penarikan = T_PenarikanOpc::updateOrCreate(
                            ['id_tag' => $row->id_tag, 
                             'waktu_penarikan' => $_param_penarikan['waktu_penarikan']], $_param_penarikan);
            }

            
            DB::commit();
            return "Success";
        }
        catch(\Exception $ex){
            DB::rollBack();
            return $ex->getMessage();
        }
    }


    public function plgWeb($plant='3'){
        $api = M_OpcApi::where('api_name','PlgWeb Tuban '.$plant)->first();
        if(is_null($api)){
            return "Plg Web tidak ditemukan";
        }
        DB::beginTransaction();
        try{
            $excel_url = str_replace("{{tgl}}",date('Ymd',strtotime('yesterday')),$api->api_url);
            $path = Storage::disk('public')->put('plgweb_tuban'.$plant.'.xls', file_get_contents($excel_url), 'public');

            $filepathsource = storage_path('app/public/plgweb_tuban'.$plant.'.xls');
            $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($filepathsource);
            $worksheet = $spreadsheet->getSheetByName("Resume");

            $plant = MPlant::where(DB::raw("upper(nm_plant)"),'=','TUBAN '.$plant)->first();

            foreach($worksheet->toArray() as $row){
                $tag = M_TagList::whereHas('jenis', function($query) use ($row){
                                    $query->where('jenis_opc','=',trim($row[1]));
                                })
                                ->where('id_plant', $plant->id)
                                ->first();
                if(!is_null($tag)){
                    $_param_penarikan = [];
                    $_param_penarikan['id_tag'] = $tag->id;
                    $_param_penarikan['waktu_penarikan'] = date('Y-m-d', strtotime('yesterday'));
                    $_param_penarikan['status'] = TRUE;
                    $_param_penarikan['created_by'] = '2';
                    $_param_penarikan['tag_value'] = $row[2];
    
                    $_penarikan = T_PenarikanOpc::updateOrCreate(
                                ['id_tag' => $tag->id, 
                                 'waktu_penarikan' => $_param_penarikan['waktu_penarikan']], $_param_penarikan);
                }
            }

            DB::commit();
            return "Success";
        }
        catch(\Exception $ex){
            DB::rollBack();
            return $ex->getMessage();
        }
    }
}
<?php

namespace App\Services;

class BaseService
{
    public function getSapConfig()
    {
        return [
            'ashost' => env('SAP_ASHOST', '10.61.3.31'),
            'sysnr' => env('SAP_SYSNR', '10'),
            'client' => env('SAP_CLIENT', '110'),
            'user' => env('SAP_USER', 'DMMRFC'),
            'passwd' => env('SAP_PASSWD', 'sisiDMM')
        ];
    }
}

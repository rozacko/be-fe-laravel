<?php

namespace App\Services;

use App\Models\BiayaMaintenance;
use App\Models\MCostcenter;
use App\Models\MCostelement;
use App\Models\MProjectCapex;
use App\Models\SapSyncConfig;
use App\Models\SapSyncLog;
use App\Models\T_Capex;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\DB;

use SAPNWRFC\Connection as SapConnection;
use SAPNWRFC\Exception as SapException;

class SyncSapService extends BaseService
{
    private function getData($config, $status = false, $dateInput = null)
    {
        $id = 0;
        $dateParam = [];
        if (!$status || $status == 'resync') {
            $id = $config->id;
        }
        $arrayParam = json_decode($config->parameter, true);
        if ($config->type == 'biaya') {
            $dateFrom = Carbon::now()->subDay()->format('Ymd');
            $dateTo = Carbon::now()->format('Ymd');
            if ($dateInput) {
                $dateFrom = $dateInput['date_from'];
                $dateTo = $dateInput['date_to'];
            }
            $dateParam['ta'] = Carbon::createFromFormat('Ymd', $dateFrom)->format('Y-m-d');
            $dateParam['tr'] = Carbon::createFromFormat('Ymd', $dateTo)->format('Y-m-d');
            $date = [
                'I_BUDAT_FROM' => $dateFrom,
                'I_BUDAT_TO' => $dateTo,
            ];
            $params = [
                'I_KOKRS' => 'SGG',
                'LR_KOSTL' => [],
                'LR_KSTAR' => []
            ];
            $costCenters = MCostcenter::select('costcenter')->get();
            foreach ($costCenters as $costCenter) {
                array_push($params['LR_KOSTL'], [
                    "SIGN" => "I",
                    "OPTION" => "EQ",
                    "LOW" => "$costCenter->costcenter",
                    "HIGH" => ""
                ]);
            }
            $costElements = MCostelement::select('cost_element')->get();
            foreach ($costElements as $costElement) {
                $low = str_pad($costElement->cost_element, 10, "0", STR_PAD_LEFT);
                array_push($params['LR_KSTAR'], [
                    "SIGN" => "I",
                    "OPTION" => "EQ",
                    "LOW" => "$low",
                    "HIGH" => ""
                ]);
            }
            $arrayParam = array_merge($params, $date);
        } elseif ($config->type == 'capex') {
            $year = Carbon::now()->year;
            if ($dateInput) {
                $year = Carbon::createFromFormat('Ymd', $dateInput['date_from'])->year;
            }
            $dateParam['year'] = $year;
            $date = [
                "I_GJAHR" => "$year"
            ];
            $params = [
                'I_PBUKR' => '7000',
                'T_PSPHI' => []
            ];

            $projects = MProjectCapex::select('no_project')->get();
            foreach ($projects as $project) {
                array_push($params['T_PSPHI'], [
                    "SIGN" => "I",
                    "OPTION" => "EQ",
                    "LOW" => "$project->no_project",
                    "HIGH" => ""
                ]);
            }
            $arrayParam = array_merge($params, $date);
        }

        $config->dateParam = $dateParam;
        if ($status == 'resync' || $status == 'test') {
            $config->parsedParam = $config->parameter;
        } else {
            $config->parsedParam = json_encode($arrayParam);
        }
        $configName = $config->name;

        $model = SapSyncLog::create([
            'sap_sync_config_id' => $id,
            'config_name' => $configName,
            'year' => date('Y'),
            'month' => date('m'),
            'status' => 'process',
            'tcode' => $config->tcode,
            'parameter' => $config->parsedParam,
        ]);

        $response = $this->reqSap($config);

        $statusResponse = 'success';
        if ($response['status'] == 'fail') {
            $statusResponse = 'fail';
            $model->note = substr($response['message'], 0, 190);
        } elseif ($status == 'test') {
            $model->note = "Test Run";
        } elseif ($response['status'] == 'success' && stripos($response['message'], 'success') === false) {
            $model->note = substr($response['message'], 0, 190);
        }

        $model->status = $statusResponse;
        $model->save();

        return $response;
    }

    private function reqSap($config)
    {
        $sapConfig = $this->getSapConfig();

        try {
            $type = $config->type;
            $param = json_decode($config->parsedParam, true);
            $options = [
                'rtrim' => true
            ];
            $sap = new SapConnection($sapConfig);
            $function = $sap->getFunction($config->tcode);
            $result = $function->invoke($param, $options);

            if ($type == 'capex') {
                $data = $result['RETURN_DATA'];
            } else {
                $data = $result['T_DATA'];
            }

            $this->store($data, $type, $config);

            return [
                'status' => 'success',
                'message' => 'Successfully synced and saved data',
            ];
        } catch (SapException $e) {
            $errorInfo = $e->getErrorInfo();
            if (stripos($errorInfo['message'], 'no costs') !== false) {
                return [
                    'status' => 'success',
                    'message' => $errorInfo['message'],
                ];
            }
            return [
                'status' => 'fail',
                'message' => $e->getMessage(),
            ];
        } catch (Exception $e) {
            return [
                'status' => 'fail',
                'message' => $e->getMessage(),
            ];
        }
    }

    private function store($data, $type, $config)
    {
        try {
            DB::beginTransaction();

            if ($type == 'biaya') {
                $costElements = MCostelement::select('cost_element', 'cost_element_name')->get()
                    ->pluck('cost_element_name', 'cost_element')->toArray();
                $costCenters = MCostcenter::select('costcenter', 'costcenter_name')->get()
                    ->pluck('costcenter_name', 'costcenter')->toArray();
                $lastDate = '';
                $arr_lastDate = [];

                foreach ($data as $item) {
                    $lastDate = Carbon::createFromFormat('Ymd', $item['BUDAT'])->format('Y-m-d');
                    $arr_lastDate[] =  (object) array('last_date' => $lastDate);
                    $costElement = substr($item['KSTAR'], 2);
                    $costCenter = $item['KOSTL'];
                    $elementName = array_key_exists($costElement, $costElements) ? $costElements[$costElement] : null;
                    $costCenterName = array_key_exists($costCenter, $costCenters) ? $costCenters[$costCenter] : null;
                    $insertData = [
                        'no_dokumen' => substr($item['BELNR'], 1),
                        'kode_opco' => $item['BUKRS'],
                        'cost_element' => $costElement,
                        'cost_center' => $costCenter,
                        'tanggal' => $lastDate,
                        'row' => $item['BUZEI'],
                        'cost_element_name' => $elementName,
                        'cost_center_name' => $costCenterName,
                        'biaya' =>  floatval($item['WTGBTR']) * 100
                    ];
                    BiayaMaintenance::updateOrCreate([
                        'no_dokumen' => $insertData['no_dokumen'],
                        'row' => $insertData['row']
                    ], $insertData);
                }
            } elseif ($type == 'capex') {
                $year = $config->dateParam['year'];
                T_Capex::where('tahun', $year)->delete();
                foreach ($data as $item) {
                    $insertData = [
                        'level' => $item['LEVEL'],
                        'no_project' => $item['CURR_PROJECT'],
                        'wbs' => $item['WBS'],
                        'description' => $item['DESC'],
                        'kode_opco' => $item['COMPANY_CODE'],
                        'tanggal' => Carbon::createFromFormat('Ymd', $item['CREAT_ON'])->format('Y-m-d'),
                        'tahun' => $item['GJAHR'],
                        'kode_plant' => $item['PLANT'],
                        'inv_reason' => $item['INV_REASON'],
                        'rs_cost' => $item['RS_COST'],
                        'pr_respon' => $item['PR_RESPON'],
                        'npr_respon' => $item['NPR_RESPON'],
                        'pspri' => $item['PSPRI'],
                        'prctr' => $item['PRCTR'],
                        'actual' => $item['ACTUAL'],
                        'budget' => $item['BUDGET'],
                        'commit' => $item['COMMIT'],
                        'avail' => $item['AVAIL'],
                        'rmorp' => $item['RMORP'],
                        'plan_cost' => $item['PLAN_COST'],
                        'assig_cost' => $item['ASSIG_COST']
                    ];
                    T_Capex::create($insertData);
                }
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            throw new Exception($e->getMessage());
        }
    }

    private function inMillion($nominal = null, $digit = 0)
    {
        if ($nominal && $nominal > 0) {
            $nominal = round($nominal / 1000000, $digit);
        } else
            $nominal = 0;
        return $nominal;
    }

    public function execute($id, $type, $date = null, $config = null, $status = false)
    {
        if (!$config) {
            $config = SapSyncConfig::where(['id' => $id, 'type' => $type, 'status' => 'active'])->firstOrFail();
        }

        return $this->getData($config, $status, $date);
    }
}

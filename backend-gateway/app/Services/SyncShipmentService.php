<?php

namespace App\Services;

use App\Models\M_JenisShipment;
use App\Models\T_PenarikanShipment;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client;
use App\Models\T_StatusSyncShipment;

class SyncShipmentService {

    public function shipment_realisasi($api){
        DB::beginTransaction();
        try{
            $_sync_value = [];

            $check_sync = T_StatusSyncShipment::where([['id_api_shipment','=',$api->id], ['tgl_penarikan','=',date('Y-m-d', strtotime("yesterday"))]])->first();
            if(!is_null($check_sync) && (int) $check_sync->num_tried >= 2)
                return "Already Exceed Maximum of Numbers Tried";

            try{
                $client = new Client();
                $_param_type = ['GET' => 'query', 'POST' => 'form_params'];
                $_params =  json_decode($api->api_params, true);
                $api_param = [];
                foreach($_params as $key=>$value){
                    if(preg_match("/\{{(.*?)\}}/", $value)){
                        preg_match_all("/\{{(.*?)\}}/", $value, $matches);
                        $api_param[$key] = $this->{$matches[1][0]}($matches[1][1]);
                    }
                    else{
                        $api_param[$key] = $value;
                    }
                }
                $response = $client->request($api->api_method, $api->api_url, ['form_params' => $api_param, 'verify' => false]);
                $result = $response->getBody()->getContents();
                $_sync_value = json_decode($result, true);
                
                $_status_sync = ['id_api_shipment' => $api->id, 'tgl_penarikan' => date('Y-m-d', strtotime("yesterday")), 'status' => true];
                if(app()->runningInConsole()){
                    $_status_sync['created_by'] = '2';
                    $_status_sync['updated_by'] = '2';
                }

                if(!is_null($check_sync))
                    $_status_sync['num_tried'] = (int) $check_sync->num_tried + 1;

                T_StatusSyncShipment::updateOrCreate(['id_api_shipment' => $api->id, 'tgl_penarikan' => date('Y-m-d', strtotime("yesterday"))],$_status_sync);
            }
            catch(\GuzzleHttp\Exception\GuzzleException $guzzEx){                
                $_status_sync = ['id_api_shipment' => $api->id, 'tgl_penarikan' => date('Y-m-d', strtotime("yesterday")), 'status' => false];
                if(app()->runningInConsole()){
                    $_status_sync['created_by'] = '2';
                    $_status_sync['updated_by'] = '2';
                }
                T_StatusSyncShipment::updateOrCreate(['id_api_shipment' => $api->id, 'tgl_penarikan' => date('Y-m-d', strtotime("yesterday"))],$_status_sync);
                DB::commit();
                return $guzzEx->getMessage();
            }

            $_list = M_JenisShipment::where('id_api_shipment', $api->id)
                                    ->whereNot(function($query){
                                        $query->where('jenis_shipment','ilike','%conveyor%')
                                            ;
                                    })->get();

            $_conveyor_plant =  M_JenisShipment::where('id_api_shipment', $api->id)
                                ->where(function($query){
                                    $query->where('jenis_shipment','ilike','%conveyor%')
                                    ->orWhere('jenis_shipment','ilike','%packer%');
                                })->get();

            $_plant_conv = [];
            foreach($_conveyor_plant as $_con_plt){
                $_tag_params = json_decode($_con_plt->shipment_params, true);
                foreach($_tag_params as $_tag_param){
                    $_plant_conv[$_con_plt->id_plant][$_tag_param['attr']][] = $_tag_param;
                }
            }

            foreach($_list as $_tag){
                $_tag_params = json_decode($_tag->shipment_params, true);
                $_match = [];
                if(stripos(strtolower($_tag->jenis_shipment),'total release')===false){
                    foreach($_tag_params as $_tag_param){
                        $_match[$_tag_param['attr']][] = $_tag_param;
                    }
                }
                if(stripos(strtolower($_tag->jenis_shipment),'packer')===false){
                    $_match = array_merge($_match, $_plant_conv[$_tag->id_plant]);
                }
                $_arr_matches = [];
                foreach($_match as $_match_key => $_match_value){
                    $_bug_arr = [$_match_key => $_match_value];
                    $_arr_matches[] = $this->_key_match_value($_match_value, array_column($_sync_value, $_match_key), $_tag->shipment_params_check);
                }
                $_key_matches = call_user_func_array('array_intersect_key', $_arr_matches);
                $_matches_sync = array_intersect_key($_sync_value, $_key_matches);

                array_walk($_matches_sync, function(&$_match_val){
                    $_match_val['KWANTUMX'] = floatval($this->parse_number($_match_val['KWANTUMX'],','));
                });

                $_data_sync = array_sum(array_column($_matches_sync, 'KWANTUMX'));

                $_create_param['id_jenis_shipment'] = $_tag->id;
                $_create_param['shipment_date'] = date('Y-m-d', strtotime("yesterday"));
                $_create_param['data_value'] = $_data_sync;
                if(app()->runningInConsole()){
                    $_create_param['created_by'] = '2';
                    $_create_param['updated_by'] = '2';
                }
                T_PenarikanShipment::updateOrCreate(['id_jenis_shipment' => $_tag->id, 
                                        'shipment_date' => $_create_param['shipment_date']
                                    ], $_create_param);
            }

            DB::commit();
            return "success";
        }
        catch(\Exception $ex){
            DB::rollBack();
            return $ex->getMessage();
        }
    }

    public function shipment_inbound($api){
        DB::beginTransaction();
        try{
            $_sync_value = [];

            try{
                $client = new Client();
                $_param_type = ['GET' => 'query', 'POST' => 'form_params'];
                $_params =  json_decode($api->api_params, true);
                $api_param = [];
                foreach($_params as $key=>$value){
                    if(preg_match("/\{{(.*?)\}}/", $value)){
                        preg_match_all("/\{{(.*?)\}}/", $value, $matches);
                        $api_param[$key] = $this->{$matches[1][0]}($matches[1][1]);
                    }
                    else{
                        $api_param[$key] = $value;
                    }
                }
                $response = $client->request($api->api_method, $api->api_url, ['form_params' => $api_param, 'verify' => false]);
                $result = $response->getBody()->getContents();
                $_sync_value = json_decode($result, true);
                
                $_status_sync = ['id_api_shipment' => $api->id, 'tgl_penarikan' => date('Y-m-d', strtotime("yesterday")), 'status' => true];
                if(app()->runningInConsole()){
                    $_status_sync['created_by'] = '2';
                    $_status_sync['updated_by'] = '2';
                }
                T_StatusSyncShipment::updateOrCreate(['id_api_shipment' => $api->id, 'tgl_penarikan' => date('Y-m-d', strtotime("yesterday"))],$_status_sync);
            }
            catch(\GuzzleHttp\Exception\GuzzleException $guzzEx){                
                $_status_sync = ['id_api_shipment' => $api->id, 'tgl_penarikan' => date('Y-m-d', strtotime("yesterday")), 'status' => false];
                if(app()->runningInConsole()){
                    $_status_sync['created_by'] = '2';
                    $_status_sync['updated_by'] = '2';
                }
                T_StatusSyncShipment::updateOrCreate(['id_api_shipment' => $api->id, 'tgl_penarikan' => date('Y-m-d', strtotime("yesterday"))],$_status_sync);
                DB::commit();
                return $guzzEx->getMessage();
            }


            $_list = M_JenisShipment::where('id_api_shipment', $api->id)->get();
            foreach($_list as $_tag){
                $_tag_params = json_decode($_tag->shipment_params, true);
                $_match = [];
                foreach($_tag_params as $_tag_param){
                    $_match[$_tag_param['attr']][] = $_tag_param;
                }
                $_arr_matches = [];
                foreach($_match as $_match_key => $_match_value){
                    $_arr_matches[] = $this->_key_match_value($_match_value, array_column($_sync_value, $_match_key));
                }
                $_key_matches = call_user_func_array('array_intersect_key', $_arr_matches);
                $_matches_sync = array_intersect_key($_sync_value, $_key_matches);

                array_walk($_matches_sync, function(&$_match_val){
                    $_match_val['NETTO'] = floatval($_match_val['NETTO']);
                });

                $_data_sync = array_sum(array_column($_matches_sync, 'NETTO'));

                $_create_param['id_jenis_shipment'] = $_tag->id;
                $_create_param['shipment_date'] = date('Y-m-d', strtotime("yesterday"));
                $_create_param['data_value'] = $_data_sync;
                if(app()->runningInConsole()){
                    $_create_param['created_by'] = '2';
                    $_create_param['updated_by'] = '2';
                }
                T_PenarikanShipment::updateOrCreate(['id_jenis_shipment' => $_tag->id, 'shipment_date' => date('Y-m-d', strtotime("yesterday"))], $_create_param);
            }

            DB::commit();
            return "success";
        }
        catch(\Exception $ex){
            DB::rollBack();
            return $ex->getMessage();
        }
    }

    private function _tgl_sync($format){
        return date($format, strtotime('yesterday'));
    }

    private function _key_match_value($_match_key_values, $_array, $_params_cond = "and"){
        $_contain_pattern = [];
        $_skip_pattern = [];
        foreach($_match_key_values as $_match_key_value){
            if($_match_key_value['condition'] == 'contain'){
                $_contain_pattern[] = "(?=.*?".$_match_key_value['value'].")";
            }
            else{
                $_skip_pattern[] = "(?!.*?".$_match_key_value['value'].")";
            }
        }

        $_str_pattern = "";
        if($_params_cond == "and")
            $_str_pattern .= "(".implode("",$_contain_pattern).")";
        else
            $_str_pattern .= "(".implode("|", $_contain_pattern).")";

        $_str_pattern .= "(".implode("", $_skip_pattern).")";

        $_pattern = "/^".$_str_pattern.".*$/i";

        return preg_grep($_pattern,$_array);
    }

    function parse_number($number, $dec_point=null) {
        if (empty($dec_point)) {
            $locale = localeconv();
            $dec_point = $locale['decimal_point'];
        }
        return floatval(str_replace($dec_point, '.', preg_replace('/[^\d'.preg_quote($dec_point).']/', '', $number)));
    }
}
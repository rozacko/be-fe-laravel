<?php

namespace App\Services;

use App\Models\M_JenisShipment;
use Illuminate\Support\Facades\DB;

use App\Models\M_TagList;

class ProductionService
{
    static public function getDataTag($plant, $tgl_report, $_params){
        $data = M_TagList::with(['jenis' => function($query) use ($_params){
            foreach($_params['query'] as $_param){
                $query->where($_param['column'],$_param['condition'],$_param['value']);
            }
            foreach($_params['skip'] as $_param){
                $query->whereNot($_param['column'],$_param['condition'],$_param['value']);
            }
        }, 'latestValue' => function($query) use($tgl_report){
            $query->whereDate('waktu_penarikan', $tgl_report);
        }])
        ->whereHas('jenis', function($query) use($_params) {
            foreach($_params['query'] as $_param){
                $query->where($_param['column'],$_param['condition'],$_param['value']);
            }
            foreach($_params['skip'] as $_param){
                $query->whereNot($_param['column'],$_param['condition'],$_param['value']);
            }
        })
        ->where('id_plant',$plant)->get();

        return $data;
    }

    static public function getDataShipment($plant, $tgl_report, $_params){
        $data = M_JenisShipment::with(['latestValue' => function($query) use($tgl_report){
                    $query->whereDate('shipment_date', $tgl_report);
                }])
                ->where('id_plant', $plant);

        foreach($_params['query'] as $_param){
            $data->where($_param['column'],$_param['condition'],$_param['value']);
        }
        foreach($_params['skip'] as $_param){
            $data->whereNot($_param['column'],$_param['condition'],$_param['value']);
        }

        return $data->get();
    }

    static public function summaryValue($data){
        $_sum = 0.00;
        foreach($data as $dt){
            if(!is_null($dt->latestValue)){
                if(!is_null($dt->latestValue->harpros_value))
                    $_sum += (double) $dt->latestValue->harpros_value;
                else if(!is_null($dt->latestValue->msdr_value))
                    $_sum += (double) $dt->latestValue->msdr_value;
                else
                    $_sum += (double) $dt->latestValue->tag_value;
            }
        }

        return $_sum;
    }
}
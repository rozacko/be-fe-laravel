<?php

namespace App\Imports;

use App\Models\SHEManPower;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class SheManPowerImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new SHEManPower([
            'bulan' => $row['bulan'],
            'tahun' => $row['tahun'],
            'nm_perusahaan' => $row['nama_perusahaan'],
            'jml_orang' => $row['jumlah_pekerja'],
            'status' => $row['status'],
            'keterangan' => $row['keterangan'],
        ]);
    }
}

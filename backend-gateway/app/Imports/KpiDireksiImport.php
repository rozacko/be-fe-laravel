<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Models\KpiDireksi;

class KpiDireksiImport implements ToCollection, WithHeadingRow
{

    private $tahun;
    private $bulan;

    public function __construct(int $tahun,int $bulan)
    {
        $this->tahun = $tahun;
        $this->bulan = $bulan;
    }
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        foreach($collection as $key => $row){

            KpiDireksi::updateOrCreate(
                ['kpi'=>$row['key_performance_indicator'],'kpi_group'=>$row['kpi_group'],'tahun'=>$this->tahun,'bulan'=>$this->bulan],
                [
                    'unit'=>(!is_null($row['unit']))?$row['unit']:null,
                    'polarisasi'=>(!is_null($row['polarisasi']))?$row['polarisasi']:0,
                    'target'=>(!is_null($row['target']))?str_replace(",",".",$row['target']):0,
                    'prognose'=>(!is_null($row['prognose']))?str_replace(",",".",$row['prognose']):0,
                    'realisasi'=>(!is_null($row['real']))?str_replace(",",".",$row['real']):0,
                    'persentase'=>(!is_null($row['capaian_kpi']))?str_replace(",",".",$row['capaian_kpi']):0
                ]
            );
        }
    }

    public function headingRow(): int
    {
        return 1;
    }
}

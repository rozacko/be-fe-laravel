<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToModel;
use App\Models\T_OverhaulIssues;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class OverhaulIssuesImport implements ToModel, WithHeadingRow
{
    protected $plant;
    protected $tahun;

    public function __construct($plant, $tahun)
    {
        $this->plant = $plant;
        $this->tahun = $tahun;

        T_OverhaulIssues::where('ovh_year',$tahun)->where('id_plant',$plant)->delete();
    }

    public function model(array $row)
    {
        return new T_OverhaulIssues([
            'id_plant' => $this->plant,
            'ovh_year' => $this->tahun,
            'issues' => $row['issues'],
            'budget' => $row['budget']
        ]);
    }
}

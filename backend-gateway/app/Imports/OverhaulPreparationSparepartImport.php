<?php

namespace App\Imports;

use App\Models\T_OverhaulSparePart;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class OverhaulPreparationSparepartImport implements ToModel, WithHeadingRow
{
    protected $id_plant;
    protected $tahun;

    public function __construct($id_plant, $tahun)
    {
        $this->id_plant = $id_plant;
        $this->tahun = $tahun;

        T_OverhaulSparePart::where('id_plant',$id_plant)->where('ovh_year',$tahun)->delete();
    }
    
    public function model(array $row)
    {
        $params = [
            'id_plant' => $this->id_plant,
            'ovh_year' => $this->tahun,
            'budget' => ucwords($row['budget']),
            'kode_material' => $row['no_stock'],
            'nama_material' => $row['deskripsi_material'],
            'nm_unit_kerja' => strtoupper($row['unit_kerja']),
            'total_price' => $row['harga_total'],
            'category' => $row['rutinnon_rutin']=='201' ? 'Rutin' : 'Non-Rutin',
            'status' => $row['status']
        ];

        if(!is_null($row['no_reservasi']) && $row['no_reservasi'] != '')
            $params['reservasi_no'] = $row['no_reservasi'];

        if(!is_null($row['no_pr']) && $row['no_pr'] != '')
            $params['pr_no'] = $row['no_pr'];

        if(!is_null($row['no_po']) && $row['no_po'] != '')
            $params['po_no'] = $row['no_po'];

        if(!is_null($row['tgl_delivery_po']) && $row['tgl_delivery_po'] != '')
            $params['po_nopo_delivery_date'] = \Carbon\Carbon::createFromFormat('d-m-Y',$row['tgl_delivery_po'])->format('Y-m-d');

        if(!is_null($row['catatan']) && $row['catatan'] != '')
            $params['notes'] = $row['catatan'];   

        return new T_OverhaulSparePart($params);
    }
}

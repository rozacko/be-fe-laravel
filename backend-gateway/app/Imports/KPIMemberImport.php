<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Models\LaporanKPIMember;

class KPIMemberImport implements ToCollection, WithHeadingRow
{

    private $tahun;

    public function __construct(int $tahun)
    {
        $this->tahun = $tahun;
    }
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        foreach($collection as $key => $row){
            if($row['target_poin'] == "#N/A" ||$row['target_poin'] == "#VALUE!" ){
                $target_poin = 0;
            }else{
                $target_poin = (!is_null($row['target_poin']))?str_replace(",",".",$row['target_poin']):0;
            }

            if($row['q1'] == "#N/A" ||$row['q1'] == "#VALUE!" ){
                $q1 = 0;
            }else{
                $q1 = (!is_null($row['q1']))?str_replace(",",".",$row['q1']):0;
            }

            if($row['q2'] == "#N/A" ||$row['q2'] == "#VALUE!" ){
                $q2 = 0;
            }else{
                $q2 = (!is_null($row['q2']))?str_replace(",",".",$row['q2']):0;
            }

            if($row['s1'] == "#N/A" ||$row['s1'] == "#VALUE!" ){
                $s1 = 0;
            }else{
                $s1 = (!is_null($row['s1']))?str_replace(",",".",$row['s1']):0;
            }
            if($row['q3'] == "#N/A" ||$row['q3'] == "#VALUE!" ){
                $q3 = 0;
            }else{
                $q3 = (!is_null($row['q3']))?str_replace(",",".",$row['q3']):0;
            }

            if($row['q4'] == "#N/A" ||$row['q4'] == "#VALUE!" ){
                $q4 = 0;
            }else{
                $q4 = (!is_null($row['q4']))?str_replace(",",".",$row['q4']):0;
            }

            if($row['s2'] == "#N/A" ||$row['s2'] == "#VALUE!" ){
                $s2 = 0;
            }else{
                $s2 = (!is_null($row['s2']))?str_replace(",",".",$row['s2']):0;
            }

            if($row['tahun'] == "#N/A" ||$row['tahun'] == "#VALUE!" ){
                $nilai_akhir = 0;
            }else{
                $nilai_akhir = (!is_null($row['tahun']))?str_replace(",",".",$row['tahun']):0;
            }

            LaporanKPIMember::updateOrCreate(
                ['tahun'=>$this->tahun,
                'no_pegawai'=>$row['nomor_pegawai'],
                'nama_pegawai'=>$row['nama_pegawai']
            ],
            [
                'jabatan'=>$row['jabatan'],
                'nama_organisasi'=>$row['nama_organisasi'],
                'unit_kerja'=>$row['unit_kerja'],
                'target_point'=>$target_poin,
                'q1'=>$q1,
                'q2'=>$q2,
                's1'=>$s1,
                'q3'=>$q3,
                'q4'=>$q4,
                's2'=>$s2,
                'nilai_akhir'=>$nilai_akhir,
            ]
            );
        }
    }

    public function headingRow(): int
    {
        return 4;
    }
}

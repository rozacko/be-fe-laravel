<?php

namespace App\Imports;

use App\Models\CapexItem;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Carbon\Carbon;

class CapexItemImport implements ToModel, WithHeadingRow
{

    public function __construct()
    {
    }

    public function model(array $row)
    {
        $params = [
            'no_project' => $row['no_project'],
            'no_wbs' => $row['no_wbs'],
            'nama_capex' => $row['nama_capex'],
            'tipe' => $row['tipe'],
            'jenis' => $row['jenis'],
            'nilai' => $row['nilai'],
            'inisiator_seksi' => $row['inisiator_seksi'],
            'inisiator_biro' => $row['inisiator_biro'],
            'inisiator_dept' => $row['inisiator_dept'],
            'realisasi_engineering' => $row['realisasi_engineering'],
            'realisasi_procurement' => $row['realisasi_procurement'],
            'realisasi_delivery' => $row['realisasi_delivery'],
            'realisasi_closing' => $row['realisasi_closing'],
            'progress_status' => $row['progress_status'],
            'keterangan' => $row['keterangan']

        ];

        return $params;
        die;

        return new CapexItem($params);
    }
}
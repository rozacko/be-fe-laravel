<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Models\T_OverhaulBudget;

class OverhaulBudgetImport implements ToModel, WithHeadingRow
{
   protected $plant;
   protected $tahun;

   public function __construct($plant, $tahun)
   {
        $this->plant = $plant;
        $this->tahun = $tahun;
   }
   public function model(array $row)
   {
        return new T_OverhaulBudget([
            "id_plant"=> $this->plant,
            "tahun"=> $this->tahun,
            "unit_kerja"=> $row['unit_kerja'],
            "budget_capex"=> $row['ajuan_budget_capex'],
            "budget_opex"=> $row['ajuan_budget_opex'],
        ]);
   }
}

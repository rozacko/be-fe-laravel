<?php

namespace App\Imports;

use App\Models\TInspeksiLingkungan;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterImport;
use Illuminate\Support\Facades\Cache;

class InspeksiLingkunganImport implements ToCollection, WithHeadingRow, WithChunkReading, WithEvents
{
    protected static $result;
    protected static $key;
    const STATUS_SUCCESS = "success";
    const STATUS_FAILED = "failed";

    public function __construct(string $key)
    {
        self::$key = $key;
    }

    public function mappingKey($row)
    {
        $mapRow=[];
        $mapRow['departemen'] = $row['departemen'];
        $mapRow['area'] = $row['detail_area'];
        $mapRow['equipment_no'] = $row['equipment_no_lokasi'];
        $mapRow['uraian_temuan'] = $row['uraian_temuan'];
        $mapRow['tanggal_inspeksi'] = $row['tanggal_inspeksi_ex_01_05_2023'];
        $mapRow['tanggal_closing'] = $row['tanggal_closing_ex_01_05_2023'];
        $mapRow['status'] = $row['status_open_close'];
        $mapRow['devisi_area'] = $row['devisi_area'];
        $mapRow['unit_kerja'] = $row['unit_kerja'];
        $mapRow['evaluasi'] = $row['evaluasi'];
        return $mapRow;
    }
    
    public function collection(Collection $rows)
    {
        $result = ["success" => [], "failed" => [], "all" => []];
        foreach ($rows as $key => $cellRow) {
            $row = $this->mappingKey($cellRow);

            $importResult = self::STATUS_SUCCESS;
            $message = "";
            $nomerBaris = $key + 1;

            if ($this->isMandatoryFieldNotEmpty($row)) {
                $excelTangalInspeksi = str_ireplace("/", "-", $row['tanggal_inspeksi']);
                $excelTangalClosing = str_ireplace("/", "-", $row['tanggal_closing']);
                $isValidTanggalInspeksi = $this->isValidFormatDate($excelTangalInspeksi);
                $isValidTanggalClosing = $this->isValidFormatDate($excelTangalClosing);
                if (!$isValidTanggalInspeksi || !$isValidTanggalClosing) {
                    $noteInput = "";
                    if (!$isValidTanggalInspeksi){
                        $noteInput = "Tanggal inspeksi ".$row['tanggal_inspeksi']." harus berformat d-m-y";
                    }
                    
                    if (!$isValidTanggalClosing){
                        $noteInput .= (strlen($noteInput) > 0)?" & ":"";
                        $noteInput .= "Tanggal closing ".$row['tanggal_closing']." harus berformat d-m-y";
                    }
                    $constMessage = "Data tanggal tidak valid,".$noteInput;

                    Log::info($constMessage, ["data" => $row]);
                    $importResult = self::STATUS_FAILED;
                    $message = $constMessage;
                    $result['failed'][] = ["baris" => $nomerBaris, "result" => $importResult, "message" => $message];
                    $result['all'][] = ["baris" => $nomerBaris, "result" => $importResult, "message" => $message];
                    continue;
                }

                $tanggalInspeksi = formatDate($excelTangalInspeksi);
                $tanggalClosing = formatDate($excelTangalClosing);

                TInspeksiLingkungan::updateOrCreate(
                    ['departemen' => $row['departemen'], 'tanggal_inspeksi' => $tanggalInspeksi, 'area' => $row['area'],'devisi_area'=>$row['devisi_area'],'unit_kerja' => $row['unit_kerja']],
                    [
                        'equipment_no' => $row['equipment_no'],
                        'uraian_temuan' => $row['uraian_temuan'],
                        'tanggal_closing' => $tanggalClosing,
                        'status' => $row['status'],
                        'evaluasi' =>$row['evaluasi'],
                    ]
                );
                $result['success'][] = ["baris" => $nomerBaris, "result" => $importResult, "message" => $message];
            } else {
                $constMessage = "Data tidak lengkap, silahkan lengkapi data terlebih dahulu";
                Log::info($constMessage, ["data" => $row]);
                $importResult = self::STATUS_FAILED;
                $message = $constMessage;
                $result['failed'][] = ["baris" => $nomerBaris, "result" => $importResult, "message" => $message];
                $result['all'][] = ["baris" => $nomerBaris, "result" => $importResult, "message" => $message];
                continue;
            }
            $result['all'][] = ["baris" => $nomerBaris, "result" => $importResult, "message" => $message];
        }
        self::$result = $result;
    }

    protected function isMandatoryFieldNotEmpty($row): bool
    {
        $keyData = ["departemen","tanggal_inspeksi","area","devisi_area","unit_kerja"];
        foreach ($keyData as $value) {
            if (array_key_exists($value, $row) && empty($row[$value])) {
                return false;
            }
        }
        return true;
    }

    public function headingRow(): int
    {
        return 1;
    }

    public function chunkSize(): int
    {
        return 100;
    }

    public function registerEvents(): array
    {
        return [
            // Array callable, refering to a static method.
            AfterImport::class => [self::class, 'afterImport'],

        ];
    }

    public static function afterImport(AfterImport $event)
    {
        Cache::set(self::$key, self::$result, now()->addMinutes(10));
        return self::$result;
    }

    protected function isValidFormatDate($date)
    {
        $date = trim($date, " ");
        $formatToCheck = ['d-m-Y', 'j-n-Y', 'd-n-Y', 'j-m-Y'];
        foreach ($formatToCheck as $valueFormat) {
            if (validateDate($date, $valueFormat)) {
                return true;
            }
        }
        return false;
    }

}

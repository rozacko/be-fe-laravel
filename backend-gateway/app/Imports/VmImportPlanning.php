<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Models\TPlanningScheduleKerja;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterImport;
use Illuminate\Support\Facades\Cache;

class VmImportPlanning implements ToCollection, WithHeadingRow, WithChunkReading, WithEvents
{
    protected static $result;
    protected static $key;
    const STATUS_SUCCESS = "success";
    const STATUS_FAILED = "failed";

    public function __construct(string $key)
    {
        self::$key = $key;
    }

    public function collection(Collection $rows)
    {
        $result = ["success" => [], "failed" => [], "all" => []];
        foreach ($rows as $key => $row) {
            $importResult = self::STATUS_SUCCESS;
            $message = "";
            $nomerBaris = $key + 1;

            if ($this->isMandatoryFieldNotEmpty($row)) {
                TPlanningScheduleKerja::updateOrCreate(
                    ['no_pegawai' => $row['nopeg'], 'tahun' => $row['tahun'], 'bulan' => $row['bulan']],
                    [
                        'nm_pegawai' => $row['nama'],
                        'spesifikasi' => $row['spesifikasi'],
                        'perusahaan' => $row['perusahaan'],
                        'tgl_1' => $row['1'],
                        'tgl_2' => $row['2'],
                        'tgl_3' => $row['3'],
                        'tgl_4' => $row['4'],
                        'tgl_5' => $row['5'],
                        'tgl_6' => $row['6'],
                        'tgl_7' => $row['7'],
                        'tgl_8' => $row['8'],
                        'tgl_9' => $row['9'],
                        'tgl_10' => $row['10'],
                        'tgl_11' => $row['11'],
                        'tgl_12' => $row['12'],
                        'tgl_13' => $row['13'],
                        'tgl_14' => $row['14'],
                        'tgl_15' => $row['15'],
                        'tgl_16' => $row['16'],
                        'tgl_17' => $row['17'],
                        'tgl_18' => $row['18'],
                        'tgl_19' => $row['19'],
                        'tgl_20' => $row['20'],
                        'tgl_21' => $row['21'],
                        'tgl_22' => $row['22'],
                        'tgl_23' => $row['23'],
                        'tgl_24' => $row['24'],
                        'tgl_25' => $row['25'],
                        'tgl_26' => $row['26'],
                        'tgl_27' => $row['27'],
                        'tgl_28' => $row['28'],
                        'tgl_29' => $row['29'],
                        'tgl_30' => $row['30'],
                        'tgl_31' => $row['31'],
                    ]
                );
                $result['success'][] = ["baris" => $nomerBaris, "result" => $importResult, "message" => $message];
            } else {
                $constMessage = "Data tidak lengkap, data nopeg, tahun, bulan tidak boleh kosong";
                Log::info($constMessage, ["data" => $row]);
                $importResult = self::STATUS_FAILED;
                $message = $constMessage;
                $result['failed'][] = ["baris" => $nomerBaris, "result" => $importResult, "message" => $message];
                $result['all'][] = ["baris" => $nomerBaris, "result" => $importResult, "message" => $message];
                continue;
            }
            $result['all'][] = ["baris" => $nomerBaris, "result" => $importResult, "message" => $message];
        }
        self::$result = $result;
    }

    protected function isMandatoryFieldNotEmpty($row): bool
    {
        return !empty($row['nopeg']) && !empty($row['tahun']) && !empty($row['bulan']);
    }


    public function headingRow(): int
    {
        return 1;
    }

    public function chunkSize(): int
    {
        return 100;
    }

    public function registerEvents(): array
    {
        return [
            // Array callable, refering to a static method.
            AfterImport::class => [self::class, 'afterImport'],

        ];
    }

    public static function afterImport(AfterImport $event)
    {
        Cache::set(self::$key, self::$result, now()->addMinutes(10));
        return self::$result;
    }
}

<?php

namespace App\Imports;

use App\Models\T_OverhaulActivity;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Facades\DB;
use App\Models\MArea;
use Carbon\Carbon;

class OverhaulActivityImport implements ToModel, WithHeadingRow
{
    protected $id_plant;
    
    public function __construct($id_plant)
    {
        $this->id_plant = $id_plant;
    }
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $area = MArea::where(DB::raw('upper(nm_area)'), trim(strtoupper($row['area'])))->first();
        return T_OverhaulActivity::updateOrCreate([ 
            'id_plant' => $this->id_plant,
            'id_area' => $area->id,
            'activity_name' => $row['aktivitas'],
            'order_no' => $row['no_order'],
        ],
        [
            //
            'id_plant' => $this->id_plant,
            'id_area' => $area->id,
            'activity_name' => $row['aktivitas'],
            'activity_category' => $row['kategori_aktivitas'],
            'unit_kerja' => strtoupper($row['seksi_peminta']),
            'order_no' => $row['no_order'],
            'jenis_pekerjaan' => $row['rutinnon_rutin'],
            'judul_kontrak' => $row['judul_kontrak_tahunan'],
            'total_manpower' => $row['total_mp'],
            'total_consumable' => $row['total_consumable'],
            'total_tool' => $row['total_tools'],
            'budget_category' => ucwords($row['budget']),
            'start_date' => Carbon::createFromFormat('d-m-Y',$row['tanggal_mulai'])->format('Y-m-d'),
            'end_date' => Carbon::createFromFormat('d-m-Y',$row['tanggal_selesai'])->format('Y-m-d'),
        ]);
    }
}

<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Models\TPresensiPegawai;
use Illuminate\Support\Facades\Cache;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterImport;

class VmImportRealisasi implements ToCollection, WithHeadingRow, WithChunkReading, WithEvents
{
    protected static $result;
    protected static $key;
    const STATUS_SUCCESS = "success";
    const STATUS_FAILED = "failed";

    public function __construct(string $key)
    {
        self::$key = $key;
    }

    public function collection(Collection $rows)
    {
        $result = ["success" => [], "failed" => [], "all" => []];
        foreach ($rows as $key => $row) {
            $importResult = self::STATUS_SUCCESS;
            $message = "";
            $nomerBaris = $key + 1;

            $excelTangal = str_ireplace("/", "-", $row['tanggal']);
            if ($this->isMandatoryFieldNotEmpty($row) && $this->isValidFormatDate($excelTangal)) {
                $tanggal = formatDate($excelTangal);
                $time = "";
                $reformatTime = str_ireplace("/", "-", $row['time']);
                $reformatTime = str_ireplace("  ", " ", $reformatTime);

                if ($this->isValidFormatDateTime($reformatTime)) {
                    $time = formatDate($reformatTime, 'H:i:s');
                } else {
                    Log::info("Format time not valid", ["data" => $row]);
                    $importResult = self::STATUS_FAILED;
                    $message = "Format time tidak valid, format yang benar d/m/Y H:i" . " data yang di input " . $row['time'];
                    $result['failed'][] = ["baris" => $nomerBaris, "result" => $importResult, "message" => $message];
                    $result['all'][] = ["baris" => $nomerBaris, "result" => $importResult, "message" => $message];
                    continue;
                }
                TPresensiPegawai::updateOrCreate(
                    ['no_pegawai' => $row['nopeg'], 'tanggal' => $tanggal, 'event' => $row['event']],
                    [
                        'nama_pegawai' => $row['nama'],
                        'time' => $time,
                        'state' => $row['state'],
                        'new_state' => $row['new_state'],
                        'exception' => $row['exception'],
                        'preference' => $row['preference'],
                    ]
                );
                $result['success'][] = ["baris" => $nomerBaris, "result" => $importResult, "message" => $message];
            } else {
                Log::info("Format tanggal not valid", ["data" => $row]);
                $importResult = self::STATUS_FAILED;
                $message = "Format tanggal tidak valid, format yang benar d/m/Y" . " data yang di input " . $row['tanggal'];
                $result['failed'][] = ["baris" => $nomerBaris, "result" => $importResult, "message" => $message];
            }
            $result['all'][] = ["baris" => $nomerBaris, "result" => $importResult, "message" => $message];
        }
        self::$result = $result;
    }

    protected function isMandatoryFieldNotEmpty($row): bool
    {
        return !empty($row['nopeg']) && !empty($row['tanggal']) && !empty($row['event']);
    }

    protected function isValidFormatDate($date)
    {
        $date = trim($date, " ");
        $formatToCheck = ['d-m-Y', 'j-n-Y', 'd-n-Y', 'j-m-Y'];
        foreach ($formatToCheck as $valueFormat) {
            if (validateDate($date, $valueFormat)) {
                return true;
            }
        }
        return false;
    }

    protected function isValidFormatDateTime($datetime)
    {
        $datetime = trim($datetime, " ");
        $arrDatetime = explode(" ", $datetime);
        $date = $arrDatetime[0];
        $time = $arrDatetime[1];

        $formatDateToCheck = ['d-m-Y', 'j-m-Y', 'd-n-Y', 'j-n-Y'];
        $formatTimeToCheck = ['H:i', 'G:i'];
        foreach ($formatDateToCheck as $valueFormat) {
            if (validateDate($date, $valueFormat)) {
                foreach ($formatTimeToCheck as $valueFormatTime) {
                    if (validateDate($time, $valueFormatTime)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public function headingRow(): int
    {
        return 1;
    }

    public function chunkSize(): int
    {
        return 100;
    }

    public function registerEvents(): array
    {
        return [
            // Array callable, refering to a static method.
            AfterImport::class => [self::class, 'afterImport'],

        ];
    }

    public static function afterImport(AfterImport $event)
    {
        Cache::set(self::$key, self::$result, now()->addMinutes(10));
        return self::$result;
    }
}

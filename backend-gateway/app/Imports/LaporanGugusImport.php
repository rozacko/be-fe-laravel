<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Models\LaporanGugus;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;

class LaporanGugusImport implements ToCollection, WithHeadingRow,WithCalculatedFormulas
{
    private $bulan;
    private $tahun;

    public function __construct(string $bulan,int $tahun)
    {
        $this->bulan = $bulan;
        $this->tahun = $tahun;
    }
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        foreach($collection as $key => $row){
            if(!is_null($row['1'])){

            if($key==0){
                $gugus = str_replace(",",".",$row['17']);
                $fasilitator = str_replace(",",".",$row['18']);
            }
            if(!is_null($row['17'])){
                $gugus = str_replace(",",".",$row['17']);
            }
            if(!is_null($row['18'])){
                $fasilitator = str_replace(",",".",$row['18']);
            }

            LaporanGugus::updateOrCreate(
                ['bulan'=>$this->bulan,'tahun'=>$this->tahun,'fasilitator'=>$row['1'],'gugus'=>$row['2'],'sga'=>$row['3']],
                [
                    'commitment_management'=>(!is_null($row['4']))?$row['4']:0,
                    'papan_control'=>(!is_null($row['5']))?$row['5']:0,
                    'she'=>(!is_null($row['6']))?$row['6']:0,
                    '5r'=>(!is_null($row['r1']))?str_replace(",",".",$row['r1']):0,
                    'autonomous_maintenance'=>(!is_null($row['12']))?str_replace(",",".",$row['12']):0,
                    'planed_maintenance'=>(!is_null($row['13']))?str_replace(",",".",$row['13']):0,
                    'focused_improvement'=>(!is_null($row['14']))?str_replace(",",".",$row['14']):0,
                    'target'=>(!is_null($row['15']))?str_replace(",",".",$row['15']):0,
                    'nilai_SGA'=>(!is_null($row['16']))?str_replace(",",".",$row['16']):0,
                    'nilai_gugus'=>$gugus,
                    'nilai_fasilitator'=>$fasilitator,
                    "r2"=>(!is_null($row["r2"]))?str_replace(",",".",$row["r2"]):0,
                    'r3'=>(!is_null($row['r3']))?str_replace(",",".",$row['r3']):0,
                    'r4'=>(!is_null($row['r4']))?str_replace(",",".",$row['r4']):0,
                    'r5'=>(!is_null($row['r5']))?str_replace(",",".",$row['r5']):0,
                ]
            );
        }
        }
    }

    public function headingRow(): int
    {
        return 5;
    }
}

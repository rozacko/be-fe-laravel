<?php

namespace App\Imports;

use App\Models\t_realisasi_capex;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Carbon\Carbon;

class RealisasiCapexImport implements ToModel, WithHeadingRow
{

    public function __construct()
    {
    }

    public function model(array $row)
    {
        $params = [
            'no_project' => $row['no_project'],
            'no_wbs' => $row['no_wbs'],
            'item_capex' => $row['item_capex'],
            'unit_kerja' => $row['unit_kerja'],
            'pm' => $row['pm'],
            'tipe' => $row['tipe'],
            'kai' => $row['kai'],
            'op_dev' => $row['op_dev'],
            'nilai_investasi' => $row['nilai_investasi'],
            'committed' => $row['committed'],
            'pelampauan' => $row['pelampauan'],
            'real_spending' => $row['real_spending'],
            'network' => $row['network'],
            'reservasi' => $row['reservasi'],
            'no_pr' => $row['no_pr'],
            'nilai_pr' => $row['nilai_pr'],
            'no_po' => $row['no_po'],
            'doc_date' => $row['doc_date'],
            'del_date' => $row['del_date'],
            'nilai_po' => $row['nilai_po'],
            // 'tgl_gr' => Carbon::createFromFormat('d-m-Y',$row['tgl_gr'])->format('Y-m-d'),
            // 'spending_gr' => $row['spending_gr'],
            'progres_fisik' => $row['progres_fisik'],
            'progres_report_terakhir' => $row['progres_report_terakhir'],
            'nilai_auc' => $row['nilai_auc'],
            'status_bast' => $row['status_bast'],
            'status_wbs' => $row['status_wbs'],
            'keterangan' => $row['keterangan']
        ];

        return $params;die;

        return new t_realisasi_capex($params);
    }
}
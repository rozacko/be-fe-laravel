<?php

namespace App\Imports;

use App\Models\T_OverhaulActivityProgress;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Models\T_OverhaulActivity;
use App\Models\T_OverhaulPeriode;
use App\Models\MArea;
use Illuminate\Support\Facades\DB;

class OverhaulProgressSheetImport implements ToModel, WithHeadingRow
{
    protected $id_plant;

    public function __construct($id_plant)
    {
        $this->id_plant = $id_plant;
    }
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $periode = T_OverhaulPeriode::where('id_plant',$this->id_plant)->where('status','Open')->first();
        $area = MArea::where(DB::raw('lower(nm_area)'),strtolower($row['area']))->first();
        $activity = T_OverhaulActivity::where('id_plant', $this->id_plant)
        ->where(function($query) use ($periode){
            $query->whereBetween('start_date',[$periode->start_date, $periode->end_date])
            ->orWhereBetween('start_date',[$periode->start_date, $periode->end_date]);
        })
        ->where('id_area',$area->id)
        ->where(DB::raw('lower(activity_name)'),trim(strtolower($row['activity'])))
        ->where(DB::raw('lower(activity_category)'),strtolower($row['category']))->first();
        if(is_null($activity)){
            throw new \Exception("Activity ".$row['activity']." di area ".$row['area']." pada periode ini tidak ditemukan!");            
        }
        return new T_OverhaulActivityProgress([
            //
            'id_activity' => $activity->id,
            'progress_date' => \Carbon\Carbon::createFromFormat('d-m-Y',$row['tanggal_progress'])->format('Y-m-d'),
            'plan_cumulative' => $row['kumulatif_rencana'],
            'real_cumulative' => $row['kumulatif_realisasi'],
        ]);
    }
}

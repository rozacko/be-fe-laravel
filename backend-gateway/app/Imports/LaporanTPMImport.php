<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Models\LaporanTPM;

class LaporanTPMImport implements ToCollection, WithHeadingRow
{
    private $tahun;

    public function __construct(int $tahun)
    {
        $this->tahun = $tahun;
    }
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {

        foreach($collection as $key => $row){
            if($key==0){
                $deskripsi = $row['deskripsi'];
            }

            if(!is_null($row['deskripsi'])){
                $deskripsi = $row['deskripsi'];
            }
            LaporanTPM::updateOrCreate(
                ['deskripsi'=>$deskripsi,'parameter'=>$row[2],'tahun'=>$this->tahun],
                [
                    'jan'=>(!is_null($row['jan']))?str_replace(",",".",$row['jan']):0,
                    'feb'=>(!is_null($row['feb']))?str_replace(",",".",$row['feb']):0,
                    'mar'=>(!is_null($row['mar']))?str_replace(",",".",$row['mar']):0,
                    'q1'=>(!is_null($row['q1']))?str_replace(",",".",$row['q1']):0,
                    'apr'=>(!is_null($row['apr']))?str_replace(",",".",$row['apr']):0,
                    'mei'=>(!is_null($row['mei']))?str_replace(",",".",$row['mei']):0,
                    'jun'=>(!is_null($row['jun']))?str_replace(",",".",$row['jun']):0,
                    'q2'=>(!is_null($row['q2']))?str_replace(",",".",$row['q2']):0,
                    's1'=>(!is_null($row['s1']))?str_replace(",",".",$row['s1']):0,
                    'jul'=>(!is_null($row['jul']))?str_replace(",",".",$row['jul']):0,
                    'agust'=>(!is_null($row['agust']))?str_replace(",",".",$row['agust']):0,
                    'sept'=>(!is_null($row['sept']))?str_replace(",",".",$row['sept']):0,
                    'q3'=>(!is_null($row['q3']))?str_replace(",",".",$row['q3']):0,
                    'okt'=>(!is_null($row['okt']))?str_replace(",",".",$row['okt']):0,
                    'nov'=>(!is_null($row['nov']))?str_replace(",",".",$row['nov']):0,
                    'des'=>(!is_null($row['des']))?str_replace(",",".",$row['des']):0,
                    'q4'=>(!is_null($row['q4']))?str_replace(",",".",$row['q4']):0,
                    's2'=>(!is_null($row['s2']))?str_replace(",",".",$row['s2']):0,
                    'nilai_akhir'=>(!is_null($row[$this->tahun]))?str_replace(",",".",$row[$this->tahun]):0
                ]
            );
        }
    }

    public function headingRow(): int
    {
        return 3;
    }
}

<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class OverhaulProgressImport implements WithMultipleSheets
{
   protected $id_plant;

   public function __construct($id_plant)
   {
        $this->id_plant = $id_plant;
   }
   public function sheets(): array
   {
        return [
            new OverhaulProgressSheetImport($this->id_plant)
        ];
   }
}

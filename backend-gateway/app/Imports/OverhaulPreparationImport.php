<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class OverhaulPreparationImport implements WithMultipleSheets
{
    protected $plant;
    protected $tahun;
    
    public function __construct($plant, $tahun)
    {
        $this->plant = $plant;
        $this->tahun = $tahun;
    }
    public function sheets(): array
    {
        return [
            'Data Spareparts OVH' => new OverhaulPreparationSparepartImport($this->plant, $this->tahun),
            'Data Aktivitas OVH' => new OverhaulActivityImport($this->plant),
            'Data Issue OVH' => new OverhaulIssuesImport($this->plant, $this->tahun)
        ];
    }
}

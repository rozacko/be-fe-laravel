<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

use App\Models\MEqInspection;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ItemInspectionPlantTemplate implements FromCollection, WithHeadings, ShouldAutoSize, WithStyles
{
    protected $plant;
    protected $area;

    public function __construct($area)
    {
        // $this->plant = $plant;
        $this->area = $area;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $collection = new Collection();

        $areas = MEqInspection::select(['m_eq_inspection.*','m_area.nm_area'])->where('area_id',$this->area)
                    ->join('m_area','m_area.id','=','m_eq_inspection.area_id')
                    ->get();

        foreach ($areas as $key => $value) {
            $collection->push([
                'kode_equipment' => $value->kd_equipment,
                'desc_equipment' => $value->nm_equipment,
                'id_area' => $this->area,
                'nm_area' => $value->nm_area,
                'id_kondisi' => '1',
                'remark' => '-',
                'create_date' => date('Y-m-d'),
                'last_condition' => '',
                'last_inspection' => date('Y-m-d'),
                'last_remark' => '',
            ]);
        }
        
        return $collection;
    }

    public function headings(): array
    {
        return ['kode_equipment','desc_equipment','id_area','nm_area','id_kondisi','remark','create_date','last_condition','last_inspection','last_remark'];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1    => ['font' => ['bold' => true]],
        ];
    }
}


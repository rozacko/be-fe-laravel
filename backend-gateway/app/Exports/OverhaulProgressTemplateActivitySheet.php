<?php

namespace App\Exports;

use App\Models\MPlant;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use App\Models\T_OverhaulActivity;
use App\Models\T_OverhaulPeriode;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithDefaultStyles;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Style\Style;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class OverhaulProgressTemplateActivitySheet implements FromCollection, WithHeadings, WithTitle, WithStyles, ShouldAutoSize, WithDefaultStyles
{
    protected $id_plant;

    public function __construct($id_plant)
    {
        $this->id_plant = $id_plant;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        //
        $periode = T_OverhaulPeriode::where('id_plant', $this->id_plant)->where('status','Open')->first();
        $data = T_OverhaulActivity::select('id_area','activity_name','activity_category','start_date','end_date')
                    ->where('id_plant', $this->id_plant)
                    ->where(function($query) use ($periode){
                        $query->whereBetween('start_date',[$periode->start_date, $periode->end_date])
                        ->orWhereBetween('start_date',[$periode->start_date, $periode->end_date]);
                    })->get();
        $collection = new Collection();
        foreach($data as $dt){
            $collection->push([
                'area' => $dt->area->nm_area,
                'activity' => $dt->activity_name,
                'category' => $dt->activity_category,
                'start_date' => Carbon::parse($dt->start_date)->format('d-m-Y'),
                'end_date' => Carbon::parse($dt->end_date)->format('d-m-Y'),
            ]);
        }
        return $collection;
    }

    public function title(): string
    {
        $plant = MPlant::find($this->id_plant);
        return "Activity On Going ".$plant->nm_plant;
    }

    public function headings(): array
    {
        return ['Area', 'Activity Name', 'Category', 'Start Date', 'End Date'];
    }

    public function styles(Worksheet $sheet)
    {
        $sheet->getStyle("A1:E1")->getFont()->setBold(true);
    }

    public function defaultStyles(Style $defaultStyle)
    {
        // return $defaultStyle->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        return [
            'borders' => [
                'allborders' => [
                    'style' => 'thin',
                    'color' => ['rgb' => 'DDDDDD']
                ]
            ]
        ];
    }
}

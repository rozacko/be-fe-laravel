<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class OverhaulProgressTemplate implements WithMultipleSheets
{
    protected $id_plant;

    public function __construct($id_plant)
    {
        $this->id_plant = $id_plant;
    }

    public function sheets(): array
    {
        $sheet = [];

        $sheet[] = new OverhaulProgressTemplateProgressSheet($this->id_plant);
        $sheet[] = new OverhaulProgressTemplateActivitySheet($this->id_plant);

        return $sheet;
    }
}

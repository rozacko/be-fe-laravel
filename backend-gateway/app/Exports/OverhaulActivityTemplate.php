<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use App\Models\T_OverhaulActivity;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class OverhaulActivityTemplate implements FromCollection, WithHeadings, ShouldAutoSize, WithStyles
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        //
        $collection = new Collection();
        $collection->push([
            'area' => 'CRUSHER','activity' => 'Perbaikan Clay Crusher','category'=>'Major','start'=>date('d-m-Y'),'end'=>date('d-m-Y',  strtotime("+25 day")),'unit_kerja' => 'BENGLIS'
        ]);
        $collection->push([
            'area' => 'RAWMILL','activity' => 'Ganti Bearing','category'=>'Minor','start'=>date('d-m-Y'),'end'=>date('d-m-Y',  strtotime("+25 day")),'unit_kerja' => 'PMRM'
        ]);
        return $collection;
    }

    public function headings(): array
    {
        return ['Area','Activity Name','Category','Start Date','End Date','Unit'];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1    => ['font' => ['bold' => true]],
        ];
    }
}

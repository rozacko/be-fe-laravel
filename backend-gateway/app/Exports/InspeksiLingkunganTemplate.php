<?php

namespace App\Exports;

use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class InspeksiLingkunganTemplate implements WithHeadings, WithStyles, ShouldAutoSize
{
    public function headings(): array
    {
        return ['No.', 'DEPARTEMEN', 'DETAIL AREA', 'EQUIPMENT NO / LOKASI', 'URAIAN TEMUAN', 'TANGGAL INSPEKSI (ex: 01-05-2023)', 'TANGGAL CLOSING (ex: 01-05-2023)', 'STATUS (open / close)','DEVISI AREA','UNIT KERJA', 'EVALUASI'];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1    => ['font' => ['bold' => true]],
        ];
    }
}

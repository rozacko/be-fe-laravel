<?php

namespace App\Exports;

use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class SheUnsafeActionTemplate implements FromCollection, WithHeadings, WithStyles, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        //
        $collection = new Collection();
        $collection->push([
            "1","PT. BERDIKARI PUTRA PERKASA","1 PEKERJA TIDAK MEMAKAI FULL BODY HARNES","SEBELAH TIMUR 341 EP1","2023-02-27","1","","TERJATUH","MENGGUNAKAN APD YANG BENAR SESUAI STANDART K3","","CLOSE"
        ]);
        return $collection;
    }

    public function headings(): array
    {
        return ['No.','CV/PT/Unit Kerja','Uraian Temuan','Lokasi Kejadian','Tanggal Temuan','Jumlah Temuan','Pengawas','Identifikasi Bahaya','Tindakan','Keterangan','Status'];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1    => ['font' => ['bold' => true]],
        ]; 
    }
}

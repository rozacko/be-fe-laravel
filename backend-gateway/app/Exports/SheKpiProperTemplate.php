<?php

namespace App\Exports;

use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class SheKpiProperTemplate implements FromCollection, WithHeadings, WithStyles, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        //
        $collection = new Collection();
        $collection->push([
            "1","Produksi Terak","Ton"

        ]);
        $collection->push([
            "2","Produksi Semen","Ton"
        ]);
        $collection->push([
            "3","Indeks Clinker",""
        ]);
        $collection->push([
            "4","Pemakaian bahan baku alternatif","%"
        ]);
        $collection->push([
            "5","Pemakaian energi listrik spesifik","kWh/ton semen"
        ]);
        $collection->push([
            "6","Pemakaian energi panas spesifik","kkal/kg clinker"
        ]);
        $collection->push([
            "7","Pengurangan limbah B3 internal","%"
        ]);
        $collection->push([
            "8","Pemakaian bahan bakar alternatif (TSR)","%"
        ]);
        $collection->push([
            "9","Pengurangan sampah/ limbah Non B3","%"
        ]);
        $collection->push([
            "10","Tingkat produk reject (zak semen)","%"
        ]);
        $collection->push([
            "11","Indeks emisi gas CO2","kgCO2/ton semen"
        ]);
        $collection->push([
            "12","Indeks pemakaian air","M3/ton semen"
        ]);
        $collection->push([
            "13","Pemakaian air permukaan (recycle & air hujan)","%"
        ]);
        $collection->push([
            "14","Pemakaian air sumur","%"
        ]);
        $collection->push([
            "15","OEE","%"
        ]);
        return $collection;
    }

    public function headings(): array
    {
        return ['No.','KPI/Kebijakan Perusahaan','Satuan','Target','Realisasi','%KPI'];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1    => ['font' => ['bold' => true]],
        ]; 
    }
}

<?php

namespace App\Exports;

use App\Models\SHEMFireAlarm;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithDefaultStyles;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Style\Style;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class SheFirealarmBulananSheet implements FromCollection, WithHeadings, WithTitle, WithStyles, ShouldAutoSize, WithDefaultStyles
{
    public function __construct()
    {
        
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $data = SHEMFireAlarm::where('tipe','bulanan')->orderBy('id','asc')->get();
        $collection = new Collection();
        foreach($data as $dt){
            $collection->push([
                'id' => $dt->id,                
                'parameter' => $dt->parameter,              
            ]);
        }
        return $collection;
    }

    public function title(): string
    {        
        return "Bulanan";
    }

    public function headings(): array
    {
        return ['ID', 'Parameter', 'CCR 1 & 2', 'CCR 1 & 2 (STATUS)','CCR 1 & 2 (KETERANGAN)','', 'CCR 3 & 4','CCR 3 & 4 (STATUS)','CCR 3 & 4 (KETERANGAN)'];
    }

    public function styles(Worksheet $sheet)
    {
        $sheet->getStyle("A1:I1")->getFont()->setBold(true);
        $sheet->protectCells('A1:B20', 'SHEGHOPO');
    }

    public function defaultStyles(Style $defaultStyle)
    {
        // return $defaultStyle->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        return [
            'borders' => [
                'allborders' => [
                    'style' => 'thin',
                    'color' => ['rgb' => 'DDDDDD']
                ]
            ]
        ];
    }
}

<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class SheFirealarmTemplate implements WithMultipleSheets
{
    public function __construct()
    {        
    }

    public function sheets(): array
    {
        $sheet = [];

        $sheet[] = new SheFirealarmMingguanSheet();
        $sheet[] = new SheFirealarmBulananSheet();

        return $sheet;
    }
}

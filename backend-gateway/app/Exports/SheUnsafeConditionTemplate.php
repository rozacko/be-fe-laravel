<?php

namespace App\Exports;

use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class SheUnsafeConditionTemplate implements FromCollection, WithHeadings, WithStyles, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        //
        $collection = new Collection();
        $collection->push([
            "1","Raw Mill","RAW MILL & PRODUCT TRANSPORT TUBAN 2","342 BI1","DUST BIN","BIBIR COR LANTAI 2 REJECT DUST BIN KEROPOS DAN RAWAN","PERLU REKONDIS","KONSTRUKSI MESIN","RKC2","","","2023-02-27","2023-03-27","v","v","","OPEN",""
        ]);
        return $collection;
    }

    public function headings(): array
    {
        return ['No.','Area','TPM / SGA','No. Equipment','Lokasi','Uraian Temuan','Saran Tindak Lanjut','PIC','Unit Kerja','ID Trans MSO','No. Notifikasi','Tanggal Temuan','Tanggal Target','Pengecekan 1','Pengecekan 2','Tanggal Closing','Status','Keterangan'];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1    => ['font' => ['bold' => true]],
        ]; 
    }
}

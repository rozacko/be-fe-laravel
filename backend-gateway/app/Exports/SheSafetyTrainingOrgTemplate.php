<?php

namespace App\Exports;

use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class SheSafetyTrainingOrgTemplate implements FromCollection, WithHeadings, WithStyles, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        //
        $collection = new Collection();
        $collection->push([
            "1","xxxx","Rozacko","Produksi","xxx","IV","5"

        ]);
        $collection->push([
            "2","aaaa","Rozacki","Maintenance","yyy","II","2"

        ]);
        $collection->push([
            "3","yyyy","Rozacku","Lingkungan","aaa","III","3"
        ]);
        return $collection;
    }

    public function headings(): array
    {
        return ['No.','No Pegawai','Nama Pegawai','Departement','Unit Kerja','Band','Jam Pelatihan'];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1    => ['font' => ['bold' => true]],
        ]; 
    }
}

<?php

namespace App\Exports;

use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithTitle;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use App\Models\T_OverhaulPeriode;
use App\Models\T_OverhaulActivity;

class OverhaulProgressTemplateProgressSheet implements FromCollection, WithHeadings, WithTitle, WithStyles, ShouldAutoSize
{
    protected $id_plant;

    public function __construct($id_plant)
    {
        $this->id_plant = $id_plant;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        //
        $periode = T_OverhaulPeriode::where('id_plant', $this->id_plant)->where('status','Open')->first();
        $data = T_OverhaulActivity::select('id_area','activity_name','activity_category')
                    ->where('id_plant', $this->id_plant)
                    ->where(function($query) use ($periode){
                        $query->whereBetween('start_date',[$periode->start_date, $periode->end_date])
                        ->orWhereBetween('start_date',[$periode->start_date, $periode->end_date]);
                    })->get();
        $collection = new Collection();
        foreach($data as $dt){
            $collection->push([
                 $dt->area->nm_area, $dt->activity_name, $dt->activity_category, date('d-m-Y'),'0.00','0.00'
            ]);
        }
        return $collection;
    }

    public function headings(): array
    {
        return ['Area', 'Activity','Category', 'Tanggal Progress',' % Kumulatif Rencana',' % Kumulatif Realisasi'];
    }

    public function title(): string
    {
        return 'Activity Progress';
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1    => ['font' => ['bold' => true]],
        ];
    }
}

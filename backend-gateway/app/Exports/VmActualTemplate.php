<?php

namespace App\Exports;

use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class VmActualTemplate implements WithHeadings, WithStyles, ShouldAutoSize
{
    public function headings(): array
    {
        return ['No.', 'NOPEG', 'NAMA', 'TIME', 'STATE', 'NEW STATE', 'EXCEPTION', 'PREFERENCE', 'TANGGAL', 'EVENT'];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1    => ['font' => ['bold' => true]],
        ];
    }
}

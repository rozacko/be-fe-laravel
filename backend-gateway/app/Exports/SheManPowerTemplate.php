<?php

namespace App\Exports;

use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class SheManPowerTemplate implements FromCollection, WithHeadings, WithStyles, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        //
        $collection = new Collection();
        $collection->push([
            "1","PT. SEMEN INDONESIA LOGISTIK","2","ADMIN","PROJECT"

        ]);
        $collection->push([
            "2","PT. INTI KARYA JAYA PERKASA","1","RETENSI DUCT SP4","PROJECT"

        ]);
        $collection->push([
            "3","PT. MANUNGGAL JAYA SAKTI","2","STAFF ADMIN","PROJECT"
        ]);
        return $collection;
    }

    public function headings(): array
    {
        return ['No.','Nama Perusahaan','Jumlah Pekerja','Status','Keterangan'];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1    => ['font' => ['bold' => true]],
        ]; 
    }
}

<?php

namespace App\Exports;

use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class SheAparTemplate implements FromCollection, WithHeadings, WithStyles, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        //
        $collection = new Collection();
        $collection->push([
            "1","Kantor Crusher","OPCR","F4","Lobby pintu depan","F4","K","7","V","V","V","4","07","2019","BAIK",""
        ]);
        return $collection;
    }

    public function headings(): array
    {
        return ['No.','AREA','UNIT KERJA','JENIS APAR','LOKASI','MAP BARU','CLAMP','PRESS (Bar)','HOSE','SEGEL','SAPOT','BERAT (Kg)','BULAN M.AKTIF','TAHUN M.AKTIF','STATUS','KETERANGAN'];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1    => ['font' => ['bold' => true]],
        ]; 
    }
}

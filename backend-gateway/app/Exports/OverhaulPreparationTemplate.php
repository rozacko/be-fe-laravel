<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class OverhaulPreparationTemplate implements WithMultipleSheets
{
   public function sheets(): array
   {
        $sheet = [];
        $sheet[] = new OverhaulPreparationSparepartTemplate();
        $sheet[] = new OverhaulPreparationBiayaTemplate();
        $sheet[] = new OverhaulIssuesTemplate();

        return $sheet;
   }
}

<?php

namespace App\Exports;

use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithTitle;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class OverhaulPreparationBiayaTemplate implements FromCollection, WithHeadings, ShouldAutoSize, WithStyles, WithTitle
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        //
        $collection = new Collection();
        $collection->push([
            'KILN','12345','Bongkar Pasang Bearing','Rutin','Minor','Opex','PMKC3-4','PERBAIKAN MEKANIKAL AREA COOLER OVERHAUL PABRIK TUBAN','10-11-2022','30-11-2022','40000000','25000000','10000000'
        ]);
        $collection->push([
            'RAWMILL','67890','PASANG AKSES 344DA6','Non-Rutin','Major','Capex','RKC-4','PERBAIKAN MEKANIKAL ACCESORIES','13-12-2022','10-01-2023','30000000','15000000','9000000'
        ]);
        return $collection;
    }

    public function title(): string
    {
        return "Data Aktivitas OVH";
    }

    public function headings(): array
    {
        return ['Area','No. Order','Aktivitas','Rutin/Non-Rutin','Kategori Aktivitas','Budget','Seksi Peminta','Judul Kontrak Tahunan','Tanggal Mulai','Tanggal Selesai','Total MP','Total Consumable','Total Tools'];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1    => ['font' => ['bold' => true]],
        ]; 
    }
}

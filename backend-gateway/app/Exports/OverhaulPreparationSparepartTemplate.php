<?php

namespace App\Exports;

use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithTitle;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class OverhaulPreparationSparepartTemplate implements FromCollection, WithHeadings, WithStyles, ShouldAutoSize, WithTitle
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        //
        $collection = new Collection();
        $collection->push([
            "626-204508","LADDER,STRAIGHT,COVER TRAY:600MM","203","Capex", "2000000", "PR","3822630","2100029728","","","BENGLIS","perlu percepatan"

        ]);
        $collection->push([
           "317-200448","PLATE, AIR DISTRIBUTION;592MMX232MMX40MM","203","Capex", "10000000", "PO","3701648","","6010022651","21-11-2022","PMKC","ikut jasa fabrikasi"

        ]);
        $collection->push([
            "602-200711","CARBON BRUSH:50MMX32MMX25MM;MR7;CE","201","Opex", "1000000", "RESERVASI","3861710","","","","BM","perlu percepatan"
        ]);
        return $collection;
    }

    public function title(): string
    {
        return "Data Spareparts OVH";
    }

    public function headings(): array
    {
        return ['No. Stock','Deskripsi Material','Rutin/Non-Rutin','Budget', 'Harga Total','Status','No. Reservasi','No. PR','No. PO','Tgl Delivery PO','Unit Kerja','Catatan'];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1    => ['font' => ['bold' => true]],
        ]; 
    }
}

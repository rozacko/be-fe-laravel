<?php

namespace App\Exports;

use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class SheHydrantTemplate implements FromCollection, WithHeadings, WithStyles, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        //
        $collection = new Collection();
        $collection->push([
            '1','TB.1/2','Depan kantor PMKC 1 - 2 ','1','1','V','','v','V','V','','V','V','','v','V','v','v','v','v','','7'
        ]);
        return $collection;
    }

    public function headings(): array
    {
        return ['No','MAIN PUMP','LOKASI','NO PILAR','CEK','Body Pilar','Ket. Pilar','Valve Kopling (kiri)','Valve Kopling (atas)','Valve Kopling (kanan)','Ket. Valve Kopling','Copling (kiri)','Copling (kanan)','Ket. Copling','Tutup Kopling (kiri)','Tutup Kopling (kanan)','Box (Casing)','Box (Hose)','Box (Nozle)','Box (kunci)','Ket. Box','Press (bar)'];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1    => ['font' => ['bold' => true]],
        ]; 
    }
}

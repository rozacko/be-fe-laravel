<?php

namespace App\Exports;

use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithTitle;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class OverhaulIssuesTemplate implements FromCollection, WithTitle, WithHeadings, ShouldAutoSize, WithStyles
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        //
        $collection = new Collection();
        $collection->push(['Part untuk Bongkar Bearing belum datang','Opex']);
        $collection->push(['Part untuk Perbaikan Major Crusher Masih Nego','Capex']);
        $collection->push(['Data SAP perlu diupdate kembali','All User']);
        return $collection;
    }

    public function title(): string
    {
        return 'Data Issue OVH';
    }

    public function headings(): array
    {
        return ['Issues', 'Budget'];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1    => ['font' => ['bold' => true]],
        ]; 
    }
}

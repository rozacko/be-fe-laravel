<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithColumnWidths;

class NeracaLimbahExport implements FromView, WithColumnWidths, WithStyles
{
    protected $data;
    public function __construct($data)
    {
        $this->data = $data;
    }

    public function view(): View
    {
        return view('report.neraca_limbah_b3.export_excel', [
            'data' => $this->data
        ]);
    }

    public function columnWidths(): array
    {
        return [
            'A' => 20,
            'B' => 30,
            'C' => 20,
            'D' => 20,
            'E' => 20,
            'F' => 30,
            'G' => 20,
            'H' => 20,
            'I' => 20
        ];
    }

    public function styles(Worksheet $sheet)
    {
        $styleBorder = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => 'FFFF'],
                ],
            ]
        ];
        $sheet->getStyle('A1:' . $sheet->getHighestColumn() . $sheet->getHighestRow())->applyFromArray($styleBorder);
        return [
            // Styling a specific cell by coordinate.
            'A1:I1' => ['font' => ['bold' => true, 'size' => 14]],
            'A2:I2' => ['font' => ['bold' => true, 'size' => 12]],
        ];
    }
}

<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class KPIDireksiExport implements FromView, WithColumnWidths,WithStyles
{
    protected $data;

    function __construct($data) {
        $this->data = $data;
    }

    public function view(): View {
        return view('report.kpidireksi.laporankpidireksi', [
            'data' => $this->data
        ]);
    }

    public function columnWidths(): array {
        return [
            'A' => 20,
            'B' => 20,
            'C' => 20,
            'D' => 20,
            'E' => 20,
            'F' => 10,
            'G' => 10,
            'H' => 30,
            'I' => 25,
            'J' => 20,
        ];
    }

    public function styles(Worksheet $sheet) {
        $styleBorder = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => 'FFFF'],
                ],
            ]
        ];
        $sheet->getStyle('A4:' . $sheet->getHighestColumn() . $sheet->getHighestRow())->applyFromArray($styleBorder);
        // $sheet->getStyle('A1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        // $sheet->getStyle('A2')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        return [
            // Styling a specific cell by coordinate.
            'A1' => ['font' => ['bold' => true,'size' => 14]],
        ];
    }
}

<?php

namespace App\Imports;

// use App\Models\T_OverhaulActivity;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Facades\DB;
use App\Models\MArea;
use Carbon\Carbon;

class ItemInspectionPlantImport implements ToModel, WithHeadingRow
{
    protected $id_plant;
    
    public function __construct()
    {
        
    }
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        // $area = MArea::where(DB::raw('upper(nm_area)'), trim(strtoupper($row['area'])))->first();
        // return new T_OverhaulActivity([
        //     //
        //     'id_plant' => $this->id_plant,
        //     'id_area' => $area->id,
        //     'activity_name' => $row['activity_name'],
        //     'activity_category' => $row['category'],
        //     'start_date' => Carbon::createFromFormat('d-m-Y',$row['start_date'])->format('Y-m-d'),
        //     'end_date' => Carbon::createFromFormat('d-m-Y',$row['end_date'])->format('Y-m-d'),
        // ]);
    }
}

<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TSparepartJasa extends Model
{
    use HasFactory ,Modifier, SoftDeletes;
    protected $table = 't_sparepart_jasa';
    protected $fillable = [
        'tanggal',
        'pr',
        'kode_plant',
        'nama_plant',
        'pekerjaan',
        'estimator',
        'nilai',
        'engineering_tanggal',
        'engineering_status',
        'rfq_tanggal',
        'rfq_status',
        'po_tanggal',
        'po_status',
        'status_pekerjaan',
        'vendor',
        'kontrak',
        'start_date',
        'finish_date',
        'pr_ke_estimator',
        'app_p_sawab',
        'app_p_didit',
        'app_p_zaini',
        'nama_estimator',
        'pengadaan',
        'user',
        'keterangan',
        "created_by", "updated_by", "deleted_by"
    ];
}

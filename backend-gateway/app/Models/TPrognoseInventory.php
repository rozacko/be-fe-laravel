<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TPrognoseInventory extends Model
{
    use HasFactory,Modifier, SoftDeletes;
    protected $table = 't_prognose_inventory';
    protected $fillable = ["parameters","tahun",'bulan','value',"created_by","updated_by", "deleted_by"];
}

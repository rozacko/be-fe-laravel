<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SHEUnsafeAction extends Model
{
    use Modifier, HasFactory;
    protected $table = 'she_unsafe_action';
    protected $fillable = [  
        'bulan',
        'tahun',
        'nm_perusahaan',
        'uraian_temuan',
        'lokasi_kejadian',
        'tgl_temuan',
        'jml_temuan',
        'pengawas',
        'identifikasi',
        'tindakan',
        'keterangan',
        'file',
        'file_gdrive',
        'status',
        'created_by',
        'updated_by',
    ];
}

<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SHEKpiSafety extends Model
{
    use Modifier, HasFactory;
    protected $table = 'she_kpi_safety';
    protected $fillable = [  
        'tahun',
        'kons1',
        'kons2',
        'target1',
        'target2',        
        'created_by',
        'updated_by',
    ];
}

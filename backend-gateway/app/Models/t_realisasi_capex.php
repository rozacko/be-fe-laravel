<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class t_realisasi_capex extends Model
{
    use HasFactory;

    public $table = "t_realisasi_capex";

    protected $fillable = [
        'no_project',
        'no_wbs',
        'item_capex',
        'unit_kerja',
        'pm',
        'tipe',
        'kai',
        'op_dev',
        'nilai_investasi',
        'committed',
        'pelampauan',
        'real_spending',
        'network',
        'reservasi',
        'no_pr',
        'nilai_pr',
        'no_po',
        'doc_date',
        'del_date',
        'nilai_po',
        'tgl_gr',
        'spending_gr',
        'progres_fisik',
        'progres_report_terakhir',
        'nilai_auc',
        'status_bast',
        'status_wbs',
        'keterangan'
    ];
}
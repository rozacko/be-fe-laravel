<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Modifier;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

class TPresensiPegawai extends Model
{
    use HasFactory, Modifier, SoftDeletes;
    protected $casts = [
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
    ];
    
    protected $primaryKey = 'id_presensi_pegawai';
    protected $table = 't_presensi_pegawai';
    protected $fillable = ['no_pegawai', 'nama_pegawai', 'time', 'state', 'new_state', 'exeption', 'preference', 'tanggal', 'event', 'created_by', 'updated_by'];

      /**
     * The "booted" method of the model.
     */
    protected static function booted(): void
    {
        static::creating(function ($model) {
            $model->uuid = Uuid::uuid4();
        });
    }
    
    public function countDataByNoPegawai($arrPegawai, $bulan, $tahun)
    {
        $query = TPresensiPegawai::select(DB::raw('count(distinct no_pegawai) as total_realisasi,no_pegawai'))
                ->whereRaw('EXTRACT(YEAR FROM tanggal) = ?', $tahun)
                ->whereRaw('EXTRACT(MONTH FROM tanggal) = ?', $bulan)
                ->whereIn('no_pegawai', $arrPegawai)
                ->groupBy('no_pegawai');
        return $query->get();
    }
}

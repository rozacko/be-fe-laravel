<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TCogmReport extends Model
{
    use HasFactory, Modifier;
    protected $table ='t_cogm_report';
    protected $fillable = ['kode_opco','nama_biaya','tahun','bulan','total_biaya','created_by','updated_by','created_at','updated_at'];
}

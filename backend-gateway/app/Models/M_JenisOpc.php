<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class M_JenisOpc extends Model
{
    use HasFactory, Modifier, SoftDeletes;
    protected $table = "m_jenis_opc";
    protected $fillable = ["id_area","jenis_opc", "created_by", "updated_by"];

    public function area(){
        return $this->belongsTo("App\Models\MArea",'id_area');
    }

    public function tags(){
        return $this->hasMany("App\Models\M_TagList",'id_jenis_opc');
    }
}

<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SHEProgramLingIs extends Model
{
    use Modifier, HasFactory;
    protected $table = 'she_program_lingkungan_is';
    protected $fillable = [
        'id_program',
        'inisiatif_strategis',
        'start_date',
        'end_date',
        'created_by',
        'updated_by',
    ];
}

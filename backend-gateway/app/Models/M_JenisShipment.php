<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class M_JenisShipment extends Model
{
    use HasFactory, Modifier, SoftDeletes;
    protected $table = "m_jenis_shipment";
    protected $fillable = ["id_api_shipment",
                            "id_plant",
                            "jenis_shipment",
                            "shipment_params",
                            "created_by", "updated_by"];

    public function plant(){
        return $this->belongsTo("App\Models\MPlant","id_plant");
    }

    public function api(){
        return $this->belongsTo("App\Models\M_ApiShipment","id_api_shipment");
    }

    public function latestValue(){
        return $this->hasOne("App\Models\T_PenarikanShipment","id_jenis_shipment")->ofMany('shipment_date','max');
    }
}

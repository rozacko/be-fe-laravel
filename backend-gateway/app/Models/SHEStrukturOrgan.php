<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SHEStrukturOrgan extends Model
{
    use Modifier, HasFactory;
    protected $table = 'she_struktur_organ';
    protected $fillable = [  
        'tahun',
        'file',        
        'created_by',
        'updated_by',
    ];
}

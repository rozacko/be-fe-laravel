<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SHESertifLing extends Model
{
    use Modifier, HasFactory;
    protected $table = 'she_sertif_ling';
    protected $fillable = [
        'tahun',
        'nm_sertifikat',
        'nm_penerbit',
        'end_date',
        'file',
        'created_by',
        'updated_by',
    ];
}

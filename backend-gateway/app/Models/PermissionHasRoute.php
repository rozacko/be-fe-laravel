<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;
use App\Traits\Modifier;

class PermissionHasRoute extends Pivot {

    use Modifier;
}

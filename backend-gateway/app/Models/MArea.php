<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MArea extends Model
{
    use Modifier, HasFactory;
    protected $table = 'm_area';
    protected $fillable = [  
        'nm_area',
        'ref_sap',
        'created_by',
        'updated_by',
    ];

    public function overhaulActivity(){
        return $this->hasMany("App\Models\T_OverhaulActivity","id_area");
    }
}

<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class T_InspectionComment extends Model
{
    use HasFactory, Modifier, SoftDeletes;
    protected $table = "t_plant_inspection_comment";
    
    protected $fillable = [
        'id_inspection',
        'remark',
        'user',
        'action',
        'created_by',
        'updated_by'
    ];
}

<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SHEApar extends Model
{
    use Modifier, HasFactory;
    protected $table = 'she_apar';
    protected $fillable = [  
        'bulan',
        'tahun',
        'area',
        'jenis_apar',
        'unit_kerja',
        'lokasi',
        'map_baru',
        'clamp',
        'press_bar',
        'hose',
        'segel',
        'sapot',
        'berat',
        'bulan_aktf',
        'tahun_aktf',
        'status',
        'file',
        'file_gdrive',
        'keterangan',
        'created_by',
        'updated_by',
    ];
}

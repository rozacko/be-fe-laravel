<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SHEHydrantFile extends Model
{
    use Modifier, HasFactory;
    protected $table = 'she_hydrant_file';
    protected $fillable = [  
        'id_hydrant',
        'deskripsi',
        'file',        
        'created_by',
        'updated_by',
    ];
}

<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class T_StatusSyncShipment extends Model
{
    use HasFactory, Modifier, SoftDeletes;
    protected $table = "t_status_sync_shipment";
    protected $fillable = ["id_api_shipment",
                            "tgl_penarikan",
                            "status","num_tried",
                            "created_by", "updated_by"];

    public function api(){
        return $this->belongsTo("App\Models\M_ApiShipment","id_api_shipment");
    }
}

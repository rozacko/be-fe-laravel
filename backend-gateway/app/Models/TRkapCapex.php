<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TRkapCapex extends Model
{
    use HasFactory,Modifier;

    protected $table = 't_capex_planning';
    protected $fillable = ['no_project','planning_description','wbs','tahun','bulan','total_budget','create_by','create_date','delete_mark','update_by','update_date'];
}

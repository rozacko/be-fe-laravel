<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class T_RealisasiCapexCommited extends Model
{
    use HasFactory;

    public $table = "t_realisasi_capex_commited";

    protected $fillable = [
        'id_realisasi_capex',
        'bulan',
        'value'
    ];
}
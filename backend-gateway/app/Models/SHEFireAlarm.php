<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SHEFireAlarm extends Model
{
    use Modifier, HasFactory;
    protected $table = 'she_fire_alarm';
    protected $fillable = [  
        'parameter_id',
        'bulan',
        'tahun',
        'dt_m',
        'dt_w1',
        'dt_w2',
        'dt_w3',
        'dt_w4',
        'status',
        'keterangan',
        'ccr_tuban',
        'created_by',
        'updated_by',
    ];
}

<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SHEKomPersonil extends Model
{
    use Modifier, HasFactory;
    protected $table = 'she_kom_personil';
    protected $fillable = [
        'tahun',
        'bidang',
        'kriteria',
        'created_by',
        'updated_by',
    ];
}

<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SHEManHours extends Model
{
    use Modifier, HasFactory;
    protected $table = 'she_man_hours';
    protected $fillable = [  
        'bulan',
        'tahun',
        'jml_jam',
        'jml_hari',
        'created_by',
        'updated_by',
    ];
}

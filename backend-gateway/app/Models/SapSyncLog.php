<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SapSyncLog extends Model
{
    protected $fillable = [
        'sap_sync_config_id',
        'year',
        'month',
        'status',
        'note',
        'tcode',
        'parameter',
        'config_name',
        'created_at',
        'updated_at'
    ];

    public static function statuses()
    {
        return [
            'success' => 'Success',
            'process' => 'Process',
            'fail' => 'Fail'
        ];
    }

    public function config()
    {
        return $this->belongsTo(SapSyncConfig::class, 'sap_sync_config_id', 'id');
    }
}

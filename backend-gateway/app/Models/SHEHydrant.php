<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SHEHydrant extends Model
{
    use Modifier, HasFactory;
    protected $table = 'she_hydrant';
    protected $fillable = [  
        'bulan',
        'tahun',
        'main_pump',
        'lokasi',
        'no_pilar',
        'cek',
        'p_body',
        'p_ket',
        'p_status',
        'v_kiri',
        'v_atas',
        'v_kanan',
        'v_ket',
        'v_status',
        'k_kiri',
        'k_kanan',
        'k_ket',
        'k_status',
        'tk_kiri',
        'tk_kanan',
        'tk_ket',
        'tk_status',
        'b_casing',
        'b_hose',
        'b_nozle',
        'b_kunci',
        'b_ket',
        'press_bar',
        'pb_ket',
        'status_kondisi',
        'created_by',
        'updated_by',
    ];
}

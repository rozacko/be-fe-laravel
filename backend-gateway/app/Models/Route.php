<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Modifier;
use Illuminate\Database\Eloquent\SoftDeletes;

class Route extends Model {

    use Modifier, SoftDeletes;

    protected $fillable = ["unique_code", "name", "http_method", "url", "path", "middleware", "status", "created_by", "updated_by"];
    protected $hiddens = ["pivot"];

    public function permissions(){
        return $this->belongsToMany("App\Models\Permission", "permission_has_routes")->using("App\Models\PermissionHasRoute")->withTimestamps();
    }
}

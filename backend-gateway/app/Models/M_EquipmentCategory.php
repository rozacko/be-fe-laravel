<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class M_EquipmentCategory extends Model
{
    use HasFactory, Modifier, SoftDeletes;
    protected $table = "m_equipment_category";
    protected $fillable = ["category_name", "created_by", "updated_by"];
}

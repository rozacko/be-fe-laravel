<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class T_RealisasiCapexCashout extends Model
{
    use HasFactory;

    public $table = "t_realisasi_capex_cashout";

    protected $fillable = [
        'id_realisasi_capex',
        'bulan',
        'value'
    ];
}
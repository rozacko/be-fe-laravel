<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SapSyncConfig extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'type',
        'tcode',
        'parameter',
        'schedule',
        'at_date',
        'at_time',
        'status',
        'created_at',
        'updated_at'
    ];

    public static function types()
    {
        return [
            'capex' => 'Capex',
            'biaya' => 'Biaya',
            'produksi' => 'Produksi',
            'penjualan' => 'Penjualan'
        ];
    }

    public static function schedules()
    {
        return [
            'minutely' => 'Minutely',
            'everyfiveminute' => 'Every 5 Minutes',
            'hourly' => 'Hourly',
            'daily' => 'Daily',
            'monthly' => 'Monthly'
        ];
    }

    public static function statuses()
    {
        return [
            'active' => 'Active',
            'non_active' => 'Non Active'
        ];
    }

    public function logs()
    {
        return $this->hasMany(SapSyncLog::class);
    }
}

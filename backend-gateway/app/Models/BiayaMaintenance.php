<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BiayaMaintenance extends Model
{
    use SoftDeletes;

    protected $table = 't_biaya_maintenance';

    protected $fillable = [
        'no_dokumen',
        'kode_opco',
        'row',
        'cost_center',
        'cost_center_name',
        'cost_element',
        'cost_element_name',
        'tanggal',
        'biaya',
        'created_at',
        'updated_at'
    ];
}

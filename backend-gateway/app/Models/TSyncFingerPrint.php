<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Modifier;

class TSyncFingerPrint extends Model
{
    use HasFactory, Modifier;

    protected $table = 't_sync_finger_print';
    protected $fillable = ['tanggal', 'status_sync', 'created_by', 'updated_by'];
}

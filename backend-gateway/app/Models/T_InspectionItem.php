<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class T_InspectionItem extends Model
{
    use HasFactory, Modifier, SoftDeletes;
    protected $table = "t_item_inspection";
    
    protected $fillable = [
        'id_inspection',
        'id_equipment',
        'kode_equipment',
        'nama_equipment',
        'tgl_inspection',
        'id_area',
        'nama_area',
        'id_kondisi',
        'kode_kondisi',
        'remark',
        'created_by',
        'updated_by'
    ];

    // public function plant(){
    //     return $this->belongsTo("App\Models\MPlant",'id_plant');
    // }

    // public function area(){
    //     return $this->belongsTo("App\Models\MArea", "id_area");
    // }

    // public function unitkerja(){
    //     return $this->belongsTo("App\Models\MWorkgroup", "id_unitkerja");
    // }
}

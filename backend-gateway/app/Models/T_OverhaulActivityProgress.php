<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class T_OverhaulActivityProgress extends Model
{
    use HasFactory, Modifier, SoftDeletes;
    protected $table="t_overhaul_activity_progress";
    protected $fillable = [
        "id_activity",
        "progress_date",
        "plan_cumulative",
        "real_cumulative",
        "created_by",
        "updated_by",];

    public function activity(){
        return $this->belongsTo("App\Models\T_OverhaulActivity",'id_activity');
    }

    public function dokumentasi(){
        return $this->hasMany("App\Models\T_OverhaulActivityDokumentasi","id_progress");
    }
}

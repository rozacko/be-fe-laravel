<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SHESertifAnalis extends Model
{
    use Modifier, HasFactory;
    protected $table = 'she_sertif_analis';
    protected $fillable = [
        'tahun',
        'start_bulan',
        'start_tahun',
        'end_bulan',
        'end_tahun',
        'nm_sertifikat',
        'file',
        'created_by',
        'updated_by',
    ];
}

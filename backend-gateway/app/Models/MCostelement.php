<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MCostelement extends Model
{
    use Modifier, HasFactory;
    protected $table = 'm_costelement';
    protected $fillable = [        
      'cost_element',
      'cost_element_name',
      'cost_element_group',
      'biaya_group',
      'sap',
      'created_by',
      'updated_by',
    ];
}

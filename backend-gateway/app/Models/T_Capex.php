<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class T_Capex extends Model
{
    use HasFactory, Modifier, SoftDeletes;
    protected $table = "t_capex";
    protected $fillable = [
        'level',
        'no_project',
        'wbs',
        'description',
        'kode_opco',
        'tanggal',
        'tahun',
        'kode_plant',
        'inv_reason',
        'rs_cost',
        'pr_respon',
        'npr_respon',
        'pspri',
        'prctr',
        'actual',
        'budget',
        'commit',
        'avail',
        'rmorp',
        'plan_cost',
        'assig_cost',
        "created_by",
        "updated_by"
    ];
}

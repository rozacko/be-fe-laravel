<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Permission\Models\Permission as SpatiePermission;

class Permission extends SpatiePermission
{

    use Modifier, SoftDeletes;

    protected $fillable = ["unique_code", "name", "feature_id", "action_id", "status", "created_by", "updated_by"];
    protected $attributes = [
        "guard_name" => "api",
    ];

    public function routes()
    {
        return $this->belongsToMany("App\Models\Route", "permission_has_routes")->using("App\Models\PermissionHasRoute")->withTimestamps();
    }

    public function scopeDatatable($query)
    {
        return $query->leftJoin('features', 'features.id', '=', 'permissions.feature_id')
            ->leftJoin('modules', 'modules.id', '=', 'features.module_id')
            ->leftJoin('actions', 'actions.id', '=', 'permissions.action_id')
            ->select('permissions.*', 'modules.name as module_name', 'features.name as feature_name', 'actions.name as action_name')
            ->orderBy("modules.name", "asc")->orderBy("features.name", "asc")->orderBy("actions.name", "asc");
    }
}

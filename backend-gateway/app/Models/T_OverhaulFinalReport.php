<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class T_OverhaulFinalReport extends Model
{
    use HasFactory, Modifier, SoftDeletes;
    protected $table="t_overhaul_final_report";
    protected $fillable=[
                            "id_plant",
                            "report_year",
                            "title",
                            "filepath",
                            "created_by",
                            "updated_by",
                        ];
}

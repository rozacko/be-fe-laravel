<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TQcClk extends Model
{
    use HasFactory,Modifier,SoftDeletes;
    protected $table = 't_qc_clk';
    protected $fillable = [
    "plant_id","tanggal","jam","lsf","sim","alm","c3s","fcao","so3","created_by","updated_by"
    ];
}

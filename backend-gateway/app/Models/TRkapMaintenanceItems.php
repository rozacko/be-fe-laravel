<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TRkapMaintenanceItems extends Model
{
    use HasFactory,Modifier;

    protected $table = 't_rkap_maintenance_items';
    protected $fillable = ['id_rkap_maintenance','bulan','nilai_rkap','created_by','updated_by'];
}

<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TOrderTask extends Model
{
    use HasFactory, Modifier, SoftDeletes;
    protected $table = 't_order_task';
    protected $fillable = [
        "order_type",
        "order",
        "description",
        "system_status",
        "equipment",
        "equipment_description",
        "total_act_cost",
        "tanggal_actual",
        "entered_by",
        "planner_group",
        "plant_section",
        "res_cost_center",
        "planning_plant",
        "functional_loc",
        "cost_center",
        "created_on",
        "user_status",
        "basic_start_date",
        "basic_fin_date",
        "status_order",
        "reason",
        "description_uk",
        "sub_area_proses",
        "plant",
        "area_proses",
        "bulan",
        "kode_area_proses",
        "tahun",
        "order_confirmation", "created_by", "updated_by", "deleted_by"
    ];
}

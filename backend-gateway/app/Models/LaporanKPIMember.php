<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Modifier;
use Illuminate\Database\Eloquent\SoftDeletes;

class LaporanKPIMember extends Model
{
    use Modifier, HasFactory;
    protected $table = 't_laporan_kpi_member';
    protected $primaryKey = 'id_laporan_kpi';

    protected $fillable = [
        'tahun',
        'no_pegawai',
        'nama_pegawai',
        'nama_organisasi',
        'jabatan',
        'unit_kerja',
        'target_point',
        'q1',
        'q2',
        's1',
        'q3',
        'q4',
        's2',
        'nilai_akhir',
        'created_by',
        'updated_by',
    ];
}

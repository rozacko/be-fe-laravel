<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TPrognoseCapex extends Model
{
    use HasFactory,Modifier;
    protected $table = 't_prognose_capex';
    protected $fillable = ['no_project','prognose_description','wbs','tahun','bulan','prognose','created_by','created_date','delete_mark','update_by','update_date'];
}

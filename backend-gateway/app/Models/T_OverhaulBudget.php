<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class T_OverhaulBudget extends Model
{
    use HasFactory, Modifier;

    protected $table="t_overhaul_budget";
    protected $fillable = [
        "id_plant",
        "tahun",
        "unit_kerja",
        "budget_capex",
        "budget_opex",
        "created_by",
        "updated_by",
    ];
}

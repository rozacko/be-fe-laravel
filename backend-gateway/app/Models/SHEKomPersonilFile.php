<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SHEKomPersonilFile extends Model
{
    use Modifier, HasFactory;
    protected $table = 'she_kom_personil_file';
    protected $fillable = [
        'id_kompetensi',
        'no_peg',
        'nm_pegawai',
        'nm_sertifikat',
        'nm_penerbit',
        'end_date',
        'file',
        'created_by',
        'updated_by',
    ];
}

<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class M_ApiShipment extends Model
{
    use HasFactory, Modifier, SoftDeletes;
    protected $table = "m_api_shipment";
    protected $fillable = ["api_name","api_url","api_method","status","api_params","created_by", "updated_by"];
}

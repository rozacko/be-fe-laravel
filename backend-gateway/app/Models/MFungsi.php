<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MFungsi extends Model
{
    use Modifier, HasFactory;
    protected $table = 'm_fungsi';
    protected $fillable = [  
        'uuid',
        'nama_fungsi',
        'created_by',
        'updated_by',
    ];
}

<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MCostcenter extends Model
{
    use Modifier, HasFactory;
    protected $table = 'm_costcenter';
    protected $fillable = [        
      'costcenter',
      'costcenter_name',
      'sap',
      'created_by',
      'updated_by',
    ];
}

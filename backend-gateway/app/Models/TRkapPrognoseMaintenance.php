<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TRkapPrognoseMaintenance extends Model
{
    use HasFactory,Modifier;
    protected $table = 't_rkap_prognose_maintenance';
    protected $fillable = ['code_opco','jenis_rkap','tahun','status','created_by','updated_by'];
}

<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SHEIzinLingFile extends Model
{
    use Modifier, HasFactory;
    protected $table = 'she_izin_lingkungan_file';
    protected $fillable = [
        'id_perizinan',
        'area',
        'no_izin',
        'masa_berlaku',
        'penerbit',
        'file',
        'created_by',
        'updated_by',
    ];
}

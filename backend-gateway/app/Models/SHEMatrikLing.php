<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SHEMatrikLing extends Model
{
    use Modifier, HasFactory;
    protected $table = 'she_matrik_ling';
    protected $fillable = [
        'tahun',
        'jenis',
        'nomor',
        'peraturan',
        'emisi_udara',
        'pembuangan_air',
        'pembuangan_tanah',
        'sda',
        'penggunaan_energi',
        'pancaran_energi',
        'limbah',
        'atribut_fisik',
        'k3',
        'taat',
        'keterangan',
        'created_by',
        'updated_by',
    ];
}

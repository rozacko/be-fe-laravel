<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SHESafetyTrainingOrg extends Model
{
    use Modifier, HasFactory;
    protected $table = 'she_safety_training_org';
    protected $fillable = [  
        'id_training',
        'no_pegawai',
        'nm_pegawai',
        'departement',
        'unit_kerja',
        'band',
        'jam_pelatihan',
        'created_by',
        'updated_by',
    ];
}

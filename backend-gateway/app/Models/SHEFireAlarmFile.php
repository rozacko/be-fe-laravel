<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SHEFireAlarmFile extends Model
{
    use Modifier, HasFactory;
    protected $table = 'she_fire_alarm_file';
    protected $fillable = [  
        'bulan',
        'tahun',
        'deskripsi',
        'file',        
        'created_by',
        'updated_by',
    ];
}

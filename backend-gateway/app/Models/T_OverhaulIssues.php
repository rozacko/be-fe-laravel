<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class T_OverhaulIssues extends Model
{
    use HasFactory, Modifier;

    protected $table = 't_overhaul_issues';
    protected $fillable = [
                                "id_plant",
                                "ovh_year",
                                "budget",
                                "issues",
                                "created_by",
                                "updated_by",
                            ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Modifier;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Menu extends Model
{
    use Modifier, SoftDeletes;

    public $incrementing = false;
    protected $fillable = ["id", "name", "description", "permission_id", "url", "order_no", "icon", "parent_id", "status", "created_by", "updated_by", "powerbi_url"];

    public function features()
    {
        return $this->belongsTo("App\Models\Permission");
    }

    public function permission()
    {
        return $this->hasOne("App\Models\Permission", "id", "permission_id")
            ->withDefault(["name" => "no Permission"]);
    }

    public function parent()
    {
        return $this->hasOne("App\Models\Menu", "id", "parent_id")
            ->withDefault(["name" => "No Parent"]);
    }

    public function scopeRoot($query)
    {
        return DB::table("menus")
            ->select(
                "menus.id",
                "menus.uuid",
                "menus.name as text",
                "menus.icon",
                "menus.order_no",
                DB::raw("(CASE WHEN count(*) = count(childs.*) THEN true ELSE false END) as children")
            )
            ->leftJoin("menus as childs", "menus.id", "=", "childs.parent_id")
            ->where("menus.parent_id", 0)
            ->orderBy("menus.order_no")
            ->groupBy("menus.id");
    }

    public function scopeChilds($query, $parent)
    {
        return DB::table("menus")
            ->select(
                "menus.id",
                "menus.uuid",
                "menus.name as text",
                "menus.icon",
                DB::raw("(CASE WHEN count(*) = count(childs.*) THEN true ELSE false END) as children")
            )
            ->leftJoin("menus as childs", "menus.id", "=", "childs.parent_id")
            ->where("menus.parent_id", $parent)
            ->groupBy("menus.id");
    }

    public function scopeByUser($query, $user_id)
    {
        return DB::table("model_has_roles")
            ->select(
                "menus.id",
                "menus.name as title",
                "menus.description",
                "menus.url as path",
                "menus.parent_id",
                "menus.order_no",
                "menus.icon",
                "menus.powerbi_url",
                "role_has_permissions.permission_id",
                "permissions.name as permission_name",
                "permissions.feature_id",
                "permissions.action_id"
            )
            ->join("users", "model_has_roles.model_id", "=", "users.id")
            ->join("role_has_permissions", "model_has_roles.role_id", "=", "role_has_permissions.role_id")
            ->join("permissions", "role_has_permissions.permission_id", "=", "permissions.id")
            ->leftJoin("menus", "menus.permission_id", "=", "role_has_permissions.permission_id")
            ->where("users.id", $user_id)
            ->whereNull("menus.deleted_at")
            ->orderBy("menus.order_no", "ASC");
    }

    public function scopeParentNoPermission($query, array $id)
    {
        return $query->select(
            "menus.id",
            "menus.name as title",
            "menus.description",
            "menus.url as path",
            "menus.parent_id",
            "menus.order_no",
            "menus.icon",
            "menus.powerbi_url",
            DB::raw("null as permission_id"),
            DB::raw("null as  permission_name"),
            DB::raw("null as feature_id"),
            DB::raw("null as action_id")
        )
            ->whereIn('menus.id', $id)
            ->whereNull('permission_id');
    }

    public function scopeDetail($query)
    {
        return DB::table("menus")->leftJoin('permissions', 'permissions.id', '=', 'menus.permission_id')
            ->leftJoin('features', 'features.id', '=', 'permissions.feature_id')
            ->leftJoin('modules', 'modules.id', '=', 'features.module_id')
            ->leftJoin('actions', 'actions.id', '=', 'permissions.action_id')
            ->select('menus.*', 'modules.name as module_name', 'features.name as feature_name', 'permissions.name as permission_name', 'actions.name as action_name');
    }
}

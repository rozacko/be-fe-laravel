<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Modifier;
use Illuminate\Database\Eloquent\SoftDeletes;
use Ramsey\Uuid\Uuid;

class TDokumenLingkungan extends Model
{
    const FORMATDATETIME = "datetime:d-m-Y H:i:s";
    use HasFactory, Modifier, SoftDeletes;
    protected $casts = [
        'created_at' => self::FORMATDATETIME,
        'updated_at' => self::FORMATDATETIME,
        'deleted_at' => self::FORMATDATETIME,
    ];

    protected $primaryKey = 'id';
    protected $table = 't_dokumen_lingkungan';
    protected $fillable = ['area_id', 'nm_area', 'judul', 'no_dokumen', 'ruang_lingkup','jenis_dokumen','filename','created_by', 'updated_by'];
    protected $hiddens = ["deleted_at"];

    /**
     * The "booted" method of the model.
     */
    protected static function booted(): void
    {
        static::creating(function ($model) {
            $model->uuid = Uuid::uuid4();
        });
    }
}

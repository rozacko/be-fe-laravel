<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MEqInspection extends Model
{
    use Modifier, HasFactory;
    protected $table = 'm_eq_inspection';
    protected $fillable = [  
        'kd_equipment',
        'nm_equipment',
        'area_id',
        'plant_id',
        'ref_sap',
        'created_by',
        'updated_by',
    ];
}

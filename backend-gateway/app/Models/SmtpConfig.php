<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Modifier;
use Illuminate\Database\Eloquent\SoftDeletes;

class SmtpConfig extends Model
{
    use Modifier, SoftDeletes;

    protected $fillable = ["host", "port", "username", "password", "encryption", "status"];
}

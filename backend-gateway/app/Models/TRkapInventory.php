<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TRkapInventory extends Model
{
    use HasFactory,Modifier, SoftDeletes;
    protected $table = 't_rkap_inventory';
    protected $fillable = ["parameters","tahun",'bulan','value',"created_by","updated_by", "deleted_by"];
}

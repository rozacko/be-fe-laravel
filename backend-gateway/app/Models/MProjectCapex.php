<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MProjectCapex extends Model
{
    use Modifier, HasFactory;
    protected $table = 'm_project_capex';
    protected $fillable = [  
        'no_project',
        'description',
        'sap',
        'kategori_capex',
        'created_by',
        'updated_by',
    ];
}

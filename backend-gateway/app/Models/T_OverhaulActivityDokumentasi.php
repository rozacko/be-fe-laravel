<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class T_OverhaulActivityDokumentasi extends Model
{
    use HasFactory, Modifier, SoftDeletes;
    protected $table="t_overhaul_dokumentasi_progress";
    protected $fillable = [
        "id_progress",
        "file_path",
        "created_by",
        "updated_by",];

    public function progress(){
        return $this->belongsTo("App\Models\T_OverhaulActivityProgress",'id_progress');
    }
}

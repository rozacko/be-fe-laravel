<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TQcCoal extends Model
{
    use HasFactory,Modifier,SoftDeletes;
    protected $table = 't_qc_coal';
    protected $fillable = [
    "quality","tanggal","ghv","ac","tm","created_by","updated_by"
    ];
}

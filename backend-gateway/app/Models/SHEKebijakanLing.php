<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SHEKebijakanLing extends Model
{
    use Modifier, HasFactory;
    protected $table = 'she_kebijakan_ling';
    protected $fillable = [  
        'tahun',
        'nm_kebijakan',
        'file',        
        'created_by',
        'updated_by',
    ];
}

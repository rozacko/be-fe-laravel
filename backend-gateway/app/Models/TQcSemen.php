<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TQcSemen extends Model
{
    use HasFactory,Modifier,SoftDeletes;
    protected $table = 't_qc_semen';
    protected $fillable = [
    "quality","type","plant_id","tanggal","jam","so3","fcao","blaine","res45","loi","created_by","updated_by"
    ];
}

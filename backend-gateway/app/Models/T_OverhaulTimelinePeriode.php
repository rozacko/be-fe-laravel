<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class T_OverhaulTimelinePeriode extends Model
{
    use HasFactory, Modifier, SoftDeletes;
    protected $table = 't_overhaul_timeline_periode';
    protected $fillable = ["id_periode", 
                            "path",
                            "created_by",
                            "updated_by",];
}

<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SHEKpiProper extends Model
{
    use Modifier, HasFactory;
    protected $table = 'she_kpi_proper';
    protected $fillable = [
        'tahun',
        'kebijakan',
        'satuan',
        'target',
        'realisasi',
        'kpi',
        'created_by',
        'updated_by',
    ];
}

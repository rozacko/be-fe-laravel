<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class T_PenarikanOpc extends Model
{
    use HasFactory, Modifier;
    protected $table='t_penarikan_opc';
    protected $fillable = ["id_tag","tag_value","msdr_value","harpros_value","waktu_penarikan","created_by","status","updated_by"];

    public function opc(){
        return $this->belongsTo("App\Models\M_TagList",'id_tag');
    }

}

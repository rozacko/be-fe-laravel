<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CapexItem extends Model
{
    use HasFactory;
    public $table = "t_capex_item";

    protected $fillable = [
        'no_project',
        'no_wbs',
        'nama_capex',
        'tipe',
        'jenis',
        'nilai',
        'inisiator_seksi',
        'inisiator_biro',
        'inisiator_dept',
        'realisasi_engineering',
        'realisasi_procurement',
        'realisasi_delivery',
        'realisasi_closing',
        'progress_status',
        'created_by',
        'created_at',
        'updated_by',
        'updated_at',
        'deleted_at',
        'keterangan'
    ];
}
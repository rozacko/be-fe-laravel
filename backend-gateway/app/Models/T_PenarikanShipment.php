<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class T_PenarikanShipment extends Model
{
    use HasFactory, Modifier, SoftDeletes;
    protected $table = "t_penarikan_shipment";
    protected $fillable = ["id_jenis_shipment",
                            "shipment_date",
                            "status",
                            "data_value",
                            "msdr_value",
                            "harpros_value",
                            "created_by", "updated_by"];

    public function jenis(){
        return $this->belongsTo("App\Models\M_JenisShipment","id_jenis_shipment");
    }
}

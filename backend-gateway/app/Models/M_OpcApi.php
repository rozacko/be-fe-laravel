<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class M_OpcApi extends Model
{
    use HasFactory, Modifier, SoftDeletes;
    protected $table = "m_opcapi";
    protected $fillable = ["api_name","api_url", "created_by", "updated_by"];
}

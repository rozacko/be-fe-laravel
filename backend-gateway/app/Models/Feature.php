<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Modifier;
use Illuminate\Database\Eloquent\SoftDeletes;

class Feature extends Model {

    use Modifier, SoftDeletes;

    public $incrementing = false;
    protected $fillable = ["id", "name", "module_id", "status", "created_by", "updated_by"];
    protected $appends = ["module_name"];
    protected $hidden = ["module"];

    public function module(){
        return $this->belongsTo("App\Models\Module");
    }
    
    public function getModuleNameAttribute(){
        return $this->module->name;
    }

    public function permissions()
    {
        return $this->hasMany("App\Models\Permission");
    }
}

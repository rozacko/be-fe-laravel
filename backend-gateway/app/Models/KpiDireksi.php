<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Modifier;
use Illuminate\Database\Eloquent\SoftDeletes;

class KpiDireksi extends Model
{
    use Modifier, HasFactory;
    protected $table = 'tk_kpi_direksi';
    protected $primaryKey = 'id';
    protected $fillable = [
        'kpi',
        'kpi_group',
        'tahun',
        'bulan',
        'unit',
        'polarisasi',
        'target',
        'prognose',
        'realisasi',
        'bobot',
        'nilai',
        'nilai_akhir',
        'persentase',
        'created_by',
        'updated_by',
    ];
}

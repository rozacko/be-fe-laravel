<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TQcHotMeal extends Model
{
    use HasFactory,Modifier,SoftDeletes;
    protected $table = 't_qc_hot_meal';
    protected $fillable =["quality","tanggal","so3_ilc","so3_slc","derajat_kalsinasi_ilc","derajat_kalsinasi_slc",'created_by','updated_by'];
}

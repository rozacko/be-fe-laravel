<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SHEUnsafeCondition extends Model
{
    use Modifier, HasFactory;
    protected $table = 'she_unsafe_condition';
    protected $fillable = [  
        'bulan',
        'tahun',
        'area',
        'tpm_sga',
        'no_equipment',
        'lokasi_kerja',
        'uraian_temuan',
        'saran',
        'pic',
        'unit_kerja',
        'id_trans_mso',
        'no_notif',
        'tgl_temuan',
        'tgl_target',
        'tgl_closing',
        'cek_1',
        'cek_2',
        'file_1',
        'file_gdrive_1',
        'file_2',
        'file_gdrive_2',
        'status',
        'keterangan',
        'created_by',
        'updated_by',
    ];
}

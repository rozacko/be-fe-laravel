<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Ramsey\Uuid\Uuid;

class MJenisLimbahEnviro extends Model
{
    const FORMATDATETIME = "datetime:d-m-Y H:i:s";
    use HasFactory, SoftDeletes;
    protected $casts = [
        'created_at' => self::FORMATDATETIME,
        'updated_at' => self::FORMATDATETIME,
        'deleted_at' => self::FORMATDATETIME,
    ];

    protected $primaryKey = 'id';
    protected $table = 'm_jenis_limbah_enviro';

    /**
     * The "booted" method of the model.
     */
    protected static function booted(): void
    {
        static::creating(function ($model) {
            $model->uuid = Uuid::uuid4();
        });
    }
}

<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MKondisi extends Model
{
    use Modifier, HasFactory;
    protected $table = 'm_kondisi';
    protected $fillable = [  
        'kd_kondisi',
        'nm_kondisi',
        'warna',
        'created_by',
        'updated_by',
    ];
}

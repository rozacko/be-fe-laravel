<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class T_OverhaulSparePart extends Model
{
    use HasFactory, Modifier;
    protected $table = "t_overhaul_preparation_sparepart";
    protected $fillable = [
                        "id_plant",
                        "ovh_year",
                        "nm_unit_kerja",
                        "kode_material",
                        "nama_material",
                        "category",
                        "budget",
                        "status",
                        "reservasi_no",
                        "pr_no",
                        "po_no",
                        "po_delivery_date",
                        "notes",
                        "total_price",
                        "created_by",
                        "updated_by",];

    public function plant(){
        return $this->belongsTo("App\Models\MPlant",'id_plant');
    }

    public function area(){
        return $this->belongsTo("App\Models\MArea", "id_area");
    }

    public function unitkerja(){
        return $this->belongsTo("App\Models\MWorkgroup", "id_unitkerja");
    }
}

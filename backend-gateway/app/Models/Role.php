<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role as SpatieRole;

class Role extends SpatieRole
{
    use Modifier, SoftDeletes;

    protected $fillable = ["name", "code", "status", "created_by", "updated_by"];

    public function scopePermissionModel($query, $role_uuid)
    {
        $role_permissions = $query->select("role_has_permissions.*")
            ->join("role_has_permissions", "roles.id", "=", "role_has_permissions.role_id")
            ->where("roles.uuid", $role_uuid)->toSql();

        return DB::table("modules")
            ->select("modules.name AS module_name",
                "features.module_id",
                "features.name AS feature_name",
                "permissions.feature_id",
                "permissions.name AS permission_name",
                "permissions.id AS permission_id",
                "actions.name AS action_name",
                "role_permissions.permission_id AS auth"
            )
            ->leftJoin("features", "modules.id", "=", "features.module_id")
            ->leftJoin("permissions", "features.id", "=", "permissions.feature_id")
            ->leftJoin("actions", "permissions.action_id", "=", "actions.id")
            ->leftJoin(DB::raw("({$role_permissions}) role_permissions"), "permissions.id", "=", "role_permissions.permission_id")
            ->setBindings([$role_uuid]);
    }
}

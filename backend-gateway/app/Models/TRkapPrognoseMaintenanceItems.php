<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TRkapPrognoseMaintenanceItems extends Model
{
    use HasFactory,Modifier;
    protected $table = 't_rkap_prognose_maintenance_items';
    protected $fillable = ['id_rkap_prognose_maintenance','bulan','nilai_rkap','created_by','updated_by'];
}

<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class T_OverhaulPeriode extends Model
{
    use HasFactory, Modifier, SoftDeletes;
    protected $table = 't_overhaul_periode';
    protected $fillable = [
        "id_plant",
        "start_date",
        "end_date",
        "status",
        "created_by",
        "updated_by",];

public function plant(){
return $this->belongsTo("App\Models\MPlant",'id_plant');
}
}

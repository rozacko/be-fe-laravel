<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SHEManPower extends Model
{
    use Modifier, HasFactory;
    protected $table = 'she_man_power';
    protected $fillable = [  
        'bulan',
        'tahun',
        'nm_perusahaan',
        'jml_orang',
        'status',
        'keterangan',
        'created_by',
        'updated_by',
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Modifier;
use Illuminate\Database\Eloquent\SoftDeletes;

class Module extends Model {

    use Modifier, SoftDeletes;

    public $incrementing = false;
    protected $fillable = ["id", "name", "status", "created_by", "updated_by"];

    public function features(){
        return $this->hasMany("App\Models\Feature");
    }
}

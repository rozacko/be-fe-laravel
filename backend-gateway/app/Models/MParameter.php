<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Modifier;

class MParameter extends Model
{

    use Modifier, HasFactory;
    public $incrementing = false;
    protected $table = 'm_parameter';
    protected $fillable = [
        'id',
        'name',
        'created_by',
        'updated_by',
    ];
}

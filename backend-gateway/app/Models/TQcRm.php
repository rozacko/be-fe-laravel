<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TQcRm extends Model
{
    use HasFactory, Modifier, SoftDeletes;
    protected $table = 't_qc_rm';
    protected $fillable = ['plant_id',"tanggal",'jam','lsf','sim','alm','h2o','res90','res200', 'created_by', 'updated_by'];
}

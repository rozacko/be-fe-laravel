<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TRkapPerformanceItems extends Model
{
    use HasFactory,Modifier;

    protected $table = 't_rkap_performance_items';
    protected $fillable = ['id_rkap_performance','bulan','nilai_rkap','created_by','updated_by'];
}

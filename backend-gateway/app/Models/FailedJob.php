<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FailedJob extends Model
{

    protected $appends = ['class', 'short_exception'];

    public function getClassAttribute()
    {
        $start = strpos($this->payload, ":\"") + 2;
        $length = strpos($this->payload, "\",") - $start;
        $class = str_replace("\\\\", "\\", substr($this->payload, $start, $length));
        return $class;
    }

    public function getShortExceptionAttribute()
    {
        return substr($this->exception, 0, strpos($this->exception, "Stack trace"));
    }
}

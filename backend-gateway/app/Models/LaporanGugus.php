<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Modifier;
use Illuminate\Database\Eloquent\SoftDeletes;

class LaporanGugus extends Model
{
    use Modifier, HasFactory;
    protected $table = 't_laporan_gugus';
    protected $primaryKey = 'id_laporan_gugus';

    protected $fillable = [
        'bulan',
        'tahun',
        'fasilitator',
        'gugus',
        'sga',
        'commitment_management',
        'papan_control',
        'she',
        '5r',
        'r2',
        'r3',
        'r4',
        'r5',
        'autonomous_maintenance',
        'planed_maintenance',
        'focused_improvement',
        'target',
        'nilai_SGA',
        'nilai_gugus',
        'nilai_fasilitator',
        'created_by',
        'updated_by',
    ];


    public function scopeTotalGugus($query){

    }
}

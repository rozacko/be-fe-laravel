<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SHEIzinLing extends Model
{
    use Modifier, HasFactory;
    protected $table = 'she_izin_lingkungan';
    protected $fillable = [
        'tahun',                 
        'nm_perizinan',
        'sub_perizinan',
        'p_tuban',
        'p_gresik',
        'pelabuhan_tuban',
        'pelabuhan_gresik',
        'tambang_tuban',
        'keterangan',            
        'created_by',
        'updated_by',
    ];
}

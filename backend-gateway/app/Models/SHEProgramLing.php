<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SHEProgramLing extends Model
{
    use Modifier, HasFactory;
    protected $table = 'she_program_lingkungan';
    protected $fillable = [
        'tahun',
        'program',
        'sasaran',
        'target',
        'pic',
        'created_by',
        'updated_by',
    ];
}

<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SHEMFireAlarm extends Model
{
    use Modifier, HasFactory;
    protected $table = 'she_m_fire_alarm';
    protected $fillable = [  
        'parameter',
        'tipe',        
        'created_by',
        'updated_by',
    ];
}

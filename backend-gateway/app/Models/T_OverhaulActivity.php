<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class T_OverhaulActivity extends Model
{
    use HasFactory, Modifier, SoftDeletes;
    protected $table="t_overhaul_activity";
    protected $fillable = [
                            "id_plant",
                            "id_area",
                            "activity_name",
                            "start_date",
                            "end_date",
                            "activity_category",
                            "unit_kerja",
                            "order_no",
                            "jenis_pekerjaan",
                            "judul_kontrak",
                            "budget_category",
                            "total_manpower",
                            "total_consumable",
                            "total_tool",
                            "created_by",
                            "updated_by",];

    public function plant(){
        return $this->belongsTo("App\Models\MPlant",'id_plant');
    }

    public function area(){
        return $this->belongsTo("App\Models\MArea", "id_area");
    }

    public function latestProgress(){
        return $this->hasOne("App\Models\T_OverhaulActivityProgress","id_activity")->ofMany('progress_date','max');
    }

    public function progress(){
        return $this->hasMany("App\Models\T_OverhaulActivityProgress","id_activity");
    }

    public function dokumentasi(){
        return $this->hasManyThrough("App\Models\T_OverhaulActivityDokumentasi","App\Models\T_OverhaulActivityProgress","id_activity","id_progress");
    }
}

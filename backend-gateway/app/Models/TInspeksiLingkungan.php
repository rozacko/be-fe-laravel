<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Modifier;
use Illuminate\Database\Eloquent\SoftDeletes;
use Ramsey\Uuid\Uuid;

class TInspeksiLingkungan extends Model
{
    const FORMATDATETIME = "datetime:d-m-Y H:i:s";
    use HasFactory, Modifier, SoftDeletes;
    protected $casts = [
        'created_at' => self::FORMATDATETIME,
        'updated_at' => self::FORMATDATETIME,
        'deleted_at' => self::FORMATDATETIME,
    ];

    protected $primaryKey = 'id';
    protected $table = 't_inspeksi_lingkungan';
    protected $fillable = ['departemen','area','equipment_no','uraian_temuan','tanggal_inspeksi','tanggal_closing','status','foto_temuan','foto_closing','devisi_area','unit_kerja','evaluasi','created_by', 'updated_by','uuid'];
    protected $hiddens = ["deleted_at"];

    /**
     * The "booted" method of the model.
     */
    protected static function booted(): void
    {
        static::creating(function ($model) {
            $model->uuid = Uuid::uuid4();
        });
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Modifier;
use Illuminate\Database\Eloquent\SoftDeletes;

class LaporanTPM extends Model
{

    const TPMIMPLEMENTATION ="SCORE TPM IMPLEMENTATION";
    const FOCUSIMPROVEMENT = "FOCUSED IMPROVEMENT";
    const FOCUSAREA = "FOCUS AREA";
    const GENBARATESGA = "GENBA RATE SGA";
    const PARTISIPASIMEMBERTPM = "PARTISIPASI MEMBER TPM";

    const TARGET = "Target";
    const REALISASI = "Realisasi";
    const ACHIEVEMENT = "Achievement";

    use Modifier, HasFactory;
    protected $table = 't_laporan_tpm';
    protected $primaryKey = 'id_laporan_tpm';
    protected $fillable = [
        'deskripsi',
        'parameter',
        'tahun',
        'jan',
        'feb',
        'mar',
        'q1',
        'apr',
        'mei',
        'jun',
        'q2',
        's1',
        'jul',
        'aug',
        'sep',
        'q3',
        'okt',
        'nov',
        'des',
        'q4',
        's2',
        'nilai_akhir',
        'created_by',
        'updated_by',
    ];

    public static function deskripsiTPM($tahun,$deskripsi){
        return LaporanTPM::query()->where('tahun',$tahun)
                ->where('deskripsi','like',"%".$deskripsi."%");
    }

    public function scopeRealisasi($query){
        $data = $query->where('parameter','like',LaporanTPM::REALISASI."%")
                ->select('q1','q2','s1','q3','q4','s2','nilai_akhir')->first();
        if(!is_null($data)){
            return [
                (!is_null($data->q1))?$data->q1:0,
                (!is_null($data->q2))?$data->q2:0,
                (!is_null($data->s1))?$data->s1:0,
                (!is_null($data->q3))?$data->q3:0,
                (!is_null($data->q4))?$data->q4:0,
                (!is_null($data->s2))?$data->s2:0,
                (!is_null($data->nilai_akhir))?$data->nilai_akhir:0,
            ];

        }else{
            return [
                0,
                0,
                0,
                0,
                0,
                0,
                0
            ];
        }
    }

    public function scopeTarget($query){
        $data = $query->where('parameter','like',LaporanTPM::TARGET."%")
        ->select('q1','q2','s1','q3','q4','s2','nilai_akhir')->first();

        if(!is_null($data)){
            return [
                (!is_null($data->q1))?$data->q1:0,
                (!is_null($data->q2))?$data->q2:0,
                (!is_null($data->s1))?$data->s1:0,
                (!is_null($data->q3))?$data->q3:0,
                (!is_null($data->q4))?$data->q4:0,
                (!is_null($data->s2))?$data->s2:0,
                (!is_null($data->nilai_akhir))?$data->nilai_akhir:0,
            ];

        }else{
            return [
                0,
                0,
                0,
                0,
                0,
                0,
                0
            ];
        }
    }

    public function scopeAchievement($query){
       $data = $query->where('parameter','like',LaporanTPM::ACHIEVEMENT."%")
        ->select('q1','q2','s1','q3','q4','s2','nilai_akhir')->first();

        if(!is_null($data)){
            return [
                (!is_null($data->q1))?$data->q1:0,
                (!is_null($data->q2))?$data->q2:0,
                (!is_null($data->s1))?$data->s1:0,
                (!is_null($data->q3))?$data->q3:0,
                (!is_null($data->q4))?$data->q4:0,
                (!is_null($data->s2))?$data->s2:0,
                (!is_null($data->nilai_akhir))?$data->nilai_akhir:0,
            ];
        }else{
            return [
                0,
                0,
                0,
                0,
                0,
                0,
                0
            ];
        }
    }

}

<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class M_TagList extends Model
{
    use HasFactory, Modifier, SoftDeletes;
    protected $table = "m_taglist";
    protected $fillable = ["id_jenis_opc","id_api","id_plant","tag_name","sync_period","sync_time",
                            "hourly_type","hourly_factor","daily_type","daily_factor",
                            "created_by", "updated_by"];

    public function jenis(){
        return $this->belongsTo("App\Models\M_JenisOpc",'id_jenis_opc');
    }

    public function api(){
        return $this->belongsTo("App\Models\M_OpcApi",'id_api');
    }

    public function plant(){
        return $this->belongsTo("App\Models\MPlant",'id_plant');
    }

    public function tag_value(){
        return $this->hasMany("App\Models\T_PenarikanOpc","id_tag");
    }

    public function latestValue(){
        return $this->hasOne("App\Models\T_PenarikanOpc","id_opc")->ofMany('waktu_penarikan','max');
    }

    public function tag_val_date($tgl){
        return $this->hasOne("App\Models\T_PenarikanOpc","id_tag")->ofMany([
            'waktu_penarikan' => 'max',
            'id' => 'max',
        ], function ($query) use($tgl) {
            $query->where('waktu_penarikan',\Carbon\Carbon::createFromFormat('d/m/Y',$tgl)->format('Y-m-d'));
        });
    }
}

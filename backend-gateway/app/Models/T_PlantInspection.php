<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class T_PlantInspection extends Model
{
    use HasFactory, Modifier, SoftDeletes;
    protected $table = "t_plant_inspection";

    protected $fillable = [
        'id_plant',
        'kode_inspection',
        'tgl_inspection',
        'desc_inspection',
        'kode_opco',
        'kode_plant',
        'status',
        'id_area',
        'desc_area',
        'remark',
        'id_fungsi',
        'created_by',
        'updated_by'
    ];


    // public function plant(){
    //     return $this->belongsTo("App\Models\MPlant",'id_plant');
    // }

    // public function area(){
    //     return $this->belongsTo("App\Models\MArea", "id_area");
    // }

    // public function unitkerja(){
    //     return $this->belongsTo("App\Models\MWorkgroup", "id_unitkerja");
    // }
}

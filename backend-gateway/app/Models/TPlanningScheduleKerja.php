<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Modifier;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

class TPlanningScheduleKerja extends Model
{
    use HasFactory, Modifier, SoftDeletes;
    protected $casts = [
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
    ];

    protected $primaryKey = 'id_planning_schedule_kerja';
    protected $table = 't_planning_schedule_kerja';
    protected $fillable = ['no_pegawai', 'nm_pegawai', 'spesifikasi', 'perusahaan', 'tahun', 'bulan', 'tgl_1', 'tgl_2', 'tgl_3', 'tgl_4', 'tgl_5', 'tgl_6', 'tgl_7', 'tgl_8', 'tgl_9', 'tgl_10', 'tgl_11', 'tgl_12', 'tgl_13', 'tgl_14', 'tgl_15', 'tgl_16', 'tgl_17', 'tgl_18', 'tgl_19', 'tgl_20', 'tgl_21', 'tgl_22', 'tgl_23', 'tgl_24', 'tgl_25', 'tgl_26', 'tgl_27', 'tgl_28', 'tgl_29', 'tgl_30', 'tgl_31', 'created_by', 'updated_by'];
    protected $appends = array('bulan_text');

    public function getBulanTextAttribute()
    {
        return monthNumberToIndonesiaMonth($this->bulan);
    }

    public function countDataByNoPegawai($arrPegawai, $bulan, $tahun)
    {
        $query = TPlanningScheduleKerja::select(DB::raw('count(distinct no_pegawai) as total_planning,no_pegawai'))
                ->where(['tahun' => $tahun, 'bulan' => $bulan])
                ->whereIn('no_pegawai', $arrPegawai)
                ->groupBy('no_pegawai');
        return $query->get();
    }

    /**
     * The "booted" method of the model.
     */
    protected static function booted(): void
    {
        static::creating(function ($model) {
            $model->uuid = Uuid::uuid4();
        });
    }
}

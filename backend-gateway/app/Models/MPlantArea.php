<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MPlantArea extends Model
{
    use Modifier, HasFactory;
    protected $table = 'm_plant_area';
    protected $fillable = [  
        'kd_area',
        'nm_area',
        'plant_id',
        'ref_sap',
        'ref_opc',
        'created_by',
        'updated_by',
    ];
}

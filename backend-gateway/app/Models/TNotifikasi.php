<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TNotifikasi extends Model
{
    use HasFactory, Modifier,SoftDeletes;
    protected $table = 't_notifikasi';
    protected $fillable = [
        'notif_date',
        'notif_time',
        'system_status',
        'notification',
        'order',
        'functional_loc',
        'description',
        'priority',
        'notification_type',
        'reported_by',
        'created_user',
        'changed_user',
        'main_work_ctr',
        'planner_group',
        'plant_section',
        'main_plant',
        'equipment',
        'cost_center',
        'reference_date',
        'created_on',
        'today',
        'umur_notif',
        'notif_aging',
        'description_standar',
        'notif_convert_to_order',
        'notif_close',
        'klasifikasi_uk',
        "created_by","updated_by","deleted_by"
    ];
}

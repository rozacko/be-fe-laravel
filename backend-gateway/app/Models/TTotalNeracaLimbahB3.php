<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Ramsey\Uuid\Uuid;
use App\Traits\Modifier;

class TTotalNeracaLimbahB3 extends Model
{
    const FORMATDATETIME = "datetime:d-m-Y H:i:s";
    use HasFactory, SoftDeletes, Modifier;
    protected $casts = [
        'created_at' => self::FORMATDATETIME,
        'updated_at' => self::FORMATDATETIME,
        'deleted_at' => self::FORMATDATETIME,
    ];

    protected $primaryKey = 'id';
    protected $table = 't_total_neraca_limbahb3';
    protected $fillable = ["plant","id_jenis_limbah","tahun","bulan",'ton_masuk','ton_keluar','ton_sisa_tps', 'created_by', 'updated_by','uuid'];
    
    /**
     * The "booted" method of the model.
     */
    protected static function booted(): void
    {
        static::creating(function ($model) {
            $model->uuid = Uuid::uuid4();
        });
    }

    public function jenislimbah()
    {
        return $this->belongsTo(MJenisLimbahEnviro::class, 'id_jenis_limbah', 'id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Ramsey\Uuid\Uuid;
use App\Traits\Modifier;

class TNeracaLimbahB3 extends Model
{
    const FORMATDATETIME = "datetime:d-m-Y H:i:s";
    use HasFactory, SoftDeletes, Modifier;
    protected $casts = [
        'created_at' => self::FORMATDATETIME,
        'updated_at' => self::FORMATDATETIME,
        'deleted_at' => self::FORMATDATETIME,
    ];
    protected $primaryKey = 'id';
    protected $table = 't_neraca_limbahb3';
    protected $fillable = ["plant", "id_jenis_limbah", "tanggal_masuk", "sumber_masuk", "jml_ton_masuk", "masa_simpan_masuk", "tanggal_keluar", "sumber_keluar", "jml_ton_keluar", "masa_simpan_keluar", "sisa_tps", 'created_by', 'updated_by', 'uuid'];

    /**
     * The "booted" method of the model.
     */
    protected static function booted(): void
    {
        static::creating(function ($model) {
            $model->uuid = Uuid::uuid4();
        });
    }

    public function jenislimbah()
    {
        return $this->belongsTo(MJenisLimbahEnviro::class, 'id_jenis_limbah', 'id');
    }
}

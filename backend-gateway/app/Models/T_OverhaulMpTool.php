<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class T_OverhaulMpTool extends Model
{
    use HasFactory, Modifier, SoftDeletes;
    protected $table = "t_overhaul_preparation_mptool";
    protected $fillable = [
                        "id_plant",
                        "id_area",
                        "order_no",
                        "activity_name",
                        "jenis_pekerjaan",
                        "id_unitkerja",
                        "judul_kontrak",
                        "tgl_masuk",
                        "total_manpower",
                        "total_consumable",
                        "total_tool",
                        "order_date",
                        "created_by",
                        "updated_by",];

    public function plant(){
        return $this->belongsTo("App\Models\MPlant",'id_plant');
    }

    public function area(){
        return $this->belongsTo("App\Models\MArea", "id_area");
    }

    public function unitkerja(){
        return $this->belongsTo("App\Models\MWorkgroup", "id_unitkerja");
    }
}

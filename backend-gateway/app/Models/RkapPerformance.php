<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Modifier;

class RkapPerformance extends Model
{
    use Modifier, HasFactory;
    protected $table = 't_rkap_performance';
    protected $fillable = [
        'id_plant',
        'tahun',
        'bulan',
        'id_parameter',
        'nilai_rkap',
        'created_by',
        'updated_by',
    ];
}

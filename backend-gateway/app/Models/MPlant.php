<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MPlant extends Model
{
    use Modifier, HasFactory;
    protected $table = 'm_plant';
    protected $fillable = [  
        'kd_plant',
        'nm_plant',
        'ref_sap',
        'created_by',
        'updated_by',
    ];
}

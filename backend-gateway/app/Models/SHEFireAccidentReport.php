<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SHEFireAccidentReport extends Model
{
    use Modifier, HasFactory;
    protected $table = 'she_fire_accident_report';
    protected $fillable = [  
        'tgl_doc',
        'accident',
        'location',
        'person_status',
        'investigation_team',
        'report_prepared',
        'report_reviewed',
        'report_approved',
        'file',
        'status',
        'type',
        'created_by',
        'updated_by',
    ];
}

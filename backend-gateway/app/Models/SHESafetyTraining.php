<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SHESafetyTraining extends Model
{
    use Modifier, HasFactory;
    protected $table = 'she_safety_training';
    protected $fillable = [  
        'tgl_training',
        'judul_training',
        'created_by',
        'updated_by',
    ];
}

<?php

namespace App\Models;

use App\Traits\Modifier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MWorkgroup extends Model
{
    use Modifier, HasFactory;
    protected $table = 'm_workgroup';
    protected $fillable = [        
      'unit_code',
      'unit_name',
      'parent_code',
      'lv_wg',
      'created_by',
      'updated_by',
  ];
}

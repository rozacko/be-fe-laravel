<?php

namespace App\Http\Controllers\LimbahB3;

use App\Exports\NeracaLimbahExport;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MJenisLimbahEnviro;
use App\Models\TNeracaLimbahB3;
use App\Models\TTotalNeracaLimbahB3;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class NeracaLimbahB3Controller extends Controller
{
    /**
     * @OA\Post(
     *   tags={"Neraca Limbah B3"},
     *   path="/api/neraca-limbah-b3/list",
     *   summary="Get neraca limbah b3 list",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="plant", in="query", description="isikan plant (tuban/gresik)", required=true, @OA\Schema( type="string" )),
     *   @OA\Parameter(name="id_jenis_limbah", in="query", description="parameter id_jenis_limbah", required=true, @OA\Schema( type="string" )),
     *   @OA\Parameter(name="tahun", in="query", description="parameter tahun", required=true, @OA\Schema( type="string" )),
     *   @OA\Parameter(name="bulan", in="query", description="parameter bulan", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     *   @OA\Response(response=500, description="Internal Server Error"),
     * )
     */
    public function index(Request $request)
    {
        $this->validate($request, [
            'id_jenis_limbah' => 'required',
            'tahun' => 'required',
            'bulan' => 'required',
        ]);

        try {
            $plant = strtolower($request->plant);
            $idJenisLimbah = $request->id_jenis_limbah;
            $tahun = $request->tahun;
            $bulan = $request->bulan;

            //validate id_jenis_limbah
            $dataIdJenisLimbah = MJenisLimbahEnviro::find($idJenisLimbah);
            if ($dataIdJenisLimbah == null) {
                $response = responseFail(__('messages.read-fail'), 'data jenis limbah tidak ditemukan');
                return response()->json($response, Response::HTTP_BAD_REQUEST);
            }

            $data = $this->getDataIndex($idJenisLimbah, $plant, $tahun, $bulan);

            $response = responseSuccess(__('messages.read-success'), $data);
            return response()->json($response);
        } catch (\Exception $ex) {
            $response = responseFail(__('messages.show-import-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function getDataIndex($idJenisLimbah, $plant, $tahun, $bulan)
    {
        try {
            $query = TNeracaLimbahB3::query();
            $dataNeraca = $query->where("id_jenis_limbah", $idJenisLimbah)
                ->whereRaw('lower(plant) = ?', $plant)
                ->whereRaw('EXTRACT(YEAR FROM tanggal_masuk) = ?', $tahun)
                ->whereRaw('EXTRACT(MONTH FROM tanggal_masuk) = ?', $bulan)
                ->get();
            $dataNeraca->load('jenislimbah');

            $dataTotal = TTotalNeracaLimbahB3::with('jenislimbah')->where(['plant' => $plant, "id_jenis_limbah" => $idJenisLimbah, 'tahun' => $tahun, 'bulan' => $bulan])->first();

            $arrDataTotal = [];
            if ($dataTotal != null) {
                $arrDataTotal[] = $dataTotal;
            }

            $dataBulanLalu = $this->getDataOneMonthBefore($plant, $idJenisLimbah, $tahun, $bulan);
            $sisaBulanLalu = 0;
            if ($dataBulanLalu != null) {
                $sisaBulanLalu = $dataBulanLalu->ton_sisa_tps;
            }

            return ["data" => $dataNeraca, "data_total" => $arrDataTotal, "sisa_bulan_lalu" => $sisaBulanLalu];
        } catch (\Exception $ex) {
            return ["data" => [], "data_total" => [], "sisa_bulan_lalu" => []];
        }
    }

    public function getDataOneMonthBefore($plant, $idJenisLimbah, $tahun, $bulan)
    {
        try {
            $monthString = $tahun . "-" . $bulan;
            $timestamp = strtotime($monthString);
            $dateTime = new \DateTime();
            $dateTime->setTimestamp($timestamp);

            // Subtract one month from the specified month
            $previousMonth = $dateTime->sub(new \DateInterval('P1M'));
            $monthNumber = $previousMonth->format('m');
            $yearNumber = $previousMonth->format('Y');
            return TTotalNeracaLimbahB3::with('jenislimbah')->where(['plant' => $plant, "id_jenis_limbah" => $idJenisLimbah, 'tahun' => $yearNumber, 'bulan' => $monthNumber])->first();
        } catch (\Exception $ex) {
            return [];
        }
    }
    /**
     * @OA\Post(
     *   tags={"Neraca Limbah B3"},
     *   path="/api/neraca-limbah-b3/store",
     *   summary="create neraca limbah b3",
     *   security={{"token": {}}},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(
     *                 @OA\Property(
     *                     property="plant",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="id_jenis_limbah",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="tanggal_limbah",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="sumber_limbah",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="jumlah_limbah",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="masa_simpan_limbah",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="tanggal_penyimpanan",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="sumber_penyimpanan",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="jumlah_penyimpanan",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="masa_simpan_produk",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="sisa",
     *                     type="string"
     *                 )
     *             )
     *         )
     *     ),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     *   @OA\Response(response=500, description="Internal Server Error"),
     * )
     */
    public function store(Request $request)
    {
        try {
            $dataNeraca = $request->json()->all();
            $totalMasuk = 0;
            $totalKeluar = 0;
            $totalSisa = 0;
            DB::beginTransaction();
            foreach ($dataNeraca as $key => $value) {
                $plant = $value['plant'];
                $idJenisLimbah = $value['id_jenis_limbah'];
                $tanggalMasuk = $value['tanggal_limbah'];
                $sumberMasuk = $value['sumber_limbah'];
                $jmlTonMasuk = $value['jumlah_limbah'];
                $masaSimpanMasuk = $value['masa_simpan_limbah'];
                $tanggalKeluar = $value['tanggal_penyimpanan'];
                $sumberKeluar = $value['sumber_penyimpanan'];
                $jmlTonKeluar = $value['jumlah_penyimpanan'];
                $masaSimpanKeluar = $value['masa_simpan_produk'];
                $sisaTps = $value['sisa'];

                $checkData = $this->validateData($value);
                if (strlen($checkData) > 0) {
                    DB::rollBack();
                    $response = responseFail(__('messages.read-fail'), "data tidak valid pada tanggal " . $value['tanggal_limbah'] . "," . $checkData);
                    return response()->json($response, Response::HTTP_BAD_REQUEST);
                }
                $arrUpdateIF = ['plant' => $plant, "id_jenis_limbah" => $idJenisLimbah, "tanggal_masuk" => $tanggalMasuk];
                $arrUpdateData = ["sumber_masuk" => $sumberMasuk, "jml_ton_masuk" => $jmlTonMasuk, "masa_simpan_masuk" => $masaSimpanMasuk, "tanggal_keluar" => $tanggalKeluar, "sumber_keluar" => $sumberKeluar, "jml_ton_keluar" => $jmlTonKeluar, "masa_simpan_keluar" => $masaSimpanKeluar, "sisa_tps" => $sisaTps];

                $result = TNeracaLimbahB3::updateOrCreate($arrUpdateIF, $arrUpdateData);
                if (!$result) {
                    DB::rollBack();
                    $response = responseSuccess(__('messages.create-fail'), "error ketika tambah data pada tanggal" . $value['tanggal_limbah']);
                    return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
                }

                $totalMasuk = $totalMasuk + $jmlTonMasuk;
                $totalKeluar = $totalKeluar + $jmlTonKeluar;
                $totalSisa = $totalSisa + $sisaTps;
            }

            $arrTanggalMasuk = explode("-", $tanggalMasuk);
            $tahun = $arrTanggalMasuk[0];
            $bulan = $arrTanggalMasuk[1];
            $arrUpdateIF = ['plant' => $plant, "id_jenis_limbah" => $idJenisLimbah, "tahun" => $tahun, "bulan" => $bulan];
            $arrUpdateData = ["ton_masuk" => $totalMasuk, "ton_keluar" => $totalKeluar, "ton_sisa_tps" => $totalSisa];

            $result = TTotalNeracaLimbahB3::updateOrCreate($arrUpdateIF, $arrUpdateData);
            if (!$result) {
                DB::rollBack();
                $response = responseSuccess(__('messages.create-fail'), "error ketika kalkulasi total");
                return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
            }

            DB::commit();
            $response = responseSuccess(__('messages.create-success'));
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollBack();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    function validateJenisLimbah($idJenisLimbah)
    {
        if (strlen($idJenisLimbah) > 0) {
            $dataIdJenisLimbah = MJenisLimbahEnviro::find($idJenisLimbah);
            if ($dataIdJenisLimbah == null) {
                return 'data jenis limbah tidak ditemukan';
            }
            return "";
        }
        return "data jenis limbah tidak boleh kosong";
    }

    protected function validateDataFormat($inputDate, $column)
    {
        $valueFormat = 'Y-m-d';
        if (strlen($inputDate) == 0) {
            return "data " . $column . " tidak boleh kosong";
        } else {
            if (!validateDate($inputDate, $valueFormat)) {
                return "format " . $column . " tidak sesuai YYYY-MM-DD";
            }
        }
    }

    protected function validateData($row)
    {
        $idJenisLimbah = $row['id_jenis_limbah'];
        $error = $this->validateJenisLimbah($idJenisLimbah);
        if (strlen($error) > 0) {
            return $error;
        }

        $jmlTonMasuk = $row['jumlah_limbah'];
        $jmlTonKeluar = $row['jumlah_penyimpanan'];
        $sisaTps = $row['sisa'];

        if (!is_numeric($jmlTonMasuk)) {
            return "jumlah_limbah harus berupa angka";
        }

        if (!is_numeric($jmlTonKeluar)) {
            return "jumlah_penyimpanan harus berupa angka";
        }

        if (!is_numeric($sisaTps)) {
            return "sisa harus berupa angka";
        }
        $tanggalMasuk = $row['tanggal_limbah'];
        $masaSimpanMasuk = $row['masa_simpan_limbah'];
        $tanggalKeluar = $row['tanggal_penyimpanan'];
        $masaSimpanKeluar = $row['masa_simpan_produk'];

        $error = $this->validateDataFormat($tanggalMasuk, "tanggal_limbah");
        if (strlen($error) > 0) {
            return $error;
        }

        $error = $this->validateDataFormat($masaSimpanMasuk, "masa_simpan_limbah");
        if (strlen($error) > 0) {
            return $error;
        }

        $error = $this->validateDataFormat($tanggalKeluar, "tanggal_penyimpanan");
        if (strlen($error) > 0) {
            return $error;
        }

        $error = $this->validateDataFormat($masaSimpanKeluar, "masa_simpan_produk");
        if (strlen($error) > 0) {
            return $error;
        }
    }

    /**
     * @OA\Get(
     *   tags={"Neraca Limbah B3"},
     *   path="/api/neraca-limbah-b3/export",
     *   summary="Get neraca limbah b3 list",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="plant", in="query", description="isikan plant (tuban/gresik)", required=true, @OA\Schema( type="string" )),
     *   @OA\Parameter(name="id_jenis_limbah", in="query", description="parameter id_jenis_limbah", required=true, @OA\Schema( type="string" )),
     *   @OA\Parameter(name="tahun", in="query", description="parameter tahun", required=true, @OA\Schema( type="string" )),
     *   @OA\Parameter(name="bulan", in="query", description="parameter bulan", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     *   @OA\Response(response=500, description="Internal Server Error"),
     * )
     */
    public function exportData(Request $request)
    {
        $this->validate($request, [
            'id_jenis_limbah' => 'required',
            'tahun' => 'required',
            'bulan' => 'required',
        ]);

        try {
            $plant = strtolower($request->plant);
            $idJenisLimbah = $request->id_jenis_limbah;
            $tahun = $request->tahun;
            $bulan = $request->bulan;

            //validate id_jenis_limbah
            $dataIdJenisLimbah = MJenisLimbahEnviro::find($idJenisLimbah);
            if ($dataIdJenisLimbah == null) {
                $response = responseFail(__('messages.read-fail'), 'data jenis limbah tidak ditemukan');
                return response()->json($response, Response::HTTP_BAD_REQUEST);
            }
            $namaLimbah = $dataIdJenisLimbah->nama_limbah;
            $data = $this->getDataIndex($idJenisLimbah, $plant, $tahun, $bulan);

            if (count($data['data_total']) == 0) {
                $data['data_total'][0]['ton_masuk'] = 0;
                $data['data_total'][0]['ton_keluar'] = 0;
                $data['data_total'][0]['ton_sisa_tps'] = 0;
            }
            $fileName = 'neraca-limbah-b3-' . $plant . "-" . $namaLimbah . "-" . $request->bulan . '-' . $request->tahun . date('YmdHis');
            return Excel::download((new NeracaLimbahExport($data)), $fileName . '.xlsx');
        } catch (\Exception $ex) {
            $response = responseFail(__('messages.show-import-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}

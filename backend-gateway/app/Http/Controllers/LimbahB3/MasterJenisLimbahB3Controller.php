<?php

namespace App\Http\Controllers\LimbahB3;

use App\Http\Controllers\Controller;
use App\Models\MJenisLimbahB3;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Resources\LimbahB3\MasterJenisLimbahB3ListCollection;
use App\Models\MPlant;
use Illuminate\Support\Facades\DB;

class MasterJenisLimbahB3Controller extends Controller
{
    /**
     * @OA\Get(
     *   tags={"JENIS LIMBAH B3"},
     *   path="/api/master-jenis-limbah/list",
     *   summary="Get master jenis limbah list",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="plant", in="query", description="parameter plant", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     *   @OA\Response(response=500, description="Internal Server Error"),
     * )
     */
    public function index(Request $request)
    {
        $this->validate($request, [
            'plant' => 'required'
        ]);
        try {
            $idPlant = $request->plant;
            $query = MJenisLimbahB3::query();
            $model = $query->where('plant_id', $idPlant)->get();
            $data = new MasterJenisLimbahB3ListCollection($model);
            $response = responseSuccess(__('messages.read-success'), $data);
            return response()->json($response);
        } catch (\Exception $ex) {
            $response = responseFail(__('messages.show-import-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Post(
     *   tags={"JENIS LIMBAH B3"},
     *   path="/api/master-jenis-limbah/store",
     *   summary="Get master jenis limbah list",
     *   security={{"token": {}}},
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"plant","jenisLimbahB3","sumber"},
     *       @OA\Property(property="plant", type="string", format="text", example="1"),
     *       @OA\Property(property="jenisLimbahB3", type="string", format="text", example="oli bekas"),
     *       @OA\Property(property="sumber", type="string", format="text", example="sumber"),
     *     )
     *   ),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     *   @OA\Response(response=500, description="Internal Server Error"),
     * )
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'plant' => 'required',
            'jenisLimbahB3' => 'required',
            'sumber' => 'required'
        ]);
        try {
            $idPlant = $request->plant;
            $jenisLimbahB3 = $request->jenisLimbahB3;
            $sumber = $request->sumber;

            $plantData = MPlant::find($idPlant);
            if ($plantData == null) {
                $response = responseFail(__('messages.validation-fail'), ['plant tidak ditemukan']);
                return response()->json($response, Response::HTTP_BAD_REQUEST);
            }

            DB::beginTransaction();
            $mJenisModel = new MJenisLimbahB3;
            $mJenisModel->plant_id = $idPlant;
            $mJenisModel->jenis_limbah_b3 = $jenisLimbahB3;
            $mJenisModel->sumber = $sumber;
            $mJenisModel->save();
            DB::commit();

            $response = responseSuccess(__('messages.create-success'));
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollBack();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *   tags={"JENIS LIMBAH B3"},
     *   path="/api/master-jenis-limbah/edit/{uuid}",
     *   summary="Get detail jenis limbah b3",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="uuid", in="path", description="parameter uuid", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     *   @OA\Response(response=500, description="Internal Server Error"),
     * )
     */
    public function show($uuid)
    {
        $this->isValidUuid($uuid);
        try {
            $model = MJenisLimbahB3::where('uuid', $uuid)->firstOrFail();
            $dataModel = [];
            if ($model != null) {
                $dataModel[] = $model;
            }

            $data = new MasterJenisLimbahB3ListCollection($dataModel);
            $response = responseSuccess(__('messages.read-success'), $data);
            return response()->json($response);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            $response = responseFail(__('messages.read-fail'));
            return response()->json($response, Response::HTTP_BAD_REQUEST);
        } catch (\Exception $ex) {
            $response = responseFail(__('messages.read-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Put(
     *   tags={"JENIS LIMBAH B3"},
     *   path="/api/master-jenis-limbah/update/{uuid}",
     *   summary="update master jenis limbah B3",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="uuid", in="path", description="parameter uuid", required=true, @OA\Schema( type="string" )),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"plant","jenisLimbahB3","sumber"},
     *       @OA\Property(property="plant", type="string", format="text"),
     *       @OA\Property(property="jenisLimbahB3", type="string", format="text"),
     *       @OA\Property(property="sumber", type="string", format="text"),
     *     )
     *   ),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     *   @OA\Response(response=500, description="Internal Server Error"),
     * )
     */
    public function update(string $uuid, Request $request)
    {
        $this->validate($request, [
            'plant' => 'required',
            'jenisLimbahB3' => 'required',
            'sumber' => 'required'
        ]);
        try {
            $idPlant = $request->plant;
            $jenisLimbahB3 = $request->jenisLimbahB3;
            $sumber = $request->sumber;
            
            DB::beginTransaction();
            $mJenisModel = MJenisLimbahB3::where('uuid', $uuid)->firstOrFail();
            $mJenisModel->plant_id = $idPlant;
            $mJenisModel->jenis_limbah_b3 = $jenisLimbahB3;
            $mJenisModel->sumber = $sumber;
            $mJenisModel->save();
            DB::commit();

            $response = responseSuccess(__('messages.update-success'));
            return response()->json($response);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            DB::rollBack();
            $response = responseFail(__('messages.read-fail'));
            return response()->json($response, Response::HTTP_BAD_REQUEST);
        } catch (\Exception $ex) {
            DB::rollBack();
            $response = responseFail(__('messages.update-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Delete(
     *   tags={"JENIS LIMBAH B3"},
     *   path="/api/master-jenis-limbah/delete/{uuid}",
     *   summary="delete master jenis limbah b3 by uuid",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="uuid", in="path", description="uuid that needs to be delete", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     *   @OA\Response(response=500, description="Internal Server Error"),
     * )
     */
    public function delete($uuid)
    {
        $this->isValidUuid($uuid);
        $model = MJenisLimbahB3::where('uuid', $uuid)->firstOrFail();
        try {
            DB::beginTransaction();
            $model->delete();
            DB::commit();
            $response = responseSuccess(__('messages.delete-success'), $model);
            return response()->json($response);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            DB::rollBack();
            $response = responseFail(__('messages.read-fail'));
            return response()->json($response, Response::HTTP_BAD_REQUEST);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.delete-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}

<?php

namespace App\Http\Controllers\DokumenLingkungan;

use App\Http\Controllers\Controller;
use App\Http\Resources\DokumenLingkungan\MasterDokumenLingkunganListCollection;
use App\Models\MAreaEnviro;
use App\Models\MJenisDokumenEnviro;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use App\Models\TDokumenLingkungan;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Storage;

class MasterDokumenLingkunganController extends Controller
{
    /**
     * @OA\Get(
     *   tags={"DOKUMEN LINGKUNGAN"},
     *   path="/api/dokumen-lingkungan/list",
     *   summary="Get dokumen lingkungan list",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="area", in="query", description="isikan nama area bukan id_area", required=true, @OA\Schema( type="string" )),
     *   @OA\Parameter(name="tahun", in="query", description="parameter tahun", required=true, @OA\Schema( type="string" )),
     *   @OA\Parameter(name="keyword", in="query", description="parameter keyword", required=false, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     *   @OA\Response(response=500, description="Internal Server Error"),
     * )
     */
    public function index(Request $request)
    {
        $this->validate($request, [
            'area' => 'required',
            'tahun' => 'required',
        ]);
        try {
            $area = strtolower($request->area);
            $tahun = $request->tahun;

            $query = TDokumenLingkungan::query();
            $arrSelect = ["id", "uuid", "judul", "area_id", "nm_area", "tahun", "no_dokumen", "ruang_lingkup", "filename", "jenis_dokumen"];
            $query = $query->select(...$arrSelect);
            $isNotEmptyKeyword = ($request->keyword != null || strlen(trim($request->keyword) > 0));
            if ($request->has('keyword') && $isNotEmptyKeyword) {
                $keyword = strtolower($request->keyword);
                $query->where(function ($query) use ($keyword) {
                    $keyword = "%" . $keyword . "%";
                    $query->whereRaw('lower(judul) like ?', $keyword);
                });
            }

            $model = $query->whereRaw('lower(nm_area) = ?', $area)
                ->where('tahun', $tahun);

            $model =  DataTables::of($query)
                ->addIndexColumn()
                ->editColumn('filename', function ($row) {
                    $file = "";
                    if ($row->filename != null && strlen($row->filename) > 0) {
                        $file = Storage::disk('minio')->url($row->filename);
                    }
                    return $file;
                })
                ->make(true)
                ->getData(true);

            $response = responseDatatableSuccess(__('messages.read-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            $response = responseFail(__('messages.show-import-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Post(
     *   tags={"DOKUMEN LINGKUNGAN"},
     *   path="/api/dokumen-lingkungan/store",
     *   summary="create dokumen lingkungan",
     *   security={{"token": {}}},
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"id_area","judul","no_dokumen","ruang_lingkup","jenis_dokumen","tahun"},
     *       @OA\Property(property="judul", type="string", format="text"),
     *       @OA\Property(property="no_dokumen", type="string", format="text"),
     *       @OA\Property(property="id_area", type="string", format="text"),
     *       @OA\Property(property="ruang_lingkup", type="string", format="text"),
     *       @OA\Property(property="jenis_dokumen", type="string", format="text"),
     *       @OA\Property(property="tahun", type="string", format="text"),
     *       @OA\Property(property="filename", type="string", format="text"),
     *     )
     *   ),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     *   @OA\Response(response=500, description="Internal Server Error"),
     * )
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'judul' => 'required',
            'no_dokumen' => 'required',
            'id_area' => 'required',
            'ruang_lingkup' => 'required',
            'jenis_dokumen' => 'required',
            'tahun' => 'required',
            'filename' => '',
        ]);
        try {
            $idArea = $request->id_area;
            $judul = $request->judul;
            $noDokumen = $request->no_dokumen;
            $ruangLingkup = $request->ruang_lingkup;
            $jenisDokumen = $request->jenis_dokumen;
            $tahun = $request->tahun;
            $filename = $request->filename;

            $namaArea = "";
            $dataArea = MAreaEnviro::find($idArea);
            if ($dataArea == null) {
                $response = responseFail(__('messages.read-fail'), 'data area tidak ditemukan');
                return response()->json($response, Response::HTTP_BAD_REQUEST);
            }
            $namaArea = $dataArea->nama_area;

            $dataJenisDokumen = MJenisDokumenEnviro::where('nama_dokumen', $jenisDokumen)->first();
            if ($dataJenisDokumen == null) {
                $response = responseFail(__('messages.read-fail'), 'jenis dokumen tidak ditemukan');
                return response()->json($response, Response::HTTP_BAD_REQUEST);
            }

            DB::beginTransaction();
            $tDokumenLingkungan = new TDokumenLingkungan;
            $tDokumenLingkungan->area_id = $idArea;
            $tDokumenLingkungan->nm_area = $namaArea;
            $tDokumenLingkungan->judul = $judul;
            $tDokumenLingkungan->no_dokumen = $noDokumen;
            $tDokumenLingkungan->ruang_lingkup = $ruangLingkup;
            $tDokumenLingkungan->jenis_dokumen = $jenisDokumen;
            $tDokumenLingkungan->tahun = $tahun;
            $tDokumenLingkungan->filename = $filename;
            $tDokumenLingkungan->save();
            DB::commit();

            $response = responseSuccess(__('messages.create-success'));
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollBack();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *   tags={"DOKUMEN LINGKUNGAN"},
     *   path="/api/dokumen-lingkungan/edit/{uuid}",
     *   summary="Get detail dokumen lingkungan",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="uuid", in="path", description="parameter uuid", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     *   @OA\Response(response=500, description="Internal Server Error"),
     * )
     */
    public function show($uuid)
    {
        $this->isValidUuid($uuid);
        try {
            $model = TDokumenLingkungan::where('uuid', $uuid)->firstOrFail();
            $dataModel = [];
            if ($model != null) {
                $dataModel[] = $model;
            }

            $data = new MasterDokumenLingkunganListCollection($dataModel);
            $response = responseSuccess(__('messages.read-success'), $data);
            return response()->json($response);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            $response = responseFail(__('messages.read-fail'));
            return response()->json($response, Response::HTTP_BAD_REQUEST);
        } catch (\Exception $ex) {
            $response = responseFail(__('messages.read-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Put(
     *   tags={"DOKUMEN LINGKUNGAN"},
     *   path="/api/dokumen-lingkungan/update/{uuid}",
     *   summary="update master dokumen lingkungan",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="uuid", in="path", description="parameter uuid", required=true, @OA\Schema( type="string" )),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"id_area","judul","no_dokumen","ruang_lingkup","jenis_dokumen","tahun"},
     *       @OA\Property(property="id_area", type="string",description="isikan id area", format="text"),
     *       @OA\Property(property="judul", type="string", format="text"),
     *       @OA\Property(property="no_dokumen", type="string", format="text"),
     *       @OA\Property(property="ruang_lingkup", type="string", format="text"),
     *       @OA\Property(property="jenis_dokumen", type="string", description="isikan nama jenis dokumen" ,format="text"),
     *       @OA\Property(property="filename", type="string", format="text"),
     *       @OA\Property(property="tahun", type="string", format="text"),
     *     )
     *   ),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     *   @OA\Response(response=500, description="Internal Server Error"),
     * )
     */
    public function update(string $uuid, Request $request)
    {
        $this->validate($request, [
            'id_area' => 'required',
            'judul' => 'required',
            'no_dokumen' => 'required',
            'ruang_lingkup' => 'required',
            'jenis_dokumen' => 'required',
            'tahun' => 'required',
            'filename' => '',
        ]);
        try {
            $idArea = $request->id_area;
            $judul = $request->judul;
            $noDokumen = $request->no_dokumen;
            $ruangLingkup = $request->ruang_lingkup;
            $jenisDokumen = $request->jenis_dokumen;
            $filename = $request->filename;
            $tahun = $request->tahun;

            $namaArea = "";
            $dataArea = MAreaEnviro::find($idArea);
            if ($dataArea == null) {
                $response = responseFail(__('messages.read-fail'), 'data area tidak ditemukan');
                return response()->json($response, Response::HTTP_BAD_REQUEST);
            }
            $namaArea = $dataArea->nama_area;

            $dataJenisDokumen = MJenisDokumenEnviro::whereRaw('lower(nama_dokumen) = ?', strtolower($jenisDokumen))->first();
            if ($dataJenisDokumen == null) {
                $response = responseFail(__('messages.read-fail'), 'jenis dokumen tidak ditemukan');
                return response()->json($response, Response::HTTP_BAD_REQUEST);
            }

            DB::beginTransaction();
            $tDokumenLingkungan = TDokumenLingkungan::where('uuid', $uuid)->firstOrFail();
            $oldFile = $tDokumenLingkungan->filename;

            $tDokumenLingkungan->area_id = $idArea;
            $tDokumenLingkungan->nm_area = $namaArea;
            $tDokumenLingkungan->judul = $judul;
            $tDokumenLingkungan->no_dokumen = $noDokumen;
            $tDokumenLingkungan->ruang_lingkup = $ruangLingkup;
            $tDokumenLingkungan->jenis_dokumen = $jenisDokumen;
            $tDokumenLingkungan->filename = $filename;
            $tDokumenLingkungan->tahun = $tahun;

            $tDokumenLingkungan->save();
            DB::commit();

            if (strlen($oldFile) > 0 && strlen($filename) > 0) {
                DeleteFiles($oldFile);
            }

            $response = responseSuccess(__('messages.update-success'), $tDokumenLingkungan);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollBack();
            $response = responseFail(__('messages.update-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Delete(
     *   tags={"DOKUMEN LINGKUNGAN"},
     *   path="/api/dokumen-lingkungan/delete/{uuid}",
     *   summary="delete master dokumen lingkungan by uuid",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="uuid", in="path", description="uuid that needs to be delete", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     *   @OA\Response(response=500, description="Internal Server Error"),
     * )
     */
    public function delete($uuid)
    {
        $this->isValidUuid($uuid);
        $model = TDokumenLingkungan::where('uuid', $uuid)->firstOrFail();
        try {
            $fileName = $model->filename;
            DB::beginTransaction();
            $model->delete();
            DB::commit();

            if (strlen($fileName) > 0) {
                DeleteFiles($fileName);
            }

            $response = responseSuccess(__('messages.delete-success'), $model);
            return response()->json($response);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            DB::rollBack();
            $response = responseFail(__('messages.read-fail'));
            return response()->json($response, Response::HTTP_BAD_REQUEST);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.delete-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Post(
     *   tags={"DOKUMEN LINGKUNGAN"},
     *   path="/api/dokumen-lingkungan/upload",
     *   summary="upload file dokumen lingkungan",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *      @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     description="file to upload",
     *                     property="upload_file",
     *                     type="string",
     *                     format="binary",
     *                 )
     *             )
     *         )
     *     )
     * )
     */
    public function uploadfile(Request $request)
    {
        $request->only(
            'upload_file',
        );
        $this->validate($request, [
            'upload_file' => 'required|mimes:pdf|max:10240',
        ]);

        try {
            $file = $request->file('upload_file');
            $explode = explode('.', $file->getClientOriginalName());
            $originalName = $explode[0];
            $return = StoreFiles($request, $originalName, 'she_enviro/dokumen_lingkungan');
            $response = responseSuccess(__('messages.upload-success'), $return);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            $response = responseFail(__('messages.upload-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Post(
     *   tags={"File"},
     *   path="/api/file/delete",
     *   summary="delete file",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"file"},
     *       @OA\Property(property="file", type="string", format="text")
     *     )
     *   ),
     * )
     */
    public function deleteFile(Request $request)
    {
        try {
            if ($request->has('file') && !empty($request->file)) {
                $file = $request->file;
                $result = DeleteFiles($file);
                $response = responseSuccess(__('messages.delete-success'), $result);
                $httpStatus = Response::HTTP_OK;
                if ($result == "File does not exist") {
                    $response = responseFail(__('messages.read-fail'), $result);
                    $httpStatus = Response::HTTP_NOT_FOUND;
                }
                return response()->json($response, $httpStatus);
            } else {
                $response = responseFail(__('messages.validation-fail'));
                return response()->json($response, Response::HTTP_BAD_REQUEST);
            }
        } catch (\Exception $ex) {
            $response = responseFail(__('messages.delete-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}

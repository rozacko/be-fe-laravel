<?php

namespace App\Http\Controllers\DokumenLingkungan;

use App\Http\Controllers\Controller;
use App\Models\MAreaEnviro;
use App\Models\MJenisDokumenEnviro;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class MasterDDLController extends Controller
{
    /**
     * @OA\Get(
     *   tags={"DOKUMEN LINGKUNGAN"},
     *   path="/api/dokumen-lingkungan/ddl/area",
     *   summary="Get area list",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     *   @OA\Response(response=500, description="Internal Server Error"),
     * )
     */
    public function listArea(Request $request)
    {
        try {
            $data = MAreaEnviro::all();
            $response = responseSuccess(__('messages.read-success'), $data);
            return response()->json($response);
        } catch (\Exception $ex) {
            $response = responseFail(__('messages.show-import-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *   tags={"DOKUMEN LINGKUNGAN"},
     *   path="/api/dokumen-lingkungan/ddl/jenisdokumen",
     *   summary="Get area list",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     *   @OA\Response(response=500, description="Internal Server Error"),
     * )
     */
    public function listJenisDokumen(Request $request)
    {
        try {
            $data = MJenisDokumenEnviro::all();
            $response = responseSuccess(__('messages.read-success'), $data);
            return response()->json($response);
        } catch (\Exception $ex) {
            $response = responseFail(__('messages.show-import-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}

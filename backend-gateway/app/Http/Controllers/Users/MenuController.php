<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Models\Menu;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class MenuController extends Controller
{
    /**
     * @OA\Get(
     *   tags={"Profile"},
     *   path="/api/me/menu",
     *   summary="Get menu",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */
    public function index()
    {
        $user_id = Auth::user()->id;
        $menuChild = json_decode(json_encode(Menu::ByUser($user_id)->distinct()->get()), true);
        $parent_id = array_column($menuChild, 'parent_id');
        $menuParent = json_decode(json_encode(Menu::parentNoPermission($parent_id)->get()), true);
        $menus = array_merge($menuChild, $menuParent);
        $model = $this->buildTree($menus);
        $response = responseSuccess(__('messages.read-success'), $model);
        return response()->json($response, Response::HTTP_OK);
    }

    private function buildTree(array $menus, $parentId = 0)
    {
        $branch = array();

        foreach ($menus as $menu) {
            $access = [];
            foreach ($menus as $i => $iv) {
                if ($menu['feature_id'] == $iv['feature_id']) {
                    $access[] = trim($iv['action_id']);
                }
            }
            $menu['access'] = array_values(array_unique($access));
            if ($menu['powerbi_url'] != '') {
                $menu['path'] = $menu['path'] . '?powerbi_url=' . $menu['powerbi_url'];
            }
            if ($menu['parent_id'] == $parentId && !is_null($menu['parent_id'])) {
                $children = $this->buildTree($menus, $menu['id']);
                if ($children) {
                    $menu['children'] = $children;
                }
                $branch[] = $menu;
            }
        }

        return $branch;
    }
}

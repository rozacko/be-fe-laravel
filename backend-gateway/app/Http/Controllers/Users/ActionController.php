<?php

namespace App\Http\Controllers\Users;

use App\Models\Action;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class ActionController extends Controller
{

    public function index()
    {
        $query = Action::query();
        $model = Datatables::of($query)->make(true)->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response, Response::HTTP_OK);
    }

    public function show($uuid)
    {
        $this->isValidUuid($uuid);
        $model = Action::where('uuid', $uuid)->firstOrFail();
        $response = responseSuccess(__('messages.read-success'), $model);
        return response()->json($response, Response::HTTP_OK);
    }
}

<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\DataTables;

/**
 * @OA\Parameter(
 *   parameter="uuid",
 *   name="uuid",
 *   required=true,
 *   in="path",
 *   description="The unique identifier of entity"
 * )
 */
class UserController extends Controller
{
    /**
     * @OA\Get(
     *   tags={"User"},
     *   path="/api/user",
     *   summary="Get user list",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */
    public function index(Request $request)
    {
        $query = User::with('roles');
        if($request->filled('name')){
            $query->orWhere('users.name', 'ilike', "%{$request->get('name')}%");
        }

        $model = DataTables::of($query)->addIndexColumn()
            // ->filter(function ($query) use ($request) {
            //     if ($request->has('name') && $request->get('name')) {
            //         $query->orWhere('users.name', 'ilike', "%{$request->get('name')}%");
            //     }
            // })
            ->make(true)->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    /**
     * @OA\Post(
     *   tags={"User"},
     *   path="/api/user",
     *   summary="Create user",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"name","username","email","password"},
     *       @OA\Property(property="name", type="string", format="text", example="sonif"),
     *       @OA\Property(property="username", type="string", format="text", example="sonif"),
     *       @OA\Property(property="email", type="email", format="text", example="sonif@mail.com"),
     *       @OA\Property(property="password", type="string", format="text", example="12345678"),
     *       @OA\Property(property="roles", type="array", example={1,2,3},
     *         @OA\Items()
     *       ),
     *     )
     *   )
     * )
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'username' => 'required|string|unique:users,username,NULL,id,deleted_at,NULL',
            'email' => 'required|email|unique:users',
            'password' => 'required|string|min:6|max:50',
            'roles' => 'nullable|array|min:1',
            'roles.*' => 'nullable|exists:roles,id',
        ]);

        $data = [
            'name' => $request->name,
            'username' => $request->username,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ];

        DB::beginTransaction();
        try {
            $model = User::create($data);
            $model->syncRoles($request->input('roles'));
            DB::commit();
            $response = responseSuccess(__('messages.create-success'), $model);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *   tags={"User"},
     *   path="/api/user/{uuid}",
     *   summary="User show",
     *   security={{"token": {}}},
     *   @OA\Parameter(ref="#/components/parameters/uuid"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function show($uuid)
    {
        $this->isValidUuid($uuid);
        $model = User::where('uuid', $uuid)->with("roles")->firstOrFail();
        $response = responseSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    /**
     * @OA\Put(
     *   tags={"User"},
     *   path="/api/user/{uuid}",
     *   summary="User update",
     *   security={{"token": {}}},
     *   @OA\Parameter(ref="#/components/parameters/uuid"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       @OA\Property(property="name", type="string", format="text", example="sonif"),
     *       @OA\Property(property="username", type="string", format="text", example="sonif"),
     *       @OA\Property(property="email", type="email", format="text", example="sonif@mail.com"),
     *     )
     *   ),
     *   @OA\Response(response=200, description="Successfully updated"),
     *   @OA\Response(response=400, description="Bad request"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */
    public function update(Request $request, $uuid)
    {
        $this->isValidUuid($uuid);
        $this->validate($request, [
            'name' => 'sometimes|required|string',
            'username' => 'sometimes|required|string|unique:users,username,' . $uuid . ',uuid,deleted_at,NULL',
            'email' => 'sometimes|required|email|unique:users,email,' . $uuid . ',uuid',
            'roles' => 'nullable|array|min:1',
            'roles.*' => 'nullable|exists:roles,id',
        ]);
        $model = User::where('uuid', $uuid)->firstOrFail();
        $data = $request->only('name', 'username', 'email');
        DB::beginTransaction();
        try {
            $model->syncRoles($request->input('roles'));
            $model->update($data);
            DB::commit();
            $response = responseSuccess(__('messages.update-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.update-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Delete(
     *   tags={"User"},
     *   path="/api/user/{uuid}",
     *   summary="User destroy",
     *   security={{"token": {}}},
     *   @OA\Parameter(ref="#/components/parameters/uuid"),
     *   @OA\Response(response=200, description="Successfully deleted"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function destroy($uuid)
    {
        $this->isValidUuid($uuid);
        $model = User::where('uuid', $uuid)->firstOrFail();

        DB::beginTransaction();
        try {
            $model->delete();
            DB::commit();
            $response = responseSuccess(__('messages.delete-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.delete-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Post(
     *   tags={"User"},
     *   path="/api/user/{uuid}/role",
     *   summary="Sync Roles to User",
     *   security={{"token": {}}},
     *   @OA\Parameter(ref="#/components/parameters/uuid"),
     *   @OA\Response(response=200, description="Successfully synchronized"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"roles"},
     *       @OA\Property(
     *         property="roles", 
     *         type="array", 
     *         example={1,2,3},
     *         @OA\Items()
     *       ),
     *     )
     *   )
     * )
     */
    public function syncRole(Request $request, $uuid)
    {
        $this->isValidUuid($uuid);
        $model = User::where('uuid', $uuid)->firstOrFail();
        $this->validate($request, [
            'roles' => 'required|array|min:1',
            'roles.*' => 'required|exists:roles,id',
        ]);
        DB::beginTransaction();
        try {
            $model->syncRoles($request->input('roles'));
            DB::commit();
            $response = responseSuccess(__('messages.create-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}

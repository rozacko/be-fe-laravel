<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class RoleController extends Controller
{
    /**
     * @OA\Get(
     *   tags={"Role & Permissions"},
     *   path="/api/role",
     *   summary="Get role list",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */
    public function index(Request $request)
    {
        $query = Role::query();
        $model = Datatables::of($query)->addIndexColumn()->make(true)->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    /**
     * @OA\Post(
     *   tags={"Role & Permissions"},
     *   path="/api/role",
     *   summary="Create role",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"name","code"},
     *       @OA\Property(property="name", type="string", format="text", example="sonif"),
     *       @OA\Property(property="username", type="string", format="text", example="sonif"),
     *     )
     *   )
     * )
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:roles',
            'code' => 'required|unique:roles|max:50',
        ]);

        $data = $request->only(['name', 'code']);

        DB::beginTransaction();
        try {
            $model = Role::create($data);
            DB::commit();
            $response = responseSuccess(__('messages.create-success'), $model);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *   tags={"Role & Permissions"},
     *   path="/api/role/{uuid}",
     *   summary="Detail role",
     *   security={{"token": {}}},
     *   @OA\Parameter(ref="#/components/parameters/uuid"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function show($uuid)
    {
        $this->isValidUuid($uuid);
        $model = Role::where('uuid', $uuid)->with('users')->firstOrFail();
        $response = responseSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    /**
     * @OA\Put(
     *   tags={"Role & Permissions"},
     *   path="/api/role/{uuid}",
     *   summary="Role update",
     *   security={{"token": {}}},
     *   @OA\Parameter(ref="#/components/parameters/uuid"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"name","code"},
     *       @OA\Property(property="name", type="string", format="text", example="sonif"),
     *       @OA\Property(property="username", type="string", format="text", example="sonif"),
     *     )
     *   ),
     *   @OA\Response(response=200, description="Successfully updated"),
     *   @OA\Response(response=400, description="Bad request"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */
    public function update(Request $request, $uuid)
    {
        $this->isValidUuid($uuid);
        $this->validate($request, [
            'name' => 'required|unique:roles,name,' . $uuid . ',uuid',
            'code' => 'required|unique:roles,code,' . $uuid . ',uuid|max:50',
        ]);
        $model = Role::where('uuid', $uuid)->firstOrFail();

        DB::beginTransaction();
        try {
            $model->name = $request->input('name');
            $model->code = $request->input('code');
            $model->save();
            DB::commit();
            $response = responseSuccess(__('messages.update-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.update-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Delete(
     *   tags={"Role & Permissions"},
     *   path="/api/role/{uuid}",
     *   summary="Role destroy",
     *   security={{"token": {}}},
     *   @OA\Parameter(ref="#/components/parameters/uuid"),
     *   @OA\Response(response=200, description="Successfully deleted"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function destroy($uuid)
    {
        $this->isValidUuid($uuid);
        $model = Role::where('uuid', $uuid)->firstOrFail();

        DB::beginTransaction();
        try {
            $model->delete();
            DB::commit();
            $response = responseSuccess(__('messages.delete-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.delete-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Post(
     *   tags={"Role & Permissions"},
     *   path="/api/role/{uuid}/permission",
     *   summary="Sync permissions to role (becareful! this will change real permissions)",
     *   security={{"token": {}}},
     *   @OA\Parameter(ref="#/components/parameters/uuid"),
     *   @OA\Response(response=200, description="Successfully synchronized"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"permissions"},
     *       @OA\Property(
     *         property="permissions", 
     *         type="array", 
     *         example={1,2,3,4},
     *         @OA\Items()
     *       ),
     *     )
     *   )
     * )
     */
    public function addPermission(Request $request, $uuid)
    {
        $this->isValidUuid($uuid);
        $model = Role::where('uuid', $uuid)->firstOrFail();
        $this->validate($request, [
            'permissions' => 'required|array|min:1',
            'permissions.*' => 'required|exists:permissions,id',
        ]);
        DB::beginTransaction();
        try {
            $model->syncPermissions($request->input('permissions'));
            DB::commit();
            $response = responseSuccess(__('messages.create-success'), $model);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *   tags={"Role & Permissions"},
     *   path="/api/role/{uuid}/permission",
     *   summary="Detail role permissions",
     *   security={{"token": {}}},
     *   @OA\Parameter(ref="#/components/parameters/uuid"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function getPermission($uuid)
    {
        $this->isValidUuid($uuid);
        DB::enableQueryLog();
        $model = Role::PermissionModel($uuid)->get();
        $model = json_decode(json_encode($model), true);
        $tree = $this->buildTree($model);
        $response = responseSuccess(__('messages.read-success'), $tree);
        return response()->json($response);
    }

    private function buildTree(array $models)
    {
        $tree = [];
        usort($models, function ($a, $b) {
            return strcasecmp($a['module_name'], $b['module_name']);
        });
        $modules = array_unique(array_column($models, "module_id"));
        foreach ($modules as $key => $module) {
            $data = [
                "id" => $models[$key]["module_id"],
                "name" => $models[$key]["module_name"],
                "features" => $this->buildFeature($module, $models),
            ];

            $tree["modules"][] = $data;
        }

        return $tree;
    }

    private function buildFeature($module_id, $models)
    {
        $tree = [];
        usort($models, function ($a, $b) {
            return strcasecmp($a['feature_name'], $b['feature_name']);
        });
        $features = array_unique(array_column($models, "feature_id"));
        foreach ($features as $key => $feature) {
            if ($module_id == $models[$key]["module_id"]) {
                $data = [
                    "id" => $models[$key]["feature_id"],
                    "name" => $models[$key]["feature_name"],
                    "module_id" => $module_id,
                    "permissions" => $this->buildPermission($feature, $models),
                ];

                $tree[] = $data;
            }
        }
        return $tree;
    }

    private function buildPermission($feature_id, $models)
    {
        $tree = [];
        foreach ($models as $key => $model) {
            if ($feature_id == $model["feature_id"]) {
                $data = [
                    "id" => $model["permission_id"],
                    "name" => $model["permission_name"],
                    "action_name" => $model["action_name"],
                    "feature_id" => $feature_id,
                    "selected" => "n",
                ];
                if ($model["auth"] != "") {
                    $data["selected"] = "y";
                }

                $tree[] = $data;
            }
        }
        return $tree;
    }

    /**
     * @OA\Post(
     *   tags={"Role & Permissions"},
     *   path="/api/role/{uuid}/user",
     *   summary="Add users to role",
     *   security={{"token": {}}},
     *   @OA\Parameter(ref="#/components/parameters/uuid"),
     *   @OA\Response(response=200, description="Successfully added"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"users"},
     *       @OA\Property(
     *         property="users", 
     *         type="array", 
     *         example={1,2,3,4},
     *         @OA\Items()
     *       ),
     *     )
     *   )
     * )
     */
    public function addUser(Request $request, $uuid)
    {
        $this->isValidUuid($uuid);
        $role = Role::where('uuid', $uuid)->firstOrFail();
        $this->validate($request, [
            'users' => 'required|array|min:1',
            'users.*' => 'required|exists:users,id',
        ]);
        DB::beginTransaction();
        try {
            $users = $request->input('users');
            $return_model = [];
            foreach ($users as $user) {
                $model = User::find($user);
                $model->assignRole($role->id);
                $return_model[] = $model;
            }
            DB::commit();
            $response = responseSuccess(__('messages.create-success'), $return_model);
            return response()->json($response, 201);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, 500);
        }
    }

    /**
     * @OA\Delete(
     *   tags={"Role & Permissions"},
     *   path="/api/role/{uuid}/user",
     *   summary="Remove users from role",
     *   security={{"token": {}}},
     *   @OA\Parameter(ref="#/components/parameters/uuid"),
     *   @OA\Response(response=200, description="Successfully removed"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"users"},
     *       @OA\Property(
     *         property="users", 
     *         type="array", 
     *         example={1,2,3,4},
     *         @OA\Items()
     *       ),
     *     )
     *   )
     * )
     */
    public function removeUser(Request $request, $uuid)
    {
        $this->isValidUuid($uuid);
        $role = Role::where('uuid', $uuid)->firstOrFail();
        $this->validate($request, [
            'users' => 'required|array|min:1',
            'users.*' => 'required|exists:users,id',
        ]);
        DB::beginTransaction();
        try {
            $users = $request->input('users');
            foreach ($users as $user) {
                $model = User::find($user);
                $model->removeRole($role->id);
            }
            DB::commit();
            $response = responseSuccess(__('messages.delete-success'), $model);
            return response()->json($response, Response::HTTP_OK);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.delete-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}

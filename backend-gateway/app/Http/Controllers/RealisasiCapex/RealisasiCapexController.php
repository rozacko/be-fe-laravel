<?php

namespace App\Http\Controllers\RealisasiCapex;

use App\Http\Controllers\Controller;
use App\Imports\RealisasiCapexImport;
use App\Imports\ExcelImportsWithHeader;
use App\Traits\ValidationExcelImport;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Illuminate\Http\Response;
use App\Models\T_RealisasiCapex;
use App\Models\CapexItem;
use App\Models\T_RealisasiCapexCommited;
use App\Models\T_RealisasiCapexCashout;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;


class RealisasiCapexController extends Controller
{
    use ValidationExcelImport;

    /**
     * @OA\Post(
     *   tags={"Realisasi Capex Upload"},
     *   path="/api/realisasi-capex/upload/",
     *   summary="Realisasi Capex Upload",
     *   security={{"token": {}}},
     *    @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(property="quality",type="string", description="in list : cm-1,cm-2,cm-3,cm-4,atox"),
     *                 @OA\Property(property="upload_file",type="string", format="binary" , description="mimes:xls,xlsx"),
     *             )
     *         ),
     *         description="Fiels Quality Menggunakan Format : cm-1,cm-2,cm-3,cm-4,atox ",
     *     ),
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest")
     * )
     */

    public function upload(Request $request)
    {
        # code...
        $this->validate($request, [
            'upload_file' => 'required|mimes:xls,xlsx'
        ]);

        $file = $request->file('upload_file');
        $data = Excel::toArray(new ExcelImportsWithHeader, $file)[0];

        if (count(@$data) == 0 || !$this->compareTemplate("realisasi-capex", array_keys(@$data[0]))) {
            return response()->json(responseFail(__('messages.validation-template-fail'), ["template" => [__('messages.validation-template-fail')]]), 400)->throwResponse();
        }

        $lineValidation = [
            'no_project' => 'required',
            'no_wbs' => 'required',
            'item_capex' => 'required',
            'unit_kerja' => 'required',
            'pm' => 'required',
            'tipe' => 'required',
            'kai' => 'required',
            'op_dev' => 'required',
            'nilai_investasi' => 'required',
            'network' => 'required',
            'reservasi' => 'required',
            'no_pr' => 'required',
            'nilai_pr' => 'required',
            'no_po' => 'required',
            'doc_date' => 'required|date|date_format:Y-m-d',
            'del_date' => 'required|date|date_format:Y-m-d',
        ];

        $dataExcel = collect($data);
        foreach ($dataExcel as $key => $item) {
            foreach ($item as $k => $val) {
                $item[$k] = trim($val) == '' ? null : $val;
            }

            $tanggal = $item['doc_date'] != null ? \Carbon\Carbon::parse(Date::excelToDateTimeObject($item['doc_date']))->format('Y-m-d') : null;
            $item['doc_date'] = $tanggal;

            $tanggal = $item['del_date'] != null ? \Carbon\Carbon::parse(Date::excelToDateTimeObject($item['del_date']))->format('Y-m-d') : null;
            $item['del_date'] = $tanggal;

            $this->validateImport($item, $lineValidation, ($key + 2));
        }

        $cashout = ['jan_cashout', 'feb_cashout', 'mar_cashout', 'apr_cashout', 'mei_cashout', 'jun_cashout', 'jul_cashout', 'ags_cashout', 'sep_cashout', 'okt_cashout', 'nov_cashout', 'des_cashout'];
        $commited = ['jan_commited', 'feb_commited', 'mar_commited', 'apr_commited', 'mei_commited', 'jun_commited', 'jul_commited', 'ags_commited', 'sep_commited', 'okt_commited', 'nov_commited', 'des_commited'];

        $insert = $dataExcel->map(function ($item) use ($cashout, $commited) {
            foreach ($item as $k => $val) {
                $item[$k] = trim($val) == '' ? null : $val;
            }

            foreach ($cashout as $k) {
                unset($item[$k]);
            }

            foreach ($commited as $k) {
                unset($item[$k]);
            }

            $item['doc_date'] = $item['doc_date'] != null ? \Carbon\Carbon::parse(Date::excelToDateTimeObject($item['doc_date']))->format('Y-m-d') : null;
            $item['del_date'] = $item['del_date'] != null ? \Carbon\Carbon::parse(Date::excelToDateTimeObject($item['del_date']))->format('Y-m-d') : null;
            $item['tgl_gr'] = $item['tgl_gr'] != null ? \Carbon\Carbon::parse(Date::excelToDateTimeObject($item['tgl_gr']))->format('Y-m-d') : null;

            $item['created_by'] = Auth::user()->uuid;
            $item['created_at'] = now();
            return $item;
        });

        DB::beginTransaction();
        try {
            foreach ($insert->toArray() as $key => $value) {
                $header = T_RealisasiCapex::create($value);

                $cc = $dataExcel[$key];
                foreach ($cashout as $k => $val) {
                    if (!empty(trim($cc[$val]))) {
                        T_RealisasiCapexCashout::create(['id_realisasi_capex' => $header->id, 'bulan' => $k + 1, 'value' => $cc[$val]]);
                    }
                }

                foreach ($commited as $k => $val) {
                    if (!empty(trim($cc[$val]))) {
                        T_RealisasiCapexCommited::create(['id_realisasi_capex' => $header->id, 'bulan' => $k + 1, 'value' => $cc[$val]]);
                    }
                }
            }

            DB::commit();
            $response = responseSuccess(__('messages.create-success'));
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollBack();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    
    /**
     * @OA\Post(
     *   tags={"Capex Item Upload"},
     *   path="/api/capex-item/upload/",
     *   summary="Capex Item Upload",
     *   security={{"token": {}}},
     *    @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(property="quality",type="string", description="in list : cm-1,cm-2,cm-3,cm-4,atox"),
     *                 @OA\Property(property="upload_file",type="string", format="binary" , description="mimes:xls,xlsx"),
     *             )
     *         ),
     *         description="Fiels Quality Menggunakan Format : cm-1,cm-2,cm-3,cm-4,atox ",
     *     ),
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest")
     * )
     */
    public function uploaditem(Request $request)
    {
        # code...
        $this->validate($request, [
            'upload_file' => 'required|mimes:xls,xlsx'
        ]);

        $file = $request->file('upload_file');
        $data = Excel::toArray(new ExcelImportsWithHeader, $file)[0];

        if (count(@$data) == 0 || !$this->compareTemplate("capex-item", array_keys(@$data[0]))) {
            return response()->json(responseFail(__('messages.validation-template-fail'), ["template" => [__('messages.validation-template-fail')]]), 400)->throwResponse();
        }

        $lineValidation = [
            'no_wbs' => 'required'
        ];

        $dataExcel = collect($data);
        foreach ($dataExcel as $key => $item) {
            foreach ($item as $k => $val) {
                $item[$k] = trim($val) == '' ? null : $val;
            }

            $this->validateImport($item, $lineValidation, ($key + 2));
        }

        $insert = $dataExcel->map(function ($item) {
            foreach ($item as $k => $val) {
                $item[$k] = trim($val) == '' ? null : $val;
            }

            $item['created_by'] = Auth::user()->uuid;
            $item['created_at'] = now();
            return $item;
        });

        DB::beginTransaction();
        try {
            foreach ($insert->toArray() as $key => $value) {
                $header = CapexItem::create($value);
            }

            DB::commit();
            $response = responseSuccess(__('messages.create-success'));
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollBack();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private function validateImport($data, array $rules, $line)
    {
        $messages = [
            'required' => __('validation.required'),
            'numeric' => __('validation.numeric'),
            'exists' => __('validation.exists'),
        ];

        $validator = Validator::make($data, $rules, $messages);

        if ($validator->fails()) {
            $response = responseFail(__('messages.validation-fail') . " " . __('messages.inline-fail') . " : " . $line, $validator->errors(), []);
            $return = response()->json($response, Response::HTTP_BAD_REQUEST, [], JSON_PRETTY_PRINT);
            $return->throwResponse();
        }
    }

    /**
     * @OA\Get(
     *   tags={"Realisasi Capex Datatable"},
     *   path="/api/realisasi-capex/get-datatable",
     *   summary="Realisasi Capex Datatable",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */
    public function index(Request $request)
    {
        $data = T_RealisasiCapex::get();

        $model = Datatables::of($data)
            ->addIndexColumn()
            ->make(true)
            ->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    /**
     * @OA\Get(
     *   tags={"Capex Item Datatable"},
     *   path="/api/capex-item/get-datatable",
     *   summary="Capex Item Datatable",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */
    public function indexitem(Request $request)
    {
        $data = CapexItem::get();

        $model = Datatables::of($data)
            ->addIndexColumn()
            ->make(true)
            ->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    /**
     * @OA\Get(
     *   tags={"Get Progress Realisasi Capex BarChart"},
     *   path="/api/realisasi-capex/get-progress-realisasi",
     *   summary="Get Progress Realisasi Capex BarChart",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="Ok"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */
    public function getBarProgressRealisasiCapex(Request $request)
    {
        try {
            $data = DB::select("
            select a.*,(coalesce(a.jumlah,0)::float/coalesce(a.total,1)::float)*100 as percentage from(
                SELECT 'WBS' AS grp, (select count(id) from t_realisasi_capex trc where no_wbs is not null) as jumlah, (select count(id) from t_realisasi_capex trc) as total
                union 
                SELECT 'NETWORK' AS grp, (select count(id) from t_realisasi_capex trc where \"network\" is not null) as jumlah, (select count(id) from t_realisasi_capex trc) as total
                union 
                SELECT 'RESERVASI' AS grp, (select count(id) from t_realisasi_capex trc where reservasi is not null) as jumlah, (select count(id) from t_realisasi_capex trc) as total
                union 
                SELECT 'PR' AS grp, (select count(id) from t_realisasi_capex trc where no_pr is not null) as jumlah, (select count(id) from t_realisasi_capex trc) as total
                union 
                SELECT 'RFQ' AS grp, (select count(id) from t_realisasi_capex trc where no_wbs is not null) as jumlah, (select count(id) from t_realisasi_capex trc) as total
                union 
                SELECT 'PO' AS grp, (select count(id) from t_realisasi_capex trc where no_po is not null) as jumlah, (select count(id) from t_realisasi_capex trc) as total
                union 
                SELECT 'GR' AS grp, (select count(id) from t_realisasi_capex trc where tgl_gr is not null) as jumlah, (select count(id) from t_realisasi_capex trc) as total
                ) a",[]);
            
            $result = array();
            foreach ($data as $key => $value) {
                $result[] = array($value->grp,$value->percentage);
            }

            $response = responseDatatableSuccess(__('messages.read-success'), ['data'=>$result]);
            return response()->json($response);   
        } catch (\Exception $ex) {
            $response = responseFail(__('messages.read-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

}
<?php

namespace App\Http\Controllers\InspeksiArea;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use App\Models\TInspeksiArea;
use App\Http\Resources\InspeksiArea\MasterInspeksiAreaListCollection;

class MasterInspeksiAreaController extends Controller
{
    /**
     * @OA\Get(
     *   tags={"INSPEKSI AREA"},
     *   path="/api/inspeksi-area/list",
     *   summary="Get INSPEKSI AREA list",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="area", in="query", description="parameter area", required=true, @OA\Schema( type="string" )),
     *   @OA\Parameter(name="tahun", in="query", description="parameter tahun", required=true, @OA\Schema( type="string" )),
     *   @OA\Parameter(name="bulan", in="query", description="parameter bulan", required=true, @OA\Schema( type="string" )),
     *   @OA\Parameter(name="status", in="query", description="parameter status", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     *   @OA\Response(response=500, description="Internal Server Error"),
     * )
     */
    public function index(Request $request)
    {
        $this->validate($request, [
            'area' => 'required',
            'tahun' => 'required',
            'bulan' => 'required',
            'status' => 'required'
        ]);
        try {
            $idArea = $request->area;
            $tahun = $request->tahun;
            $bulan = $request->bulan;
            $status = $request->status;

            $query = TInspeksiArea::query();
            $model = $query->where('area_id', $idArea)
                        ->where('status', $status)
                        ->whereRaw('EXTRACT(YEAR FROM tanggal) = ?', $tahun)
                        ->whereRaw('EXTRACT(MONTH FROM tanggal) = ?', $bulan)
                        ->get();
            $data = new MasterInspeksiAreaListCollection($model);
            $response = responseSuccess(__('messages.read-success'), $data);
            return response()->json($response);
        } catch (\Exception $ex) {
            $response = responseFail(__('messages.show-import-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Post(
     *   tags={"INSPEKSI AREA"},
     *   path="/api/inspeksi-area/store",
     *   summary="create INSPEKSI AREA",
     *   security={{"token": {}}},
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"id_area","equipment","uraian_temuan","tanggal","foto_temuan","foto_closing","divisi_area","unit_kerja","evaluasi","status"},
     *       @OA\Property(property="id_area", type="string", format="text"),
     *       @OA\Property(property="equipment", type="string", format="text"),
     *       @OA\Property(property="uraian_temuan", type="string", format="text"),
     *       @OA\Property(property="tanggal", type="string", format="text"),
     *       @OA\Property(property="foto_temuan", type="string", format="text"),
     *       @OA\Property(property="foto_closing", type="string", format="text"),
     *       @OA\Property(property="divisi_area", type="string", format="text"),
     *       @OA\Property(property="unit_kerja", type="string", format="text"),
     *       @OA\Property(property="evaluasi", type="string", format="text"),
     *       @OA\Property(property="status", type="string", format="text"),
     *     )
     *   ),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     *   @OA\Response(response=500, description="Internal Server Error"),
     * )
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'id_area' => 'required',
            'equipment' => 'required',
            'uraian_temuan' => 'required',
            'tanggal' => 'required',
            'foto_temuan' => 'required',
            'foto_closing' => 'required',
            'divisi_area' => 'required',
            'unit_kerja' => 'required',
            'evaluasi' => 'required',
            'status' => 'required',
        ]);
        try {
            $idArea = $request->id_area;
            $equipment = $request->equipment;
            $uraianTemuan = $request->uraian_temuan;
            $tanggal = $request->tanggal;
            $fotoTemuan = $request->foto_temuan;
            $fotoClosing = $request->foto_closing;
            $divisiArea = $request->divisi_area;
            $unitKerja = $request->unit_kerja;
            $evaluasi = $request->evaluasi;
            $status = $request->status;

            DB::beginTransaction();
            $tInspeksiArea = new TInspeksiArea;
            $tInspeksiArea->area_id = $idArea;
            $tInspeksiArea->equipment = $equipment;
            $tInspeksiArea->uraian_temuan = $uraianTemuan;
            $tInspeksiArea->tanggal = $tanggal;
            $tInspeksiArea->foto_temuan = $fotoTemuan;
            $tInspeksiArea->foto_closing = $fotoClosing;
            $tInspeksiArea->divisi_area = $divisiArea;
            $tInspeksiArea->unit_kerja = $unitKerja;
            $tInspeksiArea->evaluasi = $evaluasi;
            $tInspeksiArea->status = $status;
            $tInspeksiArea->save();
            DB::commit();

            $response = responseSuccess(__('messages.create-success'));
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollBack();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *   tags={"INSPEKSI AREA"},
     *   path="/api/inspeksi-area/edit/{uuid}",
     *   summary="Get detail INSPEKSI AREA",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="uuid", in="path", description="parameter uuid", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     *   @OA\Response(response=500, description="Internal Server Error"),
     * )
     */
    public function show($uuid)
    {
        $this->isValidUuid($uuid);
        try {
            $model = TInspeksiArea::where('uuid', $uuid)->firstOrFail();
            $dataModel = [];
            if ($model != null) {
                $dataModel[] = $model;
            }

            $data = new MasterInspeksiAreaListCollection($dataModel);
            $response = responseSuccess(__('messages.read-success'), $data);
            return response()->json($response);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            $response = responseFail(__('messages.read-fail'));
            return response()->json($response, Response::HTTP_BAD_REQUEST);
        } catch (\Exception $ex) {
            $response = responseFail(__('messages.read-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Put(
     *   tags={"INSPEKSI AREA"},
     *   path="/api/inspeksi-area/update/{uuid}",
     *   summary="update master INSPEKSI AREA",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="uuid", in="path", description="parameter uuid", required=true, @OA\Schema( type="string" )),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"id_area","equipment","uraian_temuan","tanggal","foto_temuan","foto_closing","divisi_area","unit_kerja","evaluasi","status"},
     *       @OA\Property(property="id_area", type="string", format="text"),
     *       @OA\Property(property="equipment", type="string", format="text"),
     *       @OA\Property(property="uraian_temuan", type="string", format="text"),
     *       @OA\Property(property="tanggal", type="string", format="text"),
     *       @OA\Property(property="foto_temuan", type="string", format="text"),
     *       @OA\Property(property="foto_closing", type="string", format="text"),
     *       @OA\Property(property="divisi_area", type="string", format="text"),
     *       @OA\Property(property="unit_kerja", type="string", format="text"),
     *       @OA\Property(property="evaluasi", type="string", format="text"),
     *       @OA\Property(property="status", type="string", format="text"),
     *     )
     *   ),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     *   @OA\Response(response=500, description="Internal Server Error"),
     * )
     */
    public function update(string $uuid, Request $request)
    {
        $this->validate($request, [
            'id_area' => 'required',
            'equipment' => 'required',
            'uraian_temuan' => 'required',
            'tanggal' => 'required',
            'foto_temuan' => 'required',
            'foto_closing' => 'required',
            'divisi_area' => 'required',
            'unit_kerja' => 'required',
            'evaluasi' => 'required',
            'status' => 'required',
        ]);
        try {
            $idArea = $request->id_area;
            $equipment = $request->equipment;
            $uraianTemuan = $request->uraian_temuan;
            $tanggal = $request->tanggal;
            $fotoTemuan = $request->foto_temuan;
            $fotoClosing = $request->foto_closing;
            $divisiArea = $request->divisi_area;
            $unitKerja = $request->unit_kerja;
            $evaluasi = $request->evaluasi;
            $status = $request->status;
            
            DB::beginTransaction();
            $tInspeksiArea = TInspeksiArea::where('uuid', $uuid)->firstOrFail();
            $tInspeksiArea->area_id = $idArea;
            $tInspeksiArea->equipment = $equipment;
            $tInspeksiArea->uraian_temuan = $uraianTemuan;
            $tInspeksiArea->tanggal = $tanggal;
            $tInspeksiArea->foto_temuan = $fotoTemuan;
            $tInspeksiArea->foto_closing = $fotoClosing;
            $tInspeksiArea->divisi_area = $divisiArea;
            $tInspeksiArea->unit_kerja = $unitKerja;
            $tInspeksiArea->evaluasi = $evaluasi;
            $tInspeksiArea->status = $status;
            $tInspeksiArea->save();
            DB::commit();

            $response = responseSuccess(__('messages.update-success'));
            return response()->json($response);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            DB::rollBack();
            $response = responseFail(__('messages.read-fail'));
            return response()->json($response, Response::HTTP_BAD_REQUEST);
        } catch (\Exception $ex) {
            DB::rollBack();
            $response = responseFail(__('messages.update-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Delete(
     *   tags={"INSPEKSI AREA"},
     *   path="/api/inspeksi-area/delete/{uuid}",
     *   summary="delete master INSPEKSI AREA by uuid",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="uuid", in="path", description="uuid that needs to be delete", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     *   @OA\Response(response=500, description="Internal Server Error"),
     * )
     */
    public function delete($uuid)
    {
        $this->isValidUuid($uuid);
        $model = TInspeksiArea::where('uuid', $uuid)->firstOrFail();
        try {
            DB::beginTransaction();
            $model->delete();
            DB::commit();
            $response = responseSuccess(__('messages.delete-success'), $model);
            return response()->json($response);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            DB::rollBack();
            $response = responseFail(__('messages.read-fail'));
            return response()->json($response, Response::HTTP_BAD_REQUEST);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.delete-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}

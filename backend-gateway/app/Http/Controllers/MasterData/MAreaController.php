<?php

namespace App\Http\Controllers\MasterData;

use App\Http\Controllers\Controller;
use App\Models\MArea;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\DataTables;

class MAreaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = new MArea();
    }

    /**
     * @OA\Get(
     *   tags={"Area"},
     *   path="/api/area/",
     *   summary="Get area list",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */

    public function index(Request $request){
        $query = $this->model::query();
        $model = Datatables::of($query)
                ->addIndexColumn()                    
                ->addColumn('action', function($row){
                    $btn = '<a href="#" onclick="show('.$row->id.')" class="btn btn-primary btn-sm"><i class="fa fa-edit" style="padding-right: 0px; font-size:10;"></i></a> ';
                    $btn .= '<a href="#" onclick="btn_delete('.$row->id.')" class="btn btn-danger btn-sm"><i class="fa fa-trash" style="padding-right: 0px; font-size:10;"></i></a> ';
                    return $btn;
                })
                ->make(true)
                ->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    } 

    /**
     * @OA\Post(
     *   tags={"Area"},
     *   path="/api/area",
     *   summary="Create area",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Request"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"nm_area"},
     *       @OA\Property(property="nm_area", type="string", format="text", example="Electric"),
     *       @OA\Property(property="ref_sap", type="string", format="text", example="222"),
     *     )
     *   )
     * )
     */
    public function store(Request $request)
    {
        $data = $request->only(
            'nm_area',
            'ref_sap',
        );

        $this->validate($request, [
            'nm_area' => 'required',
        ]);

        DB::beginTransaction();
        try {
            $model = $this->model->create($data);
            DB::commit();
            $response = responseSuccess(__('messages.create-success'), $model);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *   tags={"Area"},
     *   path="/api/area/{id}",
     *   summary="Detail Area",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Ok"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */

    public function show($id)
    {
        $model = $this->model->where('id', $id)->firstOrFail();
        $response = responseSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    /**
     * @OA\Put(
     *   tags={"Area"},
     *   path="/api/area/{id}",
     *   summary="Area update",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", description="The name that needs to be update", required=true, @OA\Schema( type="string" )),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"nm_area"},
     *       @OA\Property(property="nm_area", type="string", format="text", example="electrical eng"),
     *       @OA\Property(property="ref_sap", type="string", format="text", example="222"),
     *     )
     *   ),
     *   @OA\Response(response=200, description="Successfully updated"),
     *   @OA\Response(response=400, description="Bad request"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */
    public function update(Request $request, $id)
    {        
        $this->validate($request, [
            'nm_area' => 'required',
        ]);
        $model = $this->model->where('id', $id)->firstOrFail();
        DB::beginTransaction();
        try {
            $model->nm_area = $request->input('nm_area');
            $model->ref_sap = $request->input('ref_sap');
            $model->save();
            DB::commit();
            $response = responseSuccess(__('messages.update-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.update-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Delete(
     *   tags={"Area"},
     *   path="/api/area/{id}",
     *   summary="Area destroy",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", description="The name that needs to be delete", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Successfully deleted"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function destroy($id)
    {        
        $model = $this->model->where('id', $id)->firstOrFail();
        DB::beginTransaction();
        try {
            $model->delete();
            DB::commit();
            $response = responseSuccess(__('messages.delete-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.delete-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}


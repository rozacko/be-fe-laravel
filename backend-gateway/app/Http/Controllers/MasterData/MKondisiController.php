<?php

namespace App\Http\Controllers\MasterData;

use App\Http\Controllers\Controller;
use App\Models\MKondisi;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\DataTables;

class MKondisiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = new MKondisi();
    }

    /**
     * @OA\Get(
     *   tags={"Kondisi"},
     *   path="/api/kondisi/",
     *   summary="Get kondisi list",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */

    public function index(Request $request){
        $query = $this->model::query();
        $model = Datatables::of($query)
                ->addIndexColumn()                    
                ->addColumn('action', function($row){
                    $btn = '<a href="#" onclick="show('.$row->id.')" class="btn btn-primary btn-sm"><i class="fa fa-edit" style="padding-right: 0px; font-size:10;"></i></a> ';
                    $btn .= '<a href="#" onclick="btn_delete('.$row->id.')" class="btn btn-danger btn-sm"><i class="fa fa-trash" style="padding-right: 0px; font-size:10;"></i></a> ';
                    return $btn;
                })
                ->make(true)
                ->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    } 

    /**
     * @OA\Post(
     *   tags={"Kondisi"},
     *   path="/api/kondisi",
     *   summary="Create kondisi",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Request"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"kd_kondisi","nm_kondisi"},
     *       @OA\Property(property="kd_kondisi", type="string", format="text", example="111"),
     *       @OA\Property(property="nm_kondisi", type="string", format="text", example="KONDISI 1"),
     *       @OA\Property(property="warna", type="string", format="text", example="#22222"),
     *     )
     *   )
     * )
     */
    public function store(Request $request)
    {
        $data = $request->only(
            'kd_kondisi',
            'nm_kondisi',
            'warna',
        );

        $this->validate($request, [
            'kd_kondisi' => 'required',
            'nm_kondisi' => 'required',
        ]);

        DB::beginTransaction();
        try {
            $model = $this->model->create($data);
            DB::commit();
            $response = responseSuccess(__('messages.create-success'), $model);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *   tags={"Kondisi"},
     *   path="/api/kondisi/{id}",
     *   summary="Detail Kondisi",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Ok"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */

    public function show($id)
    {
        $model = $this->model->where('id', $id)->firstOrFail();
        $response = responseSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    /**
     * @OA\Put(
     *   tags={"Kondisi"},
     *   path="/api/kondisi/{id}",
     *   summary="Kondisi update",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", description="The name that needs to be update", required=true, @OA\Schema( type="string" )),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"kd_kondisi","nm_kondisi"},
     *       @OA\Property(property="kd_kondisi", type="string", format="text", example="111"),
     *       @OA\Property(property="nm_kondisi", type="string", format="text", example="KONDISI 1"),
     *       @OA\Property(property="warna", type="string", format="text", example="#22222"),
     *     )
     *   ),
     *   @OA\Response(response=200, description="Successfully updated"),
     *   @OA\Response(response=400, description="Bad request"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */
    public function update(Request $request, $id)
    {        
        $this->validate($request, [
            'kd_kondisi' => 'required',
            'nm_kondisi' => 'required',
        ]);
        $model = $this->model->where('id', $id)->firstOrFail();
        DB::beginTransaction();
        try {
            $model->kd_kondisi = $request->input('kd_kondisi');
            $model->nm_kondisi = $request->input('nm_kondisi');
            $model->warna = $request->input('warna');
            $model->save();
            DB::commit();
            $response = responseSuccess(__('messages.update-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.update-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Delete(
     *   tags={"Kondisi"},
     *   path="/api/kondisi/{id}",
     *   summary="Kondisi destroy",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", description="The name that needs to be delete", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Successfully deleted"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function destroy($id)
    {        
        $model = $this->model->where('id', $id)->firstOrFail();
        DB::beginTransaction();
        try {
            $model->delete();
            DB::commit();
            $response = responseSuccess(__('messages.delete-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.delete-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}


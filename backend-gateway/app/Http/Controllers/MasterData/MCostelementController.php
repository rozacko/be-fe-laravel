<?php

namespace App\Http\Controllers\MasterData;
use App\Models\MCostelement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class MCostelementController extends Controller
{
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = new MCostelement();
    }

    /**
     * @OA\Get(
     *   tags={"CostElement"},
     *   path="/api/costelement/",
     *   summary="Get costelement list",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */

    public function index(Request $request){
        $query = $this->model::query();
        $model = Datatables::of($query)
                ->addIndexColumn()                    
                ->addColumn('action', function($row){
                    $btn = '<a href="#" onclick="show('.$row->id.')" class="btn btn-primary btn-sm"><i class="fa fa-edit" style="padding-right: 0px; font-size:10;"></i></a> ';
                    $btn .= '<a href="#" onclick="btn_delete('.$row->id.')" class="btn btn-danger btn-sm"><i class="fa fa-trash" style="padding-right: 0px; font-size:10;"></i></a> ';
                    return $btn;
                })
                ->make(true)
                ->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);        
    } 

    /**
     * @OA\Post(
     *   tags={"CostElement"},
     *   path="/api/costelement",
     *   summary="Create costelement",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Request"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"cost_element","cost_element_name","cost_element_group","biaya_group"},
     *       @OA\Property(property="cost_element", type="string", format="text", example="123"),
     *       @OA\Property(property="cost_element_name", type="string", format="text", example="TBN1"),
     *       @OA\Property(property="cost_element_group", type="string", format="text", example="222"),
     *       @OA\Property(property="biaya_group", type="string", format="text", example="1"),
     *       @OA\Property(property="sap", type="string", format="text", example="sap"),     
     *     )
     *   )
     * )
     */
    public function store(Request $request)
    {
        $data = $request->only(
            'cost_element',
            'cost_element_name',
            'cost_element_group',
            'biaya_group',
            'sap',
        );

        $this->validate($request, [
            'cost_element' => 'required|unique:m_costelement',
            'cost_element_name' => 'required',
            'cost_element_group' => 'required',
            'biaya_group' => 'required',
        ]);

        DB::beginTransaction();
        try {
            $model = $this->model->create($data);
            DB::commit();
            $response = responseSuccess(__('messages.create-success'), $model);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *   tags={"CostElement"},
     *   path="/api/costelement/{id}",
     *   summary="Detail CostElement",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Ok"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function show($id)
    {
        $model = $this->model->where('id', $id)->firstOrFail();
        $response = responseSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    /**
     * @OA\Put(
     *   tags={"CostElement"},
     *   path="/api/costelement/{id}",
     *   summary="CostElement update",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"cost_element","cost_element_name","cost_element_group","biaya_group"},
     *       @OA\Property(property="cost_element", type="string", format="text", example="123"),
     *       @OA\Property(property="cost_element_name", type="string", format="text", example="TBN1"),
     *       @OA\Property(property="cost_element_group", type="string", format="text", example="222"),
     *       @OA\Property(property="biaya_group", type="string", format="text", example="1"),
     *       @OA\Property(property="sap", type="string", format="text", example="sap"),     
     *     )
     *   ),
     *   @OA\Response(response=200, description="Successfully updated"),
     *   @OA\Response(response=400, description="Bad request"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */
    public function update(Request $request, $id)
    {        
        $this->validate($request, [
            'cost_element' => 'required|unique:m_costelement,cost_element,'.$id,
            'cost_element_name' => 'required',
            'cost_element_group' => 'required',
            'biaya_group' => 'required',
        ]);
        $model = $this->model->where('id', $id)->firstOrFail();

        DB::beginTransaction();
        try {
            $model->cost_element = $request->input('cost_element');
            $model->cost_element_name = $request->input('cost_element_name');
            $model->cost_element_group = $request->input('cost_element_group');
            $model->biaya_group = $request->input('biaya_group');
            $model->sap = $request->input('sap');
            $model->save();
            DB::commit();
            $response = responseSuccess(__('messages.update-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.update-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Delete(
     *   tags={"CostElement"},
     *   path="/api/costelement/{id}",
     *   summary="CostElement destroy",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Successfully deleted"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function destroy($id)
    {        
        $model = $this->model->where('id', $id)->firstOrFail();
        DB::beginTransaction();
        try {
            $model->delete();
            DB::commit();
            $response = responseSuccess(__('messages.delete-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.delete-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}


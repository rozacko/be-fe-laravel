<?php

namespace App\Http\Controllers\MasterData;

use App\Http\Controllers\Controller;
use App\Models\MEqInspection;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\DataTables;

class MEqInspectionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = new MEqInspection();
    }

    /**
     * @OA\Get(
     *   tags={"Equipment Inspection"},
     *   path="/api/eqinspection/",
     *   summary="Get eqinspection list",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */

    public function index(Request $request){
        $query = $this->model::
                leftJoin('m_plant', 'm_eq_inspection.plant_id', '=', 'm_plant.id')
                ->leftJoin('m_area', 'm_eq_inspection.area_id', '=', 'm_area.id')
                ->select('m_eq_inspection.id','m_eq_inspection.kd_equipment','m_eq_inspection.nm_equipment','m_eq_inspection.ref_sap','m_plant.nm_plant','m_area.nm_area');
        $model = Datatables::of($query)
                ->addIndexColumn()                    
                ->addColumn('action', function($row){
                    $btn = '<a href="#" onclick="show('.$row->id.')" class="btn btn-primary btn-sm"><i class="fa fa-edit" style="padding-right: 0px; font-size:10;"></i></a> ';
                    $btn .= '<a href="#" onclick="btn_delete('.$row->id.')" class="btn btn-danger btn-sm"><i class="fa fa-trash" style="padding-right: 0px; font-size:10;"></i></a> ';
                    return $btn;
                })
                ->make(true)
                ->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    } 

    /**
     * @OA\Post(
     *   tags={"Equipment Inspection"},
     *   path="/api/eqinspection",
     *   summary="Create eqinspection",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Request"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"kd_equipment","nm_equipment","area_id","plant_id"},
     *       @OA\Property(property="kd_equipment", type="string", format="text", example="111"),
     *       @OA\Property(property="nm_equipment", type="string", format="text", example="KONDISI 1"),
     *       @OA\Property(property="area_id", type="integer", format="text", example="1"),
     *       @OA\Property(property="plant_id", type="integer", format="text", example="1"),
     *       @OA\Property(property="ref_sap", type="string", format="text", example="KONDISI 1"),
     *     )
     *   )
     * )
     */
    public function store(Request $request)
    {
        $data = $request->only(
            'kd_equipment',
            'nm_equipment',
            'area_id',
            'plant_id',
            'ref_sap',
        );

        $this->validate($request, [
            'kd_equipment' => 'required',
            'nm_equipment' => 'required',
            'area_id' => 'required|integer',
            'plant_id' => 'required|integer',
        ]);

        DB::beginTransaction();
        try {
            $model = $this->model->create($data);
            DB::commit();
            $response = responseSuccess(__('messages.create-success'), $model);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *   tags={"Equipment Inspection"},
     *   path="/api/eqinspection/{id}",
     *   summary="Detail Equipment Inspection",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Ok"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */

    public function show($id)
    {
        $model = $this->model->where('id', $id)->firstOrFail();
        $response = responseSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    /**
     * @OA\Put(
     *   tags={"Equipment Inspection"},
     *   path="/api/eqinspection/{id}",
     *   summary="Equipment Inspection update",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", description="The name that needs to be update", required=true, @OA\Schema( type="string" )),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"kd_equipment","nm_equipment","area_id","plant_id"},
     *       @OA\Property(property="kd_equipment", type="string", format="text", example="111"),
     *       @OA\Property(property="nm_equipment", type="string", format="text", example="KONDISI 1"),
     *       @OA\Property(property="area_id", type="integer", format="text", example="1"),
     *       @OA\Property(property="plant_id", type="integer", format="text", example="1"),
     *       @OA\Property(property="ref_sap", type="string", format="text", example="KONDISI 1"),
     *     )
     *   ),
     *   @OA\Response(response=200, description="Successfully updated"),
     *   @OA\Response(response=400, description="Bad request"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */
    public function update(Request $request, $id)
    {        
        $this->validate($request, [
            'kd_equipment' => 'required',
            'nm_equipment' => 'required',
            'area_id' => 'required|integer',
            'plant_id' => 'required|integer',
        ]);
        $model = $this->model->where('id', $id)->firstOrFail();
        DB::beginTransaction();
        try {
            $model->kd_equipment = $request->input('kd_equipment');
            $model->nm_equipment = $request->input('nm_equipment');
            $model->area_id = $request->input('area_id');
            $model->plant_id = $request->input('plant_id');
            $model->ref_sap = $request->input('ref_sap');
            $model->save();
            DB::commit();
            $response = responseSuccess(__('messages.update-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.update-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Delete(
     *   tags={"Equipment Inspection"},
     *   path="/api/eqinspection/{id}",
     *   summary="Equipment Inspection destroy",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", description="The name that needs to be delete", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Successfully deleted"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function destroy($id)
    {        
        $model = $this->model->where('id', $id)->firstOrFail();
        DB::beginTransaction();
        try {
            $model->delete();
            DB::commit();
            $response = responseSuccess(__('messages.delete-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.delete-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}


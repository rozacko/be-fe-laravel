<?php

namespace App\Http\Controllers\MasterData;

use App\Http\Controllers\Controller;
use App\Models\MPlantArea;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\DataTables;

class MPlantAreaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = new MPlantArea();
    }

    /**
     * @OA\Get(
     *   tags={"Plant Area"},
     *   path="/api/plant_area/",
     *   summary="Get plant area list",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */

    public function index(Request $request){
        $query = $this->model::
                leftJoin('m_plant', 'm_plant_area.plant_id', '=', 'm_plant.id')
                ->select('m_plant_area.id','m_plant_area.kd_area','m_plant_area.nm_area','m_plant.nm_plant');
        $model = Datatables::of($query)
                ->addIndexColumn()                    
                ->addColumn('action', function($row){
                    $btn = '<a href="#" onclick="show('.$row->id.')" class="btn btn-primary btn-sm"><i class="fa fa-edit" style="padding-right: 0px; font-size:10;"></i></a> ';
                    $btn .= '<a href="#" onclick="btn_delete('.$row->id.')" class="btn btn-danger btn-sm"><i class="fa fa-trash" style="padding-right: 0px; font-size:10;"></i></a> ';
                    return $btn;
                })
                ->make(true)
                ->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    } 

    /**
     * @OA\Post(
     *   tags={"Plant Area"},
     *   path="/api/plant_area",
     *   summary="Create plant area",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Request"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"kd_area","nm_area","plant_id"},
     *       @OA\Property(property="kd_area", type="string", format="text", example="TBN1"),
     *       @OA\Property(property="nm_area", type="string", format="text", example="222"),
     *       @OA\Property(property="plant_id", type="integer", example="1"),
     *       @OA\Property(property="ref_sap", type="string", format="text", example="qwert"),
     *       @OA\Property(property="ref_opc", type="string", format="text", example="qwert"),
     *     )
     *   )
     * )
     */
    public function store(Request $request)
    {
        $data = $request->only(
            'kd_area',
            'nm_area',
            'plant_id',
            'ref_sap',
            'ref_opc',
        );

        $this->validate($request, [
            'kd_area' => 'required',
            'nm_area' => 'required',
            'plant_id' => 'required|integer|exists:m_plant,id',
        ]);

        DB::beginTransaction();
        try {
            $model = $this->model->create($data);
            DB::commit();
            $response = responseSuccess(__('messages.create-success'), $model);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *   tags={"Plant Area"},
     *   path="/api/plant_area/{id}",
     *   summary="Detail Plant Area",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Ok"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */

    public function show($id)
    {
        $model = $this->model->where('id', $id)->firstOrFail();
        $response = responseSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    /**
     * @OA\Put(
     *   tags={"Plant Area"},
     *   path="/api/plant_area/{id}",
     *   summary="Plant Area update",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", description="The name that needs to be update", required=true, @OA\Schema( type="string" )),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"kd_area","nm_area","plant_id"},
     *       @OA\Property(property="kd_area", type="string", format="text", example="TBN1"),
     *       @OA\Property(property="nm_area", type="string", format="text", example="222"),
     *       @OA\Property(property="plant_id", type="integer", example="1"),
     *       @OA\Property(property="ref_sap", type="string", format="text", example="qwert"),
     *       @OA\Property(property="ref_opc", type="string", format="text", example="qwert"),
     *     )
     *   ),
     *   @OA\Response(response=200, description="Successfully updated"),
     *   @OA\Response(response=400, description="Bad request"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'kd_area' => 'required',
            'nm_area' => 'required',
            'plant_id' => 'required|integer|exists:m_plant,id',
        ]);
        $model = $this->model->where('id', $id)->firstOrFail();
        DB::beginTransaction();
        try {
            $model->kd_area = $request->input('kd_area');
            $model->nm_area = $request->input('nm_area');
            $model->plant_id = $request->input('plant_id');
            $model->ref_sap = $request->input('ref_sap');
            $model->ref_opc = $request->input('ref_opc');
            $model->save();
            DB::commit();
            $response = responseSuccess(__('messages.update-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.update-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Delete(
     *   tags={"Plant Area"},
     *   path="/api/plant_area/{id}",
     *   summary="Plant Area destroy",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", description="The name that needs to be delete", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Successfully deleted"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function destroy($id)
    {        
        $model = $this->model->where('id', $id)->firstOrFail();
        DB::beginTransaction();
        try {
            $model->delete();
            DB::commit();
            $response = responseSuccess(__('messages.delete-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.delete-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}


<?php

namespace App\Http\Controllers\Synchronization;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Yajra\DataTables\DataTables;
use App\Models\M_JenisShipment;
use App\Models\M_ApiShipment;
use App\Models\T_PenarikanShipment;
use App\Models\T_StatusSyncShipment;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client;
use App\Services\SyncShipmentService;

class ShipmentSyncController extends Controller
{
    /**
     * @OA\Get(
     *   tags={"Shipment Management"},
     *   path="/api/shipment",
     *   summary="Get Shipment Management list",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */
    public function index(Request $request){
        $query = M_ApiShipment::query();
        $model = DataTables::of($query)
                ->addColumn('synched_time', function($row) use ($request){
                    $sync_api = T_StatusSyncShipment::where('id_api_shipment', $row->id)->whereDate('tgl_penarikan',\Carbon\Carbon::createFromFormat('d/m/Y', $request->tgl)->format('Y-m-d'))->first();
                    if($sync_api){
                        return \Carbon\Carbon::parse($sync_api->created_at)->format('d-m-Y H:i');
                    }
                    return '';
                })
                ->addColumn('status', function($row) use ($request){
                    $sync_api = T_StatusSyncShipment::where('id_api_shipment', $row->id)->whereDate('tgl_penarikan',\Carbon\Carbon::createFromFormat('d/m/Y', $request->tgl)->format('Y-m-d'))->first();
                    if($sync_api){
                        return $sync_api->status ? 'OK' : 'Not OK';
                    }
                    return 'Not OK';
                })
                ->addColumn('action', function($row) use ($request){
                    $sync_api = T_StatusSyncShipment::where('id_api_shipment', $row->id)->whereDate('tgl_penarikan',\Carbon\Carbon::createFromFormat('d/m/Y', $request->tgl)->format('Y-m-d'))->first();
                    if((is_null($sync_api) || (!is_null($sync_api) && !$sync_api->status)) && (\Carbon\Carbon::createFromFormat('d/m/Y', $request->tgl)->format('Y-m-d') == date('Y-m-d', strtotime('yesterday')))){
                        
                    return '<button class="btn btn-success" onclick="syncData('.$row->id.')"><span><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M13 5.91517C15.8 6.41517 18 8.81519 18 11.8152C18 12.5152 17.9 13.2152 17.6 13.9152L20.1 15.3152C20.6 15.6152 21.4 15.4152 21.6 14.8152C21.9 13.9152 22.1 12.9152 22.1 11.8152C22.1 7.01519 18.8 3.11521 14.3 2.01521C13.7 1.91521 13.1 2.31521 13.1 3.01521V5.91517H13Z" fill="currentColor"/>
                            <path opacity="0.3" d="M19.1 17.0152C19.7 17.3152 19.8 18.1152 19.3 18.5152C17.5 20.5152 14.9 21.7152 12 21.7152C9.1 21.7152 6.50001 20.5152 4.70001 18.5152C4.30001 18.0152 4.39999 17.3152 4.89999 17.0152L7.39999 15.6152C8.49999 16.9152 10.2 17.8152 12 17.8152C13.8 17.8152 15.5 17.0152 16.6 15.6152L19.1 17.0152ZM6.39999 13.9151C6.19999 13.2151 6 12.5152 6 11.8152C6 8.81517 8.2 6.41515 11 5.91515V3.01519C11 2.41519 10.4 1.91519 9.79999 2.01519C5.29999 3.01519 2 7.01517 2 11.8152C2 12.8152 2.2 13.8152 2.5 14.8152C2.7 15.4152 3.4 15.7152 4 15.3152L6.39999 13.9151Z" fill="currentColor"/>
                            </svg>
                            </span>';
                    }
                    else{
                        return '';
                    };
                })
                ->make(true)
                ->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    /**
     * @OA\Post(
     *   tags={"Shipment Management"},
     *   path="/api/shipment/sync",
     *   summary="Upload Overhaul Progress",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"id_api"},
     *       @OA\Property(property="id_api", type="string", format="text", example="12345"),
     *     )
     *   )
     * )
     */
    public function syncApi(Request $request)
    {
        $api = M_ApiShipment::findOrFail($request->id_api);

        $response = (new SyncShipmentService)->{strtolower(preg_replace("/[^A-Za-z0-9]/", "_", $api->api_name))}($api);

        if($response=="successs"){
            $response = responseSuccess(__('messages.create-success'));
            return response()->json($response, Response::HTTP_CREATED);
        }
        else{
            $response = responseFail(__('messages.create-fail'), $response);
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

}

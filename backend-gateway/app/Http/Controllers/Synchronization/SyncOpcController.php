<?php

namespace App\Http\Controllers\Synchronization;
use App\Http\Controllers\Controller;
use App\Models\M_JenisOpc;
use App\Models\M_OpcApi;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use App\Models\M_TagList;
use App\Models\MArea;
use App\Models\MPlant;
use App\Models\T_PenarikanOpc;
use App\Services\SyncOpcService;
use GuzzleHttp\Client;

class SyncOpcController extends Controller{
    
    /**
     * @OA\Get(
     *   tags={"OPC Tag"},
     *   path="/api/opc",
     *   summary="Get opc tag list",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */
    public function index(Request $request)
    {
        $query = M_TagList::query();

        $model = DataTables::of($query)
                ->addColumn('plant',function(M_TagList $data){
                    return $data->plant->nm_plant;
                })
                ->addColumn('jenis_opc',function(M_TagList $data){
                    return $data->jenis->jenis_opc;
                })
                ->addColumn('api_name',function(M_TagList $data){
                    return $data->api->api_name;
                })
                ->addColumn('tag_value', function($row) use ($request){
                    $tag_val = T_PenarikanOpc::where('id_tag', $row->id)->whereDate('waktu_penarikan',\Carbon\Carbon::createFromFormat('d/m/Y', $request->tgl)->format('Y-m-d'))->first();
                    if($tag_val){
                        return $tag_val->tag_value;
                    }
                    return '';
                })
                ->addColumn('synched_time', function($row) use ($request){
                    $tag_val = T_PenarikanOpc::where('id_tag', $row->id)->whereDate('waktu_penarikan',\Carbon\Carbon::createFromFormat('d/m/Y', $request->tgl)->format('Y-m-d'))->first();
                    if($tag_val){
                        return \Carbon\Carbon::parse($tag_val->waktu_penarikan)->format('d-m-Y H:i');
                    }
                    return '';
                })
                ->addColumn('status', function($row) use ($request){
                    $tag_val = T_PenarikanOpc::where('id_tag', $row->id)->whereDate('waktu_penarikan',\Carbon\Carbon::createFromFormat('d/m/Y', $request->tgl)->format('Y-m-d'))->first();
                    if($tag_val){
                        return $tag_val->status ? 'OK' : 'Not OK';
                    }
                    return 'Not OK';
                })
                ->addColumn('action', function($data) use ($request){
                    if($data->sync_period == 'Daily' && (\Carbon\Carbon::createFromFormat('d/m/Y', $request->tgl)->format('Y-m-d') == date('Y-m-d', strtotime('yesterday')))){
                        return '<button class="btn btn-success" onclick="syncData('.$data->id.')"><span><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M13 5.91517C15.8 6.41517 18 8.81519 18 11.8152C18 12.5152 17.9 13.2152 17.6 13.9152L20.1 15.3152C20.6 15.6152 21.4 15.4152 21.6 14.8152C21.9 13.9152 22.1 12.9152 22.1 11.8152C22.1 7.01519 18.8 3.11521 14.3 2.01521C13.7 1.91521 13.1 2.31521 13.1 3.01521V5.91517H13Z" fill="currentColor"/>
                        <path opacity="0.3" d="M19.1 17.0152C19.7 17.3152 19.8 18.1152 19.3 18.5152C17.5 20.5152 14.9 21.7152 12 21.7152C9.1 21.7152 6.50001 20.5152 4.70001 18.5152C4.30001 18.0152 4.39999 17.3152 4.89999 17.0152L7.39999 15.6152C8.49999 16.9152 10.2 17.8152 12 17.8152C13.8 17.8152 15.5 17.0152 16.6 15.6152L19.1 17.0152ZM6.39999 13.9151C6.19999 13.2151 6 12.5152 6 11.8152C6 8.81517 8.2 6.41515 11 5.91515V3.01519C11 2.41519 10.4 1.91519 9.79999 2.01519C5.29999 3.01519 2 7.01517 2 11.8152C2 12.8152 2.2 13.8152 2.5 14.8152C2.7 15.4152 3.4 15.7152 4 15.3152L6.39999 13.9151Z" fill="currentColor"/>
                        </svg>
                        </span>';
                    }
                    
                    return '';
                })
                ->make(true)
                ->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

   /**
     * @OA\Get(
     *   tags={"OPC Tag"},
     *   path="/api/opc/{uuid}",
     *   summary="OPC Tag show",
     *   security={{"token": {}}},
     *   @OA\Parameter(ref="#/components/parameters/uuid"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function show($uuid)
    {
        $this->isValidUuid($uuid);
        $model = M_TagList::where('uuid', $uuid)->firstOrFail();
        $model->category_name = $model->equipment->category->category_name;
        $model->equipment_name = $model->equipment->equipment_name;
        $model->plant = $model->plant->plant;
        $model->api_name = $model->api->api_name;
        $response = responseSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

   /**
     * @OA\Put(
     *   tags={"OPC Tag"},
     *   path="/api/opc/{uuid}",
     *   summary="OPC update",
     *   security={{"token": {}}},
     *   @OA\Parameter(ref="#/components/parameters/uuid"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       @OA\Property(property="sync_time", type="string", format="text", example="07:00"),
     *       @OA\Property(property="sync_period", type="string", format="text", example="Daily"),
     *     )
     *   ),
     *   @OA\Response(response=200, description="Successfully updated"),
     *   @OA\Response(response=400, description="Bad request"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */
    public function update(Request $request, $uuid)
    {
        $this->isValidUuid($uuid);
        $this->validate($request, [
            'name' => 'required|string',
        ]);
        $model = M_TagList::where('uuid', $uuid)->firstOrFail();
        $data = $request->only('sync_time','sync_period');
        $response = responseSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    /**
     * @OA\Get(
     *   tags={"OPC Tag"},
     *   path="/api/opc/getData",
     *   summary="Get opc tag data",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */
    public function syncData(){
        $data = M_TagList::all();
        $dt_rest = [];
        $dt_rest_map = [];
        $dt_shipment = [];
        foreach($data as $dt){
            if(strpos($dt->api->api_name,'OPC REST')){
                $dt_rest[] = ["name" => $dt->tag_name, 'props' => ['name' => 'Value']];
                $dt_rest_map[$dt->tag_name] = $dt->id; 
            }
            else{
                $dt_shipment[] = $dt->tag_name;
            }
        }

        //get data from OPC REST
        $url_rest = M_OpcApi::where('api_name','OPC REST')->first();
        $getTag = '{
            "tags": [
              {
                '.json_encode($dt_rest).'
              }
            ],
            "status": "OK",
            "message": "",
            "token": "'.env('OPC_REST').'"
          }';//&_=1469589103720';

          $params_opc = [
            'message' => $getTag,
            '_' => '1469589103720'
          ];

          $client = new Client();
          $response = $client->request('GET', $url_rest->api_url, $params_opc);
          $result = json_decode($response->getBody()->getContents(),true);

          foreach($result['tags'] as $key=>$item){
              $values = $item['props'];
              $log = new T_PenarikanOpc();
              $log->id_tag = $dt_rest_map[$item['name']];
              if(isset($values['val'])){
                $log->tag_value = $values['val'];
              }
              else{
                $log->tag_value = '0.00';
              }
              $log->save();
          }
    }

     /**
     * @OA\Post(
     *   tags={"OPC Tag"},
     *   path="/api/opc/synctag",
     *   summary="Sync Data Tag",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"id_tag","tgl_sync"},
     *       @OA\Property(property="id_tag", type="string", format="text", example="12345"),
     *       @OA\Property(property="tgl_sync", type="string", format="text", example="13/02/2023"),
     *     )
     *   )
     * )
     */
    public function syncTag(Request $request){
        DB::beginTransaction();
        $tag = M_TagList::find($request->id_tag);
        $api = M_OpcApi::find($tag->id_api);

        try{

            $_tag = [['name' => $tag->tag_name, 'props' => [['name' => 'Value']]]];
            $getTag = ['tags' => $_tag, 'status' => 'OK', 'message' => '', 'token' => env('OPC_REST')];

            $params_opc = [
                'message' => json_encode($getTag),
                '_' => '1469589103720'
            ];

            $client = new Client();
            $_param_type = ['GET' => 'query', 'POST' => 'form_params'];
            $response = $client->request($api->method, $api->api_url, [$_param_type[$api->method] => $params_opc]);
            $result = (string)$response->getBody();
            $_data = json_decode(substr($result, 1, strlen($result)-3),true);

            $params['id_tag'] = $request->id_tag;
            $params['waktu_penarikan'] = date('Y-m-d H:i:s');
            if($_data['status'] != 'OK'){
                $params['status'] = false;
            }
            else{
                $data_tag = $_data['tags'];
                $_data = $data_tag[array_search($tag->tag_name, array_column($data_tag,'name'))];
                if(isset($_data['props'][0]['val'])){
                    $params['tag_value'] = $_data['props'][0]['val'];
                }
            }
            $model = T_PenarikanOpc::where('id_tag',$request->id_tag)->whereDate('waktu_penarikan',date('Y-m-d'))->first();
            if($model){
                $model = T_PenarikanOpc::where('id_tag',$request->id_tag)->whereDate('waktu_penarikan',date('Y-m-d'))->update($params);
            }
            else{
                $model = T_PenarikanOpc::create($params);
            }
            DB::commit();
            $response = responseSuccess(__('messages.create-success'));
            return response()->json($response, Response::HTTP_CREATED);
        }
        catch(\Exception $ex){
            DB::rollBack();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

     /**
     * @OA\Post(
     *   tags={"OPC Tag"},
     *   path="/api/opc/syncdaily",
     *   summary="Sync Data OPC Daily",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"tgl_sync"},
     *       @OA\Property(property="tgl_sync", type="string", format="text", example="13/02/2023"),
     *     )
     *   )
     * )
     */
    public function syncDaily(Request $request)
    {
        $response = (new SyncOpcService)->syncOpcList('Daily');
        return response()->json($response);
    }

     /**
     * @OA\Post(
     *   tags={"OPC Tag"},
     *   path="/api/opc/synchourly",
     *   summary="Sync Data OPC hourly",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"tgl_sync"},
     *       @OA\Property(property="tgl_sync", type="string", format="text", example="13/02/2023"),
     *     )
     *   )
     * )
     */
    public function syncHourly(Request $request)
    {
        $response = (new SyncOpcService)->syncOpcList('Hourly');
        return response()->json($response);
    }

     /**
     * @OA\Post(
     *   tags={"OPC Tag"},
     *   path="/api/opc/syncminutely",
     *   summary="Sync Data OPC minutely",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"tgl_sync"},
     *       @OA\Property(property="tgl_sync", type="string", format="text", example="13/02/2023"),
     *     )
     *   )
     * )
     */
    public function syncMinutely(Request $request)
    {
        $response = (new SyncOpcService)->syncOpcList('Minutely');
        return response()->json($response);
    }
}
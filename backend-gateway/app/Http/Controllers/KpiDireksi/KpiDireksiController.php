<?php

namespace App\Http\Controllers\KpiDireksi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\KpiDireksiImport;
use Illuminate\Http\Response;
use Yajra\DataTables\DataTables;
use App\Models\KpiDireksi;
use App\Http\Resources\KPIDireksi\ListDireksi;
use App\Exports\KPIDireksiExport;

class KpiDireksiController extends Controller
{
    /**
     * @OA\Post(
     *   tags={"KPI Direksi"},
     *   path="/api/kpidireksi/import",
     *   summary="import kpi direksi",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *      @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(property="bulan", type="string", format="text", example="12"),
     *                 @OA\Property(property="tahun", type="string", format="text", example="2023"),
     *                 @OA\Property(
     *                     description="file to upload",
     *                     property="upload_file",
     *                     type="string",
     *                     format="binary",
     *                 )
     *             )
     *         )
     *     )
     * )
     */
    public function storeImport(Request $request){
        $this->validate($request, [
            'bulan' => 'required',
            'tahun' => 'required',
            'upload_file' => 'required',
        ]);
        DB::beginTransaction();
        try {
            Excel::import(new KpiDireksiImport($request->tahun,$request->bulan), request()->file('upload_file'));
            DB::commit();
            $response = responseSuccess(__('messages.create-success'), []);
            return response()->json($response, Response::HTTP_CREATED);

        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *   tags={"KPI Direksi"},
     *   path="/api/kpidireksi/list",
     *   summary="Get Datatable Laporan KPI Direksi",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"bulan","tahun"},
     *       @OA\Property(property="bulan", type="string", format="text", example="02,03,04"),
     *       @OA\Property(property="tahun", type="string", format="text", example="2023")
     *     )
     *   )
     * )
     */

     public function list(Request $request){
        $data = KpiDireksi::where('bulan',(int)$request->bulan)
                ->where('tahun',$request->tahun)
                ->select('kpi_group')
                ->distinct()
                ->get();


        return response()->json([
            'result'=>200,
            'data'=>ListDireksi::collection($data)
        ]);

    }

    public function exportKPI(Request $request){
        $dataArr = KpiDireksi::where('bulan',(int)$request->bulan)
                ->where('tahun',$request->tahun)
                ->orderBy('id')
                ->orderBy('kpi_group')
                ->get();

        $data = [
                'tanggal'=>self::namaBulan($request->bulan)." ".$request->tahun,
                'model'=>$dataArr
            ];
        Excel::store(new KPIDireksiExport($data), 'laporan-kpi-direksi-'.$request->bulan.'-'.$request->tahun.'.xlsx', 'public');
        $responsex = [
            'message' => 'Laporan KPI Direksi has been exported',
            'file_path' => url('api/tpm/downloadLaporan/laporan-kpi-direksi-'.$request->bulan.'-'.$request->tahun.'.xlsx')
        ];

        $response = responseSuccess(__('messages.create-success'), $responsex);
        return response()->json($response, Response::HTTP_CREATED);
    }

    private function namaBulan($bulan){
        $bulanList = [
            '01'=>"Januari",
            '02'=>"Februari",
            '03'=>"Maret",
            '04'=>"April",
            '05'=>"Mei",
            '06'=>"Juni",
            '07'=>"Juli",
            '08'=>"Agustus",
            '09'=>"September",
            '10'=>"Oktober",
            '11'=>"November",
            '12'=>"Desember",
        ];

        return $bulanList[$bulan];
    }
}

<?php

namespace App\Http\Controllers\Production;

use App\Http\Controllers\Controller;
use App\Models\MArea;
use Illuminate\Http\Request;
use App\Services\ProductionService;
use Yajra\DataTables\DataTables;
use Illuminate\Database\Eloquent\Collection;

class WhrpgReportsController extends Controller
{
    /**
     * @OA\Get(
     *   tags={"WHRPG Report"},
     *   path="/api/production/whrpg/",
     *   summary="Get datatable WHRPG report",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */
    public function index(Request $request){
        $tgl_report = \Carbon\Carbon::createFromFormat('d-m-Y', $request->tgl_report)->format('Y-m-d');
        $collection = new Collection();

        $area = MArea::where('nm_area','WHRPG')->first();

        $_params['query'] = [
            ['column' => 'jenis_opc','condition' => 'ilike', 'value' => '%running%'],
            ['column' => 'id_area','condition' => '=', 'value' => $area->id],
        ];
        $_params['skip'] = [];

        $_status_arr['keterangan'] = 'Status';
        $_status = ProductionService::getDataTag(null, $tgl_report, $_params);
        foreach($_status as $_stts){
            $_status_arr['data'] = !is_null($_stts->latestValue) ? strtoupper($_stts->latestValue->tag_value) : 'FALSE';
        }
        $collection->push($_status_arr);

        $_jop_arr['keterangan'] = 'JOP (Jam)';

        $_params['query'] = [
            ['column' => 'jenis_opc','condition' => 'ilike', 'value' => '%jam op%'],
            ['column' => 'id_area','condition' => '=', 'value' => $area->id],
        ];
        $_params['skip'] = [];

        $jam_operasional = ProductionService::getDataTag(null, $tgl_report, $_params);
        $_sum_jop = ProductionService::summaryValue($jam_operasional);
        $_jop_arr['data'] = $_sum_jop;
        $collection->push($_jop_arr);

        $_steam_arr['keterangan'] = 'Total Steam';

        $_params['query'] = [
            ['column' => 'jenis_opc','condition' => 'ilike', 'value' => '%totalizer steam%'],
            ['column' => 'id_area','condition' => '=', 'value' => $area->id],
        ];
        $_params['skip'] = [];

        $jam_operasional = ProductionService::getDataTag(null, $tgl_report, $_params);
        $_sum_steam = ProductionService::summaryValue($jam_operasional);
        $_steam_arr['data'] = $_sum_steam;
        $collection->push($_steam_arr);

        $_mw_arr['keterangan'] = 'Daya (MW)';

        $_params['query'] = [
            ['column' => 'jenis_opc','condition' => 'ilike', 'value' => '%daya%'],
            ['column' => 'jenis_opc','condition' => 'ilike', 'value' => '%mw%'],
            ['column' => 'id_area','condition' => '=', 'value' => $area->id],
        ];
        $_params['skip'] = [];

        $jam_operasional = ProductionService::getDataTag(null, $tgl_report, $_params);
        $_sum_mw = ProductionService::summaryValue($jam_operasional);
        $_mw_arr['data'] = $_sum_mw;
        $collection->push($_mw_arr);

        $_kwh_arr['keterangan'] = 'Produksi (KWh)';

        $_params['query'] = [
            ['column' => 'jenis_opc','condition' => 'ilike', 'value' => '%prod%'],
            ['column' => 'jenis_opc','condition' => 'ilike', 'value' => '%kwh%'],
            ['column' => 'id_area','condition' => '=', 'value' => $area->id],
        ];
        $_params['skip'] = [];

        $jam_operasional = ProductionService::getDataTag(null, $tgl_report, $_params);
        $_sum_kwh = ProductionService::summaryValue($jam_operasional);
        $_kwh_arr['data'] = $_sum_kwh;
        $collection->push($_kwh_arr);

        $model = DataTables::of($collection)->make(true)->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }
}

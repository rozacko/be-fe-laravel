<?php

namespace App\Http\Controllers\Production;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\M_JenisShipment;
use App\Models\M_TagList;
use App\Models\MPlant;
use Illuminate\Database\Eloquent\Collection;
use App\Models\MArea;
use App\Services\ProductionService;
use Yajra\DataTables\DataTables;

class PackerReportsController extends Controller
{
    /**
     * @OA\Get(
     *   tags={"Packer Report"},
     *   path="/api/production/packer/",
     *   summary="Get datatable Packer report",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */
    public function index(Request $request){
        $tgl_report = \Carbon\Carbon::createFromFormat('d-m-Y', $request->tgl_report)->format('Y-m-d');
        $collection = new Collection();

        $plants = MPlant::all();
        $area = MArea::where('nm_area','PACKER')->first();

        $_seec_arr = ['keterangan' => 'SEEC (KWh)'];
        $_zak_arr = ['keterangan' => 'Total Release Semen Bag'];

        $_brand = ['SG' => 'Semen Gresik','SP' => 'Semen Padang','Dynamix' => 'Dynamix','ST' => 'Semen Tonasa'];
        
        $_arr_brand = [];
        foreach($_brand as $key=>$value){
            $_arr_brand[$key] = ['keterangan' => $value];
        }

        $_curah = ['OPC' => ['skip' => ['premium'], 'brand' => 'UltraPro'],
                   'PCC' => ['skip' => ['premium'], 'brand' => 'EzPro'],
                   'OPC Prem' => ['skip' => [], 'brand' => 'SprintPro'],
                   'PCC Prem' => ['skip' => [], 'brand' => 'PowerPro']];

        $_tot_curah = ['keterangan' => 'Total Release Semen Curah'];
        $_tot_jumbo = ['keterangan' => 'Total Release Jumbo Bag'];

        $_curah_brand = [];
        $_jumbo_brand = [];
        foreach($_curah as $key=>$value){
            $_curah_brand[$key] = ['keterangan' => 'CRH - '.$value['brand']];
            $_jumbo_brand[$key] = ['keterangan' => 'JB - '.$value['brand']];
        }
        
        foreach($plants as $plant){
            $_params['query'] = [
                            ['column' => 'jenis_opc','condition' => 'ilike', 'value' => '%kwh%'],
                            ['column' => 'id_area','condition' => '=', 'value' => $area->id],
                        ];
            $_params['skip'] = [];

            $total_kwh = ProductionService::getDataTag($plant->id, $tgl_report, $_params);
            $_sum_kwh = ProductionService::summaryValue($total_kwh);

            $_seec_arr[$plant->nm_plant] = $_sum_kwh;

            $_params['query'] = [
                ['column' => 'jenis_shipment','condition' => 'ilike', 'value' => '%total zak%'],
            ];
            $_params['skip'] = [];

            $total_zak = ProductionService::getDataShipment($plant->id, $tgl_report, $_params);
            $_sum_zak = ProductionService::summaryValue($total_zak);

            $_zak_arr[$plant->nm_plant] = $_sum_zak;

            foreach($_brand as $key=>$value){
                $_params['query'] = [
                    ['column' => 'jenis_shipment','condition' => 'ilike', 'value' => '%Brand '.$key.'%'],
                ];
                $_params['skip'] = [];

                $total_brand = ProductionService::getDataShipment($plant->id, $tgl_report, $_params);
                $_sum_brand = ProductionService::summaryValue($total_brand);
    
                $_arr_brand[$key][str_replace(" ","_",strtolower($plant->nm_plant))] = $_sum_brand;
            }

            //total curah
            $_params['query'] = [
                ['column' => 'jenis_shipment','condition' => 'ilike', 'value' => '%total curah%'],
            ];
            $_params['skip'] = [];

            $total_curah = ProductionService::getDataShipment($plant->id, $tgl_report, $_params);
            $_sum_curah = ProductionService::summaryValue($total_curah);

            $_tot_curah[$plant->nm_plant] = $_sum_curah;


            //total jumbo bag
            $_params['query'] = [
                ['column' => 'jenis_shipment','condition' => 'ilike', 'value' => '%total jumbo%'],
            ];
            $_params['skip'] = [];

            $total_curah = ProductionService::getDataShipment($plant->id, $tgl_report, $_params);
            $_sum_curah = ProductionService::summaryValue($total_curah);

            $_tot_jumbo[$plant->nm_plant] = $_sum_curah;

            foreach($_curah as $key=>$value){
                //Curah
                $_params['query'] = [
                    ['column' => 'jenis_shipment','condition' => 'ilike', 'value' => '%'.$key.'%'],
                ];

                $_params['skip'] = [];
                foreach($value['skip'] as $_value){
                    $_params['skip'][] = ['column' => 'jenis_shipment','condition' => 'ilike', 'value' => '%'.$_value.'%'];
                }
                $_params['skip'][] = ['column' => 'jenis_shipment','condition' => 'ilike', 'value' => '%jumbo%'];

                $total_brand = ProductionService::getDataShipment($plant->id, $tgl_report, $_params);
                $_sum_brand = ProductionService::summaryValue($total_brand);
    
                $_curah_brand[$key][str_replace(" ","_",strtolower($plant->nm_plant))] = $_sum_brand;

                //Jumbo Bag
                $_params['query'] = [
                    ['column' => 'jenis_shipment','condition' => 'ilike', 'value' => '%'.$key.'%'],
                    ['column' => 'jenis_shipment','condition' => 'ilike', 'value' => '%jumbo%'],
                ];
                $_params['skip'] = [];
                foreach($value['skip'] as $_value){
                    $_params['skip'][] = ['column' => 'jenis_shipment','condition' => 'ilike', 'value' => '%'.$_value.'%'];
                }

                $total_brand = ProductionService::getDataShipment($plant->id, $tgl_report, $_params);
                $_sum_brand = ProductionService::summaryValue($total_brand);
    
                $_jumbo_brand[$key][str_replace(" ","_",strtolower($plant->nm_plant))] = $_sum_brand;
            }
        }

        $_zak_arr['child'] = $_arr_brand;
        $_tot_curah['child'] = $_curah_brand;
        $_tot_jumbo['child'] = $_jumbo_brand;

        $collection->push($_seec_arr);
        $collection->push($_zak_arr);
        $collection->push($_tot_curah);
        $collection->push($_tot_jumbo);

        $model = DataTables::of($collection)->make(true)->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    /**
     * @OA\Get(
     *   tags={"Packer Report"},
     *   path="/api/production/packer/status",
     *   summary="Get status Packer report",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */
    public function status(Request $request){
        $tgl_report = \Carbon\Carbon::createFromFormat('d-m-Y', $request->tgl_report)->format('Y-m-d');
        $collection = new Collection();

        $plant = MPlant::find($request->id_plant);
        $area = MArea::where('nm_area','PACKER')->first();

        $_params['query'] = [
            ['column' => 'jenis_opc','condition' => 'ilike', 'value' => '%runtime%'],
            ['column' => 'id_area','condition' => '=', 'value' => $area->id],
        ];
        
        $_params['skip'] = [];

        $_status_running = ProductionService::getDataTag($plant->id, $tgl_report, $_params);
        foreach($_status_running as $_status){
            $_row = ['keterangan' => str_replace('RUNTIME_','',$_status->tag_name), 'status' => strtoupper(is_null($_status->latestValue) ? 'FALSE' : $_status->latestValue->tag_value)];
            $collection->push($_row);
        }

        $model = DataTables::of($collection)->make(true)->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

     /**
     * @OA\Get(
     *   tags={"Packer Report"},
     *   path="/api/production/packer/silo",
     *   summary="Get silo Packer report",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */
    public function silo(Request $request){
        $tgl_report = \Carbon\Carbon::createFromFormat('d-m-Y', $request->tgl_report)->format('Y-m-d');
        $collection = new Collection();

        $plant = MPlant::find($request->id_plant);
        $area = MArea::where('nm_area','PACKER')->first();

        $_silo_prem = ['1' => 'OPC Prem', '7' => 'PCC Prem', '10' => 'PCC Prem', '16' => 'PCC Prem'];
        
    }
}

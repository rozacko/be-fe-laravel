<?php

namespace App\Http\Controllers\Production;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\ProductionService;
use Illuminate\Database\Eloquent\Collection;
use Yajra\DataTables\DataTables;
use App\Models\MPlant;

class ProductionIndexReportsController extends Controller
{
    /**
     * @OA\Get(
     *   tags={"Production Index Report"},
     *   path="/api/production/production/prodindex",
     *   summary="Get production index report",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */
    public function index (Request $request)
    {
        $tgl_report = \Carbon\Carbon::createFromFormat('d-m-Y', $request->tgl_report)->format('Y-m-d');
        $collection = new Collection();

        $plants = MPlant::all();
       
        $_jop_arr = ['keterangan' => 'JOP (Jam)'];
        foreach($plants as $plant){
            $_params['query'] = [
                                ['column' => 'jenis_opc','condition' => 'ilike', 'value' => '%jam op%'],
                            ];
            $_params['skip'] = [];
            $jam_operasional = ProductionService::getDataTag($plant->id, $tgl_report, $_params);

            $_sum_jop = ProductionService::summaryValue($jam_operasional);
            $_jop_arr[$plant->nm_plant] = $_sum_jop;
        }
        // $collection->push($_jop_arr);

        $_rop_arr = ['keterangan' => 'Summary'];
        $_tot_arr = ['keterangan' => 'Total Akumulasi (Ton/Hari)'];
        
        foreach($plants as $plant){
            $_params['query'] = [
                            ['column' => 'jenis_opc','condition' => 'ilike', 'value' => '%prod%'],
                        ];
            $_params['skip'] = [];

            $total_produksi = ProductionService::getDataTag($plant->id, $tgl_report, $_params);
            $_sum_tot = ProductionService::summaryValue($total_produksi);

            $_tot_arr[$plant->nm_plant] = (double) $_sum_tot / (double) 1000;
            $_rop_arr[$plant->nm_plant] = (integer) $_jop_arr[$plant->nm_plant] == 0 ? 0 : (double)$_tot_arr[$plant->nm_plant] / (double) $_jop_arr[$plant->nm_plant];
        }

        $collection->push($_rop_arr);
        // $collection->push($_tot_arr);

        $_seec_arr = ['keterangan' => 'SEEC (KWh)'];
        
        foreach($plants as $plant){
            $_params['query'] = [
                            ['column' => 'jenis_opc','condition' => 'ilike', 'value' => '%kwh%'],
                        ];
            $_params['skip'] = [];

            $total_produksi = ProductionService::getDataTag($plant->id, $tgl_report, $_params);
            $_sum_kwh = ProductionService::summaryValue($total_produksi);

            $_seec_arr[$plant->nm_plant] = (integer)$_tot_arr[$plant->nm_plant] == 0 ? 0 : (double) $_sum_kwh / (double)$_tot_arr[$plant->nm_plant];
        }

        $collection->push($_seec_arr);

        $_coal_arr = ['keterangan' => 'Coal Report'];
        foreach($plants as $plant){
            $_params['query'] = [
                ['column' => 'jenis_opc','condition' => 'ilike', 'value' => '%coal%'],
            ];
            $_params['skip'] = [
                ['column' => 'jenis_opc','condition' => 'ilike', 'value' => '%total%']
            ];

            $total_produksi = ProductionService::getDataTag($plant->id, $tgl_report, $_params);
            $_sum_coal = ProductionService::summaryValue($total_produksi);

            $_coal_arr[$plant->nm_plant] = (integer)$_tot_arr[$plant->nm_plant] == 0 ? 0 : (double) $_sum_coal / (double)$_tot_arr[$plant->nm_plant];
        }
        $collection->push($_coal_arr);

        $_heat_arr = ['keterangan' => 'Heat Consumption'];
        foreach($plants as $plant){
            $_params['query'] = [
                ['column' => 'jenis_opc','condition' => 'ilike', 'value' => '%solar%'],
            ];
            $_params['skip'] = [
                ['column' => 'jenis_opc','condition' => 'ilike', 'value' => '%total%']
            ];

            $total_solar = ProductionService::getDataTag($plant->id, $tgl_report, $_params);
            $_sum_solar = ProductionService::summaryValue($total_solar);

            $_solar_index[$plant->nm_plant] = (integer)$_tot_arr[$plant->nm_plant] == 0 ? 0 : (double) $_sum_solar / ((double)$_tot_arr[$plant->nm_plant]);
            $_sum_solar *= 0.82;

            $_params['query'] = [
                ['column' => 'jenis_opc','condition' => 'ilike', 'value' => '%coal%'],
            ];
            $_params['skip'] = [
                ['column' => 'jenis_opc','condition' => 'ilike', 'value' => '%total%']
            ];
            $total_coal = ProductionService::getDataTag($plant->id, $tgl_report, $_params);
            $_sum_coal = ProductionService::summaryValue($total_coal);

            $_coal_index[$plant->nm_plant] = (integer)$_tot_arr[$plant->nm_plant] == 0 ? 0 : (double) $_sum_coal / ((double)$_tot_arr[$plant->nm_plant]) ;

            $_params['query'] = [
                ['column' => 'jenis_opc','condition' => 'ilike', 'value' => '%sekam%'],
            ];
            $_params['skip'] = [];

            $total_af = ProductionService::getDataTag($plant->id, $tgl_report, $_params);
            $_sum_af = ProductionService::summaryValue($total_af);

            $_rate_af[$plant->nm_plant] = $_sum_af;

            $_heat_arr[$plant->nm_plant] = $_sum_solar + $_sum_coal + $_sum_af;
        }
        $collection->push($_heat_arr);

        $model = DataTables::of($collection)->make(true)->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }
}

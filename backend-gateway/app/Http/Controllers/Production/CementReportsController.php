<?php

namespace App\Http\Controllers\Production;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Yajra\DataTables\DataTables;
use App\Models\M_JenisOpc;
use App\Models\M_TagList;
use App\Models\T_PenarikanOpc;
use App\Models\MArea;
use Illuminate\Support\Facades\DB;
use App\Models\MPlant;
use Illuminate\Database\Eloquent\Collection;

class CementReportsController extends Controller
{
    //
    //
    protected $area;
    protected $opc;

    function __construct()
    {
        $this->area = MArea::where(DB::raw('upper(nm_area)'), 'FINISH MILL')->first();
    }

    /**
     * @OA\Get(
     *   tags={"Cement Report"},
     *   path="/api/production/cement/",
     *   summary="Get datatable Cement report",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */
    public function index (Request $request)
    {
        $tgl_report = \Carbon\Carbon::createFromFormat('d-m-Y', $request->tgl_report)->format('Y-m-d');
        $collection = new Collection();

        $area = MArea::where('nm_area','FINISH MILL')->first();
        $plants = MPlant::all();

        $_status_arr = ['keterangan' => 'Status'];
        foreach($plants as $plant){
            foreach(range(1,2) as $fm){
                $_status_arr['FM '.$fm.' '.$plant->nm_plant] = 'ON';
            }
            if(str_replace(" ",'_',strtolower($plant->nm_plant))=='tuban_1'){
                $_status_arr['FM '.$fm.' '.$plant->nm_plant] = 'ON';
            }
        }
        $collection->push($_status_arr);
       
        $_jop_arr = ['keterangan' => 'JOP (Jam)'];
        foreach($plants as $plant){
            $jam_operasional = M_TagList::with(['jenis' => function($query) use ($area){
                            $query->where('id_area', $area->id)
                            ->where('jenis_opc','ilike','%Jam Op%');
                        }, 'latestValue' => function($query) use($tgl_report){
                            $query->whereDate('waktu_penarikan', $tgl_report);
                        }])
                        ->whereHas('jenis', function($query) use($area) {
                            $query->where('id_area', $area->id)
                            ->where('jenis_opc','ilike','%Jam Op%');
                        })
                        ->where('id_plant',$plant->id)->get();

            $_sum_jop['1'] = 0;
            $_sum_jop['2'] = 0;
            $_sum_jop['9'] = 0;

            $fms = range(1,2);
            $fms[] = 9;
            foreach($jam_operasional as $jop){
                foreach($fms as $fm){
                    if(stripos($jop->jenis->jenis_opc,$fm) && !is_null($jop->latestValue)){
                        if(!is_null($jop->latestValue->harpros_value))
                            $_sum_jop[$fm] += (double) $jop->latestValue->harpros_value;
                        else if(!is_null($jop->latestValue->msdr_value))
                            $_sum_jop[$fm] += (double) $jop->latestValue->msdr_value;
                        else
                            $_sum_jop[$fm] += (double) $jop->latestValue->tag_value;
                    }
                }
            }
            $_jop_arr['FM 1 '.$plant->nm_plant] = $_sum_jop[1];
            $_jop_arr['FM 2 '.$plant->nm_plant] = $_sum_jop[2];
            if(str_replace(" ",'_',strtolower($plant->nm_plant))=='tuban_1')
            $_jop_arr['FM 9 '.$plant->nm_plant] = $_sum_jop[9];
        }
        $collection->push($_jop_arr);

        $_rop_arr = ['keterangan' => 'Rate Produksi (Ton/Jam)'];
        $_tot_arr = ['keterangan' => 'Total Akumulasi (Ton/Hari)'];
        
        foreach($plants as $plant){
            $total_produksi = M_TagList::with(['jenis' => function($query) use ($area){
                            $query->where('id_area', $area->id)
                            ->where('jenis_opc','ilike','%Prod%');
                        }, 'latestValue' => function($query) use($tgl_report){
                            $query->whereDate('waktu_penarikan', $tgl_report);
                        }])
                        ->whereHas('jenis', function($query) use($area) {
                            $query->where('id_area', $area->id)
                            ->where('jenis_opc','ilike','%Prod%');
                        })
                        ->where('id_plant',$plant->id)->get();

            $_sum_tot['1'] = 0;
            $_sum_tot['2'] = 0;

            $fms = range(1,2);

            if(str_replace(" ",'_',strtolower($plant->nm_plant))=='tuban_1'){
                $_sum_tot['9'] = 0;
                $fms[] = 9;
            }
            foreach($total_produksi as $tot){
                foreach($fms as $fm){
                    foreach($fms as $fm){
                        if(stripos($tot->jenis->jenis_opc,$fm) && !is_null($tot->latestValue)){
                            if(!is_null($tot->latestValue->harpros_value))
                                $_sum_tot[$fm] += (double) $tot->latestValue->harpros_value;
                            else if(!is_null($tot->latestValue->msdr_value))
                                $_sum_tot[$fm] += (double) $tot->latestValue->msdr_value;
                            else
                                $_sum_tot[$fm] += (double) $tot->latestValue->tag_value;
                        }
                    }
                }
            }

            foreach($fms as $fm){
                $_tot_arr['FM '.$fm.' '.$plant->nm_plant] = (double) $_sum_tot[$fm] / (double) 1000;
                $_rop_arr['FM '.$fm.' '.$plant->nm_plant] = (integer) $_jop_arr['FM '.$fm.' '.$plant->nm_plant] == 0 ? 0 : (double)$_tot_arr['FM '.$fm.' '.$plant->nm_plant] / (double) $_jop_arr['FM '.$fm.' '.$plant->nm_plant];
            }
        }

        $collection->push($_rop_arr);   
       
        $_seec_arr = ['keterangan' => 'SEEC (KWh)'];
        
        foreach($plants as $plant){
            
            $_sum_tot['1'] = 0;
            $_sum_tot['2'] = 0;

            $fms = range(1,2);

            if(str_replace(" ",'_',strtolower($plant->nm_plant))=='tuban_1'){
                $_sum_tot['9'] = 0;
                $fms[] = 9;
            }

            $total_produksi = M_TagList::with(['jenis' => function($query) use ($area){
                            $query->where('id_area', $area->id)
                            ->where('jenis_opc','ilike','%KWH%');
                        }, 'latestValue' => function($query) use($tgl_report){
                            $query->whereDate('waktu_penarikan', $tgl_report);
                        }])
                        ->whereHas('jenis', function($query) use($area) {
                            $query->where('id_area', $area->id)
                            ->where('jenis_opc','ilike','%KWH%');
                        })
                        ->where('id_plant',$plant->id)->get();

            $_sum_kwh = 0;
            foreach($total_produksi as $tot){
                foreach($fms as $fm){
                    if(stripos($tot->jenis->jenis_opc,$fm) && !is_null($tot->latestValue)){
                        if(!is_null($tot->latestValue->harpros_value))
                            $_sum_kwh += (double) $tot->latestValue->harpros_value;
                        else if(!is_null($tot->latestValue->msdr_value))
                            $_sum_kwh += (double) $tot->latestValue->msdr_value;
                        else
                            $_sum_kwh += (double) $tot->latestValue->tag_value;
                    }
                }
            }

            foreach($fms as $fm){
                $_seec_arr['FM '.$fm.' '.$plant->nm_plant] = (integer)$_tot_arr['FM '.$fm.' '.$plant->nm_plant] == 0 ? 0 : (double) $_sum_kwh / ((double)$_tot_arr['FM '.$fm.' '.$plant->nm_plant] * (double) 0.9);
            }
        }

        $collection->push($_seec_arr);
        
        $collection->push($_tot_arr);    

        $_jenis_semen = [];
        $_jenis_semen[] = ['query' => ['PCC'], 'skip' => 'Prem'];
        $_jenis_semen[] = ['query' => ['OCC'], 'skip' => 'Prem'];
        $_jenis_semen[] = ['query' => ['PCC','Prem']];
        $_jenis_semen[] = ['query' => ['OCC','Prem']];

        $_produksi_pcc = [];
        
        $_clinker_arr = ['keterangan' => 'Clinker Index Total'];
        $_clinker_pcc_arr = ['keterangan' => 'Clinker Index PCC Reguler'];

        $_sum_terak = [];
        $_sum_jenis_prod = [];
        
        foreach($_jenis_semen as $_jenis){
            $_tot_jenis_arr = ['keterangan' => 'Total Akumulasi '.implode(" ",$_jenis['query']).' (Ton/Hari)'];

            foreach($plants as $plant){
                $_sum_tot['1'] = 0;
                $_sum_tot['2'] = 0;
    
                $fms = range(1,2);
    
                if(str_replace(" ",'_',strtolower($plant->nm_plant))=='tuban_1'){
                    $_sum_tot['9'] = 0;
                    $fms[] = 9;
                }
    
                $total_produksi = M_TagList::with(['jenis' => function($query) use ($_jenis){
                                $query->where('jenis_opc','ilike','%prod%');
                                foreach($_jenis['query'] as $_query){
                                    $query->where('jenis_opc','ilike','%'.$_query.'%');
                                }
                                if(isset($_jenis['skip'])){
                                    $query->whereNot('jenis_opc','ilike','%'.$_jenis['skip'].'%');
                                }
                            }, 'latestValue' => function($query) use($tgl_report){
                                $query->whereDate('waktu_penarikan', $tgl_report);
                            }])
                            ->whereHas('jenis', function($query) use ($_jenis){
                                $query->where('jenis_opc','ilike','%prod%');
                                foreach($_jenis['query'] as $_query){
                                    $query->where('jenis_opc','ilike','%'.$_query.'%');
                                }
                                if(isset($_jenis['skip'])){
                                    $query->whereNot('jenis_opc','ilike','%'.$_jenis['skip'].'%');
                                }                                
                            })
                            ->where('id_plant',$plant->id)->get();

                $total_terak = M_TagList::with(['jenis' => function($query) use ($_jenis){
                                            $query->where('jenis_opc','ilike','%terak%');
                                            foreach($_jenis['query'] as $_query){
                                                $query->where('jenis_opc','ilike','%'.$_query.'%');
                                            }
                                            if(isset($_jenis['skip'])){
                                                $query->whereNot('jenis_opc','ilike','%'.$_jenis['skip'].'%');
                                            }
                                        }, 'latestValue' => function($query) use($tgl_report){
                                            $query->whereDate('waktu_penarikan', $tgl_report);
                                        }])
                                        ->whereHas('jenis', function($query) use ($_jenis){
                                            $query->where('jenis_opc','ilike','%terak%');
                                            foreach($_jenis['query'] as $_query){
                                                $query->where('jenis_opc','ilike','%'.$_query.'%');
                                            }
                                            if(isset($_jenis['skip'])){
                                                $query->whereNot('jenis_opc','ilike','%'.$_jenis['skip'].'%');
                                            }                                
                                        })
                                        ->where('id_plant',$plant->id)->get();
    
                $_sum_tot_jenis = 0;
                foreach($total_produksi as $tot){
                    foreach($fms as $fm){
                        if(stripos($tot->jenis->jenis_opc,$fm) && !is_null($tot->latestValue)){
                            if(!is_null($tot->latestValue->harpros_value))
                                $_sum_tot_jenis += (double) $tot->latestValue->harpros_value;
                            else if(!is_null($tot->latestValue->msdr_value))
                                $_sum_tot_jenis += (double) $tot->latestValue->msdr_value;
                            else
                                $_sum_tot_jenis += (double) $tot->latestValue->tag_value;
                        }
                    }
                }

                $_sum_tot_terak = 0;
                foreach($total_terak as $tot){
                    foreach($fms as $fm){
                        if(stripos($tot->jenis->jenis_opc,$fm) && !is_null($tot->latestValue)){
                            if(!is_null($tot->latestValue->harpros_value))
                                $_sum_tot_terak += (double) $tot->latestValue->harpros_value;
                            else if(!is_null($tot->latestValue->msdr_value))
                                $_sum_tot_terak += (double) $tot->latestValue->msdr_value;
                            else
                                $_sum_tot_terak += (double) $tot->latestValue->tag_value;
                        }
                    }
                }
    
                foreach($fms as $fm){
                    $_tot_jenis_arr['FM '.$fm.' '.$plant->nm_plant] = (double) $_sum_tot_jenis / (double) 1000; 
                    $_sum_jenis_prod[$plant->nm_plant][$fm][implode(" ",$_jenis['query'])] = (double) $_sum_tot_jenis / (double) 1000;
                    $_sum_terak[$plant->nm_plant][$fm][implode(" ",$_jenis['query'])] = (double) $_sum_tot_terak / (double) 1000;
                }
            }
            $collection->push($_tot_jenis_arr);
        }

        //Hitung Clinker Index
        foreach($plants as $plant){
            $fms = range(1,2);
    
            if(str_replace(" ",'_',strtolower($plant->nm_plant))=='tuban_1'){
                $_sum_tot['9'] = 0;
                $fms[] = 9;
            }

            foreach($fms as $fm){
                $_clinker_arr['FM '.$fm.' '.$plant->nm_plant] = (integer) array_sum($_sum_terak[$plant->nm_plant][$fm]) == 0 ? 0 : (double) $_tot_arr['FM '.$fm.' '.$plant->nm_plant] / (double) array_sum($_sum_terak[$plant->nm_plant][$fm]) ;
                $_clinker_pcc_arr['FM '.$fm.' '.$plant->nm_plant] = (integer) $_sum_terak[$plant->nm_plant][$fm]['PCC'] == 0 ? 0 : (double) $_sum_jenis_prod[$plant->nm_plant][$fm]['PCC'] / (double) $_sum_terak[$plant->nm_plant][$fm]['PCC'] ;  
            }
        }

        $collection->push($_clinker_arr);
        $collection->push($_clinker_pcc_arr);

        $model = DataTables::of($collection)->make(true)->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }
}

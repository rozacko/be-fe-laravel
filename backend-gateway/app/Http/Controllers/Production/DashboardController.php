<?php

namespace App\Http\Controllers\Production;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Yajra\DataTables\DataTables;
use App\Models\M_JenisOpc;
use App\Models\T_PenarikanOpc;
use App\Models\M_ApiShipment;
use App\Models\M_JenisShipment;
use App\Models\T_PenarikanShipment;
use App\Models\MArea;
use Illuminate\Support\Facades\DB;
use App\Models\MPlant;

class DashboardController extends Controller
{
    //
}

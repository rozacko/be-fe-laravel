<?php

namespace App\Http\Controllers\Production;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Yajra\DataTables\DataTables;
use App\Models\M_JenisOpc;
use App\Models\M_TagList;
use App\Models\T_PenarikanOpc;
use App\Models\MArea;
use Illuminate\Support\Facades\DB;
use App\Models\MPlant;
use App\Services\ProductionService;
use Illuminate\Database\Eloquent\Collection;

class ClinkerReportsController extends Controller
{
    //
    //
    protected $area;
    protected $opc;

    function __construct()
    {
        $this->area = MArea::where(DB::raw('upper(nm_area)'), 'KILN')->first();
        $this->opc = M_JenisOpc::where('id_area', $this->area->id)->get();
    }

    /**
     * @OA\Get(
     *   tags={"Clinker Report"},
     *   path="/api/production/clinker/",
     *   summary="Get datatable Clinker report",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */
    public function index (Request $request)
    {
        $tgl_report = \Carbon\Carbon::createFromFormat('d-m-Y', $request->tgl_report)->format('Y-m-d');
        $collection = new Collection();

        $area = MArea::where('nm_area','KILN')->first();
        $plants = MPlant::all();

        $_status_arr = ['keterangan' => 'Status'];
        foreach($plants as $plant){
            $_status_arr[$plant->nm_plant] = 'ON';
        }
        $collection->push($_status_arr);
       
        $_jop_arr = ['keterangan' => 'JOP (Jam)'];
        foreach($plants as $plant){
            $_params['query'] = [
                            ['column' => 'jenis_opc','condition' => 'ilike', 'value' => '%jam op%'],
                            ['column' => 'id_area','condition' => '=', 'value' => $area->id],
                        ];
            $_params['skip'] = [];

            $jam_operasional = ProductionService::getDataTag($plant->id, $tgl_report, $_params);
            $_sum_jop = ProductionService::summaryValue($jam_operasional);
            
            $_jop_arr[$plant->nm_plant] = $_sum_jop;
        }
        $collection->push($_jop_arr);

        $_rop_arr = ['keterangan' => 'Rate Produksi (Ton/Jam)'];
        $_tot_arr = ['keterangan' => 'Total Akumulasi (Ton/Hari)'];
        
        foreach($plants as $plant){
            $_params['query'] = [
                ['column' => 'jenis_opc','condition' => 'ilike', 'value' => '%prod%'],
                ['column' => 'id_area','condition' => '=', 'value' => $area->id],
            ];
            $_params['skip'] = [];

            $total_produksi = ProductionService::getDataTag($plant->id, $tgl_report, $_params);
            $_sum_tot = ProductionService::summaryValue($total_produksi);

            $_tot_arr[$plant->nm_plant] = (double) $_sum_tot / (double) 1000;
            $_rop_arr[$plant->nm_plant] = (integer) $_jop_arr[$plant->nm_plant] == 0 ? 0 : (double)$_tot_arr[$plant->nm_plant] / (double) $_jop_arr[$plant->nm_plant];
        }

        $collection->push($_rop_arr);
        $collection->push($_tot_arr);       
       
        $_seec_arr = ['keterangan' => 'SEEC (KWh)'];
        
        foreach($plants as $plant){
            $_params['query'] = [
                ['column' => 'jenis_opc','condition' => 'ilike', 'value' => '%kwh%'],
                ['column' => 'id_area','condition' => '=', 'value' => $area->id],
            ];
            $_params['skip'] = [];

            $total_kwh = ProductionService::getDataTag($plant->id, $tgl_report, $_params);
            $_sum_kwh = ProductionService::summaryValue($total_kwh);

            $_seec_arr[$plant->nm_plant] = (integer)$_tot_arr[$plant->nm_plant] == 0 ? 0 : (double) $_sum_kwh / ((double)$_tot_arr[$plant->nm_plant] * (double) 0.9);
        }

        $collection->push($_seec_arr);

        $_stec = ['keterangan' => 'STEC (Kcal/Kg)'];

        $_rate_af = ['keterangan' => 'Rate Alternative Fuel (Ton/Hari)'];
        $_solar_index = ['keterangan' => 'Konsumsi Solar Index (Ton/Hari)'];
        $_coal_index = ['keterangan' => 'Coal Index (Ton/Ton)'];

        foreach($plants as $plant){
            $_params['query'] = [
                ['column' => 'jenis_opc','condition' => 'ilike', 'value' => '%solar kiln%'],
            ];
            $_params['skip'] = [];

            $total_solar = ProductionService::getDataTag($plant->id, $tgl_report, $_params);
            $_sum_solar = ProductionService::summaryValue($total_solar);

            $_solar_index[$plant->nm_plant] = (integer)$_tot_arr[$plant->nm_plant] == 0 ? 0 : (double) $_sum_solar / ((double)$_tot_arr[$plant->nm_plant]);
            $_sum_solar *= 0.82;

            $_params['query'] = [
                ['column' => 'jenis_opc','condition' => 'ilike', 'value' => '%coal kiln%'],
            ];
            $_params['skip'] = [];
            $total_coal = ProductionService::getDataTag($plant->id, $tgl_report, $_params);
            $_sum_coal = ProductionService::summaryValue($total_coal);

            $_coal_index[$plant->nm_plant] = (integer)$_tot_arr[$plant->nm_plant] == 0 ? 0 : (double) $_sum_coal / ((double)$_tot_arr[$plant->nm_plant]) ;

            $_params['query'] = [
                ['column' => 'jenis_opc','condition' => 'ilike', 'value' => '%sekam%'],
            ];
            $_params['skip'] = [];

            $total_af = ProductionService::getDataTag($plant->id, $tgl_report, $_params);
            $_sum_af = ProductionService::summaryValue($total_af);

            $_rate_af[$plant->nm_plant] = $_sum_af;

            $_stec[$plant->nm_plant] = $_sum_solar + $_sum_coal + $_sum_af;
        }
        $collection->push($_stec);

        $_stec = ['keterangan' => 'STEC (Kcal/Kg)'];

        foreach($plants as $plant){
            $_params['query'] = [
                ['column' => 'jenis_opc','condition' => 'ilike', 'value' => '%sekam%'],
            ];
            $_params['skip'] = [];

            $total_produksi = ProductionService::getDataTag($plant->id, $tgl_report, $_params);
            $_sum_pow = ProductionService::summaryValue($total_produksi);

            $_stec[$plant->nm_plant] = (integer)$_tot_arr[$plant->nm_plant] == 0 ? 0 : (double) $_sum_pow / ((double)$_tot_arr[$plant->nm_plant] * (double) 0.9);
        }
        $collection->push($_stec);

        $model = DataTables::of($collection)->make(true)->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }
}

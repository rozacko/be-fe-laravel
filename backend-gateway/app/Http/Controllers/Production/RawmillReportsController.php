<?php

namespace App\Http\Controllers\Production;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Yajra\DataTables\DataTables;
use App\Models\M_JenisOpc;
use App\Models\M_TagList;
use App\Models\T_PenarikanOpc;
use App\Models\MArea;
use Illuminate\Support\Facades\DB;
use App\Models\MPlant;
use Illuminate\Database\Eloquent\Collection;

class RawmillReportsController extends Controller
{
    //
    //
    protected $area;
    protected $opc;

    function __construct()
    {
        $this->area = MArea::where(DB::raw('upper(nm_area)'), 'RAWMILL')->first();
        $this->opc = M_JenisOpc::where('id_area', $this->area->id)->get();
    }

    /**
     * @OA\Get(
     *   tags={"Rawmill Report"},
     *   path="/api/production/rawmill/",
     *   summary="Get datatable Rawmill report",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */
    public function index (Request $request)
    {
        $tgl_report = \Carbon\Carbon::createFromFormat('d-m-Y', $request->tgl_report)->format('Y-m-d');
        $collection = new Collection();

        $area = MArea::where('nm_area','RAWMILL')->first();
        $plants = MPlant::all();

        $_status_arr = ['keterangan' => 'Status'];
        foreach($plants as $plant){
            $_status_arr[$plant->nm_plant] = 'ON';
        }
        $collection->push($_status_arr);
       
        $_jop_arr = ['keterangan' => 'JOP (Jam)'];
        foreach($plants as $plant){
            $jam_operasional = M_TagList::with(['jenis' => function($query) use ($area){
                            $query->where('id_area', $area->id)
                            ->where('jenis_opc','ilike','%Jam Op%');
                        }, 'latestValue' => function($query) use($tgl_report){
                            $query->whereDate('waktu_penarikan', $tgl_report);
                        }])
                        ->whereHas('jenis', function($query) use($area) {
                            $query->where('id_area', $area->id)
                            ->where('jenis_opc','ilike','%Jam Op%');
                        })
                        ->where('id_plant',$plant->id)->get();

            $_sum_jop = 0;
            foreach($jam_operasional as $jop){
                if(!is_null($jop->latestValue)){
                    if(!is_null($jop->latestValue->harpros_value))
                        $_sum_jop += (double) $jop->latestValue->harpros_value;
                    else if(!is_null($jop->latestValue->msdr_value))
                        $_sum_jop += (double) $jop->latestValue->msdr_value;
                    else
                        $_sum_jop += (double) $jop->latestValue->tag_value;
                }
            }
            $_jop_arr[$plant->nm_plant] = $_sum_jop;
        }
        $collection->push($_jop_arr);

        $_rop_arr = ['keterangan' => 'Rate Produksi (Ton/Jam)'];
        $_tot_arr = ['keterangan' => 'Total Akumulasi (Ton/Hari)'];
        
        foreach($plants as $plant){
            $total_produksi = M_TagList::with(['jenis' => function($query) use ($area){
                            $query->where('id_area', $area->id)
                            ->where('jenis_opc','ilike','%Prod%');
                        }, 'latestValue' => function($query) use($tgl_report){
                            $query->whereDate('waktu_penarikan', $tgl_report);
                        }])
                        ->whereHas('jenis', function($query) use($area) {
                            $query->where('id_area', $area->id)
                            ->where('jenis_opc','ilike','%Prod%');
                        })
                        ->where('id_plant',$plant->id)->get();

            $_sum_tot = 0;
            foreach($total_produksi as $tot){
                if(!is_null($tot->latestValue)){
                    if(!is_null($tot->latestValue->harpros_value))
                        $_sum_tot += (double) $tot->latestValue->harpros_value;
                    else if(!is_null($tot->latestValue->msdr_value))
                        $_sum_tot += (double) $tot->latestValue->msdr_value;
                    else
                        $_sum_tot += (double) $tot->latestValue->tag_value;
                }
            }

            $_tot_arr[$plant->nm_plant] = (double) $_sum_tot / (double) 1000;
            $_rop_arr[$plant->nm_plant] = (integer) $_jop_arr[$plant->nm_plant] == 0 ? 0 : (double)$_tot_arr[$plant->nm_plant] / (double) $_jop_arr[$plant->nm_plant];
        }

        $collection->push($_rop_arr);
        $collection->push($_tot_arr);

        
        $_seeu_arr = ['keterangan' => 'SEEC_SEEU'];

        foreach($plants as $plant){
            $total_produksi = M_TagList::with(['jenis' => function($query) use ($area){
                            $query->where('id_area', $area->id)
                            ->where('jenis_opc','ilike','%KWH%');
                        }, 'latestValue' => function($query) use($tgl_report){
                            $query->whereDate('waktu_penarikan', $tgl_report);
                        }])
                        ->whereHas('jenis', function($query) use($area) {
                            $query->where('id_area', $area->id)
                            ->where('jenis_opc','ilike','%KWH%');
                        })
                        ->where('id_plant',$plant->id)->get();

            $_sum_pow = 0;
            foreach($total_produksi as $tot){
                if(!is_null($tot->latestValue)){
                    if(!is_null($tot->latestValue->harpros_value))
                        $_sum_pow += (double) $tot->latestValue->harpros_value;
                    else if(!is_null($tot->latestValue->msdr_value))
                        $_sum_pow += (double) $tot->latestValue->msdr_value;
                    else
                        $_sum_pow += (double) $tot->latestValue->tag_value;
                }
            }

            $_seeu_arr[$plant->nm_plant] = (integer)$_tot_arr[$plant->nm_plant] == 0 ? 0 : (double) $_sum_pow / ((double)$_tot_arr[$plant->nm_plant] * (double) 0.9);
        }
        $collection->push($_seeu_arr);

        $_seec_arr = ['keterangan' => 'SEEC (KWh)'];
        
        foreach($plants as $plant){
            $total_produksi = M_TagList::with(['jenis' => function($query) use ($area){
                            $query->where('id_area', $area->id)
                            ->where('jenis_opc','ilike','%KWH%');
                        }, 'latestValue' => function($query) use($tgl_report){
                            $query->whereDate('waktu_penarikan', $tgl_report);
                        }])
                        ->whereHas('jenis', function($query) use($area) {
                            $query->where('id_area', $area->id)
                            ->where('jenis_opc','ilike','%KWH%');
                        })
                        ->where('id_plant',$plant->id)->get();

            $_sum_kwh = 0;
            foreach($total_produksi as $tot){
                if(!is_null($tot->latestValue)){
                    if(!is_null($tot->latestValue->harpros_value))
                        $_sum_kwh += (double) $tot->latestValue->harpros_value;
                    else if(!is_null($tot->latestValue->msdr_value))
                        $_sum_kwh += (double) $tot->latestValue->msdr_value;
                    else
                        $_sum_kwh += (double) $tot->latestValue->tag_value;
                }
            }

            $_seec_arr[$plant->nm_plant] = (integer)$_tot_arr[$plant->nm_plant] == 0 ? 0 : (double) $_sum_kwh / ((double)$_tot_arr[$plant->nm_plant] * (double) 0.9);
        }

        $collection->push($_seec_arr);

        $model = DataTables::of($collection)->make(true)->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }
}

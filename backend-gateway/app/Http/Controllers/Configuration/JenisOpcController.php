<?php 
namespace App\Http\Controllers\Configuration;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\M_JenisOpc;
use App\Models\MArea;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class JenisOpcController extends Controller
{
    public function index(Request $request)
    {
        return view("pages.synchronization.jenisopc.index");
    }

    public function list()
    {
        $data = M_JenisOpc::query();
        return DataTables::of($data)
            ->addColumn('nm_area', function(M_JenisOpc $jenisopc){
                return $jenisopc->area->nm_area;
            })->toJson();
    }

    public function create()
    {
        $area = MArea::all();
        return view("pages.synchronization.jenisopc.create",['area' => $area]);
    }

    public function store(Request $request)
    {
        $data = $request->only(['id_area', 'jenis_opc']);

        $model = M_JenisOpc::create($data);
        return redirect('config/jenisopc');
    }

    public function show($uuid)
    {
        $model = M_JenisOpc::where('uuid', $uuid)->first();
        $area = MArea::all();
        return view("pages.synchronization.jenisopc.show", ['data' => $model, 'area' => $area]);
    }

    public function edit($uuid)
    {
        $this->isValidUuid($uuid);
        $category = MArea::all();
        $model = M_JenisOpc::where('uuid', $uuid)->first();
        return view("pages.synchronization.jenisopc.edit", ['data' => $model, 'category' => $category]);
    }

    public function update(Request $request, $uuid)
    {
        $this->isValidUuid($uuid);
        $model = M_JenisOpc::where('uuid', $uuid)->firstOrFail();
        $data =  $request->only(['id_area', 'jenis_opc']);

        $model->update($data);
        return redirect('config/jenisopc');
    }

    public function destroy($uuid)
    {
        $this->isValidUuid($uuid);
        $model = M_JenisOpc::where('uuid', $uuid)->firstOrFail();

        DB::beginTransaction();
        try {
            $model->delete();
            DB::commit();
            $response = responseSuccess(__('messages.delete-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.delete-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function getJenisOpc($area)
    {
        $model = M_JenisOpc::where('id_area', $area)->get(); 
        $response = responseSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }
   
}
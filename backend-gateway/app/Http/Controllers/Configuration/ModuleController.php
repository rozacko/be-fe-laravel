<?php

namespace App\Http\Controllers\Configuration;

use App\Http\Controllers\Controller;
use App\Models\Module;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;

class ModuleController extends Controller
{
    public function index(Request $request)
    {
        if ($request->wantsJson()) {
            return datatables(Module::query())->toJson();
        } else {
            return view("pages.module.index");
        }
    }

    public function features(Request $request, $uuid)
    {
        if (!Str::isUuid($uuid)) {
            return response(null, Response::HTTP_NOT_FOUND);
        }

        $module = Module::whereUuid($uuid)->firstOrFail();
        if ($request->wantsJson()) {
            return datatables()->of($module->features)->toJSON();
        } else {
            return view("pages.features.index", $module);
        }
    }
}

<?php

namespace App\Http\Controllers\Configuration;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\M_ApiShipment;
use App\Models\M_JenisShipment;
use App\Models\MPlant;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use Yajra\DataTables\DataTables;

class ShipmentController extends Controller
{
    //
    public function index(Request $request)
    {
        return view("pages.shipment.shipment.index");
    }

    public function list(Request $request){
        $data = M_JenisShipment::query();
        return Datatables::of($data)
                ->addColumn('plant', function(M_JenisShipment $data){
                    if(!is_null($data->plant))
                        return $data->plant->nm_plant;
                    else
                        return "";
                    //return $data->plant->nm_plant;
                })
                ->addColumn('api_name', function(M_JenisShipment $data){
                    return $data->api->api_name;
                })->toJson();
    }

    public function create(Request $request)
    {
        $plant = MPlant::all();
        $api = M_ApiShipment::all();
        return view("pages.shipment.shipment.create",['plant' => $plant, 'api' => $api]);
    }

    public function store(Request $request)
    {
        $data = $request->only(["id_api_shipment",
                                "id_plant",
                                "jenis_shipment",
                                "shipment_params"]);

        $data['shipment_params'] = json_encode($data['shipment_params']);
        $model = M_JenisShipment::create($data);
        return redirect('config/shipment');
    }

    public function edit($uuid)
    {                
        $this->isValidUuid($uuid);
        $data = M_JenisShipment::where('uuid', $uuid)->first();
        $plant = MPlant::all();
        $api = M_ApiShipment::all();
        return view("pages.shipment.shipment.edit",['plant' => $plant, 'api' => $api, 'data' => $data]);
    }

    public function show($uuid)
    {                
        $this->isValidUuid($uuid);
        $data = M_JenisShipment::where('uuid', $uuid)->first();
        $plant = MPlant::all();
        $api = M_ApiShipment::all();
        return view("pages.shipment.shipment.show",['plant' => $plant, 'api' => $api, 'data' => $data]);
    }

    public function update(Request $request, $uuid)
    {
        $this->isValidUuid($uuid);
        $model = M_JenisShipment::where('uuid', $uuid)->firstOrFail();
        $data = $request->only(["id_api_shipment",
                                "id_plant",
                                "jenis_shipment",
                                "shipment_params"]);

        $data['shipment_params'] = json_encode($data['shipment_params']);
        $model->update($data);
        return redirect('config/shipment');
    }

    public function destroy($uuid)
    {
        $this->isValidUuid($uuid);
        $model = M_JenisShipment::where('uuid', $uuid)->firstOrFail();

        DB::beginTransaction();
        try {
            $model->delete();
            DB::commit();
            $response = responseSuccess(__('messages.delete-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.delete-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}

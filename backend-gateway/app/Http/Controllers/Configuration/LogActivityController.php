<?php

namespace App\Http\Controllers\Configuration;

use App\Http\Controllers\Controller;
use App\Models\LogActivity;
use Illuminate\Http\Request;

class LogActivityController extends Controller
{
    public function index(Request $request)
    {
        if ($request->wantsJson()) {
            return datatables(LogActivity::select('log_activities.*', 'users.name as user_name')->leftJoin('users', 'users.id', '=', 'log_activities.user_id')->orderBy('created_at', 'desc'))->toJson();
        } else {
            return view("pages.log.index");
        }
    }
}

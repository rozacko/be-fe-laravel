<?php

namespace App\Http\Controllers\Configuration;

use App\Http\Controllers\Controller;
use App\Models\Action;
use App\Models\M_JenisOpc;
use Illuminate\Http\Request;
use App\Models\MArea;
use App\Models\M_OpcApi;
use App\Models\M_TagList;
use App\Models\M_OpcDaily;
use App\Models\MPlant;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class OpcController extends Controller
{
    public function index(Request $request)
    {
        return view("pages.synchronization.opc.index");
    }

    public function list(Request $request){
        $data = M_TagList::query();
        return Datatables::of($data)
                ->addColumn('plant', function(M_TagList $data){
                    return $data->plant->nm_plant;
                })
                ->addColumn('api_name', function(M_TagList $data){
                    return $data->api->api_name;
                })
                ->addColumn('jenis_opc', function(M_TagList $data){
                    return $data->jenis->jenis_opc;
                })->toJson();
    }

    public function create(Request $request)
    {
        $plant = MPlant::all();
        $api = M_OpcApi::all();
        $area = MArea::all();
        return view("pages.synchronization.opc.create",['plant' => $plant, 'api' => $api, 'area' => $area]);
    }

    public function store(Request $request)
    {
        $data = $request->only(['id_jenis_opc', 'id_plant', 'id_api','tag_name','sync_period','sync_time']);
        $model = M_TagList::create($data);
        return redirect('config/opc');
    }

    public function edit($uuid)
    {                
        $this->isValidUuid($uuid);
        $data = M_TagList::where('uuid', $uuid)->first();
        $plant = MPlant::all();
        $api = M_OpcApi::all();
        $area = MArea::all();
        $jenisopc = M_JenisOpc::where('id',$data->id_jenis_opc)->get();
        return view("pages.synchronization.opc.edit",['plant' => $plant, 'api' => $api, 'area' => $area, 'jenisopc' => $jenisopc, 'data' => $data]);
    }

    public function show($uuid)
    {                
        $this->isValidUuid($uuid);
        $data = M_TagList::where('uuid', $uuid)->first();
        $plant = MPlant::all();
        $api = M_OpcApi::all();
        $area = MArea::all();
        $jenisopc = M_JenisOpc::where('id',$data->id_jenis_opc)->get();
        return view("pages.synchronization.opc.edit",['plant' => $plant, 'api' => $api, 'area' => $area, 'jenisopc' => $jenisopc, 'data' => $data]);
    }

    public function update(Request $request, $uuid)
    {
        $this->isValidUuid($uuid);
        $model = M_TagList::where('uuid', $uuid)->firstOrFail();
        $data =  $request->only(['id_jenis_opc', 'id_plant', 'id_api','tag_name','sync_period','sync_time']);

        $model->update($data);
        return redirect('config/opc');
    }

    public function destroy($uuid)
    {
        $this->isValidUuid($uuid);
        $model = M_TagList::where('uuid', $uuid)->firstOrFail();

        DB::beginTransaction();
        try {
            $model->delete();
            DB::commit();
            $response = responseSuccess(__('messages.delete-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.delete-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}

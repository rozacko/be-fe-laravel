<?php 
namespace App\Http\Controllers\Configuration;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\M_OpcApi;
use Illuminate\Support\Facades\DB;

class OpcApiController extends Controller
{
    public function index(Request $request)
    {
        return view("pages.synchronization.opcapi.index");
    }

    public function list()
    {
        $data = M_OpcApi::query();
        return datatables($data)->toJson();
    }

    public function create()
    {
        return view("pages.synchronization.opcapi.create");
    }

    public function store(Request $request)
    {
        $data = $request->only(['api_name','api_url']);

        $model = M_OpcApi::create($data);
        return redirect('config/opcapi');
    }

    public function show($uuid)
    {
        $model = M_OpcApi::where('uuid', $uuid)->first();
        return view("pages.synchronization.opcapi.show", $model);
    }

    public function edit($uuid)
    {
        $this->isValidUuid($uuid);
        $model = M_OpcApi::where('uuid', $uuid)->first();
        return view("pages.synchronization.opcapi.edit", $model);
    }

    public function update(Request $request, $uuid)
    {
        $this->isValidUuid($uuid);
        $model = M_OpcApi::where('uuid', $uuid)->firstOrFail();
        $data =  $request->only(['api_name','api_url','params']);

        $model->update($data);
        return redirect('config/opcapi');
    }

    public function destroy($uuid)
    {
        $this->isValidUuid($uuid);
        $model = M_OpcApi::where('uuid', $uuid)->firstOrFail();

        DB::beginTransaction();
        try {
            $model->delete();
            DB::commit();
            $response = responseSuccess(__('messages.delete-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.delete-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
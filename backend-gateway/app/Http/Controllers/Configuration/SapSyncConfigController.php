<?php

namespace App\Http\Controllers\Configuration;

use App\Http\Controllers\Controller;
use App\Models\SapSyncConfig;
use App\Services\SyncSapService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\Rule;
use Yajra\DataTables\Facades\DataTables;

class SapSyncConfigController extends Controller
{
    public function index(Request $request)
    {
        if ($request->wantsJson()) {
            return datatables(SapSyncConfig::query())->toJson();
        } else {
            $data['types'] = SapSyncConfig::types();
            $data['schedules'] = SapSyncConfig::schedules();
            $data['statuses'] = SapSyncConfig::statuses();
            $data['configs'] = SapSyncConfig::all();

            return view("pages.sap.config.index", $data);
        }
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255|unique:sap_sync_configs,name',
            'type' => ['required', Rule::in(array_keys(SapSyncConfig::types()))],
            'tcode' => 'required|max:255',
            'parameter' => 'required',
            'schedule' => ['required', Rule::in(array_keys(SapSyncConfig::schedules()))],
            'at_date' => Rule::requiredIf($request->input('schedule') == 'monthly'),
            'at_time' => Rule::requiredIf(in_array($request->input('schedule'), ['daily', 'monthly'])),
            'status' => ['required', Rule::in(array_keys(SapSyncConfig::statuses()))],
        ]);

        if ($request->at_date == '') {
            $request['at_date'] = NULL;
        }

        try {
            $model = SapSyncConfig::create($request->all());

            $response = responseSuccess(__('messages.create-success'), $model);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (Exception $e) {
            $response = responseFail(__('messages.create-fail'), [$e->getMessage()]);
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function show($id)
    {
        $model = SapSyncConfig::findOrFail($id);

        $response = responseSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    public function update(Request $request, $id)
    {
        $model = SapSyncConfig::findOrFail($id);

        $request->validate([
            'name' => 'required|max:255|unique:sap_sync_configs,name,' . $id . ',id',
            'type' => ['required', Rule::in(array_keys(SapSyncConfig::types()))],
            'tcode' => 'required|max:255',
            'parameter' => 'required',
            'schedule' => ['required', Rule::in(array_keys(SapSyncConfig::schedules()))],
            'at_date' => Rule::requiredIf($request->input('schedule') == 'monthly'),
            'at_time' => Rule::requiredIf(in_array($request->input('schedule'), ['daily', 'monthly'])),
            'status' => ['required', Rule::in(array_keys(SapSyncConfig::statuses()))],
        ]);

        if ($request->at_date == '') {
            $request['at_date'] = NULL;
        }

        try {
            $model->update($request->all());
            $response = responseSuccess(__('messages.update-success'), $model);
            return response()->json($response);
        } catch (Exception $e) {
            $response = responseFail(__('messages.update-fail'), [$e->getMessage()]);
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function destroy($id)
    {
        $model = SapSyncConfig::findOrFail($id);

        try {
            $model->delete();
            $response = responseSuccess(__('messages.delete-success'), $model);
            return response()->json($response);
        } catch (Exception $e) {
            $response = responseFail(__('messages.delete-fail'), [$e->getMessage()]);
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function testSync(Request $request)
    {
        $config = (object) $request->all();
        $response = (new SyncSapService)->execute(null, null, null, $config, 'test');
        if ($response['status'] == 'fail') {
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        return response()->json($response);
    }

    public function manualSync(Request $request)
    {
        $request->validate([
            'id' => 'required|exists:sap_sync_configs',
            'start_date' => 'required',
            'end_date' => 'required'
        ]);

        $startDate = str_replace("-", "", $request->start_date);
        $endDate = str_replace("-", "", $request->end_date);
        $type = SapSyncConfig::select('type')->where('id', $request->id)->first();
        $id = $request->id;
        $date = [
            "date_from" => $startDate,
            "date_to" => $endDate
        ];

        $response = (new SyncSapService)->execute($id, $type->type, $date);

        if ($response['status'] == 'fail') {
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        return response()->json($response);
    }
}

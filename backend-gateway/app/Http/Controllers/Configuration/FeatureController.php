<?php

namespace App\Http\Controllers\Configuration;

use App\Http\Controllers\Controller;
use App\Models\Feature;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class FeatureController extends Controller
{
    public function permission(Request $request, $uuid)
    {
        if (Str::isUuid($uuid)) {
            $feature = Feature::whereUuid($uuid)->firstOrFail();
            if ($request->wantsJson()) {
                return datatables()->of($feature->permissions()->with('routes'))->toJSON();
            }

            return view("pages.permissions.index", $feature);
        }

        abort(404);
    }
}

<?php

namespace App\Http\Controllers\Configuration;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    public function index()
    {
        return view('pages.auth.index');
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'username' => ['required', 'string'],
            'password' => 'required',
        ]);
        $credentials = $request->only('username', 'password');
        $credentials['is_developer'] = 'y';
        if (!Auth::guard('web')->attempt($credentials)) {
            throw ValidationException::withMessages([
                'username' => __('auth.failed'),
            ]);
        }
        return redirect('config/log_activity');
    }

    public function destroy(Request $request)
    {
        Auth::guard('web')->logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect('config/login');
    }
}

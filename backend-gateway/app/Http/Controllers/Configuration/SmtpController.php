<?php

namespace App\Http\Controllers\Configuration;

use App\Http\Controllers\Controller;
use App\Models\SmtpConfig;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class SmtpController extends Controller
{
    public function index(Request $request)
    {

        if ($request->wantsJson()) {
            return datatables(SmtpConfig::query())->toJson();
        } else {
            return view("pages.smtp.index");
        }
    }

    public function create()
    {
        return view("pages.smtp.create")->render();
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'host' => 'required',
            'port' => 'required',
            'username' => 'required',
            'password' => 'required',
            'encryption' => 'required',
        ]);
        $data = $request->only(['port', 'host', 'encryption', 'username', 'password']);

        DB::beginTransaction();
        try {
            $model = SmtpConfig::create($data);
            DB::commit();
            $response = responseSuccess(__('messages.create-success'), $model);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function edit($uuid)
    {
        $this->isValidUuid($uuid);
        $model = SmtpConfig::where('uuid', $uuid)->firstOrFail();
        return view("pages.smtp.edit", $model)->render();
    }

    public function update(Request $request, $uuid)
    {
        $this->isValidUuid($uuid);
        $this->validate($request, [
            'host' => 'required',
            'port' => 'required',
            'username' => 'required',
            'password' => 'required',
            'encryption' => 'required',
        ]);
        $model = SmtpConfig::where('uuid', $uuid)->firstOrFail();
        $data = $request->only(['port', 'host', 'encryption', 'username', 'password']);
        DB::beginTransaction();
        try {
            $model->update($data);
            DB::commit();
            $response = responseSuccess(__('messages.update-success'), $model);
            return response()->json($response, 200);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.update-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function destroy($uuid)
    {
        $this->isValidUuid($uuid);
        $model = SmtpConfig::where('uuid', $uuid)->firstOrFail();

        DB::beginTransaction();
        try {
            $model->delete();
            DB::commit();
            $response = responseSuccess(__('messages.delete-success'), $model);
            return response()->json($response, 200);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.delete-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function active($uuid)
    {
        $this->isValidUuid($uuid);
        $model = SmtpConfig::where('uuid', $uuid)->firstOrFail();

        DB::beginTransaction();
        try {
            SmtpConfig::query()->update(["status" => "n"]);
            $model->update(["status" => "y"]);
            DB::commit();
            $response = responseSuccess(__('messages.update-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.update-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}

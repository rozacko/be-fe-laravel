<?php

namespace App\Http\Controllers\Configuration;

use App\Http\Controllers\Controller;
use App\Models\Action;
use Illuminate\Http\Request;

class ActionController extends Controller
{
    public function index(Request $request)
    {
        if ($request->wantsJson()) {
            return datatables(Action::query())->toJson();
        } else {
            return view("pages.action.index");
        }
    }
}

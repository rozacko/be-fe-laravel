<?php

namespace App\Http\Controllers\Configuration;

use App\Http\Controllers\Controller;
use App\Models\SapSyncConfig;
use App\Models\SapSyncLog;
use App\Services\SyncSapService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SapSyncLogController extends Controller
{
    public function index(Request $request)
    {
        if ($request->wantsJson()) {
            return datatables(SapSyncLog::query())->toJson();
        } else {
            return view("pages.sap.log.index");
        }
    }

    public function reSync($id)
    {
        $log = SapSyncLog::findOrFail($id);
        $config = SapSyncConfig::findOrFail($log->sync_sap_config_id);
        $config->parameter = $log->parameter;
        $config->name = $log->config_name;
        $response = (new SyncSapService)->execute(null, null, null, $config);
        if ($response['status'] == 'fail') {
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        return response()->json($response);
    }
}

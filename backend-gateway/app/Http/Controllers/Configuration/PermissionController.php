<?php

namespace App\Http\Controllers\Configuration;

use App\Http\Controllers\Controller;
use App\Models\Permission;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class PermissionController extends Controller
{
    public function index(Request $request)
    {
        $columns = ['modules.name' => 'name', 'features.name' => 'name', 'actions.name' => 'name', 'permissions.name' => 'name'];
        $query = Permission::Datatable();
        $model = Datatables::of($query)
            ->escapeColumns([])
            ->filter(function ($query) use ($request, $columns) {
                foreach ($columns as $key => $value) {
                    if ($request->has($value)) {
                        if ($request->get($value)) {
                            $query->orWhere($key, 'ilike', "%{$request->get($value)}%");
                        }
                    }
                }
            })
            ->make(true)->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response, 200);
    }
}

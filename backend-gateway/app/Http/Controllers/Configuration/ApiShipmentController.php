<?php

namespace App\Http\Controllers\Configuration;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\M_ApiShipment;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class ApiShipmentController extends Controller
{
    //
    public function index(Request $request)
    {
        return view("pages.shipment.api.index");
    }

    public function list()
    {
        $data = M_ApiShipment::query();
        return datatables($data)->toJson();
    }

    public function create()
    {
        return view("pages.shipment.api.create");
    }

    public function store(Request $request)
    {
        $data = $request->only(['api_name','api_url','api_method','api_params']);

        $model = M_ApiShipment::create($data);
        return redirect('config/shipmentapi');
    }

    public function show($uuid)
    {
        $model = M_ApiShipment::where('uuid', $uuid)->first();
        return view("pages.shipment.api.show", $model);
    }

    public function edit($uuid)
    {
        $this->isValidUuid($uuid);
        $model = M_ApiShipment::where('uuid', $uuid)->first();
        return view("pages.shipment.api.edit", $model);
    }

    public function update(Request $request, $uuid)
    {
        $this->isValidUuid($uuid);
        $model = M_ApiShipment::where('uuid', $uuid)->firstOrFail();
        $data =  $request->only(['api_name','api_url','api_method']);

        foreach($request->_params['key'] as $k=>$v){
            $data['api_params'][$v] = $request->_params['value'][$k];
        }
        $data['api_params'] = json_encode($data['api_params']);

        $model->update($data);
        return redirect('config/shipmentapi');
    }

    public function destroy($uuid)
    {
        $this->isValidUuid($uuid);
        $model = M_ApiShipment::where('uuid', $uuid)->firstOrFail();

        DB::beginTransaction();
        try {
            $model->delete();
            DB::commit();
            $response = responseSuccess(__('messages.delete-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.delete-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}

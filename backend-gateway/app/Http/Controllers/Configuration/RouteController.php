<?php

namespace App\Http\Controllers\Configuration;

use App\Http\Controllers\Controller;
use App\Models\Route;
use Illuminate\Http\Request;

class RouteController extends Controller
{
    public function index(Request $request)
    {
        if ($request->wantsJson()) {
            return datatables(Route::with('permissions'))->toJson();
        } else {
            return view("pages.routes.index");
        }
    }
}

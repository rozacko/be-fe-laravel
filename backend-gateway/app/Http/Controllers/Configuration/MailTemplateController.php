<?php

namespace App\Http\Controllers\Configuration;

use App\Http\Controllers\Controller;
use App\Models\MailTemplate;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class MailTemplateController extends Controller
{
    public function index(Request $request)
    {

        if ($request->wantsJson()) {
            return datatables(MailTemplate::query())->toJson();
        } else {
            return view("pages.mail_template.index");
        }
    }

    public function create()
    {
        return view("pages.mail_template.create");
    }

    public function store(Request $request)
    {
        $data = $request->only(['id', 'subject', 'template']);

        $model = MailTemplate::create($data);
        return redirect('config/mail_template');
    }

    public function show($uuid)
    {
        $model = MailTemplate::where('uuid', $uuid)->first();
        return view("pages.mail_template.show", $model);
    }

    public function edit($uuid)
    {
        $this->isValidUuid($uuid);
        $model = MailTemplate::where('uuid', $uuid)->first();
        return view("pages.mail_template.edit", $model);
    }

    public function update(Request $request, $uuid)
    {
        $this->isValidUuid($uuid);
        $model = MailTemplate::where('uuid', $uuid)->firstOrFail();
        $data = $request->only(['id', 'subject', 'template']);

        $model->update($data);
        return redirect('config/mail_template');
    }

    public function destroy($uuid)
    {
        $this->isValidUuid($uuid);
        $model = MailTemplate::where('uuid', $uuid)->firstOrFail();

        DB::beginTransaction();
        try {
            $model->delete();
            DB::commit();
            $response = responseSuccess(__('messages.delete-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.delete-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}

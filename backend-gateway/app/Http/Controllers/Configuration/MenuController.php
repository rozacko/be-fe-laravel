<?php

namespace App\Http\Controllers\Configuration;

use App\Http\Controllers\Controller;
use App\Models\Menu;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class MenuController extends Controller
{
    public function index(Request $request)
    {
        if ($request->wantsJson()) {
            $id = $request->get("id");
            if (!$id || $id === "#") {
                return Menu::root()->get();
            } else {
                return Menu::childs($id)->get();
            }
        }

        return view("pages.menu.index");
    }

    function list()
    {
        $menus = json_decode(json_encode(Menu::with('permission')->orderBy("order_no")->get()), true);
        $model = $this->buildTree($menus);
        return view("pages.menu.list", ["model" => $model]);
    }

    public function show($uuid)
    {
        $this->isValidUuid($uuid);
        $menu = Menu::Detail()->where("menus.uuid", $uuid)->first();
        return view("pages.menu.detail", compact("menu"));
    }

    public function create(Request $request, $id)
    {
        $menu = Menu::find($id);
        return view("pages.menu.create", compact("menu"));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "id" => "required|integer|unique:menus",
            'name' => 'required',
            "url" => "required",
            "permission_id" => "required"
        ]);
        $data = $request->only(['id', 'name', 'description', 'permission_id', 'url', 'order_no', 'icon', 'parent_id', 'powerbi_url']);
        if ($data['permission_id'] == "") {
            $data['permission_id'] = null;
        }

        if($data['powerbi_url']!="" || ! is_null($data['powerbi_url'])){
            $data['powerbi_url'] = base64_encode($data['powerbi_url']);
        }

        if ($data["parent_id"] == "0") {
            $data["order_no"] = $this->getMaxOrderParent() + 1;
        } else {
            $data["order_no"] = $this->getMaxOrderChild($data["parent_id"]) + 1;
        }
        DB::beginTransaction();
        try {
            $model = Menu::create($data);
            DB::commit();
            $response = responseSuccess(__('messages.create-success'), $model);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function update(Request $request, $uuid)
    {
        $this->isValidUuid($uuid);
        $this->validate($request, [
            'name' => 'required',
            "url" => "required",
            "permission_id" => "required"
        ]);
        $model = Menu::where('uuid', $uuid)->firstOrFail();
        $data = $request->only(['name', 'description', 'permission_id', 'url', 'order_no', 'icon', 'parent_id', 'powerbi_url']);
        if($data['powerbi_url']!="" || ! is_null($data['powerbi_url'])){
            $data['powerbi_url'] = base64_encode($data['powerbi_url']);
        }
        DB::beginTransaction();
        try {
            $model->update($data);
            DB::commit();
            $response = responseSuccess(__('messages.update-success'), $model);
            return response()->json($response, Response::HTTP_OK);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.update-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function updateSequence(Request $request)
    {
        $payload = $request->get("sequence");
        DB::beginTransaction();
        try {
            $this->saveSequence($payload);
            DB::commit();
            $response = responseSuccess(__('messages.update-success'), []);
            return response()->json($response, Response::HTTP_OK);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.update-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function destroy($uuid)
    {
        $this->isValidUuid($uuid);
        $menu = Menu::whereUuid($uuid)->firstOrFail();
        DB::beginTransaction();
        try {
            $menu->delete();
            DB::commit();
            $response = responseSuccess(__('messages.delete-success'), $menu);
            return response()->json($response, Response::HTTP_OK);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.delete-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private function getMaxOrderParent()
    {
        $max = Menu::where("parent_id", "0")->max("order_no");
        return $max;
    }

    private function getMaxOrderChild($parent_id)
    {
        $max = Menu::where("parent_id", $parent_id)->max("order_no");
        return $max;
    }

    private function buildTree(array $menus, $parentId = 0)
    {
        $branch = array();

        foreach ($menus as $menu) {
            if ($menu['parent_id'] == $parentId && !is_null($menu['parent_id'])) {
                $children = $this->buildTree($menus, $menu['id']);
                if ($children) {
                    $menu['children'] = $children;
                }
                $branch[] = $menu;
            }
        }

        return $branch;
    }

    private function saveSequence($sequence, $parent_id = 0)
    {
        foreach ($sequence as $key => $value) {
            Menu::where("id", $value["id"])->update(["order_no" => $key + 1, "parent_id" => $parent_id]);
            if (isset($value['children'])) {
                $this->saveSequence($value['children'], $value['id']);
            }
        }
    }
}

<?php

namespace App\Http\Controllers\Overhaul;

use App\Http\Controllers\Controller;
use App\Models\T_OverhaulFinalReport;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class OverhaulFinalReportController extends Controller
{
    //
    public function index(Request $request){
        $data = T_OverhaulFinalReport::where('id_plant', $request->plant)
                        ->where('report_year', $request->tahun)
                        ->orderBy('created_at','desc')
                        ->get();

        $model = DataTables::of($data)
                            ->editColumn('filepath',function($row){
                                return ReadFiles($row->filepath);
                            })
                            ->addColumn('action', function($row){
                                return '<a href="#" onclick="btn_delete(\''.$row->uuid.'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash" style="padding-right: 0px; font-size:10;"></i></a> ';
                            })
                            ->make(true)
                            ->getData(true);

        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    public function add(Request $request){
        DB::beginTransaction();
        try{
            $validation = [
                'upload_file' => 'required|mimes:pdf,jpeg,png',
                'id_plant' => 'required',
                'report_year' => 'required',
                'title' => 'required'
            ];
            $this->validate($request, $validation);

            $path = StoreFiles($request, $request->filename, '/overhaul/finalreport');
            $params = $request->only(['id_plant','report_year','title']);
            $params['filepath'] = $path;
            $model = T_OverhaulFinalReport::create($params);

            DB::commit();
            $response = responseSuccess(__('messages.create-success'), $model);
            return response()->json($response, Response::HTTP_CREATED);
        }
        catch(\Exception $ex){
            DB::rollBack();
            $response = responseFail(__('messages.create-fail'), $ex->getTraceAsString());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function destroy($uuid){
        $this->isValidUuid($uuid);
        $model = T_OverhaulFinalReport::where('uuid', $uuid)->firstOrFail();

        DB::beginTransaction();
        try {
            $model->delete();
            DB::commit();
            $response = responseSuccess(__('messages.delete-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.delete-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}

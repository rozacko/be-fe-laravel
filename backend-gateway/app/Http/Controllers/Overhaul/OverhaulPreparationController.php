<?php

namespace App\Http\Controllers\Overhaul;

use App\Exports\OverhaulBudgetTemplate;
use App\Exports\OverhaulPreparationBiayaTemplate;
use App\Exports\OverhaulPreparationSparepartTemplate;
use App\Exports\OverhaulPreparationTemplate;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Imports\OverhaulBudgetImport;
use App\Imports\OverhaulPreparationBiayaImport;
use App\Imports\OverhaulPreparationImport;
use App\Imports\OverhaulPreparationSparepartImport;
use App\Models\MPlant;
use App\Models\MWorkgroup;
use App\Models\T_OverhaulActivity;
use App\Models\T_OverhaulIssues;
use App\Models\T_OverhaulMpTool;
use App\Models\T_OverhaulPeriode;
use App\Models\T_OverhaulSparePart;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\DataTables;

class OverhaulPreparationController extends Controller
{
    /**
     * @OA\Get(
     *   tags={"Dashboard Overhaul Preparation"},
     *   path="/api/overhaul/preparation/opex",
     *   summary="Get budget opex overhaul",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */
    public function getBudgetOpex(Request $request){

    }

    /**
     * @OA\Get(
     *   tags={"Dashboard Overhaul Preparation"},
     *   path="/api/overhaul/preparation/getChartByStatus",
     *   summary="Get Chart Part Preparation By Status",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"status","id_plant"},
     *       @OA\Property(property="status", type="string", format="text", example="Rutin/Non-Rutin"),
     *       @OA\Property(property="id_plant", type="string", format="text", example="id-plant"),
     *     )
     *   )
     * )
     */
    public function getChartPartByStatus(Request $request){
        $data = T_OverhaulSparePart::select(DB::raw("count(*) as total"), 'status')
                                    ->where('category',$request->status)
                                     ->where('id_plant',$request->id_plant)
                                     ->groupBy('status')
                                     ->get();
        $response = responseSuccess(__('messages.read-success'), $data);
        return response()->json($response);
    }

    /**
     * @OA\Get(
     *   tags={"Dashboard Overhaul Preparation"},
     *   path="/api/overhaul/preparation/getDataTableByStatus",
     *   summary="Get Datatable Part Preparation By Status",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"status","id_plant"},
     *       @OA\Property(property="status", type="string", format="text", example="Rutin/Non-Rutin"),
     *       @OA\Property(property="id_plant", type="string", format="text", example="id-plant"),
     *     )
     *   )
     * )
     */
    public function getDataTablePartByStatus(Request $request)
    {
        $data = T_OverhaulMpTool::select('id_unitkerja',
                                        DB::raw("sum(case when status='READY' then 1 else 0 end) as total_ready"),
                                        DB::raw("sum(case when status!='READY' then 1 else 0 end) as total_nonready"))
                                        ->where('category',$request->status)
                                     ->where('id_plant',$request->id_plant)
                                     ->groupBy('id_unitkerja')
                                     ->get();

        $model = DataTables::of($data)
                    ->addIndexColumn()      
                    ->addColumn('unit_kerja',function($row){
                        $unitkerja = MWorkgroup::find($row->id_unitkerja);
                        return $unitkerja->unit_name;
                    })
                    ->editColumn('total_ready',function($row){
                        return (double) $row->total_ready/$row->total;
                    })
                    ->editColumn('total_nonready',function($row){
                        return (double) $row->total_nonready/$row->total;
                    })
                    ->editColumn('total',function($row){
                        return (double) 100;
                    })
                    ->make(true)
                    ->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    /**
     * @OA\Get(
     *   tags={"Dashboard Overhaul Preparation"},
     *   path="/api/overhaul/preparation/import",
     *   summary="Get Overhaul Preparation Template",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="Ok"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */
    public function templatePreparation(Request $request)
    {
        return Excel::download(new OverhaulPreparationTemplate(), 'overhaul_preparation_data.xlsx');
    }

    /**
     * @OA\Post(
     *   tags={"Dashboard Overhaul Preparation"},
     *   path="/api/overhaul/preparation/import",
     *   summary="Upload Overhaul Data Preparation",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"id_plant","tahun","upload_file"},
     *       @OA\Property(property="id_plant", type="string", format="text", example="12345"),
     *       @OA\Property(property="tahun", type="string", format="text", example="2023"),
     *       @OA\Property(property="upload_file", type="file", format="text", example="activity.xlsx"),
     *     )
     *   )
     * )
     */
    public function uploadData(Request $request)
    {
       DB::beginTransaction();
       try{
            Excel::import(new OverhaulPreparationImport($request->id_plant, $request->tahun), request()->file('upload_file'));
            DB::commit();
            $response = responseSuccess(__('messages.create-success'));
            return response()->json($response, Response::HTTP_CREATED);
       }
       catch(\Exception $ex){
            DB::rollBack();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
       }
    }

    /**
     * @OA\Get(
     *   tags={"Dashboard Overhaul Preparation"},
     *   path="/api/overhaul/preparation/budget",
     *   summary="Get Overhaul Budget Preparation Template",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="Ok"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */
    public function templateBudget(Request $request)
    {
        return Excel::download(new OverhaulBudgetTemplate(), 'overhaul_budget_template.xlsx');
    }

    /**
     * @OA\Post(
     *   tags={"Dashboard Overhaul Preparation"},
     *   path="/api/overhaul/preparation/budget",
     *   summary="Upload Overhaul Budget Preparation",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"id_plant","tahun","upload_file"},
     *       @OA\Property(property="id_plant", type="string", format="text", example="12345"),
     *       @OA\Property(property="tahun", type="string", format="text", example="2023"),
     *       @OA\Property(property="upload_file", type="file", format="text", example="activity.xlsx"),
     *     )
     *   )
     * )
     */
    public function uploadBudget(Request $request)
    {
       DB::beginTransaction();
       try{
            Excel::import(new OverhaulBudgetImport($request->id_plant, $request->tahun), request()->file('upload_file'));
            DB::commit();
            $response = responseSuccess(__('messages.create-success'));
            return response()->json($response, Response::HTTP_CREATED);
       }
       catch(\Exception $ex){
            DB::rollBack();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
       }
    }

    
     /**
     * @OA\Get(
     *   tags={"Dashboard Overhaul Preparation"},
     *   path="/api/overhaul/preparation/partchart",
     *   summary="Get chart status part",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */
    public function getChartPart(Request $request)
    {
        $model = T_OverhaulSparePart::select('status', 
                        DB::raw("count(*) as jumlah_part"))
                    ->where(DB::raw("lower(category)"), strtolower($request->category))
                    ->where('id_plant', $request->id_plant)
                    ->where('ovh_year', $request->tahun)
                    ->groupBy('status')
                    ->orderBy('status')
                    ->get();

        $data = ['series' => [], 'label' => []];
        foreach($model as $mdl){
            $data['series'][] = $mdl->jumlah_part;
            $data['label'][] = $mdl->status;
        }

        $response = responseSuccess(__('messages.read-success'), $data);
        return response()->json($response);
    }

    /**
     * @OA\Get(
     *   tags={"Dashboard Overhaul Preparation"},
     *   path="/api/overhaul/preparation/parttable",
     *   summary="Get table status part",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */
    public function getTablePart(Request $request){
        $data = T_OverhaulSparePart::select('nm_unit_kerja',
                                DB::raw('(SUM(case when category=\'Ready\' then 1 else 0 end)::double precision/count(*)::double precision)::double precision * 100::double precision as ready'),
                                DB::raw('(SUM(case when category!=\'Ready\' then 1 else 0 end)::double precision/count(*)::double precision)::double precision * 100::double precision as not_ready'),
                                DB::raw('\'100\' as total'))
                    ->where('id_plant',$request->id_plant)
                    ->where('category',$request->category)
                    ->where('ovh_year',$request->tahun)
                    ->groupBy('nm_unit_kerja')
                    ->get();

        $model = DataTables::of($data)
                    ->addIndexColumn()   
                    ->make(true)
                    ->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

     /**
     * @OA\Get(
     *   tags={"Dashboard Overhaul Preparation"},
     *   path="/api/overhaul/preparation/timeline",
     *   summary="Get timeline overhaul",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */
    public function timeLine(Request $request){
        $plant = $request->plant;
        $periode = T_OverhaulPeriode::where('id_plant', $plant)->where('status','Open')->first();
        $activities = T_OverhaulActivity::whereBetween('start_date', [$periode->start_date, $periode->end_date])
                                ->orWhereBetween('end_date', [$periode->start_date, $periode->end_date])
                                ->get();
        $data = [];
        foreach($activities as $activity){
            $data[] = ['x' => $activity->activity_name, 'y' => [$activity->start_date, $activity->end_date]];
        }

        $response = responseSuccess(__('messages.read-success'), $data);
        return response()->json($response);
    }

       /**
     * @OA\Get(
     *   tags={"Dashboard Overhaul Preparation"},
     *   path="/api/overhaul/preparation/barchartbudget",
     *   summary="Get table bar chart",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */
    public function barChartBudget(Request $request){
        $data_part = T_OverhaulSparePart::select('nm_unit_kerja','category',DB::raw('sum(total_price) as total_harga'))
                                ->where('ovh_year', $request->tahun)
                                ->where('id_plant', $request->id_plant)
                                ->where('budget','Opex')
                                ->groupBy('nm_unit_kerja','category')
                                ->get();

        $_budget = [];
        foreach($data_part as $row){
            if(isset($_budget[$row->nm_unit_kerja][$row->category]))
                $_budget[$row->nm_unit_kerja][$row->category] += (double) $row->total_harga;
            else
                $_budget[$row->nm_unit_kerja][$row->category] = (double) $row->total_harga;
        }

        $data_activity = T_OverhaulActivity::select('unit_kerja','jenis_pekerjaan',
                                    DB::raw('sum(total_manpower+total_consumable+total_tool) as total_harga'))
                                ->whereYear('start_date', $request->tahun)
                                ->where('id_plant', $request->id_plant)
                                ->where('budget_category','Opex')
                                ->groupBy('unit_kerja','jenis_pekerjaan')
                                ->get();

        foreach($data_activity as $row){
            if(isset($_budget[$row->unit_kerja][$row->jenis_pekerjaan]))
                $_budget[$row->unit_kerja][$row->jenis_pekerjaan] += (double) $row->total_harga;
            else
                $_budget[$row->unit_kerja][$row->jenis_pekerjaan] = (double) $row->total_harga;
        }

        $data = [];
        foreach($_budget as $key=>$value){
            $data['categories'][] = $key;

            if(isset($value['Rutin']))
                $data['series'][0]['data'][] = $value['Rutin'];
            else
                $data['series'][0]['data'][] = 0;

            if(isset($value['Non-Rutin']))
                $data['series'][1]['data'][] = $value['Non-Rutin'];
            else
                $data['series'][1]['data'][] = 0;
        }

        $data['series'][0]['name'] = 'Rutin';
        $data['series'][1]['name'] = 'Non-Rutin';

        $response = responseSuccess(__('messages.read-success'), $data);
        return response()->json($response);
    }

     /**
     * @OA\Get(
     *   tags={"Dashboard Overhaul Preparation"},
     *   path="/api/overhaul/preparation/partchartbudget",
     *   summary="Get chart part by budget",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */
    public function donutChartBudget(Request $request)
    {
        $model = T_OverhaulSparePart::select(
                        DB::raw('case when lower(status)=\'ready\' then status else \'Not Ready\' end as status'), 
                        DB::raw("count(*) as jumlah_part"))
                    ->where('id_plant', $request->id_plant)
                    ->where('ovh_year', $request->tahun)
                    ->where(DB::raw('trim(budget)'), $request->budget)
                    ->groupBy(DB::raw('case when lower(status)=\'ready\' then status else \'Not Ready\' end'))
                    ->orderBy(DB::raw('case when lower(status)=\'ready\' then status else \'Not Ready\' end'))
                    ->get();

        $data = ['series' => [], 'label' => []];
        foreach($model as $mdl){
            $data['series'][] = $mdl->jumlah_part;
            $data['label'][] = ucwords($mdl->status);
        }

        $response = responseSuccess(__('messages.read-success'), $data);
        return response()->json($response);
    }

     /**
     * @OA\Get(
     *   tags={"Dashboard Overhaul Preparation"},
     *   path="/api/overhaul/preparation/budgetopexdonut",
     *   summary="Get chart budget opex",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */
    public function budgetOpexChart(Request $request)
    {
        $data_part = T_OverhaulSparePart::select('category',DB::raw('sum(total_price) as total_harga'))
                                ->where('ovh_year', $request->tahun)
                                ->where('id_plant', $request->id_plant)
                                ->where('budget','Opex')
                                ->groupBy('category')
                                ->get();

        $_budget = [];
        foreach($data_part as $row){
            if(isset($_budget[$row->category]))
                $_budget[$row->category] += (double) $row->total_harga;
            else
                $_budget[$row->category] = (double) $row->total_harga;
        }

        $data_activity = T_OverhaulActivity::select('jenis_pekerjaan',
                                    DB::raw('sum(total_manpower+total_consumable+total_tool) as total_harga'))
                                ->whereYear('start_date', $request->tahun)
                                ->where('id_plant', $request->id_plant)
                                ->where('budget_category','Opex')
                                ->groupBy('jenis_pekerjaan')
                                ->get();

        foreach($data_activity as $row){
            if(isset($_budget[$row->jenis_pekerjaan]))
                $_budget[$row->jenis_pekerjaan] += (double) $row->total_harga;
            else
                $_budget[$row->jenis_pekerjaan] = (double) $row->total_harga;
        }

        $data = [];
        $data = ['series' => [], 'label' => []];

        foreach($_budget as $key=>$value){
            $data['series'][] = $value;
            $data['label'][] = ucwords($key);
        }

        $data['total'] = array_sum($data['series']);

        $response = responseSuccess(__('messages.read-success'), $data);
        return response()->json($response);
    }

    /**
     * @OA\Get(
     *   tags={"Dashboard Overhaul Preparation"},
     *   path="/api/overhaul/preparation/tablecapex",
     *   summary="Get table overhaul capex",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */
    public function tableCapex(Request $request){
        $data = T_OverhaulSparePart::select(DB::raw('nm_unit_kerja'),
                        DB::raw('coalesce(sum(total_price),0) as total_harga'))
                    ->where('id_plant',$request->id_plant)
                    ->where('budget','Capex')
                    ->where('ovh_year',$request->tahun)
                    ->groupBy('nm_unit_kerja')
                    ->get();

        $model = DataTables::of($data)
                    ->addIndexColumn()   
                    ->make(true)
                    ->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    } 

     /**
     * @OA\Get(
     *   tags={"Dashboard Overhaul Preparation"},
     *   path="/api/overhaul/preparation/tableissue",
     *   summary="Get table overhaul issues",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */
    public function tableIssue(Request $request){
        $data = T_OverhaulIssues::where('id_plant',$request->id_plant)
                    ->where('ovh_year',$request->tahun)
                    ->get();

        $model = DataTables::of($data)
                    ->addIndexColumn()   
                    ->make(true)
                    ->getData(true);

        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    } 
    
}

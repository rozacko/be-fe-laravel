<?php

namespace App\Http\Controllers\Overhaul;

use App\Exports\OverhaulActivityTemplate;
use App\Exports\OverhaulProgressTemplate;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Imports\OverhaulActivityImport;
use App\Imports\OverhaulProgressImport;
use App\Models\MArea;
use App\Models\MPlant;
use App\Models\SHEUnsafeCondition;
use App\Models\T_OverhaulActivity;
use App\Models\T_OverhaulActivityDokumentasi;
use App\Models\T_OverhaulActivityProgress;
use App\Models\T_OverhaulPeriode;
use App\Models\T_OverhaulSparePart;
use App\Models\T_OverhaulTimelinePeriode;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\CollectionDataTable;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Support\Facades\Storage;

class OverhaulProgressController extends Controller
{
    /**
     * @OA\Post(
     *   tags={"Dashboard Overhaul Progress"},
     *   path="/api/overhaul/progress/periode",
     *   summary="Insert New Overhaul Periode",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"id_plant","start_date","end_date"},
     *       @OA\Property(property="id_plant", type="string", format="text", example="12345"),
     *       @OA\Property(property="start_date", type="string", format="text", example="dd-mm-yyyy"),
     *       @OA\Property(property="end_date", type="string", format="text", example="dd-mm-yyyy"),
     *     )
     *   )
     * )
     */
    public function storeDataPeriode(Request $request)
    {
        $data = $request->only(
            'id_plant',
            'start_date',
            'end_date'
        );

        $this->validate($request, [
            'id_plant' => 'required',
            'start_date' => 'required',
            'end_date' => 'required'
        ]);

        DB::beginTransaction();
        try{
            T_OverhaulPeriode::where('status','Open')->where('id_plant',$data['id_plant'])->update(['status'=>'Close']);
            $data['start_date'] = Carbon::createFromFormat('d-m-Y',$data['start_date'])->format('Y-m-d');
            $data['end_date'] = Carbon::createFromFormat('d-m-Y',$data['end_date'])->format('Y-m-d');
            $model = T_OverhaulPeriode::create($data);
            DB::commit();
            $response = responseSuccess(__('messages.create-success'), $model);
            return response()->json($response, Response::HTTP_CREATED);
        }
        catch(\Exception $ex){
            DB::rollback();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

   /**
     * @OA\Get(
     *   tags={"Dashboard Overhaul Progress"},
     *   path="/api/overhaul/progress/periode/{plant}",
     *   summary="Get Overhaul Periode",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="plant", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Ok"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */
    public function getDataPeriode($plant, Request $request)
    {
        $data = T_OverhaulPeriode::select('*',DB::raw('(end_date - start_date)+1 as duration'),DB::raw('(current_date - start_date)+1 as current_day'))->where('id_plant',$plant)->where('status','Open')->first();
        $response = responseSuccess(__('messages.read-success'), $data);
        return response()->json($response);
    }

    /**
     * @OA\Get(
     *   tags={"Dashboard Overhaul Progress"},
     *   path="/api/overhaul/progress/major",
     *   summary="Get Overhaul Major Activity Progress",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="plant", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Parameter(name="category", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Ok"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */
    public function tableMajor(Request $request){
        $plant = $request->plant;
        $periode = T_OverhaulPeriode::where('id_plant', $plant)->where(DB::raw('lower(status)'),'open')->first();
        $areas = MArea::with(['overhaulActivity' => function($query) use($plant, $periode){
                            $query->where('id_plant', $plant)
                            ->where(function($query2) use ($periode){
                            $query2->whereBetween('start_date',[$periode->start_date, $periode->end_date])
                                ->orWhereBetween('end_date',[$periode->start_date, $periode->end_date]);
                            })
                            ->where(DB::raw('lower(activity_category)'),strtolower('major'));
                        }])
                    ->whereHas('overhaulActivity', function($query) use($plant, $periode){
                            $query->where('id_plant', $plant)
                            ->where(function($query2) use ($periode){
                            $query2->whereBetween('start_date',[$periode->start_date, $periode->end_date])
                                ->orWhereBetween('end_date',[$periode->start_date, $periode->end_date]);
                            })
                            ->where(DB::raw('lower(activity_category)'),strtolower('major'));
                        })->get();

        // return response()->json($areas);
        $collection = new Collection();
        foreach($areas as $area){
            $_areas[$area->nm_area] = ['ket' => $area->nm_area, 'status' => 'area'];
            foreach($area->overhaulActivity as $activity){
                $_coll['ket'] = $activity->activity_name;
                if(!is_null($activity->latestProgress)){
                    $_coll['plan']= $activity->latestProgress->plan_cumulative;
                    $_coll['real'] = $activity->latestProgress->real_cumulative;
                    $_coll['variance'] = doubleval($_coll['real']) - doubleval($_coll['plan']);
                }
                else{
                    $_coll['plan']= 0.00;
                    $_coll['real'] = 0.00;
                    $_coll['variance'] = 0.00;
                }
                $_areas[$area->nm_area]['activity'][] = $_coll;
            }
            $_areas[$area->nm_area]['plan'] = (double) array_sum(array_column($_areas[$area->nm_area]['activity'], 'plan')) / (double) count($_areas[$area->nm_area]['activity']) ;
            $_areas[$area->nm_area]['real'] = (double) array_sum(array_column($_areas[$area->nm_area]['activity'], 'real')) / (double) count($_areas[$area->nm_area]['activity']) ;
            $_areas[$area->nm_area]['variance'] = (double) $_areas[$area->nm_area]['real'] - (double) $_areas[$area->nm_area]['plan'];
        }
        $_plan = (double) array_sum(array_column($_areas, 'plan')) / (double) count($_areas);
        $_real = (double) array_sum(array_column($_areas, 'real')) / (double) count($_areas);
        $_variance = (double) $_real - (double) $_plan;
        $collection->push(['ket' => '', 'plan' => $_plan, 'real' => $_real, 'variance' => $_variance, 'status' => 'header']);

        foreach($_areas as $_area){
            $collection->push(['ket' => $_area['ket'], 'plan' => $_area['plan'], 'real' => $_area['real'], 'variance' => $_area['variance'], 'status' => 'area']);
            foreach($_area['activity'] as $_activity){
                $collection->push(['ket' => $_activity['ket'], 'plan' => $_activity['plan'], 'real' => $_activity['real'], 'variance' => $_activity['variance'], 'status' => 'activity']);
            }
        }

        return response()->json($collection);
    }

     /**
     * @OA\Get(
     *   tags={"Dashboard Overhaul Progress"},
     *   path="/api/overhaul/progress/minor",
     *   summary="Get Overhaul Minor Activity Progress",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="plant", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Parameter(name="category", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Ok"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */
    public function tableMinor(Request $request){
        $plant = $request->plant;
        $periode = T_OverhaulPeriode::where('id_plant', $plant)->where('status','Open')->first();
        $data = T_OverhaulActivity::with('latestProgress')
                                ->where('id_plant', $plant)
                                ->where('activity_category','Minor')
                                ->where(function($query) use ($periode){
                                    $query->whereBetween('start_date',[$periode->start_date, $periode->end_date])
                                        ->orWhereBetween('end_date',[$periode->start_date, $periode->end_date]);
                                })
                                ->orderBy('unit_kerja')
                                ->get();

        $collection = new Collection();
        $_unit_kerja = [];
        $_plan = 0.00;
        $_real = 0.00;
        $_count = 0;
        foreach($data as $row){
            if(!is_null($row->latestProgress)){
                $_unit_kerja[$row->unit_kerja]['plan'][] = $row->latestProgress->plan_cumulative;
                $_unit_kerja[$row->unit_kerja]['real'][] = $row->latestProgress->real_cumulative;
                $_unit_kerja[$row->unit_kerja]['variance'][] = doubleval($row->latestProgress->real_cumulative) - doubleval($row->latestProgress->plan_cumulative);

                $_plan += (double) $row->latestProgress->plan_cumulative;
                $_real += (double) $row->latestProgress->real_cumulative;
            }
            else{
                $_unit_kerja[$row->unit_kerja]['real'][] = 0.00;
                $_unit_kerja[$row->unit_kerja]['plan'][] = 0.00;
                $_unit_kerja[$row->unit_kerja]['variance'][] = 0.00;
            }
            $_count++;
        }
        
        $_plan = (double) $_plan / (double)  $_count;
        $_real =  (double) $_real / (double)  $_count;
        $_variance = (double) $_real - (double) $_plan;
        $collection->push(['ket' => '', 'plan' => $_plan, 'real' => $_real, 'variance' => $_variance, 'status' => 'header']);

        foreach($_unit_kerja as $_unit => $value){
            $_plan = (double) array_sum($value['plan']) / (double) count($value['plan']);
            $_real = (double) array_sum($value['real']) / (double) count($value['real']);
            $_variance = (double) $_real - (double) $_plan;
            $collection->push(['ket' => $_unit, 'plan' => $_plan, 'real' => $_real, 'variance' => $_variance]);
        }

        return response()->json($collection);
    }

    /**
     * @OA\Get(
     *   tags={"Dashboard Overhaul Progress"},
     *   path="/api/overhaul/progress/activity/{plant}",
     *   summary="Get Overhaul Activity on Periode",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="plant", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Ok"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */
    public function getPeriodeActivity($plant, Request $request)
    {
        $periode = T_OverhaulPeriode::where('id_plant', $plant)->where('status','Open')->first();
        $data = T_OverhaulActivity::with(['area' => function($query) {
                        $query->select('id','nm_area');
                    }])->where('id_plant', $plant)
                    ->where(function($query) use ($periode){
                        $query->whereBetween('start_date',[$periode->start_date, $periode->end_date])
                        ->orWhereBetween('end_date',[$periode->start_date, $periode->end_date]);
                    })
                    ->get();

        $response = responseSuccess(__('messages.read-success'), $data);
        return response()->json($response);
    }

    /**
     * @OA\Post(
     *   tags={"Dashboard Overhaul Progress"},
     *   path="/api/overhaul/progress/activity",
     *   summary="Insert New Overhaul Activity",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"id_plant","id_area","activity_name","start_date","end_date","activity_category"},
     *       @OA\Property(property="id_plant", type="string", format="text", example="12345"),
     *       @OA\Property(property="id_area", type="string", format="text", example="12345"),
     *       @OA\Property(property="activity_name", type="string", format="text", example="Perbaikan Mesin"),
     *       @OA\Property(property="start_date", type="string", format="text", example="dd-mm-yyyy"),
     *       @OA\Property(property="end_date", type="string", format="text", example="dd-mm-yyyy"),
     *       @OA\Property(property="activity_category", type="string", format="text", example="Major/Minor"),
     *     )
     *   )
     * )
     */
    public function storeDataActivity(Request $request)
    {
        $data = $request->only(
            'id_plant',
            'id_area',
            'activity_name',
            'start_date',
            'end_date',
            'activity_category'
        );

        $this->validate($request, [
            'id_plant' => 'required',
            'id_area' => 'required',
            'activity_name' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'activity_category' => 'required'
        ]);

        DB::beginTransaction();
        try{
            $data['start_date'] = Carbon::createFromFormat('d-m-Y',$data['start_date'])->format('Y-m-d');
            $data['end_date'] = Carbon::createFromFormat('d-m-Y',$data['end_date'])->format('Y-m-d');
            $model = T_OverhaulActivity::create($data);
            DB::commit();
            $response = responseSuccess(__('messages.create-success'), $model);
            return response()->json($response, Response::HTTP_CREATED);
        }
        catch(\Exception $ex){
            DB::rollback();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Post(
     *   tags={"Dashboard Overhaul Progress"},
     *   path="/api/overhaul/progress/progress",
     *   summary="Insert New Overhaul Activity Progress",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"id_activity","progress_date","plan_cumulative","real_cumulative"},
     *       @OA\Property(property="id_activity", type="string", format="text", example="12345"),
     *       @OA\Property(property="progress_date", type="string", format="text", example="dd-mm-yyyy"),
     *       @OA\Property(property="plan_cumulative", type="number", format="text", example="80.97"),
     *       @OA\Property(property="real_cumulative", type="number", format="text", example="50.00"),
     *       @OA\Property(property="dokumentasi", type="file", format="text"),
     *     )
     *   )
     * )
     */
    public function storeDataProgress(Request $request)
    {
        $data = $request->only(
            'id_activity',
            'progress_date',
            'plan_cumulative',
            'real_cumulative',
        );

        $this->validate($request, [
            'id_activity' => 'required',
            'progress_date' => 'required',
            'plan_cumulative' => 'required',
            'real_cumulative' => 'required',
            'upload_file' => 'mimes:jpg,jpeg,png',
        ]);

        DB::beginTransaction();
        try{
            $data['progress_date'] = Carbon::createFromFormat('d-m-Y',$data['progress_date'])->format('Y-m-d');
            $model = T_OverhaulActivityProgress::updateOrCreate(['id_activity' => $request->id_activity, 'progress_date' => $data['progress_date']], $data);            
            if($request->hasFile("upload_file")){
                T_OverhaulActivityDokumentasi::where('id_progress', $model->id)->delete();                
                $activity = T_OverhaulActivity::find($request->id_activity);
                $path = StoreFiles($request, $activity->activity_name, '/overhaul/progress');
                T_OverhaulActivityDokumentasi::create(['id_progress' => $model->id, 'file_path' => $path]);
            }
            DB::commit();
            $response = responseSuccess(__('messages.create-success'), $model);
            return response()->json($response, Response::HTTP_CREATED);
        }
        catch(\Exception $ex){
            DB::rollback();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *   tags={"Dashboard Overhaul Progress"},
     *   path="/api/overhaul/progress/template/activity",
     *   summary="Get Overhaul Activity Template",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="Ok"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */
    public function getActivityTemplate(Request $request)
    {
        return Excel::download(new OverhaulActivityTemplate, 'overhaul_activity_template.xlsx');
    }

    /**
     * @OA\Post(
     *   tags={"Dashboard Overhaul Progress"},
     *   path="/api/overhaul/progress/upload/activity",
     *   summary="Upload Overhaul Activity",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"id_plant","upload_file"},
     *       @OA\Property(property="id_plant", type="string", format="text", example="12345"),
     *       @OA\Property(property="upload_file", type="file", format="text", example="activity.xlsx"),
     *     )
     *   )
     * )
     */
    public function uploadActivity(Request $request)
    {
       DB::beginTransaction();
       try{
            Excel::import(new OverhaulActivityImport($request->id_plant), request()->file('upload_file'));
            DB::commit();
            $response = responseSuccess(__('messages.create-success'));
            return response()->json($response, Response::HTTP_CREATED);
       }
       catch(\Exception $ex){
            DB::rollBack();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
       }
    }

    /**
     * @OA\Get(
     *   tags={"Dashboard Overhaul Progress"},
     *   path="/api/overhaul/progress/template/progress",
     *   summary="Get Overhaul Activity Progress Template",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="Ok"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */
    public function getProgressTemplate($id_plant)
    {
        return Excel::download(new OverhaulProgressTemplate($id_plant), 'overhaul_progress_template.xlsx');
    }

    /**
     * @OA\Post(
     *   tags={"Dashboard Overhaul Progress"},
     *   path="/api/overhaul/progress/upload/progress",
     *   summary="Upload Overhaul Progress",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"id_plant","upload_file"},
     *       @OA\Property(property="id_plant", type="string", format="text", example="12345"),
     *       @OA\Property(property="upload_file", type="file", format="text", example="activity.xlsx"),
     *     )
     *   )
     * )
     */
    public function uploadProgress(Request $request)
    {
       DB::beginTransaction();
       try{
            Excel::import(new OverhaulProgressImport($request->id_plant), request()->file('upload_file'));
            DB::commit();
            $response = responseSuccess(__('messages.create-success'));
            return response()->json($response, Response::HTTP_CREATED);
       }
       catch(\Exception $ex){
            DB::rollBack();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
       }
    }

    /**
     * @OA\Post(
     *   tags={"Dashboard Overhaul Progress"},
     *   path="/api/overhaul/progress/timeline",
     *   summary="Upload Overhaul Progress",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"id_plant","upload_file"},
     *       @OA\Property(property="id_plant", type="string", format="text", example="12345"),
     *       @OA\Property(property="upload_file", type="file", format="text", example="timeline.png"),
     *     )
     *   )
     * )
     */
    public function uploadTimeline(Request $request)
    {
        $validation = [
            'upload_file' => 'required|mimes:jpeg,png',
            'id_plant' => 'required',
        ];
        $validator = Validator::make($request->all(), $validation);
        if (!$validator->fails()) {
            $param = $request->only(['id_plant']);
            DB::beginTransaction();
            try{
                $periode = T_OverhaulPeriode::where('id_plant', $request->id_plant)->where('status','Open')->first();
                $plant = MPlant::find($request->id_plant);
                $path = $request->file('upload_file')->storePublicly(
                    $plant->kd_plant.'/overhaul/timeline', 'minio'
                );
                $timeline = T_OverhaulTimelinePeriode::where('id_periode', $periode->id)->first();
                if($timeline){
                    Storage::disk('minio')->delete($timeline->path);
                    $timeline->path = $path;
                    $timeline->save();
                }
                else{
                    $param['id_periode'] = $periode->id;
                    $param['path'] = $path;
                    $timeline = T_OverhaulTimelinePeriode::create($param);
                }               
                DB::commit();
                $response = responseSuccess(__('messages.create-success'), $timeline);
                return response()->json($response, Response::HTTP_CREATED);
            }
            catch(\Exception $ex){
                DB::rollBack();
                $response = responseFail(__('messages.create-fail'), $ex->getMessage());
                return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
            }
        }
        else{
            $response = responseFail(__('messages.create-fail'), 'File harus bentuk jpg, png!');
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *   tags={"Dashboard Overhaul Progress"},
     *   path="/api/overhaul/progress/timeline/{plant}",
     *   summary="Get Overhaul Progress Timeline on Periode",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="plant", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Ok"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */
    public function getTimelineImage($plant)
    {
        $periode = T_OverhaulPeriode::where('id_plant', $plant)->where('status','Open')->first();
        $data = T_OverhaulTimelinePeriode::where('id_periode', $periode->id)->first();
        $data->path = Storage::url($data->path);
        $response = responseSuccess(__('messages.read-success'), $data);
        return response()->json($response);
    }

    /**
     * @OA\Get(
     *   tags={"Dashboard Overhaul Progress"},
     *   path="/api/overhaul/progress/dokumentasi/{plant}",
     *   summary="Get Overhaul Progress Gallery on Periode",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="plant", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Ok"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */
    public function getDokumentasi($plant)
    {
        $periode = T_OverhaulPeriode::where('id_plant', $plant)->where('status','Open')->first();
        $data = T_OverhaulActivity::with('dokumentasi')
                        ->where(function($query) use ($periode){
                        $query->whereBetween('start_date',[$periode->start_date, $periode->end_date])
                        ->orWhereBetween('end_date',[$periode->start_date, $periode->end_date]);
                    })
                    ->get();
        $gallery = [];
        foreach($data as $dt){
            foreach($dt->dokumentasi as $pict){
                $gallery[] = ['activity_name' => $dt->activity_name, 'img_url' => ReadFiles($pict->file_path), 'img_id' => 'doc-'.$pict->id];
            }
        }
        $response = responseSuccess(__('messages.read-success'), $gallery);
        return response()->json($response);
    }

    /**
     * @OA\Get(
     *   tags={"Chart Overhaul Progress"},
     *   path="/api/overhaul/progress/chart/{plant}",
     *   summary="Get Overhaul Progress Gallery on Periode",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="plant", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Ok"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */
    public function getChart($plant, Request $request){
        $periode = T_OverhaulPeriode::where('id_plant', $plant)->where('status','Open')->first();
        $activities = T_OverhaulActivity::with('progress')
                            ->whereBetween('start_date', [$periode->start_date, $periode->end_date])
                            ->orWhereBetween('end_date', [$periode->start_date, $periode->end_date])
                            ->get();

        $_data_raw = [];
        foreach($activities as $activity){
            foreach($activity->progress as $_progress){
                $_data_raw['plan_'.strtolower($activity->activity_category)][$_progress->progress_date][] = (double) $_progress->plan_cumulative;
                $_data_raw['real_'.strtolower($activity->activity_category)][$_progress->progress_date][] = (double) $_progress->real_cumulative;
            }
        }

        $_sum_chart = [];        
        foreach($_data_raw['plan_major'] as $key=>$value){
            $_sum_chart['Plan Major'][$key] = (double) array_sum($value) / (double) count($value);
        }
        foreach($_data_raw['plan_minor'] as $key=>$value){
            $_sum_chart['Plan Minor'][$key] = (double) array_sum($value) / (double) count($value);
        }
        foreach($_data_raw['real_major'] as $key=>$value){
            $_sum_chart['Actual Major'][$key] = (double) array_sum($value) / (double) count($value);
        }
        foreach($_data_raw['real_minor'] as $key=>$value){
            $_sum_chart['Actual Minor'][$key] = (double) array_sum($value) / (double) count($value);
        }

        $_chart = [];
        foreach($_sum_chart as $key=>$value){
            $_arr = [];
            foreach($value as $_k => $_v){
                $_arr[] = ['x' => $_k, 'y' => $_v];
            }
            $_chart[] = ['name' => $key, 'data' => $_arr];
        }

        $_min_date = [];
        $_min_date[] = min(array_keys($_data_raw['plan_major']));
        $_min_date[] = min(array_keys($_data_raw['plan_minor']));

        $data['series'] = $_chart;
        $data['start_idle'] = min($_min_date);
        $data['max_idle'] = \Carbon\Carbon::createFromFormat('Y-m-d',$periode->start_date)->subDay()->format('Y-m-d');

        $response = responseSuccess(__('messages.read-success'), $data);
        return response()->json($response);
    }

    /**
     * @OA\Get(
     *   tags={"Dashboard Overhaul Progress"},
     *   path="/api/overhaul/progress/unsafe",
     *   summary="Get table unsafe condition",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */
    public function getUnsafe(Request $request){
        $plant = MPlant::find($request->plant);
        $periode = T_OverhaulPeriode::where('id_plant', $request->plant)->where('status','Open')->first();

        $data = SHEUnsafeCondition::select('area', 
                                DB::raw('count(*) as total'),
                                DB::raw('sum(case when tgl_closing is null then 1 else 0 end) as open'),
                                DB::raw('sum(case when tgl_closing is not null then 1 else 0 end) as close'),                                
                                DB::raw('(sum(case when tgl_closing is not null then 1 else 0 end)::float / count(*)::float) * 100::float as progress')
                            )
                            ->where(function($query) use($periode) {
                                $query->whereBetween('tgl_temuan', [$periode->start_date, $periode->end_date])
                                    ->orWhereBetween('tgl_temuan', [$periode->start_date, $periode->end_date]);
                            })
                            ->where('lokasi_kerja','ilike','%'.$plant->nm_plant.'%')
                            ->groupBy('area')
                            ->get();

        $model = DataTables::of($data)
                    ->addIndexColumn()   
                    ->make(true)
                    ->getData(true);

        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

     /**
     * @OA\Get(
     *   tags={"Dashboard Overhaul Progress"},
     *   path="/api/overhaul/progress/opex",
     *   summary="Get table Progress Budget Opex",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */
    public function getOpex(Request $request){
        $periode = T_OverhaulPeriode::where('id_plant', $request->plant)->where('status','Open')->first();
        $data = T_OverhaulSparePart::select('nm_unit_kerja', 
                            DB::raw('sum(total_price) as rencana'),
                            DB::raw('sum(case when lower(status)=\'ready\' then total_price else 0 end) as realisasi')
                            )
                        ->where(function($query) use($periode){
                            $query->where('ovh_year', \Carbon\Carbon::createFromFormat('Y-m-d', $periode->start_date)->format('Y'))
                            ->orWhere('ovh_year', \Carbon\Carbon::createFromFormat('Y-m-d', $periode->end_date)->format('Y'));
                        })
                        ->groupBy('nm_unit_kerja')
                        ->orderBy('nm_unit_kerja')
                        ->get();

        $model = DataTables::of($data)
        ->addIndexColumn()   
        ->make(true)
        ->getData(true);
    
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }
}

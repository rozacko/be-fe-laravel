<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Tymon\JWTAuth\Facades\JWTAuth;

/**
 * @OA\Info(
 *      version="1.0.0",
 *      x={
 *          "logo": {
 *              "url": "https://via.placeholder.com/190x90.png?text=L5-Swagger"
 *          }
 *      },
 *      title="GHOPO API",
 *      description="GHOPO API Documentations",
 * )
 * 
 * @OA\SecurityScheme(
 *   securityScheme="token",
 *   type="apiKey",
 *   name="Authorization",
 *   in="header",
 *   description="The authorization header with the bearer token"
 * )
 */
class ApiController extends Controller
{
    public function register(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'username' => 'required|string|unique:users',
            'email' => 'required|email|unique:users',
            'password' => 'required|string|min:6|max:50'
        ]);

        $user = User::create([
            'name' => $request->name,
            'username' => $request->username,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);

        $response = responseSuccess(__('messages.create-success'), $user);
        return response()->json($response, Response::HTTP_CREATED);
    }

    /**
     * @OA\Post(
     *   tags={"Authorization"},
     *   path="/api/login",
     *   summary="Login to get token",
     *   @OA\Response(response=200, description="Successfully login"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"username","password"},
     *       @OA\Property(property="username", type="string", format="text", example="developer"),
     *       @OA\Property(property="password", type="string", format="text", example="developer"),
     *     )
     *   )
     * )
     */
    public function authenticate(Request $request)
    {
        $credentials = $request->only('username', 'password');

        $this->validate($request, [
            'username' => 'required|string',
            'password' => 'required|string'
        ]);

        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                $response = responseFail(__('messages.login-fail'));
                return response()->json($response, Response::HTTP_BAD_REQUEST);
            }
        } catch (JWTException $e) {
            $response = responseFail(__('messages.login-fail'), ['message' => $e->getMessage()]);
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $response = responseSuccess(__('messages.login-success'), Auth::user());
        return response()->json($response)->header('Authorization', "Bearer $token");
    }

    /**
     * @OA\Get(
     *   tags={"Authorization"},
     *   path="/api/logout",
     *   summary="Logout from session",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=400, description="Bad request"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */
    public function logout(Request $request)
    {
        try {
            JWTAuth::invalidate();
            $response = responseSuccess(__('messages.logout-success'));
            return response()->json($response, Response::HTTP_OK, [], JSON_PRETTY_PRINT);
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            $response = responseFail(__('messages.logout-fail'), $e->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR, [], JSON_PRETTY_PRINT);
        }
    }
}

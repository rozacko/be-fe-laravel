<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function validate(Request $request, array $rules, array $messages = [], array $customAttributes = [])
    {
        $messages = [
            'required' => __('validation.required'),
            'unique'   => __('validation.unique'),
            'email'    => __('validation.email'),
            'numeric'  => __('validation.numeric'),
            'exists'   => __('validation.exists'),
            'date'     => __('validation.date'),
            'string'     => __('validation.string'),
            'min'     => __('validation.min'),
            'max'     => __('validation.max'),
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $response = responseFail(__('messages.validation-fail'), $validator->errors(), $request->all());
            $return = response()->json($response, Response::HTTP_BAD_REQUEST, [], JSON_PRETTY_PRINT);
            $return->throwResponse();
        }
    }

    /**
     * Check if a given string is a valid UUID
     *
     * @param string $uuid The string to check
     */
    public function isValidUuid($uuid)
    {
        if (!is_string($uuid) || (preg_match('/^[a-f\d]{8}(-[a-f\d]{4}){4}[a-f\d]{8}$/i', $uuid) !== 1)) {
            $response = responseFail(__('messages.uuid-fail'));
            $return = response()->json($response, Response::HTTP_BAD_REQUEST, [], JSON_PRETTY_PRINT);
            $return->throwResponse();
        }
    }

    public function validateImportBaseController($data, array $rules, $line)
    {
        $messages = [
            'required' => __('validation.required'),
            'unique'   => __('validation.unique'),
            'email'    => __('validation.email'),
            'numeric'  => __('validation.numeric'),
            'exists'   => __('validation.exists'),
            'date'     => __('validation.date'),
            'string'     => __('validation.string'),
            'min'     => __('validation.min'),
            'max'     => __('validation.max'),
        ];

        $validator = Validator::make($data, $rules, $messages);

        if ($validator->fails()) {
            DB::rollBack();
            $response = responseFail(__('messages.validation-fail') . " " . __('messages.inline-fail') . " : " . $line, $validator->errors(), []);
            $return = response()->json($response, Response::HTTP_BAD_REQUEST, [], JSON_PRETTY_PRINT);
            $return->throwResponse();
        }
    }
}

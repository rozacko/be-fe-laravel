<?php

namespace App\Http\Controllers\Tpm;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\LaporanTPM;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use App\Imports\LaporanTPMImport;
use Excel;
use Storage;

class TpmController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = new LaporanTPM();
    }

    /**
     * @OA\Post(
     *   tags={"LaporanTPM"},
     *   path="/api/tpm/importreport",
     *   summary="Import Report TPM",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully imported"),
     *   @OA\Response(response=400, description="Bad Request"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"tahun","excel"},
     *       @OA\Property(property="tahun", type="string", format="text", example="2023"),
     *       @OA\Property(property="excel", type="string", format="binary"),
     *     )
     *   )
     * )
     */
    public function storeImportReport(Request $request){

        $this->validate($request, [
            'tahun' => 'required',
        ]);
        DB::beginTransaction();
        try {
            Excel::import(new LaporanTPMImport($request->tahun), request()->file('upload_file'));
            DB::commit();
            $response = responseSuccess(__('messages.create-success'), []);
            return response()->json($response, Response::HTTP_CREATED);

        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *   tags={"LaporanTPM"},
     *   path="/api/tpm/scoreimplementation",
     *   summary="Get score implementation",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=401, description="Unauthorized"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"tahun"},
     *       @OA\Property(property="tahun", type="string", format="text", example="2023")
     *     )
     *   )
     * )
     */
    public function scoreImplementation(Request $request){

        $model =[
            'categori'=>["Q1","Q2","S1","Q3","Q4","S2",$request->tahun],
            'Realisasi'=>LaporanTPM::deskripsiTPM($request->tahun,LaporanTPM::TPMIMPLEMENTATION)->realisasi(),
            'Target'=>LaporanTPM::deskripsiTPM($request->tahun,LaporanTPM::TPMIMPLEMENTATION)->target(),
            'Achievement'=>LaporanTPM::deskripsiTPM($request->tahun,LaporanTPM::TPMIMPLEMENTATION)->achievement(),
        ];

        $response = responseSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }


    /**
     * @OA\Get(
     *   tags={"LaporanTPM"},
     *   path="/api/tpm/focusedimprovement",
     *   summary="Get Focused Improvement",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=401, description="Unauthorized"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"tahun"},
     *       @OA\Property(property="tahun", type="string", format="text", example="2023")
     *     )
     *   )
     * )
     */
    public function focusedImprovement(Request $request){
        $model =[
            'categori'=>["Q1","Q2","S1","Q3","Q4","S2",$request->tahun],
            'Realisasi'=>LaporanTPM::deskripsiTPM($request->tahun,LaporanTPM::FOCUSIMPROVEMENT)->realisasi(),
            'Target'=>LaporanTPM::deskripsiTPM($request->tahun,LaporanTPM::FOCUSIMPROVEMENT)->target(),
            'Achievement'=>LaporanTPM::deskripsiTPM($request->tahun,LaporanTPM::FOCUSIMPROVEMENT)->achievement(),
        ];

        $response = responseSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    /**
     * @OA\Get(
     *   tags={"LaporanTPM"},
     *   path="/api/tpm/focusarea",
     *   summary="Get Focus Area",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=401, description="Unauthorized"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"tahun"},
     *       @OA\Property(property="tahun", type="string", format="text", example="2023")
     *     )
     *   )
     * )
     */
    public function focusArea(Request $request){
        $model =[
            'categori'=>["Q1","Q2","S1","Q3","Q4","S2",$request->tahun],
            'Realisasi'=>LaporanTPM::deskripsiTPM($request->tahun,LaporanTPM::FOCUSAREA)->realisasi(),
            'Target'=>LaporanTPM::deskripsiTPM($request->tahun,LaporanTPM::FOCUSAREA)->target(),
            'Achievement'=>LaporanTPM::deskripsiTPM($request->tahun,LaporanTPM::FOCUSAREA)->achievement(),
        ];

        $response = responseSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    /**
     * @OA\Get(
     *   tags={"LaporanTPM"},
     *   path="/api/tpm/genbasga",
     *   summary="Get Genba SGA",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=401, description="Unauthorized"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"tahun"},
     *       @OA\Property(property="tahun", type="string", format="text", example="2023")
     *     )
     *   )
     * )
     */
    public function genbaSga(Request $request){
        $model =[
            'categori'=>["Q1","Q2","S1","Q3","Q4","S2",$request->tahun],
            'Realisasi'=>LaporanTPM::deskripsiTPM($request->tahun,LaporanTPM::GENBARATESGA)->realisasi(),
            'Target'=>LaporanTPM::deskripsiTPM($request->tahun,LaporanTPM::GENBARATESGA)->target(),
            'Achievement'=>LaporanTPM::deskripsiTPM($request->tahun,LaporanTPM::GENBARATESGA)->achievement(),
        ];

        $response = responseSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    /**
     * @OA\Get(
     *   tags={"LaporanTPM"},
     *   path="/api/tpm/partisipasiMember",
     *   summary="Get Partisipasi Member TPM",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=401, description="Unauthorized"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"tahun"},
     *       @OA\Property(property="tahun", type="string", format="text", example="2023")
     *     )
     *   )
     * )
     */
    public function partisipasiMember(Request $request){
        $model =[
            'categori'=>["Q1","Q2","S1","Q3","Q4","S2",$request->tahun],
            'Realisasi'=>LaporanTPM::deskripsiTPM($request->tahun,LaporanTPM::PARTISIPASIMEMBERTPM)->realisasi(),
            'Target'=>LaporanTPM::deskripsiTPM($request->tahun,LaporanTPM::PARTISIPASIMEMBERTPM)->target(),
            'Achievement'=>LaporanTPM::deskripsiTPM($request->tahun,LaporanTPM::PARTISIPASIMEMBERTPM)->achievement(),
        ];

        $response = responseSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

     /**
     * @OA\Get(
     *   tags={"LaporanTPM"},
     *   path="/api/tpm/downloadTemplate",
     *   summary="Download Template TPM",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */
    public function downloadTemplate(){

        return Storage::download('formattpm.xlsx');
    }

    /**
     * @OA\Get(
     *   tags={"LaporanTPM"},
     *   path="/api/tpm/listdashboardtpm",
     *   summary="Get Datatable Laporan TPM",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"bulan","tahun"},
     *       @OA\Property(property="bulan", type="string", format="text", example="02,03,04"),
     *       @OA\Property(property="tahun", type="string", format="text", example="2023"),
     *     )
     *   )
     * )
     */
    public function listTPM(Request $request){
        $data = LaporanTPM::where('tahun',$request->tahun)
                ->get();

        $model = DataTables::of($data)
                ->addIndexColumn()
                ->make(true)
                ->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);

    }
}

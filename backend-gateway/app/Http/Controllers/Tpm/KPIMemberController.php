<?php

namespace App\Http\Controllers\Tpm;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\LaporanKPIMember;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use App\Imports\KPIMemberImport;
use Excel;
use Storage;

class KPIMemberController extends Controller
{

    /**
     * @OA\Get(
     *   tags={"LaporanTPM"},
     *   path="/api/tpm/listLaporankpimember",
     *   summary="Get Datatable Laporan KPI Member",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"bulan","tahun"},
     *       @OA\Property(property="bulan", type="string", format="text", example="02,03,04"),
     *       @OA\Property(property="tahun", type="string", format="text", example="2023"),
     *       @OA\Property(property="fasilitator", type="string", format="text", example="BAHAN BAKU"),
     *       @OA\Property(property="gugus", type="string", format="text", example="CRUSHER 1-2"),
     *     )
     *   )
     * )
     */
    public function listLaporankpimember(Request $request){
        $data = LaporanKPIMember::where('tahun',$request->tahun);

        if($request->organisasi !="all"){
            $data->where('nama_organisasi',$request->organisasi);
        }
        if($request->unit_kerja !="all"){
            $data->where('unit_kerja',$request->unit_kerja);
        }

        $model = DataTables::of($data)
                ->addIndexColumn()
                ->make(true)
                ->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    /**
     * @OA\Post(
     *   tags={"LaporanTPM"},
     *   path="/api/tpm/importreportkpimember",
     *   summary="Import Report TPM KPIMEMBER",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully imported"),
     *   @OA\Response(response=400, description="Bad Request"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"bulan","tahun","excel"},
     *       @OA\Property(property="bulan", type="string", format="text", example="02"),
     *       @OA\Property(property="tahun", type="string", format="text", example="2023"),
     *       @OA\Property(property="excel", type="string", format="binary"),
     *     )
     *   )
     * )
     */
    public function storeImportReport(Request $request){

        $this->validate($request, [
            'tahun' => 'required',
        ]);
        DB::beginTransaction();
        try {
            Excel::import(new KPIMemberImport($request->tahun), request()->file('upload_file'));
            DB::commit();
            $response = responseSuccess(__('messages.create-success'), []);
            return response()->json($response, Response::HTTP_CREATED);

        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}

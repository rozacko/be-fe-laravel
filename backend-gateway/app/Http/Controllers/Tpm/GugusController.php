<?php

namespace App\Http\Controllers\Tpm;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\LaporanGugus;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use App\Imports\LaporanGugusImport;
use Excel;
use Storage;
use App\Exports\LaporanGugusExport;

class GugusController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = new LaporanGugus();
    }
    /**
     * @OA\Get(
     *   tags={"LaporanTPM"},
     *   path="/api/tpm/listLaporangugus",
     *   summary="Get Datatable Laporan Gugus",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"bulan","tahun"},
     *       @OA\Property(property="bulan", type="string", format="text", example="02,03,04"),
     *       @OA\Property(property="tahun", type="string", format="text", example="2023"),
     *       @OA\Property(property="fasilitator", type="string", format="text", example="BAHAN BAKU"),
     *       @OA\Property(property="gugus", type="string", format="text", example="CRUSHER 1-2"),
     *     )
     *   )
     * )
     */
    public function listLaporangugus(Request $request){
        $data = LaporanGugus::where('bulan',$request->bulan)
                ->where('tahun',$request->tahun);

        if($request->fasilitator !="all"){
            $data->where('fasilitator',$request->fasilitator);
        }
        if($request->gugus !="all"){
            $data->where('gugus',$request->gugus);
        }

        $model = DataTables::of($data->orderBy('fasilitator','ASC')->orderBy('gugus','ASC')->get())
                ->addColumn('total_gugus',function($data)use($request){
                    $jumlah = LaporanGugus::where('fasilitator',$data->fasilitator)
                              ->where('gugus',$data->gugus)
                              ->where('bulan',$request->bulan)
                              ->where('tahun',$request->tahun)
                              ->count();
                    return $jumlah;
                })
                ->addColumn('total_fasilitator',function($data)use($request){
                    $jumlah = LaporanGugus::where('fasilitator',$data->fasilitator)
                              ->where('bulan',$request->bulan)
                              ->where('tahun',$request->tahun)
                              ->count();
                    return $jumlah;
                })
                ->addIndexColumn()
                ->make(true)
                ->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);

    }

    /**
     * @OA\Post(
     *   tags={"LaporanTPM"},
     *   path="/api/tpm/importreportgugus",
     *   summary="Import Report TPM GUGUS",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully imported"),
     *   @OA\Response(response=400, description="Bad Request"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"bulan","tahun","excel"},
     *       @OA\Property(property="bulan", type="string", format="text", example="02"),
     *       @OA\Property(property="tahun", type="string", format="text", example="2023"),
     *       @OA\Property(property="excel", type="string", format="binary"),
     *     )
     *   )
     * )
     */
    public function storeImportReport(Request $request){

        $this->validate($request, [
            'bulan' => 'required',
            'tahun' => 'required',
        ]);
        DB::beginTransaction();
        try {
            Excel::import(new LaporanGugusImport($request->bulan,$request->tahun), request()->file('upload_file'));
            DB::commit();
            $response = responseSuccess(__('messages.create-success'), []);
            return response()->json($response, Response::HTTP_CREATED);

        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function exportGugus(Request $request){

        $model = LaporanGugus::where('bulan',$request->bulan)
                    ->where('tahun',$request->tahun)->get();
        $data = [
                'tanggal'=>self::namaBulan($request->bulan)." ".$request->tahun,
                'model'=>$model
            ];
        Excel::store(new LaporanGugusExport($data), 'laporan-gugus-'.$request->bulan.'-'.$request->tahun.'.xlsx', 'public');
        $responsex = [
            'message' => 'Laporan Gugus has been exported',
            'file_path' => url('api/tpm/downloadLaporan/laporan-gugus-'.$request->bulan.'-'.$request->tahun.'.xlsx')
        ];

        $response = responseSuccess(__('messages.create-success'), $responsex);
        return response()->json($response, Response::HTTP_CREATED);
    }

    public function downloadGugus($laporan){
        $path = storage_path('app/public/'.$laporan);
        return response()->download($path,$laporan);
    }

    private function namaBulan($bulan){
        $bulanList = [
            '01'=>"Januari",
            '02'=>"Februari",
            '03'=>"Maret",
            '04'=>"April",
            '05'=>"Mei",
            '06'=>"Juni",
            '07'=>"Juli",
            '08'=>"Agustus",
            '09'=>"September",
            '10'=>"Oktober",
            '11'=>"November",
            '12'=>"Desember",
        ];

        return $bulanList[$bulan];
    }
}

<?php

namespace App\Http\Controllers\Tpm;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\LaporanKPIMember;
use App\Models\LaporanGugus;

class FilterController extends Controller
{

    /**
     * @OA\Get(
     *   tags={"LaporanTPM"},
     *   path="/api/tpm/filterFasilitator",
     *   summary="Get Filter Fasilitator",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),

     * )
     */

    public function filterFasilitator(){

        $data = LaporanGugus::select('fasilitator')->distinct()->get();

        $response = responseSuccess(__('messages.read-success'), $data);
        return response()->json($response);
    }

    /**
     * @OA\Get(
     *   tags={"LaporanTPM"},
     *   path="/api/tpm/filterGugus",
     *   summary="Get Filter Gugus",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       @OA\Property(property="fasilitator", type="string", format="text", example="BAHAN BAKU")
     *     )
     *   )
     * )
     */

    public function filterGugus(Request $request){
        $data = LaporanGugus::select('gugus')
                ->where('fasilitator',$request->fasilitator)
                ->distinct()
                ->get();

        $response = responseSuccess(__('messages.read-success'), $data);
        return response()->json($response);
    }

    /**
     * @OA\Get(
     *   tags={"LaporanTPM"},
     *   path="/api/tpm/filterOrganisasi",
     *   summary="Get Filter Organisasi",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),

     * )
     */
    public function filterOrganisasi(){
        $data = LaporanKPIMember::select('nama_organisasi')->distinct()->get();

        $response = responseSuccess(__('messages.read-success'), $data);
        return response()->json($response);
    }

    /**
     * @OA\Get(
     *   tags={"LaporanTPM"},
     *   path="/api/tpm/filterUnitKerja",
     *   summary="Get Filter Unit Kerja",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       @OA\Property(property="organisasi", type="string", format="text", example="BAHAN BAKU")
     *     )
     *   )
     * )
     */

    public function filterUnitKerja(Request $request){
        $data = LaporanKPIMember::select('unit_kerja')
                ->where('nama_organisasi',$request->organisasi)
                ->distinct()
                ->get();
        $response = responseSuccess(__('messages.read-success'), $data);
        return response()->json($response);
    }

}

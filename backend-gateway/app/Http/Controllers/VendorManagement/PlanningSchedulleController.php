<?php

namespace App\Http\Controllers\VendorManagement;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TPlanningScheduleKerja;
use Illuminate\Http\Response;
use Yajra\DataTables\DataTables;
use App\Models\TPresensiPegawai;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\DownloadExcel;
use Illuminate\Support\Collection;

class PlanningSchedulleController extends Controller
{
    protected $tplanningschedulekerja;
    protected $tpresensipegawai;
    public function __construct()
    {
        $this->tplanningschedulekerja = new TPlanningScheduleKerja();
        $this->tpresensipegawai = new TPresensiPegawai();
    }

    protected function getListDataTablePlanning($request)
    {
        $query = $this->buildQueryIndex($request);

        return DataTables::of($query)
            ->addIndexColumn()
            ->make(true)
            ->getData(true);
    }

    /**
     * @OA\Get(
     *   tags={"Vendor Management"},
     *   path="/api/upload-schedule/list",
     *   summary="Get planning schedulle list",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="tahun", in="query", description="parameter tahun", required=true, @OA\Schema( type="string" )),
     *   @OA\Parameter(name="bulan", in="query", description="parameter bulan", required=true, @OA\Schema( type="string" )),
     *   @OA\Parameter(name="keyword", in="query", description="search keyword", required=false, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     *   @OA\Response(response=500, description="Internal Server Error"),
     * )
     */
    public function index(Request $request)
    {
        $this->validateIndex($request);
        try {
            $model = $this->getListDataTablePlanning($request);

            $response = responseDatatableSuccess(__('messages.read-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            $response = responseFail(__('messages.show-import-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    protected function buildQueryIndex($request)
    {
        $query = $this->tplanningschedulekerja->query();
        $query = $query->where(['tahun' => $request->tahun, 'bulan' => $request->bulan]);
        if ($request->has('keyword') && strlen($request->keyword) > 0) {
            $keyword = $request->keyword;
            $query->where(function ($query) use ($keyword) {
                $keyword = "%" . $keyword . "%";
                $query->where('no_pegawai', 'like', $keyword)
                    ->orWhere('nm_pegawai', 'like', $keyword);
            });
        }
        return $query;
    }

    protected function validateIndex($request)
    {
        $this->validate($request, [
            'tahun' => 'required',
            'bulan' => 'required',
            'keyword' => 'string|nullable'
        ]);
    }

    /**
     * @OA\Get(
     *   tags={"Vendor Management"},
     *   path="/api/planning-realisasi-fingerprint",
     *   summary="Get planning actual list",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="tahun", in="query", description="parameter tahun", required=true, @OA\Schema( type="string" )),
     *   @OA\Parameter(name="bulan", in="query", description="parameter bulan", required=true, @OA\Schema( type="string" )),
     *   @OA\Parameter(name="keyword", in="query", description="search keyword", required=false, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     *   @OA\Response(response=500, description="Internal Server Error"),
     * )
     */
    public function planningActual(Request $request)
    {
        $this->validateIndex($request);
        try {
            $bulan = $request->bulan;
            $tahun = $request->tahun;

            $model = $this->getListDataTablePlanning($request);
            if (!empty($model['data'])) {
                $resultPresence = $this->getTotalDataPresence($model['data'], $bulan, $tahun);
                if (strlen($resultPresence['error']) == 0) {
                    $model['data'] = $resultPresence['data'];
                } else {
                    $model['data'] = [];
                    $response = responseFail(__('messages.show-import-fail'), $resultPresence['error']);
                    return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
                }
            }

            $response = responseDatatableSuccess(__('messages.read-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            Log::error(__FUNCTION__ . " on line " . __LINE__ . " param " . func_get_args() . "message " . $ex->getMessage());
            $response = responseFail(__('messages.show-import-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    protected function getTotalDataPresence($arrPlanning, $bulan, $tahun) :Array
    {
        try {
            $arrPegawai = [];
            foreach ($arrPlanning as $key => $value) {
                $arrPegawai[] = (string)$value['no_pegawai'];
            }

            $totalDataPlanning = $this->tplanningschedulekerja->countDataByNoPegawai($arrPegawai, $bulan, $tahun);
            $arrTotalDataPlanning = [];
            foreach ($totalDataPlanning as $key => $value) {
                $nomerPegawai = $value->no_pegawai;
                $arrTotalDataPlanning[$nomerPegawai] = $value->total_planning;
            }
            $totalDataActual = $this->tpresensipegawai->countDataByNoPegawai($arrPegawai, $bulan, $tahun);
            $arrTotalDataActual = [];
            foreach ($totalDataActual as $key => $value) {
                $nomerPegawai = $value->no_pegawai;
                $arrTotalDataActual[$nomerPegawai] = $value->total_realisasi;
            }

            $reqMapping = ["planning" => $arrPlanning, "totalDataPlanning" => $arrTotalDataPlanning, "totalDataActual" => $arrTotalDataActual];

            $fixModel = $this->mappingDataPlanningActual($reqMapping);
            if (strlen($fixModel['error']) == 0) {
                return ["error" => "", "data" => $fixModel['data']];
            }
            return ["error" => $fixModel['error'], "data" => []];
        } catch (\Exception $ex) {
            Log::error(__FUNCTION__ . " on line " . __LINE__ . " param " . func_get_args() . "message " . $ex->getMessage());
            return ["error" => "error get total data presence", "data" => []];
        }
    }

    protected function mappingDataPlanningActual($reqMapping)
    {
        try {
            $arrTotalDataPlanning = $reqMapping['totalDataPlanning'];
            $arrTotalDataActual = $reqMapping['totalDataActual'];
            $arrPlanning = $reqMapping['planning'];

            $arrResult = [];
            foreach ($arrPlanning as $value) {
                $nomerPegawai = $value['no_pegawai'];
                $totalPlanning = 0;
                $totalRealisasi = 0;

                if (array_key_exists($nomerPegawai, $arrTotalDataPlanning)) {
                    $totalPlanning = $arrTotalDataPlanning[$nomerPegawai];
                }

                if (array_key_exists($nomerPegawai, $arrTotalDataActual)) {
                    $totalRealisasi = $arrTotalDataActual[$nomerPegawai];
                }

                $arrResult[] = array(
                    "id_planning_schedule_kerja" => $value['id_planning_schedule_kerja'],
                    "uuid" => $value['uuid'],
                    "no_pegawai" => $value['no_pegawai'],
                    "nm_pegawai" => $value['nm_pegawai'],
                    "spesifikasi" => $value['spesifikasi'],
                    "perusahaan" => $value['perusahaan'],
                    "tahun" => $value['tahun'],
                    "bulan" => $value['bulan'],
                    "planning" => $totalPlanning,
                    "realisasi" => $totalRealisasi,
                );
            }
            return ["error" => "", "data" => $arrResult];
        } catch (\Exception $ex) {
            Log::error(__FUNCTION__ . " on line " . __LINE__ . " param " . func_get_args() . "message " . $ex->getMessage());
            return ["error" => "error mapping data planning actual", "data" => []];
        }
    }

    /**
     * @OA\Post(
     *   tags={"Vendor Management"},
     *   path="/api/upload-schedule/download-excel",
     *   summary="download planning schedulle list",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="tahun", in="query", description="parameter tahun", required=true, @OA\Schema( type="string" )),
     *   @OA\Parameter(name="bulan", in="query", description="parameter bulan", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     *   @OA\Response(response=500, description="Internal Server Error"),
     * )
     */
    public function downloadPlanning(Request $request)
    {
        $this->validateIndex($request);
        try {
            $tahun = $request->tahun;
            $bulan = $request->bulan;
            $query = $this->buildQueryIndex($request);
            $dataPlanning = $query->get()->toArray();
            $dataExportPlanning = $this->mapDownloadData($dataPlanning);
            $fileName = sprintf("planning-%s_%s_%s.xlsx", $bulan, $tahun, date('YmdHis'));
            $arrTanggal = [];
            for ($i = 1; $i < 32; $i++) {
                $arrTanggal[] = (string) $i;
            }
            $columns = array_merge(["No", "No Pegawai", "Nama Pegawai", "Spesifikasi", "Perusahaan", "Tahun", "Bulan"], $arrTanggal);
            return Excel::download((new DownloadExcel($dataExportPlanning, $columns)), $fileName);
        } catch (\Exception $ex) {
            $response = responseFail(__('messages.show-import-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function mapDownloadData($dataPlanning): Collection
    {
        $arrResult = [];
        foreach ($dataPlanning as $key => $value) {
            $number = $key + 1;
            $arrResult[] = [
                "no" => $number,
                "no_pegawai" => $value['no_pegawai'],
                "nm_pegawai" => $value['nm_pegawai'],
                "spesifikasi" => $value['spesifikasi'],
                "perusahaan" => $value['perusahaan'],
                "tahun" => $value['tahun'],
                "bulan" => $value['bulan'],
                "tgl_1" => $value['tgl_1'],
                "tgl_2" => $value['tgl_2'],
                "tgl_3" => $value['tgl_3'],
                "tgl_4" => $value['tgl_4'],
                "tgl_5" => $value['tgl_5'],
                "tgl_6" => $value['tgl_6'],
                "tgl_7" => $value['tgl_7'],
                "tgl_8" => $value['tgl_8'],
                "tgl_9" => $value['tgl_9'],
                "tgl_10" => $value['tgl_10'],
                "tgl_11" => $value['tgl_11'],
                "tgl_12" => $value['tgl_12'],
                "tgl_13" => $value['tgl_13'],
                "tgl_14" => $value['tgl_14'],
                "tgl_15" => $value['tgl_15'],
                "tgl_16" => $value['tgl_16'],
                "tgl_17" => $value['tgl_17'],
                "tgl_18" => $value['tgl_18'],
                "tgl_19" => $value['tgl_19'],
                "tgl_20" => $value['tgl_20'],
                "tgl_21" => $value['tgl_21'],
                "tgl_22" => $value['tgl_22'],
                "tgl_23" => $value['tgl_23'],
                "tgl_24" => $value['tgl_24'],
                "tgl_25" => $value['tgl_25'],
                "tgl_26" => $value['tgl_26'],
                "tgl_27" => $value['tgl_27'],
                "tgl_28" => $value['tgl_28'],
                "tgl_29" => $value['tgl_29'],
                "tgl_30" => $value['tgl_30'],
                "tgl_31" => $value['tgl_31']
            ];
        }
        return collect($arrResult);
    }

    /**
     * @OA\Post(
     *   tags={"Vendor Management"},
     *   path="/api/planning-realisasi-fingerprint/download-excel",
     *   summary="download planning schedulle list",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="tahun", in="query", description="parameter tahun", required=true, @OA\Schema( type="string" )),
     *   @OA\Parameter(name="bulan", in="query", description="parameter bulan", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     *   @OA\Response(response=500, description="Internal Server Error"),
     * )
     */
    public function downloadPlanningActual(Request $request)
    {
        $this->validateIndex($request);
        try {
            $tahun = $request->tahun;
            $bulan = $request->bulan;
            $query = $this->buildQueryIndex($request);
            $dataPlanning = $query->get();
            $resultPlanningActual = $this->getTotalDataPresence($dataPlanning, $bulan, $tahun);
            $dataPlanningActual = $resultPlanningActual['data'];
            $dataExportPlanning = $this->mapDownloadDataPlanningActual($dataPlanningActual);
            $fileName = sprintf("planning realisasi-%s_%s_%s.xlsx", $bulan, $tahun, date('YmdHis'));
            $columns = ["No", "No Pegawai", "Nama Pegawai", "Spesifikasi", "Perusahaan", "Tahun", "Bulan", "Planning", "Realisasi"];
            return Excel::download((new DownloadExcel($dataExportPlanning, $columns)), $fileName);
        } catch (\Exception $ex) {
            $response = responseFail(__('messages.show-import-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function mapDownloadDataPlanningActual($dataPlanningActual): Collection
    {
        $arrResult = [];
        foreach ($dataPlanningActual as $key => $value) {
            $number = $key + 1;
            $arrResult[] = [
                "no" => $number,
                "no_pegawai" => (string) $value['no_pegawai'],
                "nm_pegawai" => (string) $value['nm_pegawai'],
                "spesifikasi" => (string) $value['spesifikasi'],
                "perusahaan" => (string) $value['perusahaan'],
                "tahun" => (string) $value['tahun'],
                "bulan" => (string) $value['bulan'],
                "total_planning" => (string) $value['planning'],
                "total_realisasi" => (string) $value['realisasi'],
            ];
        }
        return collect($arrResult);
    }
}

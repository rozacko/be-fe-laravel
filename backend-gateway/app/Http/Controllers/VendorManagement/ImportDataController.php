<?php

namespace App\Http\Controllers\VendorManagement;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\VmPlanningTemplate;
use App\Exports\VmActualTemplate;
use App\Imports\VmImportPlanning;
use App\Imports\VmImportRealisasi;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cache;

class ImportDataController extends Controller
{
    /**
     * @OA\Get(
     *   tags={"Vendor Management"},
     *   path="/api/vendormanagement/template/{file}",
     *   summary="Get template planning",
     *   security={{"token": {}}},
     *   @OA\Parameter(
     *          name="file",
     *          description="file (planning / actual)",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     *   @OA\Response(response=500, description="Internal Server Error"),
     * )
     */
    public function downloadTemplate(Request $request)
    {
        if (!empty($request->file)) {
            switch ($request->file) {
                case 'planning':
                    return Excel::download(new VmPlanningTemplate(), 'VM_PLANNING.xlsx');
                    break;
                case 'actual':
                    return Excel::download(new VmActualTemplate(), 'VM_REALISASI.xlsx');
                    break;
                default:
                    return ["error" => "no action", "data" => []];
                    break;
            }
        } else {
            $this->validate($request, [
                'file' => 'required',
            ]);
        }
    }

    /**
     * @OA\Post(
     *   tags={"Vendor Management"},
     *   path="/api/upload-schedule",
     *   summary="import schedule planning",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *      @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     description="file to upload",
     *                     property="upload_file",
     *                     type="string",
     *                     format="binary",
     *                 )
     *             )
     *         )
     *     )
     * )
     */
    public function importPlanning(Request $request)
    {

        try {
            $this->validate($request, [
                'upload_file' => 'required|mimes:xls,xlsx',
            ]);
            $file = $request->file('upload_file');
            $key = date('YmdHis');
            DB::beginTransaction();
            Excel::import(new VmImportPlanning($key), $file);
            $resultImport = Cache::get($key);
            $responseData = [
                "total_data" => count($resultImport['all']),
                "total_success" => count($resultImport['success']),
                "total_failed" => count($resultImport['failed']),
                "failed_data" => $resultImport['failed'],
            ];
            DB::commit();
            $response = responseSuccess(__('messages.import-success'), $responseData);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollBack();
            $response = responseFail(__('messages.import-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Post(
     *   tags={"Vendor Management"},
     *   path="/api/upload-realisasi-fingerprint",
     *   summary="import schedule realisasi",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *      @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     description="file to upload",
     *                     property="upload_file",
     *                     type="string",
     *                     format="binary",
     *                 )
     *             )
     *         )
     *     )
     * )
     */
    public function importRealisasi(Request $request)
    {
        try {
            $this->validate($request, [
                'upload_file' => 'required|mimes:xls,xlsx',
            ]);
            $file = $request->file('upload_file');
            $key = date('YmdHis');
            DB::beginTransaction();
            Excel::import(new VmImportRealisasi($key), $file);
            $resultImport = Cache::get($key);
            $responseData = [
                "total_data" => count($resultImport['all']),
                "total_success" => count($resultImport['success']),
                "total_failed" => count($resultImport['failed']),
                "failed_data" => $resultImport['failed'],
            ];
            DB::commit();
            $response = responseSuccess(__('messages.import-success'), $responseData);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollBack();
            $response = responseFail(__('messages.import-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}

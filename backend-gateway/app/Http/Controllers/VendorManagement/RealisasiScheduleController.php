<?php

namespace App\Http\Controllers\VendorManagement;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TPresensiPegawai;
use Illuminate\Http\Response;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Collection;
use App\Exports\DownloadExcel;

class RealisasiScheduleController extends Controller
{
    protected $tpresensipegawai;
    public function __construct()
    {
        $this->tpresensipegawai = new TPresensiPegawai();
    }

    /**
     * @OA\Get(
     *   tags={"Vendor Management"},
     *   path="/api/realisasi-fingerprint",
     *   summary="Get realisasi list",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="tahun", in="query", description="parameter tahun", required=true, @OA\Schema( type="string" )),
     *   @OA\Parameter(name="bulan", in="query", description="parameter bulan", required=true, @OA\Schema( type="string" )),
     *   @OA\Parameter(name="keyword", in="query", description="search keyword", required=false, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     *   @OA\Response(response=500, description="Internal Server Error"),
     * )
     */
    public function index(Request $request)
    {
        $this->validateIndex($request);
        try {
            $query = $this->buildQueryIndex($request);

            $model = DataTables::of($query)
                ->addIndexColumn()
                ->make(true)
                ->getData(true);

            $response = responseDatatableSuccess(__('messages.read-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            $response = responseFail(__('messages.show-import-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    protected function buildQueryIndex($request)
    {
        $query = $this->tpresensipegawai->query();
        $query = $query->whereRaw('EXTRACT(YEAR FROM tanggal) = ?', $request->tahun)
            ->whereRaw('EXTRACT(MONTH FROM tanggal) = ?', $request->bulan);
        if ($request->has('keyword') && strlen($request->keyword) > 0) {
            $keyword = $request->keyword;
            $query->where(function ($query) use ($keyword) {
                $keyword = "%" . $keyword . "%";
                $query->where('no_pegawai', 'like', $keyword)
                    ->orWhere('nama_pegawai', 'like', $keyword);
            });
        }
        return $query;
    }

    protected function validateIndex($request)
    {
        $this->validate($request, [
            'tahun' => 'required',
            'bulan' => 'required',
            'keyword' => 'string|nullable'
        ]);
    }

    /**
     * @OA\Delete(
     *   tags={"Vendor Management"},
     *   path="/api/realisasi-fingerprint/{uuid}",
     *   summary="delete realisasi by uuid",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="uuid", in="path", description="uuid that needs to be delete", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     *   @OA\Response(response=500, description="Internal Server Error"),
     * )
     */
    public function destroy($uuid)
    {
        $this->isValidUuid($uuid);
        $model = $this->tpresensipegawai::where('uuid', $uuid)->firstOrFail();

        DB::beginTransaction();
        try {
            $model->delete();
            DB::commit();
            $response = responseSuccess(__('messages.delete-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.delete-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Post(
     *   tags={"Vendor Management"},
     *   path="/api/realisasi-fingerprint/download-excel",
     *   summary="download realisasi list",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="tahun", in="query", description="parameter tahun", required=true, @OA\Schema( type="string" )),
     *   @OA\Parameter(name="bulan", in="query", description="parameter bulan", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     *   @OA\Response(response=500, description="Internal Server Error"),
     * )
     */
    public function downloadRealisasi(Request $request)
    {
        $this->validateIndex($request);
        try {
            $tahun = $request->tahun;
            $bulan = $request->bulan;
            $query = $this->buildQueryIndex($request);
            $dataRealisasi = $query->get()->toArray();
            $dataExportRealisasi = $this->mapDownloadData($dataRealisasi);
            $fileName = sprintf("realisasi-%s_%s_%s.xlsx", $bulan, $tahun, date('YmdHis'));
            $columns = array_merge(["No", "No Pegawai", "Nama Pegawai", "Time", "State", "New State", "Exception", "Preference", "Tanggal", "Event"]);
            return Excel::download((new DownloadExcel($dataExportRealisasi, $columns)), $fileName);
        } catch (\Exception $ex) {
            $response = responseFail(__('messages.show-import-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function mapDownloadData($dataRealisasi): Collection
    {
        $arrResult = [];
        foreach ($dataRealisasi as $key => $value) {
            $number = $key + 1;
            $arrResult[] = [
                "no" => $number,
                "no_pegawai" => (string) $value['no_pegawai'],
                "nama_pegawai" => (string) $value['nama_pegawai'],
                "time" => (string) $value['time'],
                "state" => (string) $value['state'],
                "new_state" => (string) $value['new_state'],
                "exception" => (string) $value['exception'],
                "preference" => (string) $value['preference'],
                "tanggal" => (string) $value['tanggal'],
                "event" => (string) $value['event']
            ];
        }
        return collect($arrResult);
    }

    /**
     * @OA\Get(
     *   tags={"Vendor Management"},
     *   path="/api/realisasi-fingerprint/{uuid}",
     *   summary="get realisasi by uuid",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="uuid", in="path", description="uuid to show", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     *   @OA\Response(response=500, description="Internal Server Error"),
     * )
     */
    public function show($uuid)
    {
        $this->isValidUuid($uuid);
        try {
            $model = $this->tpresensipegawai::where('uuid', $uuid)->first();
            $result[] = $model->toArray();
            $response = responseSuccess(__('messages.read-success'), $result);
            return response()->json($response);
        } catch (\Exception $ex) {
            $response = responseFail(__('messages.show-import-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}

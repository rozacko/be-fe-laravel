<?php

namespace App\Http\Controllers\Qc;

use App\Exports\DownloadExcel;
use App\Http\Controllers\Controller;
use App\Imports\ExcelImportsWithHeader;
use App\Models\TQcSemen;
use App\Rules\DuplicateDataExceptUuid;
use App\Rules\DuplicateDataImportRules;
use App\Rules\UniqueDataCollectionRules;
use App\Traits\ValidationExcelImport;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class QcSemenController extends Controller
{
    //
    use ValidationExcelImport;

    public function __construct()
    {
        $this->hours = '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24';
        $this->quality = 'fm-1,fm-2,fm-3,fm-4,fm-5,fm-6,fm-7,fm-8,fm-9';
        $this->type = 'sprint-pro,ultra-pro,powers-pro,ez-pro,fb';
    }

    /**
     * @OA\Get(
     *   tags={"Data QC Quality Cemment (Tab Semen) Get List Table "},
     *   path="/api/qc/quality-cement",
     *   summary="Data QC Quality Cemment (Tab Semen) Get List Table",
     *   security={{"token": {}}},
     *      @OA\Parameter(
     *         name="tanggal",
     *         in="query",
     *         description="format YYYY-MM-DD ex: (2023-12-31)",
     *         @OA\Schema( format="date",type="string", description="format YYYY-MM-DD ex: (2023-12-31)" )
     *      ),
     *      @OA\Parameter(
     *         name="plant_id",
     *         in="query",
     *         description="id plant, bukan  kodeplant",
     *         @OA\Schema( format="integer",type="string", description="id plant, bukan  kodeplant" )
     *      ),
     *      @OA\Parameter(
     *         name="type",
     *         in="query",
     *         description="in list : sprint-pro,ultra-pro,powers-pro,ez-pro,fb", example="fb",
     *         @OA\Schema( format="text",type="string", description="in list : sprint-pro,ultra-pro,powers-pro,ez-pro,fb", example="fb"),
     *      ),
     *      @OA\Parameter(
     *         name="quality",
     *         in="query",
     *         description="in list : fm-1,fm-2,fm-3,fm-4,fm-5,fm-6,fm-7,fm-8,fm-9", example="fm-9",
     *         @OA\Schema( format="text",type="string", description="in list : fm-1,fm-2,fm-3,fm-4,fm-5,fm-6,fm-7,fm-8,fm-9", example="fm-9"),
     *      ),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */
    public function index(Request $request, $exported=false)
    {
        # code...
        $query = TQcSemen::leftJoin('m_plant', 't_qc_semen.plant_id', '=', 'm_plant.id')
            ->select('t_qc_semen.*', 'm_plant.nm_plant', 'm_plant.kd_plant');

        $rules = [];
        if($request->filled('tanggal')){
            $rules['tanggal'] = 'required|date';
            $query = $query->where('t_qc_semen.tanggal', $request->get('tanggal'));
        }
        if($request->filled('plant_id')){
            $rules['plant_id'] = 'required|exists:m_plant,id';
            $query = $query->where('t_qc_semen.plant_id', $request->get('plant_id'));
        }

        if($request->filled('type')){
            $rules['type'] =  "required|in:" . $this->type;
            $query = $query->where('t_qc_semen.type', $request->get('type'));
        }

        if($request->filled('quality')){
            $rules['quality'] ="required|in:" . $this->quality;
            $query = $query->where('t_qc_semen.quality', $request->get('quality'));
        }

        if(count($rules)!=0){
            $this->validate($request, $rules);
        }

        if($exported){
            $query->select('m_plant.nm_plant','m_plant.kd_plant',"t_qc_semen.quality","t_qc_semen.type","t_qc_semen.tanggal","t_qc_semen.jam","t_qc_semen.so3","t_qc_semen.fcao","t_qc_semen.blaine","t_qc_semen.res45","t_qc_semen.loi");
        }

        $model = DataTables::of($query);
        if($exported)
        {
            return $model->getFilteredQuery()->get();
        }
        $model = $model->make(true)->getData(true);

        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    /**
     * @OA\Get(
     *   tags={"Data QC Quality Cemment (Tab Semen) Show "},
     *   path="/api/qc/quality-cement/{uuid}",
     *   summary="Data QC Quality Cemment (Tab Semen) Show",
     *   security={{"token": {}}},
     *      @OA\Parameter(
     *         name="uuid",
     *         in="path",
     *         description="uuid",
     *         required=true,
     *         @OA\Schema( format="uuid",type="string" )
     *      ),
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */
    public function show($id)
    {
        $this->isValidUuid($id);
        $request = new Request(['uuid' => $id]);
        $this->validate($request, [
            'uuid' => 'required|exists:t_qc_semen',
        ]);
        $model = TQcSemen::leftJoin('m_plant', 't_qc_semen.plant_id', '=', 'm_plant.id')
            ->select('t_qc_semen.*', 'm_plant.nm_plant', 'm_plant.kd_plant')
            ->where('uuid', $id)->first();
        $response = responseSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    /**
     * @OA\Put(
     *   tags={"Data QC Quality Cemment (Tab Semen) Update "},
     *   path="/api/qc/quality-cement/{uuid}",
     *   summary="Data QC Quality Cemment (Tab Semen) Update",
     *   security={{"token": {}}},
     *      @OA\Parameter(
     *         name="uuid",
     *         in="path",
     *         description="uuid",
     *         required=true,
     *         @OA\Schema( format="uuid",type="string" )
     *      ),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"plant_id", "tanggal", "type", "quality", "jam", "so3", "fcao", "blaine", "res45", "loi"},
     *       @OA\Property(property="tanggal",type="date", description="YYYY-MM-DD", example="2023-12-31"),
     *       @OA\Property(property="plant_id",type="integer", description="id dari plant bukan kode plant", example="1"),
     *       @OA\Property(property="type",type="string", description="iin list : sprint-pro,ultra-pro,powers-pro,ez-pro,fb", example="fb"),
     *       @OA\Property(property="quality",type="string", description="iin list : fm-1,fm-2,fm-3,fm-4,fm-5,fm-6,fm-7,fm-8,fm-9", example="fm-9"),
     *       @OA\Property(property="jam", type="string", format="text",example="1", description="angka 1-24"),
     *       @OA\Property(property="so3", type="string", format="text",example="so3"),
     *       @OA\Property(property="fcao", type="string", format="text",example="fcao"),
     *       @OA\Property(property="blaine", type="string", format="text",example="blaine"),
     *       @OA\Property(property="h2o", type="string", format="text",example="h2o"),
     *       @OA\Property(property="res45", type="string", format="text",example="res45"),
     *       @OA\Property(property="loi", type="string", format="text",example="loi"),
     *     )
     *   ),
     *   @OA\Response(response=200, description="Successfully updated"),
     *   @OA\Response(response=400, description="Bad request"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */
    public function update($uuid, Request $request)
    {
        $this->isValidUuid($uuid);
        $request = new Request($request->only(["plant_id", "tanggal", "type", "quality", "jam", "so3", "fcao", "blaine", "res45", "loi"]));
        $rules = [
            'plant_id' => 'required|exists:m_plant,id',
            'tanggal' => 'required|date',
            "type" => "required|in:" . $this->type,
            "quality" => "required|in:" . $this->quality,
            "jam" => ['required', 'in:' . $this->hours],
            "so3" => 'required',
            "fcao" => 'required',
            "blaine" => 'required',
            "res45" => 'required',
            "loi" => 'required',
            't_qc_semen' => ["required", new DuplicateDataExceptUuid]
        ];
        $request = $request->merge([
            'uuid' => $uuid,
            "t_qc_semen" => [
                'uuid' => $uuid,
                'where' => [
                    'tanggal' => $request->get('tanggal'),
                    'quality' => $request->get('quality'),
                    'jam' => $request->get('jam'),
                    'type' => $request->get('type'),
                    'plant_id' => $request->get('plant_id'),
                ]
            ]
        ]);
        $this->validate($request, $rules);
        $data = TQcSemen::where('uuid', $uuid)->first();
        try {
            $data->update([
                'plant_id' => $request->get("plant_id"),
                'tanggal' => $request->get("tanggal"),
                "type" => $request->get("type"),
                "quality" => $request->get("quality"),
                "jam" => $request->get("jam"),
                "so3" => $request->get("so3"),
                "fcao" => $request->get("fcao"),
                "blaine" => $request->get("blaine"),
                "res45" => $request->get("res45"),
                "loi" => $request->get("loi"),
                'updated_by' => Auth::user()->uuid,
            ]);
            DB::commit();
            $response = responseSuccess(__('messages.update-success'), []);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.update-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Delete(
     *   tags={"Data QC Quality Cemment (Tab Semen) Delete "},
     *   path="/api/qc/quality-cement/{uuid}",
     *   summary="Data QC Quality Cemment (Tab Semen) Delete",
     *   security={{"token": {}}},
     *      @OA\Parameter(
     *         name="uuid",
     *         in="path",
     *         description="uuid",
     *         required=true,
     *         @OA\Schema( format="uuid",type="string" )
     *      ),
     *   @OA\Response(response=200, description="Successfully deleted"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function destroy($uuid)
    {
        $this->isValidUuid($uuid);
        $request = new Request(['uuid' => $uuid]);
        $this->validate($request, [
            'uuid' => 'required|exists:t_qc_semen',
        ]);
        $model = TQcSemen::where('uuid', $uuid)->first();
        DB::beginTransaction();
        try {
            $model->delete();
            DB::commit();
            $response = responseSuccess(__('messages.delete-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.delete-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Post(
     *   tags={"Data QC Qaulity Cemment (Tab Semen) Import File "},
     *   path="/api/qc/quality-cement-import",
     *   summary="Data QC Qaulity Cemment (Tab Semen) Import File",
     *   security={{"token": {}}},
     *    @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(property="tanggal",type="date", description="YYYY-MM-DD"),
     *                 @OA\Property(property="plant_id",type="integer", description="id dari plant bukan kode plant"),
     *                 @OA\Property(property="type",type="string", description="iin list : sprint-pro,ultra-pro,powers-pro,ez-pro,fb"),
     *                 @OA\Property(property="quality",type="string", description="iin list : fm-1,fm-2,fm-3,fm-4,fm-5,fm-6,fm-7,fm-8,fm-9"),
     *                 @OA\Property(property="upload_file",type="string", format="binary" , description="mimes:xls,xlsx"),
     *             )
     *         ),
     *         description="
     *         Pada File Excel Format Jam Memakai angka 1-24
     *         quality in list : 'fm-1,fm-2,fm-3,fm-4,fm-5,fm-6,fm-7,fm-8,fm-9'
     *         type in list : 'sprint-pro,ultra-pro,powers-pro,ez-pro,fb'",
     *     ),
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest")
     * )
     */
    public function import(Request $request)
    {
        # code...
        $this->validate($request, [
            'upload_file' => 'required|mimes:xls,xlsx',
            'plant_id' => 'required|exists:m_plant,id',
            'tanggal' => 'required|date',
            "type" => "required|in:" . $this->type,
            "quality" => "required|in:" . $this->quality
        ]);

        $file = $request->file('upload_file');
        $data = Excel::toArray(new ExcelImportsWithHeader, $file)[0];

        if (count(@$data) == 0  || !$this->compareTemplate("qc_semen", array_keys(@$data[0]))) {
            return response()->json(responseFail(__('messages.validation-template-fail'), ["template" => [__('messages.validation-template-fail')]]), 400)->throwResponse();
        }

        $dataExcel = collect($data);

        $lineValidation = [
            "jam" => ['required', 'in:' . $this->hours],
            "so3" => 'required',
            "fcao" => 'required',
            "blaine" => 'required',
            "res45" => 'required',
            "loi" => 'required',
            't_qc_semen' => ["required", new DuplicateDataImportRules]
        ];
        $findMultipleJam = $dataExcel->countBy('jam')->filter(function ($item) {
            return $item > 1;
        })->first();

        $this->validateImport(['jam_unique' => $findMultipleJam], ['jam_unique' => [new UniqueDataCollectionRules]], 1);

        $tanggal = $request->get('tanggal');
        $plant_id = $request->get('plant_id');
        $type = $request->get('type');
        $quality = $request->get('quality');
        foreach ($dataExcel as $key => $item) {
            $item['t_qc_semen'] = [
                'jam' => $item['jam'],
                'tanggal' => $tanggal,
                'line' => ($key + 2)
            ];
            $this->validateImport($item, $lineValidation, ($key + 2));
        }
        $insert = $dataExcel->map(function ($item) use ($tanggal, $plant_id, $type, $quality) {
            $item['quality'] = $quality;
            $item['type'] = $type;
            $item['tanggal'] =  $tanggal;
            $item['created_by'] = Auth::user()->uuid;
            $item['created_at'] = now();
            $item['plant_id'] = $plant_id;
            return $item;
        });

        DB::beginTransaction();
        try {
            TQcSemen::insert($insert->toArray());
            DB::commit();
            $response = responseSuccess(__('messages.create-success'), []);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *   tags={"Data QC Qaulity Cemment (Tab Semen) Download Excel "},
     *   path="/api/qc/quality-cement-download",
     *   summary="Data QC Qaulity Cemment (Tab Semen) Download Excel",
     *   security={{"token": {}}},
     *      @OA\Parameter(
     *         name="tanggal",
     *         in="query",
     *         description="format YYYY-MM-DD ex: (2023-12-31)" ,
     *         @OA\Schema( format="date",type="string", description="format YYYY-MM-DD ex: (2023-12-31)" )
     *      ),
     *      @OA\Parameter(
     *         name="plant_id",
     *         in="query",
     *         description="id plant, bukan  kodeplant" ,
     *         @OA\Schema( format="integer",type="string", description="id plant, bukan  kodeplant" )
     *      ),
     *      @OA\Parameter(
     *         name="type",
     *         in="query",
     *         description="in list : sprint-pro,ultra-pro,powers-pro,ez-pro,fb", example="fb",
     *         @OA\Schema( format="text",type="string", description="in list : sprint-pro,ultra-pro,powers-pro,ez-pro,fb", example="fb"),
     *      ),
     *      @OA\Parameter(
     *         name="quality",
     *         in="query",
     *         description="in list : fm-1,fm-2,fm-3,fm-4,fm-5,fm-6,fm-7,fm-8,fm-9", example="fm-9",
     *         @OA\Schema( format="text",type="string", description="in list : fm-1,fm-2,fm-3,fm-4,fm-5,fm-6,fm-7,fm-8,fm-9", example="fm-9"),
     *      ),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */
    public function download(Request $request)
    {
        $request = $request->merge(["length" => -1]);
        $data    = $this->index($request, true);
        $columns = ['Nama Plant','Kode Plant',"Quality","Type","Tanggal","Jam","SO3","FCAO","BLAINE","RES45","LOI"];
        return Excel::download((new DownloadExcel($data,$columns)), "QM- Quality Cement.xlsx");
    }

    private function validateImport($data, array $rules, $line)
    {
        $messages = [
            'required' => __('validation.required'),
            'numeric'  => __('validation.numeric'),
            'exists'   => __('validation.exists'),
        ];

        $validator = Validator::make($data, $rules, $messages);

        if ($validator->fails()) {
            $response = responseFail(__('messages.validation-fail') . " " . __('messages.inline-fail') . " : " . $line, $validator->errors(), []);
            $return = response()->json($response, Response::HTTP_BAD_REQUEST, [], JSON_PRETTY_PRINT);
            $return->throwResponse();
        }
    }
}

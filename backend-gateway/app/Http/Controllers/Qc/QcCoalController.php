<?php

namespace App\Http\Controllers\Qc;

use App\Exports\DownloadExcel;
use App\Http\Controllers\Controller;
use App\Imports\ExcelImportsWithHeader;
use App\Models\TQcCoal;
use App\Rules\DuplicateDataExceptUuid;
use App\Rules\DuplicateDataImportRules;
use App\Rules\UniqueDataCollectionRules;
use App\Traits\ValidationExcelImport;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Yajra\DataTables\Facades\DataTables;

class QcCoalController extends Controller
{
    //
    use ValidationExcelImport;

    public function __construct()
    {
        $this->quality = 'cm-1,cm-2,cm-3,cm-4,atox';
    }

    /**
     * @OA\Get(
     *   tags={"Data QC Coal Get List Table "},
     *   path="/api/qc/coal",
     *   summary="Data QC Coal Get List Table",
     *   security={{"token": {}}},
     *      @OA\Parameter(
     *         name="tanggal",
     *         in="query",
     *         description="format YYYY-MM-DD ex: (2023-12-31)",
     *         @OA\Schema( format="date",type="string", description="format YYYY-MM-DD ex: (2023-12-31)" )
     *      ),
     *      @OA\Parameter(
     *         name="quality",
     *         in="query",
     *         description="in list : cm-1,cm-2,cm-3,cm-4,atox",
     *         @OA\Schema( format="text",type="string", description="in list : cm-1,cm-2,cm-3,cm-4,atox" )
     *      ),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */
    public function index(Request $request , $exported = false)
    {
        # code...
        $query= TQcCoal::query();

        $rules = [];
        if($request->filled('tanggal')){
            $rules['tanggal'] = 'required|date';
            $query = $query->where('t_qc_coal.tanggal', $request->get('tanggal'));
        }
        if($request->filled('quality')){
            $rules['quality'] = 'required|in:'.$this->quality;
            $query = $query->where('t_qc_coal.quality', $request->get('quality'));
        }

        if($exported){
            $query->select("quality","tanggal","ghv","ac","tm");
        }

        if(count($rules)!=0){
            $this->validate($request, $rules);
        }

        $model = DataTables::of($query);
        if($exported)
        {
            return $model->getFilteredQuery()->get();
        }
        $model = $model->make(true)->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    /**
     * @OA\Get(
     *   tags={"Data QC Coal Show "},
     *   path="/api/qc/coal/{uuid}",
     *   summary="Data QC Coal Show",
     *   security={{"token": {}}},
     *      @OA\Parameter(
     *         name="uuid",
     *         in="path",
     *         description="uuid",
     *         required=true,
     *         @OA\Schema( format="uuid",type="string" )
     *      ),
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */
    public function show($id)
    {
        $this->isValidUuid($id);
        $request = new Request(['uuid'=> $id]);
        $this->validate($request, [
            'uuid' => 'required|exists:t_qc_coal',
        ]);
        $model = TQcCoal::where('uuid', $id)->firstorfail();
        $response = responseSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    /**
     * @OA\Put(
     *   tags={"Data QC Coal Update "},
     *   path="/api/qc/coal/{uuid}",
     *   summary="Data QC Coal Update",
     *   security={{"token": {}}},
     *      @OA\Parameter(
     *         name="uuid",
     *         in="path",
     *         description="uuid",
     *         required=true,
     *         @OA\Schema( format="uuid",type="string" )
     *      ),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"quality","tanggal","ghv","ac","tm"},
     *       @OA\Property(property="quality", type="string", format="text", example="cm-1", description="cm-1,cm-2,cm-3,cm-4,atox"),
     *       @OA\Property(property="tanggal", type="date", format="text", example="2023-12-31"),
     *       @OA\Property(property="ghv", type="string", format="text", example="ghv"),
     *       @OA\Property(property="ac", type="string", format="text", example="ac"),
     *       @OA\Property(property="tm", type="string", format="text", example="tm"),
     *     )
     *   ),
     *   @OA\Response(response=200, description="Successfully updated"),
     *   @OA\Response(response=400, description="Bad request"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */
    public function update($uuid, Request $request)
    {
        $this->isValidUuid($uuid);
        $request = new Request($request->only(["quality","tanggal","ghv","ac","tm"]));
        $rules = [
            'quality' => 'required|in:'.$this->quality,
            'tanggal' => 'required|date',
            'uuid' => 'required|exists:t_qc_coal',
            'ghv' => 'required',
            'ac' => 'required',
            'tm' => 'required',
            't_qc_coal' => ["required", new DuplicateDataExceptUuid]
        ];
        $request = $request->merge([
            'uuid'=> $uuid,
            "t_qc_coal"=>[
                'uuid' => $uuid,
                'where' => [
                    'tanggal' => $request->get('tanggal'),
                    'quality' => $request->get('quality'),
                ]
            ]
        ]);
        $this->validate($request, $rules);
        $data = TQcCoal::where('uuid', $uuid)->firstorfail();
        DB::beginTransaction();
        try {
            $data->update([
                'tanggal' => $request->get('tanggal'), 
                "quality" => $request->get('quality'), 
                'ghv' => $request->get('ghv'), 
                'ac' => $request->get('ac'), 
                'tm' => $request->get('tm'), 
                'updated_by'=> Auth::user()->uuid,
            ]);
            DB::commit();
            $response = responseSuccess(__('messages.update-success'), []);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.update-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Delete(
      *   tags={"Data QC Coal Delete "},
     *   path="/api/qc/coal/{uuid}",
     *   summary="Data QC Coal Delete",
     *   security={{"token": {}}},
     *      @OA\Parameter(
     *         name="uuid",
     *         in="path",
     *         description="uuid",
     *         required=true,
     *         @OA\Schema( format="uuid",type="string" )
     *      ),
     *   @OA\Response(response=200, description="Successfully deleted"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function destroy($uuid)
    {        
        $this->isValidUuid($uuid);
        $request = new Request(['uuid'=> $uuid]);
        $this->validate($request, [
            'uuid' => 'required|exists:t_qc_coal',
        ]);
        $model = TQcCoal::where('uuid', $uuid)->firstorfail();
        DB::beginTransaction();
        try {
            $model->delete();
            DB::commit();
            $response = responseSuccess(__('messages.delete-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.delete-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Post(
     *   tags={"Data QC Coal Import File "},
     *   path="/api/qc/coal-import",
     *   summary="Data QC Coal Import File",
     *   security={{"token": {}}},
     *    @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(property="quality",type="string", description="in list : cm-1,cm-2,cm-3,cm-4,atox"),
     *                 @OA\Property(property="upload_file",type="string", format="binary" , description="mimes:xls,xlsx"),
     *             )
     *         ),
     *         description="Fiels Quality Menggunakan Format : cm-1,cm-2,cm-3,cm-4,atox ",
     *     ),
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest")
     * )
     */
    public function import(Request $request)
    {
        # code...
        $this->validate($request, [
            'upload_file' => 'required|mimes:xls,xlsx',
            'quality' => 'required|in:'.$this->quality,
        ]);

        $file = $request->file('upload_file');
        $data = Excel::toArray(new ExcelImportsWithHeader, $file)[0];

        if (count(@$data)==0  || !$this->compareTemplate("qc_coal", array_keys(@$data[0]))) {
            return response()->json(responseFail(__('messages.validation-template-fail'),["template" => [__('messages.validation-template-fail')]]), 400)->throwResponse();
        }

        $dataExcel = collect($data);
        $lineValidation = [
           "tanggal"   => 'required|date|date_format:Y-m-d',
           "ghv"=>'required',
           "ac"=>'required',
           "tm"=>'required',
           't_qc_coal' => ["required", new DuplicateDataImportRules]
        ];
        
        $findMultipleJam = $dataExcel->countBy('tanggal')->filter(function ($item) { 
            return $item > 1;
        })->first();


        $this->validateImport(['tanggal_unique'=> $findMultipleJam], ['tanggal_unique'=> [new UniqueDataCollectionRules]], 1);

        foreach ($dataExcel as $key => $item) {
            $tanggal = $item['tanggal']!=null ? \Carbon\Carbon::parse(Date::excelToDateTimeObject($item['tanggal']))->format('Y-m-d'): null;
            $item['tanggal'] = $tanggal;
            $item['t_qc_coal'] = [
                'tanggal' => $tanggal,
                'line' => ($key + 2)
            ];
            $this->validateImport($item, $lineValidation, ($key + 2));
        }
        $quality = $request->get('quality');
        $insert = $dataExcel->map(function($item) use ($quality){
            $item['quality'] = $quality;
            $item['tanggal'] = \Carbon\Carbon::parse(Date::excelToDateTimeObject($item['tanggal']))->format('Y-m-d');
            $item['created_by'] = Auth::user()->uuid;
            $item['created_at'] = now();
            return $item;
        });
        
        DB::beginTransaction();
        try {
            TQcCoal::insert($insert->toArray());
            DB::commit();
            $response = responseSuccess(__('messages.create-success'), []);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        } 
    }


    /**
     * @OA\Get(
     *   tags={"Data QC Coal Download Excel "},
     *   path="/api/qc/coal-download",
     *   summary="Data QC Coal Download Excel",
     *   security={{"token": {}}},
     *      @OA\Parameter(
     *         name="tanggal",
     *         in="query",
     *         description="format YYYY-MM-DD ex: (2023-12-31)",
     *         @OA\Schema( format="date",type="string", description="format YYYY-MM-DD ex: (2023-12-31)" )
     *      ),
    *      @OA\Parameter(
     *         name="quality",
     *         in="query",
     *         description="in list : cm-1,cm-2,cm-3,cm-4,atox",
     *         @OA\Schema( format="text",type="string", description="in list : cm-1,cm-2,cm-3,cm-4,atox" )
     *      ),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */
    public function download(Request $request)
    {
        $request = $request->merge(["length" => -1]);
        $data    = $this->index($request, true);
        $columns = ["Quality","Tanggal","GHV","AC","TM"];
        return Excel::download((new DownloadExcel($data,$columns)), "QM-Coal.xlsx");
    }

    private function validateImport($data, array $rules, $line)
    {
        $messages = [
            'required' => __('validation.required'),
            'numeric'  => __('validation.numeric'),
            'exists'   => __('validation.exists'),
        ];

        $validator = Validator::make($data, $rules, $messages);

        if ($validator->fails()) {
            $response = responseFail(__('messages.validation-fail') . " " . __('messages.inline-fail') . " : " . $line, $validator->errors(), []);
            $return = response()->json($response, Response::HTTP_BAD_REQUEST, [], JSON_PRETTY_PRINT);
            $return->throwResponse();
        }
    }
}

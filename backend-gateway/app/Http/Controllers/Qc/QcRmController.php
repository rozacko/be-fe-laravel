<?php

namespace App\Http\Controllers\Qc;

use App\Exports\DownloadExcel;
use App\Http\Controllers\Controller;
use App\Imports\ExcelImportsWithHeader;
use App\Models\TQcRm;
use App\Rules\DuplicateDataExceptUuid;
use App\Rules\DuplicateDataImportRules;
use App\Rules\UniqueDataCollectionRules;
use App\Traits\ValidationExcelImport;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class QcRmController extends Controller
{
    //
    use ValidationExcelImport;

    public function __construct()
    {
        $this->hours = '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24';
    }

    /**
     * @OA\Get(
     *   tags={"Data QC Raw Meal Get List Table "},
     *   path="/api/qc/raw-meal",
     *   summary="Data QC Raw Meal Get List Table",
     *   security={{"token": {}}},
     *      @OA\Parameter(
     *         name="tanggal",
     *         in="query",
     *         description="tanggal",
     *         @OA\Schema( format="date",type="string", description="format YYYY-MM-DD ex: (2023-12-31)" )
     *      ),
     *      @OA\Parameter(
     *         name="plant_id",
     *         in="query",
     *         description="plant_id",
     *         @OA\Schema( format="integer",type="string", description="id plant, bukan  kodeplant" )
     *      ),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */
    public function index(Request $request, $exported=false)
    {
        # code...
        $query= TQcRm::leftJoin('m_plant', 't_qc_rm.plant_id', '=', 'm_plant.id')
        ->select('t_qc_rm.*','m_plant.nm_plant','m_plant.kd_plant');

        $rules = [];
        if($request->filled('tanggal')){
            $rules['tanggal'] = 'required|date';
            $query = $query->where('t_qc_rm.tanggal', $request->get('tanggal'));
        }
        if($request->filled('plant_id')){
            $rules['plant_id'] = 'required|exists:m_plant,id';
            $query = $query->where('t_qc_rm.plant_id', $request->get('plant_id'));
        }

        if(count($rules)!=0){
            $this->validate($request, $rules);
        }
        if($exported){
            $query->select('m_plant.nm_plant','m_plant.kd_plant',"t_qc_rm.tanggal","t_qc_rm.jam","t_qc_rm.lsf","t_qc_rm.sim","t_qc_rm.alm","t_qc_rm.h2o","t_qc_rm.res90","t_qc_rm.res200");
        }

        $model = DataTables::of($query);
        if($exported)
        {
            return $model->getFilteredQuery()->get();
        }
        $model = $model->make(true)->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    /**
     * @OA\Get(
     *   tags={"Data QC Raw meal Show "},
     *   path="/api/qc/raw-meal/{uuid}",
     *   summary="Data QC Raw meal Show",
     *   security={{"token": {}}},
     *      @OA\Parameter(
     *         name="uuid",
     *         in="path",
     *         description="uuid",
     *         required=true,
     *         @OA\Schema( format="uuid",type="string" )
     *      ),
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */
    public function show($id)
    {
        $this->isValidUuid($id);
        $request = new Request(['uuid'=> $id]);
        $this->validate($request, [
            'uuid' => 'required|exists:t_qc_rm',
        ]);
        $model = TQcRm::leftJoin('m_plant', 't_qc_rm.plant_id', '=', 'm_plant.id')
        ->select('t_qc_rm.*','m_plant.nm_plant','m_plant.kd_plant')
        ->where('uuid', $id)->first();
        $response = responseSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    /**
     * @OA\Put(
     *   tags={"Data QC Raw meal Update "},
     *   path="/api/qc/raw-meal/{uuid}",
     *   summary="Data QC Raw meal Update",
     *   security={{"token": {}}},
     *      @OA\Parameter(
     *         name="uuid",
     *         in="path",
     *         description="uuid",
     *         required=true,
     *         @OA\Schema( format="uuid",type="string" )
     *      ),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"plant_id","tanggal","jam","lsf","sim","alm","h2o","res90","res200"},
     *       @OA\Property(property="plant_id", type="string", format="text", example="1", description="id plant bukan kode plant"),
     *       @OA\Property(property="tanggal", type="string", format="text", example="2023-12-31"),
     *       @OA\Property(property="jam", type="string", format="text", example="1"),
     *       @OA\Property(property="lsf", type="string", format="text", example="lsf"),
     *       @OA\Property(property="sim", type="string", format="text", example="sim"),
     *       @OA\Property(property="alm", type="string", format="text", example="alm"),
     *       @OA\Property(property="h2o", type="string", format="text", example="h2o"),
     *       @OA\Property(property="res90", type="string", format="text", example="res90"),
     *       @OA\Property(property="res200", type="string", format="text", example="res200"),
     *     )
     *   ),
     *   @OA\Response(response=200, description="Successfully updated"),
     *   @OA\Response(response=400, description="Bad request"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */
    public function update($uuid, Request $request)
    {
        $this->isValidUuid($uuid);
        $request = new Request($request->only(['plant_id','tanggal',"jam",'lsf','sim','alm','h2o','res90','res200']));
        $rules = [
            'plant_id' => 'required|exists:m_plant,id',
            'tanggal' => 'required|date',
            "jam" => ['required','in:'.$this->hours],
            'lsf' => 'required',
            'sim' => 'required',
            'alm' => 'required',
            'h2o' => 'required',
            'res90' => 'required',
            'res200' => 'required',
            't_qc_rm' => ["required", new DuplicateDataExceptUuid]
        ];
        $request = $request->merge([
            'uuid'=> $uuid,
            "t_qc_rm"=>[
                'uuid' => $uuid,
                'where' => [
                    'tanggal' => $request->get('tanggal'),
                    'plant_id' => $request->get('plant_id'),
                    'jam' => $request->get('jam'),
                ]
            ]
        ]);
        $this->validate($request, $rules);
        $data = TQcRm::where('uuid', $uuid)->first();
        try {
            $data->update([
                'plant_id' =>$request->get("plant_id"),
                'tanggal' =>$request->get("tanggal"),
                "jam" =>$request->get("jam"),
                'lsf' =>$request->get("lsf"),
                'sim' =>$request->get("sim"),
                'alm' =>$request->get("alm"),
                'h2o' =>$request->get("h2o"),
                'res90' =>$request->get("res90"),
                'res200' =>$request->get("res200"),
                'updated_by'=> Auth::user()->uuid,
            ]);
            DB::commit();
            $response = responseSuccess(__('messages.update-success'), []);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.update-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Delete(
      *   tags={"Data QC Raw meal Delete "},
     *   path="/api/qc/raw-meal/{uuid}",
     *   summary="Data QC Raw meal Delete",
     *   security={{"token": {}}},
     *      @OA\Parameter(
     *         name="uuid",
     *         in="path",
     *         description="uuid",
     *         required=true,
     *         @OA\Schema( format="uuid",type="string" )
     *      ),
     *   @OA\Response(response=200, description="Successfully deleted"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function destroy($uuid)
    {        
        $this->isValidUuid($uuid);
        $request = new Request(['uuid'=> $uuid]);
        $this->validate($request, [
            'uuid' => 'required|exists:t_qc_rm',
        ]);
        $model = TQcRm::where('uuid', $uuid)->first();
        DB::beginTransaction();
        try {
            $model->delete();
            DB::commit();
            $response = responseSuccess(__('messages.delete-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.delete-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Post(
     *   tags={"Data QC Raw Meal Import File "},
     *   path="/api/qc/raw-meal-import",
     *   summary="Data QC Raw Meal Import File",
     *   security={{"token": {}}},
     *    @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(property="tanggal",type="date", description="YYYY-MM-DD"),
     *                 @OA\Property(property="plant_id",type="integer", description="id dari plant bukan kode plant"),
     *                 @OA\Property(property="upload_file",type="string", format="binary" , description="mimes:xls,xlsx"),
     *             )
     *         ),
     *         description="Pada File Excel Format Jam Memakai angka 1-24",
     *     ),
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest")
     * )
     */
    public function import(Request $request)
    {
        # code...
        $this->validate($request, [
            'upload_file' => 'required|mimes:xls,xlsx',
            'plant_id' => 'required|exists:m_plant,id',
            'tanggal' => 'required|date'
        ]);

        $file = $request->file('upload_file');
        $data = Excel::toArray(new ExcelImportsWithHeader, $file)[0];

        if (!$this->compareTemplate("qc_rm", array_keys(@$data[0]))) {
            return response()->json(responseFail(__('messages.validation-template-fail'),["template" => [__('messages.validation-template-fail')]]), 400)->throwResponse();
        }

        $dataExcel = collect($data);
        $lineValidation = [
           "jam" => ['required','in:'.$this->hours],
           'lsf' => 'required',
           'sim' => 'required',
           'alm' => 'required',
           'h2o' => 'required',
           'res90' => 'required',
           'res200' => 'required',
           't_qc_rm' => ["required", new DuplicateDataImportRules]
        ];
        $findMultipleJam = $dataExcel->countBy('jam')->filter(function ($item) { 
            return $item > 1;
        })->first();
        $this->validateImport(['jam_unique'=> $findMultipleJam], ['jam_unique'=> [new UniqueDataCollectionRules]], 1);

        $tanggal = $request->get('tanggal');
        $plant_id = $request->get('plant_id');
        foreach ($dataExcel as $key => $item) {
            $item['t_qc_rm'] = [
                'jam' => $item['jam'],
                'tanggal' => $tanggal,
                'line' => ($key + 2)
            ];
            $this->validateImport($item, $lineValidation, ($key + 2));
        }
        $insert = $dataExcel->map(function($item) use ($tanggal, $plant_id) {
            $item['tanggal'] =  $tanggal;
            $item['created_by'] = Auth::user()->uuid;
            $item['created_at'] = now();
            $item['plant_id'] = $plant_id;
            return $item;
        });

        DB::beginTransaction();
        try {
            TQcRm::insert($insert->toArray());
            DB::commit();
            $response = responseSuccess(__('messages.create-success'), []);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        } 
    }

    /**
     * @OA\Get(
     *   tags={"Data QC Raw Meal Download Excel "},
     *   path="/api/qc/raw-meal-download",
     *   summary="Data QC Raw Meal Download Excel",
     *   security={{"token": {}}},
     *      @OA\Parameter(
     *         name="tanggal",
     *         in="query",
     *         description="format YYYY-MM-DD ex: (2023-12-31)",
     *         @OA\Schema( format="date",type="string", description="format YYYY-MM-DD ex: (2023-12-31)" )
     *      ),
     *      @OA\Parameter(
     *         name="plant_id",
     *         in="query",
     *         description="id plant, bukan  kodeplant",
     *         @OA\Schema( format="integer",type="string", description="id plant, bukan  kodeplant" )
     *      ),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */
    public function download(Request $request)
    {
        $request = $request->merge(["length" => -1]);
        $data    = $this->index($request, true);
        $columns = ['Nama Plant','Kode Plant',"Tanggal","Jam","LSF","SIM","ALM","H2O","RES90","RES200"];
        return Excel::download((new DownloadExcel($data,$columns)), "QM-Raw Meal.xlsx");
    }

    private function validateImport($data, array $rules, $line)
    {
        $messages = [
            'required' => __('validation.required'),
            'numeric'  => __('validation.numeric'),
            'exists'   => __('validation.exists'),
        ];

        $validator = Validator::make($data, $rules, $messages);

        if ($validator->fails()) {
            $response = responseFail(__('messages.validation-fail') . " " . __('messages.inline-fail') . " : " . $line, $validator->errors(), []);
            $return = response()->json($response, Response::HTTP_BAD_REQUEST, [], JSON_PRETTY_PRINT);
            $return->throwResponse();
        }
    }
}

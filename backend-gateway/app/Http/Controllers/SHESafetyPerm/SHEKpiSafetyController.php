<?php

namespace App\Http\Controllers\SHESafetyPerm;
use App\Models\SHEKpiSafety;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class SHEKpiSafetyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = new SHEKpiSafety();
    }

    public function index(Request $request, $tahun)
    {   
        $model = $this->model->where('tahun', $tahun)->firstOrFail();
        $response = responseSuccess(__('messages.read-success'), $model);
        return response()->json($response);        
    } 

    public function store(Request $request)
    {
        $data = $request->only(
            'tahun',
            'kons1',
            'kons2',
            'target1',
            'target2',
        );

        $this->validate($request, [
            'tahun' => 'required',
            'kons1' => 'required',
            'kons2' => 'required',
            'target1' => 'required',
            'target2' => 'required',
        ]);

        DB::beginTransaction();
        try {
            $model = $this->model->updateOrCreate(['tahun' => $data['tahun']], $data);
            DB::commit();
            $response = responseSuccess(__('messages.create-success'), $model);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}


<?php

namespace App\Http\Controllers\SHESafetyPerm;
use App\Models\SHEApar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\SheAparTemplate;
use App\Imports\ExcelImportsWithHeader;
use App\Traits\ValidationExcelImport;
use Illuminate\Support\Facades\Validator;

class SHEAparController extends Controller
{
    use ValidationExcelImport;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = new SHEApar();
    }

    /**
     * @OA\Get(
     *   tags={"SHE-Apar"},
     *   path="/api/apar/",
     *   summary="Get apar list",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */

    public function index(Request $request){
        $query = $this->model;
        if(!empty($request->bulan)){
            $query = $query->where('bulan', strval($request->bulan));
        }
        if(!empty($request->tahun)){
            $query = $query->where('tahun', strval($request->tahun));
        }
        if(!empty($request->status) && $request->status != 'ALL'){
            $query = $query->where('status', strval($request->status));
        }
        $model = Datatables::of($query)
                ->addIndexColumn()   
                ->addColumn('report', function($row){
                    $month = date("F", mktime(0, 0, 0, $row->bulan, 10));
                    $ret = $month.' '.$row->tahun;
                    return $ret;
                }) 
                ->addColumn('m_aktif', function($row){
                    $month = date("F", mktime(0, 0, 0, $row->bulan_aktf, 10));
                    $ret = $month.' '.$row->tahun_aktf;
                    return $ret;
                })        
                ->addColumn('action', function($row){
                    $btn = '<a href="#" onclick="show('.$row->id.')" class="btn btn-primary btn-sm"><i class="fa fa-edit" style="padding-right: 0px; font-size:10;"></i></a> ';
                    $btn .= '<a href="#" onclick="btn_delete('.$row->id.')" class="btn btn-danger btn-sm"><i class="fa fa-trash" style="padding-right: 0px; font-size:10;"></i></a> ';
                    return $btn;
                })
                ->addColumn('files', function($row){                    
                    $btn = '';
                    if($row->file != ''){
                        $btn = ReadFiles($row->file);
                    }   
                    return $btn;
                })
                ->make(true)
                ->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);        
    } 

    /**
     * @OA\Post(
     *   tags={"SHE-Apar"},
     *   path="/api/apar",
     *   summary="Create apar",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Request"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"bulan","tahun","area","jenis_apar","unit_kerja","lokasi","clamp","press_bar","status"},
     *       @OA\Property(property="bulan", type="string", format="text", example="01"),
     *       @OA\Property(property="tahun", type="string", format="text", example="2023"),     
     *       @OA\Property(property="area", type="string", format="text", example="Kantor Crusher"),
     *       @OA\Property(property="unit_kerja", type="string", format="text", example="OPCR"),
     *       @OA\Property(property="jenis_apar", type="string", format="text", example="F4"),
     *       @OA\Property(property="lokasi", type="string", format="text", example="Lobby pintu depan"),
     *       @OA\Property(property="map_baru", type="string", format="text", example="F4"),
     *       @OA\Property(property="clamp", type="string", format="text", example="K"),
     *       @OA\Property(property="press_bar", type="number", format="float", example="10"),
     *       @OA\Property(property="hose", type="boolean", example="true"),
     *       @OA\Property(property="segel", type="boolean", example="true"),
     *       @OA\Property(property="sapot", type="boolean", example="true"),
     *       @OA\Property(property="berat", type="number", format="float", example="10"),
     *       @OA\Property(property="bulan_aktf", type="string", format="text", example="01"),
     *       @OA\Property(property="tahun_aktf", type="string", format="text", example="2024"), 
     *       @OA\Property(property="status", type="string", format="text", example="BAIK"),
     *       @OA\Property(property="keterangan", type="string", format="text", example="-"),
     *     )
     *   )
     * )
     */
    public function store(Request $request)
    {
        $data = $request->only(
            'bulan',
            'tahun',
            'area',
            'jenis_apar',
            'unit_kerja',
            'lokasi',
            'map_baru',
            'clamp',
            'press_bar',
            'hose',
            'segel',
            'sapot',
            'berat',
            'bulan_aktf',
            'tahun_aktf',
            'status',
            'file',
            'file_gdrive',
            'keterangan',
        );

        $this->validate($request, [
            'bulan' => 'required',
            'tahun' => 'required',
            'area' => 'required',
            'jenis_apar' => 'required',
            'unit_kerja' => 'required',
            'lokasi' => 'required',
            'clamp' => 'required',
            'press_bar' => 'required',
            'berat' => 'required',
            'bulan_aktf' => 'required',
            'tahun_aktf' => 'required',
            'status' => 'required',
        ]);

        DB::beginTransaction();
        try {
            $model = $this->model->create($data);
            DB::commit();
            $response = responseSuccess(__('messages.create-success'), $model);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *   tags={"SHE-Apar"},
     *   path="/api/apar/{id}",
     *   summary="Detail SHE-Apar",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Ok"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function show($id)
    {
        $model = $this->model->where('id', $id)->firstOrFail();
        $response = responseSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    /**
     * @OA\Put(
     *   tags={"SHE-Apar"},
     *   path="/api/apar/{id}",
     *   summary="SHE-Apar update",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"bulan","tahun","area","jenis_apar","unit_kerja","lokasi","clamp","press_bar","status"},
     *       @OA\Property(property="bulan", type="string", format="text", example="01"),
     *       @OA\Property(property="tahun", type="string", format="text", example="2023"),     
     *       @OA\Property(property="area", type="string", format="text", example="Kantor Crusher"),
     *       @OA\Property(property="unit_kerja", type="string", format="text", example="OPCR"),
     *       @OA\Property(property="jenis_apar", type="string", format="text", example="F4"),
     *       @OA\Property(property="lokasi", type="string", format="text", example="Lobby pintu depan"),
     *       @OA\Property(property="map_baru", type="string", format="text", example="F4"),
     *       @OA\Property(property="clamp", type="string", format="text", example="K"),
     *       @OA\Property(property="press_bar", type="number", format="float", example="10"),
     *       @OA\Property(property="hose", type="boolean", example="true"),
     *       @OA\Property(property="segel", type="boolean", example="true"),
     *       @OA\Property(property="sapot", type="boolean", example="true"),
     *       @OA\Property(property="berat", type="number", format="float", example="10"),
     *       @OA\Property(property="bulan_aktf", type="string", format="text", example="01"),
     *       @OA\Property(property="tahun_aktf", type="string", format="text", example="2024"), 
     *       @OA\Property(property="status", type="string", format="text", example="BAIK"),
     *       @OA\Property(property="keterangan", type="string", format="text", example="-"),
     *     )
     *   ),
     *   @OA\Response(response=200, description="Successfully updated"),
     *   @OA\Response(response=400, description="Bad request"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'bulan' => 'required',
            'tahun' => 'required',
            'area' => 'required',
            'jenis_apar' => 'required',
            'unit_kerja' => 'required',
            'lokasi' => 'required',
            'clamp' => 'required',
            'press_bar' => 'required',
            'berat' => 'required',
            'bulan_aktf' => 'required',
            'tahun_aktf' => 'required',
            'status' => 'required',
        ]);
        $model = $this->model->where('id', $id)->firstOrFail();

        DB::beginTransaction();
        try {
            if($model->file != '' && $model->file != $request->input('file')){
                $return = DeleteFiles($model->file);
            }
            $model->bulan = $request->input('bulan');
            $model->tahun = $request->input('tahun');
            $model->area = $request->input('area');
            $model->jenis_apar = $request->input('jenis_apar');
            $model->unit_kerja = $request->input('unit_kerja');
            $model->lokasi = $request->input('lokasi');
            $model->map_baru = $request->input('map_baru');
            $model->clamp = $request->input('clamp');
            $model->press_bar = $request->input('press_bar');
            $model->hose = isset($request->hose)  ? true : false;
            $model->segel = isset($request->segel) ? true : false;
            $model->sapot = isset($request->sapot) ? true : false;
            $model->berat = $request->input('berat');
            $model->bulan_aktf = $request->input('bulan_aktf');
            $model->tahun_aktf = $request->input('tahun_aktf');
            $model->status = $request->input('status');
            $model->file = $request->input('file');
            $model->file_gdrive = $request->input('file_gdrive');
            $model->keterangan = $request->input('keterangan');          
            $model->save();
            DB::commit();
            $response = responseSuccess(__('messages.update-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.update-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Delete(
     *   tags={"SHE-Apar"},
     *   path="/api/apar/{id}",
     *   summary="SHE-Apar destroy",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Successfully deleted"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function destroy($id)
    {        
        $model = $this->model->where('id', $id)->firstOrFail();
        DB::beginTransaction();
        try {
            if($model->file != '')
                $return = DeleteFiles($model->file);
            $model->delete();
            DB::commit();
            $response = responseSuccess(__('messages.delete-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.delete-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *   tags={"SHE-Apar"},
     *   path="/api/apar/template",
     *   summary="Get SHE-Apar Template",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="Ok"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */
    public function template(Request $request)
    {
        return Excel::download(new SheAparTemplate(), 'SHE_Apar.xlsx');
    }

    /**
     * @OA\Post(
     *   tags={"SHE-Apar"},
     *   path="/api/apar/import",
     *   summary="import SHE-Apar",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"fileupload"},
     *       @OA\Property(property="fileupload", type="file", format="text", example="apar.xlsx"),
     *     )
     *   )
     * )
     */
    public function import(Request $request)
    {
        $this->validate($request, [
            'bulan' => 'required',
            'tahun' => 'required',
            'upload_file' => 'required|mimes:xls,xlsx',
        ]);
        $file = $request->file('upload_file');
        $data = Excel::toArray(new ExcelImportsWithHeader, $file)[0];
        $string = '';
        if (!$this->compareTemplate("apar", array_keys($data[0]))) {
            return response()->json(responseFail(__('messages.validation-template-fail'), ["template" => [__('messages.validation-template-fail')]]), 400)->throwResponse();
        }
        $dataExcel = collect($data);
        $dataMaps = [];
        $lineValidation = [
            'area' => 'required',
            'jenis_apar' => 'required',
            'unit_kerja' => 'required',
            'lokasi' => 'required',
            'clamp' => 'required',
            'press_bar' => 'required',
            'berat_kg' => 'required',
            'bulan_maktif' => 'required',
            'tahun_maktif' => 'required',
            'status' => 'required',
        ];
        
        foreach ($dataExcel as $key => $item) {            
            $this->validateImport($item, $lineValidation, ($key + 2));
            array_push($dataMaps, [
                'bulan' => $request->bulan,
                'tahun' => $request->tahun,
                'area' => $item['area'],
                'jenis_apar' => $item['jenis_apar'],
                'unit_kerja' => $item['unit_kerja'],
                'lokasi' => $item['lokasi'],
                'map_baru' => $item['map_baru'],
                'clamp' => $item['clamp'],
                'press_bar' => $item['press_bar'],
                'hose' => strtolower($item['hose']) == 'v' ? true : false,
                'segel' => strtolower($item['segel']) == 'v' ? true : false,
                'sapot' => strtolower($item['sapot']) == 'v' ? true : false,
                'berat' => $item['berat_kg'], 
                'bulan_aktf' => $item['bulan_maktif'],
                'tahun_aktf' => $item['tahun_maktif'],
                'status' => $item['status'],
                'keterangan' => $item['keterangan'],
                'created_by' => Auth::user()->id,
                'created_at' => now()
            ]);
        }

        DB::beginTransaction();
        try{
            SHEApar::insert($dataMaps);
            DB::commit();
            $response = responseSuccess(__('messages.import-success'));
            return response()->json($response, Response::HTTP_CREATED);
        }
        catch(\Exception $ex){
            DB::rollBack();
            $response = responseFail(__('messages.import-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function upload_file(Request $request)
    {                  
        $data = $request->only(            
            'upload_file',
        );

        $this->validate($request, [            
            'upload_file' => 'required|mimes:jpeg,jpg,png,gif,svg|max:10240',
        ]);
        
        try {
            $return = StoreFiles($request, 'apar', 'she_safety');
            $response = responseSuccess(__('messages.upload-success'), $return);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.upload-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private function validateImport($data, array $rules, $line)
    {
        $messages = [
            'required' => __('validation.required'),
            'numeric'  => __('validation.numeric'),
            'exists'   => __('validation.exists'),
        ];

        $validator = Validator::make($data, $rules, $messages);

        if ($validator->fails()) {
            $response = responseFail(__('messages.validation-fail') . " " . __('messages.inline-fail') . " : " . $line, $validator->errors(), []);
            $return = response()->json($response, Response::HTTP_BAD_REQUEST, [], JSON_PRETTY_PRINT);
            $return->throwResponse();
        }
    }
}


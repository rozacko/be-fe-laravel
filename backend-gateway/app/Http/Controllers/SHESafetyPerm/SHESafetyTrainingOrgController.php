<?php

namespace App\Http\Controllers\SHESafetyPerm;
use App\Models\SHESafetyTrainingOrg;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\SheSafetyTrainingOrgTemplate;
use App\Imports\ExcelImportsWithHeader;
use App\Traits\ValidationExcelImport;
use Illuminate\Support\Facades\Validator;

class SHESafetyTrainingOrgController extends Controller
{
    use ValidationExcelImport;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = new SHESafetyTrainingOrg();
    }

    /**
     * @OA\Get(
     *   tags={"SHE-SafetyTrainingOrg"},
     *   path="/api/safetytrainingorg/",
     *   summary="Get safetytrainingorg list",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */

    public function index(Request $request){        
        $query = $this->model
                    ->where('id_training', $request->id);
        $model = Datatables::of($query)
                ->addIndexColumn()           
                ->addColumn('action', function($row){
                    $btn = '<a href="#" onclick="btn_delete_org('.$row->id.')" class="btn btn-danger btn-sm"><i class="fa fa-trash" style="padding-right: 0px; font-size:10;"></i></a> ';
                    return $btn;
                })
                ->make(true)
                ->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);        
    } 

    /**
     * @OA\Post(
     *   tags={"SHE-SafetyTrainingOrg"},
     *   path="/api/safetytrainingorg",
     *   summary="Create safetytrainingorg",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Request"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"id_training","no_pegawai","nm_pegawai","departement","unit_kerja","band","jam_pelatihan"},
     *       @OA\Property(property="id_training", type="string", format="text", example="1"),
     *       @OA\Property(property="no_pegawai", type="string", format="text", example="2023"),     
     *       @OA\Property(property="nm_pegawai", type="string", format="text", example="xxx"),
     *       @OA\Property(property="departement", type="string", format="text", example="xxx"),
     *       @OA\Property(property="unit_kerja", type="string", format="text", example="xxx"),
     *       @OA\Property(property="band", type="string", format="text", example="xxx"),
     *       @OA\Property(property="jam_pelatihan", type="number", format="float", example="10"),     
     *     )
     *   )
     * )
     */
    public function store(Request $request)
    {
        $data = $request->only(
            'id_training',
            'no_pegawai',
            'nm_pegawai',
            'departement',
            'unit_kerja',
            'band',
            'jam_pelatihan',
        );

        $this->validate($request, [
            'id_training' => 'required',
            'no_pegawai' => 'required',
            'nm_pegawai' => 'required',
            'departement' => 'required',
            'unit_kerja' => 'required',
            'band' => 'required',
            'jam_pelatihan' => 'required',
        ]);

        DB::beginTransaction();
        try {
            $model = $this->model->create($data);
            DB::commit();
            $response = responseSuccess(__('messages.create-success'), $model);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *   tags={"SHE-SafetyTrainingOrg"},
     *   path="/api/safetytrainingorg/{id}",
     *   summary="Detail SHE-SafetyTrainingOrg",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Ok"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function show($id)
    {
        $model = $this->model->where('id', $id)->firstOrFail();
        $response = responseSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    /**
     * @OA\Put(
     *   tags={"SHE-SafetyTrainingOrg"},
     *   path="/api/safetytrainingorg/{id}",
     *   summary="SHE-SafetyTrainingOrg update",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"id_training","no_pegawai","nm_pegawai","departement","unit_kerja","band","jam_pelatihan"},
     *       @OA\Property(property="id_training", type="string", format="text", example="1"),
     *       @OA\Property(property="no_pegawai", type="string", format="text", example="2023"),     
     *       @OA\Property(property="nm_pegawai", type="string", format="text", example="xxx"),
     *       @OA\Property(property="departement", type="string", format="text", example="xxx"),
     *       @OA\Property(property="unit_kerja", type="string", format="text", example="xxx"),
     *       @OA\Property(property="band", type="string", format="text", example="xxx"),
     *       @OA\Property(property="jam_pelatihan", type="number", format="float", example="10"),    
     *     )
     *   ),
     *   @OA\Response(response=200, description="Successfully updated"),
     *   @OA\Response(response=400, description="Bad request"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'id_training' => 'required',
            'no_pegawai' => 'required',
            'nm_pegawai' => 'required',
            'departement' => 'required',
            'unit_kerja' => 'required',
            'band' => 'required',
            'jam_pelatihan' => 'required',
        ]);
        $model = $this->model->where('id', $id)->firstOrFail();

        DB::beginTransaction();
        try {
            $model->id_training = $request->input('id_training');
            $model->no_pegawai = $request->input('no_pegawai');
            $model->nm_pegawai = $request->input('nm_pegawai');
            $model->departement = $request->input('departement');
            $model->unit_kerja = $request->input('unit_kerja');
            $model->band = $request->input('band');
            $model->jam_pelatihan = $request->input('jam_pelatihan');                     
            $model->save();
            DB::commit();
            $response = responseSuccess(__('messages.update-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.update-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Delete(
     *   tags={"SHE-SafetyTrainingOrg"},
     *   path="/api/safetytrainingorg/{id}",
     *   summary="SHE-SafetyTrainingOrg destroy",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Successfully deleted"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function destroy($id)
    {        
        $model = $this->model->where('id', $id)->firstOrFail();
        DB::beginTransaction();
        try {
            $model->delete();
            DB::commit();
            $response = responseSuccess(__('messages.delete-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.delete-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *   tags={"SHE-SafetyTrainingOrg"},
     *   path="/api/safetytrainingorg/template",
     *   summary="Get SHE-SafetyTrainingOrg Template",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="Ok"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */
    public function template(Request $request)
    {
        return Excel::download(new SheSafetyTrainingOrgTemplate(), 'SHE_SafetyTrainingOrg.xlsx');
    }

    /**
     * @OA\Post(
     *   tags={"SHE-SafetyTrainingOrg"},
     *   path="/api/safetytrainingorg/import",
     *   summary="import SHE-SafetyTrainingOrg",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"fileupload"},
     *       @OA\Property(property="fileupload", type="file", format="text", example="safetytrainingorg.xlsx"),
     *     )
     *   )
     * )
     */
    public function import(Request $request)
    {
        $this->validate($request, [
            'id_training' => 'required',
            'upload_file' => 'required|mimes:xls,xlsx',
        ]);
        $file = $request->file('upload_file');
        $data = Excel::toArray(new ExcelImportsWithHeader, $file)[0];        
        if (!$this->compareTemplate("safety-training-org", array_keys($data[0]))) {
            return response()->json(responseFail(__('messages.validation-template-fail'), ["template" => [__('messages.validation-template-fail')]]), 400)->throwResponse();
        }
        
        $dataExcel = collect($data);
        $dataMaps = [];
        $lineValidation = [
            'no_pegawai' => 'required',
            'nama_pegawai' => 'required',
            'departement' => 'required',
            'unit_kerja' => 'required',
            'band' => 'required',
            'jam_pelatihan' => 'required|numeric',            
        ];
        
        foreach ($dataExcel as $key => $item) {            
            $this->validateImport($item, $lineValidation, ($key + 2));
            array_push($dataMaps, [
                'no_pegawai' => $item['no_pegawai'],
                'nm_pegawai' => $item['nama_pegawai'],
                'departement' => $item['departement'],
                'unit_kerja' => $item['unit_kerja'],
                'band' => $item['band'],
                'jam_pelatihan' => $item['jam_pelatihan'],
                'id_training' => $request->id_training,
                'created_by' => Auth::user()->id,
                'created_at' => now()
            ]);
        }

        DB::beginTransaction();
        try{
            SHESafetyTrainingOrg::insert($dataMaps);
            DB::commit();
            $response = responseSuccess(__('messages.import-success'));
            return response()->json($response, Response::HTTP_CREATED);
        }
        catch(\Exception $ex){
            DB::rollBack();
            $response = responseFail(__('messages.import-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private function validateImport($data, array $rules, $line)
    {
        $messages = [
            'required' => __('validation.required'),
            'numeric'  => __('validation.numeric'),
            'exists'   => __('validation.exists'),
        ];

        $validator = Validator::make($data, $rules, $messages);

        if ($validator->fails()) {
            $response = responseFail(__('messages.validation-fail') . " " . __('messages.inline-fail') . " : " . $line, $validator->errors(), []);
            $return = response()->json($response, Response::HTTP_BAD_REQUEST, [], JSON_PRETTY_PRINT);
            $return->throwResponse();
        }
    }
}


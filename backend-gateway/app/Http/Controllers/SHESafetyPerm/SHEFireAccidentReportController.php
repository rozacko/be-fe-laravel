<?php

namespace App\Http\Controllers\SHESafetyPerm;
use App\Models\SHEFireAccidentReport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;

class SHEFireAccidentReportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = new SHEFireAccidentReport();
    }

    /**
     * @OA\Get(
     *   tags={"SHE-FireAccidentReport"},
     *   path="/api/fireaccidentreport/",
     *   summary="Get fireaccidentreport list",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */

    public function index(Request $request){
        $query = $this->model;
        if(!empty($request->bulan)){
            $query = $query->whereMonth('tgl_doc', $request->bulan);
        }
        if(!empty($request->tahun)){
            $query = $query->whereYear('tgl_doc', $request->tahun);
        }
        if(!empty($request->status) && $request->status != 'ALL'){
            $query = $query->where('status', $request->status);
        }
        $model = Datatables::of($query)
                ->addIndexColumn()                    
                ->addColumn('action', function($row){
                    $btn = '<a href="#" onclick="show('.$row->id.')" class="btn btn-primary btn-sm"><i class="fa fa-edit" style="padding-right: 0px; font-size:10;"></i></a> ';
                    $btn .= '<a href="#" onclick="btn_delete('.$row->id.')" class="btn btn-danger btn-sm"><i class="fa fa-trash" style="padding-right: 0px; font-size:10;"></i></a> ';
                    return $btn;
                })
                ->addColumn('files', function($row){                    
                    $btn = '';
                    if($row->file != ''){
                        $btn = ReadFiles($row->file);
                    }   
                    return $btn;
                })
                ->make(true)
                ->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);        
    } 

    /**
     * @OA\Post(
     *   tags={"SHE-FireAccidentReport"},
     *   path="/api/fireaccidentreport",
     *   summary="Create fireaccidentreport",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Request"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"tgl_doc","accident","location","person_status","investigation_team","report_prepared","report_reviewed","report_approved","file","status","type"},
     *       @OA\Property(property="tgl_doc", type="string", format="date", example="2023-03-13"),     
     *       @OA\Property(property="accident", type="string", format="text", example="Terkena flashback api Cutting Torch, 09 Desember 2022 Pukul 10.00 WIBB (Shift 1)"),
     *       @OA\Property(property="location", type="string", format="text", example="1 Area Ruang Compressor 341 CP2-7 bangunan sebelah Selatan."),
     *       @OA\Property(property="person_status", type="string", format="text", example="Korban 1, Sdr. Arif Martoyo (OP RKC1) pertolongan pertama poliklinik pabrik, First Aid."),
     *       @OA\Property(property="investigation_team", type="string", format="text", example="Musiran, Zaini, Jaenuri, Teguh Irianto, Dwi Agus A, Dwi Arif S, Abdullah, Jatmiko, Anjar P, Heru S, Juni CW"),
     *       @OA\Property(property="report_prepared", type="string", format="text", example="Teguh Irianto, Dwi Agus A, Jatmiko, Jaenuri, Dwi Arif S"),
     *       @OA\Property(property="report_reviewed", type="string", format="text", example="Musiran"),
     *       @OA\Property(property="report_approved", type="string", format="text", example="Reni Wulandari"),
     *       @OA\Property(property="file", type="string", format="binary", example=""),     
     *       @OA\Property(property="status", type="string", format="text", example="CLOSED"),
     *       @OA\Property(property="type", type="string", format="text", example="xxx"),
     *     )
     *   )
     * )
     */
    public function store(Request $request)
    {          
        $data = $request->only(
            'tgl_doc',
            'accident',
            'location',
            'person_status',
            'investigation_team',
            'report_prepared',
            'report_reviewed',
            'report_approved',
            'file',
            'status',
            'type',
        );

        $this->validate($request, [
            'tgl_doc' => 'required|date',
            'accident' => 'required',
            'location' => 'required',
            'person_status' => 'required',
            'investigation_team' => 'required',
            'report_prepared' => 'required',
            'report_reviewed' => 'required',
            'report_approved' => 'required',  
            'file' => 'required',
            'status' => 'required',
            'type' => 'required',
        ]);
        
        DB::beginTransaction();
        try {
            $model = $this->model->create($data);
            DB::commit();
            $response = responseSuccess(__('messages.create-success'), $model);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *   tags={"SHE-FireAccidentReport"},
     *   path="/api/fireaccidentreport/{id}",
     *   summary="Detail SHE-FireAccidentReport",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Ok"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function show($id)
    {
        $model = $this->model->where('id', $id)->firstOrFail();
        $response = responseSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    /**
      * @OA\Put(
     *   tags={"SHE-FireAccidentReport"},
     *   path="/api/fireaccidentreport/{id}",
     *   summary="Update fireaccidentreport",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"tgl_doc","accident","location","person_status","investigation_team","report_prepared","report_reviewed","report_approved","file","status","type"},
     *       @OA\Property(property="tgl_doc", type="string", format="date", example="2023-03-13"),     
     *       @OA\Property(property="accident", type="string", format="text", example="Terkena flashback api Cutting Torch, 09 Desember 2022 Pukul 10.00 WIBB (Shift 1)"),
     *       @OA\Property(property="location", type="string", format="text", example="1 Area Ruang Compressor 341 CP2-7 bangunan sebelah Selatan."),
     *       @OA\Property(property="person_status", type="string", format="text", example="Korban 1, Sdr. Arif Martoyo (OP RKC1) pertolongan pertama poliklinik pabrik, First Aid."),
     *       @OA\Property(property="investigation_team", type="string", format="text", example="Musiran, Zaini, Jaenuri, Teguh Irianto, Dwi Agus A, Dwi Arif S, Abdullah, Jatmiko, Anjar P, Heru S, Juni CW"),
     *       @OA\Property(property="report_prepared", type="string", format="text", example="Teguh Irianto, Dwi Agus A, Jatmiko, Jaenuri, Dwi Arif S"),
     *       @OA\Property(property="report_reviewed", type="string", format="text", example="Musiran"),
     *       @OA\Property(property="report_approved", type="string", format="text", example="Reni Wulandari"),
     *       @OA\Property(property="file", type="string", format="binary", example=""),     
     *       @OA\Property(property="status", type="string", format="text", example="CLOSED"),
     *       @OA\Property(property="type", type="string", format="text", example="CLOSED"),
     *     )
     *   ),
     *   @OA\Response(response=200, description="Successfully updated"),
     *   @OA\Response(response=400, description="Bad request"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'tgl_doc' => 'required|date',
            'accident' => 'required',
            'location' => 'required',
            'person_status' => 'required',
            'investigation_team' => 'required',
            'report_prepared' => 'required',
            'report_reviewed' => 'required',
            'report_approved' => 'required',
            'file' => 'required',
            'status' => 'required',
            'type' => 'required',
        ]);        
        $model = $this->model->where('id', $id)->firstOrFail();
        DB::beginTransaction();
        try {         
            if($model->file != '' && $model->file != $request->input('file')){
                $return = DeleteFiles($model->file);
            }   
            $model->tgl_doc = $request->input('tgl_doc');
            $model->accident = $request->input('accident');
            $model->location = $request->input('location');
            $model->person_status = $request->input('person_status');
            $model->investigation_team = $request->input('investigation_team');
            $model->report_prepared = $request->input('report_prepared');
            $model->report_reviewed = $request->input('report_reviewed');
            $model->report_approved = $request->input('report_approved');
            $model->file = $request->input('file');
            $model->status = $request->input('status');         
            $model->type = $request->input('type');         
            $model->save();
            DB::commit();
            $response = responseSuccess(__('messages.update-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.update-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Delete(
     *   tags={"SHE-FireAccidentReport"},
     *   path="/api/fireaccidentreport/{id}",
     *   summary="SHE-FireAccidentReport destroy",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Successfully deleted"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function destroy($id)
    {        
        $model = $this->model->where('id', $id)->firstOrFail();
        DB::beginTransaction();
        try {
            if($model->file != '')
                $return = DeleteFiles($model->file);
            $model->delete();
            DB::commit();
            $response = responseSuccess(__('messages.delete-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.delete-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function upload_file(Request $request)
    {                  
        $data = $request->only(            
            'upload_file',
        );

        $this->validate($request, [            
            'upload_file' => 'required|mimes:pdf|max:10240',
        ]);
        
        try {
            $return = StoreFiles($request, 'accident', 'she_safety');
            $response = responseSuccess(__('messages.upload-success'), $return);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.upload-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}

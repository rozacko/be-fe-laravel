<?php

namespace App\Http\Controllers\SHESafetyPerm;
use App\Models\SHEUnsafeCondition;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\SheUnsafeConditionTemplate;
use App\Imports\ExcelImportsWithHeader;
use App\Traits\ValidationExcelImport;
use Illuminate\Support\Facades\Validator;

class SHEUnsafeConditionController extends Controller
{
    use ValidationExcelImport;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = new SHEUnsafeCondition();
    }

    /**
     * @OA\Get(
     *   tags={"SHE-UnsafeCondition"},
     *   path="/api/unsafecondition/",
     *   summary="Get unsafecondition list",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */

    public function index(Request $request){
        $query = $this->model;
        if(!empty($request->bulan)){
            $query = $query->where('bulan', strval($request->bulan));
        }
        if(!empty($request->tahun)){
            $query = $query->where('tahun', strval($request->tahun));
        }
        if(!empty($request->status) && $request->status != 'ALL'){
            $query = $query->where('status', strval($request->status));
        }
        $model = Datatables::of($query)
                ->addIndexColumn() 
                ->addColumn('report', function($row){
                    $month = date("F", mktime(0, 0, 0, $row->bulan, 10));
                    $ret = $month.' '.$row->tahun;
                    return $ret;
                })                   
                ->addColumn('action', function($row){
                    $btn = '<a href="#" onclick="show('.$row->id.')" class="btn btn-primary btn-sm"><i class="fa fa-edit" style="padding-right: 0px; font-size:10;"></i></a> ';
                    $btn .= '<a href="#" onclick="btn_delete('.$row->id.')" class="btn btn-danger btn-sm"><i class="fa fa-trash" style="padding-right: 0px; font-size:10;"></i></a> ';
                    return $btn;
                })
                ->addColumn('files1', function($row){                    
                    $btn = '';
                    if($row->file != ''){
                        $btn = ReadFiles($row->file);
                    }   
                    return $btn;
                })
                ->addColumn('files2', function($row){                    
                    $btn = '';
                    if($row->file != ''){
                        $btn = ReadFiles($row->file);
                    }   
                    return $btn;
                })
                ->make(true)
                ->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);        
    } 

    /**
     * @OA\Post(
     *   tags={"SHE-UnsafeCondition"},
     *   path="/api/unsafecondition",
     *   summary="Create unsafecondition",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Request"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"bulan","tahun","area","tpm_sga","no_equipment","lokasi_kerja","uraian_temuan","saran","pic","unit_kerja","tgl_temuan","status"},
     *       @OA\Property(property="bulan", type="string", format="text", example="01"),
     *       @OA\Property(property="tahun", type="string", format="text", example="2023"),     
     *       @OA\Property(property="area", type="string", format="text", example="-"),
     *       @OA\Property(property="tpm_sga", type="string", format="text", example="-"),
     *       @OA\Property(property="lokasi_kerja", type="string", format="text", example="-"),
     *       @OA\Property(property="uraian_temuan", type="string", format="text", example="-"),
     *       @OA\Property(property="saran", type="string", format="text", example="-"),
     *       @OA\Property(property="pic", type="string", format="text", example="-"),
     *       @OA\Property(property="unit_kerja", type="string", format="text", example="-"),
     *       @OA\Property(property="id_trans_mso", type="string", format="text", example="-"),
     *       @OA\Property(property="no_notif", type="string", format="text", example="-"),
     *       @OA\Property(property="tgl_temuan", type="string", format="date", example="2023-03-13"),
     *       @OA\Property(property="tgl_target", type="string", format="date", example="2023-03-13"),
     *       @OA\Property(property="tgl_closing", type="string", format="date", example="2023-03-13"),
     *       @OA\Property(property="cek_1", type="boolean", example="true"),
     *       @OA\Property(property="cek_2", type="boolean", example="true"),
     *       @OA\Property(property="file_1", type="string", format="text", example=""),
     *       @OA\Property(property="file_gdrive_1", type="string", format="text", example=""),     
     *       @OA\Property(property="file_2", type="string", format="text", example=""),
     *       @OA\Property(property="file_gdrive_2", type="string", format="text", example=""),     
     *       @OA\Property(property="status", type="string", format="text", example="CLOSED"),
     *       @OA\Property(property="keterangan", type="string", format="text", example="-"),            
     *     )
     *   )
     * )
     */
    public function store(Request $request)
    {                 
        $data = $request->only(
            'bulan',
            'tahun',
            'area',
            'tpm_sga',
            'no_equipment',
            'lokasi_kerja',
            'uraian_temuan',
            'saran',
            'pic',
            'unit_kerja',
            'id_trans_mso',
            'no_notif',
            'tgl_temuan',
            'tgl_target',
            'tgl_closing',
            'cek_1',
            'cek_2',
            'file_1',
            'file_2',
            'file_gdrive_1',
            'file_gdrive_2',
            'status',
            'keterangan',
        );

        $this->validate($request, [
            'bulan' => 'required',
            'tahun' => 'required',
            'area' => 'required',
            'tpm_sga' => 'required',
            'no_equipment' => 'required',
            'lokasi_kerja' => 'required',
            'uraian_temuan' => 'required',
            'saran' => 'required',
            'pic' => 'required',
            'unit_kerja' => 'required',
            'tgl_temuan' => 'required|date',
            'status' => 'required',
        ]);

        DB::beginTransaction();
        try {
            $model = $this->model->create($data);
            DB::commit();
            $response = responseSuccess(__('messages.create-success'), $model);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *   tags={"SHE-UnsafeCondition"},
     *   path="/api/unsafecondition/{id}",
     *   summary="Detail SHE-UnsafeCondition",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Ok"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function show($id)
    {
        $model = $this->model->where('id', $id)->firstOrFail();
        $response = responseSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    /**
     * @OA\Put(
     *   tags={"SHE-UnsafeCondition"},
     *   path="/api/unsafecondition/{id}",
     *   summary="SHE-UnsafeCondition update",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"bulan","tahun","area","tpm_sga","no_equipment","lokasi_kerja","uraian_temuan","saran","pic","unit_kerja","tgl_temuan","status"},
     *       @OA\Property(property="bulan", type="string", format="text", example="01"),
     *       @OA\Property(property="tahun", type="string", format="text", example="2023"),     
     *       @OA\Property(property="area", type="string", format="text", example="-"),
     *       @OA\Property(property="tpm_sga", type="string", format="text", example="-"),
     *       @OA\Property(property="lokasi_kerja", type="string", format="text", example="-"),
     *       @OA\Property(property="uraian_temuan", type="string", format="text", example="-"),
     *       @OA\Property(property="saran", type="string", format="text", example="-"),
     *       @OA\Property(property="pic", type="string", format="text", example="-"),
     *       @OA\Property(property="unit_kerja", type="string", format="text", example="-"),
     *       @OA\Property(property="id_trans_mso", type="string", format="text", example="-"),
     *       @OA\Property(property="no_notif", type="string", format="text", example="-"),
     *       @OA\Property(property="tgl_temuan", type="string", format="date", example="2023-03-13"),
     *       @OA\Property(property="tgl_target", type="string", format="date", example="2023-03-13"),
     *       @OA\Property(property="tgl_closing", type="string", format="date", example="2023-03-13"),
     *       @OA\Property(property="cek_1", type="boolean", example="true"),
     *       @OA\Property(property="cek_2", type="boolean", example="true"),
     *       @OA\Property(property="file_1", type="string", format="text", example=""),
     *       @OA\Property(property="file_gdrive_1", type="string", format="text", example=""),     
     *       @OA\Property(property="file_2", type="string", format="text", example=""),
     *       @OA\Property(property="file_gdrive_2", type="string", format="text", example=""),     
     *       @OA\Property(property="status", type="string", format="text", example="CLOSED"),
     *       @OA\Property(property="keterangan", type="string", format="text", example="-"),
     *     )
     *   ),
     *   @OA\Response(response=200, description="Successfully updated"),
     *   @OA\Response(response=400, description="Bad request"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'bulan' => 'required',
            'tahun' => 'required',
            'area' => 'required',
            'tpm_sga' => 'required',
            'no_equipment' => 'required',
            'lokasi_kerja' => 'required',
            'uraian_temuan' => 'required',
            'saran' => 'required',
            'pic' => 'required',
            'unit_kerja' => 'required',
            'tgl_temuan' => 'required|date',
            'status' => 'required',
        ]);
        $model = $this->model->where('id', $id)->firstOrFail();

        DB::beginTransaction();
        try {
            if($model->file != '')
                $return = DeleteFiles($model->file);
            $model->bulan = $request->input('bulan');
            $model->tahun = $request->input('tahun');
            $model->area = $request->input('area');
            $model->tpm_sga = $request->input('tpm_sga');
            $model->no_equipment = $request->input('no_equipment');
            $model->lokasi_kerja = $request->input('lokasi_kerja');
            $model->uraian_temuan = $request->input('uraian_temuan');
            $model->saran = $request->input('saran');
            $model->pic = $request->input('pic');
            $model->unit_kerja = $request->input('unit_kerja');
            $model->id_trans_mso = $request->input('id_trans_mso');
            $model->no_notif = $request->input('no_notif');
            $model->tgl_temuan = $request->input('tgl_temuan');
            $model->tgl_target = $request->input('tgl_target');
            $model->tgl_closing = $request->input('tgl_closing');
            $model->cek_1 = $request->input('cek_1') ? true : false;
            $model->cek_2 = $request->input('cek_2') ? true : false;
            $model->file_1 = $request->input('file_1');
            $model->file_2 = $request->input('file_2');
            $model->file_gdrive_1 = $request->input('file_gdrive_1');
            $model->file_gdrive_2 = $request->input('file_gdrive_2');
            $model->status = $request->input('status');
            $model->keterangan = $request->input('keterangan');         
            $model->save();
            DB::commit();
            $response = responseSuccess(__('messages.update-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.update-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Delete(
     *   tags={"SHE-UnsafeCondition"},
     *   path="/api/unsafecondition/{id}",
     *   summary="SHE-UnsafeCondition destroy",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Successfully deleted"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function destroy($id)
    {        
        $model = $this->model->where('id', $id)->firstOrFail();
        DB::beginTransaction();
        try {
            if($model->file != '')
                $return = DeleteFiles($model->file);
            $model->delete();
            DB::commit();
            $response = responseSuccess(__('messages.delete-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.delete-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *   tags={"SHE-UnsafeCondition"},
     *   path="/api/unsafecondition/template",
     *   summary="Get SHE-UnsafeCondition Template",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="Ok"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */
    public function template(Request $request)
    {
        return Excel::download(new SheUnsafeConditionTemplate(), 'SHE_Unsafe_Condition.xlsx');
    }

    /**
     * @OA\Post(
     *   tags={"SHE-UnsafeCondition"},
     *   path="/api/unsafecondition/import",
     *   summary="import SHE-UnsafeCondition",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"fileupload"},
     *       @OA\Property(property="fileupload", type="file", format="text", example="unsafecondition.xlsx"),
     *     )
     *   )
     * )
     */
    public function import(Request $request)
    {
        $this->validate($request, [
            'bulan' => 'required',
            'tahun' => 'required',
            'upload_file' => 'required|mimes:xls,xlsx',
        ]);
        $file = $request->file('upload_file');
        $data = Excel::toArray(new ExcelImportsWithHeader, $file)[0];
        $string = '';
        if (!$this->compareTemplate("unsafe-condition", array_keys($data[0]))) {
            return response()->json(responseFail(__('messages.validation-template-fail'), ["template" => [__('messages.validation-template-fail')]]), 400)->throwResponse();
        }
        $dataExcel = collect($data);
        $dataMaps = [];
        $lineValidation = [
            'area' => 'required',
            'tpm_sga' => 'required',
            'no_equipment' => 'required',
            'lokasi' => 'required',
            'uraian_temuan' => 'required',
            'saran_tindak_lanjut' => 'required',
            'pic' => 'required',
            'unit_kerja' => 'required',
            'tanggal_temuan' => 'required',
            'status' => 'required',
        ];
        
        foreach ($dataExcel as $key => $item) {            
            $this->validateImport($item, $lineValidation, ($key + 2));        
            array_push($dataMaps, [
                'bulan' => $request->bulan,
                'tahun' => $request->tahun,
                'area' => $item['area'],
                'tpm_sga' => $item['tpm_sga'],
                'no_equipment' => $item['no_equipment'],
                'lokasi_kerja' => $item['lokasi'],
                'uraian_temuan' => $item['uraian_temuan'],
                'saran' => $item['saran_tindak_lanjut'],
                'pic' => $item['pic'],
                'unit_kerja' => $item['unit_kerja'],
                'id_trans_mso' => $item['id_trans_mso'],
                'no_notif' => $item['no_notifikasi'],
                'tgl_temuan' => $item['tanggal_temuan'],
                'tgl_target' => $item['tanggal_target'],
                'cek_1' => strtolower($item['pengecekan_1']) == 'v' ? true : false,
                'cek_2' => strtolower($item['pengecekan_2']) == 'v' ? true : false,
                'tgl_closing' => $item['tanggal_closing'],
                'status' => $item['status'],
                'keterangan' => $item['keterangan'],
                'created_by' => Auth::user()->id,
                'created_at' => now()
            ]);
        }

        DB::beginTransaction();
        try{
            SHEUnsafeCondition::insert($dataMaps);
            DB::commit();
            $response = responseSuccess(__('messages.import-success'));
            return response()->json($response, Response::HTTP_CREATED);
        }
        catch(\Exception $ex){
            DB::rollBack();
            $response = responseFail(__('messages.import-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function upload_file(Request $request)
    {                  
        $data = $request->only(            
            'upload_file',
        );

        $this->validate($request, [            
            'upload_file' => 'required|mimes:jpeg,jpg,png,gif,svg|max:10240',
        ]);
        
        try {
            $return = StoreFiles($request, 'unsafe_action', 'she_safety');
            $response = responseSuccess(__('messages.upload-success'), $return);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.upload-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private function validateImport($data, array $rules, $line)
    {
        $messages = [
            'required' => __('validation.required'),
            'numeric'  => __('validation.numeric'),
            'exists'   => __('validation.exists'),
        ];

        $validator = Validator::make($data, $rules, $messages);

        if ($validator->fails()) {
            $response = responseFail(__('messages.validation-fail') . " " . __('messages.inline-fail') . " : " . $line, $validator->errors(), []);
            $return = response()->json($response, Response::HTTP_BAD_REQUEST, [], JSON_PRETTY_PRINT);
            $return->throwResponse();
        }
    }
}


<?php

namespace App\Http\Controllers\SHESafetyPerm;
use App\Models\SHEMFireAlarm;
use App\Models\SHEFireAlarm;
use App\Models\SHEFireAlarmFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\SheFirealarmTemplate;
use App\Imports\ExcelImportsWithHeader;
use App\Traits\ValidationExcelImport;
use Illuminate\Support\Facades\Validator;

class SHEFireAlarmController extends Controller
{
    use ValidationExcelImport;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->mastermodel = new SHEMFireAlarm();
        $this->model = new SHEFireAlarm();
    }

    /**
     * @OA\Get(
     *   tags={"SHE-FireAlarm"},
     *   path="/api/firealarm/",
     *   summary="Get firealarm list",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */

    public function index(Request $request){
        $query = $this->model;
        if(!empty($request->bulan)){
            $query = $query->where('bulan', strval($request->bulan));
        }
        if(!empty($request->tahun)){
            $query = $query->where('tahun', strval($request->tahun));
        }
        $query = $query->select('bulan','tahun')->groupBy('bulan','tahun');
        $model = Datatables::of($query)
                ->addIndexColumn()  
                ->addColumn('report', function($row){
                    $month = date("F", mktime(0, 0, 0, $row->bulan, 10));
                    $ret = $month.' '.$row->tahun;
                    return $ret;
                })
                ->addColumn('report_id', function($row){
                    $ret = $row->tahun.$row->bulan;
                    return $ret;
                })  
                ->addColumn('status', function($row){
                    $ret = 'CLOSED';
                    $query = $this->model;
                    if(!empty($request->bulan)){
                        $query = $query->where('bulan', strval($request->bulan));
                    }
                    if(!empty($request->tahun)){
                        $query = $query->where('tahun', strval($request->tahun));
                    }
                    $query = $query->where('status','OPEN')->count();
                    if($query != 0)
                        $ret = 'OPEN';
                    return $ret;
                })                   
                ->addColumn('action', function($row){
                    $id =  strval($row->tahun.$row->bulan);
                    $btn = '<a href="#" onclick="show('.$id.')" class="btn btn-primary btn-sm"><i class="fa fa-edit" style="padding-right: 0px; font-size:10;"></i></a> ';
                    $btn .= '<a href="#" onclick="btn_delete('.$id.')" class="btn btn-danger btn-sm"><i class="fa fa-trash" style="padding-right: 0px; font-size:10;"></i></a> ';                    
                    return $btn;
                })
                ->make(true)
                ->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);        
    } 

    /**
     * @OA\Post(
     *   tags={"SHE-FireAlarm"},
     *   path="/api/firealarm",
     *   summary="Create firealarm",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Request"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"parameter_id","bulan","tahun","status"},
     *       @OA\Property(property="bulan", type="string", format="text", example="01"),
     *       @OA\Property(property="tahun", type="string", format="text", example="2023"),
     *       @OA\Property(property="dt_m", type="boolean", example="true"),
     *       @OA\Property(property="dt_w1", type="boolean", example="true"),
     *       @OA\Property(property="dt_w2", type="boolean", example="true"),
     *       @OA\Property(property="dt_w3", type="boolean", example="true"),
     *       @OA\Property(property="dt_w4", type="boolean", example="true"),
     *       @OA\Property(property="status", type="string", format="text", example="CLOSED"),
     *       @OA\Property(property="keterangan", type="string", format="text", example=""),
     *     )
     *   )
     * )
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'bulan' => 'required',
            'tahun' => 'required',
        ]);

        $master = $this->mastermodel->orderBy('id','asc')->get();
        DB::beginTransaction();
        try {
            if($master){
                foreach ($master as $key => $value) {
                    $id = $value->id;
                    $data['parameter_id'] = $id;
                    $data['bulan'] = $request->bulan;
                    $data['tahun'] = $request->tahun;
                    foreach (['12','34'] as $k => $ccr) {
                        $data['ccr_tuban'] = $ccr;                        
                        $var_m = 'b_ccr'.$ccr;
                        $ket = 'ket_'.$ccr;
                        $status = 'status_'.$ccr.'_'.$id;
                        foreach (range(1,4) as $kk => $vv) {
                            $var_w = 'm_ccr'.$ccr.'_'.$vv;
                            $data['dt_w'.$vv] = isset($request->$var_w[$id]) ? true : false;
                        }
                        $data['dt_m'] = isset($request->$var_m[$id]) ? true : false;
                        $data['keterangan'] = isset($request->$ket[$id]) ? $request->$ket[$id] : '';
                        $data['status'] = $request->$status;
                        $model = $this->model->create($data);
                    }
                }
            }
            DB::commit();
            $response = responseSuccess(__('messages.create-success'), $model);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *   tags={"SHE-FireAlarm"},
     *   path="/api/firealarm/{id}",
     *   summary="Detail SHE-FireAlarm",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Ok"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function show($id)
    {        
        $bulan = substr($id,-2);
        $tahun = substr($id,0,4);
        $model = $this->model->where('bulan', $bulan)
                ->where('tahun', $tahun)
                ->get();        
        $response = responseSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    /**
     * @OA\Get(
     *   tags={"SHE-FireAlarm"},     
     *   path="/api/mfirealarm/",
     *   summary="List Master SHE-FireAlarm",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Ok"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function show_master()
    {
        $model = $this->mastermodel->orderBy('id','asc')->get();
        $response = responseSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    /**
     * @OA\Put(
     *   tags={"SHE-FireAlarm"},
     *   path="/api/firealarm/{id}",
     *   summary="SHE-FireAlarm update",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"parameter_id","bulan","tahun","status"},
     *       @OA\Property(property="bulan", type="string", format="text", example="01"),
     *       @OA\Property(property="tahun", type="string", format="text", example="2023"),
     *       @OA\Property(property="dt_m", type="boolean", example="true"),
     *       @OA\Property(property="dt_w1", type="boolean", example="true"),
     *       @OA\Property(property="dt_w2", type="boolean", example="true"),
     *       @OA\Property(property="dt_w3", type="boolean", example="true"),
     *       @OA\Property(property="dt_w4", type="boolean", example="true"),
     *       @OA\Property(property="status", type="string", format="text", example="CLOSED"),
     *       @OA\Property(property="keterangan", type="string", format="text", example=""),
     *     )
     *   ),
     *   @OA\Response(response=200, description="Successfully updated"),
     *   @OA\Response(response=400, description="Bad request"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'bulan' => 'required',
            'tahun' => 'required',
        ]);

        $master = $this->mastermodel->orderBy('id','asc')->get();
        DB::beginTransaction();
        try {
            if($master){
                foreach ($master as $key => $value) {
                    $id = $value->id;
                    $data['parameter_id'] = $id;
                    $data['bulan'] = $request->bulan;
                    $data['tahun'] = $request->tahun;
                    foreach (['12','34'] as $k => $ccr) {
                        $data['ccr_tuban'] = $ccr;                        
                        $var_m = 'b_ccr'.$ccr;
                        $ket = 'ket_'.$ccr;
                        $status = 'status_'.$ccr.'_'.$id;
                        foreach (range(1,4) as $kk => $vv) {
                            $var_w = 'm_ccr'.$ccr.'_'.$vv;
                            $data['dt_w'.$vv] = isset($request->$var_w[$id]) ? true : false;
                        }
                        $data['dt_m'] = isset($request->$var_m[$id]) ? true : false;
                        $data['keterangan'] = isset($request->$ket[$id]) ? $request->$ket[$id] : '';
                        $data['status'] = $request->$status;                        

                        $model = $this->model->updateOrCreate([
                            'parameter_id' => $data['parameter_id'],
                            'bulan' => $data['bulan'],
                            'tahun' => $data['tahun'],
                            'ccr_tuban' => $data['ccr_tuban'],
                        ], [
                            'parameter_id' => $data['parameter_id'],
                            'bulan' => $data['bulan'],
                            'tahun' => $data['tahun'],
                            'ccr_tuban' => $data['ccr_tuban'],
                            'keterangan' => $data['keterangan'],
                            'status' => $data['status'],
                            'dt_m' => $data['dt_m'],
                            'dt_w1' => $data['dt_w1'],
                            'dt_w2' => $data['dt_w2'],
                            'dt_w3' => $data['dt_w3'],
                            'dt_w4' => $data['dt_w4'],
                        ]);
                    }
                    
                }
            }         
            DB::commit();
            $response = responseSuccess(__('messages.update-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.update-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Delete(
     *   tags={"SHE-FireAlarm"},
     *   path="/api/firealarm/{id}",
     *   summary="SHE-FireAlarm destroy",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Successfully deleted"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function destroy($id)
    {                
        DB::beginTransaction();
        try {            
            $bulan = substr($id,-2);
            $tahun = substr($id,0,4);
            $model = $this->model->where('bulan', $bulan)
                    ->where('tahun', $tahun)
                    ->delete();
            DB::commit();
            $response = responseSuccess(__('messages.delete-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.delete-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *   tags={"SHE-FireAlarm"},
     *   path="/api/firealarm/template",
     *   summary="Get SHE-FireAlarm Template",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="Ok"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */
    public function template(Request $request)
    {
        return Excel::download(new SheFirealarmTemplate(), 'SHE_FIRE_ALARM.xlsx');
    }

    /**
     * @OA\Post(
     *   tags={"SHE-FireAlarm"},
     *   path="/api/firealarm/import",
     *   summary="import SHE-FireAlarm",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"fileupload"},
     *       @OA\Property(property="fileupload", type="file", format="text", example="firealarm.xlsx"),
     *     )
     *   )
     * )
     */
    public function import(Request $request)
    {
        $this->validate($request, [
            'bulan' => 'required',
            'tahun' => 'required',
            'upload_file' => 'required|mimes:xls,xlsx',
        ]);
        $file = $request->file('upload_file');
        $data = Excel::toArray(new ExcelImportsWithHeader, $file)[0];
        $data2 = Excel::toArray(new ExcelImportsWithHeader, $file)[1];
        
        //unset colom header null
        unset($data[0]['8']);
        unset($data2[0]['5']);
        
        if (!$this->compareTemplate("fire-alarm-mingguan", array_keys($data[0]))) {
            return response()->json(responseFail(__('messages.validation-template-fail'), ["template" => [__('messages.validation-template-fail')]]), 400)->throwResponse();
        }
        
        if (!$this->compareTemplate("fire-alarm-bulanan", array_keys($data2[0]))) {
            return response()->json(responseFail(__('messages.validation-template-fail'), ["template" => [__('messages.validation-template-fail')]]), 400)->throwResponse();
        }
        $dataExcel = collect($data);
        $dataExcel2 = collect($data2);
        $dataMaps = [];
        $dataMaps2 = [];
        $lineValidation = [
            'id' => 'required',
            'parameter' => 'required',           
        ];
        
        foreach ($dataExcel as $key => $item) {
            $this->validateImport($item, $lineValidation, ($key + 2), 'Mingguan');
            array_push($dataMaps, [
                'bulan' => $request->bulan,
                'tahun' => $request->tahun,
                'parameter_id' => $item['id'],                
                'status' => $item['ccr_1_2_status'] != '' ? strtoupper($item['ccr_1_2_status']) : 'CLOSE',
                'keterangan' => $item['ccr_1_2_keterangan'] != '' ? $item['ccr_1_2_keterangan'] : '',
                'dt_w1' => strtolower($item['ccr_1_2_i']) == 'v' ? true : false,
                'dt_w2' => strtolower($item['ccr_1_2_ii']) == 'v' ? true : false,
                'dt_w3' => strtolower($item['ccr_1_2_iii']) == 'v' ? true : false,
                'dt_w4' => strtolower($item['ccr_1_2_iv']) == 'v' ? true : false,
                'ccr_tuban' => '12',                
                'created_by' => Auth::user()->id,
                'created_at' => now()
            ]);
            array_push($dataMaps, [
                'bulan' => $request->bulan,
                'tahun' => $request->tahun,
                'parameter_id' => $item['id'],                
                'status' => $item['ccr_3_4_status'] != '' ? strtoupper($item['ccr_3_4_status']) : 'CLOSE',
                'keterangan' => $item['ccr_3_4_keterangan'] != '' ? $item['ccr_3_4_keterangan'] : '',
                'dt_w1' => strtolower($item['ccr_3_4_i']) == 'v' ? true : false,
                'dt_w2' => strtolower($item['ccr_3_4_ii']) == 'v' ? true : false,
                'dt_w3' => strtolower($item['ccr_3_4_iii']) == 'v' ? true : false,
                'dt_w4' => strtolower($item['ccr_3_4_iv']) == 'v' ? true : false,
                'ccr_tuban' => '34',                
                'created_by' => Auth::user()->id,
                'created_at' => now()
            ]);
        }

        foreach ($dataExcel2 as $key => $item) {
            $this->validateImport($item, $lineValidation, ($key + 2), 'Bulanan');
            array_push($dataMaps2, [
                'bulan' => $request->bulan,
                'tahun' => $request->tahun,
                'parameter_id' => $item['id'],                
                'status' => $item['ccr_1_2_status'] != '' ? strtoupper($item['ccr_1_2_status']) : 'CLOSE',
                'keterangan' => $item['ccr_1_2_keterangan'] != '' ? $item['ccr_1_2_keterangan'] : '',
                'dt_m' => strtolower($item['ccr_1_2']) == 'v' ? true : false,
                'ccr_tuban' => '12',
                'created_by' => Auth::user()->id,
                'created_at' => now()
            ]);
            array_push($dataMaps2, [
                'bulan' => $request->bulan,
                'tahun' => $request->tahun,
                'parameter_id' => $item['id'],                
                'status' => $item['ccr_3_4_status'] != '' ? strtoupper($item['ccr_3_4_status']) : 'CLOSE',
                'keterangan' => $item['ccr_3_4_keterangan'] != '' ? $item['ccr_3_4_keterangan'] : '',
                'dt_m' => strtolower($item['ccr_3_4']) == 'v' ? true : false,
                'ccr_tuban' => '34',                
                'created_by' => Auth::user()->id,
                'created_at' => now()
            ]);
        }
        // return $dataMaps;
        DB::beginTransaction();
        try{
            SHEFireAlarm::insert($dataMaps);
            SHEFireAlarm::insert($dataMaps2);
            DB::commit();
            $response = responseSuccess(__('messages.import-success'));
            return response()->json($response, Response::HTTP_CREATED);
        }
        catch(\Exception $ex){
            DB::rollBack();
            $response = responseFail(__('messages.import-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private function validateImport($data, array $rules, $line, $sheet)
    {
        $messages = [
            'required' => __('validation.required'),
            'numeric'  => __('validation.numeric'),
            'exists'   => __('validation.exists'),
        ];

        $validator = Validator::make($data, $rules, $messages);

        if ($validator->fails()) {
            $response = responseFail(__('messages.validation-fail') . " " . __('messages.inline-fail') . " : " . $line . '(Sheet : '.$sheet.')', $validator->errors(), []);
            $return = response()->json($response, Response::HTTP_BAD_REQUEST, [], JSON_PRETTY_PRINT);
            $return->throwResponse();
        }
    }
}


<?php

namespace App\Http\Controllers\SHESafetyPerm;
use App\Models\SHEManHours;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class SHEManHoursController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = new SHEManHours();
    }

    /**
     * @OA\Get(
     *   tags={"SHE-ManHours"},
     *   path="/api/manhours/",
     *   summary="Get manhours list",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */

    public function index(Request $request){        
        $query = DB::table('she_man_hours')
            ->leftJoin('she_man_power', function($join)
            {
                $join->on('she_man_hours.bulan', '=', 'she_man_power.bulan');
                $join->on('she_man_hours.tahun', '=', 'she_man_power.tahun');
        
            })
            ->select('she_man_hours.id','she_man_hours.bulan','she_man_hours.tahun','she_man_hours.jml_jam','she_man_hours.jml_hari',DB::raw("SUM(she_man_power.jml_orang) as jml_orang"))
            ->groupBy('she_man_hours.id','she_man_hours.bulan','she_man_hours.tahun','she_man_hours.jml_jam','she_man_hours.jml_hari');
        if(!empty($request->bulan)){
            $query = $query->where('she_man_hours.bulan', $request->bulan);
            $query = $query->where('she_man_power.bulan', $request->bulan);
        }
        if(!empty($request->tahun)){
            $query = $query->where('she_man_hours.tahun', $request->tahun);
            $query = $query->where('she_man_power.tahun', $request->tahun);
        }
        $model = Datatables::of($query)
                ->addIndexColumn()
                ->addColumn('report', function($row){
                    $month = date("F", mktime(0, 0, 0, $row->bulan, 10));
                    $ret = $month.' '.$row->tahun;
                    return $ret;
                })
                ->addColumn('jml_orang', function($row){                    
                    $ret = number_format($row->jml_orang);
                    return $ret;
                }) 
                ->addColumn('total', function($row){                    
                    $ret = number_format($row->jml_orang*$row->jml_jam*$row->jml_hari);
                    return $ret;
                })                
                ->addColumn('action', function($row){
                    $btn = '<a href="#" onclick="show('.$row->id.')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> ';
                    $btn .= '<a href="#" onclick="btn_delete('.$row->id.')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a> ';
                    return $btn;
                })
                ->make(true)
                ->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);        
    } 

    /**
     * @OA\Post(
     *   tags={"SHE-ManHours"},
     *   path="/api/manhours",
     *   summary="Create manhours",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Request"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"bulan","tahun","jml_jam","jml_hari"},
     *       @OA\Property(property="bulan", type="string", format="text", example="01"),
     *       @OA\Property(property="tahun", type="string", format="text", example="2023"),
     *       @OA\Property(property="jml_jam", type="number", format="float", example="8"),
     *       @OA\Property(property="jml_hari", type="number", format="float", example="22"),
     *     )
     *   )
     * )
     */
    public function store(Request $request)
    {
        $data = $request->only(
            'bulan',
            'tahun',
            'jml_jam',
            'jml_hari',
        );

        $this->validate($request, [
            'bulan' => 'required',
            'tahun' => 'required',
            'jml_jam' => 'required',
            'jml_hari' => 'required',
        ]);

        DB::beginTransaction();
        try {
            $model = $this->model->create($data);
            DB::commit();
            $response = responseSuccess(__('messages.create-success'), $model);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *   tags={"SHE-ManHours"},
     *   path="/api/manhours/{id}",
     *   summary="Detail SHE-ManHours",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Ok"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function show($id)
    {
        $model = $this->model->where('id', $id)->firstOrFail();
        $response = responseSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    /**
     * @OA\Put(
     *   tags={"SHE-ManHours"},
     *   path="/api/manhours/{id}",
     *   summary="SHE-ManHours update",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"bulan","tahun","jml_jam","jml_hari"},
     *       @OA\Property(property="bulan", type="string", format="text", example="01"),
     *       @OA\Property(property="tahun", type="string", format="text", example="2023"),
     *       @OA\Property(property="jml_jam", type="number", format="float", example="8"),
     *       @OA\Property(property="jml_hari", type="number", format="float", example="22"),
     *     )
     *   ),
     *   @OA\Response(response=200, description="Successfully updated"),
     *   @OA\Response(response=400, description="Bad request"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'bulan' => 'required',
            'tahun' => 'required',
            'jml_jam' => 'required',
            'jml_hari' => 'required',
        ]);
        $model = $this->model->where('id', $id)->firstOrFail();

        DB::beginTransaction();
        try {
            $model->bulan = $request->input('bulan');
            $model->tahun = $request->input('tahun');            
            $model->jml_jam = $request->input('jml_jam');            
            $model->jml_hari = $request->input('jml_hari');
            $model->save();
            DB::commit();
            $response = responseSuccess(__('messages.update-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.update-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Delete(
     *   tags={"SHE-ManHours"},
     *   path="/api/manhours/{id}",
     *   summary="SHE-ManHours destroy",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Successfully deleted"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function destroy($id)
    {        
        $model = $this->model->where('id', $id)->firstOrFail();
        DB::beginTransaction();
        try {
            $model->delete();
            DB::commit();
            $response = responseSuccess(__('messages.delete-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.delete-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}


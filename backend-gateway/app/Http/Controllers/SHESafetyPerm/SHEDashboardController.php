<?php

namespace App\Http\Controllers\SHESafetyPerm;
use App\Models\SHEKpiSafety;
use App\Models\SHEManPower;
use App\Models\SHEManHours;
use App\Models\SHEUnsafeAction;
use App\Models\SHEUnsafeCondition;
use App\Models\SHEAccidentReport;
use App\Models\SHEFireAccidentReport;
use App\Models\SHESafetyTraining;
use App\Models\SHESafetyTrainingOrg;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class SHEDashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * @OA\Get(
     *   tags={"SHE-Dashboard"},
     *   path="/api/dashboard_kpi/",
     *   summary="Get dashboard_kpi",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */

    public function dashboard_kpi(Request $request){
        $konstanta = SHEKpiSafety::where('tahun', strval($request->tahun))->firstOrFail();
        $realisasiLTIFR = 0;
        $realisasiNOFA = SHEFireAccidentReport::whereYear('tgl_doc', strval($request->tahun))->where('type', 'Kerugian > 3M')->count();
        $query = $konstanta->kons1 * (2 - ($realisasiLTIFR / $konstanta->target1)) + $konstanta->kons2 * (2 - ($realisasiNOFA / $konstanta->target2)) . ' %';        
        $response = responseSuccess(__('messages.read-success'), $query);
        return response()->json($response);
    } 

    /**
     * @OA\Get(
     *   tags={"SHE-Dashboard"},
     *   path="/api/dashboard_ltifr_ltisr/",
     *   summary="Get dashboard_ltifr_ltisr",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */

    public function dashboard_ltifr_ltisr(Request $request){
        if($request->status == 'Bulan'){
            $bulan = $this->bulan();
            $bulan_now = 1000000*date('n')/12;
            $bulan_q = 1000000*4/12;
            return $bulan_now;
            $perbulan = 1000000;
            $query['data1'] = [28, 29, 33, 36, 32, 32, 33, 28, 29, 33, 36, 32];
            $query['data2'] = [12, 11, 14, 18, 17, 13, 13, 12, 11, 14, 18, 17];
            $query['label'] = [];
            foreach ($bulan as $key => $value) {
                array_push($query['label'], $value['desc']);
            }
        }else{
            $query['data1'] = [28, 29, 33];
            $query['data2'] = [12, 11, 14];
            $query['label'] = ['2019','2020','2023'];
        }
        $response = responseSuccess(__('messages.read-success'), $query);
        return response()->json($response);
    }

    /**
     * @OA\Get(
     *   tags={"SHE-Dashboard"},
     *   path="/api/dashboard_man_power/",
     *   summary="Get dashboard_man_power",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */

    public function dashboard_man_power(Request $request){
        $sig_ket = ['SIG','PT. SEMEN INDONESIA GROUP'];
        $dt_sig = SHEManPower::whereIn('nm_perusahaan', $sig_ket);
        $dt_non_sig = SHEManPower::query();
        if(!empty($request->bulan) && $request->bulan != 'ALL'){
            $dt_sig = $dt_sig->where('bulan', strval($request->bulan));
            $dt_non_sig = $dt_non_sig->where('bulan', strval($request->bulan));
        }
        if(!empty($request->tahun)){
            $dt_sig = $dt_sig->where('tahun', strval($request->tahun));
            $dt_non_sig = $dt_non_sig->where('tahun', strval($request->tahun));
        }
        $dt_sig = $dt_sig->count();
        $dt_non_sig = $dt_non_sig->count();
        $query = [$dt_sig,$dt_non_sig]; 
        $response = responseSuccess(__('messages.read-success'), $query);
        return response()->json($response);
    }

    /**
     * @OA\Get(
     *   tags={"SHE-Dashboard"},
     *   path="/api/dashboard_man_hours/",
     *   summary="Get dashboard_man_hours",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */

    public function dashboard_man_hours(Request $request){
        $sig_ket = ['SIG','PT. SEMEN INDONESIA GROUP'];
        $dt_sig = SHEManPower::whereIn('nm_perusahaan', $sig_ket);
        $dt_non_sig = SHEManPower::query();
        $dt_hours = SHEManHours::select(DB::raw("(jml_jam * jml_hari) as jml_hours"));
        if(!empty($request->bulan) && $request->bulan != 'ALL'){
            $dt_sig = $dt_sig->where('bulan', strval($request->bulan));
            $dt_non_sig = $dt_non_sig->where('bulan', strval($request->bulan));
            $dt_hours = $dt_hours->where('bulan', strval($request->bulan));
        }
        if(!empty($request->tahun)){
            $dt_sig = $dt_sig->where('tahun', strval($request->tahun));
            $dt_non_sig = $dt_non_sig->where('tahun', strval($request->tahun));
            $dt_hours = $dt_hours->where('tahun', strval($request->tahun));
        }
        $dt_sig = $dt_sig->count();
        $dt_non_sig = $dt_non_sig->count();
        $dt_hours = $dt_hours->first();
        $query = [($dt_hours->jml_hours*$dt_sig),($dt_hours->jml_hours*$dt_non_sig)]; 
        $response = responseSuccess(__('messages.read-success'), $query);
        return response()->json($response);
    }

    /**
     * @OA\Get(
     *   tags={"SHE-Dashboard"},
     *   path="/api/dashboard_unsafe_action/",
     *   summary="Get dashboard_unsafe_action",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */

    public function dashboard_unsafe_action(Request $request){
        $query['bulan'] = [];
        $query['data1'] = [];
        $query['data2'] = [];
        $dt_open = SHEUnsafeAction::where('status', 'OPEN')
                    ->select('bulan',DB::raw("count(id) as jumlah"))
                    ->groupBy('bulan');
        $dt_close = SHEUnsafeAction::where('status', 'CLOSE')
                    ->select('bulan',DB::raw("count(id) as jumlah"))
                    ->groupBy('bulan');
        if(!empty($request->tahun)){
            $dt_open = $dt_open->where('tahun', strval($request->tahun));
            $dt_close = $dt_close->where('tahun', strval($request->tahun));
        }
        $dt_open = $dt_open->get();
        $dt_close = $dt_close->get();

        //mapping
        $bulan = $this->bulan();
        foreach ($bulan as $key => $value) {
            array_push($query['bulan'], $value['desc']);
            array_push($query['data1'], 0);
            array_push($query['data2'], 0);            
            foreach ($dt_open as $kk => $vv) {
                if($value['data'] == $vv->bulan){
                    $query['data1'][$key] = $vv->jumlah;
                }
            }
            foreach ($dt_close as $kk => $vv) {
                if($value['data'] == $vv->bulan){
                    $query['data2'][$key] = $vv->jumlah;
                }
            }
        }        
        $response = responseSuccess(__('messages.read-success'), $query);
        return response()->json($response);
    }

    /**
     * @OA\Get(
     *   tags={"SHE-Dashboard"},
     *   path="/api/dashboard_unsafe_condition/",
     *   summary="Get dashboard_unsafe_condition",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */

     public function dashboard_unsafe_condition(Request $request){        
        $query['bulan'] = [];
        $query['data1'] = [];
        $query['data2'] = [];
        $dt_open = SHEUnsafeCondition::where('status', 'OPEN')
                    ->select('bulan',DB::raw("count(id) as jumlah"))
                    ->groupBy('bulan');
        $dt_close = SHEUnsafeCondition::where('status', 'CLOSE')
                    ->select('bulan',DB::raw("count(id) as jumlah"))
                    ->groupBy('bulan');
        if(!empty($request->tahun)){
            $dt_open = $dt_open->where('tahun', strval($request->tahun));
            $dt_close = $dt_close->where('tahun', strval($request->tahun));
        }
        $dt_open = $dt_open->get();
        $dt_close = $dt_close->get();

        //mapping
        $bulan = $this->bulan();
        foreach ($bulan as $key => $value) {
            array_push($query['bulan'], $value['desc']);
            array_push($query['data1'], 0);
            array_push($query['data2'], 0);            
            foreach ($dt_open as $kk => $vv) {
                if($value['data'] == $vv->bulan){
                    $query['data1'][$key] = $vv->jumlah;
                }
            }
            foreach ($dt_close as $kk => $vv) {
                if($value['data'] == $vv->bulan){
                    $query['data2'][$key] = $vv->jumlah;
                }
            }
        }        
        $response = responseSuccess(__('messages.read-success'), $query);
        return response()->json($response);
    }

    /**
     * @OA\Get(
     *   tags={"SHE-Dashboard"},
     *   path="/api/dashboard_accident/",
     *   summary="Get dashboard_accident",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */

    public function dashboard_accident(Request $request){
        $query['bulan'] = [];
        $query['data1'] = [];
        $query['data2'] = [];
        $dt_open = SHEAccidentReport::where('status', 'OPEN')
                    ->select('tgl_doc',DB::raw("count(id) as jumlah"))
                    ->groupBy('tgl_doc');
        $dt_close = SHEAccidentReport::where('status', 'CLOSE')
                    ->select('tgl_doc',DB::raw("count(id) as jumlah"))
                    ->groupBy('tgl_doc');
        if(!empty($request->tahun)){
            $dt_open = $dt_open->whereYear('tgl_doc', $request->tahun);
            $dt_close = $dt_close->whereYear('tgl_doc', $request->tahun);
        }
        $dt_open = $dt_open->get();
        $dt_close = $dt_close->get();        
        //mapping
        $bulan = $this->bulan();
        foreach ($bulan as $key => $value) {
            array_push($query['bulan'], $value['desc']);
            array_push($query['data1'], 0);
            array_push($query['data2'], 0);            
            foreach ($dt_open as $kk => $vv) {
                $bulan = date('m', strtotime($vv->tgl_doc));
                if($value['data'] == $bulan){
                    $query['data1'][$key] = $vv->jumlah;
                }
            }
            foreach ($dt_close as $kk => $vv) {
                $bulan = date('m', strtotime($vv->tgl_doc));
                if($value['data'] == $bulan){
                    $query['data2'][$key] = $vv->jumlah;
                }
            }
        }        
        $response = responseSuccess(__('messages.read-success'), $query);
        return response()->json($response);
    }

    /**
     * @OA\Get(
     *   tags={"SHE-Dashboard"},
     *   path="/api/dashboard_accident_fire/",
     *   summary="Get dashboard_accident_fire",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */

    public function dashboard_accident_fire(Request $request){
        $query['bulan'] = [];
        $query['data1'] = [];
        $query['data2'] = [];
        $dt_open = SHEFireAccidentReport::where('status', 'OPEN')
                    ->select('tgl_doc',DB::raw("count(id) as jumlah"))
                    ->groupBy('tgl_doc');
        $dt_close = SHEFireAccidentReport::where('status', 'CLOSE')
                    ->select('tgl_doc',DB::raw("count(id) as jumlah"))
                    ->groupBy('tgl_doc');
        if(!empty($request->tahun)){
            $dt_open = $dt_open->whereYear('tgl_doc', $request->tahun);
            $dt_close = $dt_close->whereYear('tgl_doc', $request->tahun);
        }
        $dt_open = $dt_open->get();
        $dt_close = $dt_close->get();        
        //mapping
        $bulan = $this->bulan();
        foreach ($bulan as $key => $value) {
            array_push($query['bulan'], $value['desc']);
            array_push($query['data1'], 0);
            array_push($query['data2'], 0);            
            foreach ($dt_open as $kk => $vv) {
                $bulan = date('m', strtotime($vv->tgl_doc));
                if($value['data'] == $bulan){
                    $query['data1'][$key] = $vv->jumlah;
                }
            }
            foreach ($dt_close as $kk => $vv) {
                $bulan = date('m', strtotime($vv->tgl_doc));
                if($value['data'] == $bulan){
                    $query['data2'][$key] = $vv->jumlah;
                }
            }
        }        
        $response = responseSuccess(__('messages.read-success'), $query);
        return response()->json($response);
    }

    /**
     * @OA\Get(
     *   tags={"SHE-Dashboard"},
     *   path="/api/dashboard_safety_training/",
     *   summary="Get dashboard_safety_training",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */

    public function dashboard_safety_training(Request $request){
        $query['data'] = [];
        $query['label'] = [];
        $label = SHESafetyTraining::query();
        if(!empty($request->bulan) && $request->bulan != 'ALL'){
            $label = $label->whereMonth('tgl_training', $request->bulan);
        }
        if(!empty($request->tahun)){
            $label = $label->whereYear('tgl_training', $request->tahun);
        }
        $label = $label->get();
        foreach ($label as $key => $value) {
            array_push($query['label'], $value->judul_training);
            $dt_org = SHESafetyTrainingOrg::where('id_training', $value->id)->count();
            array_push($query['data'], $dt_org);
        }        
        $response = responseSuccess(__('messages.read-success'), $query);
        return response()->json($response);
    }

    function bulan($data = '')
    {        
        $dt_bulan = ['01','02','03','04','05','06','07','08','09','10','11','12']; 
        $dt_desc = ['Jan','Feb','Mar','Apr','May','June','July','Aug','Sep','Oct','Nov','Dec']; 
        $dt_desc_long = ['January','February','March','April','May','June','July','August','September','October','November','December']; 
        foreach ($dt_bulan as $key => $value) {
            if($data == $value){
                $bulan_dt['data'] = $value;
                $bulan_dt['desc'] = $dt_desc[$key];
                $bulan_dt['desc_long'] = $dt_desc_long[$key];    
                return $bulan_dt;
            }
            $bulan_arr[$key]['data'] = $value;
            $bulan_arr[$key]['desc'] = $dt_desc[$key];
            $bulan_arr[$key]['desc_long'] = $dt_desc_long[$key];
        }
        return $bulan_arr;
    }
}


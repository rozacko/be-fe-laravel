<?php

namespace App\Http\Controllers\SHESafetyPerm;
use App\Models\SHESafetyTraining;
use App\Models\SHESafetyTrainingOrg;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class SHESafetyTrainingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = new SHESafetyTraining();
        $this->model2 = new SHESafetyTrainingOrg();
    }

    /**
     * @OA\Get(
     *   tags={"SHE-SafetyTraining"},
     *   path="/api/safetytraining/",
     *   summary="Get safetytraining list",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */

    public function index(Request $request){
        $query = $this->model;
        if(!empty($request->bulan)){
            $query = $query->whereMonth('tgl_training', $request->bulan);
        }
        if(!empty($request->tahun)){
            $query = $query->whereYear('tgl_training', $request->tahun);
        }
        $model = Datatables::of($query)
                ->addIndexColumn()     
                ->addColumn('report', function($row){
                    $month = date("F", mktime(0, 0, 0, $row->bulan, 10));
                    $ret = $month.' '.$row->tahun;
                    return $ret;
                })
                ->addColumn('jml_peserta', function($row){
                    $ret = $this->model2->where('id_training',$row->id)->count();                    
                    return $ret;
                })
                ->addColumn('action', function($row){
                    $btn = '<a href="#" onclick="show('.$row->id.')" class="btn btn-primary btn-sm"><i class="fa fa-edit" style="padding-right: 0px; font-size:10;"></i></a> ';
                    $btn .= '<a href="#" onclick="btn_delete('.$row->id.')" class="btn btn-danger btn-sm"><i class="fa fa-trash" style="padding-right: 0px; font-size:10;"></i></a> ';
                    return $btn;
                })
                ->make(true)
                ->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);        
    } 

    /**
     * @OA\Post(
     *   tags={"SHE-SafetyTraining"},
     *   path="/api/safetytraining",
     *   summary="Create safetytraining",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Request"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"tgl_training","judul_training"},
     *       @OA\Property(property="tgl_training", type="string", format="date", example="2023-03-13"),
     *       @OA\Property(property="judul_training", type="string", format="text", example="Pelatihan K3"),
     *     )
     *   )
     * )
     */
    public function store(Request $request)
    {
        $data = $request->only(
            'tgl_training',
            'judul_training',            
        );

        $this->validate($request, [
            'tgl_training' => 'required',
            'judul_training' => 'required',            
        ]);

        DB::beginTransaction();
        try {
            $model = $this->model->create($data);
            DB::commit();
            $response = responseSuccess(__('messages.create-success'), $model);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *   tags={"SHE-SafetyTraining"},
     *   path="/api/safetytraining/{id}",
     *   summary="Detail SHE-SafetyTraining",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Ok"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function show($id)
    {
        $model = $this->model->where('id', $id)->firstOrFail();
        $response = responseSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    /**
     * @OA\Put(
     *   tags={"SHE-SafetyTraining"},
     *   path="/api/safetytraining/{id}",
     *   summary="SHE-SafetyTraining update",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"tgl_training","judul_training"},
     *       @OA\Property(property="tgl_training", type="string", format="date", example="2023-03-13"),
     *       @OA\Property(property="judul_training", type="string", format="text", example="Pelatihan K3"),
     *     )
     *   ),
     *   @OA\Response(response=200, description="Successfully updated"),
     *   @OA\Response(response=400, description="Bad request"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'tgl_training' => 'required',
            'judul_training' => 'required',            
        ]);
        $model = $this->model->where('id', $id)->firstOrFail();

        DB::beginTransaction();
        try {
            $model->tgl_training = $request->input('tgl_training');
            $model->judul_training = $request->input('judul_training');
            $model->save();
            DB::commit();
            $response = responseSuccess(__('messages.update-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.update-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Delete(
     *   tags={"SHE-SafetyTraining"},
     *   path="/api/safetytraining/{id}",
     *   summary="SHE-SafetyTraining destroy",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Successfully deleted"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function destroy($id)
    {        
        $model = $this->model->where('id', $id)->firstOrFail();
        DB::beginTransaction();
        try {
            $model->delete();
            $model2 = $this->model2->where('id_training', $id)->delete();
            DB::commit();
            $response = responseSuccess(__('messages.delete-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.delete-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}


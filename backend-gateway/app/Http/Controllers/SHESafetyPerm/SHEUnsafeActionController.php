<?php

namespace App\Http\Controllers\SHESafetyPerm;
use App\Models\SHEUnsafeAction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\SheUnsafeActionTemplate;
use App\Imports\ExcelImportsWithHeader;
use App\Traits\ValidationExcelImport;
use Illuminate\Support\Facades\Validator;

class SHEUnsafeActionController extends Controller
{
    use ValidationExcelImport;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = new SHEUnsafeAction();
    }

    /**
     * @OA\Get(
     *   tags={"SHE-UnsafeAction"},
     *   path="/api/unsafeaction/",
     *   summary="Get unsafeaction list",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */

    public function index(Request $request){
        $query = $this->model;
        if(!empty($request->bulan)){
            $query = $query->where('bulan', strval($request->bulan));
        }
        if(!empty($request->tahun)){
            $query = $query->where('tahun', strval($request->tahun));
        }
        if(!empty($request->status) && $request->status != 'ALL'){
            $query = $query->where('status', strval($request->status));
        }
        $model = Datatables::of($query)
                ->addIndexColumn() 
                ->addColumn('report', function($row){
                    $month = date("F", mktime(0, 0, 0, $row->bulan, 10));
                    $ret = $month.' '.$row->tahun;
                    return $ret;
                })                   
                ->addColumn('action', function($row){
                    $btn = '<a href="#" onclick="show('.$row->id.')" class="btn btn-primary btn-sm"><i class="fa fa-edit" style="padding-right: 0px; font-size:10;"></i></a> ';
                    $btn .= '<a href="#" onclick="btn_delete('.$row->id.')" class="btn btn-danger btn-sm"><i class="fa fa-trash" style="padding-right: 0px; font-size:10;"></i></a> ';
                    return $btn;
                })
                ->addColumn('files', function($row){                    
                    $btn = '';
                    if($row->file != ''){
                        $btn = ReadFiles($row->file);
                    }   
                    return $btn;
                })
                ->make(true)
                ->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);        
    } 

    /**
     * @OA\Post(
     *   tags={"SHE-UnsafeAction"},
     *   path="/api/unsafeaction",
     *   summary="Create unsafeaction",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Request"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"bulan","tahun","nm_perusahaan","uraian_temuan","lokasi_kejadian","tgl_temuan","jml_temuan","identifikasi","tindakan","status"},
     *       @OA\Property(property="bulan", type="string", format="text", example="01"),
     *       @OA\Property(property="tahun", type="string", format="text", example="2023"),     
     *       @OA\Property(property="nm_perusahaan", type="string", format="text", example="PT. BERDIKARI PUTRA PERKASA,6100007140,(ovh tuban 1)"),
     *       @OA\Property(property="uraian_temuan", type="string", format="text", example="1 PEKERJA TIDAK MEMAKAI FULL BODY HARNES"),
     *       @OA\Property(property="lokasi_kejadian", type="string", format="text", example="SEBELAH TIMUR 341 EP1"),
     *       @OA\Property(property="tgl_temuan", type="string", format="date", example="2023-03-13"),
     *       @OA\Property(property="file", type="string", format="text", example=""),
     *       @OA\Property(property="file_gdrive", type="string", format="text", example=""),
     *       @OA\Property(property="jml_temuan", type="number", format="float", example="10"),
     *       @OA\Property(property="pengawas", type="string", format="text", example="-"),
     *       @OA\Property(property="identifikasi", type="string", format="text", example="TERJATUH"),
     *       @OA\Property(property="tindakan", type="string", format="text", example="MENGGUNAKAN APD YANG BENAR SESUAI STANDART K3"),
     *       @OA\Property(property="status", type="string", format="text", example="CLOSED"),
     *       @OA\Property(property="keterangan", type="string", format="text", example="-"),
     *     )
     *   )
     * )
     */
    public function store(Request $request)
    {                 
        $data = $request->only(
            'bulan',
            'tahun',
            'nm_perusahaan',
            'uraian_temuan',
            'lokasi_kejadian',
            'tgl_temuan',
            'jml_temuan',
            'pengawas',
            'identifikasi',
            'tindakan',
            'keterangan',
            'file',
            'file_gdrive',
            'status'
        );

        $this->validate($request, [
            'bulan' => 'required',
            'tahun' => 'required',
            'nm_perusahaan' => 'required',
            'uraian_temuan' => 'required',
            'lokasi_kejadian' => 'required',
            'tgl_temuan' => 'required|date',
            'jml_temuan' => 'required',
            'identifikasi' => 'required',
            'tindakan' => 'required',
            'status' => 'required',
        ]);

        DB::beginTransaction();
        try {
            $model = $this->model->create($data);
            DB::commit();
            $response = responseSuccess(__('messages.create-success'), $model);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *   tags={"SHE-UnsafeAction"},
     *   path="/api/unsafeaction/{id}",
     *   summary="Detail SHE-UnsafeAction",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Ok"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function show($id)
    {
        $model = $this->model->where('id', $id)->firstOrFail();
        $response = responseSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    /**
     * @OA\Put(
     *   tags={"SHE-UnsafeAction"},
     *   path="/api/unsafeaction/{id}",
     *   summary="SHE-UnsafeAction update",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"bulan","tahun","nm_perusahaan","uraian_temuan","lokasi_kejadian","tgl_temuan","jml_temuan","identifikasi","tindakan","status"},
     *       @OA\Property(property="bulan", type="string", format="text", example="01"),
     *       @OA\Property(property="tahun", type="string", format="text", example="2023"),     
     *       @OA\Property(property="nm_perusahaan", type="string", format="text", example="PT. BERDIKARI PUTRA PERKASA,6100007140,(ovh tuban 1)"),
     *       @OA\Property(property="uraian_temuan", type="string", format="text", example="1 PEKERJA TIDAK MEMAKAI FULL BODY HARNES"),
     *       @OA\Property(property="lokasi_kejadian", type="string", format="text", example="SEBELAH TIMUR 341 EP1"),
     *       @OA\Property(property="tgl_temuan", type="string", format="date", example="2023-03-13"),
     *       @OA\Property(property="file", type="string", format="text", example=""),
     *       @OA\Property(property="file_gdrive", type="string", format="text", example=""),
     *       @OA\Property(property="jml_temuan", type="number", format="float", example="10"),
     *       @OA\Property(property="pengawas", type="string", format="text", example="-"),
     *       @OA\Property(property="identifikasi", type="string", format="text", example="TERJATUH"),
     *       @OA\Property(property="tindakan", type="string", format="text", example="MENGGUNAKAN APD YANG BENAR SESUAI STANDART K3"),
     *       @OA\Property(property="status", type="string", format="text", example="CLOSED"),
     *       @OA\Property(property="keterangan", type="string", format="text", example="-"),
     *     )
     *   ),
     *   @OA\Response(response=200, description="Successfully updated"),
     *   @OA\Response(response=400, description="Bad request"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'bulan' => 'required',
            'tahun' => 'required',
            'nm_perusahaan' => 'required',
            'uraian_temuan' => 'required',
            'lokasi_kejadian' => 'required',
            'tgl_temuan' => 'required|date',
            'jml_temuan' => 'required',
            'identifikasi' => 'required',
            'tindakan' => 'required',
            'status' => 'required',
        ]);
        $model = $this->model->where('id', $id)->firstOrFail();

        DB::beginTransaction();
        try {         
            if($model->file != '' && $model->file != $request->input('file')){
                $return = DeleteFiles($model->file);
            }
            $model->bulan = $request->input('bulan');
            $model->tahun = $request->input('tahun');
            $model->nm_perusahaan = $request->input('nm_perusahaan');
            $model->uraian_temuan = $request->input('uraian_temuan');
            $model->lokasi_kejadian = $request->input('lokasi_kejadian');
            $model->tgl_temuan = $request->input('tgl_temuan');
            $model->jml_temuan = $request->input('jml_temuan');
            $model->pengawas = $request->input('pengawas');
            $model->identifikasi = $request->input('identifikasi');
            $model->tindakan = $request->input('tindakan');
            $model->keterangan = $request->input('keterangan');            
            $model->file = $request->input('file');
            $model->file_gdrive = $request->input('file_gdrive');
            $model->status = $request->input('status');            
            $model->save();
            DB::commit();
            $response = responseSuccess(__('messages.update-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.update-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Delete(
     *   tags={"SHE-UnsafeAction"},
     *   path="/api/unsafeaction/{id}",
     *   summary="SHE-UnsafeAction destroy",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Successfully deleted"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function destroy($id)
    {        
        $model = $this->model->where('id', $id)->firstOrFail();
        DB::beginTransaction();
        try {
            if($model->file != '')
                $return = DeleteFiles($model->file);
            $model->delete();
            DB::commit();
            $response = responseSuccess(__('messages.delete-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.delete-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *   tags={"SHE-UnsafeAction"},
     *   path="/api/unsafeaction/template",
     *   summary="Get SHE-UnsafeAction Template",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="Ok"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */
    public function template(Request $request)
    {
        return Excel::download(new SheUnsafeActionTemplate(), 'SHE_Unsafe_Action.xlsx');
    }

    /**
     * @OA\Post(
     *   tags={"SHE-UnsafeAction"},
     *   path="/api/unsafeaction/import",
     *   summary="import SHE-UnsafeAction",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"fileupload"},
     *       @OA\Property(property="fileupload", type="file", format="text", example="unsafeaction.xlsx"),
     *     )
     *   )
     * )
     */
    public function import(Request $request)
    {
        $this->validate($request, [
            'bulan' => 'required',
            'tahun' => 'required',
            'upload_file' => 'required|mimes:xls,xlsx',
        ]);
        $file = $request->file('upload_file');
        $data = Excel::toArray(new ExcelImportsWithHeader, $file)[0];
        if (!$this->compareTemplate("unsafe-action", array_keys($data[0]))) {
            return response()->json(responseFail(__('messages.validation-template-fail'), ["template" => [__('messages.validation-template-fail')]]), 400)->throwResponse();
        }
        $dataExcel = collect($data);
        $dataMaps = [];
        $lineValidation = [
            'cvptunit_kerja' => 'required',
            'uraian_temuan' => 'required',
            'lokasi_kejadian' => 'required',
            'tanggal_temuan' => 'required|date',
            'jumlah_temuan' => 'required',
            'identifikasi_bahaya' => 'required',
            'tindakan' => 'required',
            'status' => 'required',
        ];
        
        foreach ($dataExcel as $key => $item) {            
            $this->validateImport($item, $lineValidation, ($key + 2));
            array_push($dataMaps, [
                'bulan' => $request->bulan,
                'tahun' => $request->tahun,
                'nm_perusahaan' => $item['cvptunit_kerja'],
                'uraian_temuan' => $item['uraian_temuan'],
                'lokasi_kejadian' => $item['lokasi_kejadian'],
                'tgl_temuan' => $item['tanggal_temuan'],
                'jml_temuan' => $item['jumlah_temuan'],
                'pengawas' => $item['pengawas'],
                'identifikasi' => $item['identifikasi_bahaya'],
                'tindakan' => $item['tindakan'],
                'keterangan' => $item['keterangan'],
                'status' => $item['status'],
                'created_by' => Auth::user()->id,
                'created_at' => now()
            ]);
        }

        DB::beginTransaction();
        try{
            SHEUnsafeAction::insert($dataMaps);
            DB::commit();
            $response = responseSuccess(__('messages.import-success'));
            return response()->json($response, Response::HTTP_CREATED);
        }
        catch(\Exception $ex){
            DB::rollBack();
            $response = responseFail(__('messages.import-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function upload_file(Request $request)
    {                  
        $data = $request->only(            
            'upload_file',
        );

        $this->validate($request, [            
            'upload_file' => 'required|mimes:jpeg,jpg,png,gif,svg|max:10240',
        ]);
        
        try {
            $return = StoreFiles($request, 'unsafe_action', 'she_safety');
            $response = responseSuccess(__('messages.upload-success'), $return);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.upload-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private function validateImport($data, array $rules, $line)
    {
        $messages = [
            'required' => __('validation.required'),
            'numeric'  => __('validation.numeric'),
            'exists'   => __('validation.exists'),
        ];

        $validator = Validator::make($data, $rules, $messages);

        if ($validator->fails()) {
            $response = responseFail(__('messages.validation-fail') . " " . __('messages.inline-fail') . " : " . $line, $validator->errors(), []);
            $return = response()->json($response, Response::HTTP_BAD_REQUEST, [], JSON_PRETTY_PRINT);
            $return->throwResponse();
        }
    }
}


<?php

namespace App\Http\Controllers\SHESafetyPerm;
use App\Models\SHEHydrant;
use App\Models\SHEHydrantFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\SheHydrantTemplate;
use App\Imports\ExcelImportsWithHeader;
use App\Traits\ValidationExcelImport;
use Illuminate\Support\Facades\Validator;

class SHEHydrantController extends Controller
{
    use ValidationExcelImport;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {        
        $this->model = new SHEHydrant();
        $this->modelfile = new SHEHydrantFile();
    }

    /**
     * @OA\Get(
     *   tags={"SHE-Hydrant"},
     *   path="/api/hydrant/",
     *   summary="Get hydrant list",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */

    public function index(Request $request){
        $query = $this->model;
        if(!empty($request->bulan)){
            $query = $query->where('bulan', strval($request->bulan));
        }
        if(!empty($request->tahun)){
            $query = $query->where('tahun', strval($request->tahun));
        }
        $model = Datatables::of($query)
                ->addIndexColumn() 
                ->addColumn('report', function($row){
                    $month = date("F", mktime(0, 0, 0, $row->bulan, 10));
                    $ret = $month.' '.$row->tahun;
                    return $ret;
                })                   
                ->addColumn('action', function($row){
                    $btn = '<a href="#" onclick="show('.$row->id.')" class="btn btn-primary btn-sm"><i class="fa fa-edit" style="padding-right: 0px; font-size:10;"></i></a> ';
                    $btn .= '<a href="#" onclick="btn_delete('.$row->id.')" class="btn btn-danger btn-sm"><i class="fa fa-trash" style="padding-right: 0px; font-size:10;"></i></a> ';
                    return $btn;
                })
                ->make(true)
                ->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);        
    } 

    /**
     * @OA\Post(
     *   tags={"SHE-Hydrant"},
     *   path="/api/hydrant",
     *   summary="Create hydrant",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Request"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"bulan","tahun","main_pump","lokasi","no_pilar","cek","p_body","v_kiri","v_atas","v_kanan","k_kiri","k_kanan","tk_kiri","tk_kanan","b_casing","b_hose","b_nozle","b_kunci","press_bar"},
     *       @OA\Property(property="bulan", type="string", format="text", example="01"),
     *       @OA\Property(property="tahun", type="string", format="text", example="2023"),     
     *       @OA\Property(property="main_pump", type="string", format="text", example="TB.1/2"),
     *       @OA\Property(property="lokasi", type="string", format="text", example="Depan kantor PMKC 1 - 2 "),
     *       @OA\Property(property="no_pilar", type="string", format="text", example="1"),
     *       @OA\Property(property="cek", type="string", format="text", example="1"),
     *       @OA\Property(property="p_body", type="string", format="text", example="V"),
     *       @OA\Property(property="p_ket", type="string", format="text", example=""),
     *       @OA\Property(property="v_kiri", type="string", format="text", example="V"),
     *       @OA\Property(property="v_atas", type="string", format="text", example="V"),
     *       @OA\Property(property="v_kanan", type="string", format="text", example="V"),
     *       @OA\Property(property="v_ket", type="string", format="text", example=""),
     *       @OA\Property(property="k_kiri", type="string", format="text", example="V"),
     *       @OA\Property(property="k_kanan", type="string", format="text", example="V"),
     *       @OA\Property(property="k_ket", type="string", format="text", example=""),
     *       @OA\Property(property="tk_kiri", type="string", format="text", example="V"),
     *       @OA\Property(property="tk_kanan", type="string", format="text", example="-"),
     *       @OA\Property(property="b_casing", type="string", format="text", example="-"),
     *       @OA\Property(property="b_hose", type="string", format="text", example="-"),
     *       @OA\Property(property="b_nozle", type="string", format="text", example="-"),
     *       @OA\Property(property="b_kunci", type="string", format="text", example="-"),
     *       @OA\Property(property="b_ket", type="string", format="text", example="-"),
     *       @OA\Property(property="press_bar", type="number", format="double", example="7"),
     *     )
     *   )
     * )
     */
    public function store(Request $request)
    {
        $data = $request->only(
            'bulan',
            'tahun',
            'main_pump',
            'lokasi',
            'no_pilar',
            'cek',
            'p_body',
            'p_ket',
            'v_kiri',
            'v_atas',
            'v_kanan',
            'v_ket',
            'k_kiri',
            'k_kanan',
            'k_ket',
            'tk_kiri',
            'tk_kanan',
            'tk_ket',
            'b_casing',
            'b_hose',
            'b_nozle',
            'b_kunci',
            'b_ket',
            'press_bar',
        );

        $this->validate($request, [
            'bulan' => 'required',
            'tahun' => 'required',
            'main_pump' => 'required',
            'lokasi' => 'required',
            'no_pilar' => 'required',
            'cek' => 'required',
            'p_body' => 'required',
            'v_kiri' => 'required',
            'v_atas' => 'required',
            'v_kanan' => 'required',
            'k_kiri' => 'required',
            'k_kanan' => 'required',
            'tk_kiri' => 'required',
            'tk_kanan' => 'required',
            'b_casing' => 'required',
            'b_hose' => 'required',
            'b_nozle' => 'required',
            'b_kunci' => 'required',
        ]);

        DB::beginTransaction();
        try {   
            $data['p_status'] = $request->p_body == 'K' ? 'KOTOR' : 'BERSIH';
            $valve = $request->v_kiri.$request->v_atas.$request->v_kanan;
            $data['v_status'] = substr_count($valve,"X") > 1 ? 'RUSAK' : 'BAIK';
            $kopling = $request->k_kiri.$request->k_kanan;
            $data['k_status'] = substr_count($kopling,"X") > 1 ? 'RUSAK' : 'BAIK';
            $data['pb_ket'] = $request->press_bar > 5 || $request->press_bar == NULL ? 'BAIK' : 'RUSAK';
            $data['status_kondisi'] = $request->press_bar > 5 || $request->press_bar == NULL ? 'OK' : 'RUSAK';
            $model = $this->model->create($data);
            DB::commit();
            $response = responseSuccess(__('messages.create-success'), $model);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *   tags={"SHE-Hydrant"},
     *   path="/api/hydrant/{id}",
     *   summary="Detail SHE-Hydrant",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Ok"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function show($id)
    {        
        $model = $this->model->where('id', $id)->firstOrFail();
        $response = responseSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    /**
     * @OA\Put(
     *   tags={"SHE-Hydrant"},
     *   path="/api/hydrant/{id}",
     *   summary="SHE-Hydrant update",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"bulan","tahun","main_pump","lokasi","no_pilar","cek","p_body","v_kiri","v_atas","v_kanan","k_kiri","k_kanan","tk_kiri","tk_kanan","b_casing","b_hose","b_nozle","b_kunci","press_bar"},
     *       @OA\Property(property="bulan", type="string", format="text", example="01"),
     *       @OA\Property(property="tahun", type="string", format="text", example="2023"),     
     *       @OA\Property(property="main_pump", type="string", format="text", example="TB.1/2"),
     *       @OA\Property(property="lokasi", type="string", format="text", example="Depan kantor PMKC 1 - 2 "),
     *       @OA\Property(property="no_pilar", type="string", format="text", example="1"),
     *       @OA\Property(property="cek", type="string", format="text", example="1"),
     *       @OA\Property(property="p_body", type="string", format="text", example="V"),
     *       @OA\Property(property="p_ket", type="string", format="text", example=""),
     *       @OA\Property(property="v_kiri", type="string", format="text", example="V"),
     *       @OA\Property(property="v_atas", type="string", format="text", example="V"),
     *       @OA\Property(property="v_kanan", type="string", format="text", example="V"),
     *       @OA\Property(property="v_ket", type="string", format="text", example=""),
     *       @OA\Property(property="k_kiri", type="string", format="text", example="V"),
     *       @OA\Property(property="k_kanan", type="string", format="text", example="V"),
     *       @OA\Property(property="k_ket", type="string", format="text", example=""),
     *       @OA\Property(property="tk_kiri", type="string", format="text", example="V"),
     *       @OA\Property(property="tk_kanan", type="string", format="text", example="-"),
     *       @OA\Property(property="b_casing", type="string", format="text", example="-"),
     *       @OA\Property(property="b_hose", type="string", format="text", example="-"),
     *       @OA\Property(property="b_nozle", type="string", format="text", example="-"),
     *       @OA\Property(property="b_kunci", type="string", format="text", example="-"),
     *       @OA\Property(property="b_ket", type="string", format="text", example="-"),
     *       @OA\Property(property="press_bar", type="number", format="double", example="7"),
     *     )
     *   ),
     *   @OA\Response(response=200, description="Successfully updated"),
     *   @OA\Response(response=400, description="Bad request"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'bulan' => 'required',
            'tahun' => 'required',
            'main_pump' => 'required',
            'lokasi' => 'required',
            'no_pilar' => 'required',
            'cek' => 'required',
            'p_body' => 'required',
            'v_kiri' => 'required',
            'v_atas' => 'required',
            'v_kanan' => 'required',
            'k_kiri' => 'required',
            'k_kanan' => 'required',
            'tk_kiri' => 'required',
            'tk_kanan' => 'required',
            'b_casing' => 'required',
            'b_hose' => 'required',
            'b_nozle' => 'required',
            'b_kunci' => 'required',
        ]);    
        $model = $this->model->where('id', $id)->firstOrFail();

        DB::beginTransaction();
        try {
            $model->bulan = $request->input('bulan');
            $model->tahun = $request->input('tahun');
            $model->main_pump = $request->input('main_pump');
            $model->lokasi = $request->input('lokasi');
            $model->no_pilar = $request->input('no_pilar');
            $model->cek = $request->input('cek');
            $model->p_body = $request->input('p_body');
            $model->p_ket = $request->input('p_ket');
            $model->v_kiri = $request->input('v_kiri');
            $model->v_atas = $request->input('v_atas');
            $model->v_kanan = $request->input('v_kanan');
            $model->v_ket = $request->input('v_ket');
            $model->k_kiri = $request->input('k_kiri');
            $model->k_kanan = $request->input('k_kanan');
            $model->k_ket = $request->input('k_ket');
            $model->tk_kiri = $request->input('tk_kiri');
            $model->tk_kanan = $request->input('tk_kanan');
            $model->tk_ket = $request->input('tk_ket');
            $model->b_casing = $request->input('b_casing');
            $model->b_hose = $request->input('b_hose');
            $model->b_nozle = $request->input('b_nozle');
            $model->b_kunci = $request->input('b_kunci');
            $model->b_ket = $request->input('b_ket');
            $model->press_bar = $request->input('press_bar');

            $model->p_status = $request->p_body == 'K' ? 'KOTOR' : 'BERSIH';
            $valve = $request->v_kiri.$request->v_atas.$request->v_kanan;
            $model->v_status = substr_count($valve,"X") > 1 ? 'RUSAK' : 'BAIK';
            $kopling = $request->k_kiri.$request->k_kanan;
            $model->k_status = substr_count($kopling,"X") > 1 ? 'RUSAK' : 'BAIK';
            $model->pb_ket = $request->press_bar > 5 || $request->press_bar == NULL ? 'BAIK' : 'RUSAK';
            $model->status_kondisi = $request->press_bar > 5 || $request->press_bar == NULL ? 'OK' : 'RUSAK';        
            $model->save();  
            DB::commit();
            $response = responseSuccess(__('messages.update-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.update-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Delete(
     *   tags={"SHE-Hydrant"},
     *   path="/api/hydrant/{id}",
     *   summary="SHE-Hydrant destroy",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Successfully deleted"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function destroy($id)
    {                
        $model = $this->model->where('id', $id)->firstOrFail();
        $modelfile = $this->modelfile->where('id_hydrant', $id)->get();
        DB::beginTransaction();
        try {
            foreach ($modelfile as $key => $value) {
                if($value->file != '')
                    $return = DeleteFiles($value->file);
            }
            $model->delete();
            $this->modelfile->where('id_hydrant', $id)->delete();
            DB::commit();
            $response = responseSuccess(__('messages.delete-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.delete-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Post(
     *   tags={"SHE-Hydrant"},
     *   path="/api/hydrant_file",
     *   summary="Create hydrant file",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Request"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"bulan","tahun","deskripsi","file"},
     *       @OA\Property(property="bulan", type="string", format="text", example="01"),
     *       @OA\Property(property="tahun", type="string", format="text", example="2023"),
     *       @OA\Property(property="deskripsi", type="string", format="text", example="File aaa"),
     *       @OA\Property(property="file", type="string", format="binary", example=""),          
     *     )
     *   )
     * )
     */

    public function store_file(Request $request)
    {
        $data = $request->only(            
            'id_hydrant',
            'deskripsi',            
            'file',            
        );

        $this->validate($request, [
            'id_hydrant' => 'required',
            'deskripsi' => 'required',
            'file' => 'required',
        ]);

        DB::beginTransaction();
        try {
            $model = $this->modelfile->create($data);
            DB::commit();
            $response = responseSuccess(__('messages.create-success'), $model);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *   tags={"SHE-Hydrant"},
     *   path="/api/hydrant_file/{id}",
     *   summary="List File SHE-Hydrant",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Ok"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */

    public function show_file(Request $request)
    {
        $query = $this->modelfile->where('id_hydrant', $request->id);
        $model = Datatables::of($query)
                ->addIndexColumn()           
                ->addColumn('action', function($row){
                    $btn = '<a href="#" onclick="btn_delete_doc('.$row->id.')" class="btn btn-danger btn-sm"><i class="fa fa-trash" style="padding-right: 0px; font-size:10;"></i></a> ';
                    return $btn;
                })
                ->addColumn('files', function($row){                    
                    $btn = '';
                    if($row->file != ''){
                        $btn = ReadFiles($row->file);
                    }   
                    return $btn;
                })
                ->make(true)
                ->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);   
    }


    /**
     * @OA\Delete(
     *   tags={"SHE-Hydrant"},
     *   path="/api/hydrant_file/{id}",
     *   summary="SHE-Hydrant destroy file",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Successfully deleted"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function destroy_file($id)
    {        
        $model = $this->modelfile->where('id', $id)->firstOrFail();
        DB::beginTransaction();
        try {
            if($model->file != '')
                $return = DeleteFiles($model->file);
            $model->delete();
            DB::commit();
            $response = responseSuccess(__('messages.delete-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.delete-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *   tags={"SHE-Hydrant"},
     *   path="/api/hydrant/template",
     *   summary="Get SHE-Hydrant Template",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="Ok"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */
    public function template(Request $request)
    {
        return Excel::download(new SheHydrantTemplate(), 'SHE_Hydrant.xlsx');
    }

    /**
     * @OA\Post(
     *   tags={"SHE-Hydrant"},
     *   path="/api/hydrant/import",
     *   summary="import SHE-Hydrant",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"fileupload"},
     *       @OA\Property(property="fileupload", type="file", format="text", example="hydrant.xlsx"),
     *     )
     *   )
     * )
     */
    public function import(Request $request)
    {
        $this->validate($request, [
            'bulan' => 'required',
            'tahun' => 'required',
            'upload_file' => 'required|mimes:xls,xlsx',
        ]);
        $file = $request->file('upload_file');
        $data = Excel::toArray(new ExcelImportsWithHeader, $file)[0];

        if (!$this->compareTemplate("hydrant", array_keys($data[0]))) {
            return response()->json(responseFail(__('messages.validation-template-fail'), ["template" => [__('messages.validation-template-fail')]]), 400)->throwResponse();
        }

        $dataExcel = collect($data);
        $dataMaps = [];
        $lineValidation = [
            'main_pump' => 'required',
            'lokasi' => 'required',           
            'no_pilar' => 'required',           
            'cek' => 'required',           
            'body_pilar' => 'required',
            'valve_kopling_kiri' => 'required',           
            'valve_kopling_atas' => 'required',           
            'valve_kopling_kanan' => 'required',           
            'copling_kiri' => 'required',           
            'copling_kanan' => 'required',           
            'tutup_kopling_kiri' => 'required',           
            'tutup_kopling_kanan' => 'required',           
            'box_casing' => 'required',           
            'box_hose' => 'required',           
            'box_nozle' => 'required',           
            'box_kunci' => 'required',           
            'press_bar' => 'required',           
        ];

        foreach ($dataExcel as $key => $item) {
            foreach ($item as $kk => $value) {
                if(!is_numeric($value))
                    $item[$kk] = strtoupper($value);
            }
            $this->validateImport($item, $lineValidation, ($key + 2));
            $valve = strtoupper($item['valve_kopling_kiri'].$item['valve_kopling_atas'].$item['valve_kopling_kanan']);
            $kopling = strtoupper($item['copling_kiri'].$item['copling_kanan']);
            array_push($dataMaps, [
                'bulan' => $request->bulan,
                'tahun' => $request->tahun,
                'main_pump' => $item['main_pump'],
                'lokasi' => $item['lokasi'],
                'no_pilar' => $item['no_pilar'],
                'cek' => $item['cek'],
                'p_body' => $item['body_pilar'],
                'p_ket' => $item['ket_pilar'],
                'v_kiri' => $item['valve_kopling_kiri'],
                'v_atas' => $item['valve_kopling_atas'],
                'v_kanan' => $item['valve_kopling_kanan'],
                'v_ket' => $item['ket_valve_kopling'],
                'k_kiri' => $item['copling_kiri'],
                'k_kanan' => $item['copling_kanan'],
                'k_ket' => $item['ket_copling'],
                'tk_kiri' => $item['tutup_kopling_kiri'],
                'tk_kanan' => $item['tutup_kopling_kanan'],
                'b_casing' => $item['box_casing'],
                'b_hose' => $item['box_hose'],
                'b_nozle' => $item['box_nozle'],
                'b_kunci' => $item['box_kunci'],
                'b_ket' => $item['ket_box'],
                'press_bar' => $item['press_bar'],
                'p_status' => $item['body_pilar'] == 'K' ? 'KOTOR' : 'BERSIH',
                'v_status' => substr_count($valve,"X") > 1 ? 'RUSAK' : 'BAIK',
                'k_status' => substr_count($kopling,"X") > 1 ? 'RUSAK' : 'BAIK',
                'pb_ket' => $item['press_bar'] > 5 || $item['press_bar'] == NULL ? 'BAIK' : 'RUSAK',
                'status_kondisi' => $item['press_bar'] > 5 || $item['press_bar'] == NULL ? 'OK' : 'RUSAK',
                'created_by' => Auth::user()->id,
                'created_at' => now()
            ]);
        }        
        DB::beginTransaction();
        try{
            SHEHydrant::insert($dataMaps);
            DB::commit();
            $response = responseSuccess(__('messages.import-success'));
            return response()->json($response, Response::HTTP_CREATED);
        }
        catch(\Exception $ex){
            DB::rollBack();
            $response = responseFail(__('messages.import-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function upload_file(Request $request)
    {                  
        $data = $request->only(            
            'upload_file',
        );

        $this->validate($request, [            
            'upload_file' => 'required|mimes:jpeg,jpg,png,gif,svg|max:10240',
        ]);
        
        try {
            $return = StoreFiles($request, 'hydrant', 'she_safety');
            $response = responseSuccess(__('messages.upload-success'), $return);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.upload-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private function validateImport($data, array $rules, $line)
    {
        $messages = [
            'required' => __('validation.required'),
            'numeric'  => __('validation.numeric'),
            'exists'   => __('validation.exists'),
        ];

        $validator = Validator::make($data, $rules, $messages);

        if ($validator->fails()) {
            $response = responseFail(__('messages.validation-fail') . " " . __('messages.inline-fail') . " : " . $line, $validator->errors(), []);
            $return = response()->json($response, Response::HTTP_BAD_REQUEST, [], JSON_PRETTY_PRINT);
            $return->throwResponse();
        }
    }
}


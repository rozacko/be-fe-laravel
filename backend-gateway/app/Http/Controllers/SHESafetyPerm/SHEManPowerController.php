<?php

namespace App\Http\Controllers\SHESafetyPerm;
use App\Models\SHEManPower;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\SheManPowerTemplate;
use App\Imports\ExcelImportsWithHeader;
use App\Traits\ValidationExcelImport;
use Illuminate\Support\Facades\Validator;

class SHEManPowerController extends Controller
{
    use ValidationExcelImport;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = new SHEManPower();
    }

    /**
     * @OA\Get(
     *   tags={"SHE-ManPower"},
     *   path="/api/manpower/",
     *   summary="Get manpower list",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */

    public function index(Request $request)
    {   
        $query = $this->model;
        if(!empty($request->bulan)){
            $query = $query->where('bulan', strval($request->bulan));
        }
        if(!empty($request->tahun)){
            $query = $query->where('tahun', strval($request->tahun));
        }
        if(!empty($request->status)){
            $query = $query->where('status', strval($request->status));
        }
        $model = Datatables::of($query)
                ->addIndexColumn()     
                ->addColumn('report', function($row){
                    $month = date("F", mktime(0, 0, 0, $row->bulan, 10));
                    $ret = $month.' '.$row->tahun;
                    return $ret;
                })               
                ->addColumn('action', function($row){
                    $btn = '<a href="#" onclick="show('.$row->id.')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> ';
                    $btn .= '<a href="#" onclick="btn_delete('.$row->id.')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a> ';
                    return $btn;
                })
                ->make(true)
                ->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);        
    } 

    /**
     * @OA\Post(
     *   tags={"SHE-ManPower"},
     *   path="/api/manpower",
     *   summary="Create manpower",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Request"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"bulan","tahun","nm_perusahaan","jml_orang"},
     *       @OA\Property(property="bulan", type="string", format="text", example="01"),
     *       @OA\Property(property="tahun", type="string", format="text", example="2023"),     
     *       @OA\Property(property="nm_perusahaan", type="string", format="text", example="PT. SEMEN INDONESIA LOGISTIK"),
     *       @OA\Property(property="jml_orang", type="number", format="float", example="10"),
     *       @OA\Property(property="status", type="string", format="text", example="PROJECT"),
     *       @OA\Property(property="keterangan", type="string", format="text", example="ADMIN"),
     *     )
     *   )
     * )
     */
    public function store(Request $request)
    {
        $data = $request->only(
            'bulan',
            'tahun',
            'nm_perusahaan',
            'jml_orang',
            'status',
            'keterangan',
        );

        $this->validate($request, [
            'bulan' => 'required',
            'tahun' => 'required',
            'nm_perusahaan' => 'required',
            'jml_orang' => 'required',
        ]);

        DB::beginTransaction();
        try {
            $model = $this->model->create($data);
            DB::commit();
            $response = responseSuccess(__('messages.create-success'), $model);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *   tags={"SHE-ManPower"},
     *   path="/api/manpower/{id}",
     *   summary="Detail SHE-ManPower",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Ok"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function show($id)
    {
        $model = $this->model->where('id', $id)->firstOrFail();
        $response = responseSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    /**
     * @OA\Put(
     *   tags={"SHE-ManPower"},
     *   path="/api/manpower/{id}",
     *   summary="SHE-ManPower update",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"bulan","tahun","nm_perusahaan","jml_orang"},
     *       @OA\Property(property="bulan", type="string", format="text", example="01"),
     *       @OA\Property(property="tahun", type="string", format="text", example="2023"),     
     *       @OA\Property(property="nm_perusahaan", type="string", format="text", example="PT. SEMEN INDONESIA LOGISTIK"),
     *       @OA\Property(property="jml_orang", type="number", format="float", example="10"),
     *       @OA\Property(property="status", type="string", format="text", example="PROJECT"),
     *       @OA\Property(property="keterangan", type="string", format="text", example="ADMIN"),
     *     )
     *   ),
     *   @OA\Response(response=200, description="Successfully updated"),
     *   @OA\Response(response=400, description="Bad request"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'bulan' => 'required',
            'tahun' => 'required',
            'nm_perusahaan' => 'required',
            'jml_orang' => 'required',
        ]);
        $model = $this->model->where('id', $id)->firstOrFail();

        DB::beginTransaction();
        try {
            $model->bulan = $request->input('bulan');
            $model->tahun = $request->input('tahun');            
            $model->nm_perusahaan = $request->input('nm_perusahaan');            
            $model->jml_orang = $request->input('jml_orang');            
            $model->status = $request->input('status');            
            $model->keterangan = $request->input('keterangan');            
            $model->save();
            DB::commit();
            $response = responseSuccess(__('messages.update-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.update-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Delete(
     *   tags={"SHE-ManPower"},
     *   path="/api/manpower/{id}",
     *   summary="SHE-ManPower destroy",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Successfully deleted"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function destroy($id)
    {        
        $model = $this->model->where('id', $id)->firstOrFail();
        DB::beginTransaction();
        try {
            $model->delete();
            DB::commit();
            $response = responseSuccess(__('messages.delete-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.delete-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *   tags={"SHE-ManPower"},
     *   path="/api/manpower/template",
     *   summary="Get SHE-ManPower Template",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="Ok"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */
    public function template(Request $request)
    {
        return Excel::download(new SheManPowerTemplate(), 'SHE_Man_Power.xlsx');
    }

    /**
     * @OA\Post(
     *   tags={"SHE-ManPower"},
     *   path="/api/manpower/import",
     *   summary="import SHE-ManPower",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"fileupload"},
     *       @OA\Property(property="fileupload", type="file", format="text", example="manpower.xlsx"),
     *     )
     *   )
     * )
     */
    
    public function import(Request $request)
    {
        $this->validate($request, [
            'bulan' => 'required',
            'tahun' => 'required',
            'upload_file' => 'required|mimes:xls,xlsx',
        ]);
        $file = $request->file('upload_file');
        $data = Excel::toArray(new ExcelImportsWithHeader, $file)[0];        
        if (!$this->compareTemplate("man-power", array_keys($data[0]))) {
            return response()->json(responseFail(__('messages.validation-template-fail'), ["template" => [__('messages.validation-template-fail')]]), 400)->throwResponse();
        }
        
        $dataExcel = collect($data);
        $dataMaps = [];
        $lineValidation = [
            'nama_perusahaan' => 'required',
            'jumlah_pekerja' => 'required',
            'status' => 'required',
            'keterangan' => 'required',            
        ];
        
        foreach ($dataExcel as $key => $item) {            
            $this->validateImport($item, $lineValidation, ($key + 2));
            array_push($dataMaps, [
                'nm_perusahaan' => $item['nama_perusahaan'],
                'jml_orang' => $item['jumlah_pekerja'],
                'status' => $item['status'],
                'keterangan' => $item['keterangan'],                
                'bulan' => $request->bulan,
                'tahun' => $request->tahun,
                'created_by' => Auth::user()->id,
                'created_at' => now()
            ]);
        }

        DB::beginTransaction();
        try{
            SHEManPower::insert($dataMaps);
            DB::commit();
            $response = responseSuccess(__('messages.import-success'));
            return response()->json($response, Response::HTTP_CREATED);
        }
        catch(\Exception $ex){
            DB::rollBack();
            $response = responseFail(__('messages.import-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private function validateImport($data, array $rules, $line)
    {
        $messages = [
            'required' => __('validation.required'),
            'numeric'  => __('validation.numeric'),
            'exists'   => __('validation.exists'),
        ];

        $validator = Validator::make($data, $rules, $messages);

        if ($validator->fails()) {
            $response = responseFail(__('messages.validation-fail') . " " . __('messages.inline-fail') . " : " . $line, $validator->errors(), []);
            $return = response()->json($response, Response::HTTP_BAD_REQUEST, [], JSON_PRETTY_PRINT);
            $return->throwResponse();
        }
    }
}


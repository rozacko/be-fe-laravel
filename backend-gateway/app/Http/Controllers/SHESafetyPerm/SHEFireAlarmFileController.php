<?php

namespace App\Http\Controllers\SHESafetyPerm;
use App\Models\SHEFireAlarmFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class SHEFireAlarmFileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = new SHEFireAlarmFile();
    }

    /**
     * @OA\Get(
     *   tags={"SHE-FireAlarmFile"},
     *   path="/api/firealarmfile/",
     *   summary="Get firealarmfile list",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */

    public function index(Request $request){                
        $bulan = substr($request->id,-2);
        $tahun = substr($request->id,0,4);
        $query = $this->model->where('bulan', $bulan)
                ->where('tahun', $tahun);
        $model = Datatables::of($query)
                ->addIndexColumn()           
                ->addColumn('action', function($row){
                    $btn = '<a href="#" onclick="btn_delete_doc('.$row->id.')" class="btn btn-danger btn-sm"><i class="fa fa-trash" style="padding-right: 0px; font-size:10;"></i></a> ';
                    return $btn;
                })
                ->addColumn('files', function($row){                    
                    $btn = '';
                    if($row->file != ''){
                        $btn = ReadFiles($row->file);
                    }   
                    return $btn;
                })
                ->make(true)
                ->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);   
    } 

    /**
     * @OA\Post(
     *   tags={"SHE-FireAlarmFile"},
     *   path="/api/firealarmfile",
     *   summary="Create firealarmfile",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Request"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"bulan","tahun","deskripsi","file"},
     *       @OA\Property(property="bulan", type="string", format="text", example="01"),
     *       @OA\Property(property="tahun", type="string", format="text", example="2023"),
     *       @OA\Property(property="deskripsi", type="string", format="text", example="File aaa"),
     *       @OA\Property(property="file", type="string", format="binary", example=""),          
     *     )
     *   )
     * )
     */
    public function store(Request $request)
    {
        $data = $request->only(                        
            'bulan',            
            'tahun',            
            'deskripsi',            
            'file',            
        );

        $this->validate($request, [
            'id' => 'required',
            'deskripsi' => 'required',            
            'file' => 'required',
        ]);

        DB::beginTransaction();
        try {
            $bulan = substr($request->id,-2);
            $tahun = substr($request->id,0,4);
            $data['bulan'] = $bulan;
            $data['tahun'] = $tahun;
            $data['deskripsi'] = $request->deskripsi;
            $data['file'] = $request->file;
            $model = $this->model->create($data);
            DB::commit();
            $response = responseSuccess(__('messages.create-success'), $model);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *   tags={"SHE-FireAlarmFile"},
     *   path="/api/firealarmfile/{id}",
     *   summary="Detail SHE-FireAlarmFile",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Ok"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function show($id)
    {
        $model = $this->model->where('id', $id)->firstOrFail();
        $response = responseSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    /**
     * @OA\Put(
     *   tags={"SHE-FireAlarmFile"},
     *   path="/api/firealarmfile/{id}",
     *   summary="SHE-FireAlarmFile update",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"bulan","tahun","deskripsi","file"},
     *       @OA\Property(property="bulan", type="string", format="text", example="01"),
     *       @OA\Property(property="tahun", type="string", format="text", example="2023"),
     *       @OA\Property(property="deskripsi", type="string", format="text", example="File aaa"),
     *       @OA\Property(property="file", type="string", format="binary", example=""),          
     *     )
     *   ),
     *   @OA\Response(response=200, description="Successfully updated"),
     *   @OA\Response(response=400, description="Bad request"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * @OA\Delete(
     *   tags={"SHE-FireAlarmFile"},
     *   path="/api/firealarmfile/{id}",
     *   summary="SHE-FireAlarmFile destroy",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Successfully deleted"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function destroy($id)
    {        
        $model = $this->model->where('id', $id)->firstOrFail();
        DB::beginTransaction();
        try {
            if($model->file != '')
                $return = DeleteFiles($model->file);
            $model->delete();
            DB::commit();
            $response = responseSuccess(__('messages.delete-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.delete-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function upload_file(Request $request)
    {                  
        $data = $request->only(            
            'upload_file',
        );

        $this->validate($request, [            
            'upload_file' => 'required|mimes:jpeg,jpg,png,gif,svg|max:10240',
        ]);
        
        try {
            $return = StoreFiles($request, 'apar', 'she_safety');
            $response = responseSuccess(__('messages.upload-success'), $return);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.upload-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}


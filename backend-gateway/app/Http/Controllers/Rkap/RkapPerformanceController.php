<?php

namespace App\Http\Controllers\Rkap;

use App\Exports\DownloadExcel;
use App\Http\Controllers\Controller;
use App\Imports\ExcelImportsWithHeader;
use App\Models\MPlant;
use App\Models\RkapPerformance;
use App\Models\TRkapPerformanceItems;
use App\Rules\PerformanceImportRules;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Traits\ValidationExcelImport;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class RkapPerformanceController extends Controller
{
    //
    use ValidationExcelImport;


     /**
     * @OA\Get(
     *   tags={"RKAP PERFORMANCE LIST DATATABLE"},
     *   path="/api/rkap/rkap-performance",
     *   summary="List Datatable RKAP Performance",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     * )
     */
    public function index(Request $request , $exported=false)
    {
        # code...
        $query= DB::table('ghopo.view_rkap_performance');

        if($request->filled('tahun')){
            $query = $query->where('tahun', $request->get('tahun'));
        }
        if($request->filled('plant_id')){
            $query = $query->where('id_plant', $request->get('plant_id'));
        }

        if($exported){
            return $query->select(
                "id_plant","plant_name","paramter_name","tahun","jan","feb","mar","apr","mei","jun","jul","ags","sep","okt","nov","des"
            )->get();
        }

        $model = DataTables::of($query)
                ->addIndexColumn()
                ->make(true)
                ->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);        
    }

     /**
     * @OA\Post(
     *   tags={"RKAP PERFORMANCE SHOW DATA"},
     *   path="/api/rkap/rkap-performance/{uuid}",
     *   summary="Show Data By UUID RKAP Performance",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     * )
     */
    public function show($id)
    {
        $this->isValidUuid($id);
        $request = new Request(['uuid'=> $id]);
        $this->validate($request, [
            'uuid' => 'required|exists:t_rkap_performance',
        ]);
        $model = DB::table('ghopo.view_rkap_performance')->where('uuid', $id)->first();
        $response = responseSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }


     /**
     * @OA\PUT(
     *   tags={"RKAP PERFORMANCE UPDATE"},
     *   path="/api/rkap/rkap-performance/{uuid}",
     *   summary="Update RKAP Performance",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       @OA\Property(property="jan", type="string", format="numeric", example=222),
     *       @OA\Property(property="feb", type="string", format="numeric", example=222),
     *       @OA\Property(property="mar", type="string", format="numeric", example=222),
     *       @OA\Property(property="apr", type="string", format="numeric", example=222),
     *       @OA\Property(property="mei", type="string", format="numeric", example=222),
     *       @OA\Property(property="jun", type="string", format="numeric", example=222),
     *       @OA\Property(property="jul", type="string", format="numeric", example=222),
     *       @OA\Property(property="ags", type="string", format="numeric", example=222),
     *       @OA\Property(property="sep", type="string", format="numeric", example=222),
     *       @OA\Property(property="okt", type="string", format="numeric", example=222),
     *       @OA\Property(property="nov", type="string", format="numeric", example=222),
     *       @OA\Property(property="des", type="string", format="numeric", example=222),
     *     )
     *   )
     * )
     */
    public function update($id, Request $request)
    {
        $this->isValidUuid($id);
        $request = $request->merge(['uuid'=> $id]);
        $request = new Request($request->only(["uuid","jan","feb","mar","apr","mei","jun","jul","ags","sep","okt","nov","des"]));
        $this->validate($request, [
            'uuid' => 'required|exists:t_rkap_performance',
            "jan" => 'required|numeric',
            "feb" => 'required|numeric',
            "mar" => 'required|numeric',
            "apr" => 'required|numeric',
            "mei" => 'required|numeric',
            "jun" => 'required|numeric',
            "jul" => 'required|numeric',
            "ags" => 'required|numeric',
            "sep" => 'required|numeric',
            "okt" => 'required|numeric',
            "nov" => 'required|numeric',
            "des" => 'required|numeric'
        ]);
        $data = $request->only(["jan","feb","mar","apr","mei","jun","jul","ags","sep","okt","nov","des"]);
        $header = RkapPerformance::where('uuid',$id)->first();

        DB::beginTransaction();
        try {
            $index = 0;
            foreach ($data as $key => $value) {
                # code...
                $index=$index+1;
                TRkapPerformanceItems::where('bulan', $index)->where('id_rkap_performance',$header->id)->update(['nilai_rkap'=>$value]);
            }
            DB::commit();
            $response = responseSuccess(__('messages.update-success'), []);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.update-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Post(
     *   tags={"RKAP PERFORMANCE IMPORT"},
     *   path="/api/rkap/rkap-performance-import",
     *   summary="Import RKAP Performance",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       @OA\Property(property="dokumentasi", type="file", format="file"),
     *     )
     *   )
     * )
     */
    public function import(Request $request)
    {
        # code...
        $this->validate($request, [
            'upload_file' => 'required|mimes:xls,xlsx',
        ]);

        $file = $request->file('upload_file');
        $data = Excel::toArray(new ExcelImportsWithHeader, $file)[0];

        if (!$this->compareTemplate("rkap-performance", array_keys($data[0]))) {
            return response()->json(responseFail(__('messages.validation-template-fail'), ["template" => [__('messages.validation-template-fail')]]), 400)->throwResponse();
        }

        $dataExcel = collect($data);

        $plants = MPlant::whereIn('kd_plant', $dataExcel->pluck('code_plant')->toArray())->get();

        $dataMaps = [];
        $months = ["januari", "februari", "maret", "april", "mei", "juni", "juli", "agustus", "september", "oktober", "november", "desember"];
        $lineValidation = [
            "parameters" => 'required|exists:m_parameter,id',
            "t_rkap_performance"=>[new PerformanceImportRules],
            "tahun" => 'required|numeric|digits:4',
            "januari" => 'required|numeric',
            "februari" => 'required|numeric',
            "maret" => 'required|numeric',
            "april" => 'required|numeric',
            "mei" => 'required|numeric',
            "juni" => 'required|numeric',
            "juli" => 'required|numeric',
            "agustus" => 'required|numeric',
            "september" => 'required|numeric',
            "oktober" => 'required|numeric',
            "november" => 'required|numeric',
            "desember" => 'required|numeric'
        ];
        // $idHeader = [];
        foreach ($dataExcel as $key => $item) {
            # code...
            $item['t_rkap_performance']=[
                'id_plant'=>$item['code_plant'],
                'tahun'=>$item['tahun'],
                'id_parameter'=>$item['parameters'],
                'line'=>($key+2)
            ];
            $this->validateImport($item, $lineValidation, ($key + 2));
            $header = RkapPerformance::where('id_plant',$item['code_plant'])
                    ->where('tahun',$item['tahun'])
                    ->where('id_parameter',$item['parameters'])->first();
            
            if(!$header){
                $id = RkapPerformance::insertGetId([
                    'id_plant'=>$item['code_plant'],
                    'tahun'=>$item['tahun'],
                    'id_parameter'=>$item['parameters'],
                    'created_by' => Auth::user()->uuid,
                    'created_at' => now()
                ]);
            }else{
                $id = $header->id;
            }
            foreach ($months as $i => $month) {
                # code...
                array_push($dataMaps, [
                    'id_rkap_performance' => $id,
                    'bulan' => ($i + 1),
                    'nilai_rkap' => $item[$month],
                    'created_by' => Auth::user()->uuid,
                    'created_at' => now()
                ]);
            }
        }

        DB::beginTransaction();
        try {
            TRkapPerformanceItems::insert($dataMaps);
            DB::commit();
            $response = responseSuccess(__('messages.create-success'), []);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *   tags={"RKAP PERFORMANCE / RKAP Plant Perfomance Download Excel "},
     *   path="/api/rkap/rkap-performance-download",
     *   summary="RKAP PERFORMANCE / RKAP Plant Perfomance Download Excel ",
     *   security={{"token": {}}},
     *      @OA\Parameter(
     *         name="tahun",
     *         in="query",
     *         description="Tahun Format YYYY Ex: 2023",
     *         @OA\Schema( format="date",type="string")
     *      ),
     *      @OA\Parameter(
     *         name="plant_id",
     *         in="query",
     *         description="id plant, bukan  kodeplant",
     *         @OA\Schema( format="integer",type="string", description="id plant, bukan  kodeplant" )
     *      ),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */
    public function download(Request $request)
    {
        $request = $request->merge(["length" => -1]);
        $data    = $this->index($request, true);
        $columns = ["Kode Plant", "Nama Plant", "paramter_name", "tahun", "jan", "feb", "mar", "apr", "mei", "jun", "jul", "ags", "sep", "okt", "nov", "des",];
        return Excel::download((new DownloadExcel($data, $columns)), "Rkap Performance.xlsx");
    }

    private function validateImport($data, array $rules, $line)
    {
        $messages = [
            'required' => __('validation.required'),
            'numeric'  => __('validation.numeric'),
            'exists'   => __('validation.exists'),
        ];

        $validator = Validator::make($data, $rules, $messages);

        if ($validator->fails()) {
            $response = responseFail(__('messages.validation-fail') . " " . __('messages.inline-fail') . " : " . $line, $validator->errors(), []);
            $return = response()->json($response, Response::HTTP_BAD_REQUEST, [], JSON_PRETTY_PRINT);
            $return->throwResponse();
        }
    }
}

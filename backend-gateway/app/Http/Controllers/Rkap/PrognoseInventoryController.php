<?php

namespace App\Http\Controllers\Rkap;

use App\Exports\DownloadExcel;
use App\Http\Controllers\Controller;
use App\Imports\ExcelImportsWithHeader;
use App\Models\TPrognoseInventory;
use App\Rules\DuplicateDataExceptUuid;
use App\Rules\DuplicateDataOnStore;
use App\Traits\ValidationExcelImport;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class PrognoseInventoryController extends Controller
{
    //
    use ValidationExcelImport;

    public function __construct()
    {
        $this->parameters = 'biaya-pemeliharaan';
        $this->months = '1,2,3,4,5,6,7,8,9,10,11,12';
    }

    /**
     * @OA\Get(
     *   tags={"Prognose Inventory Get List Table "},
     *   path="/api/rkap-prognose/prognose-inventory",
     *   summary="Prognose Inventory Get List Table",
     *   security={{"token": {}}},
     *      @OA\Parameter(
     *         name="parameters",
     *         in="query",
     *         description="in list : biaya-pemeliharaan",
     *         @OA\Schema( format="text",type="string" ,example="biaya-pemeliharaan")
     *      ),
     *      @OA\Parameter(
     *         name="tahun",
     *         in="query",
     *         description="YYYY",
     *         @OA\Schema( format="text",type="string" ,example="2023")
     *      ),
    *      @OA\Parameter(
     *         name="bulan",
     *         in="query",
     *         description="Numberic 1-12",
     *         @OA\Schema( format="text",type="string" ,example="1")
     *      ),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */
    public function index(Request $request, $exported=false)
    {
        # code...
        $query = TPrognoseInventory::query();

        if($request->filled('parameters')) {
            $query = $query->where('parameters', $request->get('parameters'));
        }
        if($request->filled('bulan')) {
            $query = $query->where('bulan', $request->get('bulan'));
        }
        if($request->filled('tahun')) {
            $query = $query->where('tahun', $request->get('tahun'));
        }

        if($exported){
            $query= $query->select('parameters','bulan','tahun','value as nilai');
        }

        $model = DataTables::of($query);
        if($exported)
        {
            return $model->getFilteredQuery()->get();
        }
        $model = $model->make(true)->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    /**
     * @OA\Get(
     *   tags={"Prognose Inventory Show Detail"},
     *   path="/api/rkap-prognose/prognose-inventory/{uuid}",
     *   summary="Prognose Inventory Show Detail",
     *   security={{"token": {}}},
     *      @OA\Parameter(
     *         name="uuid",
     *         in="path",
     *         description="uuid Prognose Inventory Detail",
     *         required=true,
     *         @OA\Schema( format="uuid",type="string" )
     *      ),
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */
    public function show($id)
    {
        $this->isValidUuid($id);
        $request = new Request(['uuid'=> $id]);
        $this->validate($request, [
            'uuid' => 'required|exists:t_prognose_inventory',
        ]);
        $model = TPrognoseInventory::where('uuid', $id)->firstorfail();
        $response = responseSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    /**
     * @OA\Post(
     *   tags={"Prognose Inventory Store Data "},
     *   path="/api/rkap-prognose/prognose-inventory",
     *   summary="Prognose Inventory Store Data",
     *   security={{"token": {}}},
     *    @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(property="parameters",type="string", description="inlist : biaya-pemeliharaan", example="biaya-pemeliharaan"),
     *                 @OA\Property(property="bulan",type="string", description="nomor bulan 1-12", example="1"),
     *                 @OA\Property(property="tahun",type="string", description="YYYY"),
     *                 @OA\Property(property="value",type="float", description="nilai rkap"),
     *             )
     *         ),
     *         description="Pada File Excel Format Jam Memakai angka 1-24",
     *     ),
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest")
     * )
     */
    public function store(Request $request)
    {
        # code...
        $attributes = $request->only(['parameters','bulan','tahun', 'value']);
        $rules = [
            'parameters' => 'required|in:'.$this->parameters,
            "tahun" => 'required|numeric|digits:4',
            'bulan' => 'required|numeric|in:'.$this->months,
            't_prognose_inventory' => ['required', new DuplicateDataOnStore]
        ];
        $request = $request->merge([
            't_prognose_inventory' => [
                'parameters' => @$request->get('parameters'),
                'bulan' => @$request->get('bulan'),
                'tahun' => @$request->get('tahun'),
            ]
        ]);

        $this->validate($request, $rules);
        $attributes['bulan'] = intval($request->get('bulan'));

        DB::beginTransaction();
        try {
            $attributes['created_by'] = Auth::user()->uuid;
            TPrognoseInventory::create($attributes);
            DB::commit();
            $response = responseSuccess(__('messages.create-success'), $attributes);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Put(
     *   tags={"Prognose Inventory Update "},
     *   path="/api/rkap-prognose/prognose-inventory/{uuid}",
     *   summary="Prognose Inventory Update",
     *   security={{"token": {}}},
     *      @OA\Parameter(
     *         name="uuid",
     *         in="path",
     *         description="uuid CLinker",
     *         required=true,
     *         @OA\Schema( format="uuid",type="string" )
     *      ),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"parameters","bulan","tahun","value"},
     *       @OA\Property(property="parameters", type="string", format="text", example="biaya-pemeliharaan"),
     *       @OA\Property(property="bulan", type="string", format="text", example="1"),
     *       @OA\Property(property="tahun", type="string", format="text", example="2023"),
     *       @OA\Property(property="value", type="float", format="text", example="500")
     *     ),
     *      description = "value berisi angka, parameters in list:biaya-pemeliharaan"
     *   ),
     *   @OA\Response(response=200, description="Successfully updated"),
     *   @OA\Response(response=400, description="Bad request"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */
    public function update($uuid,Request $request)
    {
        # code...
        $attributes = $request->only(['parameters','bulan','tahun', 'value']);
        $rules = [
            'parameters' => 'required|in:'.$this->parameters,
            "tahun" => 'required|numeric|digits:4',
            'bulan' => 'required|in:'.$this->months,
            't_prognose_inventory' => ['required', new DuplicateDataExceptUuid]
        ];
        $request = $request->merge([
            'uuid'=> $uuid,
            't_prognose_inventory' => [
                'uuid' => $uuid,
                'where' => [
                    'parameters' => @$request->get('parameters'),
                    'bulan' => @$request->get('bulan'),
                    'tahun' => @$request->get('tahun'),
                ]
            ]
        ]);
        $this->validate($request, $rules);
        $data = TPrognoseInventory::where('uuid', $uuid)->first();
        DB::beginTransaction();
        try {
            $attributes['updated_at'] = now();
            $attributes['updated_by'] = Auth::user()->uuid;
            $data->update($attributes);

            DB::commit();
            $response = responseSuccess(__('messages.update-success'), $attributes);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.update-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    

    /**
     * @OA\Delete(
     *   tags={"Prognose Inventory Delete Data "},
     *   path="/api/rkap-prognose/prognose-inventory/{uuid}",
     *   summary="Prognose Inventory Delete Data",
     *   security={{"token": {}}},
     *      @OA\Parameter(
     *         name="uuid",
     *         in="path",
     *         description="uuid CLinker",
     *         required=true,
     *         @OA\Schema( format="uuid",type="string" )
     *      ),
     *   @OA\Response(response=200, description="Successfully deleted"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function destroy($uuid)
    {        
        $this->isValidUuid($uuid);
        $request = new Request(['uuid'=> $uuid]);
        $this->validate($request, [
            'uuid' => 'required|exists:t_prognose_inventory',
        ]);
        $model = TPrognoseInventory::where('uuid', $uuid)->firstorfail();
        DB::beginTransaction();
        try {
            $model->delete();
            DB::commit();
            $response = responseSuccess(__('messages.delete-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.delete-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Post(
     *   tags={"Prognose Inventory Import File "},
     *   path="/api/rkap-prognose/prognose-inventory-import",
     *   summary="Prognose Inventory Import File",
     *   security={{"token": {}}},
     *    @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(property="upload_file",type="string", format="binary" , description="mimes:xls,xlsx")
     *             )
     *         ),
     *         description="Pada File Excel Format Jam Memakai angka 1-24",
     *     ),
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest")
     * )
     */
    public function import(Request $request)
    {
        # code...
        $this->validate($request, [
            'upload_file' => 'required|mimes:xls,xlsx',
        ]);

        $file = $request->file('upload_file');
        $data = Excel::toArray(new ExcelImportsWithHeader, $file)[0];

        if (!$this->compareTemplate("rkap-inventory", array_keys(@$data[0]))) {
            return response()->json(responseFail(__('messages.validation-template-fail'), ["template" => [__('messages.validation-template-fail')]]), 400)->throwResponse();
        }

        $lineValidation = [
            'parameters' => 'required|in:'.$this->parameters,
            "tahun" => 'required|numeric|digits:4',
            'bulan' => 'required|numeric|in:'.$this->months,
            't_prognose_inventory' => ['required', new DuplicateDataOnStore]
        ];

        DB::beginTransaction();
        foreach ($data as $key => $item) {
            # code...
            $item['t_prognose_inventory'] = [
                'parameters' => $item['parameters'],
                'bulan' => $item['bulan'],
                'tahun' => $item['tahun'],
            ];
            $this->validateImportBaseController($item, $lineValidation, ($key + 2)); 
            unset($item['t_prognose_inventory']);
            $item['created_by'] = Auth::user()->uuid;
            $item['created_at'] = now();
            TPrognoseInventory::create($item);
        }
        try {
            DB::commit();
            $response = responseSuccess(__('messages.create-success'), []);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *   tags={"Prognose Inventory Download Excel "},
     *   path="/api/rkap-prognose/prognose-inventory-download",
     *   summary="Prognose Inventory Download Excel",
     *   security={{"token": {}}},
      *      @OA\Parameter(
     *         name="parameters",
     *         in="query",
     *         description="in list : biaya-pemeliharaan",
     *         @OA\Schema( format="text",type="string" ,example="biaya-pemeliharaan")
     *      ),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */
    public function download(Request $request)
    {
        $request = $request->merge(["length" => -1]);
        $data    = $this->index($request, true);
        $columns = ["Parameter","Bulan", "Tahun", "Nilai"];
        return Excel::download((new DownloadExcel($data,$columns)), "Download Prognose Inventory.xlsx");
    }
}

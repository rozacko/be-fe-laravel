<?php

namespace App\Http\Controllers\DataKoreksi;

use App\Http\Controllers\Controller;
use App\Imports\ExcelImportsWithHeader;
use App\Models\TNotifikasi;
use App\Rules\ValidationFormulaExcelRules;
use App\Traits\ValidationExcelImport;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Yajra\DataTables\Facades\DataTables;

class NotifikasiController extends Controller
{
    //
    use ValidationExcelImport;
    /**
     * @OA\Get(
     *   tags={"Data Koreksi Notifikasi Get List Table "},
     *   path="/api/data-koreksi/notifikasi",
     *   summary="Data Koreksi Notifikasi Get List Table",
     *   security={{"token": {}}},
     *      @OA\Parameter(
     *         name="bulan",
     *         in="query",
     *         description="format M ex: (1,2,3,4,5,6,7,8,9,10,11,12)",
     *         @OA\Schema( format="date",type="string" )
     *      ),
     *      @OA\Parameter(
     *         name="tahun",
     *         in="query",
     *         description="numeric YYYY , ex : 2023",
     *         @OA\Schema( format="integer",type="string")
     *      ),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */
    //
    public function index(Request $request)
    {
        # code...
        $query= TNotifikasi::query();

        if($request->filled('tahun')){
            $query = $query->whereYear('created_on',$request->get('tahun'));
        }

        if($request->filled('bulan')){
            $query = $query->whereMonth('created_on',$request->get('bulan'));
        }

        $model = DataTables::of($query)
                ->make(true)
                ->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    /**
     * @OA\Get(
     *   tags={"Data Koreksi Notifikasi Show "},
     *   path="/api/data-koreksi/notifikasi/{uuid}",
     *   summary="Data Koreksi Notifikasi Show",
     *   security={{"token": {}}},
     *      @OA\Parameter(
     *         name="uuid",
     *         in="path",
     *         description="uuid Koreksi Notifikasi",
     *         required=true,
     *         @OA\Schema( format="uuid",type="string" )
     *      ),
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */
    public function show($id)
    {
        $this->isValidUuid($id);
        $request = new Request(['uuid'=> $id]);
        $this->validate($request, [
            'uuid' => 'required|exists:t_notifikasi',
        ]);
        $model = TNotifikasi::where('uuid', $id)->firstorfail();
        $response = responseSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }


    /**
     * @OA\Post(
     *   tags={"Data Koreksi Notifikasi Import File "},
     *   path="/api/data-koreksi/notifikasi-import",
     *   summary="Data Koreksi Notifikasi Import File",
     *   security={{"token": {}}},
     *    @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(property="upload_file",type="string", format="binary" , description="mimes:xls,xlsx"),
     *             )
     *         ),
     *         description="Pada File Excel Format Jam Memakai angka 1-24",
     *     ),
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest")
     * )
     */
    public function import(Request $request)
    {
        # code...
        
        $this->validate($request, [
            'upload_file' => 'required|mimes:xls,xlsx',
        ]);

        $file = $request->file('upload_file');
        $data = Excel::toArray(new ExcelImportsWithHeader, $file)[0];

        if (!$this->compareTemplate("data-koreksi-notifikasi", array_keys($data[0]))) {
            return response()->json(responseFail(__('messages.validation-template-fail'),["template" => [__('messages.validation-template-fail')]]), 400)->throwResponse();
        }

        $dataExcel = collect($data);
        $lineValidation = [
            "notif_date"=>  'required|date|date_format:Y-m-d',
            "notif_time"=>  'required|date_format:H:i:s',
            "system_status"=>  'required',
            "notification"=>  'required',
            "functional_loc"=>  'required',
            "description"=>  'required',
            "priority"=>  'required',
            "notification_type"=>  'required',
            "reported_by"=>  'required',
            "created_by"=>  'required',
            "changed_by"=>  'required',
            "main_work_ctr"=>  'required',
            "planner_group"=>  'required',
            "plant_section"=>  'required',
            "main_plant"=>  'required',
            "equipment"=>  'required',
            "cost_center"=>  'required',
            "reference_date"=>  'required|date|date_format:Y-m-d',
            "created_on"=>  'required|date|date_format:Y-m-d',
            "today"=>  ['required','date','date_format:Y-m-d', new ValidationFormulaExcelRules],
            "umur_notif"=>  ['required',new ValidationFormulaExcelRules],
            "notif_aging"=>  ['required',new ValidationFormulaExcelRules],
            "description_standar"=>  'required',
            "notif_convert_to_order"=>  ['required',new ValidationFormulaExcelRules],
            "notif_close"=>  ['required',new ValidationFormulaExcelRules],
            "klasifikasi_uk"=>  ['required',new ValidationFormulaExcelRules],
        ];
        

        DB::beginTransaction();
        foreach ($dataExcel as $key => $item) {
            $item["notif_date"] = ($item["notif_date"]==null || $item["notif_date"]== "") ? "" : \Carbon\Carbon::parse(Date::excelToDateTimeObject($item['notif_date']))->format('Y-m-d');
            $item["reference_date"] = ($item["reference_date"]==null || $item["reference_date"]== "") ? "" : \Carbon\Carbon::parse(Date::excelToDateTimeObject($item['reference_date']))->format('Y-m-d');
            $item["created_on"] = ($item["created_on"]==null || $item["created_on"]== "") ? "" : \Carbon\Carbon::parse(Date::excelToDateTimeObject($item['created_on']))->format('Y-m-d');
            $item["today"] = ($item["today"]==null || $item["today"]== "") ? "" : \Carbon\Carbon::parse(Date::excelToDateTimeObject($item['today']))->format('Y-m-d');
            TNotifikasi::create($item);
        }        
        try {
            DB::commit();
            $response = responseSuccess(__('messages.create-success'), []);
            return response()->json($response, Response::HTTP_OK);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * @OA\Delete(
     *   tags={"Data Koreksi Notifikasi Delete "},
     *   path="/api/data-koreksi/notifikasi/{uuid}",
     *   summary="Data Koreksi Notifikasi Koreksi Notifikasi Delete",
     *   security={{"token": {}}},
     *      @OA\Parameter(
     *         name="uuid",
     *         in="path",
     *         description="uuid Koreksi Notifikasi",
     *         required=true,
     *         @OA\Schema( format="uuid",type="string" )
     *      ),
     *   @OA\Response(response=200, description="Successfully deleted"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function destroy($uuid)
    {        
        $this->isValidUuid($uuid);
        $request = new Request(['uuid'=> $uuid]);
        $this->validate($request, [
            'uuid' => 'required|exists:t_notifikasi',
        ]);
        $model = TNotifikasi::where('uuid', $uuid)->firstorfail();
        DB::beginTransaction();
        try {
            $model->delete();
            DB::commit();
            $response = responseSuccess(__('messages.delete-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.delete-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}

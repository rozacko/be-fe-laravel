<?php

namespace App\Http\Controllers\DataKoreksi;

use App\Http\Controllers\Controller;
use App\Imports\ExcelImportsWithHeader;
use App\Models\TCogmReport;
use App\Rules\DuplicateDataExceptUuid;
use App\Rules\DuplicateDataImportRules;
use App\Traits\ValidationExcelImport;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class CogmController extends Controller
{
    //   
    use ValidationExcelImport;
    public function __construct()
    {
        $this->categoryBiaya = [
            'bahan-bakar',
            'bahan-baku-penolong',
            'listrik',
            'tenaga-kerja',
            'pemeliharaan',
            'deplesi-penyusutan-amortasi',
            'urusan-umum-adm',
            'pajak-asuransi'
        ];   
    }
    
    /**
     * @OA\Get(
     *   tags={"DATA DATA KOREKSI - DATA COGM LIST DTABLE"},
     *   path="/api/data-koreksi/cogm",
     *   summary="List DataTable Cogm",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="tahun", in="path", required=false, @OA\Schema( type="integer" )),
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */
    public function index(Request $request)
    {
        # code...
        $query= TCogmReport::query();

        if($request->filled('tahun')){
            $query = $query->where('tahun',$request->get('tahun'));
        }

        $model = DataTables::of($query)
                ->make(true)
                ->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    /**
     * @OA\Get(
     *   tags={"SHOW DATA KOREKSI - DATA COGM"},
     *   path="/api/data-koreksi/cogm/{uuid}",
     *   summary="Show Data Koreksi- Data COGM",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="uuid",  parameter="show", in="path", required=true, @OA\Schema( type="uuid" )),
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */
    public function show($uuid)
    {
        # code...
        $this->isValidUuid($uuid);
        $request = new Request(['uuid'=> $uuid]);
        $this->validate($request, [
            'uuid' => 'required|exists:t_cogm_report',
        ]);
        $model = TCogmReport::where('uuid', $uuid)->first();
        $response = responseSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    /**
     * @OA\Put(
     *   tags={"Update DATA KOREKSI - DATA COGM"},
     *   path="/api/data-koreksi/cogm/{uuid}",
     *   summary="Update Data Koreksi- Data COGM",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", description="The name that needs to be update", required=true, @OA\Schema( type="string" )),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"kode_opco","nama_opco","tahun","nama_biaya","bulan","total_biaya"},
     *       @OA\Property(property="kode_opco", type="string", format="text", example="7000"),
     *       @OA\Property(property="nama_opco", type="string", format="text", example="nama opco"),
     *       @OA\Property(property="tahun", type="integer", format="text", example="2023"),
     *       @OA\Property(property="nama_biaya", type="string", format="text", example="TBN1"),
     *       @OA\Property(property="bulan", type="integer", format="text", example="1"),
     *       @OA\Property(property="total_biaya", type="float", format="text", example="1000000"),
     *     )
     *   ),
     *   @OA\Response(response=200, description="Successfully updated"),
     *   @OA\Response(response=400, description="Bad request"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */
    public function update($uuid, Request $request)
    {
        
        $rules = [
            'uuid' => 'required|exists:t_cogm_report',
            "kode_opco"=>'required|in:7000',
            "nama_opco"=>'required',
            "tahun" => 'required|numeric|digits:4',
            "nama_biaya"=>'required|in:'.implode(',',$this->categoryBiaya),
            "bulan"=>'required|numeric|in:1,2,3,4,5,6,7,8,9,10,11,12',
            'total_biaya'=>'required|numeric|min:0',
            't_cogm_report' => ["required", new DuplicateDataExceptUuid]
        ];
        $request = $request->merge(['uuid'=> $uuid]);
        $this->validate($request, $rules);
        $data = TCogmReport::where('uuid', $uuid)->first();
        try {
            $data->update([
                'kode_opco'=>$request->get('kode_opco'),
                'nama_opco'=>$request->get('nama_opco'),
                'tahun'=>$request->get('tahun'),
                'nama_biaya'=>$request->get('nama_biaya'),
                'bulan'=>$request->get('bulan'),
                'total_biaya'=>$request->get('total_biaya'),
            ]);
            DB::commit();
            $response = responseSuccess(__('messages.update-success'), []);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.update-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }

    }

    /**
     * @OA\Delete(
     *   tags={"Delete DATA KOREKSI - DATA COGM Delete"},
     *   path="/api/data-koreksi/cogm/{uuid}",
     *   summary="Equipment Inspection destroy",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="uuid", in="path", description="The name that needs to be delete", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Successfully deleted"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function destroy($uuid)
    {        
        $this->isValidUuid($uuid);
        $request = new Request(['uuid'=> $uuid]);
        $this->validate($request, [
            'uuid' => 'required|exists:t_cogm_report',
        ]);
        $model = TCogmReport::where('uuid', $uuid)->first();
        DB::beginTransaction();
        try {
            $model->delete();
            DB::commit();
            $response = responseSuccess(__('messages.delete-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.delete-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Post(
     *   tags={"IMPORT DATA KOREKSI - DATA COGM IMPORT"},
     *   path="/api/data-koreksi/cogm-import",
     *   summary="Import Data Koreksi - Data Cogm",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       @OA\Property(property="upload_file", type="file", format="file"),
     *     )
     *   )
     * )
     */
    public function import(Request $request)
    {
        # code...
        
        $this->validate($request, [
            'upload_file' => 'required|mimes:xls,xlsx',
        ]);

        $file = $request->file('upload_file');
        $data = Excel::toArray(new ExcelImportsWithHeader, $file)[0];


        if (!$this->compareTemplate("cogm-report", array_keys($data[0]))) {
            return response()->json(responseFail(__('messages.validation-template-fail'),["template" => [__('messages.validation-template-fail')]]), 400)->throwResponse();
        }

        $dataExcel = collect($data);

        $dataMaps = [];
        $lineValidation = [
            "kode_opco"=>'required|in:7000',
            "nama_opco"=>'required',
            "tahun" => 'required|numeric|digits:4',
            "nama_biaya"=>'required|in:'.implode(',',$this->categoryBiaya),
            "bulan"=>'required|numeric|in:1,2,3,4,5,6,7,8,9,10,11,12',
            'total_biaya'=>'required|numeric|min:0',
            't_cogm_report' => ["required", new DuplicateDataImportRules]
        ];
        

        DB::beginTransaction();
        foreach ($dataExcel as $key => $item) {
            # code...
            $item['t_cogm_report'] = [
                'kode_opco'=> $item['kode_opco'],
                'tahun'=> $item['tahun'],
                'nama_biaya'=> $item['nama_biaya'],
                'bulan'=> $item['bulan'],
                'line'=>($key+2)
            ];
            $this->validateImport($item, $lineValidation, ($key + 2));
            unset($item['t_cogm_report']);
            unset($item['nama_opco']);
            TCogmReport::create($item);
        }        
        try {
            DB::commit();
            $response = responseSuccess(__('messages.create-success'), []);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    
    private function validateImport($data, array $rules, $line)
    {
        $messages = [
            'required' => __('validation.required'),
            'numeric'  => __('validation.numeric'),
            'exists'   => __('validation.exists'),
        ];

        $validator = Validator::make($data, $rules, $messages);

        if ($validator->fails()) {
            $response = responseFail(__('messages.validation-fail') . " " . __('messages.inline-fail') . " : " . $line, $validator->errors(), []);
            $return = response()->json($response, Response::HTTP_BAD_REQUEST, [], JSON_PRETTY_PRINT);
            $return->throwResponse();
        }
    }
}

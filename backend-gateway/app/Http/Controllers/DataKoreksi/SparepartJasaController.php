<?php

namespace App\Http\Controllers\DataKoreksi;

use App\Http\Controllers\Controller;
use App\Imports\ExcelImportsWithHeader;
use App\Models\MPlant;
use App\Models\TSparepartJasa;
use App\Rules\ValidationFormulaExcelRules;
use App\Traits\ValidationExcelImport;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Yajra\DataTables\Facades\DataTables;

class SparepartJasaController extends Controller
{
    //
    use ValidationExcelImport;
    /**
     * @OA\Get(
     *   tags={"Data Koreksi Sparepart Jasa Get List Table "},
     *   path="/api/data-koreksi/sparepart-jasa",
     *   summary="Data Koreksi Sparepart Jasa Get List Table",
     *   security={{"token": {}}},
     *      @OA\Parameter(
     *         name="bulan",
     *         in="query",
     *         description="format M ex: (1,2,3,4,5,6,7,8,9,10,11,12)",
     *         @OA\Schema( format="date",type="string" )
     *      ),
     *      @OA\Parameter(
     *         name="tahun",
     *         in="query",
     *         description="numeric YYYY , ex : 2023",
     *         @OA\Schema( format="integer",type="string")
     *      ),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */
    //
    public function index(Request $request)
    {
        # code...
        $query= TSparepartJasa::query();

        if($request->filled('tahun')){
            $query = $query->whereYear('created_on',$request->get('tahun'));
        }

        if($request->filled('bulan')){
            $query = $query->whereMonth('created_on',$request->get('bulan'));
        }

        $model = DataTables::of($query)
                ->make(true)
                ->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    /**
     * @OA\Get(
     *   tags={"Data Koreksi Sparepart Jasa Show "},
     *   path="/api/data-koreksi/sparepart-jasa/{uuid}",
     *   summary="Data Koreksi Sparepart Jasa Show",
     *   security={{"token": {}}},
     *      @OA\Parameter(
     *         name="uuid",
     *         in="path",
     *         description="uuid Koreksi Sparepart Jasa",
     *         required=true,
     *         @OA\Schema( format="uuid",type="string" )
     *      ),
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */
    public function show($id)
    {
        $this->isValidUuid($id);
        $request = new Request(['uuid'=> $id]);
        $this->validate($request, [
            'uuid' => 'required|exists:t_sparepart_jasa',
        ]);
        $model = TSparepartJasa::where('uuid', $id)->firstorfail();
        $response = responseSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }


    /**
     * @OA\Post(
     *   tags={"Data Koreksi Sparepart Jasa Import File "},
     *   path="/api/data-koreksi/sparepart-jasa-import",
     *   summary="Data Koreksi Sparepart Jasa Import File",
     *   security={{"token": {}}},
     *    @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(property="upload_file",type="string", format="binary" , description="mimes:xls,xlsx"),
     *             )
     *         ),
     *         description="Pada File Excel Format Jam Memakai angka 1-24",
     *     ),
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest")
     * )
     */
    public function import(Request $request)
    {
        # code...
        
        $this->validate($request, [
            'upload_file' => 'required|mimes:xls,xlsx',
        ]);

        $file = $request->file('upload_file');
        $data = Excel::toArray(new ExcelImportsWithHeader, $file)[0];
        // return response()->json($data[0]);

        if (!$this->compareTemplate("data-koreksi-sparepart-jasa", array_keys($data[0]))) {
            return response()->json(responseFail(__('messages.validation-template-fail'),["template" => [__('messages.validation-template-fail')]]), 400)->throwResponse();
        }

        $dataExcel = collect($data);
        $lineValidation = [
            'tanggal' => 'required|date|date_format:Y-m-d',
            'pr' => 'required',
            'pekerjaan' => 'required',
            'estimator' => 'required', 
            'nilai' => 'nullable|numeric',
            'engineering_tanggal' => 'nullable|date|date_format:Y-m-d',
            'rfq_tanggal' => 'nullable|date|date_format:Y-m-d',
            'po_tanggal' => 'nullable|date|date_format:Y-m-d',
            'kontrak' => 'nullable|numeric',
            'start_date' => 'nullable|date|date_format:Y-m-d',
            'finish_date' => 'nullable|date|date_format:Y-m-d',
            'pr_ke_estimator' => 'nullable|date|date_format:Y-m-d',
            'app_p_sawab' => 'nullable|date|date_format:Y-m-d',
            'app_p_didit' => 'nullable|date|date_format:Y-m-d',
            'app_p_zaini' => 'nullable|date|date_format:Y-m-d',
        ];
        
        $plants = MPlant::get();
        DB::beginTransaction();
        foreach ($dataExcel as $key => $item) {
            $plant = $plants->where('kd_plant', $item['kode_plant'])->first();
            $item['kode_plant'] = ($plant) ? $plant->id :  $item['kode_plant'];
            $item['nama_plant'] = ($plant) ? $plant->nm_plant :  $item['nama_plant'];
            $item['tanggal']  =($item["tanggal"]==null || $item["tanggal"]== "") ? null : \Carbon\Carbon::parse(Date::excelToDateTimeObject($item['tanggal']))->format('Y-m-d');
            $item['engineering_tanggal']  =($item["engineering_tanggal"]==null || $item["engineering_tanggal"]== "") ? null : \Carbon\Carbon::parse(Date::excelToDateTimeObject($item['engineering_tanggal']))->format('Y-m-d');
            $item['rfq_tanggal']  =($item["rfq_tanggal"]==null || $item["rfq_tanggal"]== "") ? null : \Carbon\Carbon::parse(Date::excelToDateTimeObject($item['rfq_tanggal']))->format('Y-m-d');
            $item['start_date']  =($item["start_date"]==null || $item["start_date"]== "") ? null : \Carbon\Carbon::parse(Date::excelToDateTimeObject($item['start_date']))->format('Y-m-d');
            $item['finish_date']  =($item["finish_date"]==null || $item["finish_date"]== "") ? null : \Carbon\Carbon::parse(Date::excelToDateTimeObject($item['finish_date']))->format('Y-m-d');
            $item['pr_ke_estimator']  =($item["pr_ke_estimator"]==null || $item["pr_ke_estimator"]== "") ? null : \Carbon\Carbon::parse(Date::excelToDateTimeObject($item['pr_ke_estimator']))->format('Y-m-d');
            $item['app_p_sawab']  =($item["app_p_sawab"]==null || $item["app_p_sawab"]== "") ? null : \Carbon\Carbon::parse(Date::excelToDateTimeObject($item['app_p_sawab']))->format('Y-m-d');
            $item['app_p_didit']  =($item["app_p_didit"]==null || $item["app_p_didit"]== "") ? null : \Carbon\Carbon::parse(Date::excelToDateTimeObject($item['app_p_didit']))->format('Y-m-d');
            $item['app_p_zaini']  =($item["app_p_zaini"]==null || $item["app_p_zaini"]== "") ? null : \Carbon\Carbon::parse(Date::excelToDateTimeObject($item['app_p_zaini']))->format('Y-m-d');            
            TSparepartJasa::create($item);
        }        
        try {
            DB::commit();
            $response = responseSuccess(__('messages.create-success'), []);
            return response()->json($response, Response::HTTP_OK);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * @OA\Delete(
     *   tags={"Data Koreksi Sparepart Jasa Delete "},
     *   path="/api/data-koreksi/sparepart-jasa/{uuid}",
     *   summary="Data Koreksi Sparepart Jasa Koreksi Sparepart Jasa Delete",
     *   security={{"token": {}}},
     *      @OA\Parameter(
     *         name="uuid",
     *         in="path",
     *         description="uuid Koreksi Sparepart Jasa",
     *         required=true,
     *         @OA\Schema( format="uuid",type="string" )
     *      ),
     *   @OA\Response(response=200, description="Successfully deleted"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function destroy($uuid)
    {        
        $this->isValidUuid($uuid);
        $request = new Request(['uuid'=> $uuid]);
        $this->validate($request, [
            'uuid' => 'required|exists:t_sparepart_jasa',
        ]);
        $model = TSparepartJasa::where('uuid', $uuid)->firstorfail();
        DB::beginTransaction();
        try {
            $model->delete();
            DB::commit();
            $response = responseSuccess(__('messages.delete-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.delete-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}

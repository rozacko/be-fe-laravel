<?php

namespace App\Http\Controllers\DataKoreksi;

use App\Http\Controllers\Controller;
use App\Imports\ExcelImportsWithHeader;
use App\Models\TOrderTask;
use App\Rules\ValidationFormulaExcelRules;
use App\Traits\ValidationExcelImport;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class OrderTaskController extends Controller
{
    use ValidationExcelImport;
    /**
     * @OA\Get(
     *   tags={"Data Koreksi Order Task Get List Table "},
     *   path="/api/data-koreksi/order-task",
     *   summary="Data Koreksi Order Task Get List Table",
     *   security={{"token": {}}},
     *      @OA\Parameter(
     *         name="bulan",
     *         in="query",
     *         description="format M ex: (1,2,3,4,5,6,7,8,9,10,11,12)",
     *         @OA\Schema( format="date",type="string" )
     *      ),
     *      @OA\Parameter(
     *         name="tahun",
     *         in="query",
     *         description="numeric YYYY , ex : 2023",
     *         @OA\Schema( format="integer",type="string")
     *      ),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */
    //
    public function index(Request $request)
    {
        # code...
        $query= TOrderTask::query();

        if($request->filled('tahun')){
            $query = $query->whereYear('created_on',$request->get('tahun'));
        }

        if($request->filled('bulan')){
            $query = $query->whereMonth('created_on',$request->get('bulan'));
        }

        $model = DataTables::of($query)
                ->make(true)
                ->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    /**
     * @OA\Get(
     *   tags={"Data Koreksi Order Task Show "},
     *   path="/api/data-koreksi/order-task/{uuid}",
     *   summary="Data Koreksi Order Task Show",
     *   security={{"token": {}}},
     *      @OA\Parameter(
     *         name="uuid",
     *         in="path",
     *         description="uuid Koreksi Order Task",
     *         required=true,
     *         @OA\Schema( format="uuid",type="string" )
     *      ),
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */
    public function show($id)
    {
        $this->isValidUuid($id);
        $request = new Request(['uuid'=> $id]);
        $this->validate($request, [
            'uuid' => 'required|exists:t_order_task',
        ]);
        $model = TOrderTask::where('uuid', $id)->firstorfail();
        $response = responseSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }


    /**
     * @OA\Post(
     *   tags={"Data Koreksi Order Task Import File "},
     *   path="/api/data-koreksi/order-task-import",
     *   summary="Data Koreksi Order Task Import File",
     *   security={{"token": {}}},
     *    @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(property="upload_file",type="string", format="binary" , description="mimes:xls,xlsx"),
     *             )
     *         ),
     *         description="Pada File Excel Format Jam Memakai angka 1-24",
     *     ),
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest")
     * )
     */
    public function import(Request $request)
    {
        # code...
        
        $this->validate($request, [
            'upload_file' => 'required|mimes:xls,xlsx',
        ]);

        $file = $request->file('upload_file');
        $data = Excel::toArray(new ExcelImportsWithHeader, $file)[0];

        if (!$this->compareTemplate("order-task", array_keys($data[0]))) {
            return response()->json(responseFail(__('messages.validation-template-fail'),["template" => [__('messages.validation-template-fail')]]), 400)->throwResponse();
        }

        $dataExcel = collect($data);
        $lineValidation = [
            "order_type"=>'required',
            "order"=>'required',
            "description"=>'required',
            "system_status" => [new ValidationFormulaExcelRules],
            "total_act_cost"=>'required',
            "tanggal_actual"=>'required|date|date_format:Y-m-d',
            "entered_by"=>'required',
            "planner_group"=>'required',
            "plant_section"=>'required',
            "res_cost_center"=>'required',
            "planning_plant"=>'required',
            "functional_loc"=>'required',
            "cost_center"=>'required',
            "created_on"=>'required|date|date_format:Y-m-d',
            "user_status"=>'required',
            "basic_start_date"=>'required|date|date_format:Y-m-d',
            "basic_fin_date"=>'required|date|date_format:Y-m-d',
            "status_order"=>['required',new ValidationFormulaExcelRules],
            "reason"=>['required',new ValidationFormulaExcelRules],
            "description_uk"=>['required',new ValidationFormulaExcelRules],
            "sub_area_proses"=>['required',new ValidationFormulaExcelRules],
            "plant"=>['required',new ValidationFormulaExcelRules],
            "area_proses"=>['required',new ValidationFormulaExcelRules],
            "bulan"=>['required', new ValidationFormulaExcelRules],
            "kode_area_proses"=>['required',new ValidationFormulaExcelRules],
            "tahun"=>['required',new ValidationFormulaExcelRules],
        ];
        

        DB::beginTransaction();
        foreach ($dataExcel as $key => $item) {
            $item['created_on'] = ($item["created_on"]==null || $item["created_on"]== "") ? "" : \Carbon\Carbon::parse(Date::excelToDateTimeObject($item['created_on']))->format('Y-m-d');
            $item["basic_start_date"] = ($item["basic_start_date"]==null || $item["basic_start_date"]== "") ? "" : \Carbon\Carbon::parse(Date::excelToDateTimeObject($item['basic_start_date']))->format('Y-m-d');
            $item["basic_fin_date"] = ($item["basic_fin_date"]==null || $item["basic_fin_date"]== "") ? "" : \Carbon\Carbon::parse(Date::excelToDateTimeObject($item['basic_fin_date']))->format('Y-m-d');
            $item["tanggal_actual"] = ($item["tanggal_actual"]==null || $item["tanggal_actual"]== "") ? "" : \Carbon\Carbon::parse(Date::excelToDateTimeObject($item['tanggal_actual']))->format('Y-m-d');
            $this->validateImportBaseController($item, $lineValidation, ($key + 2));
            TOrderTask::create($item);
        }        
        try {
            DB::commit();
            $response = responseSuccess(__('messages.create-success'), []);
            return response()->json($response, Response::HTTP_OK);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * @OA\Delete(
     *   tags={"Data Koreksi Order Task Delete "},
     *   path="/api/data-koreksi/order-task/{uuid}",
     *   summary="Data Koreksi Order Task Koreksi Order Task Delete",
     *   security={{"token": {}}},
     *      @OA\Parameter(
     *         name="uuid",
     *         in="path",
     *         description="uuid Koreksi Order Task",
     *         required=true,
     *         @OA\Schema( format="uuid",type="string" )
     *      ),
     *   @OA\Response(response=200, description="Successfully deleted"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function destroy($uuid)
    {        
        $this->isValidUuid($uuid);
        $request = new Request(['uuid'=> $uuid]);
        $this->validate($request, [
            'uuid' => 'required|exists:t_order_task',
        ]);
        $model = TOrderTask::where('uuid', $uuid)->firstorfail();
        DB::beginTransaction();
        try {
            $model->delete();
            DB::commit();
            $response = responseSuccess(__('messages.delete-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.delete-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}

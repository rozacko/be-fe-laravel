<?php

namespace App\Http\Controllers\Pr;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Validator;
use App\Traits\ValidationExcelImport;

use App\Models\T_PlantInspection;
use App\Models\T_InspectionItem;
use App\Models\T_InspectionComment;
use App\Exports\ItemInspectionPlantTemplate;
use App\Imports\ItemInspectionPlantImport;
use App\Models\User;
use DB;
use DataTables;

use Maatwebsite\Excel\Facades\Excel;


class PlantReabilityController extends Controller
{
    use ValidationExcelImport;

    public function __construct()
    {
        $this->__route = 'pages.master-data.ubah-password';
        $this->m_head = new T_PlantInspection();
        $this->m_item = new T_InspectionItem();
    }

    /**
     * @OA\Get(
     *   tags={"Plant Inspection"},
     *   path="api/plant_inspection/get-data",
     *   summary="Get plant inspection list",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */

    public function index(Request $request){
        $data = T_PlantInspection::select(['t_plant_inspection.*','users.name','m_fungsi.nama_fungsi'])
                    ->join('users',DB::raw('users.id::integer'),'=',DB::raw('t_plant_inspection.created_by::integer'))
                    ->join('m_fungsi','m_fungsi.id','=','t_plant_inspection.id_fungsi');
                    
        if($request->filled('tahun')){
            $data->where('t_plant_inspection.id_plant',DB::raw($request['plant'].'::varchar'));
        }

        if($request->filled('bulan')){
            $var = (int)$request['bulan'];
            $data->whereRaw('EXTRACT(MONTH FROM t_plant_inspection.tgl_inspection) = ?',$var);
        }

        if($request->filled('tahun')){
            $data->whereRaw('EXTRACT(YEAR FROM t_plant_inspection.tgl_inspection) = ?',$request['tahun']);
        }

        $data = $data->get();

        $model = Datatables::of($data)
                ->addIndexColumn()
                ->make(true)
                ->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);        
    }

    /**
     * @OA\Get(
     *   tags={"Plant Inspection"},
     *   path="api/plant_inspection/get-detail/{id}",
     *   summary="Get plant inspection list",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */

    public function getDetail($id){
        $data = T_PlantInspection::select(['t_plant_inspection.*','m_fungsi.nama_fungsi'])
                    ->join('users',DB::raw('users.id::integer'),'=',DB::raw('t_plant_inspection.created_by::integer'))
                    ->join('m_fungsi','m_fungsi.id','=','t_plant_inspection.id_fungsi')->where('t_plant_inspection.id',$id)
                    ->first();
        $data->item = T_InspectionItem::where('id_inspection',$id)->get();
        $data->comment = T_InspectionComment::select(['t_plant_inspection_comment.*','users.name'])->join('users',DB::raw('users.id::integer'),'=',DB::raw('t_plant_inspection_comment.created_by::integer'))->where('id_inspection',$id)->get();

        $response = responseDatatableSuccess(__('messages.read-success'), ['data'=>$data]);
        return response()->json($response);        
    } 

    /**
     * @OA\Post(
     *   tags={"Plant Inspection"},
     *   path="api/plant_inspection/",
     *   summary="Create plant inspection",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Request"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"id_plant"},
     *       @OA\Property(property="id_plant", type="string", format="text", example="1"),
     *       @OA\Property(property="kode_inspection", type="string", format="text", example="1"),
     *       @OA\Property(property="tgl_inspection", type="string", format="text", example="2023-01-01"),
     *       @OA\Property(property="desc_inspection", type="string", format="text", example="DESC"),
     *       @OA\Property(property="kode_opco", type="string", format="text", example="7000"),
     *       @OA\Property(property="kode_plant", type="string", format="text", example="7001"),
     *       @OA\Property(property="desc_area", type="string", format="text", example="Tuban"),
     *       @OA\Property(property="remark", type="string", format="text", example="Remark Here"),
     *     )
     *   )
     * )
     */

    public function store(Request $request)
    {
        // return $request;
        $this->validate($request, [
            'id_plant' => 'required',
            'kode_inspection' => 'required',
            'tgl_inspection' => 'required',
            'desc_inspection' => 'required',
            'kode_opco' => 'required',
            'kode_plant' => 'required',
            'status' => 'required',
            'desc_area' => 'required',
            'id_area' => 'required',
            'catatan' => 'required',
            'id_fungsi' => 'required'
        ]);

         $data = $request->only(
            'id_plant',
            'kode_inspection',
            'tgl_inspection',
            'desc_inspection',
            'kode_opco',
            'kode_plant',
            'status',
            'desc_area',
            'id_area',
            'id_fungsi'
        );

        $item = array();
        foreach ($request->item as $val) {
            $temp = array(
                'kode_equipment' => $val['kode_equipment'],
                'nama_equipment' => $val['desc_equipment'],
                'id_area' => $data['id_area'],
                'nama_area' => $data['desc_area'],
                'tgl_inspection' => $val['create_date'],
                'id_kondisi' => $val['id_kondisi'],
                'kode_kondisi' => $val['id_kondisi'],
                'remark' => $val['remark'],
            );
            $item[] = $temp;
        }

        DB::beginTransaction();
        try {
            if($request->id_inspection && !empty($request->id_inspection)){
                T_PlantInspection::where('id',$request->id_inspection)->update($data);
                T_InspectionItem::where('id_inspection',$request->id_inspection)->delete();
                $model = T_PlantInspection::where('id',$request->id_inspection)->first();
            }else{
                $model = T_PlantInspection::create($data);
            }
            
            T_InspectionComment::create(['id_inspection'=>$model->id,'remark'=>$request->catatan,'action'=>$data['status']]);

            foreach ($item as $val) {
                $val['id_inspection'] = $model->id;
                T_InspectionItem::create($val);
            }

            DB::commit();
            $response = responseSuccess(__('messages.create-success'), $model);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    } 

    /**
     * @OA\Get(
     *   tags={"Plant Inspection"},
     *   path="/api/plant_reability/download-template",
     *   summary="Get Overhaul Activity Template",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="Ok"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */
    public function getItemInspectionTemplate(Request $request)
    {
        // return $request;
        // return json_encode(['testing'=>'Test'])
        # code...
        $this->validate($request, [
            'area' => 'required',
        ]);

        return Excel::download(new ItemInspectionPlantTemplate($request->area), 'templateInspectionPlant.xlsx');
    }


    /**
     * @OA\Post(
     *   tags={"Plant Inspection"},
     *   path="/api/plant_reability/read-template/",
     *   summary="Read Template Item Inspection",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"upload_file"},
     *       @OA\Property(property="upload_file", type="file", format="text", example="template-item.xlsx"),
     *     )
     *   )
     * )
     */
    public function readItemInspectionTemplate(Request $request)
    {
        # code...
        $this->validate($request, [
            'upload_file' => 'required|mimes:xls,xlsx',
        ]);

       DB::beginTransaction();
       try{
        
            $data = Excel::toArray(new ItemInspectionPlantImport(), request()->file('upload_file'));

            if (!$this->compareTemplate("plant-inspection-item", array_keys($data[0]))) {
                return response()->json(responseFail(__('messages.validation-template-fail'), ["template" => [__('messages.validation-template-fail')]]), 400)->throwResponse();
            }    

            DB::commit();
            $response = responseDatatableSuccess(__('messages.read-success'), ['data'=>$data[0]]);
            return response()->json($response);   
       }
       catch(\Exception $ex){
            DB::rollBack();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
       }
    }

    /**
     * @OA\Get(
     *   tags={"Plant Inspection"},
     *   path="api/plant_inspection/get-data-approval",
     *   summary="Get list Approval plant inspection",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */

    public function index_approval(Request $request){
        $data = T_PlantInspection::select(['t_plant_inspection.*','users.name','m_fungsi.nama_fungsi'])
                    ->where('t_plant_inspection.status','SUBMIT')
                    ->join('users',DB::raw('users.id::integer'),'=',DB::raw('t_plant_inspection.created_by::integer'))
                    ->join('m_fungsi','m_fungsi.id','=','t_plant_inspection.id_fungsi')
                    ->get();

        $model = Datatables::of($data)
                ->addIndexColumn()
                ->make(true)
                ->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);        
    }

    /**
     * @OA\Post(
     *   tags={"Plant Inspection"},
     *   path="api/plant_inspection/approval",
     *   summary="Approval plant inspection",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Request"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"id_inspection"},
     *       @OA\Property(property="id_inspection", type="string", format="text", example="1"),
     *       @OA\Property(property="status", type="string", format="text", example="1"),
     *       @OA\Property(property="catatan", type="string", format="text", example="2023-01-01"),
     *     )
     *   )
     * )
     */
    
    public function approval(Request $request)
    {
        // return $request;
        $this->validate($request, [
            'id_inspection' => 'required',
            'status' => 'required',
            'catatan' => 'required'
        ]);

        $data = $request->only(
            'status'
        );

        DB::beginTransaction();
        try {
            
            T_PlantInspection::where('id',$request->id_inspection)->update($data);
            T_InspectionComment::create(['id_inspection'=>$request->id_inspection,'remark'=>$request->catatan,'action'=>$data['status']]);

            DB::commit();
            $response = responseSuccess(__('messages.create-success'), []);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    } 

    /**
     * @OA\Get(
     *   tags={"Plant Inspection"},
     *   path="/api/plant_reability/get-summary",
     *   summary="Get Data Summary Plant Inspection",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="Ok"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */
    public function getInspectionSummary(Request $request)
    {
        $year = empty($request->tahun) ? date('Y') : $request->tahun;
        $var = (int)$request['bulan'];
        $month = empty($request->bulan) ? '' : 'and extract(month from tpi.tgl_inspection) = '.$var;
        $plant = empty($request->plant) ? '' : "and id_plant = '$request->plant'";
        
        try {
            $data = DB::select("
            select
                ma.nm_area,
                ma.id,
                ma.nm_kondisi,
                ma.kd_kondisi,
                count(tii.id) as jumlah,
                tpi.tgl_inspection
            from (
                select ma.*,mk.kd_kondisi,mk.nm_kondisi from m_area ma join m_kondisi mk on 1=1
                ) ma
            left join(
                select
                    tpi.id_area,
                    max(id) as id
                from
                    t_plant_inspection tpi
                where
                    extract(year
                from
                    tpi.tgl_inspection) = ?
                {$month}
                {$plant}
                group by
                    id_area
            ) last_inspection on ma.id = last_inspection.id_area
            left join t_plant_inspection tpi on
                last_inspection.id = tpi.id
            left join t_item_inspection tii on
                tpi.id = tii.id_inspection and tii.id_kondisi::varchar = ma.kd_kondisi::varchar
            group by
                ma.nm_area,
                ma.id,
                ma.nm_kondisi,
                ma.kd_kondisi,
                tpi.tgl_inspection
            order by ma.id,ma.kd_kondisi",[$year]);
            
            $result = array();
            $cond = array();
            foreach ($data as $key => $value) {
                $result[$value->nm_area]['total'] = !isset($result[$value->nm_area]['total']) ? $value->jumlah : ($result[$value->nm_area]['total']+$value->jumlah);
                $result[$value->nm_area]['lastupdate'] = $value->tgl_inspection;
                $result[$value->nm_area][strtolower(explode(' ',$value->nm_kondisi)[0])] = $value->jumlah;
                $cond[strtolower(explode(' ',$value->nm_kondisi)[0])] = strtolower(explode(' ',$value->nm_kondisi)[0]);
            }

            $final_result = array();
            foreach ($result as $key => $value) {
                $value['name'] = $key;
                foreach ($cond as $val) {
                    $total = $value['total'] < 1 ? 1 : $value['total'];
                    $value['percent_'.$val] = Round(($value[$val]/$total*100),2);
                }
                $final_result[] = $value;
            }

            $response = responseDatatableSuccess(__('messages.read-success'), ['data'=>$final_result]);
            return response()->json($response);   
        } catch (\Exception $ex) {
            $response = responseFail(__('messages.read-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *   tags={"Plant Inspection"},
     *   path="/api/plant_reability/get-pie-summary",
     *   summary="Get Data Pie Chart Plant Inspection",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="Ok"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */
    public function getPieInspectionSummary(Request $request)
    {
        $year = empty($request->tahun) ? date('Y') : $request->tahun;
        $var = (int)$request['bulan'];
        $month = empty($request->bulan) ? '' : 'and extract(month from tpi.tgl_inspection) = '.$var;
        $plant = empty($request->plant) ? '' : "and id_plant = '$request->plant'";
        try {
            $data = DB::select("
            select
                ma.nm_kondisi,
                count(tii.id) as jumlah
            from (
                select ma.*,mk.kd_kondisi,mk.nm_kondisi from m_area ma join m_kondisi mk on 1=1
                ) ma
            left join(
                select
                    tpi.id_area,
                    max(id) as id
                from
                    t_plant_inspection tpi
                where
                    extract(year
                from
                    tpi.tgl_inspection) = ?
                {$month}
                {$plant}
                group by
                    id_area
            ) last_inspection on ma.id = last_inspection.id_area
            left join t_plant_inspection tpi on
                last_inspection.id = tpi.id
            left join t_item_inspection tii on
                tpi.id = tii.id_inspection and tii.id_kondisi::varchar = ma.kd_kondisi::varchar
            group by
                ma.nm_kondisi,
                ma.kd_kondisi
            order by ma.kd_kondisi",[$year]);
            
            $result = array();
            foreach ($data as $key => $value) {
                $result['data'][] = $value->jumlah;
                $result['label'][] = $value->nm_kondisi;
            }

            $response = responseDatatableSuccess(__('messages.read-success'), ['data'=>$result]);
            return response()->json($response);   
        } catch (\Exception $ex) {
            $response = responseFail(__('messages.read-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *   tags={"Plant Inspection"},
     *   path="/api/plant_reability/get-fungsi",
     *   summary="Get Data Fungsi",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="Ok"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */
    public function getFungsi(){
        try {
            $data = DB::select("select * from m_fungsi");

            $response = responseDatatableSuccess(__('messages.read-success'), ['data'=>$data]);
            return response()->json($response);   
        } catch (\Exception $ex) {
            $response = responseFail(__('messages.read-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}

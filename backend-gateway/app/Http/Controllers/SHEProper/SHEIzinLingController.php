<?php

namespace App\Http\Controllers\SHEProper;
use App\Models\SHEIzinLing;
use App\Models\SHEIzinLingFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;

class SHEIzinLingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = new SHEIzinLing();
        $this->modelfile = new SHEIzinLingFile();
    }

    /**
     * @OA\Get(
     *   tags={"SHE-IzinLing"},
     *   path="/api/izin_ling/",
     *   summary="Get izin_ling list",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */

    public function index(Request $request){
        $query = $this->model;        
        if(!empty($request->tahun)){
            $query = $query->where('tahun', $request->tahun);
        }        
        $model = Datatables::of($query)
                ->addIndexColumn()   
                ->addColumn('p_tuban', function($row){                    
                    $ret = $row->p_tuban;
                    if($ret == '1'){
                        $count = $this->cek_file($row->id,'Pabrik Tuban');
                        if($count != 0)
                            $ret = '2';
                    }                    
                    return $ret;
                })
                ->addColumn('p_gresik', function($row){                    
                    $ret = $row->p_gresik;
                    if($ret == '1'){
                        $count = $this->cek_file($row->id,'Pabrik Gresik');
                        if($count != 0)
                            $ret = '2';
                    }                    
                    return $ret;
                })
                ->addColumn('pelabuhan_tuban', function($row){                    
                    $ret = $row->pelabuhan_tuban;
                    if($ret == '1'){
                        $count = $this->cek_file($row->id,'Pelabuhan Tuban');
                        if($count != 0)
                            $ret = '2';
                    }                    
                    return $ret;
                }) 
                ->addColumn('pelabuhan_gresik', function($row){                    
                    $ret = $row->pelabuhan_gresik;
                    if($ret == '1'){
                        $count = $this->cek_file($row->id,'Pelabuhan Gresik');
                        if($count != 0)
                            $ret = '2';
                    }                    
                    return $ret;
                })    
                ->addColumn('tambang_tuban', function($row){                    
                    $ret = $row->tambang_tuban;
                    if($ret == '1'){
                        $count = $this->cek_file($row->id,'Tambang Tuban');
                        if($count != 0)
                            $ret = '2';
                    }                    
                    return $ret;
                })
                ->addColumn('status', function($row){   
                    $ret = '0';
                    $doc = $this->modelfile
                        ->where('id_perizinan',$row->id)                        
                        ->count(); 
                    if($doc == 0){
                        $ret = '3';
                        return $ret;
                    }
                    $myDate = date("Y-m-d", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "+1 month" ) );
                    $be_exp = $this->modelfile
                        ->where('id_perizinan',$row->id)
                        ->whereBetween('masa_berlaku',[date("Y-m-d"),$myDate])
                        ->count();                    
                    $exp = $this->modelfile
                        ->where('id_perizinan',$row->id)
                        ->where('masa_berlaku','<=',date("Y-m-d"))
                        ->count();  
                    if($be_exp != 0){
                        $ret = '1';
                    }else if($exp != 0){
                        $ret = '2';
                    }
                    return $ret;
                })              
                ->addColumn('action', function($row){
                    $btn = '<a href="#" onclick="show('.$row->id.')" class="btn btn-primary btn-sm"><i class="fa fa-edit" style="padding-right: 0px; font-size:10;"></i></a> ';
                    $btn .= '<a href="#" onclick="btn_delete('.$row->id.')" class="btn btn-danger btn-sm"><i class="fa fa-trash" style="padding-right: 0px; font-size:10;"></i></a> ';
                    return $btn;
                })
                ->make(true)
                ->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);        
    } 

    /**
     * @OA\Post(
     *   tags={"SHE-IzinLing"},
     *   path="/api/izin_ling",
     *   summary="Create izin_ling",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Request"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"tahun","nm_perizinan","sub_perizinan","p_tuban","p_gresik","pelabuhan_tuban","pelabuhan_gresik","tambang_tuban"},
     *       @OA\Property(property="tahun", type="string", format="text", example="2023"),
     *       @OA\Property(property="nm_perizinan", type="string", format="text", example="xxx"),
     *       @OA\Property(property="sub_perizinan", type="string", format="text", example="xxx"),     
     *       @OA\Property(property="p_tuban", type="string", format="text", example="1"),     
     *       @OA\Property(property="p_gresik", type="string", format="text", example="1"),     
     *       @OA\Property(property="pelabuhan_tuban", type="string", format="text", example="1"),     
     *       @OA\Property(property="pelabuhan_gresik", type="string", format="text", example="1"),     
     *       @OA\Property(property="tambang_tuban", type="string", format="text", example="1"),     
     *       @OA\Property(property="keterangan", type="string", format="text", example="1"),     
     *     )
     *   )
     * )
     */
    public function store(Request $request)
    {
        $data = $request->only(
            'tahun',
            'nm_perizinan',            
            'sub_perizinan',
            'p_tuban',
            'p_gresik',
            'pelabuhan_tuban',
            'pelabuhan_gresik',
            'tambang_tuban',
            'keterangan',
        );

        $this->validate($request, [
            'tahun' => 'required',
            'nm_perizinan' => 'required',
            'sub_perizinan' => 'required',
            'p_tuban' => 'required',
            'p_gresik' => 'required',
            'pelabuhan_tuban' => 'required',
            'pelabuhan_gresik' => 'required',
            'tambang_tuban' => 'required',
        ]);
        
        DB::beginTransaction();
        try {
            $model = $this->model->create($data);
            DB::commit();
            $response = responseSuccess(__('messages.create-success'), $model);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *   tags={"SHE-IzinLing"},
     *   path="/api/izin_ling/{id}",
     *   summary="Detail SHE-IzinLing",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Ok"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function show($id)
    {
        $model = $this->model->where('id', $id)->firstOrFail();
        $response = responseSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    /**
      * @OA\Put(
     *   tags={"SHE-IzinLing"},
     *   path="/api/izin_ling/{id}",
     *   summary="Update izin_ling",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"tahun","nm_perizinan","sub_perizinan","p_tuban","p_gresik","pelabuhan_tuban","pelabuhan_gresik","tambang_tuban"},
     *       @OA\Property(property="tahun", type="string", format="text", example="2023"),
     *       @OA\Property(property="nm_perizinan", type="string", format="text", example="xxx"),
     *       @OA\Property(property="sub_perizinan", type="string", format="text", example="xxx"),     
     *       @OA\Property(property="p_tuban", type="string", format="text", example="1"),     
     *       @OA\Property(property="p_gresik", type="string", format="text", example="1"),     
     *       @OA\Property(property="pelabuhan_tuban", type="string", format="text", example="1"),     
     *       @OA\Property(property="pelabuhan_gresik", type="string", format="text", example="1"),     
     *       @OA\Property(property="tambang_tuban", type="string", format="text", example="1"),     
     *       @OA\Property(property="keterangan", type="string", format="text", example="1"),     
     *     )
     *   ),
     *   @OA\Response(response=200, description="Successfully updated"),
     *   @OA\Response(response=400, description="Bad request"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'tahun' => 'required',
            'nm_perizinan' => 'required',
            'sub_perizinan' => 'required',
            'p_tuban' => 'required',
            'p_gresik' => 'required',
            'pelabuhan_tuban' => 'required',
            'pelabuhan_gresik' => 'required',
            'tambang_tuban' => 'required',
        ]);       
        $model = $this->model->where('id', $id)->firstOrFail();
        DB::beginTransaction();
        try { 
            $model->tahun = $request->input('tahun');
            $model->nm_perizinan = $request->input('nm_perizinan');
            $model->sub_perizinan = $request->input('sub_perizinan');
            $model->p_tuban = $request->input('p_tuban');
            $model->pelabuhan_tuban = $request->input('pelabuhan_tuban');
            $model->pelabuhan_gresik = $request->input('pelabuhan_gresik');
            $model->tambang_tuban = $request->input('tambang_tuban');
            $model->keterangan = $request->input('keterangan');
            $model->save();
            DB::commit();
            $response = responseSuccess(__('messages.update-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.update-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Delete(
     *   tags={"SHE-IzinLing"},
     *   path="/api/izin_ling/{id}",
     *   summary="SHE-IzinLing destroy",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Successfully deleted"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function destroy($id)
    {        
        $model = $this->model->where('id', $id)->firstOrFail();
        $modelfile = $this->modelfile->where('id_perizinan', $id)->get();
        DB::beginTransaction();
        try {
            foreach ($modelfile as $key => $value) {
                if($value->file != '')
                    $return = DeleteFiles($value->file);
            }
            $model->delete();
            $this->modelfile->where('id_perizinan', $id)->delete();
            DB::commit();
            $response = responseSuccess(__('messages.delete-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.delete-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    
    public function cek_file($id_perizinan, $area){
        $count = $this->modelfile->where('id_perizinan',$id_perizinan)->where('area', $area)->count();
        return $count;
    }
}

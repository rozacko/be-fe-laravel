<?php

namespace App\Http\Controllers\SHEProper;
use App\Models\SHEIzinLingFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;

class SHEIzinLingFileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = new SHEIzinLingFile();
    }

    /**
     * @OA\Get(
     *   tags={"SHE-IzinLingFile"},
     *   path="/api/izin_ling_file/",
     *   summary="Get izin_ling_file list",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */

    public function index(Request $request){
        $query = $this->model->where('id_perizinan', $request->id);
        $model = Datatables::of($query)
                ->addIndexColumn()                    
                ->addColumn('action', function($row){                    
                    $btn = '<a href="#" onclick="btn_delete_file('.$row->id.')" class="btn btn-danger btn-sm"><i class="fa fa-trash" style="padding-right: 0px; font-size:10;"></i></a> ';
                    return $btn;
                })
                ->addColumn('files', function($row){                    
                    $btn = '';
                    if($row->file != ''){
                        $btn = ReadFiles($row->file);
                    }   
                    return $btn;
                })
                ->addColumn('status', function($row){   
                    $ret = '0';
                    for ($i=0; $i<=29 ; $i++) { 
                        $date[] = date("Y-m-d", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "+".$i." day" ) );
                    }
                    if(in_array($row->masa_berlaku,$date)){
                        $ret = '1';
                    }else if($row->masa_berlaku <= date("Y-m-d")){
                        $ret = '2';
                    }
                    return $ret;
                })
                ->make(true)
                ->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);        
    } 

    /**
     * @OA\Post(
     *   tags={"SHE-IzinLingFile"},
     *   path="/api/izin_ling_file",
     *   summary="Create izin_ling_file",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Request"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"id_perizinan","area","no_izin","penerbit","masa_berlaku","file"},
     *       @OA\Property(property="id_perizinan", type="string", format="text", example="2023"),
     *       @OA\Property(property="area", type="string", format="text", example="xxx"),
     *       @OA\Property(property="no_izin", type="string", format="text", example="xxx"),
     *       @OA\Property(property="penerbit", type="string", format="text", example="xxx"),
     *       @OA\Property(property="masa_berlaku", type="string", format="date", example="2023-03-13"),     
     *       @OA\Property(property="file", type="string", format="binary", example=""),     
     *     )
     *   )
     * )
     */
    public function store(Request $request)
    {          
        $data = $request->only(
            'id_perizinan',
            'area',
            'no_izin',
            'penerbit',
            'masa_berlaku',
            'file',
        );

        $this->validate($request, [
            'id_perizinan' => 'required|integer',          
            'area' => 'required',          
            'no_izin' => 'required',          
            'penerbit' => 'required',          
            'masa_berlaku' => 'required|date',          
            'file' => 'required',
        ]);
        
        DB::beginTransaction();
        try {
            $model = $this->model->create($data);
            DB::commit();
            $response = responseSuccess(__('messages.create-success'), $model);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *   tags={"SHE-IzinLingFile"},
     *   path="/api/izin_ling_file/{id}",
     *   summary="Detail SHE-IzinLingFile",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Ok"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function show($id)
    {
        $model = $this->model->where('id', $id)->firstOrFail();
        $response = responseSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    /**
      * @OA\Put(
     *   tags={"SHE-IzinLingFile"},
     *   path="/api/izin_ling_file/{id}",
     *   summary="Update izin_ling_file",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"tahun","file"},
     *       @OA\Property(property="tahun", type="string", format="text", example="2023"),
     *       @OA\Property(property="file", type="string", format="binary", example=""),     
     *     )
     *   ),
     *   @OA\Response(response=200, description="Successfully updated"),
     *   @OA\Response(response=400, description="Bad request"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */

    public function update(Request $request, $id)
    {
        
    }

    /**
     * @OA\Delete(
     *   tags={"SHE-IzinLingFile"},
     *   path="/api/izin_ling_file/{id}",
     *   summary="SHE-IzinLingFile destroy",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Successfully deleted"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function destroy($id)
    {        
        $model = $this->model->where('id', $id)->firstOrFail();
        DB::beginTransaction();
        try {
            if($model->file != '')
                $return = DeleteFiles($model->file);
            $model->delete();
            DB::commit();
            $response = responseSuccess(__('messages.delete-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.delete-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function upload_file(Request $request)
    {                  
        $data = $request->only(            
            'upload_file',
        );

        $this->validate($request, [            
            'upload_file' => 'required|mimes:pdf|max:10240',
        ]);
        
        try {
            $return = StoreFiles($request, 'izin_ling_file', 'she_proper');
            $response = responseSuccess(__('messages.upload-success'), $return);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.upload-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}

<?php

namespace App\Http\Controllers\SHEProper;
use App\Models\SHEProgramLing;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;

class SHEProgramLingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = new SHEProgramLing();
    }

    /**
     * @OA\Get(
     *   tags={"SHE-ProgramLing"},
     *   path="/api/program_ling/",
     *   summary="Get program_ling list",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */

    public function index(Request $request){
        $query = $this->model;        
        if(!empty($request->tahun)){
            $query = $query->where('tahun', $request->tahun);
        }        
        $model = Datatables::of($query)
                ->addIndexColumn()                    
                ->addColumn('action', function($row){
                    $btn = '<a href="#" onclick="show('.$row->id.')" class="btn btn-primary btn-sm"><i class="fa fa-edit" style="padding-right: 0px; font-size:10;"></i></a> ';
                    $btn .= '<a href="#" onclick="btn_delete('.$row->id.')" class="btn btn-danger btn-sm"><i class="fa fa-trash" style="padding-right: 0px; font-size:10;"></i></a> ';
                    return $btn;
                })
                ->make(true)
                ->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);        
    } 

    /**
     * @OA\Post(
     *   tags={"SHE-ProgramLing"},
     *   path="/api/program_ling",
     *   summary="Create program_ling",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Request"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"tahun","program","sasaran","target","pic"},
     *       @OA\Property(property="tahun", type="string", format="text", example="2023"),
     *       @OA\Property(property="program", type="string", format="text", example="xxx"),
     *       @OA\Property(property="sasaran", type="string", format="binary", example="xxx"),     
     *       @OA\Property(property="target", type="string", format="binary", example="xxx"),     
     *       @OA\Property(property="pic", type="string", format="binary", example="xxx"),     
     *     )
     *   )
     * )
     */
    public function store(Request $request)
    {          
        $data = $request->only(
            'tahun',
            'program',            
            'sasaran',
            'target',
            'pic',
        );

        $this->validate($request, [
            'tahun' => 'required',
            'program' => 'required',
            'sasaran' => 'required',
            'target' => 'required',
            'pic' => 'required',
        ]);
        
        DB::beginTransaction();
        try {
            $model = $this->model->create($data);
            DB::commit();
            $response = responseSuccess(__('messages.create-success'), $model);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *   tags={"SHE-ProgramLing"},
     *   path="/api/program_ling/{id}",
     *   summary="Detail SHE-ProgramLing",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Ok"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function show($id)
    {
        $model = $this->model->where('id', $id)->firstOrFail();
        $response = responseSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    /**
      * @OA\Put(
     *   tags={"SHE-ProgramLing"},
     *   path="/api/program_ling/{id}",
     *   summary="Update program_ling",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"tahun","program","sasaran","target","pic"},
     *       @OA\Property(property="tahun", type="string", format="text", example="2023"),
     *       @OA\Property(property="program", type="string", format="text", example="xxx"),
     *       @OA\Property(property="sasaran", type="string", format="binary", example="xxx"),     
     *       @OA\Property(property="target", type="string", format="binary", example="xxx"),     
     *       @OA\Property(property="pic", type="string", format="binary", example="xxx"),     
     *     )
     *   ),
     *   @OA\Response(response=200, description="Successfully updated"),
     *   @OA\Response(response=400, description="Bad request"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'tahun' => 'required',
            'program' => 'required',
            'sasaran' => 'required',
            'target' => 'required',
            'pic' => 'required',
        ]);        
        $model = $this->model->where('id', $id)->firstOrFail();
        DB::beginTransaction();
        try { 
            $model->tahun = $request->input('tahun');
            $model->program = $request->input('program');
            $model->sasaran = $request->input('sasaran');
            $model->target = $request->input('target');
            $model->pic = $request->input('pic');
            $model->save();
            DB::commit();
            $response = responseSuccess(__('messages.update-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.update-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Delete(
     *   tags={"SHE-ProgramLing"},
     *   path="/api/program_ling/{id}",
     *   summary="SHE-ProgramLing destroy",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Successfully deleted"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function destroy($id)
    {        
        $model = $this->model->where('id', $id)->firstOrFail();
        DB::beginTransaction();
        try {
            $model->delete();
            DB::commit();
            $response = responseSuccess(__('messages.delete-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.delete-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}

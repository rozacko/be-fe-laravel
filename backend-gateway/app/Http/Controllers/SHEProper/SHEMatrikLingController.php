<?php

namespace App\Http\Controllers\SHEProper;
use App\Models\SHEMatrikLing;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;

class SHEMatrikLingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = new SHEMatrikLing();
    }

    /**
     * @OA\Get(
     *   tags={"SHE-MatrikLing"},
     *   path="/api/matrik_ling/",
     *   summary="Get matrik_ling list",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */

    public function index(Request $request){
        $query = $this->model;        
        if(!empty($request->tahun)){
            $query = $query->where('tahun', $request->tahun);
        }        
        $model = Datatables::of($query)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = '<a href="#" onclick="show('.$row->id.')" class="btn btn-primary btn-sm"><i class="fa fa-edit" style="padding-right: 0px; font-size:10;"></i></a> ';
                    $btn .= '<a href="#" onclick="btn_delete('.$row->id.')" class="btn btn-danger btn-sm"><i class="fa fa-trash" style="padding-right: 0px; font-size:10;"></i></a> ';
                    return $btn;
                })
                ->make(true)
                ->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);        
    } 

    /**
     * @OA\Post(
     *   tags={"SHE-MatrikLing"},
     *   path="/api/matrik_ling",
     *   summary="Create matrik_ling",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Request"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"tahun","jenis","nomor","peraturan"},
     *       @OA\Property(property="tahun", type="string", format="text", example="2023"),
     *       @OA\Property(property="jenis", type="string", format="text", example="xxx"),
     *       @OA\Property(property="nomor", type="string", format="text", example="xxx"),
     *       @OA\Property(property="peraturan", type="string", format="text", example="xxx"),
     *       @OA\Property(property="emisi_udara", type="string", format="text", example="1"),
     *       @OA\Property(property="pembuangan_air", type="string", format="text", example="1"),
     *       @OA\Property(property="pembuangan_tanah", type="string", format="text", example="1"),
     *       @OA\Property(property="sda", type="string", format="text", example="1"),
     *       @OA\Property(property="penggunaan_energi", type="string", format="text", example="1"),
     *       @OA\Property(property="pancaran_energi", type="string", format="text", example="1"),
     *       @OA\Property(property="limbah", type="string", format="text", example="1"),
     *       @OA\Property(property="atribut_fisik", type="string", format="text", example="1"),
     *       @OA\Property(property="k3", type="string", format="text", example="1"),
     *       @OA\Property(property="taat", type="string", format="text", example="1"),
     *       @OA\Property(property="keterangan", type="string", format="text", example="1"),
     *     )
     *   )
     * )
     */
    public function store(Request $request)
    {
        $data = $request->only(
            'tahun',
            'jenis',
            'nomor',
            'peraturan',
            'emisi_udara',
            'pembuangan_air',
            'pembuangan_tanah',
            'sda',
            'penggunaan_energi',
            'pancaran_energi',
            'limbah',
            'atribut_fisik',
            'k3',
            'taat',
            'keterangan',
        );

        $this->validate($request, [
            'tahun' => 'required',
            'jenis' => 'required',
            'nomor' => 'required',
            'peraturan' => 'required',
        ]);
        
        DB::beginTransaction();
        try {
            $data['emisi_udara'] = isset($request->emisi_udara) ? '1' : '0';
            $data['pembuangan_air'] = isset($request->pembuangan_air) ? '1' : '0';
            $data['pembuangan_tanah'] = isset($request->pembuangan_tanah) ? '1' : '0';
            $data['sda'] = isset($request->sda) ? '1' : '0';
            $data['penggunaan_energi'] = isset($request->penggunaan_energi) ? '1' : '0';
            $data['pancaran_energi'] = isset($request->pancaran_energi) ? '1' : '0';
            $data['limbah'] = isset($request->limbah) ? '1' : '0';
            $data['atribut_fisik'] = isset($request->atribut_fisik) ? '1' : '0';
            $data['k3'] = isset($request->k3) ? '1' : '0';
            $model = $this->model->create($data);
            DB::commit();
            $response = responseSuccess(__('messages.create-success'), $model);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *   tags={"SHE-MatrikLing"},
     *   path="/api/matrik_ling/{id}",
     *   summary="Detail SHE-MatrikLing",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Ok"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function show($id)
    {
        $model = $this->model->where('id', $id)->firstOrFail();
        $response = responseSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    /**
      * @OA\Put(
     *   tags={"SHE-MatrikLing"},
     *   path="/api/matrik_ling/{id}",
     *   summary="Update matrik_ling",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"tahun","jenis","nomor","peraturan"},
     *       @OA\Property(property="tahun", type="string", format="text", example="2023"),
     *       @OA\Property(property="jenis", type="string", format="text", example="xxx"),
     *       @OA\Property(property="nomor", type="string", format="text", example="xxx"),
     *       @OA\Property(property="peraturan", type="string", format="text", example="xxx"),
     *       @OA\Property(property="emisi_udara", type="string", format="text", example="1"),
     *       @OA\Property(property="pembuangan_air", type="string", format="text", example="1"),
     *       @OA\Property(property="pembuangan_tanah", type="string", format="text", example="1"),
     *       @OA\Property(property="sda", type="string", format="text", example="1"),
     *       @OA\Property(property="penggunaan_energi", type="string", format="text", example="1"),
     *       @OA\Property(property="pancaran_energi", type="string", format="text", example="1"),
     *       @OA\Property(property="limbah", type="string", format="text", example="1"),
     *       @OA\Property(property="atribut_fisik", type="string", format="text", example="1"),
     *       @OA\Property(property="k3", type="string", format="text", example="1"),
     *       @OA\Property(property="taat", type="string", format="text", example="1"),
     *       @OA\Property(property="keterangan", type="string", format="text", example="1"),   
     *     )
     *   ),
     *   @OA\Response(response=200, description="Successfully updated"),
     *   @OA\Response(response=400, description="Bad request"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'tahun' => 'required',
            'jenis' => 'required',
            'nomor' => 'required',
            'peraturan' => 'required',            
        ]);    
        $model = $this->model->where('id', $id)->firstOrFail();
        DB::beginTransaction();
        try {
            $model->tahun = $request->input('tahun');
            $model->jenis = $request->input('jenis');
            $model->nomor = $request->input('nomor');
            $model->peraturan = $request->input('peraturan');
            $model->emisi_udara = isset($request->emisi_udara) ? '1' : '0';
            $model->pembuangan_air = isset($request->pembuangan_air) ? '1' : '0';
            $model->pembuangan_tanah = isset($request->pembuangan_tanah) ? '1' : '0';
            $model->sda = isset($request->sda) ? '1' : '0';
            $model->penggunaan_energi = isset($request->penggunaan_energi) ? '1' : '0';
            $model->pancaran_energi = isset($request->pancaran_energi) ? '1' : '0';
            $model->limbah = isset($request->limbah) ? '1' : '0';
            $model->atribut_fisik = isset($request->atribut_fisik) ? '1' : '0';
            $model->k3 = isset($request->k3) ? '1' : '0';
            $model->taat = $request->input('taat');
            $model->keterangan = $request->input('keterangan');
            $model->save();
            DB::commit();
            $response = responseSuccess(__('messages.update-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.update-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Delete(
     *   tags={"SHE-MatrikLing"},
     *   path="/api/matrik_ling/{id}",
     *   summary="SHE-MatrikLing destroy",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Successfully deleted"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function destroy($id)
    {        
        $model = $this->model->where('id', $id)->firstOrFail();
        DB::beginTransaction();
        try {
            $model->delete();
            DB::commit();
            $response = responseSuccess(__('messages.delete-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.delete-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}

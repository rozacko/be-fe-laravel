<?php

namespace App\Http\Controllers\SHEProper;
use App\Models\SHEProgramLingIs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;

class SHEProgramLingIsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = new SHEProgramLingIs();
    }

    /**
     * @OA\Get(
     *   tags={"SHE-ProgramLingIs"},
     *   path="/api/program_ling_is/",
     *   summary="Get program_ling_is list",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */

    public function index(Request $request){
        $query = $this->model->where('id_program', $request->id);
        $model = Datatables::of($query)
                ->addIndexColumn()                    
                ->addColumn('jan', function($row){                    
                    $data = $this->cek_bulan('01',$row->start_date, $row->end_date);
                    return $data;
                })
                ->addColumn('feb', function($row){                    
                    $data = $this->cek_bulan('02',$row->start_date, $row->end_date);
                    return $data;
                })
                ->addColumn('mar', function($row){                    
                    $data = $this->cek_bulan('03',$row->start_date, $row->end_date);
                    return $data;
                })
                ->addColumn('apr', function($row){                    
                    $data = $this->cek_bulan('04',$row->start_date, $row->end_date);
                    return $data;
                })
                ->addColumn('mei', function($row){                    
                    $data = $this->cek_bulan('05',$row->start_date, $row->end_date);
                    return $data;
                })
                ->addColumn('jun', function($row){                    
                    $data = $this->cek_bulan('06',$row->start_date, $row->end_date);
                    return $data;
                })
                ->addColumn('jul', function($row){                    
                    $data = $this->cek_bulan('07',$row->start_date, $row->end_date);
                    return $data;
                })
                ->addColumn('aug', function($row){                    
                    $data = $this->cek_bulan('08',$row->start_date, $row->end_date);
                    return $data;
                })
                ->addColumn('sep', function($row){                    
                    $data = $this->cek_bulan('09',$row->start_date, $row->end_date);
                    return $data;
                })
                ->addColumn('okt', function($row){                    
                    $data = $this->cek_bulan('10',$row->start_date, $row->end_date);
                    return $data;
                })
                ->addColumn('nov', function($row){                    
                    $data = $this->cek_bulan('11',$row->start_date, $row->end_date);
                    return $data;
                })
                ->addColumn('des', function($row){                    
                    $data = $this->cek_bulan('12',$row->start_date, $row->end_date);
                    return $data;
                })
                ->addColumn('action', function($row){                    
                    $btn = '<a href="#" onclick="btn_delete_is('.$row->id.')" class="btn btn-danger btn-sm"><i class="fa fa-trash" style="padding-right: 0px; font-size:10;"></i></a> ';
                    return $btn;
                })
                ->make(true)
                ->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);        
    } 

    /**
     * @OA\Post(
     *   tags={"SHE-ProgramLingIs"},
     *   path="/api/program_ling_is",
     *   summary="Create program_ling_is",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Request"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"id_program","inisiatif_strategis","start_date","end_date"},
     *       @OA\Property(property="id_program", type="string", format="text", example="1"),
     *       @OA\Property(property="inisiatif_strategis", type="string", format="text", example="xxx"),
     *       @OA\Property(property="start_date", type="string", format="binary", example="03"),     
     *       @OA\Property(property="end_date", type="string", format="binary", example="04"), 
     *     )
     *   )
     * )
     */
    public function store(Request $request)
    {          
        $data = $request->only(
            'id_program',
            'inisiatif_strategis',
            'start_date',            
            'end_date',            
        );

        $this->validate($request, [
            'id_program' => 'required',
            'inisiatif_strategis' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',            
        ]);
        
        DB::beginTransaction();
        try {
            $model = $this->model->create($data);
            DB::commit();
            $response = responseSuccess(__('messages.create-success'), $model);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *   tags={"SHE-ProgramLingIs"},
     *   path="/api/program_ling_is/{id}",
     *   summary="Detail SHE-ProgramLingIs",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Ok"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function show($id)
    {
        $model = $this->model->where('id', $id)->firstOrFail();
        $response = responseSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    public function update(Request $request, $id)
    {
    }

    /**
     * @OA\Delete(
     *   tags={"SHE-ProgramLingIs"},
     *   path="/api/program_ling_is/{id}",
     *   summary="SHE-ProgramLing destroy",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Successfully deleted"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function destroy($id)
    {        
        $model = $this->model->where('id', $id)->firstOrFail();
        DB::beginTransaction();
        try {
            $model->delete();
            DB::commit();
            $response = responseSuccess(__('messages.delete-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.delete-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function cek_bulan($bulan,$start, $end){
        $ret = '0';
        $cek = [];
        $entry = false;
        $dt_bulan = ['01','02','03','04','05','06','07','08','09','10','11','12']; 
        foreach ($dt_bulan as $key => $value) {
            if($value == $start){
                $entry = true;
            }
            if($value == $end){
                $entry = false;
            }
            if($entry)
                array_push($cek, $value);
        }

        if(in_array($bulan,$cek))
            $ret = '1';
        elseif($bulan == $start && $bulan == $end)
            $ret = '1';
        return $ret;
    }
}

<?php

namespace App\Http\Controllers\SHEProper;
use App\Models\SHEKomPersonil;
use App\Models\SHEKomPersonilFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;

class SHEKomPersonilController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = new SHEKomPersonil();
        $this->modelfile = new SHEKomPersonilFile();
    }

    /**
     * @OA\Get(
     *   tags={"SHE-KomPersonil"},
     *   path="/api/kom_personil/",
     *   summary="Get kom_personil list",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */

    public function index(Request $request){
        $query = $this->model;        
        if(!empty($request->tahun)){
            $query = $query->where('tahun', $request->tahun);
        }        
        $model = Datatables::of($query)
                ->addIndexColumn()
                ->addColumn('status', function($row){   
                    $ret = '0';
                    $doc = $this->modelfile
                        ->where('id_kompetensi',$row->id)                        
                        ->count(); 
                    if($doc == 0){
                        $ret = '3';
                        return $ret;
                    }
                    $myDate = date("Y-m-d", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "+1 month" ) );
                    $be_exp = $this->modelfile
                        ->where('id_kompetensi',$row->id)
                        ->whereBetween('end_date',[date("Y-m-d"),$myDate])
                        ->count();                    
                    $exp = $this->modelfile
                        ->where('id_kompetensi',$row->id)
                        ->where('end_date','<=',date("Y-m-d"))
                        ->count();  
                    if($be_exp != 0){
                        $ret = '1';
                    }else if($exp != 0){
                        $ret = '2';
                    }
                    return $ret;
                })              
                ->addColumn('action', function($row){
                    $btn = '<a href="#" onclick="show('.$row->id.')" class="btn btn-primary btn-sm"><i class="fa fa-edit" style="padding-right: 0px; font-size:10;"></i></a> ';
                    $btn .= '<a href="#" onclick="btn_delete('.$row->id.')" class="btn btn-danger btn-sm"><i class="fa fa-trash" style="padding-right: 0px; font-size:10;"></i></a> ';
                    return $btn;
                })
                ->make(true)
                ->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);        
    } 

    /**
     * @OA\Post(
     *   tags={"SHE-KomPersonil"},
     *   path="/api/kom_personil",
     *   summary="Create kom_personil",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Request"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"tahun","bidang","kriteria"},
     *       @OA\Property(property="tahun", type="string", format="text", example="2023"),
     *       @OA\Property(property="bidang", type="string", format="text", example="xxx"),
     *       @OA\Property(property="kriteria", type="string", format="text", example="xxx"),       
     *     )
     *   )
     * )
     */
    public function store(Request $request)
    {
        $data = $request->only(
            'tahun',
            'bidang',            
            'kriteria',            
        );

        $this->validate($request, [
            'tahun' => 'required',
            'bidang' => 'required',
            'kriteria' => 'required',            
        ]);
        
        DB::beginTransaction();
        try {
            $model = $this->model->create($data);
            DB::commit();
            $response = responseSuccess(__('messages.create-success'), $model);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *   tags={"SHE-KomPersonil"},
     *   path="/api/kom_personil/{id}",
     *   summary="Detail SHE-KomPersonil",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Ok"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function show($id)
    {
        $model = $this->model->where('id', $id)->firstOrFail();
        $response = responseSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    /**
      * @OA\Put(
     *   tags={"SHE-KomPersonil"},
     *   path="/api/kom_personil/{id}",
     *   summary="Update kom_personil",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"tahun","bidang","kriteria"},
     *       @OA\Property(property="tahun", type="string", format="text", example="2023"),
     *       @OA\Property(property="bidang", type="string", format="text", example="xxx"),
     *       @OA\Property(property="kriteria", type="string", format="text", example="xxx"),       
     *     )
     *   ),
     *   @OA\Response(response=200, description="Successfully updated"),
     *   @OA\Response(response=400, description="Bad request"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'tahun' => 'required',
            'bidang' => 'required',
            'kriteria' => 'required',            
        ]);

        $model = $this->model->where('id', $id)->firstOrFail();
        DB::beginTransaction();
        try { 
            $model->tahun = $request->input('tahun');
            $model->bidang = $request->input('bidang');
            $model->kriteria = $request->input('kriteria');            
            $model->save();
            DB::commit();
            $response = responseSuccess(__('messages.update-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.update-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Delete(
     *   tags={"SHE-KomPersonil"},
     *   path="/api/kom_personil/{id}",
     *   summary="SHE-KomPersonil destroy",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Successfully deleted"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function destroy($id)
    {        
        $model = $this->model->where('id', $id)->firstOrFail();
        $modelfile = $this->modelfile->where('id_kompetensi', $id)->get();
        DB::beginTransaction();
        try {
            foreach ($modelfile as $key => $value) {
                if($value->file != '')
                    $return = DeleteFiles($value->file);
            }
            $model->delete();
            $this->modelfile->where('id_kompetensi', $id)->delete();
            DB::commit();
            $response = responseSuccess(__('messages.delete-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.delete-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}

<?php

namespace App\Http\Controllers\SHEProper;
use App\Models\SHEKpiProper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\SheKpiProperTemplate;
use App\Imports\ExcelImportsWithHeader;
use App\Traits\ValidationExcelImport;
use Illuminate\Support\Facades\Validator;

class SHEKpiProperController extends Controller
{
    use ValidationExcelImport;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = new SHEKpiProper();
    }

    /**
     * @OA\Get(
     *   tags={"SHE-KpiProper"},
     *   path="/api/kpiproper/",
     *   summary="Get kpiproper list",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */

    public function index(Request $request)
    {   
        $query = $this->model;
        if(!empty($request->tahun)){
            $query = $query->where('tahun', strval($request->tahun));
        }
        $model = Datatables::of($query)
                ->addIndexColumn()         
                ->addColumn('action', function($row){
                    $btn = '<a href="#" onclick="show('.$row->id.')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> ';
                    $btn .= '<a href="#" onclick="btn_delete('.$row->id.')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a> ';
                    return $btn;
                })
                ->addColumn('target', function($row){                    
                    return number_format($row->target,2,'.');
                })
                ->addColumn('realisasi', function($row){                    
                    return number_format($row->realisasi,2, '.');
                })
                ->addColumn('kpi', function($row){                    
                    return number_format(($row->kpi*100),2,'.');
                })
                ->make(true)
                ->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);        
    } 

    /**
     * @OA\Post(
     *   tags={"SHE-KpiProper"},
     *   path="/api/kpiproper",
     *   summary="Create kpiproper",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Request"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"tahun","kebijakan","target","realisasi","kpi"},
     *       @OA\Property(property="tahun", type="string", format="text", example="2023"),     
     *       @OA\Property(property="kebijakan", type="string", format="text", example="xxx"),
     *       @OA\Property(property="target", type="number", format="float", example="10"),
     *       @OA\Property(property="realisasi", type="number", format="float", example="10"),
     *       @OA\Property(property="kpi", type="number", format="float", example="10"),     
     *     )
     *   )
     * )
     */
    public function store(Request $request)
    {
        $data = $request->only(
            'tahun',
            'kebijakan',
            'satuan',
            'target',
            'realisasi',
            'kpi',
        );

        $this->validate($request, [
            'tahun' => 'required',
            'kebijakan' => 'required',
            'target' => 'required',
            'realisasi' => 'required',
            'kpi' => 'required',
        ]);

        DB::beginTransaction();
        try {
            $model = $this->model->create($data);
            DB::commit();
            $response = responseSuccess(__('messages.create-success'), $model);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *   tags={"SHE-KpiProper"},
     *   path="/api/kpiproper/{id}",
     *   summary="Detail SHE-KpiProper",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Ok"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function show($id)
    {
        $model = $this->model->where('id', $id)->firstOrFail();
        $response = responseSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    /**
     * @OA\Put(
     *   tags={"SHE-KpiProper"},
     *   path="/api/kpiproper/{id}",
     *   summary="SHE-KpiProper update",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"tahun","kebijakan","target","realisasi","kpi"},
     *       @OA\Property(property="tahun", type="string", format="text", example="2023"),     
     *       @OA\Property(property="kebijakan", type="string", format="text", example="xxx"),
     *       @OA\Property(property="target", type="number", format="float", example="10"),
     *       @OA\Property(property="realisasi", type="number", format="float", example="10"),
     *       @OA\Property(property="kpi", type="number", format="float", example="10"),     
     *     )
     *   ),
     *   @OA\Response(response=200, description="Successfully updated"),
     *   @OA\Response(response=400, description="Bad request"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'tahun' => 'required',
            'kebijakan' => 'required',
            'target' => 'required',
            'realisasi' => 'required',
            'kpi' => 'required',
        ]);
        $model = $this->model->where('id', $id)->firstOrFail();

        DB::beginTransaction();
        try {
            $model->tahun = $request->input('tahun');            
            $model->kebijakan = $request->input('kebijakan');            
            $model->satuan = $request->input('satuan');            
            $model->realisasi = $request->input('realisasi');            
            $model->target = $request->input('target');            
            $model->kpi = $request->input('kpi');            
            $model->save();
            DB::commit();
            $response = responseSuccess(__('messages.update-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.update-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Delete(
     *   tags={"SHE-KpiProper"},
     *   path="/api/kpiproper/{id}",
     *   summary="SHE-KpiProper destroy",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="id", in="path", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="Successfully deleted"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function destroy($id)
    {        
        $model = $this->model->where('id', $id)->firstOrFail();
        DB::beginTransaction();
        try {
            $model->delete();
            DB::commit();
            $response = responseSuccess(__('messages.delete-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.delete-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *   tags={"SHE-KpiProper"},
     *   path="/api/kpiproper/template",
     *   summary="Get SHE-KpiProper Template",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="Ok"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */
    public function template(Request $request)
    {
        return Excel::download(new SheKpiProperTemplate(), 'SHE_KPI_PROPER.xlsx');
    }

    /**
     * @OA\Post(
     *   tags={"SHE-KpiProper"},
     *   path="/api/kpiproper/import",
     *   summary="import SHE-KpiProper",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"fileupload"},
     *       @OA\Property(property="fileupload", type="file", format="text", example="kpiproper.xlsx"),
     *     )
     *   )
     * )
     */
    
    public function import(Request $request)
    {
        $this->validate($request, [
            'tahun' => 'required',
            'upload_file' => 'required|mimes:xls,xlsx',
        ]);
        $file = $request->file('upload_file');
        $data = Excel::toArray(new ExcelImportsWithHeader, $file)[0];        
        if (!$this->compareTemplate("kpi-proper", array_keys($data[0]))) {
            return response()->json(responseFail(__('messages.validation-template-fail'), ["template" => [__('messages.validation-template-fail')]]), 400)->throwResponse();
        }
        
        $dataExcel = collect($data);
        $dataMaps = [];
        $lineValidation = [
            'kpikebijakan_perusahaan' => 'required',
            'target' => 'required',
            'realisasi' => 'required',
            'kpi' => 'required',            
        ];
        
        foreach ($dataExcel as $key => $item) {            
            $this->validateImport($item, $lineValidation, ($key + 2));
            array_push($dataMaps, [
                'tahun' => $request->tahun,
                'kebijakan' => $item['kpikebijakan_perusahaan'],
                'satuan' => $item['satuan'],
                'target' => $item['target'],
                'realisasi' => $item['realisasi'],
                'kpi' => $item['kpi'],
                'created_by' => Auth::user()->id,
                'created_at' => now()
            ]);
        }

        DB::beginTransaction();
        try{
            SHEKpiProper::insert($dataMaps);
            DB::commit();
            $response = responseSuccess(__('messages.import-success'));
            return response()->json($response, Response::HTTP_CREATED);
        }
        catch(\Exception $ex){
            DB::rollBack();
            $response = responseFail(__('messages.import-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private function validateImport($data, array $rules, $line)
    {
        $messages = [
            'required' => __('validation.required'),
            'numeric'  => __('validation.numeric'),
            'exists'   => __('validation.exists'),
        ];

        $validator = Validator::make($data, $rules, $messages);

        if ($validator->fails()) {
            $response = responseFail(__('messages.validation-fail') . " " . __('messages.inline-fail') . " : " . $line, $validator->errors(), []);
            $return = response()->json($response, Response::HTTP_BAD_REQUEST, [], JSON_PRETTY_PRINT);
            $return->throwResponse();
        }
    }
}


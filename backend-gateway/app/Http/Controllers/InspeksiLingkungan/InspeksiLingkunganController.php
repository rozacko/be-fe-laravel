<?php

namespace App\Http\Controllers\InspeksiLingkungan;

use App\Exports\InspeksiLingkunganTemplate;
use App\Http\Controllers\Controller;
use App\Models\TInspeksiLingkungan;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Cache;
use App\Imports\InspeksiLingkunganImport;

class InspeksiLingkunganController extends Controller
{
    /**
     * @OA\Post(
     *   tags={"Inspeksi Lingkungan"},
     *   path="/api/inspeksi-lingkungan/list",
     *   summary="Get inspeksi Lingkungan list",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="departemen", in="query", description="parameter departemen", required=true, @OA\Schema( type="string" )),
     *   @OA\Parameter(name="tahun", in="query", description="parameter tahun", required=true, @OA\Schema( type="string" )),
     *   @OA\Parameter(name="bulan", in="query", description="parameter bulan", required=true, @OA\Schema( type="string" )),
     *   @OA\Parameter(name="status", in="query", description="parameter status", required=true, @OA\Schema( type="string" )),
     *   @OA\Parameter(name="keyword", in="query", description="parameter keyword", required=false, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     *   @OA\Response(response=500, description="Internal Server Error"),
     * )
     */
    public function index(Request $request)
    {
        $this->validate($request, [
            'departemen' => 'nullable',
            'tahun' => 'required|numeric',
            'bulan' => 'required|numeric',
            'status' => ['required', Rule::in(['open', 'close'])]
        ]);
        try {
            $departemen = $request->departemen;
            $tahun = $request->tahun;
            $bulan = $request->bulan;
            $status = $request->status;

            $query = TInspeksiLingkungan::query();
            $query = $query->where('departemen', $departemen)
                ->where('status', $status)
                ->whereRaw('EXTRACT(YEAR FROM tanggal_inspeksi) = ?', $tahun)
                ->whereRaw('EXTRACT(MONTH FROM tanggal_inspeksi) = ?', $bulan);
            $isNotEmptyKeyword = ($request->keyword != null || strlen(trim($request->keyword) > 0));
            if ($request->has('keyword') && $isNotEmptyKeyword) {
                $keyword = strtolower($request->keyword);
                $query->where(function ($query) use ($keyword) {
                    $keyword = "%" . $keyword . "%";
                    $query->whereRaw('lower(uraian_temuan) like ?', $keyword);
                });
            }

            $model = DataTables::of($query)
                ->addIndexColumn()
                ->make(true)
                ->getData(true);
            $response = responseDatatableSuccess(__('messages.read-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            $response = responseFail(__('messages.show-import-fail'), $ex->getMessage());
            return response()->json($response);
        }
    }

    /**
     * @OA\Get(
     *   tags={"Inspeksi Lingkungan"},
     *   path="/api/inspeksi-lingkungan/edit/{uuid}",
     *   summary="Get detail inspeksi lingkungan",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="uuid", in="path", description="parameter uuid", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     *   @OA\Response(response=500, description="Internal Server Error"),
     * )
     */
    public function show($uuid)
    {
        $this->isValidUuid($uuid);
        try {
            $model = TInspeksiLingkungan::where('uuid', $uuid)->firstOrFail();
            $dataModel = [];
            if ($model != null) {
                $dataModel[] = $model;
            }
            $data = $dataModel;
            $response = responseSuccess(__('messages.read-success'), $data);
            return response()->json($response);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            $response = responseFail(__('messages.read-fail'));
            return response()->json($response, Response::HTTP_BAD_REQUEST);
        } catch (\Exception $ex) {
            $response = responseFail(__('messages.read-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Post(
     *   tags={"Inspeksi Lingkungan"},
     *   path="/api/inspeksi-lingkungan/store",
     *   summary="insert inspeksi lingkungan",
     *   security={{"token": {}}},
     *      @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *               required={
     *                  "area",
     *                  "departemen",
     *                  "eqipment_no",
     *                  "devisi_area",
     *                  "unit_kerja",
     *                  "tanggal_inspeksi",
     *                  "status",
     *                  "evaluasi"
     *                },
     *                  @OA\Property(
     *                     description="area",
     *                     property="area",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     description="departemen",
     *                     property="departemen",
     *                     type="string",
     *                 ),
     *                  @OA\Property(
     *                     description="eqipment_no",
     *                     property="eqipment_no",
     *                     type="string",
     *                 ),
     *                  @OA\Property(
     *                     description="devisi_area",
     *                     property="devisi_area",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     description="unit_kerja",
     *                     property="unit_kerja",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     description="tanggal_inspeksi",
     *                     property="tanggal_inspeksi",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     description="status (open / close)",
     *                     property="status",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     description="evaluasi",
     *                     property="evaluasi",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     description="upload foto temuan",
     *                     property="foto_temuan",
     *                     type="string",
     *                     format="binary",
     *                 ),
     *                 @OA\Property(
     *                     description="upload foto closing",
     *                     property="foto_closing",
     *                     type="string",
     *                     format="binary",
     *                 )
     *             )
     *         )
     *   ),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     *   @OA\Response(response=500, description="Internal Server Error"),
     * )
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'area' => 'required',
            'departemen' => 'required',
            'eqipment_no' => 'required',
            'foto_temuan' => 'nullable|mimes:jpg,jpeg,png,webp',
            'foto_closing' => 'nullable|mimes:jpg,jpeg,png,webp',
            'devisi_area' => 'required',
            'unit_kerja' => 'required',
            'tanggal_inspeksi' => 'required|date_format:Y-m-d',
            'status' => ['required', Rule::in(['open', 'close'])],
            'evaluasi' => 'required'
        ]);
        try {
            $area = $request->area;
            $departemen = $request->departemen;
            $equipmentNo = $request->equipment_no;
            $devisiArea = $request->devisi_area;
            $unitKerja = $request->unit_kerja;
            $tanggalInspeksi = $request->tanggal_inspeksi;
            $status = $request->status;
            $evaluasi = $request->evaluasi;

            $fotoTemuan = $request->file('foto_temuan');
            $fotoClosing = $request->file('foto_closing');
            $filePathTemuan = "";
            if ($request->hasFile('foto_temuan')) {
                $explode = explode('.', $fotoTemuan->getClientOriginalName());
                $originalName = $explode[0];
                $extension = $fotoTemuan->getClientOriginalExtension();
                $rename = $originalName . date("YmdHis") . '.' . $extension;
                $filePathTemuan = $fotoTemuan->storeAs('she_enviro/inspeksi_lingkungan/foto_temuan', $rename, 'minio');
            }

            $filePathClosing = "";
            if ($request->hasFile('foto_closing')) {
                $explode = explode('.', $fotoClosing->getClientOriginalName());
                $originalName = $explode[0];
                $extension = $fotoClosing->getClientOriginalExtension();
                $rename = $originalName . date("YmdHis") . '.' . $extension;
                $filePathClosing = $fotoClosing->storeAs('she_enviro/inspeksi_lingkungan/foto_temuan', $rename, 'minio');
            }

            DB::beginTransaction();
            $tInspeksiLingkungan = new TInspeksiLingkungan;
            $tInspeksiLingkungan->departemen = $departemen;
            $tInspeksiLingkungan->area = $area;
            $tInspeksiLingkungan->equipment_no = $equipmentNo;
            $tInspeksiLingkungan->foto_temuan = $filePathTemuan;
            $tInspeksiLingkungan->foto_closing = $filePathClosing;
            $tInspeksiLingkungan->devisi_area = $devisiArea;
            $tInspeksiLingkungan->unit_kerja = $unitKerja;
            $tInspeksiLingkungan->evaluasi = $evaluasi;
            $tInspeksiLingkungan->status = $status;
            $tInspeksiLingkungan->tanggal_inspeksi = $tanggalInspeksi;
            $tInspeksiLingkungan->save();
            DB::commit();

            $response = responseSuccess(__('messages.create-success'), $tInspeksiLingkungan);
            return response()->json($response);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            DB::rollBack();
            $response = responseFail(__('messages.read-fail'));
            return response()->json($response, Response::HTTP_BAD_REQUEST);
        } catch (\Exception $ex) {
            DB::rollBack();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Post(
     *   tags={"Inspeksi Lingkungan"},
     *   path="/api/inspeksi-lingkungan/update/{uuid}",
     *   summary="update inspeksi lingkungan",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="uuid", in="path", description="parameter uuid", required=true, @OA\Schema( type="string" )),
     *      @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *               required={
     *                  "area",
     *                  "departemen",
     *                  "eqipment_no",
     *                  "devisi_area",
     *                  "unit_kerja",
     *                  "tanggal_inspeksi",
     *                  "status",
     *                  "evaluasi"
     *                },
     *                  @OA\Property(
     *                     description="area",
     *                     property="area",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     description="departemen",
     *                     property="departemen",
     *                     type="string",
     *                 ),
     *                  @OA\Property(
     *                     description="eqipment_no",
     *                     property="eqipment_no",
     *                     type="string",
     *                 ),
     *                  @OA\Property(
     *                     description="devisi_area",
     *                     property="devisi_area",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     description="unit_kerja",
     *                     property="unit_kerja",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     description="tanggal_inspeksi",
     *                     property="tanggal_inspeksi",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     description="status (open / close)",
     *                     property="status",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     description="evaluasi",
     *                     property="evaluasi",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     description="upload foto temuan",
     *                     property="foto_temuan",
     *                     type="string",
     *                     format="binary",
     *                 ),
     *                 @OA\Property(
     *                     description="upload foto closing",
     *                     property="foto_closing",
     *                     type="string",
     *                     format="binary",
     *                 )
     *             )
     *         )
     *   ),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     *   @OA\Response(response=500, description="Internal Server Error"),
     * )
     */
    public function update(String $uuid, Request $request)
    {
        $this->validate($request, [
            'area' => 'required',
            'departemen' => 'required',
            'eqipment_no' => 'required',
            'foto_temuan' => 'nullable|mimes:jpg,jpeg,png,webp',
            'foto_closing' => 'nullable|mimes:jpg,jpeg,png,webp',
            'devisi_area' => 'required',
            'unit_kerja' => 'required',
            'tanggal_inspeksi' => 'required|date_format:Y-m-d',
            'status' => ['required', Rule::in(['open', 'close'])],
            'evaluasi' => 'required'
        ]);

        try {
            $area = $request->area;
            $departemen = $request->departemen;
            $equipmentNo = $request->equipment_no;
            $devisiArea = $request->devisi_area;
            $unitKerja = $request->unit_kerja;
            $tanggalInspeksi = $request->tanggal_inspeksi;
            $status = $request->status;
            $evaluasi = $request->evaluasi;

            $fotoTemuan = $request->file('foto_temuan');
            $fotoClosing = $request->file('foto_closing');
            $filePathTemuan = "";

            if ($request->hasFile('foto_temuan')) {
                $explode = explode('.', $fotoTemuan->getClientOriginalName());
                $originalName = $explode[0];
                $extension = $fotoTemuan->getClientOriginalExtension();
                $rename = $originalName . date("YmdHis") . '.' . $extension;
                $filePathTemuan = $fotoTemuan->storeAs('she_enviro/inspeksi_lingkungan/foto_temuan', $rename, 'minio');
            }

            $filePathClosing = "";
            if ($request->hasFile('foto_closing')) {
                $explode = explode('.', $fotoClosing->getClientOriginalName());
                $originalName = $explode[0];
                $extension = $fotoClosing->getClientOriginalExtension();
                $rename = $originalName . date("YmdHis") . '.' . $extension;
                $filePathClosing = $fotoClosing->storeAs('she_enviro/inspeksi_lingkungan/foto_closing', $rename, 'minio');
            }

            DB::beginTransaction();
            $tInspeksiLingkungan = TInspeksiLingkungan::where('uuid', $uuid)->firstOrFail();
            $oldFotoTemuan = $tInspeksiLingkungan->foto_temuan;
            $oldFotoClosing = $tInspeksiLingkungan->foto_closing;

            $tInspeksiLingkungan->departemen = $departemen;
            $tInspeksiLingkungan->area = $area;
            $tInspeksiLingkungan->equipment_no = $equipmentNo;
            $tInspeksiLingkungan->foto_temuan = $filePathTemuan;
            $tInspeksiLingkungan->foto_closing = $filePathClosing;
            $tInspeksiLingkungan->devisi_area = $devisiArea;
            $tInspeksiLingkungan->unit_kerja = $unitKerja;
            $tInspeksiLingkungan->evaluasi = $evaluasi;
            $tInspeksiLingkungan->status = $status;
            $tInspeksiLingkungan->tanggal_inspeksi = $tanggalInspeksi;
            $tInspeksiLingkungan->save();
            DB::commit();

            if (strlen($oldFotoTemuan) > 0 && strlen($filePathTemuan) > 0) {
                DeleteFiles($oldFotoTemuan);
            }

            if (strlen($oldFotoClosing) > 0 && strlen($filePathClosing) > 0) {
                DeleteFiles($oldFotoClosing);
            }

            $response = responseSuccess(__('messages.update-success'));
            return response()->json($response);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            DB::rollBack();
            $response = responseFail(__('messages.read-fail'));
            return response()->json($response, Response::HTTP_BAD_REQUEST);
        } catch (\Exception $ex) {
            DB::rollBack();
            $response = responseFail(__('messages.update-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Delete(
     *   tags={"Inspeksi Lingkungan"},
     *   path="/api/inspeksi-lingkungan/delete/{uuid}",
     *   summary="delete inspeksi lingkungan by uuid",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="uuid", in="path", description="uuid that needs to be delete", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     *   @OA\Response(response=500, description="Internal Server Error"),
     * )
     */
    public function delete($uuid)
    {
        $this->isValidUuid($uuid);
        $model = TInspeksiLingkungan::where('uuid', $uuid)->firstOrFail();
        try {
            $oldFotoTemuan = $model->foto_temuan;
            $oldFotoClosing = $model->foto_closing;

            DB::beginTransaction();
            $model->delete();
            DB::commit();

            if (strlen($oldFotoTemuan) > 0) {
                DeleteFiles($oldFotoTemuan);
            }

            if (strlen($oldFotoClosing) > 0) {
                DeleteFiles($oldFotoClosing);
            }

            $response = responseSuccess(__('messages.delete-success'));
            return response()->json($response);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            DB::rollBack();
            $response = responseFail(__('messages.read-fail'));
            return response()->json($response, Response::HTTP_BAD_REQUEST);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.delete-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *   tags={"Inspeksi Lingkungan"},
     *   path="/api/inspeksi-lingkungan/download/template",
     *   summary="download template inspeksi lingkungan",
     *   security={{"token": {}}},
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=401, description="Unauthorized"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function downloadTemplate(Request $request)
    {
        return Excel::download(new InspeksiLingkunganTemplate(), 'INSPEKSI_LINGKUNGAN_TEMPLATE.xlsx');
    }

    /**
     * @OA\Post(
     *   tags={"Inspeksi Lingkungan"},
     *   path="/api/upload-inspeksi-lingkungan",
     *   summary="import inspeksi lingkungan",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *      @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     description="file to upload",
     *                     property="upload_file",
     *                     type="string",
     *                     format="binary",
     *                 )
     *             )
     *         )
     *     )
     * )
     */
    public function ImportInspeksiLingkungan(Request $request)
    {
        try {
            $this->validate($request, [
                'upload_file' => 'required|mimes:xls,xlsx',
            ]);
            $file = $request->file('upload_file');
            $key = date('YmdHis');
            DB::beginTransaction();
            Excel::import(new InspeksiLingkunganImport($key), $file);
            $resultImport = Cache::get($key);
            $responseData = [
                "total_data" => count($resultImport['all']),
                "total_success" => count($resultImport['success']),
                "total_failed" => count($resultImport['failed']),
                "failed_data" => $resultImport['failed'],
            ];
            DB::commit();

            if ($responseData['total_success'] == 0) {
                $response =  [
                    'status' => "failed",
                    'message' => "Data failed to import",
                    'data' => $responseData
                ];
                return response()->json($response, Response::HTTP_OK);
            }

            $response = responseSuccess(__('messages.import-success'), $responseData);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollBack();
            $response = responseFail(__('messages.import-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Post(
     *   tags={"Inspeksi Lingkungan"},
     *   path="/api/inspeksi-lingkungan/dashboard",
     *   summary="dashboard inspeksi Lingkungan",
     *   security={{"token": {}}},
     *   @OA\Parameter(name="tahun", in="query", description="parameter tahun", required=true, @OA\Schema( type="string" )),
     *   @OA\Parameter(name="bulan", in="query", description="parameter bulan", required=true, @OA\Schema( type="string" )),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     *   @OA\Response(response=500, description="Internal Server Error"),
     * )
     */
    public function dashboard(Request $request)
    {
        $this->validate($request, [
            'tahun' => 'required|numeric',
            'bulan' => 'required|numeric',
        ]);
        try {
            $tahun = $request->tahun;
            $bulan = $request->bulan;

            $query = TInspeksiLingkungan::query();
            $query = $query->whereRaw('EXTRACT(YEAR FROM tanggal_inspeksi) = ?', $tahun)
                ->whereRaw('EXTRACT(MONTH FROM tanggal_inspeksi) = ?', $bulan);
            $query->selectRaw('status,count(*) as jml_sts');
            $query->groupBy('status');
            $resultDBOverview = $query->get();
            $dataOverview = $this->mappingDataOverview($resultDBOverview);

            $query = TInspeksiLingkungan::query();
            $query = $query->whereRaw('EXTRACT(YEAR FROM tanggal_inspeksi) = ?', $tahun)
                ->whereRaw('EXTRACT(MONTH FROM tanggal_inspeksi) = ?', $bulan);
            $query->selectRaw('status,departemen,count(*) as jml_sts');
            $query->groupBy('status', 'departemen');
            $query->orderBy('departemen', 'asc');
            $resultDBDepartement = $query->get();
            $dataPerDepertemen = $this->mappingDataDepartemen($resultDBDepartement);

            $data = ["overview" => $dataOverview, "per_departemen" => $dataPerDepertemen];
            $response = responseSuccess(__('messages.read-success'), $data);
            return response()->json($response, Response::HTTP_OK);
        } catch (\Exception $ex) {
            $response = responseFail(__('messages.read-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function mappingDataOverview($data)
    {
        $totalOpen = 0;
        $totalClose = 0;
        $totalAll = 0;
        foreach ($data as $key => $value) {
            switch ($value['status']) {
                case 'open':
                    $totalOpen = $value['jml_sts'];
                    break;
                case 'close':
                    $totalClose = $value['jml_sts'];
                    break;
            }
        }
        $totalAll = $totalOpen + $totalClose;
        return ["total" => $totalAll, "open" => $totalOpen, "close" => $totalClose];
    }

    public function mappingDataDepartemen($data)
    {
        $dataDepartemen = [];
        foreach ($data as $key => $value) {
            if (!in_array($value['departemen'], $dataDepartemen)) {
                $dataDepartemen[] = $value['departemen'];
            }
        }

        $resultMapping = [];
        foreach ($data as $key => $value) {
            if (in_array($value['departemen'], $dataDepartemen)) {
                $resultMapping[$value['departemen']][$value['status']] = $value['jml_sts'];
            }
        }

        foreach ($dataDepartemen as $key => $value) {
            $totalOpen = 0;
            $totalClose = 0;
            $totalAll = 0;
            if (!key_exists('open', $resultMapping[$value])) {
                $resultMapping[$value]['open'] = $totalOpen;
            } else {
                $totalOpen = $resultMapping[$value]['open'];
            }

            if (!key_exists('close', $resultMapping[$value])) {
                $resultMapping[$value]['close'] = $totalClose;
            } else {
                $totalOpen = $resultMapping[$value]['close'];
            }

            if (!key_exists('total', $resultMapping[$value])) {
                $totalAll = $totalOpen + $totalClose;
                $resultMapping[$value]['total'] = $totalAll;
            }
        }
        
        return $resultMapping;
    }
}

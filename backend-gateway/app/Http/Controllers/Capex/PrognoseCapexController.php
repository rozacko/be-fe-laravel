<?php

namespace App\Http\Controllers\Capex;

use App\Exports\DownloadExcel;
use App\Http\Controllers\Controller;
use App\Imports\ExcelImportsWithHeader;
// use App\Models\TRkapCapex;
use App\Models\TPrognoseCapex;
use App\Rules\PerformanceImportRules;
use App\Traits\ValidationExcelImport;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class PrognoseCapexController extends Controller
{
    //
    use ValidationExcelImport;

     /**
     * @OA\Get(
     *   tags={"Prognose Capex LIST DATATABLE"},
     *   path="/api/rkap/prognose-capex",
     *   summary="List Datatable Prognose Capex",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     * )
     */
    public function index(Request $request, $exported = false)
    {
        # code...
        // $query= DB::table('ghopo.t_prognose_capex');
        $query= TPrognoseCapex::query();

        $rules = [];
        if($request->filled('no_project')){
            $rules['no_project'] = 'required';
            $query = $query->where('no_project', $request->get('no_project'));
        }

        if($exported){
            // 'NO PROJECT','PROGNOSE DESCRIPTION',"WBS","TAHUN","BULAN","PROGNOSE"
            $query= $query->select('no_project','prognose_description','wbs','tahun','bulan','prognose');
        }

        $model = DataTables::of($query);
        if($exported)
        {
            return $model->getFilteredQuery()->get();
        }
        $model = $model->addIndexColumn()
                ->make(true)
                ->getData(true);
        $response = responseDatatableSuccess(__('messages.read-success'), $model);
        return response()->json($response);        
    }

    /**
     * @OA\Get(
     *   tags={"Prognose Capex Get Project"},
     *   path="/api/rkap/prognose-capex-project",
     *   summary="List Data Project Prognose Capex",
     *   security={{"token": {}}},
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     * )
     */
    public function getProject(Request $request)
    {
        # code...
        $query= DB::table('ghopo.t_prognose_capex')
                ->select("no_project","id")->get();

        // $model = DataTables::of($query)
        //         ->addIndexColumn()
        //         ->make(true)
        //         ->getData(true);
        $response = responseSuccess(__('messages.read-success'), $query);
        return response()->json($response);        
    }

    /**
     * @OA\Get(
     *   tags={"PROGNOSE CAPEX SHOW DATA"},
     *   path="/api/rkap/prognose-capex/{uuid}",
     *   summary="Show Data By UUID Prognose Capex",
     *   security={{"token": {}}},
     *      @OA\Parameter(
     *         name="uuid",
     *         in="path",
     *         description="uuid",
     *         required=true,
     *         @OA\Schema( format="uuid",type="string" )
     *      ),
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest"),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */
    public function show($id)
    {
        $this->isValidUuid($id);
        $request = new Request(['uuid'=> $id]);
        $this->validate($request, [
            'uuid' => 'required|exists:t_prognose_capex',
        ]);
        $model = DB::table('ghopo.t_prognose_capex')->where('uuid', $id)->first();
        $response = responseSuccess(__('messages.read-success'), $model);
        return response()->json($response);
    }

    /**
     * @OA\Put(
     *   tags={"Data Prognose Capex Update "},
     *   path="/api/rkap/prognose-capex/{uuid}",
     *   summary="Data Prognose Capex Update",
     *   security={{"token": {}}},
     *      @OA\Parameter(
     *         name="uuid",
     *         in="path",
     *         description="uuid",
     *         required=true,
     *         @OA\Schema( format="uuid",type="string" )
     *      ),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *       required={"no_project","prognose_description","wbs","tahun","bulan","prognose"},
     *       @OA\Property(property="no_project", type="string", format="text",example="PR0005"),
     *       @OA\Property(property="prognose_description", type="string", format="text",example="keterangan"),
     *       @OA\Property(property="wbs", type="string", format="text",example="000099"),
     *       @OA\Property(property="tahun", type="string", format="text",example="2023"),
     *       @OA\Property(property="bulan", type="string", format="text",example="02"),
     *       @OA\Property(property="prognose", type="string", format="text",example="10000"),
     *     )
     *   ),
     *   @OA\Response(response=200, description="Successfully updated"),
     *   @OA\Response(response=400, description="Bad request"),
     *   @OA\Response(response=404, description="Not Found"),
     * )
     */
    public function update($uuid, Request $request)
    {
        $this->isValidUuid($uuid);
        $request = new Request($request->only(['no_project','prognose_description','wbs','tahun','bulan','prognose']));
        $rules = [
            'no_project' => 'required',
            'prognose_description' => 'required',
            'wbs' => 'required',
            'tahun' => 'required',
            'bulan' => 'required',
            'prognose' => 'required'
        ];
        // $request = $request->merge([
        //     'uuid'=> $uuid,
        //     "t_qc_kf"=>[
        //         'uuid' => $uuid,
        //         'where' => [
        //             'tanggal' => $request->get('tanggal'),
        //             'plant_id' => intval($request->get('plant_id')),
        //             'jam' => $request->get('jam'),
        //         ]
        //     ]
        // ]);
        $this->validate($request, $rules);
        $data = TPrognoseCapex::where('uuid', $uuid)->first();
        DB::beginTransaction();
        try {
            $data->update([
                'no_project' =>$request->get("no_project"),
                'prognose_description' =>$request->get("prognose_description"),
                'wbs' =>$request->get("wbs"),
                'tahun' =>$request->get("tahun"),
                'bulan' =>$request->get("bulan"),
                'prognose' =>$request->get("prognose"),
                'updated_by'=> Auth::user()->uuid,
            ]);
            DB::commit();
            $response = responseSuccess(__('messages.update-success'), []);
            // return response()->json($response, Response::HTTP_CREATED);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.update-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Post(
     *   tags={"Prognose Capex Import File "},
     *   path="/api/rkap/prognose-capex-import",
     *   summary="Data Prognose Capex Import File",
     *   security={{"token": {}}},
     *    @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(property="upload_file",type="string", format="binary" , description="mimes:xls,xlsx"),
     *             )
     *         ),
     *     ),
     *   @OA\Response(response=201, description="Successfully created"),
     *   @OA\Response(response=400, description="Bad Reqest")
     * )
     */
    public function import(Request $request)
    {
        # code...
        $this->validate($request, [
            'upload_file' => 'required|mimes:xls,xlsx',
        ]);

        $file = $request->file('upload_file');
        $data = Excel::toArray(new ExcelImportsWithHeader, $file)[0];

        // dd($data[0]);
        if (!$this->compareTemplate("prognose-capex", array_keys($data[0]))) {
            return response()->json(responseFail(__('messages.validation-template-fail'), ["template" => [__('messages.validation-template-fail')]]), 400)->throwResponse();
        }

        $dataExcel = collect($data);
        $lineValidation = [
           'no_project' => 'required',
           'prognose_description' => 'required',
           'wbs' => 'required',
           'tahun' => 'required',
           'bulan' => 'required',
           'prognose' => 'required'
        ];
        foreach ($dataExcel as $key => $item) {
            $item['capex'] = [
                'no_project'            => $item['no_project'],
                'prognose_description'  => $item['prognose_description'],
                'wbs'                   => $item['wbs'],
                'tahun'                 => $item['tahun'],
                'bulan'                 => $item['bulan'],
                'prognose'              => $item['prognose']
            ];
            $this->validateImport($item, $lineValidation, ($key + 1));
        }
        
        $insert = $dataExcel->map(function($item) {
            $item['created_by'] = Auth::user()->uuid;
            $item['created_at'] = now();
            return $item;
        });
        DB::beginTransaction();
        try {
            TPrognoseCapex::insert($insert->toArray());
            DB::commit();
            $response = responseSuccess(__('messages.create-success'), []);
            return response()->json($response, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.create-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Delete(
      *   tags={"Data Prognose Capex Delete "},
     *   path="/api/rkap/prognose-capex/{uuid}",
     *   summary="Data Prognose Capex Delete",
     *   security={{"token": {}}},
     *      @OA\Parameter(
     *         name="uuid",
     *         in="path",
     *         description="uuid",
     *         required=true,
     *         @OA\Schema( format="uuid",type="string" )
     *      ),
     *   @OA\Response(response=200, description="Successfully deleted"),
     *   @OA\Response(response=404, description="Not Found")
     * )
     */
    public function destroy($uuid)
    {        
        $this->isValidUuid($uuid);
        $request = new Request(['uuid'=> $uuid]);
        $this->validate($request, [
            'uuid' => 'required|exists:t_prognose_capex',
        ]);
        $model = TPrognoseCapex::where('uuid', $uuid)->first();
        DB::beginTransaction();
        try {
            $model->delete();
            DB::commit();
            $response = responseSuccess(__('messages.delete-success'), $model);
            return response()->json($response);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(__('messages.delete-fail'), $ex->getMessage());
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *   tags={"Data Prognose Capex Download Excel "},
     *   path="/api/rkap/prognose-capex-download",
     *   summary="Data Prognose Capex Download Excel",
     *   security={{"token": {}}},
     * @OA\Parameter(
     *         name="tahun",
     *         in="query",
     *         description="YYYY",
     *         @OA\Schema( format="text",type="string" ,example="2023")
     *      ),
     *   @OA\Response(response=200, description="OK"),
     *   @OA\Response(response=404, description="Not Found"),
     *   @OA\Response(response=401, description="Unauthorized"),
     * )
     */
    public function download(Request $request)
    {
        $request = $request->merge(["length" => -1]);
        $data    = $this->index($request, true);
        $columns = ['NO PROJECT','PROGNOSE DESCRIPTION',"WBS","TAHUN","BULAN","PROGNOSE"];
        return Excel::download((new DownloadExcel($data,$columns)), "PROGNOSE CAPEX.xlsx");
    }

    private function validateImport($data, array $rules, $line)
    {
        $messages = [
            'required' => __('validation.required'),
            'numeric'  => __('validation.numeric'),
            'exists'   => __('validation.exists'),
        ];

        $validator = Validator::make($data, $rules, $messages);

        if ($validator->fails()) {
            $response = responseFail(__('messages.validation-fail') . " " . __('messages.inline-fail') . " : " . $line, $validator->errors(), []);
            $return = response()->json($response, Response::HTTP_BAD_REQUEST, [], JSON_PRETTY_PRINT);
            $return->throwResponse();
        }
    }
}

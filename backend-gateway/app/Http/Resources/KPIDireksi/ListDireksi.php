<?php

namespace App\Http\Resources\KPIDireksi;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\KpiDireksi;

class ListDireksi extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $kpi = KpiDireksi::where('bulan',(int)$request->bulan)
                              ->where('tahun',$request->tahun)
                              ->where('kpi_group',$this->kpi_group)
                              ->get();
        return [
            'kpi_group'=>$this->kpi_group,
            'detail'=>$kpi,
            'bulan'=>$request->bulan,
            'tahun'=>$request->tahun
        ];
    }
}

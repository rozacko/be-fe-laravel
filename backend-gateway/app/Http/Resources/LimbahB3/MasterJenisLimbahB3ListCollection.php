<?php

namespace App\Http\Resources\LimbahB3;

use Illuminate\Http\Resources\Json\ResourceCollection;

class MasterJenisLimbahB3ListCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return $this->collection->map(function ($model) {
            return [
                'id' => $model->id,
                'uuid' => $model->uuid,
                'plant' => $model->plant->nm_plant,
                'jenisLimbahB3' => $model->jenis_limbah_b3,
                'sumber' => $model->sumber,
            ];
        });
    }
}


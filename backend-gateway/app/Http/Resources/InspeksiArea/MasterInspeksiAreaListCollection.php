<?php

namespace App\Http\Resources\InspeksiArea;

use Illuminate\Http\Resources\Json\ResourceCollection;

class MasterInspeksiAreaListCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {

        return $this->collection->map(function ($model) {
            return [
                'id' => $model->id,
                'uuid' => $model->uuid,
                'id_area' => $model->area_id,
                'nm_area' => $model->nm_area,
                'equipment' => $model->equipment,
                'tanggal' => $model->tanggal,
                'foto_temuan' => $model->foto_temuan,
                'foto_closing' => $model->foto_closing,
                'devisi_area' => $model->devisi_area,
                'unit_kerja' => $model->unit_kerja,
                'evaluasi' => $model->evaluasi,
                'status' => $model->status,
            ];
        });
    }
}

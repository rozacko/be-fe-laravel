<?php

namespace App\Http\Resources\DokumenLingkungan;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\Storage;

class MasterDokumenLingkunganListCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return $this->collection->map(function ($model) {
            $file = "";
            if ($model->filename != null && strlen($model->filename) > 0) {
                $file = ReadFiles($model->filename);
                if ($file == "File does not exist"){
                    $file = Storage::disk('minio')->url($model->filename);
                }
            }
            return [
                'id' => $model->id,
                'uuid' => $model->uuid,
                'judul' => $model->judul,
                'id_area' => $model->area_id,
                'nm_area' => $model->nm_area,
                'tahun' => $model->tahun,
                'no_dokumen' => $model->no_dokumen,
                'ruang_lingkup' => $model->ruang_lingkup,
                'filename' => $file,
                "jenis_dokumen"=>$model->jenis_dokumen,
            ];
        });
    }
}

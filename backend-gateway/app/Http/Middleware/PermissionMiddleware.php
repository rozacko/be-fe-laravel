<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;
use Spatie\Permission\Exceptions\UnauthorizedException;

class PermissionMiddleware
{
    public function handle($request, Closure $next, $permission, $guard = null)
    {
        $authGuard = app('auth')->guard($guard);

        if ($authGuard->guest()) {
            $response = responseFail(__('messages.spatie-not-login'));

            return response()->json($response, Response::HTTP_UNAUTHORIZED, [], JSON_PRETTY_PRINT);
        }

        $permissions = is_array($permission)
            ? $permission
            : explode('|', $permission);

        foreach ($permissions as $permission) {
            if ($authGuard->user()->can($permission)) {
                return $next($request);
            }
        }

        $response = responseFail(__('messages.spatie-not-permission'));
        return response()->json($response, Response::HTTP_FORBIDDEN, [], JSON_PRETTY_PRINT);
    }
}

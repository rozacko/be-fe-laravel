<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Tymon\JWTAuth\Facades\JWTAuth;

class JwtMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        try {
            JWTAuth::parseToken()->authenticate();
        } catch (Exception $e) {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException) {
                $response = responseFail(__('messages.jwt-invalid'));
                return response()->json($response, Response::HTTP_UNAUTHORIZED, [], JSON_PRETTY_PRINT);
            } else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException) {
                $response = responseFail(__('messages.jwt-expired'));
                return response()->json($response, Response::HTTP_UNAUTHORIZED, [], JSON_PRETTY_PRINT);
            } else {
                $response = responseFail(__('messages.jwt-not-found'));
                return response()->json($response, Response::HTTP_BAD_REQUEST, [], JSON_PRETTY_PRINT);
            }
        }
        return $next($request);
    }
}

<?php

namespace App\Http\Middleware;

use App\Models\LogActivity;
use Closure;

class LoggingActivity
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        $id = 0;

        if (app('auth')->check()) {
            $id = app('auth')->user()->id;
            $company_id = app('auth')->user()->company_id;
        }

        $as = $request->route()?->getName();

        $log['url'] = $request->url();
        $log['method'] = $request->method();
        $log['ip'] = $request->ip();
        $log['agent'] = $request->header('user-agent');
        $log['user_id'] = $id;
        $log['name'] = $as;

        $content_type = $response->headers->get('content-type');

        if (!strpos($content_type, 'officedocument')) {
            $content = (array) json_decode($response->content());
            $log['status'] = array_key_exists('status', $content) ? $content['status'] : null;
            $log['message'] = array_key_exists('message', $content) ? $content['message'] : null;
            $log['errors'] = array_key_exists('errors', $content) ? json_encode($content['errors']) : null;
            $log['http_code'] = $response->status();
            if (strpos($request->url(), 'login') == false) {
                $log['data'] = $request->getContent();
            }
        }

        if (strpos($request->url(), 'static') == false) {
            LogActivity::create($log);
        }

        return $response;
    }
}

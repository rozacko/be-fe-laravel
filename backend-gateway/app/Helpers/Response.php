<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function responseFail($message, $errors = array(), $data = array())
{
    return [
        'status' => 'fail',
        'message' => $message,
        'errors' => $errors,
        'data' => $data
    ];
}

function responseSuccess($message, $data = array())
{
    return [
        'status' => "success",
        'message' => $message,
        'data' => $data
    ];
}

function responseDatatableSuccess($message, $data = array())
{
    $response = [
        'status' => "success",
        'message' => $message,
    ];
    return array_merge($response, $data);
}

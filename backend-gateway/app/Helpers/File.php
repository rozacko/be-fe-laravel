<?php

use Illuminate\Support\Facades\Storage;

function StoreFiles($request, $filename = '', $folder = 'uploads')
{
    $path = '';
    if($request->hasFile('upload_file')) {
        $rename = '';
        $file = $request->file('upload_file');
        $explode = explode('.', $file->getClientOriginalName());
        $originalName = $explode[0];
        $extension = $file->getClientOriginalExtension();
        $rename = $filename . date("YmdHis"). '.' . $extension;
        $path = $request->file('upload_file')->storeAs($folder, $rename, 'minio');
        return $path;
    }
    return 'Failed upload file';
}

function ReadFiles($file)
{
    if(Storage::disk('minio')->exists($file)){
        return Storage::disk('minio')->url($file);
    }
    return 'File does not exist';
}

function DeleteFiles($file)
{
    if(Storage::disk('minio')->exists($file)){
        Storage::disk('minio')->delete($file);
        return 'Success delete file';
    }
    return 'File does not exist';
}

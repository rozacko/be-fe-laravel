<?php

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

function routes()
{
    if (!Schema::hasTable('routes')) {
        return false;
    }
    $models = Cache::remember('routes', 60 * 60, function () {
        return DB::table('routes')->leftJoin('permission_has_routes', 'permission_has_routes.route_id', '=', 'routes.id')
            ->leftJoin('permissions', 'permission_has_routes.permission_id', '=', 'permissions.id')
            ->orderBy('routes.id')
            ->select('routes.*', 'permissions.name as permission_name')->whereNull('routes.deleted_at')->get()->toArray();
    });
    $routes = [];
    $id = '';
    foreach ($models as $object) {
        $model = (array) $object;
        if ($id != $model['id']) {
            $id = $model['id'];
            if ($model['permission_name'] != '') {
                $model['permission'][] = $model['permission_name'];
            }
            $routes[$model['id']] = $model;
        } else {
            if ($model['permission_name'] != '') {
                $routes[$model['id']]['permission'][] = $model['permission_name'];
            }
        }
    }
    return $routes;
}

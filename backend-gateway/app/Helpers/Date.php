<?php

function monthNumberToIndonesiaMonth($numberOfMonth)
{
    $months = ["januari", "februari", "maret", "april", "mei", "juni", "juli", "agustus", "september", "oktober", "november", "desember"];
    return $months[($numberOfMonth - 1)];
}

function validateDate($date, $format = 'Y-m-d')
{
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) === $date;
}

function formatDate($date, $format = 'Y-m-d')
{
    $timeStamp = strtotime($date);
    return date($format, $timeStamp);
}

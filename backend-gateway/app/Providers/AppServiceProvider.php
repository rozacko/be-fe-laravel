<?php

namespace App\Providers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (Schema::hasTable('smtp_configs')) {
            $value = Cache::remember('smtp_configs', 60 * 60, function () {
                return DB::table('smtp_configs')->where('status', 'y')->whereNull('deleted_at')->first();
            });
            if ($value) {
                Config::set('mail.mailers.smtp.host', $value->host);
                Config::set('mail.mailers.smtp.port', $value->port);
                Config::set('mail.mailers.smtp.encryption', $value->encryption);
                Config::set('mail.mailers.smtp.username', $value->username);
                Config::set('mail.mailers.smtp.password', $value->password);
                Config::set('mail.from.address', $value->username);
            }
        }
    }
}

<?php 
namespace App\Traits;


trait ValidationExcelImport {
    private $rules = [
        "rkap-performance" => [
            "code_plant","nama_plant","parameters","tahun","januari","februari","maret","april","mei","juni","juli","agustus","september","oktober","november","desember"
        ],
        "prognose-performance" => [
            "code_plant","nama_plant","parameters","tahun","jan","feb","mar","apr","mei","jun","jul","ags","sep","okt","nov","des"
        ],
        "rkap-maintenance" => [
            "kode_opco","nama_opco","jenis_rkap_maintenance","tahun","januari","februari","maret","april","mei","juni","juli","agustus","september","oktober","november","desember"
        ],
        "rkap-prognose-maintenance" => [
            "kode_opco","nama_opco","jenis_rkap_maintenance","tahun","jan","feb","mar","apr","mei","jun","jul","ags","sep","okt","nov","des"
        ],
        "man-power" => [
            'no','nama_perusahaan','jumlah_pekerja','status','keterangan'
        ],
        "unsafe-action" => [
            'no','cvptunit_kerja','uraian_temuan','lokasi_kejadian','tanggal_temuan','jumlah_temuan','pengawas','identifikasi_bahaya','tindakan','keterangan','status'
        ],
        "unsafe-condition" => [
            'no','area','tpm_sga','no_equipment','lokasi','uraian_temuan','saran_tindak_lanjut','pic','unit_kerja','id_trans_mso','no_notifikasi','tanggal_temuan','tanggal_target','pengecekan_1','pengecekan_2','tanggal_closing','status','keterangan'
        ],
        "cogm-report" => [
            'kode_opco','nama_opco','nama_biaya','tahun','bulan','total_biaya'
        ],
        "apar" => [
            'no','area','unit_kerja','jenis_apar','lokasi','map_baru','clamp','press_bar','hose','segel','sapot','berat_kg','bulan_maktif','tahun_maktif','status','keterangan'
        ],
        "fire-alarm-mingguan" => [
            'id','parameter','ccr_1_2_i','ccr_1_2_ii','ccr_1_2_iii','ccr_1_2_iv','ccr_1_2_status','ccr_1_2_keterangan','ccr_3_4_i','ccr_3_4_ii','ccr_3_4_iii','ccr_3_4_iv','ccr_3_4_status','ccr_3_4_keterangan'
        ],
        "fire-alarm-bulanan" => [
            'id','parameter','ccr_1_2','ccr_1_2_status','ccr_1_2_keterangan','ccr_3_4','ccr_3_4_status','ccr_3_4_keterangan'
        ],
        "safety-training-org" => [
            'no','no_pegawai','nama_pegawai','departement','unit_kerja','band','jam_pelatihan'
        ],
        "qc_rm" => [
            'jam','lsf','sim','alm','h2o','res90','res200'
        ],
        "qc_kf" => [
            'jam','lsf','sim','alm','h20','res90','res200'
        ],
        "qc_clk" => [
            'jam','lsf','sim','alm',"c3s","fcao","so3"
        ],
        "qc_coal" => [
            "ghv","ac","tm"
        ],
        "qc_semen"=>[
            "jam","so3","fcao","blaine","res45","loi"
        ],
        "qc_hot_meal"=>[
            "so3_ilc","so3_slc","derajat_kalsinasi_ilc","derajat_kalsinasi_slc"
        ],
        "hydrant" => [
            "no","main_pump","lokasi","no_pilar","cek","body_pilar","ket_pilar","valve_kopling_kiri","valve_kopling_atas","valve_kopling_kanan","ket_valve_kopling","copling_kiri","copling_kanan","ket_copling","tutup_kopling_kiri","tutup_kopling_kanan","box_casing","box_hose","box_nozle","box_kunci","ket_box","press_bar"
        ],
        "rkap-inventory" => [
            "parameters","bulan","tahun","value"
        ],
        "realisasi-capex" => [
            'no_project','no_wbs','item_capex','unit_kerja','pm','tipe','kai','op_dev','nilai_investasi','committed','pelampauan','real_spending','network','reservasi','no_pr','nilai_pr','no_po','doc_date','del_date','nilai_po','tgl_gr','spending_gr','progres_fisik','progres_report_terakhir','nilai_auc','status_bast','status_wbs','keterangan'
        ],
        "kpi-proper" => [
            'no','kpikebijakan_perusahaan','satuan','target','realisasi','kpi'
        ],
        "order-task" => [
            "order_type","order","description","system_status","equipment","equipment_description","total_act_cost","tanggal_actual","entered_by","planner_group","plant_section","res_cost_center","planning_plant","functional_loc","cost_center","created_on","user_status","basic_start_date","basic_fin_date","status_order","reason","description_uk","sub_area_proses","plant","area_proses","bulan","kode_area_proses","tahun","order_confirmation"
        ],
        "data-koreksi-notifikasi" => [
            'notif_date',
            'notif_time',
            'system_status',
            'notification',
            'order',
            'functional_loc',
            'description',
            'priority',
            'notification_type',
            'reported_by',
            'created_user',
            'changed_user',
            'main_work_ctr',
            'planner_group',
            'plant_section',
            'main_plant',
            'equipment',
            'cost_center',
            'reference_date',
            'created_on',
            'today',
            'umur_notif',
            'notif_aging',
            'description_standar',
            'notif_convert_to_order',
            'notif_close',
            'klasifikasi_uk',
        ],
        'data-koreksi-sparepart-jasa'=>[
            'tanggal',
            'pr',
            'kode_plant',
            'nama_plant',
            'pekerjaan',
            'estimator',
            'nilai',
            'engineering_tanggal',
            'engineering_status',
            'rfq_tanggal',
            'rfq_status',
            'po_tanggal',
            'po_status',
            'status_pekerjaan',
            'vendor',
            'kontrak',
            'start_date',
            'finish_date',
            'pr_ke_estimator',
            'app_p_sawab',
            'app_p_didit',
            'app_p_zaini',
            'nama_estimator',
            'pengadaan',
            'user',
            'keterangan',
        ],
        "capex-item" => [
            'no_project','no_wbs','nama_capex','tipe','jenis','nilai','inisiator_seksi','inisiator_biro','inisiator_dept','realisasi_engineering','realisasi_procurement','realisasi_delivery','realisasi_closing','progress_status','keterangan'
        ]
    ];

    private function getTemplateRules($type) 
    {
        return @$this->rules[$type] ? : [];
    }

    public function compareTemplate($type, $template) 
    {
        return count(array_diff($this->getTemplateRules($type), $template)) == 0;
    }
}
